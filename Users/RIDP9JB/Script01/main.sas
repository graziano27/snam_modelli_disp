/*CAS*/
cas mysess sessopts=(caslib="public");

/*Option section*/
option nosource;
option nosource2;
option nonotes;

/*Macro path*/
%let path_librerie =/Public/DAFNE;
%let path_root = /Users/RIDP9JB/Script01;

/*include*/
/*filename C0 FILESRVC folderpath="&path_root" filename='data_gen.sas';*/
filename C1 FILESRVC folderpath="&path_root" filename='librerie.sas';
filename C2 FILESRVC folderpath="&path_root" filename='functions.sas';

/*%include C0;*/
%include C1;
%include C2;

/*Macros variables*/
%let datetime_current=%sysfunc(datetime(),datetime20.);
%let hour_current=%sysfunc(hour("&datetime_current"dt));
%let min_data="01JAN2018:00:00:00"dt;
%let FILTRO_FAMIGLIA_H="SCADA","TERNA"; /*Famiglia dati con dettaglio orario*/
%let FILTRO_FAMIGLIA_D="METEO";			/*Famigli dati con dettaglio giornaliero*/



/*Tables INPUT*/
%let tbl_allvar=lib_acn.all_variables;
%let tbl_family=lib_acn.family_variables;
%let tbl_famfeat=work.tbl_famfeat;
%let tbl_m_feat=work.tbl_m_feat;

/*Tables OUTPUT*/
%let tbl_cal=work.tbl_cal;
%let tbl_famfeat=work.tbl_famfeat;
%let tbl_h_miss=lib_acn.tbl_h_miss;

%let tbl_base_comb=work.tbl_base_comb;           /*combinazioni tablle filtri sui quali effetturare la ricerca*/
%let tbl_missing_list=lib_acn.tbl_missing_list;  /*contiene lo storico di tutti i missing delle tabelle*/
%let tbl_missing_join=work.tbl_missing_join;     /*contiene la lista delle feature missing al tempo t */
%let tbl_missing_fv=work.tbl_missing_fv;		 /*contiene per ciascun modello le feature missing per modello*/
%let tbl_h_miss_t=lib_acn.tbl_h_miss_t;			 /*Tabella storico misisng feat 4 model*/

/*Macro utils
Lista di macro da lanciare all'occorenza
*/
/*aggiorna il campo"num_feat" in tbl_family relativo al numero di feature utilizzate dai modelli-esclude le feature non callcoalte (calendario,geo, ecc.)*/
*%num_featmodel(&tbl_family,&tbl_allvar); 
/*svuota entrambe le tabelle di storico*/
*%empty_tbl_missx2(&tbl_h_miss_t,&tbl_missing_list);


/*START Missing Collection*/

/*Arrichisce lo storico con i valori mancnti individuati*/
%select_missing(&tbl_allvar,&tbl_base_comb,&tbl_missing_list);

/*Genera la lista delle feature missing al tempo t*/
%join_tblmissing_tblallvar(&tbl_allvar,&tbl_missing_list,&tbl_missing_join);

/*unisce le feature ai modelli e crea storico*/
%join_feat_fam(&tbl_family,&tbl_missing_join,&tbl_h_miss_t);


/*END Missing Collection*/


/*Carica le tabelle sul cas per alimentare il report "Missing Values"*/
%load_to_cas(&tbl_h_miss_t,tbl_h_miss_t);

/*clean work dir*/
/*proc datasets library=work kill nolist;
quit;

/*cas mysess terminate;*/