
/*TEST*/

/*FUNCTIONS*/
%macro num_featmodel(tbl_family,tbl_allvar); 
proc sql;
	create table family_variables as
	select * , tranwrd(vars,":","") as vars1 length=9999
	from &tbl_family;
run;

data temp(drop=i num_feat vars1);
	set family_variables; 	
 	do i=1 to countw(vars1,' ');
 		var_root= scan(vars1,i,' ');
	output;
 	end;
run;

proc sql;
	create table tbl_family(drop=var_root)  as
	select  a.*,b.campo_nome_feat
	from (select distinct * from temp) as a left join &tbl_allvar as b 
		on UPCASE(b.campo_nome_feat) like catt('%',UPCASE(a.var_root),'%')  
	where length(b.campo_nome_feat) ge length(a.var_root) 
;
quit;

proc sql;
	create table &tbl_family as
	select famiglia,approccio,modello,key_cluster_hour,target,vars,area_cluster,key_area_cluster,model_key,count(distinct(campo_nome_feat)) as num_feat
	from (select distinct * from tbl_family )
	group by famiglia,approccio,modello,key_cluster_hour,target,vars,area_cluster,key_area_cluster,model_key
	;
quit;

/*
data _null_;
		dur=datetime() - &timer_start;
		put 30*'-'/ 'Total time:' dur time13.2/30*'-';
	run;
*/
%mend num_featmodel;

%macro loop_H_D(hour_lag,tbl_base_comb,num_row);

	%do i=1 %to &num_row;
			/*macro value extraction*/
			proc sql noprint;
				select * into :famiglia_dati ,:tabella , :campo_dt, :campo_nome,:Condizione1 /* ,:Condizione2*/
				from &tbl_base_comb
				where  monotonic() eq &i.;
				/*where  monotonic() eq 100;*/
			quit;
		
			proc sql noprint;
				select max(data) format datetime20. into: start_data
				from &tbl_missing_list
				where tabella="&tabella" and famiglia_dati="&famiglia_dati" and Condizione1="&Condizione1"
				;
			run;	

		/*%let start_data=01JAN2017:00:00;*/
			proc sql;
				create table tbl_filtro as
				select distinct &campo_dt format datetime20. , datepart(&campo_dt) as day format date9.,datepart(&campo_dt)-intnx("year",datepart(&campo_dt),0)+1 as dayof_year ,year(datepart(&campo_dt)) as year format 4.
				from (select &campo_dt ,&campo_nome, hour(timepart(&campo_dt)) as time_hour 
						from  &tabella 
							  where datepart(&campo_dt) ge intnx("day",datepart("&start_data"dt),-7,'same') and (&Condizione1))	                /*FILTRO DATA*/
			/*where &campo_dt ne "24FEB2022:12:00:00"dt*/	
				;
			run;
	
			/*Add last datetime as sup-lim*/
			proc sql; 
				insert into tbl_filtro 
				set &campo_dt=intnx("hour","&datetime_current"dt,1,'b')
					,day=datepart("&datetime_current"dt)
					,dayof_year=datepart("&datetime_current"dt)-intnx("year",datepart("&datetime_current"dt),0)+1
					,year=year(datepart("&datetime_current"dt)) 
			;quit;	
	
	
			proc sort  data=tbl_filtro out=tbl_filtro nodupkey;
				by  &campo_dt;
			run;

		%if &hour_lag="TRUE" %then %do;

			data tbl_filtro1;
				set tbl_filtro;
				flg_missing=0;
				lagH0=hour(timepart(&campo_dt));
				lagH1=hour(timepart(lag1(&campo_dt)));
				lagd=lag1(dayof_year);
				diff_lag=lagH0-lagH1-1;
				if lagH0 =0 then lagH0=24;
				if diff_lag gt 0 then flg_missing=1;
			run;
		%end;
		%else %do;

			data tbl_filtro1;
				set tbl_filtro;
				flg_missing=0;
				lagH0=hour(timepart(&campo_dt));
				lagH1=hour(timepart(lag1(&campo_dt)));
				lagd=lag1(dayof_year);
				diff_lag=dayof_year-lagd-1;
				if lagH0 =0 then lagH0=24;
				if diff_lag gt 0 then flg_missing=1;
			run;
		%end;
			
				proc sql noprint ;
					select sum(diff_lag) as missDate into : missDate
					from tbl_filtro1 where flg_missing=1 and diff_lag is not missing
				;run;

			%put Riga : &i. - Numero di  missing lags:&missDate;

			%if &missDate gt 0 %then %do;	
					%do k=1 %to &missDate.;
						proc sql noprint;
							select &campo_dt,diff_lag into :dt,:lags from tbl_filtro1
							where flg_missing=1 and diff_lag is not missing
									and monotonic() eq &k.;
								run;
				
					%if &hour_lag="TRUE" %then %do;
			
						%do j=1 %to &lags;
		
							%let data_lag=%sysfunc(intnx(hour,"&dt"dt,-&j.),datetime20.);
							
								proc sql; 
								insert into &tbl_missing_list 
								set tabella="&tabella" 
									,famiglia_dati= "&famiglia_dati"
									,data="&data_lag"dt
									,Condizione1="&Condizione1"
									/*,Condizione2="&Condizione2"*/;
		
						 %end;
					%end;
				%else %do;
		
						%do j=1 %to &lags;	

							%let data_lag= %sysfunc(intnx(day,%sysfunc(datepart("&dt"dt)),-&j.),date9.);

								proc sql; 
									insert into &tbl_missing_list 
									set tabella="&tabella" 
										,famiglia_dati= "&famiglia_dati"
										,data=dhms("&data_lag"d,0,0,0)
										,Condizione1="&Condizione1"
										/*,Condizione2="&Condizione2"*/;
		
						 %end;
			       %end;		
			%end;
	  %end;
%end;
%mend loop_H_D;


%macro select_missing(tbl_allvar,tbl_base_comb,tbl_missing_list);

	proc sql noprint ;
		
		create table tbl_base_comb_h as
		select distinct famiglia_dati ,tabella ,campo_dt ,campo_nome ,Condizione1 /*,Condizione2 */
		from &tbl_allvar where famiglia_dati in (&FILTRO_FAMIGLIA_H );

		create table tbl_base_comb_d as
		select distinct famiglia_dati ,tabella ,campo_dt ,campo_nome ,Condizione1 /*,Condizione2 */
		from &tbl_allvar where famiglia_dati in (&FILTRO_FAMIGLIA_D);
		
	quit;


	data &tbl_base_comb;
		set tbl_base_comb_h tbl_base_comb_d;
	run;
	
	proc sql noprint;
		select count(*) into: num_row_h
		from tbl_base_comb_h;
	
		select count(*) into: num_row_d
		from tbl_base_comb_d;
	quit;

	%let num_row= %eval(&num_row_h+&num_row_d);
	%put &num_row;

	/*tabella di base vuota*/
	/*proc sql;
		create table &tbl_missing_list as
		select tabella,famiglia_dati ,0 as data format datetime20. ,Condizione1 
		from &tbl_allvar (obs=0);
	quit;*/

%put numero di combinazioni orarie trovate: &num_row_h.;


%loop_H_D("TRUE",tbl_base_comb_h,&num_row_h.);
/*DATI GIORNALIERI*/

%put numero di combinazioni giornaliere trovate: &num_row_d.;
%loop_H_D("FALSE",tbl_base_comb_d,&num_row_d.);

proc sort data= &tbl_missing_list  out= &tbl_missing_list nodupkey;by _ALL_;run;

%mend select_missing;






%macro join_tblmissing_tblallvar(tbl_allvar,tbl_missing_list,tbl_missing_join);

data tbl_allvar00;
	set &tbl_allvar;
	format  dt_low_range dt_up_range timestamp datetime20.;
	LowRange=sum(compress(scan(range,1,','),'', 'A'),0); /*elimina numeri e selezione estremo sinistro*/
	UpRange=sum(compress(scan(range,-1,','),'', 'A'),0); /*elimina numeri e selezione estremo destro*/
	dt_low_range=intnx('hour',"&datetime_current"dt,min(LowRange+0,UpRange+0),"b");
	dt_up_range=intnx('hour',"&datetime_current"dt,max(LowRange+0,UpRange+0),"b");
	timestamp="&datetime_current"dt;
run;

/*Join*/
proc sql;
	create table &tbl_missing_join as
	select a.*, b.data
	from tbl_allvar00 as a inner join &tbl_missing_list as b on
		(b.data ge a.dt_low_range and b.data le a.dt_up_range) and
		( a.tabella=b.tabella) and
		(a.Condizione1=b.Condizione1);
quit;

%mend join_tblmissing_tblallvar;



%macro join_feat_fam(tbl_family,tbl_missing_join,tbl_h_miss_t);
	proc sql ;
		create table tbl_family1 as
				select *
				,tranwrd(vars,":","") as vars1 length=999
				, (case
				   when key_cluster_hour = ("C1") then "#6#7#8#9#"
				   when key_cluster_hour = ("C2") then "#10#11#12#"  
				   when key_cluster_hour = ("C3") then "#13#14#15#16#17#18#" 
				   when key_cluster_hour = ("C4") then "#19#20#21#22#23#"
 				   when key_cluster_hour = ("C5") then "#0#1#2#3#4#5#"  
				   else key_cluster_hour 
				   end) as key_cluster_hour2
				   ,"&datetime_current"dt as timestamp format=datetime20.
		from &tbl_family
		/*having  key_cluster_hour2 contains cats("#",12,"#") or key_cluster_hour2 eq put(12,2.)*/
		having  key_cluster_hour2 contains cats("#","&hour_current","#") or key_cluster_hour2 eq "&hour_current" /*or key_cluster_hour2 is missing --------*/
		;
	quit;

	data temp(drop=i vars);
		set tbl_family1;	 	
	 	do i=1 to countw(vars1,' ');
	 		var_root= scan(vars1,i,' ');output;
	 	end;
	run;

	proc sql;
		create table tbl_famfeat as
		select a.* ,b.* /*b.campo_nome_feat ,b.data*/
		from temp(DROP=vars1) as a left join &tbl_missing_join(DROP=timestamp) as b 
			on UPCASE(b.campo_nome_feat) like catt('%',UPCASE(a.var_root),'%')
		where length(b.campo_nome_feat) ge length(a.var_root) 
	;
	quit;

	proc sql;
			create table tbl_missing as
			select distinct *,999 as  var_root
			from tbl_famfeat(drop= var_root)
			where data not is missing
	;
	run;

	/*controllo se esiste uno storico per i valori manncanti*/
	proc sql noprint;
		select count(memname) as count
		into :flg_h_ck
		from dictionary.columns
		where memname=scan(UPCASE("&tbl_h_miss_t"),-1,".") and libname = scan(UPCASE("&tbl_h_miss_t"),1,".");
	quit;
	%if &flg_h_ck eq 0 %then
		%do;
		%put la tabella "&&tbl_h_miss_t" non esiste, verrà ricreata;
		proc sql;
				create table &tbl_h_miss_t as
				select *
				from LIB_ACN.TBL_H_000 ;
	    	quit;
		%end;
	

	/*Appendo allo storico*/
	data &tbl_h_miss_t;
		set &tbl_h_miss_t tbl_missing;
		dt_timesatmp_index=(year(datepart(timestamp))+day(datepart(timestamp))+month(datepart(timestamp)))*1000 + HOUR(timestamp);
		value=999;
		flg_missing=1;
	run;

	proc sort data=&tbl_h_miss_t out=&tbl_h_miss_t nodupkey;
		by _ALL_;
	run;

%mend join_feat_fam;

%macro empty_tbl_missx2(tbl_h_miss_t,tbl_missing_list);
proc sql;
	create table &tbl_h_miss_t as
	select *
	from LIB_ACN.TBL_H_000 ;
quit;

proc sql;
		create table &tbl_missing_list as
		select tabella,famiglia_dati ,0 as data format datetime20. ,Condizione1 
		from &tbl_allvar (obs=0);
	quit;

%mend empty_tbl_missx2;




%macro load_to_cas(tbl_name,tbl_cas_name);

	proc casutil;
		droptable casdata="&tbl_cas_name" incaslib="Public" quiet ;
		load data=&tbl_name outcaslib="Public" casout="&tbl_cas_name"  promote ;
	run;

%mend load_to_cas;
