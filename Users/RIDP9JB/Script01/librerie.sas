/* Assign LIB */
%LET ORAPATH=WEBDIPS;
%LET ORAUSR=LAB_READ;
%LET ORAPWD="LAPREREAD1";
%LET ORASCH=LAB_READ;
%LET lib_path = /opt/sas/viya/config/data/cas/default/dafne;
%LET lib_path_geo = /sasdata/modelli_geo;
%LET lib_acn= /sasdata/ACN;

LIBNAME ORALPBP2 ORACLE PATH="&ORAPATH" USER=&ORAUSR PASSWORD=&ORAPWD SCHEMA=&ORASCH DB_OBJECTS=(TABLES SYNONYMS VIEWS);
LIBNAME WEBDIPS ORACLE  DB_OBJECTS=(TABLES SYNONYMS VIEWS)  PATH=&ORAPATH  SCHEMA=SVWEBV08  USER=LAB_READ PASSWORD=LAPREREAD1 DB_OBJECTS=(TABLES SYNONYMS VIEWS); 
LIBNAME for_svil "&lib_path";
LIBNAME for_geo  "&lib_path_geo";
LIBNAME lib_acn "&lib_acn" outencoding='UTF-8';




