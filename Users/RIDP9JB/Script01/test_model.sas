/*CAS*/

/*AFFIDABILE 1 non-AFFIDABILE 0*/
cas mysess sessopts=(caslib="public");
caslib _all_ assign;
/*librerie*/
%let path_root = /Users/RIDP9JB/Script01;
/*filename C0 FILESRVC folderpath="&path_root" filename='data_gen.sas';*/
filename C1 FILESRVC folderpath="&path_root" filename='librerie.sas';
/*%include C0;*/
%include C1;



/*Macros*/
%macro load_to_cas(tbl_name,tbl_cas_name);
	proc casutil;
		droptable casdata="&tbl_cas_name" incaslib="Public" quiet ;
		load data=&tbl_name outcaslib="Public" casout="&tbl_cas_name"  promote ;
	run;
%mend load_to_cas;

/*Tables*/
%let tbl_test=lib_acn.TESTINGG1_cat;
%let tbl_train=lib_acn.TRAININGG1_cat;

/*upload cas train*/
%load_to_cas(&tbl_train,TRAININGG1_cat);

/*upload cas testing*/
%load_to_cas(&tbl_test,TESTINGG1_cat);

/*TBL TRANSFORMATION*/
data public.TRAININGG1_cat14;
	set public.TRAININGG1_cat; where ora_calcolo=14;
run;

data public.TESTINGG1_cat14;
	set public.TESTINGG1_cat; where ora_calcolo=14;
run;


/*RANDOM FOREST regression*/
proc forest data=public.TRAININGG1_cat outmodel=public.forest_model ntrees=40 seed=14;
   input ANNO GIORNO GIORNO_SETTIMANA MESE ORA_CALCOLO STAGIONE/level=interval;
   target TARGET /level=interval;
   output out=public.score_at_runtime;
run;

proc forest data=public.TESTINGG1_cat inmodel=public.forest_model;
output out=public.latest_forest;
run;

data latest_forest00;
	set public.latest_forest;
abs_residual=abs(_Residual_);
run;

proc means data=latest_forest00 mean median std max min;
var abs_residual;
run;


/*RANDOM FOREST classification*/
proc forest data=(public.TRAININGG1_cat where ora_calcolo=14) outmodel=public.forest_model_class ntrees=1000 seed=14;
   input ANNO GIORNO GIORNO_SETTIMANA MESE ORA_CALCOLO STAGIONE/level=interval;
   target TARGET_class /level=nominal;
   output out=public.score_at_runtime_class;
run;

proc forest data=public.TESTINGG1_cat inmodel=public.forest_model_class;
output out=public.latest_forest_class;
run;

proc means data=public.latest_forest_class mean median std max min sum;
var _MissIt_;
run;


/*RANDOM FOREST classification d+1 14*/
proc forest data=public.TRAININGG1_cat14 outmodel=public.forest_model_class14 ntrees=1000 seed=14;
   input ANNO GIORNO GIORNO_SETTIMANA MESE ORA_CALCOLO STAGIONE/level=interval;
   target TARGET_class /level=nominal;
   output out=public.score_at_runtime_class;
run;

/**/
proc forest data=public.TESTINGG1_cat14 inmodel=public.forest_model_class14;
output out=public.latest_forest_class14;
run;

proc means data=public.latest_forest_class14 mean median std max min sum;
var _MissIt_;
run;



/*RANDOM FOREST Regression d+1 14*/
proc forest data=public.TRAININGG1_cat14 outmodel=public.forest_model_class14 ntrees=1000 seed=14;
   input ANNO GIORNO GIORNO_SETTIMANA MESE STAGIONE/level=interval;
   target TARGET /level=interval;
   output out=public.score_at_runtime_class;
run;

/**/
proc forest data=public.TESTINGG1_cat14 inmodel=public.forest_model_class14;
output out=public.latest_forest_class14;
run;

data latest_forest_class14;
	set public.latest_forest_class14;
abs_residual=abs(_Residual_);
run;

proc means data=latest_forest_class14 mean median std max min sum;
var abs_residual;
run;
