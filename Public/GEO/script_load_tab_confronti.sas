options casdatalimit=ALL;
cas geo sessopts=(caslib=casuser timeout=31536000 locale="en_US");
caslib _all_ assign;

libname for_svil "/opt/sas/viya/config/data/cas/default/dafne";

data casuser.geo_confronto_bilanci ;
	set for_svil.geo_confronto_bilanci;
run;

data casuser.geo_confronto_previsioni ;
	set for_svil.geo_confronto_previsioni;
run;

data casuser.geo_bilancio ;
	set for_svil.geo_bilancio;
run;

/* PROC CASUTIL;           */
/*    PROMOTE CASDATA="geo_bilancio" INCASLIB="casuser" OUTCASLIB="public";  */
/* QUIT;  		 */
