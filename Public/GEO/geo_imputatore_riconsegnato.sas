/*============ Inizializzo Sessione CAS ========================*/
 options casdatalimit=ALL; 
 cas mySession sessopts=(caslib=casuser timeout=31536000 locale="en_US"); 
 caslib _all_ assign;

/*============ Allocazione Libreria FOR_SVIL ===================*/
/* libname for_svil "/sasdata/modelli_geo";  */


/*LOAD DATA*/
data casuser.geo_imputer_full;
set for_svil.geo_imputer_full;
run;


/*SELECT VARS x Models*/
/*Load the var_sel*/
proc sql noprint;
		%let var_sel=;
		select Variable
			into :var_sel separated by ' '
				from for_svil.var_selected_imputer
				;
	quit;


/* MODEL TRAIN */
%macro gradboost_area(area_id=, input_ds=, var_sel=);
/* %let area_id=7; */
/* %let input_ds=casuser.geo_imputer_full_train; */
/* %let var_sel= &var_sel; */

/* data _null_; */
/* name_file=compress(cat("/sasdata/modelli_geo/Modelli_imputer/gbt_",&area_id.,"_model.txt")); */
/* call symput("save_file",name_file ); */
/* run; */

proc gradboost data=&input_ds.(where=(area_id=&area_id. and b_train))
	learningrate=0.01 noprint ntrees=600 seed=12345 minleafsize=1 maxdepth=10
	outmodel=casuser.gbt_&area_id._model;
	input  hour_scada season time_weekend_scada/ level=nominal;
	input &var_sel / level=interval;
	target target;
	output out=casuser.gbt_&area_id._preds;

	savestate rstore=casuser.gbt_&area_id._state;
	id area_id data_gas_scada hour_scada datetime_solare_scada target;
/* 	code file="&save_file."; */
run;
%mend gradboost_area;

proc sort data=casuser.geo_imputer_full out=temp_geo_imputer_full ;
	by area_id datetime_solare_scada;
run;

data casuser.geo_imputer_full;
	set temp_geo_imputer_full;
run;


data casuser.geo_imputer_full_train;
set casuser.geo_imputer_full(where=(b_train));
run;

data _null_;
do i=1 to 7;
call execute('%NRSTR(%gradboost_area(area_id='||i||', input_ds=casuser.geo_imputer_full_train, var_sel=&var_sel.))');
end;
run;



/*PREDICT THE VALUES TO IMPUTE*/
%macro gradboost_predictions(area_id=,input_ds=);
/* %let input_ds=casuser.geo_imputer_full; */
/* %let area_id = 1; */

/* data _null_; */
/* name_file=compress(cat("/sasdata/modelli_geo/gbt_","&area_id.","_model.txt")); */
/* call symput("model_name",compress(name_file) ); */
/* run; */

/* data casuser.score_out_&area_id.(keep=area_id data_gas_scada hour_scada */
/* 									datetime_solare_scada target P_target); */
/*     set &input_ds.(where=(area_id=&area_id. )); */
/*     %include "&model_name." ; */
/* run; */

proc gradboost data=&input_ds.(where=(area_id=&area_id. )) inmodel=casuser.gbt_&area_id._model;
        output out=casuser.score_out_&area_id.;
        id area_id data_gas_scada hour_scada datetime_solare_scada target;
    run;

%mend gradboost_predictions;

/**/
/*data casuser.geo_imputer_full_pred;*/
/*set casuser.geo_imputer_full(where=(b_train=0));*/
/*run;*/
data _null_;
do i=1 to 7;
call execute('%NRSTR(%gradboost_predictions(area_id='||i||', input_ds=casuser.geo_imputer_full))');
end;
run;

/*Find the full dataset*/
data casuser.imputer_predictions;
set casuser.score_out_1 casuser.score_out_2 casuser.score_out_3 casuser.score_out_4 
casuser.score_out_5 casuser.score_out_6 casuser.score_out_7;
run;

proc sort data= casuser.imputer_predictions out=imputer_predictions_s;
by area_id data_gas_scada hour_scada datetime_solare_scada target;
run;
proc sort data= casuser.geo_imputer_full out=geo_imputer_full_s;
by area_id data_gas_scada hour_scada datetime_solare_scada target;
run;


data for_svil.geo_imputer_full_imputed(compress=yes);
merge geo_imputer_full_s imputer_predictions_s ;
by area_id data_gas_scada hour_scada datetime_solare_scada target;
run;
