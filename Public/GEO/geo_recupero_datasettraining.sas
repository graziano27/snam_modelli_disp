/*===============================================================*/
/***** rilascio 2020-09-XX - versione v1.0 *****/
/***** autori: Binda, Gregori, Rizzolo *****/
/***** BIP S.p.a. *****/
/*===============================================================*/

/*============ General Options and Creation of CAS Session ========*/
options casdatalimit=ALL;
cas mySession sessopts=(caslib=casuser timeout=31536000 locale="en_US");
caslib _all_ assign;

options minoperator; /* Necessary to recognize the IN operator within a macro */
options mprint mlogic symbolgen linesize=max;
ods exclude all;

/*============ Global Vars ========*/
%global dataset_presente start_date start_date_char start_datetime start_datetime_char;


%let start = %sysfunc(datetime());
%let start_fix = %sysfunc(datetime());
%let diff_end_month=-1;

%let lib_physical=for_svil;
%let lib_memory=public;

%let geo_output_final_name = geo_trainingset;


%put --- GENERAZIONE DATASET DI TRAINING E MODELLI GEOGRAFICI ---;

%let logic_path = /Public/GEO/;
%let lib_path = /sasdata/modelli_geo;
%let model_path = &lib_path;



filename geolib FILESRVC folderpath="&logic_path" filename='librerie_viya.sas';
filename geofcn FILESRVC folderpath="&logic_path" filename='geo_functions.sas';
filename geodat FILESRVC folderpath="&logic_path" filename='geo_dataset.sas';
filename geotrn FILESRVC folderpath="&logic_path" filename='geo_training.sas';
filename geortr FILESRVC folderpath="&logic_path" filename='geo_recupero_training.sas';

data geo_limit_day;
	geo_limit_day=intnx("day",datepart(intnx("dtmonth", &start,-1,"E")),&diff_end_month);
	call symputx("geo_limit_day",geo_limit_day);
	format geo_limit_day date9.;
run;

 
%macro clean_all();

	proc datasets lib=work kill nolist memtype=data;
	quit;

	Data MVars ( Keep = Name );
		Set SasHelp.VMacro;
		Where Scope = 'GLOBAL';

		if find(Name, "SAS") = 0;

		if find(Name, "SYS") = 0;

		if find(Name, "CLIENT") = 0;

		if find(Name, "SQL") = 0;

		if find(Name, "EG") = 0;

		if find(Name, "ORA") = 0;

		IF NAME NOT IN ("GEO_OUTPUT_FINAL_NAME", "LOGIC_PATH", "START", "LIB_PHYSICAL", "LIB_MEMORY",
		"LIB_PATH", "DATASET_PRESENTE", "GRAPHTERM");
	Run;

	Data _Null_;
		Set MVars;
		Call Symdel( Name );
	Run;

%mend clean_all;

%macro Emergenza_Dataset_Training(lib_phys=, lib_mem=, dset=, date_cut=);

	/* Allocazione librerie */
	%include geolib;

/* 	Elimino trainingset esistente in &lib_phys e &lib_mem */
 	proc datasets library=&lib_phys; 
 		delete &dset; 
 	run; 
  
 	proc casutil; 
   		droptable casdata="&dset" incaslib="&lib_mem" quiet; 
 	run; 

	%put "GEO EMERGENZA GENERAZIONE DATASET INIZIO";
/*  */
/* 	Richiamo codici per ricreare il trainingset */
	%include geofcn;
 	%include geodat; 

	/* Taglio opportunamente il trainingset */
	%put "GEO fa training il primo del mese quindi taglio dset sulla data dell ultimo consuntivo 
			del mese predecente geo_limit_day che corrisponde a fine mese - 1";

 	data &lib_phys..&dset; 
 		set &lib_phys..&dset; 
 		if data_gas_rif <=&date_cut; 
 	run; 
  
/* 	Carico dset in public promuovendolo */
 	data load; 
 		set &lib_phys..&dset; 
 	run; 
 	 
 	proc casutil; 
    		droptable casdata="&dset" incaslib="&lib_mem" quiet; 
 	run; 
  
 	proc casutil; 
 		load data=load outcaslib="&lib_mem" casout="&dset" promote; 
 	quit; 

	%put "GEO GENERAZIONE DATASET TERMINATO";

	%clean_all();
	
	%put "GEO TRAINING MODELLI";
	
	%include geortr;
	
	%put "GEO TRAINING MODELLI TERMINATO";

%mend Emergenza_Dataset_Training;

%Emergenza_Dataset_Training(lib_phys=&lib_physical, lib_mem=&lib_memory, dset=&geo_output_final_name, date_cut=&geo_limit_day);

/* data time_execution; */
/* 	dt_start = &start; */
/* 	dt_end = datetime(); */
/* 	time_execution = dt_end - dt_start; */
/* 	format dt: datetime19.; */
/* 	call symputx('time_execution', time_execution); */
/* run; */
/*  */
/* %put "========================================================================"; */
/* %put "IL CODICE SAS E' GIUNTO AL TERMINE IN: &time_execution. SECONDI"; */
/*  */
/* cas mySession terminate;  */
