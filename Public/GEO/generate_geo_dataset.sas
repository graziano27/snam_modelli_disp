
/* Fallo andare con un job */


/*Generate the full geo dataset from the 1JAN2018 to the 31MAY2020 

Join the previously generated dataset of the various components. */

/*============ Allocazione Libreria FOR_SVIL ===================*/
/* libname for_geo "/sasdata/modelli_geo";  */





%let start_dt='01MAY2017:06:00:00'dt;
%let end_dt ='01JUN2020:06:00:00'dt;

%let geo_output_bil_hour = geo_output_bil_hour_imputed;
%let geo_output_bil_day = geo_output_bil_day_imputed;
%let geo_output_target = geo_output_target_imputed;
/*_imputed  */


/*Calendario  */
data calendario;
	set for_svil.output_geo_calendar;
	where datetime_solare>=&start_dt and
			datetime_solare<&end_dt;
run;

/*SCADACons  */
data scadacons;
	set for_svil.output_geo_scadacons;
	where datetime_solare>=&start_dt and
			datetime_solare<&end_dt;
	if modello='G' then key_modello=0;
	else key_modello=1;
run;

/*Meteo citta'  */
data meteocitta;
	set for_svil.output_meteocitta_areageo;
	where datetime_solare>=&start_dt and
			datetime_solare<&end_dt;
	if modello='G' then key_modello=0;
	else key_modello=1;
run;

/*Meteo celle  */
data meteocelle;
	set for_svil.output_meteo_celle_aree;
/* 	datetime_solare = dhms(data_solare_rif, hour, 0,0); */
	where datetime_solare>=&start_dt and
			datetime_solare<&end_dt;
	if modello='G' then key_modello=0;
	else key_modello=1;
run;

/*Bilancio orario  */
data bil_hour;
	set for_svil.&geo_output_bil_hour.;
	where datetime_solare>=&start_dt and
			datetime_solare<&end_dt;
run;

/*Bilancio giornaliero  */
data bil_day;
	set for_svil.&geo_output_bil_day.;
run;

/*Terna  */
data terna_prev;
	set for_svil.output_terna_prev_aree_test;
	rename dt_calcolo=datetime_solare;
	where dt_calcolo>=&start_dt and
			dt_calcolo<&end_dt;
run;
data terna_cons;
	set for_svil.output_terna_cons_aree_test;
	rename dt_calcolo=datetime_solare;
	where dt_calcolo>=&start_dt and
			dt_calcolo<&end_dt;
run;

/*Target  */
data target;
	set for_svil.&geo_output_target.;
run;


/* MERGE THE DATASETS */
proc sort data=calendario;
	by key_modello area_id datetime_solare;
run;
proc sort data=scadacons;
	by key_modello area_id datetime_solare;
run;
proc sort data=meteocitta;
	by key_modello area_id datetime_solare;
run;
proc sort data=meteocelle;
	by key_modello area_id datetime_solare;
run;
proc sort data=terna_prev;
	by key_modello area_id datetime_solare;
run;
proc sort data=terna_cons;
	by key_modello area_id datetime_solare;
run;

proc sort data=bil_hour;
	by key_modello area_id datetime_solare;
run;

data output_temp_geo_dset;
	merge calendario(in=c) scadacons(in=s) meteocitta(in=m1) meteocelle(in=m2) terna_prev(in=t1) terna_cons(in=t2)
	bil_hour(in=t3);
	by key_modello area_id datetime_solare;
	if c and s and m1 and m2 and t1 and t2 and t3;
run;


proc sort data=output_temp_geo_dset;
	by key_modello area_id data_gas ;
run;
proc sort data=bil_day ;
	by key_modello area_id data_gas ;
run;
data output_temp2_geo_dset;
	merge output_temp_geo_dset(in=o) bil_day(in=b);
	by key_modello area_id data_gas;
	if o and b;
run;

proc sort data=output_temp2_geo_dset;
	by  area_id data_gas_rif ;
run;
proc sort data=target ;
	by  area_id data_gas_rif ;
run;
data geo_full_dset;
	merge output_temp2_geo_dset(in=o) target(in=t);
	by  area_id data_gas_rif;
	if o and t;
run;





/*SAVE THE DATASET 		 */
data for_svil.geo_full_dset (compress=yes);
	set geo_full_dset;
	where datetime_solare>=&start_dt and
			datetime_solare<&end_dt;
/* 	b_imputed =0; */
/* 	if datetime_solare<'02JUL2019:06:00:00'dt then b_imputed = 1; */
run;

data casuser.geo_full_dset (compress=yes);
	set geo_full_dset;
	where datetime_solare>=&start_dt and
			datetime_solare<&end_dt;
				b_imputed =0;
	if datetime_solare<'02JUL2019:06:00:00'dt then b_imputed = 1;
run;

PROC CASUTIL;          
   PROMOTE CASDATA="geo_full_dset" INCASLIB="casuser" OUTCASLIB="public"; 
QUIT;  		

data test;
	set public.geo_full_dset;
	keep datetime_solare modello data_gas area_id ;
run;

/*  */
/* proc sql; */
/* 	create table geo_full_dset as  */
/* 		select * */
/* 		from calendario as t1 left join scadacons as t2 */
/* 		on t1.modello=t2.modello and t1.area_id=t2.area_id and t1.datetime_solare=t2.datetime_solare */
/* 		left join meteocitta as t3 */
/* 		on t1.modello=t3.modello and t1.area_id=t3.area_id and t1.datetime_solare=t3.datetime_solare */
/* 		left join meteocelle as t4 */
/* 		on t1.modello=t4.modello and t1.area_id=t4.area_id and t1.datetime_solare=t4.datetime_solare */
/* 		left join bil_hour as t5 */
/* 		on t1.modello=t5.modello and t1.area_id=t5.area_id and t1.datetime_solare=t5.datetime_solare */
/* 		left join terna_prev as t7 */
/* 		on t1.modello=t7.modello and t1.area_id=t7.area_id and t1.datetime_solare=t7.datetime_solare */
/* 		left join terna_cons as t8 */
/* 		on t1.modello=t8.modello and t1.area_id=t8.area_id and t1.datetime_solare=t8.datetime_solare */
/* 		 */
/* 	 */
/* 		 */
/* 		left join bil_day as t6 */
/* 		on t1.modello=t6.modello and t1.area_id=t6.area_id and t1.data_gas_rif=t6.data_gas */
/*  */
/* 		left join target as t9 */
/* 		on t1.area_id=t9.area_id and t1.data_gas_rif=t9.data_gas_rif */
/*  */
/* 		 */
/* 		order by t1.datetime_solare */
/* 		; */
/* quit; */

