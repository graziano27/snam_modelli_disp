%global logic_path lib_path start_datetime_char start_datetime start_date start_date_char;

%macro set_startdates_calendar(g_gas=, g1_solare=, shift_type=, shift_value=, out_calendar=);

	data dates;
		today_gas=&g_gas;
		start_date=intnx("&shift_type",today_gas, -&shift_value,"B");
		start_date_char=put(start_date, date9.);
		start_datetime=dhms(start_date,6,0,0);
		start_datetime_char=put(dhms(start_date,6,0,0), datetime19.);
		call symputx("start_date",start_date);
		call symputx("start_date_char",start_date_char);
		call symputx("start_datetime",start_datetime);
		call symputx("start_datetime_char",start_datetime_char);
		format today_gas start_date date9. start_datetime datetime19.;
	run;

	data &out_calendar (where=(data_gas_rif<=&g_gas and data_gas_rif>=&start_date));
		date=&start_date;
		do while (date<=&g1_solare);
			do hour=0 to 23;
				datetime_solare=dhms(date,hour,0,0);
				if hour<6 then
					data_gas_rif=intnx("day", date, -1);
				else data_gas_rif=date;
				output;
			end;
			date=intnx("day", date, 1, 's');
		end;
		format date data_gas_rif date9. datetime_solare datetime19.;;
	run;
 
%mend set_startdates_calendar;

/* %let logic_path = /Public/GEO/; */
/* %let lib_path = /sasdata/modelli_geo; */
/* filename C1 FILESRVC folderpath="&logic_path" filename='librerie_viya.sas'; */
/* %include C1; */

%let g_gas="11JUL2019"d;
%let g1_solare="12JUL2019"d;
%let shift_type=day;
%let shift_value=1;
%let input_calendario_dates=input_calendario_dates;
%let ds_in=oralpbp2.dati_linepack_vw;;
%let prefix=dlp_;
%let anagrafica_dlp=webdips.geo_anag_dlp;
%let ds_output=output_dlp;

	%set_startdates_calendar(g_gas=&g_gas, g1_solare=&g1_solare, shift_type=&shift_type, shift_value=&shift_value, 
						out_calendar=&input_calendario_dates);

%put &=start_datetime_char;
%put &=start_datetime;
	/*  DLP è valorizzato solo a partire dal 02JUL2019*/
	data _null_;
		start_datetime_base ='02JUL2019:06:00:00'dt;
		temp ="&start_datetime_char"dt;
		if missing(temp) or temp < start_datetime_base
		  then start_dt_dlp = start_datetime_base;
		else start_dt_dlp = temp;
		format start_dt_dlp datetime19.;
		call symputx("start_dt_dlp", start_dt_dlp);
	run;


	data &prefix.dati_linepack_vw;
		set &ds_in (keep=data_ora_rif giorno_gas tronco metanodotto 
					area_prelievo delta_linepack  linepack
			where=(giorno_gas='10JUL2019:00:00:00'dt));
/* 					where=(data_ora_rif>= &start_dt_dlp)); */
	run;


	/*Group for metanodotto*/
	proc sql;
		create table &prefix.tab_metanodotti as 
		select distinct data_ora_rif
			 , giorno_gas
			 , metanodotto
			 , area_prelievo
			 , sum(delta_linepack) as delta_linepack
			 , sum(linepack) as linepack
		from &prefix.dati_linepack_vw
		group by 1,2,3,4
		;
	quit;

/*  */
/* 	data for_svil.tab_metanodotti_10JUL2019; */
/* set &prefix.tab_metanodotti; */
/* where giorno_gas='10JUL2019:00:00:000'dt; */
/* run; */




	/* Distinct Metanodotto */
	proc sql;
		create table &prefix.metanodotti_unique as 
		select distinct metanodotto
				, area_prelievo 
		from &prefix.tab_metanodotti;
	quit;

	data &prefix.calendario_dlp (keep=dt_calendario);
		set &input_calendario_dates;
		rename datetime_solare=dt_calendario;
	run;

	proc sql;
		create table &prefix.cross_join as 
		select *
		from &prefix.metanodotti_unique 
		cross join
		&prefix.calendario_dlp
		order by dt_calendario;
	quit;

	/*Create full table with missing*/
	proc sql;
		create table &prefix.full_table as 
		select t1.*
			 , t2.delta_linepack
			 , t2.linepack
			 , t2.data_ora_rif
			 , t2.giorno_gas
		from &prefix.cross_join t1 
		left join &prefix.tab_metanodotti t2
		on t1.dt_calendario = t2.data_ora_rif and 
			t1.metanodotto=t2.metanodotto and 
			t1.area_prelievo=t2.area_prelievo
		order by metanodotto, area_prelievo, dt_calendario;
	quit;

	/*ffil dei tronchi per i valori prima dell'inizio funzionamento tronco*/
	data &prefix.dati_linepack_vw_ffill(keep= data_ora_rif flag_ffill giorno_gas 
												metanodotto area_prelievo delta_linepack linepack);
		set &prefix.full_table;
		by metanodotto area_prelievo;
		valore_clean = delta_linepack;
		valore_clean2 = linepack;
		flag_ffill = 0;

		retain temp_val temp_val2;

		/* Reset TEMP when the BY-Group changes */
		if first.metanodotto or first.area_prelievo then do;
			temp_val=.;
			temp_val2=.;
		end;

		/* Assign TEMP when valore_clean is non-missing */
		if valore_clean ne . then
			temp_val=valore_clean;
		/* When valore_clean is missing, the retained value is  assigned*/
		else if valore_clean=. then
			do;
				flag_ffill = 1;
				valore_clean=temp_val;
			end;

		if valore_clean2 ne . then
			temp_val2=valore_clean2;
		else if valore_clean2=. then
			valore_clean2=temp_val2;

		delta_linepack = valore_clean;
		linepack = valore_clean2;
		data_ora_rif=dt_calendario;
		/*Fill Giorno Gas	*/
		date_rif =datepart(data_ora_rif);
		hour=hour(data_ora_rif);
		if hour<6 then do;
			giorno_gas = intnx("day", date_rif, -1);
		end;
		else do;
			giorno_gas =date_rif;
		end;

		format giorno_gas date9.;
	run;
	

	/*Join Anagrafica*/
	proc sql;
		create table &prefix.dlp_metanodotti_anagrafica as
		select t1.*
			 , t2.Area_ID
			 , t2.Area_Descrizione
			 , t2.Area_Label
		from &prefix.dati_linepack_vw_ffill as t1 
		inner join &anagrafica_dlp(where=(compress(fl_deleted)="0")) as t2 
		on (compress(t1.metanodotto)=compress(t2.metanodotto)) and 
		   (compress(t1.area_prelievo)=compress(t2.area_prelievo))
		;
	quit;

	proc sql;
		create table &ds_output as
		select distinct data_ora_rif as datetime_solare
			 , giorno_gas as data_gas
			 , area_id
			 , area_descrizione
			 , area_label
			 , sum(delta_linepack) as dlp
			 , sum(linepack) as linepack
		from &prefix.dlp_metanodotti_anagrafica  
		group by 1,3
		order by area_id, data_ora_rif
		;
	quit;


