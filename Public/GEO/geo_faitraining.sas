%put --- GENERAZIONE DATASET DI TRAINING E MODELLI GEOGRAFICI ---;

%let logic_path = /Public/GEO/;
%let lib_path = /sasdata/modelli_geo;

filename C1 FILESRVC folderpath="&logic_path" filename='librerie_viya.sas';
filename C2 FILESRVC folderpath="&logic_path" filename='geo_master_datasettraining.sas';
%include C1;
%include C2;