
%let start_date_fulldset = "01APR2017"d;
%let start_date_train = "02JUL2019"d;

/*Keep only consuntivi of meteocelle modello G per il giorno gas*/
%macro load_meteocelle(input_ds, ouput_ds, start_date);
	/*%let geo_full_dset=input.geo_full_dset;*/
	/*%let ouput_name=meteocelle_4Imputation;*/
	data meteo_data (drop=modello);
		set &input_ds 
			(where=(modello="G" and hour = 5 and data_gas>=&start_date));

/*Il meteocelle consuntivo è riferito al giorno prima! Quindi shifto indietro di uno*/


		data_gas_scada = intnx('day', data_gas, -1);
		keep area_id data_gas_scada cel_c_:;
	run;

	proc contents data=meteo_data out=meteo_data_contents(keep=name) noprint;
	run;

	proc sql noprint;
		select name into :keeplist separated by ' '
			from meteo_data_contents
				where (name like '%area_tot' or  name like '%mean%')
					and(name not like '%besthp_%' or  name like '%vc_%')
		;
	quit;

	data &ouput_ds;
		set meteo_data;
		keep area_id data_gas_scada &keeplist;
	run;

%mend load_meteocelle;

/*Load SCADA COns old (not discretive for area geografica)*/
%macro load_scadabil_ric_old(input_ds, ouput_ds, start_date);
	/*%let input_ds=input.scada_bil_ric_ts;*/
	/*%let ouput_ds=scada_ric_old;*/
	data &ouput_ds;
		set &input_ds(
			where=(datetime_solare>= dhms(&start_date,6,0,0)));
		rename scada_bil_ric=scada_bil_global_ric;
		data_gas = ifn(hour(datetime_solare)<6, 
			intnx('day',datepart(datetime_solare),-1), datepart(datetime_solare));
		format datetime_solare datetime19. data_gas date9.;
	run;

%mend load_scadabil_ric_old;

/*Keep only consuntivi of meteocelle modello G per il giorno gas*/
%macro load_scada_data(input_ds, ouput_ds, start_date);

	data &ouput_ds;
		set &input_ds;
		where  data_gas >= &start_date and key_modello=0;
		keep datetime_solare hour data_gas data_gas_rif area_id key_modello scada_bil: 
			scada_cons_C_: scada_cons_I_: scada_cons_T_: time_weekend;
	run;	

%mend load_scada_data;




%load_scadabil_ric_old(for_svil.scada_bil_ric, scadabil_ric_old, &start_date_fulldset);
%load_meteocelle(for_svil.geo_full_dset, geo_meteocelle, &start_date_fulldset);
%load_scada_data(for_svil.geo_full_dset, geo_scada, &start_date_fulldset);

proc sort data=scadabil_ric_old;
	by datetime_solare;
run;

proc sort data=geo_scada;
	by datetime_solare ;
run;

data geo_scada_bil_full(where=( data_gas> &start_date_fulldset));
	merge geo_scada(in=L)  scadabil_ric_old; 
	by datetime_solare ;
	if L;
	scada_bil_app_ric = scada_bil_imp + scada_bil_trs + scada_bil_stc + 
		scada_bil_pnl - scada_bil_exp ;

	datetime_solare_scada = intnx("hour", datetime_solare, -1);
	hour_scada = hour(datetime_solare_scada);
	data_gas_scada = ifn(hour_scada >= 6, datepart(datetime_solare_scada), 
		intnx("day", datepart(datetime_solare_scada), -1));

	time_weekend_scada = ifn(weekday(data_gas_scada) in (1,7), 1, 0);

	season=0;
	if month(data_gas_scada) in (4,5) then season = 1;
	else if month(data_gas_scada) in (6, 7, 8) then season = 2;
	else if month(data_gas_scada) in (9, 10) then season = 3;
	format  datetime_solare_scada datetime19.;
	format data_gas_scada date9.;
/*	if nmiss(of _numeric_) > 0 then delete;*/
/*drop il primo giorno, che ha molti missing*/
	
run;
proc sort data=geo_meteocelle;
	by data_gas_scada area_id;
run;
proc sort data=geo_scada_bil_full;
by data_gas_scada area_id;
run;

data geo_scada_bil_full;
merge geo_scada_bil_full(in=p) geo_meteocelle ;
by  data_gas_scada area_id;
if p; 
run;

/*One-Hot-Encoding*/
data geo_scada_bil_full(drop=i);
	set geo_scada_bil_full ;
	array hour_array(24) hour_scada_0 - hour_scada_23;
	do i=1 to 24;
		hour_array{i} =ifn(hour_scada = i-1,1,0);
	end;
	array area_id_array(7) area_id_1 - area_id_7;
	do i=1 to 7;
		area_id_array{i} =ifn(area_id = i,1,0);
	end;
	array season_array(4) season_0 - season_3;
	do i=1 to 4;
		season_array{i} =ifn(season = i-1,1,0);
	end;
run;


proc sort data=geo_scada_bil_full;
by datetime_solare_scada data_gas_scada area_id ;
run;

data for_svil.geo_imputer_full(drop=  scada_bil_ric: scada_bil_dlp:);
set geo_scada_bil_full;
rename scada_bil_ric = target;
b_train = ifn(data_gas_scada<&start_date_train.,0,1);
run;


/* proc sort data=for_svil.geo_imputer_full; */
/* 	by datetime_solare_scada; */
/* run; */





/*SELECT VARS x Models*/
data casuser.geo_scada_bil_full(drop=  scada_bil_ric: scada_bil_dlp:);
set geo_scada_bil_full;
rename scada_bil_ric = target;
run;

proc varreduce data=casuser.geo_scada_bil_full technique=var ;
	
    class  area_id_: season_: hour_scada_: time_weekend_scada;
        reduce supervised target = cel_c_: scada_bil_: scada_cons: /
			maxeffects=50 varexp=1;
    displayout SelectionSummary=var_exp_selection_eff;
run;


data for_svil.var_selected_imputer;
set casuser.var_exp_selection_eff;
run;
