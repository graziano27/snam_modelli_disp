%macro put_run_date_init(function);

	%global dt_run;

	data _null_;
		call symputx("dt_run",datetime());
		call symputx("dt_run_char",put(datetime(),datetime19.));
	run;
	%put --- START PROCESSING FUNCTION &function &dt_run_char ---;

%mend put_run_date_init;

%macro put_run_time(function);

	data _null_;
		time_exe=datetime()-&dt_run;
		call symputx("time_exe",put(time_exe,time.));
	run;
	%put --- END PROCESSING FUNCTION &function. TOTAL PROCESSING TIME: &time_exe. ---;

%mend put_run_time;

%macro set_startdates_calendar(g_gas=, g1_solare=, shift_type=, shift_value=, out_calendar=);

	data dates;
		today_gas=&g_gas;
		start_date=intnx("&shift_type",today_gas, -&shift_value,"B");
		start_date_char=put(start_date, date9.);
		start_datetime=dhms(start_date,6,0,0);
		start_datetime_char=put(dhms(start_date,6,0,0), datetime19.);
		call symputx("start_date",start_date);
		call symputx("start_date_char",start_date_char);
		call symputx("start_datetime",start_datetime);
		call symputx("start_datetime_char",start_datetime_char);
		format today_gas start_date date9. start_datetime datetime19.;
	run;

	data &out_calendar (where=(data_gas_rif<=&g_gas and data_gas_rif>=&start_date));
		date=&start_date;
		do while (date<=&g1_solare);
			do hour=0 to 23;
				datetime_solare=dhms(date,hour,0,0);
				if hour<6 then
					data_gas_rif=intnx("day", date, -1);
				else data_gas_rif=date;
				output;
			end;
			date=intnx("day", date, 1, 's');
		end;
		format date data_gas_rif date9. datetime_solare datetime19.;;
	run;
 
%mend set_startdates_calendar;


%macro calendar_mod(input_cal_dset=, mod=, mod_char=, out=);

/*%let input_cal_dset=&input_calendario_dates; */
/*%let mod=0; */
/*%let mod_char=G; */
/*%let out=dset_with_time_0;*/


	data temp;
		set &input_cal_dset;
		/*data_gas_rif for the specific model*/
		data_gas_rif=intnx("day",data_gas_rif, &mod);
		time_month = month(data_gas_rif);
		time_day = weekday(data_gas_rif);
		time_week = week(data_gas_rif);
		format data_gas_rif date9.;
	run;

	data temp;
		set temp;
		time_day_7 = 0;
		time_day_6 = 0;
		time_day_1 = 0;
		time_day_2 = 0;
		time_month_7 = 0;
		time_month_4 = 0;
		time_month_12 = 0;
		time_month_5 = 0;
		time_festivo = 0;
		time_prefestivo = 0;

		if day(data_gas_rif) = 1 and month(data_gas_rif) = 1 then
			time_festivo = 1;

		if day(data_gas_rif) = 6 and month(data_gas_rif) = 1 then
			time_festivo = 1;

		if day(data_gas_rif) = 25 and month(data_gas_rif) = 4 then
			time_festivo = 1;

		if day(data_gas_rif) = 1 and month(data_gas_rif) = 5 then
			time_festivo = 1;

		if day(data_gas_rif) = 2 and month(data_gas_rif) = 6 then
			time_festivo = 1;

		if day(data_gas_rif) = 15 and month(data_gas_rif) = 8 then
			time_festivo = 1;

		if day(data_gas_rif) = 1 and month(data_gas_rif) = 11 then
			time_festivo = 1;

		if day(data_gas_rif) = 8 and month(data_gas_rif) = 12 then
			time_festivo = 1;

		if data_gas_rif = holiday('EASTER',year(data_gas_rif)) then
			time_festivo = 1;

		if data_gas_rif = intnx('day',holiday('EASTER',year(data_gas_rif)),1) then
			time_festivo = 1;

		if data_gas_rif = intnx('day',holiday('EASTER',year(data_gas_rif)),2) then
			time_festivo = 1;

		if day(data_gas_rif) = 25 and month(data_gas_rif) = 12 then
			time_festivo = 1;

		if day(data_gas_rif) = 26 and month(data_gas_rif) = 12 then
			time_festivo = 1;

		if day(data_gas_rif) = 31 and month(data_gas_rif) = 12 then
			time_festivo = 1;

		if day(data_gas_rif) = 5 and month(data_gas_rif) = 1 then
			time_prefestivo = 1;

		if day(data_gas_rif) = 24 and month(data_gas_rif) = 4 then
			time_prefestivo = 1;

		*if data_gas_rif = intnx('day',holiday('EASTER',year(data_gas_rif)),-1) then time_prefestivo = 1;
		if day(data_gas_rif) = 30 and month(data_gas_rif) = 4 then
			time_prefestivo = 1;

		if day(data_gas_rif) = 1 and month(data_gas_rif) = 6 then
			time_prefestivo = 1;

		if day(data_gas_rif) = 14 and month(data_gas_rif) = 8 then
			time_prefestivo = 1;

		if day(data_gas_rif) = 31 and month(data_gas_rif) = 10 then
			time_prefestivo = 1;

		if day(data_gas_rif) = 7 and month(data_gas_rif) = 12 then
			time_prefestivo = 1;

		if day(data_gas_rif) = 24 and month(data_gas_rif) = 12 then
			time_prefestivo = 1;

		if day(data_gas_rif) = 30 and month(data_gas_rif) = 12 then
			time_prefestivo = 1;
		time_week_52 = 0;
		time_week_32 = 0;
		time_week_51 = 0;
		time_week_33 = 0;

		if time_day = 7 then
			time_day_7 = 1;

		if time_day = 6 then
			time_day_6 = 1;

		if time_day = 1 then
			time_day_1 = 1;

		if time_day = 2 then
			time_day_2 = 1;

		if time_month = 7 then
			time_month_7 = 1;

		if time_month = 4 then
			time_month_4 = 1;

		if time_month = 12 then
			time_month_12 = 1;

		if time_month = 5 then
			time_month_5 = 1;

		if time_week = 52 then
			time_week_52 = 1;

		if time_week = 32 then
			time_week_32 = 1;

		if time_week = 51 then
			time_week_51 = 1;

		if time_week = 33 then
			time_week_33 = 1;
		drop time_week time_month;
	run;

	data temp_ftd;
		set temp;
		fake_time_day=time_day;
		time_natale=0;
		time_ferragosto=0;

		if time_festivo=1 and time_day=2 then
			fake_time_day=5;

		if time_festivo=1 and time_day=3 then
			fake_time_day=5;

		if time_festivo=1 and time_day=4 then
			fake_time_day=7;

		if time_festivo=1 and time_day=5 then
			fake_time_day=7;

		if time_festivo=1 and time_day=6 then
			fake_time_day=7;

		if data_gas_rif = intnx('day',holiday('EASTER',year(data_gas_rif)),0) then
			fake_time_day = 1;

		if data_gas_rif = intnx('day',holiday('EASTER',year(data_gas_rif)),1) then
			fake_time_day = 5;

		/*natale*/
		/*da sabato prima di natale a domenica dopo befana*/
		if month(data_gas_rif) = 12 and data_gas_rif >= intnx("week", mdy(12, 25, year(data_gas_rif)), -1, 'E') 
			and weekday(mdy(12, 25, year(data_gas_rif))) < 7 then
			do;
				time_natale=1;
			end;
		else if month(data_gas_rif) = 12 and data_gas_rif >= intnx("day", mdy(12, 25, year(data_gas_rif)), -1) 
			and weekday(mdy(12, 25, year(data_gas_rif))) = 7 then
			do;
				time_natale=1;
			end;

		if month(data_gas_rif) = 1 and data_gas_rif<=intnx("week", mdy(1, 6, year(data_gas_rif)), 0, 'E') 
			and weekday(mdy(1, 6, year(data_gas_rif))) > 1 then
			do;
				time_natale=1;
			end;
		else if month(data_gas_rif) = 1 and data_gas_rif <= mdy(1, 6, year(data_gas_rif)) 
			and weekday(mdy(1, 6, year(data_gas_rif))) = 1 then
			do;
				time_natale=1;
			end;

		/*ferragosto*/
		/*tutta la settimana di ferragosto da sabato prima a domenica*/
		if data_gas_rif >= intnx("day", mdy(8,15, year(data_gas_rif)), -2) 
			and data_gas_rif <= intnx("day", mdy(8,15, year(data_gas_rif)), 2) then
			do;
				time_ferragosto=1;
			end;
	run;

	data aree_dist;
		do area_id=1 to 7;
			output;
		end;
	run;

	/* Cross join con le aree */
	proc sql;
		create table temp_ftd as 
		select *
		from aree_dist
		cross join temp_ftd
		;
	quit;

	/* time ric zona calda/fredda

	a seconda del calendario e dell'area accendo con l'uno 
		ex: zona NO ha D (calda), F(fredda)
		-> accendi in accordance con il calendario nazionale
	time_risc_tot = mean(time_ric_calda, time_ric_fredda)*/
	data temp_new_vars;
		set temp_ftd;
		time_weekend = 0;
		time_weekend_ext = 0;
		time_day_7_ext = 0;
		time_day_7_ext_holy = 0;
		time_festivo_ext = 0;
		fake_time_day_group1 = 0;
		fake_time_day_group2 = 0;
		time_risc_calda = 0;
		time_risc_fredda = 0;
		time_risc_tot=0;

		/*		time_risc_zona_A = ,*/
		/*		time_risc_zona_B = 7 calda,*/
		/*		time_risc_zona_C = 7 fredda, 6 calda*/
		/*		time_risc_zona_D = 6 fredda,4-5 calda*/
		/*		time_risc_zona_E = 4-5 fredda, 1-2-3 calda*/
		/*		time_risc_zona_F = 1-2-3 fredda*/
		time_day_group1 = 0;
		time_day_group2 = 0;

		if time_day_7 = 1 or time_festivo = 1 then
			do;
				time_day_7_ext = 1; /* Sabato esteso ai festivi */
				time_day_7_ext_holy = 1; /* Sabato esteso ai festivi e ai periodi di vacanza */
			end;

		if time_festivo = 1 then
			time_festivo_ext = 1;

		if time_ferragosto = 1 or time_natale = 1 then
			do;
				time_day_7_ext_holy = 1;
				time_festivo_ext = 1;
			end;

		if time_day in (1, 7) then
			time_weekend = 1;

		if time_day in (1, 7) or time_festivo = 1 then
			time_weekend_ext = 1;

		/*		if (month(data_gas_rif) in (12, 1, 2)) or (month(data_gas_rif) = 3 and day(data_gas_rif) <= 15) then*/
		/*			time_risc_zona_A = 1;*/
		/*B*/
		if (month(data_gas_rif) in (12, 1, 2, 3)) and area_id=7 then
			time_risc_calda = 1;

		/*C*/
		if (month(data_gas_rif) in (12, 1, 2, 3)) or (month(data_gas_rif) = 11 and day(data_gas_rif) >= 15) then
			do;
				if area_id = 7 then
					time_risc_fredda=1;
				else if area_id =6 then
					time_risc_calda = 1;
			end;

		/*D*/
		if (month(data_gas_rif) in (11, 12, 1, 2, 3)) or (month(data_gas_rif) = 4 and day(data_gas_rif) <= 15) then
			do;
				if area_id =6 then
					time_risc_fredda=1;
				else if area_id in (4,5) then
					time_risc_calda = 1;
			end;

		/*E*/
		if (month(data_gas_rif) in (11, 12, 1, 2, 3)) or (month(data_gas_rif) = 4 and day(data_gas_rif) <= 15)
			or (month(data_gas_rif) = 10 and day(data_gas_rif) >= 15) then
			do;
				if area_id in(4,5) then
					time_risc_fredda=1;
				else if area_id in (1,2,3) then
					time_risc_calda = 1;
			end;

		/*F*/
		if area_id in (1,2,3) then
			time_risc_fredda = 1;

		/* Riscaldamento complessivo */
		time_risc_tot= mean(time_risc_calda, time_risc_fredda);

		if fake_time_day in (2, 3, 4, 5, 6) then
			fake_time_day_group1 = 1;
		else if fake_time_day in (1,  7) then
			fake_time_day_group2 = 1;

		if time_day in (2, 3, 4, 5, 6) and time_festivo = 0 then
			time_day_group1 = 1;

		if time_day in (1, 7) or time_festivo = 1 then
			time_day_group2= 1;
		drop time_day fake_time_day;
	run;

	data &out;
		set temp_new_vars;
		data_gas_rif_364 = intnx("day", data_gas_rif, -364);
		data_gas_rif_366 = intnx("day", data_gas_rif, -366);
		time_festivo_lag = 0;

		if day(data_gas_rif_364) = 1 and month(data_gas_rif_364) = 1 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 1 and month(data_gas_rif_366) = 1 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 6 and month(data_gas_rif_364) = 1 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 6 and month(data_gas_rif_366) = 1 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 25 and month(data_gas_rif_364) = 4 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 25 and month(data_gas_rif_366) = 4 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 1 and month(data_gas_rif_364) = 5 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 1 and month(data_gas_rif_366) = 5 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 2 and month(data_gas_rif_364) = 6 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 2 and month(data_gas_rif_366) = 6 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 15 and month(data_gas_rif_364) = 8 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 15 and month(data_gas_rif_366) = 8 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 1 and month(data_gas_rif_364) = 11 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 1 and month(data_gas_rif_366) = 11 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 8 and month(data_gas_rif_364) = 12 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 8 and month(data_gas_rif_366) = 12 then
			time_festivo_lag = 1;

		if data_gas_rif_364 = holiday('EASTER',year(data_gas_rif_364)) then
			time_festivo_lag = 1;
		else if data_gas_rif_366 = holiday('EASTER',year(data_gas_rif_366)) then
			time_festivo_lag = 1;

		if data_gas_rif_364 = intnx('day',holiday('EASTER',year(data_gas_rif_364)),1) then
			time_festivo_lag = 1;
		else if data_gas_rif_366 = intnx('day',holiday('EASTER',year(data_gas_rif_366)),1) then
			time_festivo_lag = 1;

		if data_gas_rif_364 = intnx('day',holiday('EASTER',year(data_gas_rif_364)),2) then
			time_festivo_lag = 1;
		else if data_gas_rif_366 = intnx('day',holiday('EASTER',year(data_gas_rif_366)),2) then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 25 and month(data_gas_rif_364) = 12 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 25 and month(data_gas_rif_366) = 12 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 26 and month(data_gas_rif_364) = 12 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 26 and month(data_gas_rif_366) = 12 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 31 and month(data_gas_rif_364) = 12 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 31 and month(data_gas_rif_366) = 12 then
			time_festivo_lag = 1;
		format data: date9.;
	run;

	data &out(drop = i );
		length modello $ 3.;
		set &out (drop= fake_time_day_group1 fake_time_day_group2 data_gas_rif_364 data_gas_rif_366);
		array hour_array(24) hour_0 - hour_23;
		array area_id_array(7) area_id_1 - area_id_7;

		do i=1 to 24;
			hour_array{i} =ifn(hour = i-1,1,0);
		end;
		do i=1 to 7;
			area_id_array{i} =ifn(area_id = i,1,0);
		end;

		key_modello=&mod;
		modello = "&mod_char";
	run;

	proc sort data=&out;
		by data_gas_rif datetime_solare modello; 
	run;

%mend calendar_mod;


%macro calendar(shift_type=, shift_value=, output_geo_calendar=);

/* %let shift_type=day; */
/* %let shift_value=3;*/
/* %let output_geo_calendar=&output_geo_calendar; */
/* %let input_calendario_dates=&input_calendario_dates;*/

	%set_startdates_calendar(g_gas=&g_gas, g1_solare=&g1_solare, shift_type=&shift_type, shift_value=&shift_value, out_calendar=&input_calendario_dates);

	%calendar_mod(input_cal_dset=&input_calendario_dates, mod=0, mod_char=G, out=dset_with_time_0);
	%calendar_mod(input_cal_dset=&input_calendario_dates, mod=1, mod_char=G1,out=dset_with_time_1);

	data &output_geo_calendar;
		set dset_with_time_0 dset_with_time_1;
		by data_gas_rif datetime_solare modello; 
	run;

%mend calendar;

%macro checkds(dsn);
	%if %sysfunc(exist(&dsn)) %then
		%do;
			%let dataset_presente=1;
		%end;
	%else
		%do;
			%let dataset_presente=0;
		%end;
%mend checkds;

%macro gestione_storico_anomalie_ffill(ds_in=, var_2count=, count_text=, dt_riferimento=,
										 storico_anomalie=);
										 
	PROC SQL;
		CREATE TABLE WORK.anomalie_by_dt AS 
			SELECT /* COUNT_DISTINCT_of_ID_SCADA */
		(COUNT(DISTINCT(t1.&var_2count.))) AS &count_text., t1.&DT_RIFERIMENTO 
			FROM WORK.&ds_in. t1 
			WHERE t1.flag_ffill = 1 
			GROUP BY t1.&DT_RIFERIMENTO;
	QUIT;

	data anomalie_now;
		set anomalie_by_dt;
		where &dt_riferimento. = intnx("hour", &dt_solare, -1);
		datetime_solare = dhms(datepart(&dt_solare), hour(&dt_solare), 0, 0);
	run;

	data base_anomalie;
		datetime_solare = dhms(datepart(&dt_solare), hour(&dt_solare), 0, 0);
		format datetime_solare datetime19.;
	run;

	data anomalie_now_complete;
		merge base_anomalie(in=L) anomalie_now(in=R);
		by datetime_solare;

		if L;

		if missing(&count_text.) then
			&count_text. = 0;
	run;

	%if %sysfunc(exist(&storico_anomalie)) %then
		%do;

			proc append base=&storico_anomalie data=anomalie_now_complete force;
			run;

		%end;
	%else
		%do;

			data &storico_anomalie;
				length datetime_solare &count_text. 8;
				format datetime_solare datetime19.;
				stop;
			run;

			proc append base=&storico_anomalie data=anomalie_now_complete force;
			run;

		%end;
%mend gestione_storico_anomalie_ffill;


%macro gestione_anomalie_terna(dt_base=, ds_in1=, ds_in2=, ds_out1=, ds_out2=, storico_anomalie_terna=);
	/*	%let dt_base=&start;*/
	/*	%let ds_in1=terna_prev;*/
	/*	%let ds_in2=terna_cons;*/
	/*	%let ds_out1=terna_prev;*/
	/*	%let ds_out2=terna_cons;*/
	/*	%let storico_anomalie_terna=&output_anomalie_terna;*/
	/* Fisso a 0 min e 0 secondi il datetime &start*/
	data _null_;
		cut_date=dhms(datepart(&dt_base),hour(&dt_base),0,0);
		call symput("cut_date",cut_date);
	run;

	/* Taglio i ds di input */
	proc sql;
		select max(data_rif) into :date_terna_prev 
			from &ds_in1 
				(where = (data_rif le datepart(&cut_date) ));
	quit;

	proc sql;
		select max(data_rif) into :date_terna_cons 
			from &ds_in2 
				(where = (dt_inserimento_val le &cut_date));
	quit;

	/* Calcolo la stagione in cui sono: a seconda che sia inverno o estate ho una diversa dt_baseline */
/*	data stagioni;*/
/*		date_now=datepart(&cut_date);*/
/*		d1=nwkdom(5, 1, 3, year(date_now));*/
/*		d2=nwkdom(5, 1, 10, year(date_now));*/
/*		format d1 d2 date9.;*/
/*		length stagione $7;*/
/**/
/*		if (date_now>= d1  and date_now< d2) then*/
/*			stagione="estate";*/
/*		else if (date_now < d1 or date_now >= d2) then*/
/*			stagione="inverno";*/
/*		call symput("stagione",strip(stagione));*/
/*		format date_now date9.;*/
/*	run;*/
/**/
/*	%put Sono in &stagione;*/

	data check_terna (keep=date_terna_prev date_terna_cons date_baseline_prev date_baseline_cons
		check_ritardo_terna_prev check_ritardo_terna_cons stagione);
		retain date_baseline_cons date_terna_cons check_ritardo_terna_cons 
			date_baseline_prev date_terna_prev check_ritardo_terna_prev;

		/* Creo date BASELINE su cui effettuare il confronto */
		dt_now=&cut_date;
		date_now=datepart(dt_now);
		dt_baseline=dhms(datepart(dt_now),13,0,0);

		/* La massima data di riferimento delle previsioni deve essere uguale ad oggi */
		date_baseline_prev=date_now;

		/* Se sono prima delle 13 di oggi --> 
			ultima data_rif per i consuntivi = altro ieri 
			(ultima data_rif per le previsioni = oggi)
		*/
		if dt_now lt dt_baseline then
			do;
				date_baseline_cons=intnx("day",date_now,-2);
			end;
		/* Se sono dopo le 13 di oggi --> 
				ultima data_rif per i consuntivi = ieri 
				(ultima data_rif per le previsioni = domani)
		*/
		else
			do;
				date_baseline_cons=intnx("day",date_now,-1);
			end;

		/* Effettuo il confronto */
		date_terna_prev=&date_terna_prev;
		date_terna_cons=&date_terna_cons;
		check_ritardo_terna_prev=ifn(date_terna_prev=date_baseline_prev,0,1);
		check_ritardo_terna_cons=ifn(date_terna_cons=date_baseline_cons,0,1);
		call symput("check_ritardo_terna_prev",check_ritardo_terna_prev);
		call symput("check_ritardo_terna_cons",check_ritardo_terna_cons);
		format date: date9. 
			dt: datetime19.;
	run;

	/* Creo la tabella totale di anomalie */
	data anomalie_terna_all;
		datetime_solare=&cut_date;
		check_ritardo_terna_prev=&check_ritardo_terna_prev;
		check_ritardo_terna_cons=&check_ritardo_terna_cons;
		format datetime_solare datetime19.;
	run;

	/* ====== Gestione eventuali anomalie nelle previsioni terna ====== */
	%if &check_ritardo_terna_prev=1 %then
		%do;
			%put ===== Previsioni terna non disponibili =====;

			/* Taglio il ds di input per &cut_date */
			data &ds_out1;
				set &ds_in1
					(where = (dt_inserimento_val le &cut_date));
			run;

			/* Inserisco dati vecchi di una settimana o di un giorno */
			proc sql noprint;
				select max(data_rif)
					into :date_terna_prev
						from  &ds_out1;
			quit;

			/* Controllo se il lag7 del giorno mancante che devo inserire (date_terna_prev+1gg) ? festivo.
				   Se ? festivo --> inserisco come valori del giorno mancante quelli di ieri
				   Altrimenti --> inserisco come valori del giorno mancante quelli di 1 settimana fa 
			*/
			data check_lag7_festivo_prev;
				date_terna_prev=&date_terna_prev;
				date_to_insert=intnx("day",&date_terna_prev,1);
				lag7=intnx("day",date_to_insert,-7);
				time_festivo = 0;

				if day(lag7) = 1 and month(lag7) = 1 then
					time_festivo = 1;

				if day(lag7) = 6 and month(lag7) = 1 then
					time_festivo = 1;

				if day(lag7) = 25 and month(lag7) = 4 then
					time_festivo = 1;

				if day(lag7) = 1 and month(lag7) = 5 then
					time_festivo = 1;

				if day(lag7) = 2 and month(lag7) = 6 then
					time_festivo = 1;

				if day(lag7) = 15 and month(lag7) = 8 then
					time_festivo = 1;

				if day(lag7) = 1 and month(lag7) = 11 then
					time_festivo = 1;

				if day(lag7) = 8 and month(lag7) = 12 then
					time_festivo = 1;

				if lag7 = holiday('EASTER',year(lag7)) then
					time_festivo = 1;

				if lag7 = intnx('day',holiday('EASTER',year(lag7)),1) then
					time_festivo = 1;

				if lag7 = intnx('day',holiday('EASTER',year(lag7)),2) then
					time_festivo = 1;

				if day(lag7) = 25 and month(lag7) = 12 then
					time_festivo = 1;

				if day(lag7) = 26 and month(lag7) = 12 then
					time_festivo = 1;

				if day(lag7) = 31 and month(lag7) = 12 then
					time_festivo = 1;
				call symput("time_festivo_prev",time_festivo);
				call symput("lag7prev",lag7);
				format date_to_insert date_terna_prev lag7 date9.;
			run;

			data terna_prev_add (drop=dt_inserimento_val data_rif dt_rif increment
				rename=(data_rif_add=data_rif dt_inserimento_val_add=dt_inserimento_val
				dt_rif_add=	dt_rif) );
				set &ds_out1;

				/* Se il lag7 non ? festivo, allora sostituisco il giorno mancante con il lag7 */
				if &time_festivo_prev=0 then
					do;
						if data_rif=&lag7prev;
						increment=7;
					end;

				/* Se il lag7 ? festivo, sostituisco il giorno mancante con ultimo valore disponibile */
				else
					do;
						if data_rif=&date_terna_prev;
						increment=1;
					end;

				data_rif_add=intnx("day",data_rif,increment);
				dt_inserimento_val_add=intnx("dtday",dt_inserimento_val,increment,"s");
				dt_rif_add=intnx("dtday",dt_rif,increment,"s");
				format data_rif_add date9. dt_inserimento_val_add dt_rif_add datetime19.;
			run;

			data &ds_out1;
				set &ds_out1 terna_prev_add;
			run;

		%end;
	%else
		%do;
			%put ===== Previsioni terna disponibili =====;
		%end;

	/*  ====== Gestione eventuali anomalie nei consuntivi terna ====== */
	%if &check_ritardo_terna_cons=1 %then
		%do;
			%put ===== Consuntivi terna non disponibili =====;

			/* Taglio il ds per &cut_date */
			data &ds_out2;
				set &ds_in2
					(where = (dt_inserimento_val le &cut_date));
			run;

			/* Inserisco dati vecchi di una settimana o di un giorno */
			proc sql noprint;
				select max(data_rif)
					into :date_terna_cons
						from &ds_out2;
			quit;

			/* Controllo se il lag7 del giorno mancante che devo inserire (date_terna_cons+1gg) ? festivo.
				   Se ? festivo --> inserisco come valori del giorno mancante quelli di ieri
				   Altrimenti --> inserisco come valori del giorno mancante quelli di 1 settimana fa 
			*/
			data check_lag7_festivo_cons;
				date_terna_cons=&date_terna_cons;
				date_to_insert=intnx("day",&date_terna_cons,1);
				lag7=intnx("day",date_to_insert,-7);
				time_festivo = 0;

				if day(lag7) = 1 and month(lag7) = 1 then
					time_festivo = 1;

				if day(lag7) = 6 and month(lag7) = 1 then
					time_festivo = 1;

				if day(lag7) = 25 and month(lag7) = 4 then
					time_festivo = 1;

				if day(lag7) = 1 and month(lag7) = 5 then
					time_festivo = 1;

				if day(lag7) = 2 and month(lag7) = 6 then
					time_festivo = 1;

				if day(lag7) = 15 and month(lag7) = 8 then
					time_festivo = 1;

				if day(lag7) = 1 and month(lag7) = 11 then
					time_festivo = 1;

				if day(lag7) = 8 and month(lag7) = 12 then
					time_festivo = 1;

				if lag7 = holiday('EASTER',year(lag7)) then
					time_festivo = 1;

				if lag7 = intnx('day',holiday('EASTER',year(lag7)),1) then
					time_festivo = 1;

				if lag7 = intnx('day',holiday('EASTER',year(lag7)),2) then
					time_festivo = 1;

				if day(lag7) = 25 and month(lag7) = 12 then
					time_festivo = 1;

				if day(lag7) = 26 and month(lag7) = 12 then
					time_festivo = 1;

				if day(lag7) = 31 and month(lag7) = 12 then
					time_festivo = 1;
				call symput("time_festivo_cons",time_festivo);
				call symput("lag7cons",lag7);
				format date_to_insert date_terna_cons lag7 date9.;
			run;

			data terna_cons_add (drop=dt_inserimento_val data_rif dt_rif increment
				rename=(data_rif_add=data_rif dt_inserimento_val_add=dt_inserimento_val
				dt_rif_add=	dt_rif) );
				set &ds_out2;

				/* Se il lag7 non ? festivo, allora sostituisco il giorno mancante con il lag7 */
				if &time_festivo_cons=0 then
					do;
						if data_rif=&lag7cons;
						increment=7;
					end;

				/* Se il lag7 ? festivo, sostituisco il giorno mancante con ultimo valore disponibile */
				else
					do;
						if data_rif=&date_terna_cons;
						increment=1;
					end;

				data_rif_add=intnx("day",data_rif,increment);
				dt_inserimento_val_add=intnx("dtday",dt_inserimento_val,increment,"s");
				dt_rif_add=intnx("dtday",dt_rif,increment,"s");
				format data_rif_add date9. dt_inserimento_val_add dt_rif_add datetime19.;
			run;

			data &ds_out2;
				set &ds_out2 terna_cons_add;
			run;

		%end;
	%else
		%do;
			%put ===== Consuntivi terna disponibili =====;
		%end;

	/* Storicizzo eventuali anomalie */
	%put ==== Storicizzo Anomalie Terna ====;

	%if %sysfunc(exist(&storico_anomalie_terna)) %then
		%do;

			proc append base=&storico_anomalie_terna data=anomalie_terna_all force;
			run;

		%end;
	%else
		%do;

			data &storico_anomalie_terna;
				set anomalie_terna_all;
			run;

		%end;

%mend gestione_anomalie_terna;



%macro merge_anomalie(lib_in=, ds_out=);

	data _null_;
		dt_focus=dhms(datepart(&dt_solare),hour(&dt_solare),0,0);
		call symput("dt_focus",dt_focus);
	run;

	proc sql;
		create table lista_anomalie as 
			select distinct catx(".","&lib_in",compress(lowcase(memname))) as tables_anomalie
				from dictionary.columns
					where lowcase(libname)="&lib_in" and 
						lowcase(memname) in ("geo_anomalie_scadacons" "geo_anomalie_entryexit" "geo_anomalie_dlp" "geo_anomalie_transiti"
						"geo_anomalie_bilancio" "geo_anomalie_terna");
	quit;

	proc sql noprint;
		select distinct tables_anomalie into: tables_anomalie
			separated by " " 
		from lista_anomalie
		;
	quit;

	proc sql noprint;
		select count(*) into: n_tabelle_anomalie
			from lista_anomalie
		;
	quit;

	%if &n_tabelle_anomalie >0 %then
		%do;
			%put ===== Merge Anomalie =====;

			data add_bil;
				set &lib_in..geo_anomalie_bilancio end=eof;
				if eof;
				run;
			proc sort data=add_bil nodupkey;
				by datetime_solare;
				run;
			data add_scadacons;
				set &lib_in..geo_anomalie_scadacons end=eof;
				if eof;
				run;
			proc sort data=add_scadacons nodupkey;
				by datetime_solare;
				run;
			data add_entryexit;
				set &lib_in..geo_anomalie_entryexit end=eof;
				if eof;
				run;
			proc sort data=add_entryexit nodupkey;
				by datetime_solare;
				run;
			data add_dlp;
				set &lib_in..geo_anomalie_dlp end=eof;
				if eof;
				run;
			proc sort data=add_dlp nodupkey;
				by datetime_solare;
				run;
			data add_transiti;
				set &lib_in..geo_anomalie_transiti end=eof;
				if eof;
				run;
			proc sort data=add_transiti nodupkey;
				by datetime_solare;
				run;
			data add_terna;
				set &lib_in..geo_anomalie_terna end=eof;
				if eof;
				run;
			proc sort data=add_terna nodupkey;
				by datetime_solare;
				run;	
			
			data add;
				merge add_bil add_scadacons add_entryexit add_dlp add_transiti add_terna;
				by datetime_solare;
			run;

			proc stdize data=add reponly missing=0 out=add;
			run;

			%if  %sysfunc(exist(&ds_out)) %then %do;
			
			
				proc append base=&ds_out data=add force;
				run;
			%end;
			%else %do;
				data &ds_out;
				set add;
				run;
				%end;
			
		%end;
%mend merge_anomalie;

%macro scadacons(shift_type=, shift_value=,  input_anagrafica=, input_calendario_dates=, input_misuratori_scada=, g_gas=, g1_solare=,
				prefix=, cons_output=);

/*%let input_anagrafica=&anag_scadacons;*/
/*%let input_misuratori_scada=oralpbp2.misuratori_scada;*/
/*%let g_gas=&g_gas;*/
/*%let prefix=sc_;*/
/*%let g1_solare=&g1_solare;*/
/*%let shift_type=month;*/
/*%let shift_value=1;*/
/*%let input_calendario_dates=&input_calendario_dates;*/
/*%let cons_output=&output_scadacons;*/

	%set_startdates_calendar(g_gas=&g_gas, g1_solare=&g1_solare, shift_type=&shift_type, shift_value=&shift_value, out_calendar=&input_calendario_dates);

	proc sort data=&input_anagrafica (where=(stato eq 1 and classe ne "")) 
			out=&prefix.anagrafica_portate_scada (drop=stato rename=(id_misura_aop=id_scada)) nodupkey;
		by id_misura_aop;
	run;
	/* 229 misuratori scada */

	/* Prodotto cartesiano - misuratori + aree & date */
	proc sql;
		create table &prefix.cross_join as 
		select * 
		from (&prefix.anagrafica_portate_scada (keep=id_scada classe segno area:) 
		cross join &input_calendario_dates)
		order by id_scada, datetime_solare;
	quit;

	/* Prendo il valore di ciascun misuratore */
	data &prefix.misuratori_scada(drop=id_unita_misura rename=(data_ora_rif=dt_riferimento));
		set &input_misuratori_scada(where=(data_ora_rif>="&start_datetime_char"dt));
	run;

	proc sort data=&prefix.misuratori_scada;
		by id_scada dt_riferimento;
	run;

	/* Merge con calendario + aree */
	data &prefix.misuratori_scada_join;
		merge &prefix.cross_join(in=L rename=(datetime_solare=dt_riferimento)) 
			&prefix.misuratori_scada(in=R);
		by id_scada dt_riferimento;
		if L;
	run;
	
	/* FFILL Missing values */
	data &prefix.cons_tmp(keep= id_scada DT_RIFERIMENTO valore_clean classe segno flag_ffill
		area:);
		set &prefix.MISURATORI_SCADA_JOIN;
		valore_clean_tmp=abs(input(scan(valore,1,'**'), 12.));
		valore_string = scan(valore,2,'**');
		by id_scada;
		valore_clean = abs(valore_clean_tmp*1);
		flag_ffill = 0;

		/* RETAIN the new variable */
		retain temp_val;

		/* Reset TEMP when the BY-Group changes */
		if first.id_scada then
			temp_val=.;

		/* Assign TEMP when valore_clean is non-missing */
		if valore_clean ne . then
			temp_val=valore_clean;

		/* When valore_clean is missing, assign the retained value of TEMP into X */
		else if valore_clean=. then
			do;
				valore_clean=temp_val;

				if not missing(temp_val) then
					flag_ffill=1;
			end;

		if missing(valore_clean) then
			valore_clean = 0;
	run;
	
	%if &flg_anomalie=1 %then
		%do;
			%put ===== Storicizzazione Anomalie Dati Scada Cons =====;

			%gestione_storico_anomalie_ffill(ds_in=&prefix.cons_tmp, var_2count=ID_SCADA,
			count_text=count_scadacons_missing, dt_riferimento=dt_riferimento,
			storico_anomalie=&output_anomalie_scadacons);
		%end;

	data &prefix.cons_clean_class ;
		set &prefix.cons_tmp(drop=flag_ffill);
		valore_clean=abs(valore_clean);
		if segno eq 0 then valore_clean = - valore_clean;
	run;

	/* Sum by dt_riferimento, classe and area */
	proc means data=&prefix.cons_clean_class sum noprint;
		class dt_riferimento classe area: ;
		var valore_clean;
		output out=&prefix.cons_agg(where=(_TYPE_ in (31)) drop= _FREQ_) SUM=cons;
	run;

	/* Sum by dt_riferimento and area (where classe ne 'f')*/
	/*NOTE: for each cons centrale there are eight parameters in the final table */
	proc sql;
		create table &prefix.cons_agg_sum as 
		select dt_riferimento
			, area_id
			, (sum(cons)) as cons
			, "SUM" as classe
		from &prefix.cons_agg
		where lowcase(classe) ne 'f'
		group by dt_riferimento
			, area_id;
	quit;

	/* CENTRALI */
	data &prefix.cons_agg_centrali_temp (where=(compress(classe) ne ""));
		set &prefix.cons_clean_class(drop=classe);
		if id_scada in (1162769 1163306) then classe="CNT_MES";
		else if id_scada in (807 1192686) then classe="CNT_ENN";
		else if id_scada in (2113) then classe="CNT_TAR";
		else if id_scada in (2306) then classe="CNT_MON";
		else if id_scada in (1010) then classe="CNT_MEL";
		else if id_scada in (3602) then classe="CNT_GAL";
		else if id_scada in (30000307) then classe="CNT_TBR";
		else if id_scada in (1173204) then classe="CNT_PRE";
		else if id_scada in (3654) then classe="CNT_MAL";
		else if id_scada in (10440 12364) then classe="CNT_IST";
		else if id_scada in (1123102) then classe="CNT_MAS";
		else if id_scada in (30155416) then classe="CNT_SER";
		else if id_scada in (30090869) then classe="CNT_MNB";
	run;

	proc sql;
		create table &prefix.cons_agg_centrali_temp as 
		select distinct dt_riferimento
			 , classe
			 , (sum(valore_clean)) as cons
		from &prefix.cons_agg_centrali_temp
		group by dt_riferimento, classe
		;
	quit;

	proc sql;
		create table &prefix.tab_aree as 
		select distinct area_id
			 		, area_label
		from &prefix.anagrafica_portate_scada
		order by area_id
		;
	quit;

	proc sql;
		create table &prefix.cons_agg_centrali as 
		select * from (
			&prefix.tab_aree cross join &prefix.cons_agg_centrali_temp)
		order by dt_riferimento, classe;
	quit;


	/* Features Engineering */
	/*Aggiungo colonna con somma tot per ogni ora*/
	data &prefix.cons_agg_ok(drop=classe rename=(classe_new=classe));
		length classe_new $ 10;
		set &prefix.cons_agg_centrali &prefix.cons_agg_sum &prefix.cons_agg(drop=_type_);
		classe_new=classe;
		hour=hour(DT_RIFERIMENTO);
		data_gas_rif=ifn(hour>=6, datepart(DT_RIFERIMENTO), datepart(DT_RIFERIMENTO)-1 );
		format data_gas_rif date9.;
	run;

	proc sort data=&prefix.cons_agg_ok nodupkey;
		by classe area_id dt_riferimento;
	run;

	proc sql;
		create table &prefix.classe_unique as 
		select distinct classe
		from &prefix.cons_agg_ok;
	quit;

	proc sql;
		create table &prefix.dt_area_unique as 
		select distinct area_id
					, dt_riferimento
					, hour
					, data_gas_rif
		from &prefix.cons_agg_ok
		;
	quit;

	proc sql noprint;
		create table &prefix.cross_join_classe as 
		select * 
		from (&prefix.classe_unique
			cross join &prefix.dt_area_unique);
	quit;

	proc sql; 
		create table &prefix.cons_agg_ok_full as
		select t1.*
			, t2.cons
		from &prefix.cross_join_classe t1 
		left join &prefix.cons_agg_ok t2
		on t1.classe = t2.classe and t1.dt_riferimento =t2.dt_riferimento
			and t1.area_id = t2.area_id;
	quit;


	proc sort data=&prefix.cons_agg_ok_full nodupkey;
		by classe area_id DT_RIFERIMENTO;
	run;

	/* passo di data che fa intnx che manda avanti di un ora cons_lag */
	data &prefix.cons_lag_temp;
		set &prefix.cons_agg_ok_full; 
		dt_riferimento= intnx('hour', dt_riferimento, 1);
		cons_lag = cons;
		keep dt_riferimento cons_lag classe area_id;
	run;

	data &prefix.cons_lag;
		merge &prefix.cons_lag_temp &prefix.cons_agg_ok_full;
		by classe area_id dt_riferimento;
	run;

	proc sort data=&prefix.cons_lag;
		by classe area_id data_gas_rif;
	run;

	/* uso cons_lag perch? in DT_RIFERIMENTO ho il valore precedente */
	/* creo statistiche per giorno gas */
	proc expand data=&prefix.cons_lag out=&prefix.cons_eng();
		by classe area_id data_gas_rif;
		convert cons_lag=cons_mean / transformout=(cuave);
		convert cons_lag=cons_std / transformout=(custd);
		convert cons_lag=cons_max / transformout=(cumax);
		convert cons_lag=cons_min / transformout=(cumin);
	run;

	%scadacons_clean();

	proc sort data=&prefix.cons_eng;
		by classe area_id dt_riferimento;
	run;

	proc expand data=&prefix.cons_eng out=&prefix.cons_eng();
		by classe area_id;
		id dt_riferimento;
		convert cons_lag=cons_lag24 / transformout=(lag 23);
		convert cons_lag=cons_smooth / transformout=(movave 2);
		convert cons_lag=cons_sum24 / transformout=(movsum 24);
	run;

	%scadacons_clean();

	proc sort data=&prefix.cons_eng;
		by classe area_id data_gas_rif;
	run;

	proc contents data=&prefix.cons_eng out=&prefix.cons_vars(keep=NAME) noprint;
	run;

	proc sort data=&prefix.cons_vars out=&prefix.cons_vars SORTSEQ=linguistic;
		by descending name;
	run;

	data &prefix.vars_ordered;
		length var_list $ 200;
		set &prefix.cons_vars end=eof;
		retain var_list;

		if _N_ = 1 then
			do;
				var_list = NAME;
			end;
		else
			do;
				new_var = NAME;
				var_list = catx(' ', var_list, new_var);
			end;

		if eof then
			call symputx('reorder_vars', var_list);
	run;

	data &prefix.cons_eng;
		retain &reorder_vars cons_current_hour;
		set &prefix.cons_eng;
		by classe area_id data_gas_rif;
		retain cons_cumday;

		if first.data_gas_rif then
			cons_cumday = 0;
		else cons_cumday + cons_lag;
		rename cons_lag=cons cons=cons_current_hour;
	run;

	proc sort data=&prefix.cons_eng out=&prefix.cons_eng_sort;
		by dt_riferimento area_id classe;
	run;

	proc transpose data=&prefix.cons_eng_sort out=&prefix.cons_eng_t_temp;
		by dt_riferimento area_id classe;
		var cons:;
	run;

	proc transpose data=&prefix.cons_eng_t_temp out=&prefix.cons_eng_t(drop=_name_ cons_current_hour:) delimiter=_;
		by dt_riferimento area_id;
		id _name_ classe;
		var col1;
	run;

	/* Save final datasets */
	proc contents data=&prefix.cons_eng_t out=&prefix.vars(keep=name) noprint;
	run;

	data &prefix.vars;
		set &prefix.vars(where=(name like "cons%"));
		if scan(lowcase(name),2,"_") not in ("sum24", "std", "smooth", "min", "mean", "max", "lag24", "cumday") then do;
			new_name=compress(cat("scada_",name));
		end;
		else do;
			to_start=index(name,scan(name,3,"_"));
			new_name=compress(cat("scada_cons_",substr(name,to_start,length(name)),"_",scan(name,2,"_")));
		end;
		drop to_start;
		rename_String=catx("=", name, new_name);
	run;

	proc sql noprint;
		select rename_String into :rename_list separated by " "
			from &prefix.vars;
	run;

	data cons_output(rename=(dt_riferimento=datetime_calcolo));
		set &prefix.cons_eng_t;
		rename &rename_list;
	run;
	
	data &cons_output;
		length modello $3;
		set cons_output;

		data_gas = ifn(hour(datetime_calcolo)<6,
					intnx("day", datepart(datetime_calcolo), -1),
					datepart(datetime_calcolo));

		do key_modello = 0 to 1;
			if key_modello = 0 then
				do;
	/*data gas a cui ti trovi -> rispetto al datetime laggato*/
					data_gas_rif= intnx("day", data_gas, 0);

					datetime_riferimento_dati = dhms(data_gas_rif,0,0,0);
	/*				datetime_solare=;*/

					modello = "G";
					format datetime_riferimento_dati datetime19. 
						data_gas_rif  date9.;
					output;
				end;
			else if key_modello = 1 then
				do;
					data_gas_rif = intnx("day", data_gas, 1);

					datetime_riferimento_dati = dhms(data_gas_rif,0,0,0);

					modello = "G1";
					format datetime_riferimento_dati datetime19. 
						data_gas_rif  date9.;
					output;
				end;
		end;

		rename datetime_calcolo=datetime_solare;
	run;

%mend scadacons;

%macro scadacons_clean();

	data &prefix.cons_eng;
		set &prefix.cons_eng;
		if cons_lag =. then cons_lag= 0;

		if (cons_mean > -0.0001 and cons_mean < 0.0001) or cons_mean=. then
			cons_mean = 0;

		if (cons_std > -0.0001 and cons_std < 0.0001) or cons_std=. then
			cons_std = 0;

		if (cons_max > -0.0001 and cons_max < 0.0001) or cons_max=. then
			cons_max = 0;

		if (cons_min > -0.0001 and cons_min < 0.0001) or cons_min=. then
			cons_min = 0;

		if (cons_lag24 > -0.0001 and cons_lag24 < 0.0001) or cons_lag24=. then
			cons_lag24 = 0;

		if (cons_smooth > -0.0001 and cons_smooth < 0.0001) or cons_smooth=. then
			cons_smooth = 0;

		if (cons_sum24 > -0.0001 and cons_sum24 < 0.0001) or cons_sum24=. then
			cons_sum24 = 0;
	run;

%mend scadacons_clean;



%macro meteocelle_preparedataset1_group(group, selectvar);
	/*%let group = clima_1;*/
	/*%let selectvar=t: umid alt_min PRECIPITAZIONI NEVE;*/
/*	%put &=group;*/
/*	%put &=selectvar;*/


	%if %index(%lowcase(&group.),vc)=0 and %index(%lowcase(&group.),besthp)=0 %then
		%do;

			data input_clusters_value(drop=grouping);
			set input_clusters(where=(grouping = "&group")
				keep=data_rif &selectvar.  area_id grouping);
			run;

			proc sort data=input_clusters_value;
				by data_rif area_id;
			run;

			/*stats su temp/umidita/alt*/
			proc means data=input_clusters_value noprint;
				output out=new_vars1_cluster(drop=_TYPE_ _FREQ_);
				var %substr(&selectvar.,1,15);
				by data_rif area_id;
			run;

			proc sort data=new_vars1_cluster out=new_vars1_cluster;
				by data_rif area_id _stat_;
			run;

			proc transpose data=new_vars1_cluster(where=(_stat_ not in ("N"))) out=new_vars1_cluster_t;
				by  data_rif area_id _stat_;
			run;

			data new_vars1_cluster_t(drop=_label_ _name_ _stat_);
				set new_vars1_cluster_t(rename=(col1=value));
				key = catx("_",_name_, _stat_);
			run;

			proc sort data=new_vars1_cluster_t out=new_vars1_cluster_t;
				by   data_rif area_id key;
			run;

			proc transpose data=new_vars1_cluster_t out=output_run_tmp(Drop=_name_);
				id key;
				by data_rif area_id;
			run;

			/*stats sulle precipitazioni/neve*/
			proc sql noprint;
				create table new_vars2_cluster
					as select 
						t1.data_rif as data_rif, t1.area_id,
						mean(t1.PRECIPITAZIONI) as prec_mean
						,sum(t1.PRECIPITAZIONI) as prec_sum,
						mean(t1.neve) as neve_mean
						,sum(t1.neve) as neve_sum
					from input_clusters_value t1
						group by data_rif, area_id
							order by data_rif, area_id;
			quit;

			proc sql noprint;
				create table output_run as select t1.*,
					t2.prec_mean, t2.prec_sum, t2.neve_mean ,t2.neve_sum
				from output_run_tmp t1 inner join new_vars2_cluster t2
					on t1.data_rif=t2.data_rif and t1.area_id=t2.area_id
				order by data_rif,area_id;
			quit;

			/* Rename delle colonne */
			proc contents data=output_run(drop=data_rif area_id) out=cont(keep=name) noprint;
			run;

			proc sql noprint;
				select distinct compress(cat(name ,"=",name,"_","&group")) into: rename_list
					separated by " "
				from cont
				;
			quit;

			data output;
				set output_run;
				rename &rename_list;
			run;

		%end;
	%else
		%do;
	data output_run(drop=grouping) ;
		set input_clusters(where=(grouping = "&group")
			keep=data_rif &selectvar.  area_id grouping);
	run;
			data output;
				set output_run;
				rename &selectvar.=&group.;
			run;

		%end;

	proc sort data=output;
		by data_rif area_id;
	run;

	data &dset_meteo_out;
		merge &dset_meteo_out(in=p) output;
		by data_rif area_id;
	run;

%mend meteocelle_preparedataset1_group;

%macro meteocelle_preparedataset1(dset_meteo,grouping, dset_meteo_out);
/*	%let dset_meteo = consuntivi;*/
/*	%let grouping = &grouping_all;*/
/*	%let dset_meteo_out = consuntivi_out;*/

	proc sort data=&dset_meteo  out=meteo_city(drop=superficie riga colonna
		rename=(t_max_percepita=t_mp altezza_min=alt_min
				UMIDITA=umid ));
		by cella data_rif;
	run;

/*	proc sort data=&grouping;*/
/*	by cella;*/
/*	run;*/
	proc sql;
	create table input_clusters as 
	select t1.*, t2.*
	from meteo_city t1 full join &grouping t2
	on t1.cella = t2.cella;
	quit;



	proc sort data=input_clusters(keep=data_rif area_id) out= &dset_meteo_out nodupkey;
		by data_rif area_id;
	run;

/*	noprint*/
	proc sql noprint ;
		create table dist_group as 
		select distinct grouping as GROUP
				from &grouping. ;
	quit;

	data _null_;
		length selectvar $50;
		set dist_group;
		selectvar = '';
		if group in ('area_tot','zona_1', 'zona_2', 'clima_1', 'clima_2', 
					'consumi_1', 'consumi_2', 'sub_1', 'sub_2', 'sub_3') then
				selectvar ='t: umid alt_min PRECIPITAZIONI NEVE';
		/*TODO: the clusters in this group could be evaluated in a solo step but it is time inefficent (tested, v.07)	*/
			else if  find(group,'alt_min') ge 1 then selectvar = 'alt_min';

			else if find(group,'t_max_perc') ge 1 then selectvar = 't_mp';

			else if find(group,'t_med') ge 1 then selectvar = 't_med';

			else if find(group,'t_gg') ge 1 then selectvar = 't_gg';

			else if find(group,'umid') ge 1 then selectvar = 'umid';

		call execute ('%NRSTR(%meteocelle_preparedataset1_group(group='||group||',selectvar='||selectvar||'))');
	run;

/*fill missing with 0*/
/*	data &dset_meteo_out;*/
/*	   set &dset_meteo_out;*/
/*	   array change _numeric_;*/
/*	        do over change;*/
/*	            if change=. then change=0;*/
/*        end;*/
/* 	run ;*/

%mend meteocelle_preparedataset1;

%macro meteocelle_fillrename(input_dset, input_calendario, prefix, output_dset_new, start_cal);

/* %let input_dset=meteocelle_cons; */
/* %let input_calendario=&input_calendario; */
/* %let prefix="cel_c"; */
/* %let output_dset_new=meteocelle_cons; */
/* %let start_cal=&celle_start; */

	proc sort data=&input_dset out=temp;
		by area_id datetime_solare;
	run;

	proc sort data=&input_calendario;
		by area_id datetime_solare;
	run;

	data temp;
		merge temp &input_calendario(in=p where=(datepart(datetime_solare)>=&start_cal) 
									keep=datetime_solare area:);
		by area_id datetime_solare;
		if p;
	run;

	/*--*/
	proc sql noprint;
		%let droplist1=;
		select compress(cat(&prefix, "_" , lowcase(name)))
			into :droplist1 separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="TEMP"
						and (lowcase(name) not like "dat%"
						and lowcase(name) not like "area%");
	quit;
/*%put &=droplist1;*/
	proc sql noprint;
		%let savelist1=;
		select lowcase(name)
			into :savelist1 separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="TEMP"
						and (lowcase(name) not like "dat%" 
						and lowcase(name) not like "area%");
	quit;


	proc sort data=TEMP;
		by area_id;
	run;

	data &output_dset_new(drop=a b i j &savelist1 );
/*&savelist2*/
		set	TEMP;
		array new &droplist1;
/*&droplist2;*/
		array old &savelist1;
/*&savelist2;*/
		retain new;
		by area_id;
	

		if first.area_id then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;
	run;

%mend meteocelle_fillrename;
%macro meteocelle_selectprev(dset_input, dist, dset_output);

	proc sort data=&dset_input.(where=(distanza_insert_rif=&dist)) out=prev nodupkey dupout=dup;
		by cella data_calc data_prev dt_insert;
	run;

	data &dset_output.(rename=(data_prev=data_rif));
		set prev;
		by cella data_calc  data_prev;

		if  last.data_prev;
	run;

%mend meteocelle_selectprev;

%macro meteocelle_preparedataset2(dset_in1, dset_in2, dist1, dist2, dset_output);

	data meteocelle_vals_1;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set &dset_in1.(rename=(data_rif=data_gas_rif));
		data_calc=intnx("day",data_gas_rif,&dist1.);
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(data_calc,10,0,0);
		format data_calc date9. datetime_solare datetime_calc datetime19.;
	run;

	data meteocelle_vals_2;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set &dset_in2.(rename=(data_rif=data_gas_rif));
		data_calc=intnx("day",data_gas_rif,&dist2.);
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(intnx("day",data_gas_rif,-1),6,0,0);
		format data_calc date9. datetime_solare datetime_calc datetime19.;
	run;

	data &dset_output.;
		set meteocelle_vals_1 meteocelle_vals_2;
	run;

	proc sort data=&dset_output.;
		by data_gas_rif datetime_solare area_id;
	run;

%mend meteocelle_preparedataset2;


%macro meteocelle(g_gas=, g1_solare=, shift_type=, shift_value=, input_celle_cons=, 
					input_celle_prev=, input_calendario_dates=, anag_meteo_celle=, grouping_all=, output_final=);

/*%let g_gas=&g_gas;*/
/*%let g1_solare=&g1_solare;*/
/*%let shift_type=month;*/
/*%let shift_value=2;*/
/*%let input_celle_cons=&input_celle_cons; */
/*%let input_celle_prev=&input_celle_prev; */
/*%let anag_meteo_celle=&anag_meteo_celle;*/
/*%let input_calendario_dates=&input_calendario_dates; */
/*%let grouping_all=&grouping_celle; */
/*%let output_final=&output_celle;*/

	%set_startdates_calendar(g_gas=&g_gas, g1_solare=&g1_solare, shift_type=&shift_type, shift_value=&shift_value, 
							out_calendar=&input_calendario_dates);

	/* Add Areas to calendar dates */
	proc sql;
		create table aree_dist as 
		select distinct area_id
			   , area_label
			   , area_descrizione
		from &anag_meteo_celle (where=(fl_deleted =0))
		order by area_id
		;
	quit;

	proc sql;
		create table input_calendario as 
		select a.*
			  ,b.*
		from &input_calendario_dates as a 
		cross join aree_dist as b
		;
	quit;

	data consuntivi(drop=data where=(data_rif>=&start_date));
		set &input_celle_cons(drop=k_d_e_meteo_consuntivo);
		cella=cat("s",put(superficie,z2.0),"_","r",put(riga,z2.0),"c",put(colonna,z2.0));
		data_rif=datepart(data);
		format data_rif date9.;
		t_med=(t_max+t_min)/2;
		t_gg=max(0,20-t_med);
	run;

	data previsioni(where=(data_calc>=&start_date));
		set &input_celle_prev(drop=k_d_e_dati_meteo_aree);
		cella=cat("s",put(superficie,z2.0),"_","r",put(riga,z2.0),"c",put(colonna,z2.0));
		data_prev=datepart(data);
		data_calc=datepart(dt_insert);
		distanza_insert_rif=data_prev-data_calc;
		t_med=(t_max+t_min)/2;
		t_gg=max(0,20-t_med);
		format data_prev data_calc date9.;
	run;

	/* consuntivi */
	%meteocelle_preparedataset1(consuntivi, &grouping_all, consuntivi_out);

	/* previsioni g */
	%meteocelle_selectprev(previsioni, 0, previsioni_g);
	%meteocelle_preparedataset1(previsioni_g,&grouping_all, previsioni_g_out);

	/* previsioni g1 */
	%meteocelle_selectprev(previsioni, 1, previsioni_g1);
	%meteocelle_preparedataset1(previsioni_g1, &grouping_all, previsioni_g1_out);

	/* previsioni g2 */
	%meteocelle_selectprev(previsioni, 2, previsioni_g2);
	%meteocelle_preparedataset1(previsioni_g2, &grouping_all, previsioni_g2_out);

	/*------------------*/
	/*Meteo celle ha prev pi? corte quindi fillo lo storico con consuntivi*/
	data previsioni_g_out_fill;
		set consuntivi_out(where=(data_rif<&start_date)) previsioni_g_out;
	run;

	data previsioni_g1_out_fill;
		set consuntivi_out(where=(data_rif<&start_date)) previsioni_g1_out;
	run;

	data previsioni_g2_out_fill;
		set consuntivi_out(where=(data_rif<&start_date)) previsioni_g2_out;
	run;

	/*================================*/
	/*================================*/
	/*Prepare dataset for fill of the cons with prevg*/
	%meteocelle_preparedataset2(consuntivi_out, previsioni_g_out_fill, 
					+1, 0, meteocelle_cons);

	%meteocelle_fillrename(meteocelle_cons, input_calendario, "cel_c", meteocelle_cons, &start_date);

	/*================================*/
	/*Prepare dataset for fill of the prev g with prev g+1*/
	%meteocelle_preparedataset2(previsioni_g_out_fill, previsioni_g1_out_fill, 
					0, -1, meteocelle_prev);

	%meteocelle_fillrename(meteocelle_prev, input_calendario, "cel_p", meteocelle_prevg, &start_date);
	/*================================*/
	/*Prepare dataset for fill of the prev g+1 with prev g+2*/
 	%meteocelle_preparedataset2(previsioni_g1_out_fill, previsioni_g2_out_fill, 
					-1, -2, meteocelle_prevg1);

	%meteocelle_fillrename(meteocelle_prevg1, input_calendario, "cel_p", meteocelle_prevg1, &start_date);

	proc sort data=meteocelle_cons;
		by datetime_solare area_id;
	run;

	proc sort data=meteocelle_prevg;
		by datetime_solare area_id;
	run;

	proc sort data=meteocelle_prevg1;
		by datetime_solare area_id;
	run;

	data output_final_prev_g(Rename=(date=data_solare_calc)
				where=(datetime_solare >=dhms(&start_date, 6,0,0) ));
		length  datetime_solare hour data_solare_rif data_gas_rif 8. modello $3.;
		merge meteocelle_cons(in=q 	drop=datetime_calc data: ) 
			meteocelle_prevg(in=p drop=datetime_calc data: );
		by datetime_solare area_id;
		data_solare_rif=datepart(datetime_solare);
		hour=hour(datetime_solare);
		data_gas_rif=ifn(hour<6, data_solare_rif-1, data_solare_rif);
		modello = "G";
		key_modello=0;
		if q and p;
		format data: date9.;
	run;

	data output_final_prev_g1(Rename=(date=data_solare_calc)
				where=(datetime_solare >=dhms(&start_date, 6,0,0) ));
		length  datetime_solare hour data_solare_rif data_gas_rif 8.  modello $3.;
		merge meteocelle_cons(in=q 	drop=datetime_calc data: ) 
			meteocelle_prevg1(in=p drop=datetime_calc data: );
		by datetime_solare area_id;
		hour=hour(datetime_solare);
		/* DATA DI RIFERIMENTO PER LA PREVISIONE: datetime_solare +1 giorno */
		data_solare_rif=intnx("day",datepart(datetime_solare),1);
		data_gas_rif=ifn(hour<6, data_solare_rif-1, data_solare_rif);
		modello = "G1";
		key_modello=1;
		if q and p;
		format data: date9.;
	run;

	data &output_final;
		set output_final_prev_g output_final_prev_g1;
	run;

	proc sort data=&output_final;
		by area_id;
	run;

	proc sort data=&grouping_all.(keep=area:) nodupkey out=area_anagrafica;
		by area_id;
	run;

	data &output_final;
		merge &output_final(in=p) area_anagrafica;
		by area_id;
		if p;
	run;


	proc sort data=&output_final;
		by datetime_solare area_id modello;
	run;

%mend meteocelle;




%macro drop_tables(input_lib, pfx);

	data _null_;
		pfx_mod = substr("&pfx",1,length(compress("&pfx"))-1);
		call symput("pfx_mod",lowcase(pfx_mod));
	run;

	proc sql noprint;
		select distinct memname into :drop_table_list separated by ", "
		from dictionary.columns
		where lowcase(libname)=lowcase("&input_lib") and 
			  scan(lowcase(memname),1,"_") in ("&pfx_mod");
	quit;

	proc sql noprint;
		drop table &drop_table_list
		;
	quit;

%mend drop_tables;

%macro meteocitta_process_grouping(group);

/*%let group=gg1;*/
/*%let group=hp1_c_tmax;*/

	data _null_;
		gruppo = "&group";
		if scan(gruppo,1,"_") in ("city", "meteo") then type="s";
		else if substr(gruppo,1,2) in ("hp", "vc") then type="a";
		else type="g";
		call symput("type",compress(type));
	run;

/*	%put "&type";*/

	/* Se di tipo city / meteo --> traspongo tutte le statistiche meteo
		della citt? appartenente al gruppo */
	%if "&type" = "s" %then %do;

		proc sort data=&pfx.input_clusters (where=(compress(group)="&group"))
			out=&pfx.input_clusters_run (drop=tx_city group);
			by data_gas_rif area_id;
		run;

		/* Rename delle colonne */
		proc contents data=&pfx.input_clusters_run(keep=t:) out=&pfx.cont(keep=name) noprint;
		run;

		proc sql noprint;
			select distinct compress(cat(name ,"=",name,"_","&group")) into: rename_list
					separated by " "
			from &pfx.cont
			;
		quit;

		data &pfx.input_clusters_run;
			set &pfx.input_clusters_run;
			rename &rename_list;
		run;

		proc sort data=&pfx.input_clusters_run out=&pfx.stat_all_group;
			by data_gas_rif area_id;
		run;

	%end;

	/* se ? di tipo varclus / hpreduce prendo la statistica meteo relativa */
	%else %if "&type" = "a" %then %do;

		/* Tengo solo la statistica che devo trasporre */
		data _null_;
			group="&group";
			stat=scan(group,3,"_");
			call symput("stat",compress(stat));
		run;

		data &pfx.stat_all_group (keep=data_gas_rif area_id &group);
			set &pfx.input_clusters (where=(compress(group)="&group"));
			rename &stat = &group;
		run;

		proc sort data=&pfx.stat_all_group;
			by data_gas_rif area_id;
		run;

	%end;

	/* Altrimenti faccio le statistiche */
	%else %do;

		/* Statistiche: max(tmax), min(tmin), std(tmed) */
			data &pfx.input_clusters_run (drop= group);
				set &pfx.input_clusters (where=(compress(group)="&group"));
			run;

			proc sql;
				create table &pfx.cluster_stat1 as 
				select distinct data_gas_rif
					 , area_id
					 , max(tmax) as tmax_max
					 , min(tmin) as tmin_min
					 , std(tmed) as tmed_std
				from &pfx.input_clusters_run
				group by 1,2
				order by 1,2
				;
			quit;		

			/* Statistica: mean(tmax, tmin, tmed) */
			proc sort data=&pfx.input_clusters_run out=&pfx.input_clusters_run;
				by tx_city data_gas_rif area_id;
			run;

			proc transpose data=&pfx.input_clusters_run out=&pfx.input_clusters_run_t(rename=(col1=value));
				by tx_city data_gas_rif area_id;
				var tmax tmed tmin;
			run;

			proc sql;
				create table &pfx.cluster_stat2 as 
				select distinct data_gas_rif
					  , area_id
					  , mean(value) as tall_med
				from &pfx.input_clusters_run_t
				group by 1,2
				order by 1,2
				;
			quit;

			/* Unisco tutte le statistiche */
			data &pfx.stat_all_group;
				merge &pfx.cluster_stat1 &pfx.cluster_stat2;
				by data_gas_rif area_id;
			run;

			/* Rename delle colonne */
			proc contents data=&pfx.stat_all_group(keep=t:) out=&pfx.cont(keep=name) noprint;
			run;

			proc sql noprint;
				select distinct compress(cat(name ,"=",name,"_","&group")) into: rename_list
						separated by " "
				from &pfx.cont
				;
			quit;

			data &pfx.stat_all_group;
				set &pfx.stat_all_group;
				rename &rename_list;
			run;

		%end;

		/* Aggiungo al ds di output finale */
		data &pfx.output_cluster;
			merge &pfx.output_cluster(in=p) &pfx.stat_all_group;
			by data_gas_rif area_id;
		run;

%mend meteocitta_process_grouping;

%macro meteocitta_fillrename(input_dset, input_calendario, prefix, output_dset_fill, start_cal);

/*		%let input_dset=meteo_cons;*/
/*		%let prefix="urb_c";*/
/*		%let output_dset_fill= output_cons;*/
/*		%let start_cal=&start_date_adj;*/

	proc sort data=&input_dset out=temp;
		by area_id datetime_solare;
	run;

	proc sort data=&input_calendario;
		by area_id datetime_solare;
	run;

	data temp;
		merge temp &input_calendario(in=p where=(datepart(datetime_solare)>=&start_cal) 
								keep=datetime_solare area:);
		by area_id datetime_solare;
		if p;
	run;

	/*--*/
	proc sql noprint;
	%let droplist=;
		select compress(cat(&prefix, "_" , name))
			into :droplist separated by ' '
		from dictionary.columns
		where libname="WORK" and lowcase(memname)="temp"
						and (name like "tm%" or name like "tall%"
							or name like "hp%" or name like "vc%");
	quit;

	proc sql noprint;
	%let savelist=;
		select name
			into :savelist separated by ' '
		from dictionary.columns
		where libname="WORK" and memname="TEMP"
						and (name like "tm%" or name like "tall%"
						or name like "hp%" or name like "vc%");
	quit;

	data &output_dset_fill(drop=a b i j &savelist);
		set	TEMP;
		by area_id;
		array new &droplist;
		array old &savelist;
		retain new;
		a=dim(new);
		b=dim(old);

		if _N_=1 or first.area_label then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;
	run;

	proc sql;
		drop table temp;
	quit;

%mend meteocitta_fillrename;

%macro meteocitta_process(input_dset=, input_grouping_citta=, key=, horizon_prev=);


/*%let input_dset=&input_dset;*/
/*%let input_grouping_citta = &input_grouping_citta;*/
/*%let key = cons;*/
/*%let horizon_prev = ;*/

	data &pfx.meteo_&key._citta (drop= id_tipo_dato horizon_day);
		set &input_dset;
		if "&key"="prev" then do;
			horizon_day = input(substr("&horizon_prev",2,1),best32.);
			if distanza_insert_rif = horizon_day;
		end;
	run;

	/* Prendo ultimo consuntivo/previsione disponibile */
	proc sort data=&pfx.meteo_&key._citta;
		by tx_city data_gas_rif dt_creazione;
	run;

	data &pfx.meteo_&key._citta(drop= dt: distanza_insert_rif);
		set &pfx.meteo_&key._citta;
		by tx_city data_gas_rif;
		if last.data_gas_rif;
	run;

	/* Creo le statistiche / traspongo 7 citt? pi? rapresentative, per area geografica e data_gas_rif */

	/* Aggiungo l'area geografica e i grouping_citta meteo (escludo le citt? che appartengono a nessun gruppo) */
	proc sql;
		create table &pfx.input_clusters (drop = data_calc 
							where=(area_id ne .) ) as 
		select a.*
			 ,b.area_id
			 ,b.group
		from &pfx.meteo_&key._citta as a 
		left join &input_grouping_citta as b
		on a.tx_city = b.tx_city
		;
	quit;

	/* Creo tab di output (output_cluster) con solo data_gas_rif e area_id alla quale aggiunger? 
	   a mano a mano le statistiche per gruppo */
	proc sort data=&pfx.input_clusters (keep=data_gas_rif area:) out= &pfx.output_cluster nodupkey;
		by data_gas_rif area_id;
	run;

	/* Gruppi Distinti (Escludo i gruppi "city" che li tratto poi
		singolarmente trasponendo il meteo della citt? corrispondente) */
	proc sql;
		create table &pfx.dist_group as 
		select distinct group
		from &input_grouping_citta
		;
	quit;

	data _null_;
		set &pfx.dist_group;
		call execute ('%NRSTR(%meteocitta_process_grouping(group='||group||'))');
	run;
		
	data &pfx.meteo_&key._citta_&horizon_prev. (drop=horizon_day);
		retain data_gas_rif data_calc;
		set &pfx.output_cluster;
		if "&key"="cons" then data_calc=intnx("day",data_gas_rif,1);
		else if "&key"="prev" then do;
			horizon_day = input(substr("&horizon_prev",2,1),best32.);
			data_calc=intnx("day",data_gas_rif,-horizon_day);
		end;
		format data_calc date9.;
	run;


%mend meteocitta_process;

%macro meteocitta_arrivodati(ds_have=, ds_nothave=, ds_output=);

/* H10 ho refresh dati meteo citt? --> posso usare i consuntivi riferiti a ieri, le previsioni fatte oggi per oggi,
	le previsioni fatte oggi per domani */
	data &pfx.meteo_citta_1;
		retain data_gas_rif datetime_solare datetime_calc data_calc;
		set &ds_have;
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(data_calc,10,0,0);
		format datetime: datetime19.;
	run;

/* Nelle prime ore del giorno gas (dalle H06 alle H10) non ho queste info:
	imputo i consuntivi con le previsioni fatte ieri per ieri
	imputo le previsioni fatte oggi per oggi con le previsioni fatte ieri per oggi
	imputo le previsioni fatte oggi per domani con le previsioni fatte ieri per domani */

	data &pfx.meteo_citta_2;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set &ds_nothave;
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(intnx("day",data_gas_rif,0),6,0,0);
		format datetime_solare datetime_calc datetime19.;
	run;

	data &ds_output;
		set &pfx.meteo_citta_1 &pfx.meteo_citta_2;
	run;

	proc sort data=&ds_output;
		by data_gas_rif datetime_solare;
	run;

%mend meteocitta_arrivodati;


%macro meteocitta(g_gas=, g1_solare=, shift_type=, shift_value=,
					input_dset=, input_grouping_citta=, input_calendario_dates=, 
					anag_meteo_citta=, pfx=, output_dset=);

/*%let g_gas=&g_gas;*/
/*%let g1_solare=&g1_solare;*/
/*%let shift_type=month;*/
/*%let shift_value=2;*/
/*%let input_calendario_dates=&input_calendario_dates;*/
/*%let anag_meteo_citta=&anag_meteo_citta;*/
/*%let input_dset = oralpbp2.d_e_meteo_calc;*/
/*%let input_grouping_citta = for_svil.grouping_meteo_citta_areageo;*/
/*%let pfx=mci_;*/
/*%let output_dset=output_citta;*/

	%set_startdates_calendar(g_gas=&g_gas, g1_solare=&g1_solare, shift_type=&shift_type, shift_value=&shift_value, 
							out_calendar=&input_calendario_dates);

	/* Add Areas to calendar dates */
	proc sql;
		create table aree_dist as 
		select distinct area_id
			   , area_label
			   , area_descrizione
		from &anag_meteo_celle (where=(fl_deleted =0))
		order by area_id
		;
	quit;

	proc sql;
		create table input_calendario as 
		select a.*
			  ,b.*
		from &input_calendario_dates as a 
		cross join aree_dist as b
		;
	quit;


	/*-----------------------------------------------------------------------------------*/
	/* Elaboro consuntivi, previsioni g, g1, g2 
	   OUTPUT TABLES: 
	   meteo_cons_citta_, meteo_prev_citta_g0, meteo_prev_citta_g1 e meteo_prev_citta_g2 */
	/*-----------------------------------------------------------------------------------*/

	data &pfx.consuntivi_meteo;
		set &input_dset(where=(datepart(dt_data_elaborazione)>=&start_date and 
							   lowcase(id_tipo_dato)="cons")
						rename=(nr_temp_max=tmax nr_temp_min=tmin nr_temp_med=tmed)
						drop=k_d_e_meteo_calc);

		distanza_insert_rif=datepart(dt_data_riferimento)-datepart(dt_data_elaborazione);
		data_gas_rif=datepart(dt_data_riferimento);
		data_calc=datepart(dt_data_elaborazione);
		if tx_city="Forli'" then tx_city="Forli";
		if tx_city="L'Aquila" then tx_city="Aquila";
		tx_city=compress(tx_city);
		format data: date9.;
	run;

	data &pfx.previsioni_meteo;
		set &input_dset(where=(datepart(dt_data_elaborazione)>=&start_date and 
							   lowcase(id_tipo_dato)="prev")
						rename=(nr_temp_max=tmax nr_temp_min=tmin nr_temp_med=tmed)
						drop=k_d_e_meteo_calc);

		distanza_insert_rif=datepart(dt_data_riferimento)-datepart(dt_data_elaborazione);
		data_gas_rif=datepart(dt_data_riferimento);
		data_calc=datepart(dt_data_elaborazione);
		if tx_city="Forli'" then tx_city="Forli";
		if tx_city="L'Aquila" then tx_city="Aquila";
		tx_city=compress(tx_city);
		format data: date9.;
	run;

	%meteocitta_process(input_dset=&pfx.consuntivi_meteo, input_grouping_citta=&input_grouping_citta, 
						key=cons, horizon_prev=);
	%meteocitta_process(input_dset=&pfx.previsioni_meteo, input_grouping_citta=&input_grouping_citta,
						key=prev, horizon_prev=g0);
	%meteocitta_process(input_dset=&pfx.previsioni_meteo, input_grouping_citta=&input_grouping_citta,
						key=prev, horizon_prev=g1);
	%meteocitta_process(input_dset=&pfx.previsioni_meteo, input_grouping_citta=&input_grouping_citta,
						key=prev, horizon_prev=g2);

	/*---------------------------------------------------------*/
	/* Gestisco i casi in cui il dato non mi ? ancora arrivato */
	/*---------------------------------------------------------*/

	/* Consuntivi (sia per g che per g+1), previsioni per g e previsioni per il g+1 */
	%meteocitta_arrivodati(ds_have=&pfx.meteo_cons_citta_, ds_nothave=&pfx.meteo_prev_citta_g0, ds_output=&pfx.meteo_cons);
	%meteocitta_arrivodati(ds_have=&pfx.meteo_prev_citta_g0, ds_nothave=&pfx.meteo_prev_citta_g1, ds_output=&pfx.meteo_prev_g);
	%meteocitta_arrivodati(ds_have=&pfx.meteo_prev_citta_g1, ds_nothave=&pfx.meteo_prev_citta_g2, ds_output=&pfx.meteo_prev_g1);


	/*----------------*/
	/* FFILL e RENAME */
	/*----------------*/
	%meteocitta_fillrename(&pfx.meteo_cons, input_calendario, "urb_c", &pfx.output_cons, &start_date);
	%meteocitta_fillrename(&pfx.meteo_prev_g, input_calendario, "urb_p", &pfx.output_prev_g, &start_date);
	%meteocitta_fillrename(&pfx.meteo_prev_g1, input_calendario, "urb_p", &pfx.output_prev_g1, &start_date);


	/*--------------*/
	/* MERGE FINALI */
	/*--------------*/
	/* Merge finale (consuntivi + Previsioni) per il MODELLO G */
	data &pfx.output_dset_g (rename=(date=data_solare_calc) 
		where=(datetime_solare >= dhms(&start_date, 6,0,0)));
		length datetime_solare hour data_solare_rif data_gas_rif 8. modello $2;
		merge &pfx.output_cons(in=q 	drop=datetime_calc data:) 
			&pfx.output_prev_g(in=p  drop=datetime_calc data:);
		by area_id datetime_solare;
		hour=hour(datetime_solare);
		data_solare_rif=datepart(datetime_solare);
		data_gas_rif=ifn(hour<6, data_solare_rif-1, data_solare_rif);
		modello="G";
		key_modello=0;
		if q and p;
		format data: date9.;
	run;

	/* Merge finale (consuntivi + Previsioni) per il MODELLO G+1 */
	data &pfx.output_dset_g1 (rename=(date=data_solare_calc) 
		where=(datetime_solare >= dhms(&start_date, 6,0,0)))
		;
		length datetime_solare hour data_solare_rif data_gas_rif 8. modello $2;
		merge &pfx.output_cons(in=q drop=datetime_calc data:) 
			&pfx.output_prev_g1(in=p  drop=datetime_calc data:);
		by area_id datetime_solare;
		hour=hour(datetime_solare);
		/* DATA DI RIFERIMENTO PER LA PREVISIONE: datetime_solare +1 giorno */
		data_solare_rif=intnx("day",datepart(datetime_solare),1);
		data_gas_rif=ifn(hour<6, data_solare_rif-1, data_solare_rif);
		modello="G1";
		key_modello=1;
		if q and p;
		format data: date9.;
	run;

	/* OUTPUT FINALE: SET DEI DUE OUTPUT */
	data &output_dset;
		set &pfx.output_dset_g &pfx.output_dset_g1;
	run;

	proc sort data=&output_dset;
		by datetime_solare modello area_id;
	run;

	/*--------------------------------------------*/
	/* ELIMINO TABELLE INTERMEDIE CHE NON SERVONO */
	/*--------------------------------------------*/

	%drop_tables(work, &pfx);

%mend meteocitta;



%macro terna(g_gas=, g1_solare=, shift_type=, shift_value=, 
			input_calendario_dates=, input_terna=, input_up=,
			input_remi_termo=, output_consuntivo_terna_aree=, output_previsione_terna_aree=);

/*%let shift_type=month;*/
/*%let shift_value=2;*/

	%set_startdates_calendar(g_gas=&g_gas, g1_solare=&g1_solare, shift_type=&shift_type, shift_value=&shift_value, 
							out_calendar=&input_calendario_dates);

	data calendario_orario;
		set &input_calendario_dates;
		rename datetime_solare=dt_calcolo data_gas_rif=giorno_gas;
	run;

	data calendario_orario_previsioni;
		set &input_calendario_dates;
		rename datetime_solare=dt_calcolo data_gas_rif=data_gas;
	run;

	data modelli;
		modello="G1";
		key_modello=1;output;
		modello="G";
		key_modello=0;output;
	run;

	proc sql;
		create table calendario_orario_previsioni as 
		select *
		from calendario_orario_previsioni
		cross join modelli
		;
	quit;

	data calendario_orario_previsioni;
		set calendario_orario_previsioni;
		if key_modello=1 then data_gas_rif=intnx("day",data_gas,1);
		else data_gas_rif=data_gas;
		datetime_riferimento_dati=dhms(data_gas_rif,0,0,0);
		format data_gas_rif date9. datetime_riferimento_dati datetime19.;
	run;


	data trn_prev(keep= hour_rif data_rif dt_rif dt_inserimento_val 
		energia_programmata k_d_e_up
		where=(data_rif>=&start_date)) 
		trn_cons(keep= hour_rif data_rif dt_rif dt_inserimento_val energia_programmata 
		k_d_e_up);
		set &input_terna(where=(DT_DATA_RIFERIMENTO>="&start_datetime_char"dt and 
			K_C_E_SESSIONE in (0 5)	));
		format data_rif DATE9.;
		dt_inserimento_val=intnx("dthour",dt_data_elaborazione,1);
		dt_rif=intnx("dthour",DT_DATA_RIFERIMENTO,0);
		hour_rif = hour(dt_rif);
		data_rif = datepart(DT_DATA_RIFERIMENTO);
		format dt_inserimento_val dt_rif datetime19.;

		if K_C_E_SESSIONE =5 then
			output trn_cons;
		else if K_C_E_SESSIONE =0 then
			output trn_prev;
	run;

	%if &flg_anomalie=1 %then
		%do;
			%put ===== Controllo Anomalie Dati Terna =====;

			%gestione_anomalie_terna(dt_base=&dt_solare,ds_in1=trn_prev, ds_in2=trn_cons, 
				ds_out1=trn_prev, ds_out2=trn_cons,
				storico_anomalie_terna=&output_anomalie_terna);
		%end;

	/*-------- CONSIDERO CONSUNTIVO trn_cons ---------*/
	/* considero il consuntivo */
	proc sql noprint;
		create table trn_cons_up_remi as
			select t1.*, t2.REMI from trn_cons t1 inner join
			(select distinct K_D_E_UP, REMI from &input_up where REMI in 
			(select REMI from &input_remi_termo)) t2
				on t1.K_D_E_UP = t2.K_D_E_UP
			order by remi, K_D_E_UP, dt_rif, dt_inserimento_val;
	quit;

	data trn_cons_up_remi;
		set trn_cons_up_remi;
		by remi K_D_E_UP dt_rif;

		if last.dt_rif then
			output;
	run;

	/* DATI ARRIVANO ALLE 12 O ALLE 13 DEL GIORNO G 
	RIFERITI ALLE VARIE ORE E VARIE UP DEL GIORNO G-1 (giorno
	solare e non giorno gas!) */

	/*consuntivo orario per REMI*/
	/*problemi con remi-> (remi="30314101" or remi="50034301") and consuntivo>1000000 nel 2012*/
	proc sql noprint;
		create table trn_cons_remi as select
			dt_rif, dt_inserimento_val,data_rif, hour_rif, 
			REMI, sum(energia_programmata) as CONSUNTIVO
		from trn_cons_up_remi 
			group by dt_rif, dt_inserimento_val,data_rif, hour_rif, REMI;
	quit;

	data trn_cons_remi_mod;
		set trn_cons_remi;

		if (remi eq "31307801" or remi eq "50034301" or remi eq "30314101") and data_rif<"01JAN2013"d then
			do;
				if consuntivo >=2000 and consuntivo <=10000 then
					consuntivo=consuntivo/10;
				else if consuntivo >10000 and consuntivo <=100000 then
					consuntivo=consuntivo/100;
				else if consuntivo >10000 and consuntivo <=100000 then
					consuntivo=consuntivo/100;
				else if consuntivo >100000 and consuntivo <=1000000 then
					consuntivo=consuntivo/1000;
				else if consuntivo >1000000 and consuntivo <=10000000 then
					consuntivo=consuntivo/10000;
				else if consuntivo >10000000 and consuntivo <=100000000 then
					consuntivo=consuntivo/100000;
				else if consuntivo >100000000 and consuntivo <=1000000000 then
					consuntivo=consuntivo/1000000;
				else if consuntivo >1000000000 and consuntivo <=10000000000 then
					consuntivo=consuntivo/10000000;
				else if consuntivo >10000000000 and consuntivo <=100000000000 then
					consuntivo=consuntivo/100000000;
				else if consuntivo >100000000000 and consuntivo <=1000000000000 then
					consuntivo=consuntivo/1000000000;
				else if consuntivo >1000000000000 and consuntivo <=10000000000000 then
					consuntivo=consuntivo/10000000000;
				else if consuntivo >10000000000000 and consuntivo <=100000000000000 then
					consuntivo=consuntivo/100000000000;
			end;
	run;

	
	/* mod-1 - inserimento anagrafica aree e join con consuntivi*/

	data anag_remi_terna_aree;
		set &anag_remi_terna_aree;
		keep remi area:;
		remi = compress(put(id_remi, $20.));
	run;

	proc sort data=anag_remi_terna_aree nodupkey dupout=dup_remi;
		by remi;
	run;

	proc sort data=trn_cons_remi_mod;
		by remi;
	run;

	data trn_cons_up_remi_aree;
		merge trn_cons_remi_mod(in=L) anag_remi_terna_aree(in=R);
		by remi;
		if L and R;
	run;

	/* End mod-1*/

	/* mod-2 inserisco dimensione area_id e conseguente area_label in tutti i successivi 
		step sui consuntivi */

	proc sql noprint;
		create table trn_cons_orario_aree as select
			data_rif, hour_rif, area_id, area_label, sum(CONSUNTIVO) as CONSUNTIVO
		from trn_cons_up_remi_aree 
			group by data_rif, hour_rif, area_id, area_label
				order by data_rif, hour_rif, area_id, area_label;
	quit;

	/*-----*/

	/* faccio ffill per hour di consuntivo_orario, quindi per ogni ora utilizzo
	il valore del giorno prima nella stessa ora*/

	/*&g_gas*/
	PROC SQL;
		select  max(t1.data_rif), min(t1.data_rif) into :max_d_terna, :min_d_terna
			FROM WORK.trn_cons_orario_aree t1;
	QUIT;

	/**/
	data cons_list_calendar;
		data_rif=&min_d_terna;

		do while (data_rif<=&max_d_terna);
			do hour_rif=0 to 23;
				output;
			end;

			data_rif=intnx('day', data_rif, 1);
		end;

		format data_rif date9.;
	run;

	proc sql noprint;
		create table cross_join_cons as
		select * from ((select distinct area_id, area_label from anag_remi_terna_aree) 
			cross join cons_list_calendar)
		order by data_rif, hour_rif, Area_ID;
	quit;

	proc sort data=trn_cons_orario_aree out=trn_cons_orario_aree;
		by data_rif hour_rif Area_ID;
	run;

	data trn_cons_orario_nofill_aree;
		merge trn_cons_orario_aree cross_join_cons;
		by data_rif hour_rif Area_ID;
	run;

	proc sort data=trn_cons_orario_nofill_aree out=trn_cons_orario_nofill_aree;
		by Area_ID hour_rif data_rif;
	run;

	data trn_cons_orario_fill_aree (drop= consuntivo_temp);
		set	trn_cons_orario_nofill_aree;
		by area_id hour_rif data_rif ;
		retain consuntivo_temp;

		if first.hour_rif then
			consuntivo_temp=consuntivo;
		else
			do;
				if consuntivo ne . then
					consuntivo_temp=consuntivo;
			end;

		consuntivo=consuntivo_temp;
	run;

	proc sort data=trn_cons_orario_fill_aree out=trn_cons_orario_fill_aree;
		by Area_ID data_rif hour_rif;
	run;

	data trn_cons_orario_fill_aree(where=(datepart(dt_inserimento_val)>="01JAN2012"d));
		retain data_rif hour dt_inserimento_val data_inserimento_val;
		set trn_cons_orario_fill_aree;
		dt_inserimento_val=dhms(intnx("day",data_rif,1),13,0,0);
		data_inserimento_val=datepart(dhms(intnx("day",data_rif,1),13,0,0));
		format dt_inserimento_val datetime.;
		format data_inserimento_val date9.;
	run;

	/*Ora ho i valori di consuntivo anche sulle ore precedenti alle 13
	in realta quel dato manca prima delle 13! imputo con i valori orari 
	del giorno prima
	****potevo utilizzare 7g fa */
	data trn_cons_orario_yday_aree(keep=data_rif hour_rif CONSUNTIVO Area_ID
		Area_Label);
		set trn_cons_orario_fill_aree;
		fake_data_rif=intnx("DAY",data_rif,1);
		format fake_data: date9.;
		drop data_rif;
		rename fake_data_rif=data_rif;
	run;

	/*consuntivo_post=consuntivo della data di riferimento, ma ho i valori dopo le 13*/
	/*consuntivo_yday=consuntivo del giorno precedente alla data di riferimento che uso */
	/*	per imputare le prime ore della data di riferimento finch? arriva il consuntivo corretto*/
	/*consuntivo_mix=per la data di riferimento usa i valori orari del giorno prima finch?*/
	/*	arriva il consuntivo alle ore 13*/
	data trn_cons_orario_imp_aree(drop=dt_inserimento_val data_inserimento_val);
		merge trn_cons_orario_fill_aree(in=uno rename=(consuntivo=consuntivo_post) ) 
			trn_cons_orario_yday_aree(in=due rename=(consuntivo=consuntivo_yday) );
		by Area_ID data_rif hour_rif;
		consuntivo_mix=ifn(hour_rif<13,consuntivo_yday,consuntivo_post);

		if consuntivo_mix eq . then
			consuntivo_mix=consuntivo_yday;

		if dt_inserimento_val eq . then
			dt_inserimento_val=dhms(data_rif,13,0,0);

		if hour_rif lt 6 then
			giorno_gas_rif=data_rif-1;
		else giorno_gas_rif=data_rif;
		dt_riferimento=dhms(data_rif,hour_rif,0,0);
		format giorno_gas_rif date9. dt_riferimento datetime19.;
	run;

	/* cumulati devono rispettare queste logiche
	creo la stima usando i consuntivi di ieri fino alle 13 poi calcolo valore reale */
	data trn_cons_orario_cum_aree;
		set trn_cons_orario_imp_aree;
		by Area_ID data_rif;
		retain cons_day_cum_pre cons_day_cum_post;

		if first.data_rif and consuntivo_mix ne . then
			do;
				cons_day_cum_pre=0;
				cons_day_cum_post=0;
			end;
		else if consuntivo_mix eq . or cons_day_cum_pre eq . then
			do;
				cons_day_cum_pre=.;
				cons_day_cum_post=.;
			end;

		cons_day_cum_pre+consuntivo_mix;
		cons_day_cum_post+consuntivo_post;

		if 0<=hour_rif<=12 or consuntivo_post eq . then
			cons_day_cum=cons_day_cum_pre;
		else cons_day_cum=cons_day_cum_post;
		drop consuntivo_post consuntivo_yday consuntivo_mix
			cons_day_cum_pre;
	run;

	data trn_cons_orario_gcum_aree;
		set trn_cons_orario_imp_aree;
		by Area_ID giorno_gas_rif;
		retain cons_gday_cum_pre cons_gday_cum_post;

		if first.giorno_gas_rif and consuntivo_mix ne . then
			do;
				cons_gday_cum_pre=0;
				cons_gday_cum_post=0;
			end;
		else if consuntivo_mix eq . or cons_gday_cum_pre eq . then
			do;
				cons_gday_cum_pre=.;
				cons_gday_cum_post=.;
			end;

		cons_gday_cum_pre+consuntivo_mix;
		cons_gday_cum_post+consuntivo_post;

		if 0<=hour_rif<=12 or consuntivo_post eq . then
			cons_gday_cum=cons_gday_cum_pre;
		else cons_gday_cum=cons_gday_cum_post;
		drop consuntivo_post consuntivo_yday consuntivo_mix
			cons_gday_cum_pre;
	run;

	data trn_cons_orario_tmp_aree(drop= giorno_gas_rif data_rif hour_rif  rename= (
		date=giorno_solare ) );
		merge trn_cons_orario_imp_aree(rename=consuntivo_mix=cons
			drop=consuntivo_yday)
			trn_cons_orario_cum_aree
			trn_cons_orario_gcum_aree;
		by Area_ID dt_riferimento;
		dt_calcolo=intnx("dthour",dt_riferimento,+24);
		format dt: datetime19.;
	run;

	proc sort data=trn_cons_orario_tmp_aree;
		by Area_ID dt_calcolo;
	run;

	/* mod3 - inserisco una cross join anche per il calendario orario
	complessivo, di modo da evitare possibili missing sulle aree.
	contestualmente, creo anche una cross join per il calendario previsioni,
	che utilizzer? dopo con le previsioni */

	/* cross join calendario */

	proc sql noprint;
		create table cross_join_calendario as
		select * from ((select distinct area_id, area_label from anag_remi_terna_aree) 
			cross join calendario_orario)
		order by Area_ID, dt_calcolo;
	quit;

	/* cross join calendario previsioni */

	proc sql noprint;
		create table cross_join_calendario_previsioni as
		select * from ((select distinct area_id, area_label from anag_remi_terna_aree) 
			cross join calendario_orario_previsioni)
		order by Area_ID, dt_calcolo, key_modello;
	quit;

	data trn_cons_orario_aree(drop= hour_rif 
/*		consuntivo_post cons_gday_cum_post cons_day_cum_post  */
		rename=(date=giorno_solare));
		merge cross_join_calendario(in=p)
			trn_cons_orario_tmp_aree;
		by Area_ID dt_calcolo;

		if p;
		format dt: datetime19.;
	run;

	proc sql noprint;
		%let savelist=;
		select compress(cat("terna_", name))
			into :savelist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="TRN_CONS_ORARIO_AREE"
						and name like "cons%";
	quit;

	proc sql noprint;
		%let droplist=;
		select name
			into :droplist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="TRN_CONS_ORARIO_AREE"
						and name like "cons%";
	quit;

	proc sort data=TRN_CONS_ORARIO_AREE out=TRN_CONS_ORARIO_AREE_SORTED;
		by Area_ID hour giorno_solare;
	run;

	data trn_cons_orario_final_aree(drop=a b i j &droplist);
		set	TRN_CONS_ORARIO_AREE_SORTED;
		array new &savelist;
		array old &droplist;
		by Area_ID hour;
		retain new;
		a=dim(new);
		b=dim(old);

		if first.hour then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;
	run;

	proc sort data=trn_cons_orario_final_aree;
		by dt_calcolo;
	run;

	/*	mod4 - i consuntivi sono identici per ciascuna previsione, 
	cambia solo l'orizzonte temporale di riferimento della previsione stessa;
	devo creare un record per il G e uno per il G1*/

	data trn_cons_orario_final_aree_g trn_cons_orario_final_areee_g1;
		length modello $ 8;
		set trn_cons_orario_final_aree;
		where giorno_gas >= "03JAN2012"d;
		drop trn_consUNTIVO_yday data_rif dt_inserimento_val;
		rename date=giorno_solare;
		do key_modello = 0 to 1;
			if key_modello = 0 then 
				do;
					data_gas_rif= intnx("day", giorno_gas, key_modello);
					datetime_riferimento_dati = dhms(data_gas_rif, 0,0,0);
					modello = "G";
					format datetime_riferimento_dati datetime19. 
					data_gas_rif date9.;
					output trn_cons_orario_final_aree_g;
				end;
			else if key_modello = 1 then 
				do;
					data_gas_rif = intnx("day", giorno_gas, key_modello);
					datetime_riferimento_dati = dhms(data_gas_rif, 0,0,0);
					modello = "G1";
					format datetime_riferimento_dati datetime19. 
					data_gas_rif date9.;
					output trn_cons_orario_final_areee_g1;
				end;
		end;
	run;

	
	data input_up_cons_ora_termo_aree;
		set trn_cons_orario_final_aree_g trn_cons_orario_final_areee_g1;
	run;

	proc sort data=input_up_cons_ora_termo_aree;
		by area_id dt_calcolo modello;
	run;

	/* primo output della funzione input_up_consuntivo_ora_termo */
	/*-----------------------------------*/
	/*CONSUNTIVO GIORNALIERO*/
	proc sql noprint;
		create table trn_cons_giorno_aree as select
			area_id, area_label, data_rif, sum(CONSUNTIVO_POST) as CONSUNTIVO,
			sum(CONSUNTIVO_yday) as CONSUNTIVO_yday,
			dhms(intnx("day",data_rif,1),13,0,0) format=datetime19. as dt_inserimento_val
		from trn_cons_orario_imp_aree 
			group by data_rif, area_id, area_label
				order by area_id, data_rif;
	quit;

	data trn_cons_giorno_aree;
		set trn_cons_giorno_aree;

		if _N_=1 then
			dt_inserimento_val=dhms(intnx("day",data_rif,2),6,0,0);
	run;

	/*data consuntivo_giorno;*/
	/*	set consuntivo_giorno;*/
	/*	date=datepart(dt_inserimento_val);*/
	/*	data_inserimento_val=datepart(dt_inserimento_val);*/
	/*	hour_inserimento_val=hour(dt_inserimento_val);*/
	/*	dt_calcolo=intnx("dthour",dt_riferimento,+24);*/
	/*	format dt: datetime19.;* date date9.;*/
	/*run;*/

	data trn_cons_giorno_final_aree(where=(giorno_gas<=&g_gas));
		merge cross_join_calendario(in=p) 
			trn_cons_giorno_aree(Rename=(dt_inserimento_val=dt_calcolo));
		by Area_ID dt_calcolo;

		if p;
	run;

	/*ci sono dati ancora da imputare, dove valore mancante in t deve essere uguale a quello precedente
	pi? vicino a livello temporale (quindi t-1, t-2, ecc)*/
	proc sql noprint;
		%let savelist=;
		select compress(cat("terna_", name))
			into :savelist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="TRN_CONS_GIORNO_FINAL_AREE"
						and index(lowcase(name),'cons');
	quit;

	proc sql noprint;
		%let droplist=;
		select name
			into :droplist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="TRN_CONS_GIORNO_FINAL_AREE"
						and index(lowcase(name),'cons');
	quit;

	data trn_cons_giorno_final_fill_aree(drop=i j &droplist);
		set	TRN_CONS_GIORNO_FINAL_AREE;
		array new &savelist;
		array old &droplist;
		retain new;
		by Area_ID;

		if first.Area_ID then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;
	run;

	/*	mod4 - i consuntivi sono identici per ciascuna previsione, 
	cambia solo l'orizzonte temporale di riferimento della previsione stessa;
	devo creare un record per il G e uno per il G1*/

	data trn_cons_giorno_final_aree_g trn_cons_giorno_final_aree_g1;
		length modello $ 8;
		set trn_cons_giorno_final_fill_aree;
		where giorno_gas >= "03JAN2012"d;
		drop trn_consUNTIVO_yday data_rif dt_inserimento_val;
		rename date=giorno_solare;
		do key_modello = 0 to 1;
			if key_modello = 0 then 
				do;
					data_gas_rif= intnx("day", giorno_gas, key_modello);
					datetime_riferimento_dati = dhms(data_gas_rif, 0,0,0);
					modello = "G";
					format datetime_riferimento_dati datetime19. 
					data_gas_rif date9.;
					output trn_cons_giorno_final_aree_g;
				end;
			else if key_modello = 1 then 
				do;
					data_gas_rif = intnx("day", giorno_gas, key_modello);
					datetime_riferimento_dati = dhms(data_gas_rif, 0,0,0);
					modello = "G1";
					format datetime_riferimento_dati datetime19. 
					data_gas_rif date9.;
					output trn_cons_giorno_final_aree_g1;
				end;
		end;
	run;

	data input_up_cons_termo_aree;
		set trn_cons_giorno_final_aree_g trn_cons_giorno_final_aree_g1;
	run;

	proc sort data=input_up_cons_termo_aree;
		by area_id dt_calcolo modello;
	run;

	/* secondo output della funzione input_up_consuntivo_termo */
	/*------------------------------Previsioni TERNA------------------------------------*/

	/* per modello G uso previsione MGP fatta ieri e riferita a oggi
	ho quel valore per tutto il giorno*/

	proc sort data=trn_prev out=trn_mgp_sort;
		by dt_rif dt_inserimento_val k_d_e_up;
	run;

	proc sql noprint;
		create table trn_remi_prev as
			select t1.*, t2.REMI from trn_mgp_sort t1 inner join
			(select distinct K_D_E_UP, REMI from &input_up where REMI in 
			(select REMI from &input_remi_termo)) t2
				on t1.K_D_E_UP = t2.K_D_E_UP;
	quit;

	proc sort data=trn_remi_prev out=trn_remi_mgp_sort;
		by dt_rif remi K_D_E_UP;
	run;

	data trn_remi_mgp_sort1;
		set trn_remi_mgp_sort;
		by dt_rif remi K_D_E_UP dt_inserimento_val;

		if last.dt_inserimento_val then
			output;
	run;

	data trn_remi_mgp_sort1(drop=a);
		set trn_remi_mgp_sort1;
		a=datepart(dt_rif)-datepart(dt_inserimento_val);

		if a ne 1 then
			dt_inserimento_val=dhms(intnx("day", datepart(dt_rif), -1),13,0,0);
	run;

	/*Previsioni orarie per remi*/
	proc sql noprint;
		create table trn_mgp_remi_orarie as select distinct
			dt_rif, dt_inserimento_val,data_rif, hour_rif, REMI, 
			sum(energia_programmata) as prev_mgp
		from trn_remi_mgp_sort1 
			group by dt_rif, dt_inserimento_val,data_rif, hour_rif, REMI
			order by REMI, data_rif, hour_rif;
	quit;

	data trn_mgp_remi_orarie_aree;
		merge trn_mgp_remi_orarie(in=L) anag_remi_terna_aree(in=R);
		by remi;
		if L and R;
	run;

	/*Previsioni orarie*/
	proc sql noprint;
		create table trn_mgp_orario_aree1 as select
			 distinct data_rif, hour_rif, 
			 dt_inserimento_val, area_id, area_label, sum(prev_mgp) as prev_mgp
				from  trn_mgp_remi_orarie_aree  
				group by data_rif, hour_rif, area_id, area_label
				order by data_rif, hour_rif, area_id, area_label;
	quit;

	/* faccio ffill per hour di consuntivo_orario, quindi per ogni ora utilizzo
	il valore del giorno prima nella stessa ora*/
	PROC SQL noprint;
		select max(t1.data_rif), min(t1.data_rif) into :max_p_terna, :min_p_terna
			FROM WORK.trn_mgp_orario_aree1 t1;
	QUIT;

	data prev_list_calendar;
		data_rif=&min_p_terna;

		do while (data_rif<=&max_p_terna);
			do hour_rif=0 to 23;
				dt_inserimento_val_imp=dhms(intnx("day", data_rif, -1),13,0,0);
				output;
			end;

			data_rif=intnx('day', data_rif, 1);
		end;

		format data_rif date9.;
		format dt_inserimento_val_imp datetime19.;
	run;

	proc sql noprint;
		create table cross_join_prev as
		select * from ((select distinct area_id, area_label from anag_remi_terna_aree) 
			cross join prev_list_calendar)
		order by data_rif, hour_rif, Area_ID;
	quit;


/*	proc sort data=mgp_orario out=mgp_orario;*/
/*		by data_rif hour_rif;*/
/*	run;*/

	data trn_prev_base_nofill_aree(drop=dt_inserimento_val_imp);
		merge trn_mgp_orario_aree1 cross_join_prev;
		by  data_rif hour_rif;

		if dt_inserimento_val = . then
			dt_inserimento_val=dt_inserimento_val_imp;
	run;

	proc sort data=trn_prev_base_nofill_aree out=trn_prev_base_nofill_aree;
		by Area_ID hour_rif data_rif;
	run;

	data trn_prev_base_fill_aree (drop= prev_mgp_temp);
		set	trn_prev_base_nofill_aree;
		by Area_ID hour_rif data_rif;
		retain prev_mgp_temp;

		if first.hour_rif then
			prev_mgp_temp=prev_mgp;
		else
			do;
				if prev_mgp ne . then
					prev_mgp_temp=prev_mgp;
			end;

		prev_mgp=prev_mgp_temp;
	run;

	proc sort data=trn_prev_base_fill_aree;
		by data_rif hour_rif;
	run;

	/*----------*/
/*	proc sql noprint;*/
/*		create table mgp_giornaliero as select*/
/*			data_rif, dt_inserimento_val, sum(prev_mgp) as prev_mgp_giorn*/
/*		from trn_prev_base_fill */
/*			group by data_rif, dt_inserimento_val*/
/*				order by data_rif, dt_inserimento_val;*/
/*	quit;*/

	data trn_mgp_orario_aree(where=(datepart(dt_inserimento_val)>="01JAN2012"d));
		retain data_rif hour dt_inserimento_val data_inserimento_val;
		set trn_prev_base_fill_aree;
		data_inserimento_val=datepart(dt_inserimento_val);
		format dt_inserimento_val datetime.;
		format data_inserimento_val date9.;

		if hour_rif lt 6 then
			giorno_gas_rif=data_rif-1;
		else giorno_gas_rif=data_rif;
		format giorno_gas_rif date9.;
	run;

	/* mod5 - gestisco le previsioni mgp per il g e per il g+1 in un colpo solo: 
		faccio due join:
		- join 1: mi serve per costruire il lag orario della previsione odierna, da utilizzare
						per le prime 13 ore di domani (colonna G1_prev_mgp) 
		- self-join 2: costruisco data_rif_g1 anticipando di un giorno la data_rif. 
			Uso questa data per fare la self-join, dove la porzione dx della tabella conterr? le info
			relative al G, mentre quella sx le info relative al G+1 */

	data trn_mgp_orario_g1_lag_aree;
		set trn_mgp_orario_aree;
		fake_data_rif = intnx("day", data_rif, 1);
		keep fake_data_rif hour_rif prev_mgp Area_ID;
		format fake_data_rif date9.;
		rename prev_mgp=prev_mgp_lag;
	run;

	proc sort data=trn_mgp_orario_aree;
		by Area_ID data_rif hour_rif ;
	run;

	proc sort data=trn_mgp_orario_g1_lag_aree;
		by Area_ID fake_data_rif hour_rif ;
	run;

	data trn_mgp_orario_g_g1_aree;
		merge trn_mgp_orario_aree(in=L rename=(prev_mgp=G_prev_mgp)) 
		trn_mgp_orario_g1_lag_aree(in=R rename=(fake_data_rif=data_rif));
		by Area_ID data_rif hour_rif;
		
		if L;

		prev_mgp_post = G_prev_mgp;
		prev_mgp_pre = prev_mgp_lag;

		if hour_rif >= 13 then
			do;
				G1_prev_mgp = G_prev_mgp;
				prev_mgp_pre = G_prev_mgp;
			end;
		else if hour_rif < 13 or missing(G_prev_mgp) then
			G1_prev_mgp = prev_mgp_lag;
		data_rif_G1 = data_rif - 1;
		format data_rif_G1 date9.;
	run;

	data trn_mgp_orario_g_g1_aree_all;
	    merge trn_mgp_orario_g_g1_aree(in=a drop=data_rif_G1 G1: prev:) 
		trn_mgp_orario_g_g1_aree(in=b drop=data_rif giorno_gas_rif G_: rename=(data_rif_G1=data_rif));
	    
	    by Area_ID data_rif hour_rif ;
	    if a; * ok left join, quando arriva mgp per domani il perimetro 
		sar? con data_rif fino a oggi, ed ? corretto perch? da ora data_rif inizia
		ad assuremere il ruolo di data in cui mi trovo;
		if missing(G1_prev_mgp) then G1_prev_mgp=G_prev_mgp;
		if missing(prev_mgp_pre) then prev_mgp_pre=G_prev_mgp;
		rename giorno_gas_rif=data_gas;
	run;

	data trn_mgp_cumulato_gsolare_aree;
	    set trn_mgp_orario_g_g1_aree_all;
	    by Area_ID data_rif hour_rif;
	    retain mgp_day_cum;

		if first.data_rif then
			do;
				G_mgp_day_cum=0;
				G1_mgp_day_cum_pre=0;
				G1_mgp_day_cum_post=0;
			end;
		
		G_mgp_day_cum + G_prev_mgp;
		G1_mgp_day_cum_pre + prev_mgp_pre;
		G1_mgp_day_cum_post + prev_mgp_post;

		if 0 <=hour_rif<=12 or missing(prev_mgp_post) then 
			G1_mgp_day_cum = G1_mgp_day_cum_pre;
		else G1_mgp_day_cum = G1_mgp_day_cum_post;	    
	run;

	data trn_mgp_cumulato_ggas_aree;
	    set trn_mgp_orario_g_g1_aree_all;
	    by Area_ID data_gas;
	    retain G_mgp_gday_cum G1_mgp_gday_cum;

	    if first.data_gas then
			do;
				G_mgp_gday_cum=0;
				G1_mgp_gday_cum_pre=0;
				G1_mgp_gday_cum_post=0;
			end;
		
		G_mgp_gday_cum + G_prev_mgp;
		G1_mgp_gday_cum_pre + prev_mgp_pre;
		G1_mgp_gday_cum_post + prev_mgp_post;

		if 0 <=hour_rif<=12 or missing(prev_mgp_post) then 
			G1_mgp_gday_cum = G1_mgp_gday_cum_pre;
		else G1_mgp_gday_cum = G1_mgp_gday_cum_post;	    
	run;

	data trn_mgp_orario_final_aree;
	    merge trn_mgp_cumulato_gsolare_aree(in=uno drop=G1_mgp_day_cum_pre)
	        trn_mgp_cumulato_ggas_aree (in=due);
	    by Area_ID data_rif hour_rif;
	    if uno and due;
	run;

	proc sort data=trn_mgp_orario_final_aree(drop=prev_mgp_lag prev_mgp_pre
		rename=(prev_mgp_post=G1_prev_mgp_post));
		by data_rif hour_rif Area_ID;
	run;

	proc transpose data=trn_mgp_orario_final_aree out=trn_mgp_orario_final_aree_t;
		by data_rif hour_rif Area_ID Area_Label;
		var G_prev_mgp G_mgp_day_cum G_mgp_gday_cum
			G1_prev_mgp G1_mgp_day_cum G1_mgp_gday_cum G1_mgp_gday_cum_pre
			G1_mgp_gday_cum_post G1_prev_mgp_post G1_mgp_day_cum_post;
	run;

	data trn_mgp_orario_final_aree_t;
		length type_var $ 20;
		set trn_mgp_orario_final_aree_t;
		modello = scan(_NAME_, 1, "_");
		type_var = "mgp_oraria";
		if index(_NAME_, "_day_cum") and not index (_NAME_, "post")
			then type_var = "mgp_day_cum";
		else if index(_NAME_, "_gday_cum") and not index(_NAME_, "cum_")
			then type_var = "mgp_gday_cum";
		else if index(_NAME_, "_gday_cum_pre") then type_var = "mgp_gday_cum_pre";
		else if index(_NAME_, "_gday_cum_post") then type_var = "mgp_gday_cum_post";
		else if index(_NAME_, "prev_mgp_post") then type_var = "mgp_oraria_post";
		else if index(_NAME_, "mgp_day_cum_post") then type_var = "mgp_day_cum_post";
		if hour_rif lt 6 then
			data_gas=data_rif-1;
		else data_gas=data_rif;
		key_modello = 0;
		if modello = "G1" then key_modello = 1;
		data_gas_rif = intnx("day", data_gas, key_modello);
		datetime_riferimento_dati = dhms(data_gas_rif, 0,0,0);
/*		rename col1 = mgp_oraria;*/
		format data_gas: date9. datetime_riferimento_dati datetime19.;
	run;

	proc sort data=trn_mgp_orario_final_aree_t;
		by Area_ID data_rif hour_rif key_modello;
	run;

	proc transpose data=trn_mgp_orario_final_aree_t 
		out=trn_mgp_orario_final_aree_t_2;
		by Area_ID Area_Label data_rif hour_rif key_modello modello 
		data_gas data_gas_rif;
		var col1;
		id type_var;
	run;

	/* imputo valore orario, cumulato giorno e cumulato giorno gas con i consuntivi
	di quei giorni*/

	data trn_mgp_imput_orariocum_aree(rename=consuntivo_mix=cons);
	    merge trn_cons_orario_imp_aree trn_cons_orario_gcum_aree 
		trn_cons_orario_cum_aree;
	    by Area_ID data_rif hour_rif;
	    keep data_rif hour_rif dt_inserimento_val_c cons_gday_cum 
		cons_day_cum consuntivo_mix Area_ID area_label;
	    dt_inserimento_val_c=dhms(intnx("day",data_rif,-1),0,0,0);
	    format dt_inserimento_val_c datetime19.;
	run;

/*	proc sort data=mgp_orario_final_aree;*/
/*		by Area_ID data_rif hour_rif key_modello;*/
/*	run;*/

	data trn_mgp_orario_final_fill_aree;
	    merge cross_join_calendario_previsioni(in=k rename=(date=data_rif hour=hour_rif) 
			drop= dt_calcolo) 
	        trn_mgp_orario_final_aree_t_2(in=p) trn_mgp_imput_orariocum_aree(in=q);
	    by Area_ID data_rif hour_rif ;
	    if mgp_oraria eq . then
	        do;
	            mgp_oraria=cons;
	/*           dt_inserimento_val=dt_inserimento_val_c;*/
	            mgp_day_cum=cons_day_cum;
	            mgp_gday_cum=cons_gday_cum;
	        end;
	    data_inserimento_val=datepart(dt_inserimento_val_c);
	    drop cons:;
	    drop _NAME_;
	    if k;
	run;

	proc sql noprint;
		%let savelist=;
		select compress(cat("terna_", name))
			into :savelist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="TRN_MGP_ORARIO_FINAL_FILL_AREE"
						and name like "mgp%" and not index(name, "gday_cum");
	quit;

	proc sql noprint;
		%let droplist=;
		select name
			into :droplist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="TRN_MGP_ORARIO_FINAL_FILL_AREE"
						and name like "mgp%" and not index(name, "gday_cum");
	quit;

	proc sort data=trn_mgp_orario_final_fill_aree out=TRN_MGP_ORARIO_FINAL_SORT_AREE;
		by Area_ID key_modello hour_rif data_rif;
	run;

	data input_up_mgp_ora_termo_aree(drop= i j &droplist rename=(date=giorno_solare));
		set	TRN_MGP_ORARIO_FINAL_SORT_AREE;
		array new &savelist;
		array old &droplist;
		by Area_ID key_modello hour_rif data_rif;
		retain new;

		if first.hour_rif then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;
		drop dt_inserimento_val_c data_inserimento_val;
	run;

	proc sort data=input_up_mgp_ora_termo_aree;
		by key_modello Area_ID  data_rif hour_rif;
	run;

	data input_up_mgp_ora_termo_aree_new;
		set input_up_mgp_ora_termo_aree;
		retain mgp_gday_cum_base;
		
		by key_modello Area_ID;
		if first.Area_ID then mgp_gday_cum_base=mgp_gday_cum;
		if modello = "G1" then
			do;
				if not missing(mgp_gday_cum) then
					mgp_gday_cum_base = mgp_gday_cum;
				else if missing(mgp_gday_cum) then
					do;
						mgp_gday_cum = mgp_gday_cum_base;
						mgp_gday_cum_base + terna_mgp_oraria;
						mgp_gday_cum = mgp_gday_cum_base;
					end;
			end;
			rename mgp_gday_cum = terna_mgp_gday_cum;
		dt_calcolo = dhms(data_rif, hour_rif, 0, 0);
	run;

	/*end mod5 */

	/* terzo output della funzione:input_up_mgp_ora_termo */
	/*-----------------------------------------PREVISIONI GIORNO-----------------------------------------*/
	proc sql noprint;
		create table trn_mgp_giornaliero_aree as select
			data_rif,dt_inserimento_val, area_id, area_label,
			sum(prev_mgp) as prev_mgp_giorn
		from trn_prev_base_fill_aree 
			group by data_rif, dt_inserimento_val, area_id, area_label
				order by area_id, area_label, data_rif, dt_inserimento_val;
	quit;

	/* ----------------------------------------------------- */
	/*imputo passato con consuntivi*/
	proc sql noprint;
		create table trn_mgp_imputazione_giorn_aree as select
			intnx("day",giorno_solare,-1) format=date9. as data_rif, 
			terna_consUNTIVO as prev_mgp_giorn_imp, area_id
		from input_up_cons_termo_aree
			where hour=22 and modello = "G"
				order by area_id, data_rif;
	quit;

	/*ho il consuntivo riferito al giorno data_rif...poich? sto ricostruendo lo storico
	fingo di sapere la previsione per quel valore dalle 13 del giorno precedente a data_rif da inizio giornata*/
	data trn_mgp_imputazione_giorn_aree(where=(Data_rif<&G_gas and datepart(dt_inserimento_val)>="01JAN2012"d));
		set trn_mgp_imputazione_giorn_aree;
		dt_inserimento_val=dhms(intnx("day",data_rif,-1),13,0,0);
		format dt_inserimento_val datetime19.;
	run;

	/*(rename=(prev_mgp_mixv=mgp_giorn)*/
	data trn_previsioni_giorn2_aree (drop=prev_mgp_giorn_imp);
		merge trn_mgp_giornaliero_aree(in=L)
			  trn_mgp_imputazione_giorn_aree(in=R rename=(dt_inserimento_val=dt_inserimento_val_c));
		by area_id data_rif;

		if L;

		if prev_mgp_giorn eq . then
			do;
				prev_mgp_giorn=prev_mgp_giorn_imp;
				dt_inserimento_val=dt_inserimento_val_c;
			end;
		if missing(dt_inserimento_val_c) then
			dt_inserimento_val_c = dhms(intnx("day",data_rif,-1),13,0,0);
	run;

	proc sql noprint;
		select max(data_rif)
			into :max_date_prevt
				from trn_prev;
	run;

	proc sort data=cross_join_calendario_previsioni;
		by key_modello Area_ID date hour ;
	run;

	data trn_prev_giorn_aree_final_G(where=(date<=&max_date_prevt)
		rename=(prev_mgp_giorn=mgp_giorn));
		merge cross_join_calendario_previsioni(In=p where=(modello="G")) 
		trn_previsioni_giorn2_aree(rename=(data_rif=date) where=(date<=&g_gas));
		by area_id date;

		if p;
		drop dt_inserimento_val_c;
	run;

	proc sort data=cross_join_calendario_previsioni;
		by key_modello Area_ID dt_calcolo ;
	run;

	data trn_prev_giorn_aree_final_G1(where=(data_gas<=&max_date_prevt)
		rename=(prev_mgp_giorn=mgp_giorn));
		merge cross_join_calendario_previsioni(In=p where=(modello="G1")) 
		trn_previsioni_giorn2_aree(Rename=(data_rif=data_gas_rif
						dt_inserimento_val=dt_calcolo) in=R);
		by area_id dt_calcolo;

		if p;
		drop dt_inserimento_val_c;
	run;

	data trn_prev_giorn_aree_final;
		set trn_prev_giorn_aree_final_G trn_prev_giorn_aree_final_G1;
	run;

	proc sort data=trn_prev_giorn_aree_final;
		by area_id date hour key_modello;
	run;

	/*dove valore mancante in t deve essere uguale a quello precedente
	pi? vicino a livello temporale (quindi t-1, t-2, ecc)*/

	proc sort data=cross_join_calendario_previsioni;
		by Area_ID date hour key_modello;
	run;

	data trn_prev_giorn_aree_final_fill;
		merge cross_join_calendario_previsioni(in=L)
			trn_prev_giorn_aree_final(in=R);

		by area_id date hour key_modello;
		if L;
		retain mgp_giorno_fill_g mgp_giorno_fill_g1;

		if first.area_id then 
			do;
				if modello = "G" then
					mgp_giorno_fill_g = mgp_giorn;
				else mgp_giorno_fill_g1 = mgp_giorn;
			end;
		if not missing(mgp_giorn) then 
			do;
				if modello = "G" then
					mgp_giorno_fill_g = mgp_giorn;
				else mgp_giorno_fill_g1 = mgp_giorn;
			end;
		else if missing(mgp_giorn) then
			do;
				if modello = "G" then
					mgp_giorn = mgp_giorno_fill_g;
				else if modello = "G1" then
					do;
						if not missing(mgp_giorno_fill_g1) then
							mgp_giorn = mgp_giorno_fill_g1;
						else mgp_giorn = mgp_giorno_fill_g;
					end;
			end;
		rename mgp_giorn = terna_mgp_day;
	run;

	data input_up_mgp_termo_aree;
		set trn_prev_giorn_aree_final_fill(where=(data_gas>="03JAN2012"d) 
			rename=(date=giorno_solare));
		drop dt_inserimento_val;
	run;

	/* quarto output = input_up_mgp_termo */
	/*------------------------------CONSUNTIVI-------------------------------*/
	/*----------------------------lag giornalieri----------------------------*/
	/*-----------------------------------------------------------------------*/
	data trn_cons_lag_base_aree(drop=hour);
		set input_up_cons_termo_aree;
		where hour=23;

		keep hour giorno_solare terna_consuntivo modello giorno_gas
		key_modello area_id Area_Label;

		output;

		if giorno_solare = &g_gas  and giorno_solare < &g_solare then
			do;
				giorno_solare=giorno_solare+1;
				output;
			end;
	run;

	DATA X;
		DO X=1 TO 28;
			X_str = put(X, $3.);
			OUTPUT;
			keep X_str;
		END;
	RUN;

	PROC SQL NOPRINT;
		SELECT cat("convert terna_CONSUNTIVO = terna_CONSUNTIVO_", 
		compress(X_str), "/ transformout=(lag ", compress(X_str), ");") 
		INTO:LG SEPARATED BY ' ' FROM X;
	QUIT;

	proc sort data=trn_cons_lag_base_aree;
		by key_modello area_id Area_Label giorno_solare;
	run;

	proc expand data=trn_cons_lag_base_aree
		out=trn_cons_giorno_lag_temp_aree(drop=TIME);
		by key_modello area_id area_label;
		id giorno_solare;
		&LG;
	run;

	data trn_cons_giorno_lag_temp_aree_2;
		retain area_id area_label key_modello modello giorno_solare giorno_gas;
		set trn_cons_giorno_lag_temp_aree;

		if cmiss(of _all_) then
			delete;

		drop terna_CONSUNTIVO;
	run;

	proc sort data=trn_cons_giorno_lag_temp_aree_2;
		by area_id Area_Label giorno_solare key_modello;
	run;

	data TRN_CONS_GIORNO_LAG_FINAL_AREE;
		merge cross_join_calendario_previsioni(in=p) 
		trn_cons_giorno_lag_temp_aree_2(in=q Rename=(giorno_solare=date));
		by area_id date;

		if p and q;
	run;

	proc sql noprint;
		%let savelist=;
		select compress(tranwrd(name,"CONSUNTIVO_","cons_day_lag"))
			into :savelist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="TRN_CONS_GIORNO_LAG_FINAL_AREE"
						and name like "terna%";
	quit;

	proc sql noprint;
		%let droplist=;
		select name
			into :droplist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="TRN_CONS_GIORNO_LAG_FINAL_AREE"
						and name like "terna%";
	quit;

	data input_cons_lag_aree(drop=a b i j &droplist);
		set	TRN_CONS_GIORNO_LAG_FINAL_AREE;
		array new &savelist;
		array old &droplist;
		retain new;
		a=dim(new);
		b=dim(old);

		if first.area_id then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;

		format data: date9.;
	run;

	/*---------------------------CONSUNTIVI--------------------------*/
	/*----------------------------lag orari--------------------------*/
	/*---------------------------------------------------------------*/

	data trn_cons_orario_lag_aree(rename=(terna_CONSUNTIVO_post=terna_cons_oraria_lag7d
		terna_cons_day_cum_post=terna_cons_oraria_cumday_lag7d
		terna_cons_gday_cum_post=terna_cons_oraria_cumgday_lag7d));

		retain area_id area_label dt_lag_7 key_modello modello data_gas_rif;

		set input_up_cons_ora_termo_aree;

		if key_modello = 0 then
			dt_lag_7 = intnx("dthour", dhms(giorno_solare, hour, 0 , 0), 24*7);
		else if key_modello = 1 then
			dt_lag_7 = intnx("dthour", dhms(intnx("day", giorno_solare, 1), 
								hour, 0 , 0), 24*7);

		format giorno: date9. dt_lag_7 datetime19.;
		drop terna_cons terna_cons_day_cum terna_cons_gday_cum giorno_gas
		dt_calcolo dt_riferimento datetime_riferimento_dati data_gas_rif;
	run;

	proc sort data=trn_cons_orario_lag_aree;
		by  area_id  dt_lag_7 key_modello;
	run;

	data input_cons_orario_lag_aree;
		merge trn_cons_orario_lag_aree(rename=(dt_lag_7=dt_calcolo
			drop=) 
			in=q)
			CROSS_JOIN_CALENDARIO_PREVISIONI(in=p);

		by area_id dt_calcolo key_modello;
		if p ;
	run;

	/*------------------------------PREVISIONI-------------------------------*/
	/*----------------------------lag giornalieri----------------------------*/
	/*-----------------------------------------------------------------------*/
	data trn_prev_lag_base_aree(drop=hour);
		set input_up_mgp_termo_aree(drop=mgp:
			where=(hour=23));
		output;

		if giorno_solare = &g_gas  and giorno_solare < &g_solare then
			do;
				giorno_solare=giorno_solare+1;
				output;
			end;
	run;

	DATA X;
		DO X=1 TO 28;
			X_str = put(X, $3.);
			OUTPUT;
			keep X_str;
		END;
	RUN;

	PROC SQL NOPRINT;
		SELECT cat("convert terna_mgp_day = terna_mgp_giorn_", 
		compress(X_str), "/ transformout=(lag ", compress(X_str), ");") 
		INTO:LG SEPARATED BY ' ' FROM X;
	QUIT;

	proc sort data=trn_prev_lag_base_aree;
		by key_modello area_id Area_Label giorno_solare;
	run;

	proc expand data=trn_prev_lag_base_aree
		out=trn_prev_lag_temp_aree(drop=TIME);
		by key_modello area_id area_label;
		id giorno_solare;
		&LG;
	run;

	data trn_prev_giorno_lag_aree;
		set trn_prev_lag_temp_aree;

		if cmiss(of _all_) then
			delete;

		drop terna_mgp_day;
		keep key_modello area_id Area_Label giorno_solare terna:;
	run;

	proc sort data=trn_prev_giorno_lag_aree;
		by key_modello area_id Area_Label giorno_solare;
	run;

	proc sort data=cross_join_calendario_previsioni;
		by key_modello area_id Area_Label date;
	run;

	data TRN_PREV_LAG_TEMP_AREE;
		merge cross_join_calendario_previsioni(Rename=(date=giorno_solare) in=p) 
			trn_prev_giorno_lag_aree(in=q);
		by key_modello area_id giorno_solare ;

		if p and q;
	run;

	proc sql noprint;
		%let savelist=;
		select compress(tranwrd(name,"mgp_giorn_","mgp_day_lag"))
			into :savelist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="TRN_PREV_LAG_TEMP_AREE"
						and index(lowcase(name),'mgp_giorn');
	quit;

	proc sql noprint;
		%let droplist=;
		select name
			into :droplist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="TRN_PREV_LAG_TEMP_AREE"
						and index(lowcase(name),'mgp_giorn');
	quit;

/*	proc sort data=PREVISIONI_LAG_TEMP_AREE;*/
/*		by key_modello area_id Area_Label giorno_solare hour;*/
/*	run;*/

	data input_mgp_lag_aree(drop= a b i j &droplist);
		set	TRN_PREV_LAG_TEMP_AREE;
		array new &savelist;
		array old &droplist;
		retain new;
		a=dim(new);
		b=dim(old);
		by key_modello area_id giorno_solare;
		if first.area_id then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;

		format data: date9.;
	run;

	/*---------------------------PREVISIONI--------------------------*/
	/*----------------------------lag orari--------------------------*/
	/*---------------------------------------------------------------*/

	/* devo costruire lag1 e lag7. 
	I lag usano tutte le orarie MGP riferite a uno specifico giorno, 
	senza alcuna discontinuit? pre/post 13. La soluzione pi? comoda 
	? usare l'MGP post oppure costruirli a partire dall'MGP del G. Calcolo i lag
	prendendo come riferimento il giorno da predire, in modo da avere un lag7 
	perfettamente allineato	alla stagionalit? settimanale*/

	data trn_prev_oraria_lag_aree;
		set input_up_mgp_ora_termo_aree_new;
		if key_modello = 0 then
			do;
				terna_mgp_oraria_tlg = terna_mgp_oraria;
				terna_mgp_oraria_cumgday_tlg = terna_mgp_gday_cum;
				terna_mgp_oraria_cumday_tlg = terna_mgp_day_cum;
				dt_calcolo_lag = dhms(data_rif, hour_rif, 0 , 0);
			end;
		else if key_modello = 1 then
			do;
				terna_mgp_oraria_tlg = terna_mgp_oraria_post;
				terna_mgp_oraria_cumgday_tlg = mgp_gday_cum_post;
				terna_mgp_oraria_cumday_tlg = terna_mgp_day_cum_post;
				dt_calcolo_lag = dhms(intnx("day", data_rif, 1), hour_rif, 0 , 0);
			end;
		dt_calcolo = dhms(data_rif, hour_rif, 0 , 0);
		format dt_: datetime19.;
		keep Area_ID Area_Label key_modello terna_mgp_oraria_tlg data_rif
		terna_mgp_oraria_cumgday_tlg terna_mgp_oraria_cumday_tlg 
		dt_calcolo_lag dt_calcolo;
	run;

	/* rispetto al giorno di previsione, il lag1 del G+1 coincide con l'MGP che si ha per
	il G, ovvero una previsione senza discontinuit? alle 13.
	il lag 1 rispetto al G coincide con il  lag0 del G+1. Di contro, il lag2 
	serve al G+1 ma meno al G. Posso comunque tenere lag1 e lag2 per 
	entrambi, considerando per? le differenti pertinenze */

	DATA X;
		length var $32;
		do j = 1 to 3;
		DO X=1 TO 7;
			Y = X*24;
			X_str = put(Y, $3.);
			D_str = cat(compress(put(X, $3.)), "d");
			if j = 1 then var = "terna_mgp_oraria_tlg";
			else if j = 2 then var = "terna_mgp_oraria_cumgday_tlg";
			else if j = 3 then var = "terna_mgp_oraria_cumday_tlg";
			if X in (1, 2, 7) then 
				OUTPUT;
			keep X_str var D_str;
		END;
		end;
	RUN;

	PROC SQL NOPRINT;
		SELECT cat("convert ", compress(var) ," = ", compress(var), 
		compress(D_str), "/ transformout=(lag ", compress(X_str), ");") 
		INTO:LG SEPARATED BY ' ' FROM X;
	QUIT;

/*	proc sort data=trn_prev_oraria_lag_aree;*/
/*		by key_modello area_id area_label dt_calcolo_lag;*/
/*	run;*/

	proc expand data=trn_prev_oraria_lag_aree
		out=trn_prev_oraria_lag_aree_tmp(drop=TIME);
		by key_modello area_id area_label;
		id dt_calcolo_lag;
		&LG;
	run;

	proc sql noprint;
		%let droplist=;
		select tranwrd(name,"tlg", "lag")
			into :droplist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="TRN_PREV_ORARIA_LAG_AREE_TMP"
						and name like "terna%" and index(name, "tlg");
	quit;

	proc sql noprint;
		%let savelist=;
		select name
			into :savelist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="TRN_PREV_ORARIA_LAG_AREE_TMP"
						and name like "terna%" and index(name, "tlg");
	quit;

	data input_mgp_ora_lag_aree(drop= a b i j &savelist);
		set	TRN_PREV_ORARIA_LAG_AREE_TMP(drop=data_rif dt_calcolo_lag);
		array new &droplist;
		array old &savelist;
		retain new;
		a=dim(new);
		b=dim(old);

		by key_modello area_id dt_calcolo;
		if first.area_id then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;
		drop terna_mgp_oraria_lag terna_mgp_oraria_cumgday_lag 
		terna_mgp_oraria_cumday_lag;
	run;

	proc datasets lib=work nolist;
		delete trn:;
	run;

	/*-------------------------------merge all-------------------------------*/
/*	%let output_terna_cons=&output_consuntivo_terna;*/
/*	%let output_terna_prev=&output_previsione_terna;*/

	data &output_consuntivo_terna_aree(where=(datepart(dt_calcolo)>=&start_date));
		merge  input_up_cons_ora_termo_aree(drop=terna_consuntivo_post terna_cons_day_cum_post
				terna_cons_gday_cum_post rename=(giorno_gas=data_gas))
			 input_up_cons_termo_aree(keep=area_id dt_calcolo key_modello terna_consuntivo)
			 input_cons_lag_aree(keep=area_id dt_calcolo key_modello terna:)
			 input_cons_orario_lag_aree(keep=area_id dt_calcolo key_modello terna:);
		by area_id dt_calcolo key_modello;
		drop dt_riferimento;
		rename terna_cons=terna_cons_oraria terna_CONSUNTIVO=terna_cons_day 
/*			terna_cons_day_cum=terna_cons_oraria_cumday*/
/*			terna_cons_gday_cum=terna_cons_oraria_cumgday*/
;
	run;

	proc sort data=input_up_mgp_termo_aree;
		by key_modello area_id dt_calcolo;
	run;

	data &output_previsione_terna_aree(where=(datepart(dt_calcolo)>=&start_date));
		merge  input_up_mgp_ora_termo_aree_new(drop=mgp: terna_mgp_oraria_post 
			   terna_mgp_day_cum_post)
			   input_up_mgp_termo_aree(keep=key_modello area_id dt_calcolo terna_mgp_day)
			   input_mgp_lag_aree(keep=key_modello area_id dt_calcolo terna:)
			   input_mgp_ora_lag_aree(drop=area_label);
		by key_modello area_id dt_calcolo;
		drop dt_inserimento_val;
/*		rename terna_mgp_day_cum=terna_mgp_oraria_cumday */
/*			terna_mgp_gday_cum=terna_mgp_oraria_cumgday terna_mgp_giorn=terna_mgp_day;*/
	run;

	data &output_consuntivo_terna_aree;
		set &output_consuntivo_terna_aree;
		rename dt_calcolo=datetime_solare giorno_solare=date;
	run;
	
	data &output_previsione_terna_aree;
		set &output_previsione_terna_aree;
		rename dt_calcolo=datetime_solare data_rif=date 
				hour_rif=hour;
	run;


%mend terna;

%macro read_transiti_entryexit(input_transiti_entryexit=, shift_type=, 
	shift_value=, output_transiti_entryexit=);

/*	%let shift_type=month;*/
/*	%let shift_value=36;*/
/*	%let input_calendario_dates=input_calendario_dates;*/
/*	%let output_transiti_entryexit=&output_transiti_entryexit;*/

	%set_startdates_calendar(g_gas=&g_gas, g1_solare=&g1_solare, 
			shift_type=&shift_type, shift_value=&shift_value, 
			out_calendar=&input_calendario_dates);

	proc sql noprint;
		select max(data_ora_rif) format datetime19. into: start_date_adj
		from &input_transiti_entryexit(where=(id_scada =30155316 and valore="4"
					and data_ora_rif le "&start_datetime_char"dt ));
	quit;

	data start_date_adj;
		if missing("&start_date_adj"dt) then start_dt_true = &start_dt_all;
		else start_dt_true = "&start_date_adj"dt;
		format start_dt_true datetime19.;
		call symputx("start_dt_true", start_dt_true);
	run;

	data &output_transiti_entryexit;
		set &input_transiti_entryexit;
		where data_ora_rif >= &start_dt_true;
	run;

%mend;

%macro entryexit(g_gas=, g1_solare=, shift_type=, shift_value=,  
			     input_ff=, input_transiti_entryexit=, input_anagrafica=, 
				input_calendario_dates=,  output_ds=);


/*%let input_calendario_dates=&input_calendario_dates;  */
/*%let input_ff=&input_entry_exit;  */
/*%let g_gas=&g_gas;*/
/*%let g1_solare=&g1_solare;*/
/*%let shift_type=month;*/
/*%let shift_value=42;*/
/*%let input_anagrafica=webdips.geo_anag_entryexit;*/
/*%let input_transiti_entryexit=&output_transiti_entryexit;*/
/*%let output_ds=for_svil.geo_output_entry_exit; */

	%set_startdates_calendar(g_gas=&g_gas, g1_solare=&g1_solare, shift_type=&shift_type, shift_value=&shift_value, out_calendar=&input_calendario_dates);

	/* Aggiungo Apici nella start_date per leggere ffisici */
	data _null_;
		ffisici_date=cat("'","&start_date_char","'");
		call symput("ffisici_date",ffisici_date);
	run;

	%let sql_query = 
		select dt_calcolo
			  ,id_codice_remi_ass
			  ,id_tipologia_punto
			  ,tx_punto
			  ,valore_smc
		from &input_ff
		where (dt_calcolo>= TO_DATE(&ffisici_date, 'DDMONYYYY'));

	proc sql;
		connect to Oracle as x1( user=&ORAUSR password=&ORAPWD path="&ORAPATH");
		%put &sqlxmsg;
		create table flussifisici as
			select * from connection to x1(&sql_query);
		%put &sqlxmsg;
		disconnect from x1;
	quit;

	/* Aggiungo area geo a flussifisici */
	proc sql;
		create table flussifisici (where = (compress(area_label) ne "")) as 
		select a.dt_calcolo
			  ,a.id_tipologia_punto
			  ,a.tx_punto
			  ,a.valore_smc
			  ,b.area_label
			  ,b.area_descrizione
			  ,b.area_id
		from flussifisici as a
		left join &input_anagrafica (where = (compress(id_codice_remi_ass) ne ""
		and fl_deleted = "0")) as b
		on a.id_codice_remi_ass = b.id_codice_remi_ass
		;
	quit;

	data flussifisici (drop=dt_calcolo id_tipologia_punto);
		retain datetime_solare date hour data_gas_rif area_id type;
		set flussifisici;
		datetime_solare=dt_calcolo;
		date=datepart(dt_calcolo);
		hour=hour(dt_calcolo);
		if hour<6 then data_gas_rif=intnx("day", date, -1);
		else data_gas_rif=date;
		format dat: date9. datetime: datetime19.;
		if id_tipologia_punto in ("Entry Importazione" "Entry GNL") then type = "imp";
		else if id_tipologia_punto = "Exit Esportazione" then type = "exp";
		else if id_tipologia_punto="Entry Produzione Nazionale" then type = "pnl";
		else if id_tipologia_punto="Immissione a stoccaggio" then type = "ims";
		else if id_tipologia_punto="Prelievo da stoccaggio" then type = "prs";
	run;

	/* Aggiungo S.POLO e VILLA ZANELLI --> Prendo solo id e date di interesse */
	proc sql noprint;
		select distinct id_scada into: id_misura_aop
			separated by " "
		from &input_anagrafica
		where id_scada ne . and fl_deleted = "0"
		;
	quit;

	data to_add (keep=datetime_solare valore_smc id_scada data_gas_rif);
		set &input_transiti_entryexit (where=(id_scada in (&id_misura_aop)));

		valore_smc=(input(scan(valore,1,'**'), 12.));
		rename data_ora_rif=datetime_solare;
		data_gas_rif = ifn(hour(data_ora_rif)<6,
					intnx("day", datepart(data_ora_rif), -1),
					datepart(data_ora_rif));
		format data_gas_rif date9.;
	run;

	proc sql;
		create table to_add as 
		select a.*
			  ,b.type
			  ,b.soglia
			  ,b.area_label
			  ,b.area_descrizione
			  ,b.area_id
			  ,b.nome_punto
		from to_add as a
		left join &input_anagrafica (where = (id_scada ne .)) as b 
		on a.id_scada = b.id_scada
		;
	quit;

	/* Correggo i valori di valore_smc secondo le soglie */
	data to_add (drop=id_scada soglia nome_punto);
		set to_add;
		/* Soglie in vigore prima del 20 Maggio 2020 h14 */
		if datetime_solare lt "20MAY2020:14:00:00"dt then do;
			if id_scada in (30142890 30142891) then soglia=20000;
			else if id_scada in (30142889 30142892) then soglia=10000;
		end;
		/* Dopo il 20 Maggio 2020 h14 incluso utilizzo le soglie che trovo in anagrafica */
		if valore_smc le soglia then valore_smc=0;
		tx_punto = compress(nome_punto);
	run;

	data flussifisici_teddi;
		set flussifisici to_add;
	run;

	/* CROSS JOIN con CALENDARIO DATES */
	proc sql;
		create table tx_punto as 
		select distinct area_id
				, area_label
				, area_descrizione
				, type
				, tx_punto
		from flussifisici_teddi
		order by area_id, type, tx_punto
		;
	quit;
	/* 80 rows */

	proc sql;
		create table maschera as 
		select a.*
			  ,b.*
		from &input_calendario_dates as a
		cross join tx_punto as b
		order by area_id, type, tx_punto, datetime_solare
		;
	quit;
	/* 72*80 = 5760 rows*/

	proc sort data=flussifisici_teddi;
		by area_id type tx_punto datetime_solare;
	run;

	data flussifisici_teddi;
		merge maschera(in=m) flussifisici_teddi;
		by area_id type tx_punto datetime_solare;
		if m;
	run;

	/* FFILL su valore smc */
	data flussifisici_teddi (drop=valore_smc_new);
		set flussifisici_teddi;
		by area_id type tx_punto datetime_solare;
		retain valore_smc_new;
		flag_ffill = 0;
		if first.tx_punto then valore_smc_new=valore_smc;
		else do;
			if valore_smc ne . then 
				do;
					flag_ffill = 0;
					valore_smc_new=valore_smc;
				end;
			else if valore_smc = . then 
				do;
					flag_ffill = 1;
					valore_smc=valore_smc_new;
				end;
		end;
	run;
	
	%if &flg_anomalie=1 %then
		%do;
			%put ===== Storicizzazione Anomalie EntryExit =====;

			%gestione_storico_anomalie_ffill(ds_in=flussifisici_teddi, var_2count=tx_punto,
			count_text=count_entryexit_missing, dt_riferimento=datetime_solare,
			storico_anomalie=&output_anomalie_entryexit);
		%end;

	proc sql;
		create table comp_all as 
		select distinct datetime_solare
			, data_gas_rif
			, area_label
			, area_descrizione
			, area_id
			, type
			, sum(valore_smc) as valore_smc
		from flussifisici_teddi
		group by 1,2,3,4,5,6
		order by datetime_solare, area_id, type 
		;
	quit;

	proc transpose data=comp_all out=&output_ds (drop=_name_);
		by datetime_solare data_gas_rif area_id area_label area_descrizione;
		id type;
		var valore_smc;
	run;

	/* stdize per i missing */
	proc stdize data=&output_ds out=&output_ds reponly missing=0;
	run;

	proc sort data=&output_ds (rename =(data_gas_rif=data_gas));
		by area_id datetime_solare;
	run;

%mend entryexit;


%macro deltalinepack(g_gas=, g1_solare=, shift_type=, shift_value=,
					input_calendario_dates=, ds_in=, prefix=, anagrafica_dlp=, ds_output=);
/**/
/*%let g_gas="02OCT2020"d;*/
/*%let g1_solare="03OCT2020"d;*/
/*%let shift_type=day;*/
/*%let shift_value=2;*/
/*%let input_calendario_dates=input_calendario_dates;*/
/*%let ds_in=oralpbp2.dati_linepack_vw;; */
/*%let prefix=dlp_; */
/*%let anagrafica_dlp=for_svil.geo_anag_dlp;*/
/*%let ds_output=output_dlp;*/

	%set_startdates_calendar(g_gas=&g_gas, g1_solare=&g1_solare, shift_type=&shift_type, shift_value=&shift_value, 
						out_calendar=&input_calendario_dates);

	/*  DLP è valorizzato solo a partire dal 02JUL2019*/
	data _null_;
		start_datetime_base ='02JUL2019:06:00:00'dt;
		temp ="&start_datetime_char"dt;
		if missing(temp) or temp < start_datetime_base
		  then start_dt_dlp = start_datetime_base;
		else start_dt_dlp = temp;
		format start_dt_dlp datetime19.;
		call symputx("start_dt_dlp", start_dt_dlp);
	run;


	data &prefix.dati_linepack_vw;
		set &ds_in (keep=data_ora_rif giorno_gas tronco metanodotto 
					area_prelievo delta_linepack  linepack
					where=(data_ora_rif>= &start_dt_dlp));
	run;


	/*Group for metanodotto*/
	proc sql;
		create table &prefix.tab_metanodotti as 
		select distinct data_ora_rif
			 , giorno_gas
			 , metanodotto
			 , area_prelievo
			 , sum(delta_linepack) as delta_linepack
			 , sum(linepack) as linepack
		from &prefix.dati_linepack_vw
		group by 1,2,3,4
		;
	quit;

	/* Distinct Metanodotto */
	proc sql;
		create table &prefix.metanodotti_unique as 
		select distinct metanodotto
				, area_prelievo 
		from &prefix.tab_metanodotti;
	quit;

	data &prefix.calendario_dlp (keep=dt_calendario);
		set &input_calendario_dates;
		rename datetime_solare=dt_calendario;
	run;

	proc sql;
		create table &prefix.cross_join as 
		select *
		from &prefix.metanodotti_unique 
		cross join
		&prefix.calendario_dlp
		order by dt_calendario;
	quit;

	/*Create full table with missing*/
	proc sql;
		create table &prefix.full_table as 
		select t1.*
			 , t2.delta_linepack
			 , t2.linepack
			 , t2.data_ora_rif
			 , t2.giorno_gas
		from &prefix.cross_join t1 
		left join &prefix.tab_metanodotti t2
		on t1.dt_calendario = t2.data_ora_rif and 
			t1.metanodotto=t2.metanodotto and 
			t1.area_prelievo=t2.area_prelievo
		order by metanodotto, area_prelievo, dt_calendario;
	quit;

	/*ffil dei tronchi per i valori prima dell'inizio funzionamento tronco*/
	data &prefix.dati_linepack_vw_ffill(keep= data_ora_rif flag_ffill giorno_gas 
												metanodotto area_prelievo delta_linepack linepack);
		set &prefix.full_table;
		by metanodotto area_prelievo;
		valore_clean = delta_linepack;
		valore_clean2 = linepack;
		flag_ffill = 0;

		retain temp_val temp_val2;

		/* Reset TEMP when the BY-Group changes */
		if first.metanodotto or first.area_prelievo then do;
			temp_val=.;
			temp_val2=.;
		end;

		/* Assign TEMP when valore_clean is non-missing */
		if valore_clean ne . then
			temp_val=valore_clean;
		/* When valore_clean is missing, the retained value is  assigned*/
		else if valore_clean=. then
			do;
				flag_ffill = 1;
				valore_clean=temp_val;
			end;

		if valore_clean2 ne . then
			temp_val2=valore_clean2;
		else if valore_clean2=. then
			valore_clean2=temp_val2;

		delta_linepack = valore_clean;
		linepack = valore_clean2;
		data_ora_rif=dt_calendario;
		/*Fill Giorno Gas	*/
		date_rif =datepart(data_ora_rif);
		hour=hour(data_ora_rif);
		if hour<6 then do;
			giorno_gas = intnx("day", date_rif, -1);
		end;
		else do;
			giorno_gas =date_rif;
		end;

		format giorno_gas date9.;
	run;
	
	%if &flg_anomalie=1 %then
		%do;
			%put ===== Storicizzazione Anomalie DLP =====;

			%gestione_storico_anomalie_ffill(ds_in= &prefix.dati_linepack_vw_ffill, var_2count=metanodotto, 
			count_text=count_dlp_missing, dt_riferimento=data_ora_rif,
			storico_anomalie=&output_anomalie_dlp);
		%end;

	/*Join Anagrafica*/
	proc sql;
		create table &prefix.dlp_metanodotti_anagrafica as
		select t1.*
			 , t2.Area_ID
			 , t2.Area_Descrizione
			 , t2.Area_Label
		from &prefix.dati_linepack_vw_ffill as t1 
		inner join &anagrafica_dlp(where=(compress(fl_deleted)="0")) as t2 
		on (compress(t1.metanodotto)=compress(t2.metanodotto)) and 
		   (compress(t1.area_prelievo)=compress(t2.area_prelievo))
		;
	quit;

	proc sql;
		create table &ds_output as
		select distinct data_ora_rif as datetime_solare
			 , giorno_gas as data_gas
			 , area_id
			 , area_descrizione
			 , area_label
			 , sum(delta_linepack) as dlp
			 , sum(linepack) as linepack
		from &prefix.dlp_metanodotti_anagrafica  
		group by 1,3
		order by area_id, data_ora_rif
		;
	quit;

%mend deltalinepack;
%macro transiti(g_gas=, g1_solare=, shift_type=, shift_value=, input_calendario_dates=,
				ds_in=, anag_in=, input_transiti=, ds_output=);

/*	%let g_gas=&g_gas; */
/*	%let g1_solare=&g1_solare;*/
/*	%let shift_type=month;*/
/*	%let shift_value=42;*/
/*	%let ds_in=&input_transiti;*/
/*	%let anag_in=&input_anag_transiti;*/
/*	%let input_transiti=&output_transiti_entryexit;*/
/*	%let ds_output=&output_transiti;*/
/*	%let input_calendario_dates=calendar_dates;*/


	%set_startdates_calendar(g_gas=&g_gas, g1_solare=&g1_solare, shift_type=&shift_type, shift_value=&shift_value, 
						out_calendar=&input_calendario_dates);

	proc sql noprint;
		select distinct id_scada into: list_id
		separated by " "
		from &anag_in (where=(fl_deleted = "0"))
		;
	quit;

	/****/

	proc sort data=&anag_in(where=(fl_deleted = "0")) out=anagrafica_transiti_nodup nodupkey;
		by id_scada;
	run;

	proc sql;
		create table cross_join_trs as 
		select * 
		from (anagrafica_transiti_nodup(keep=id_scada sottotipo_elemento_gas) 
		cross join &input_calendario_dates (keep=datetime_solare))
		order by id_scada, datetime_solare;
	quit;

	proc sort data=&input_transiti(where=(id_scada in (&list_id))) out=trs_raw;
		by id_scada data_ora_rif;
	run;

	data trs_tmp;
		length valore_string $ 40;
		set trs_raw;
		valore_clean_tmp=scan(valore,1,'**');
		valore_string = scan(valore,2,'**');
		rename data_ora_rif=datetime_solare;
	run;

	data trs_tmp_merge;
		merge cross_join_trs(in=L) trs_tmp(in=R);
		by id_scada datetime_solare;
		if L;
	run;

	data trs_ffill;
		length temp_val $ 40;
		set trs_tmp_merge;
		by id_scada;
		flag_ffill = 0;
		/* RETAIN the new variable */
		retain temp_val; 

		/* Reset TEMP when the BY-Group changes */
		if first.id_scada then temp_val="";

		/* Assign TEMP when valore_clean_tmp is non-missing */
		if not missing(valore_clean_tmp) then temp_val=valore_clean_tmp;

		/* When valore_clean_tmp is missing, assign the retained value of TEMP into X */
		else if missing(valore_clean_tmp) then 
			do;
				valore_clean_tmp=temp_val;
				flag_ffill = 1;
			end;
		/* Se ho ancora dei missing sono all'inizio dello storico di ciascun
			misuratore. Questo significa che il misuratore non era ancora esistente.*/
		if missing(valore_clean_tmp) then
			do;
				flag_ffill = 0; * non ? quindi un caso di ffill;
				if id_scada = 30155316 then valore_clean_tmp="4";
				else valore_clean_tmp="0";
			end;
	run;
	
	%if &flg_anomalie=1 %then
		%do;
			%put ===== Storicizzazione Anomalie Transiti =====;

			%gestione_storico_anomalie_ffill(ds_in=trs_ffill, 
			var_2count=id_scada, count_text=count_transiti_missing, dt_riferimento=datetime_solare,
			storico_anomalie=&output_anomalie_transiti);
		%end;
	

	data trs_tmp_2;
		length sottotipo_key $10;
		set trs_ffill;
		if index(lowcase(sottotipo_elemento_gas), "portata") then sottotipo_key = "trs";
		else if index(lowcase(sottotipo_elemento_gas), "pressione") then sottotipo_key = "prs";
		else if index(lowcase(sottotipo_elemento_gas), "valvole") then sottotipo_key = "sva";
	run;

	proc sort data = trs_tmp_2;
		by datetime_solare;
	run;

	proc transpose data=trs_tmp_2 delimiter=_ out=trs_tmp_2_t;
		by datetime_solare;
		id sottotipo_key id_scada ;
		var valore_clean_tmp;
	run;

	proc contents data=TRS_TMP_2_T 
		out=cont (keep=name where=(index(lowcase(name),"trs") or index(lowcase(name),"prs") )) noprint;
	run;

	proc sql noprint;
		select cat(compress(name), "num") 
			, name 
			into: savelist separated by " "
			, : droplist separated by " "
		from cont
		;
	quit;
		
	data trs_tmp_t_ren(drop=j &droplist);
		set	TRS_TMP_2_T;
		array new &savelist;
		array old &droplist;
		do j=1 to dim(new);
			new(j)=input(old(j), $8.);
		end;
	run;

	proc contents data=trs_tmp_t_ren 
		out=cont (keep=name where=(index(lowcase(name),"trs") or index(lowcase(name),"prs") )) noprint;
	run;

	proc sql noprint;
		select cat("rename ", compress(name), "=", tranwrd(compress(name), "num", ""), ";")
			into: rename_statement separated by " "
		from cont;
	quit;

	data trs_tmp_t_v2;
		set trs_tmp_t_ren;
		&rename_statement;
	run;

	data trs_tmp_t_v2;
		set trs_tmp_t_v2;
		prs_delta = abs(prs_30087076 - prs_30087082);
		if datetime_solare <= "12MAR2020:11:00:00"dt and datetime_solare >= "07SEP2019:04:00:00"dt 
			then trs_30144054_new = - trs_30144054; * solo fino al 12 Mar 2020 ore 11 comprese, 
			poi ok il suo segno;
		else trs_30144054_new = trs_30144054;
		retain trs_2825_bp;
		flag_mortara = 0;
		if sva_30155316 = "8" then sva_30155316 = "1";
		else if sva_30155316 = "2" then sva_30155316 = "4";
		if sva_30155316 = "1" then flag_mortara = 1;
		if flag_mortara = 0 then do;
			trs_2825_bp = trs_2825;
			trs_2825_2 = trs_2825;
		end;
		else if flag_mortara = 1 then do;
/*				trs_2825_bp = trs_2825;*/
				trs_2825_2 = trs_2825_bp;
		end;
		drop trs_2825_bp trs_2825 trs_30144054;
	run;

	proc transpose data=trs_tmp_t_v2 out=trs_tmp_t_vert;
		var trs:;
		by datetime_solare;
	run;

	data trs_t_vert1 trs_t_vert2;
		set trs_tmp_t_vert;
/*		id_scada = put(scan(_NAME_, 2, "_"), 16.);*/
		id_scada_int = scan(_NAME_, 2, "_");
		id_scada = input(id_scada_int, 16.);
		rename valore_clean_tmp=trs;
		drop _name_ id_scada_int;
	run;

	data trs_t_vert1;
		set trs_t_vert1;
		segno_gas = "P";
	run;

	data trs_t_vert2;
		set trs_t_vert2;
		segno_gas = "N";
	run;

	data trs_double;
		set trs_t_vert1 trs_t_vert2;
	run;

	data anagrafica_trs_full;
		set &input_anag_transiti;
		where fl_deleted = "0";
		keep id_scada area_id area_label transito segno_gas;
	run;

	proc sort data=anagrafica_trs_full;
		by id_scada segno_gas;
	run;

	proc sort data=trs_double;
		by id_scada segno_gas;
	run;

	data trs_double_all;
		merge trs_double(in=L) anagrafica_trs_full(in=R);
		by id_scada segno_gas;
		if L;
		if segno_gas = "N" then trs_new = - trs;
		else if segno_gas = "P" then trs_new = trs;
	run;

	proc sort data=trs_double_all;
		by datetime_solare Area_ID;
	run;

	proc means data=trs_double_all noprint;
		by datetime_solare Area_ID Area_Label;
		var trs_new;
		output out=transiti_aree(drop=_TYPE_ _FREQ_ rename=(trs_new=trs)) 
		sum=;
	run;

	proc sort data=transiti_aree out=&ds_output;
		by area_id datetime_solare;
	run;

	/* FFILL finale e aggiusto missing nel data_gas */
/*	data &ds_output (drop=trs_new);*/
/*		set &ds_output;*/
/*		by area_id datetime_solare;*/
/*		retain trs_new;*/
/*		if first.area_id then trs_new=trs;*/
/*		else do;*/
/*			if trs ne . then */
/*				trs_new=trs;*/
/*			else if trs = . then */
/*				trs=trs_new;*/
/*		end;*/
/*	run;*/

	data &ds_output;
		set &ds_output;
		hour=hour(datetime_solare);
		data_gas=ifn(hour(datetime_solare)<6, 
		intnx("day", datepart(datetime_solare), -1), datepart(datetime_solare));
		format data_gas date9.;
		trs = round(trs, 0.001);
	run;

	/* Sanity check */
	proc sql;
		create table sanity_check as 
		select datetime_solare
				,sum(trs)  as trs_bil
		from transiti_aree
		group by datetime_solare
		having trs_bil >= 0.0000001;
	quit;

%mend transiti;

%macro bilancio(geo_bilancio=, shift_type=, shift_value=,  geo_target=, input_trs=, input_dlp=,
				input_entryexit=, g_gas=, g1_solare=,input_geo_ricorario_imputato=, 
				output_bilancio_h_lg_mvg=, output_bilancio_d_lg_mvg=);

/*%let geo_bilancio=geo_bilancio;*/
/*%let shift_type_d=month;*/
/*%let shift_value_d=42;*/
/*%let geo_target=&geo_target;*/
/*%let input_trs=&output_transiti;*/
/*%let input_dlp=&output_dlp;*/
/*%let input_entryexit=&output_entry_exit;*/
/*%let g_gas=&g_gas;*/
/*%let g1_solare=&g1_solare;*/
/*%let geo_imputazione_ricorario=for_svil.geo_ricorario_imputed_new */
/*%let output_bilancio_h_lg_mvg=&output_bilancio_hourly;*/
/*%let output_bilancio_d_lg_mvg=&output_bilancio_daily;*/

	/* Merge transiti, dlp, entryexit and create ric */
	proc sort data=&input_entryexit;
		by area_id datetime_solare;
	run;

	proc sort data=&input_dlp;
		by area_id datetime_solare;
	run;

	proc sort data=&input_trs;
		by area_id datetime_solare;
	run;

	data geo_bilancio_short;
		merge &input_entryexit &input_dlp &input_trs;
		by area_id datetime_solare;
		stc = - ims + prs;
		ric = imp + trs + pnl - exp + stc - dlp;
		/**/
		immissione_totale = imp + pnl + prs;

		ric_notimputed=ric;
	run;

	proc sql noprint;
		select min(datetime_solare) format datetime19. into: min_dt_bilancio
		from geo_bilancio_short
		;
	quit;
	
/* 	%put &=min_dt_bilancio; */

	data _null_;
		min_dt_bilancio="&min_dt_bilancio"dt;
		if min_dt_bilancio lt "02JUL2019:06:00:00"dt then flg_recupero_storico=1;
		else flg_recupero_storico=0;
		format min_dt_bilancio datetime19.;
		call symput("flg_recupero_storico",flg_recupero_storico);
	run;

	data _null_;
		dt_solare_bil = intnx('hour', &dt_solare., -1);
		format dt_solare_bil datetime19.;
		call symput("dt_solare_bil",dt_solare_bil);
	run;

	%if &flg_recupero_storico=1 %then %do;

		%put Recupero lo storico col target imputato;
		/* Recupero lo storico*/
		proc sort data=&input_geo_ricorario_imputato.
			(where=(datetime_solare lt "02JUL2019:06:00:00"dt))
			out=geo_ricorario_imputed_new;
			by area_id datetime_solare;
		run;

		data geo_bilancio_short;
			merge geo_bilancio_short
				geo_ricorario_imputed_new;
			by area_id datetime_solare;
			ric_notimputed=ric;
/*			stc = - ims + prs;*/
/*			ric_notimputed = imp + trs + pnl - exp + stc - dlp;*/
			if ric_notimputed=. then ric = ric_imputed;
			else ric = ric_notimputed;
		run;

		data geo_bilancio_short;
			set geo_bilancio_short;
			if dlp =. then do;
				dlp = imp + trs + pnl - exp + stc - ric;
			end;
		run;
		/* se geo bilancio non esiste allora la creo */
		%if  %sysfunc(exist(&geo_bilancio)) %then %do;
		
			data &geo_bilancio(where=(datetime_solare le &dt_solare_bil));
				merge &geo_bilancio(where=(datetime_solare lt "&min_dt_bilancio"dt)) 
						geo_bilancio_short;
				by area_id datetime_solare;
			run;

		%end;
		
		/* caso particolare -> possiamo usare una data di comodo al posto della min_dt_bilancio */
		%else %do;
		
			data &geo_bilancio (where=(datetime_solare le &dt_solare_bil));
				set geo_bilancio_short;
			run;
			
		%end;
		
	%end;

	%else %do;
	/*solo se fa riga di input	 */
	%if &flg_trainingset=0 %then %do;
	/* Refresh ultimi n giorni di for_svil.geo_bilancio */
		data &geo_bilancio(where=(datetime_solare le &dt_solare_bil));
			merge &geo_bilancio(where=(datetime_solare lt "&min_dt_bilancio"dt)) 
					geo_bilancio_short;
			by area_id datetime_solare;
		run;
	%end;
	
	%end;

 	proc sort data=&geo_bilancio;
	by area_id data_gas datetime_solare;
	run;

	data &geo_bilancio;
		set &geo_bilancio(drop=dlp_cumday);
		by area_id data_gas;
		retain dlp_cumday;

		if first.area_id or first.data_gas then
			dlp_cumday = dlp;
		else dlp_cumday + dlp;
	run;
	
	proc sort data =&geo_bilancio;
	by area_id datetime_solare;
	run;
	
	/* FFill riconsegnato e bonifica valori<0 */
	/* Aggiungere flag ffill?	 */
	data geo_bilancio_ffill (rename=(valore_clean=ric));
		set &geo_bilancio(drop=linepack dlp_cumday immissione_totale);
		by area_id;
		valore_clean=ric;
		flag_ffill=0;
		retain temp_val;
		if first.area_id then temp_val=.;
		if valore_clean >0 then temp_val=valore_clean;
		else if valore_clean le 0 then do;
			flag_ffill=1;
			valore_clean=temp_val;
		end;
		drop ric temp_val;
	run;
	
	
	%if &flg_anomalie=1 %then
		%do;
			%put ===== Storicizzazione Anomalie Bilancio =====;

			%gestione_storico_anomalie_ffill(ds_in=geo_bilancio_ffill, 
			var_2count=ric,count_text=count_bilancio_negativi, dt_riferimento=datetime_solare,
			storico_anomalie=&output_anomalie_bilancio);
		%end;

	/* Target giornaliero --> Devo costruirlo nella for_svil? meglio con procedura di append..
	 */
	proc means data=geo_bilancio_ffill (where=(data_gas lt &g_gas)) noprint;
		by area_id area_label data_gas;
		var ric;
		output out=&geo_target (drop=_type_ _freq_ 
				rename=(ric=tgt_riconsegnato_totale data_gas=data_gas_rif)) sum=;
	run;

	/* Prendo da geo_bilancio_short (mi basta questo storico perché lag massimo è di 1gg) */
	%bilancio_hourlyprocess(ds_in=geo_bilancio_short, ds_out=&output_bilancio_h_lg_mvg);
	
	/* Prendo solo 1 anno rolling (si posiziona sempre all'inizio del mese..) */
	%set_startdates_calendar(g_gas=&g_gas, g1_solare=&g1_solare, shift_type=&shift_type, 
							shift_value=&shift_value, out_calendar=calendar_d);

	%bilancio_dailyprocess(ds_in=geo_bilancio_ffill(where=(datetime_solare ge "&start_datetime_char"dt)), 
						   ds_out=&output_bilancio_d_lg_mvg);

%mend bilancio;

%macro bilancio_hourlyprocess(ds_in=, ds_out=);
/**/
/*%let ds_in=for_svil.geo_bilancio;*/
/*%let ds_out=for_svil.geo_output_bilancio_h_train;*/

	data geo_bilancio_hour;
		set &ds_in /*(where =(datetime_solare <= "08JUL2020:06:00:00"dt and 
				datetime_solare >= "01JAN2017:06:00:00"dt))*/;
		/*add 1h to datetime_solare */
		datetime_gas_shift = intnx("hour", datetime_solare, 1);
		data_gas_shift = ifn(hour(datetime_gas_shift)<6,
					intnx("day", datepart(datetime_gas_shift), -1),
					datepart(datetime_gas_shift));
		format data_gas_shift date9. datetime_gas_shift datetime18.;
	run;

	proc contents data=geo_bilancio_hour (keep=dlp exp imp pnl ric stc trs)
		out=cont(keep=name) noprint;
	run;

	proc sql noprint;
		select distinct cat("convert ",compress(name),"=scada_bil_",compress(name),"_mean / transformout=(cuave);")
			 , cat("convert ",compress(name),"=scada_bil_",compress(name),"_std / transformout=(custd);")
			 , cat("convert ",compress(name),"=scada_bil_",compress(name),"_max / transformout=(cumax);")
			 , cat("convert ",compress(name),"=scada_bil_",compress(name),"_min / transformout=(cumin);")
			 , cat("convert ",compress(name),"=scada_bil_",compress(name),"_lag24 / transformout=(lag 23);")
			 , cat("convert ",compress(name),"=scada_bil_",compress(name),"_smooth / transformout=(movave 2);")
			 , cat("convert ",compress(name),"=scada_bil_",compress(name),"_sum24 / transformout=(movsum 23);")
			 , compress(cat(compress(name),"=","scada_bil_",compress(name)))
		into: cuvave separated by " "
			,: custd separated by " "
			,: cumax separated by " "
			,: cumin separated by " "
			,: lg24 separated by " "
			,: mvg2 separated by " "
			,: movsum24 separated by " "
			,: final_rename separated by " "
		from cont;
	quit;

	proc sort data=geo_bilancio_hour;
		by area_id data_gas_shift;
	run;

	proc expand data=geo_bilancio_hour out=tab_eng (drop=time);
		by area_id data_gas_shift;
		&cuvave &custd &cumax &cumin;
	run;

	proc expand data=tab_eng out=tab_trasf;
		by area_id;
		id datetime_gas_shift;
		&lg24 &mvg2 &movsum24;
	run;

	proc sort data=tab_trasf (rename=(&final_rename));
		by area_id data_gas_shift;
	run;

	proc sql noprint;
		select distinct cat("scada_bil_",compress(name),"_cumday")
			, cat("scada_bil_",compress(name))
			, cat("scada_bil_",compress(name),"_mean")
			, cat("scada_bil_",compress(name),"_std")
			, cat("scada_bil_",compress(name),"_max")
			, cat("scada_bil_",compress(name),"_min")
			, cat("scada_bil_",compress(name),"_smooth")
			, cat("scada_bil_",compress(name),"_sum24")
			into: cumday_vars separated by " "
			   ,: scada_bil_vars separated by " "
			   ,: mean_vars separated by " "
			   ,: std_vars separated by " "
			   ,: max_vars separated by " "
			   ,: min_vars separated by " "
			   ,: smooth_vars separated by " "
			   ,: sum24_vars separated by " "
		from cont;
	quit;

	data tab_trasf;
		set tab_trasf;
		by area_id data_gas_shift;
		retain &cumday_vars;
		array bil{*} &scada_bil_vars;
		array cum{*} &cumday_vars;
		do i=1 to dim(bil);
			if first.data_gas_shift then cum[i]=0;
			else cum[i] + bil[i];
		end;
		drop i;
	run;

	data geo_bilancio_hour_shift;
		set tab_trasf;
		array meanv{*} &mean_vars;
		array stdv{*} &std_vars;
		array maxv{*} &max_vars;
		array minv{*} &min_vars;
		array smoothv{*} &smooth_vars;
		array sum24v{*} &sum24_vars;
		do i=1 to dim(meanv);
			if meanv[i] gt -0.0001 and meanv[i] lt 0.0001 then meanv[i]=0;
			if stdv[i] gt -0.0001 and stdv[i] lt 0.0001 then stdv[i]=0;
			if maxv[i] gt -0.0001 and maxv[i] lt 0.0001 then maxv[i]=0;
			if minv[i] gt -0.0001 and minv[i] lt 0.0001 then minv[i]=0;
			if smoothv[i] gt -0.0001 and smoothv[i] lt 0.0001 then smoothv[i]=0;
			if sum24v[i] gt -0.0001 and sum24v[i] lt 0.0001 then sum24v[i]=0;
		end;
		drop i;
	run;

	data geo_bilancio_hour_g geo_bilancio_hour_g1;
		set geo_bilancio_hour_shift (keep=area: data_gas_shift datetime_gas_shift scada_bil:);
		length modello $3.;
		do key_modello = 0 to 1;
			if key_modello = 0 then do;
				data_gas_rif=intnx("day", data_gas_shift, 0);
				datetime_riferimento_dati = dhms(data_gas_rif,0,0,0);
				modello="G";
				format datetime_riferimento_dati datetime19. 
						data_gas_rif  date9.;
				output geo_bilancio_hour_g;
			end;
			else if key_modello = 1 then do;
				data_gas_rif = intnx("day", data_gas_shift, 1);
				datetime_riferimento_dati = dhms(data_gas_rif,0,0,0);
				modello = "G1";
				format datetime_riferimento_dati datetime19. 
						data_gas_rif  date9.;
				output geo_bilancio_hour_g1;
			end;
		end;
		rename datetime_gas_shift=datetime_solare data_gas_shift=data_gas;
	run;
				
	data &ds_out;
		retain area_id area_label key_modello modello datetime_solare data_gas 
			data_gas_rif datetime_riferimento_dati;
		set geo_bilancio_hour_g geo_bilancio_hour_g1;
	run;

	proc sort data=&ds_out;
		by area_id datetime_riferimento_dati key_modello;
	run;


%mend bilancio_hourlyprocess;


%macro bilancio_dailyprocess(ds_in=, ds_out=, g_gas=);

/*%let ds_in=&geo_bilancio;*/
/*%let ds_out=&output_bilancio_daily;*/

	data geo_bilancio_cut;
		set &ds_in;
		where data_gas <= intnx("day", &g_gas, -1);
	run;

	proc means data=geo_bilancio_cut noprint;
		by area_id area_label data_gas;
		var ric trs imp pnl stc dlp exp;
		output out=riconsegnato_daily (drop=_type_ _freq_) sum=;
	run;

	data riconsegnato_daily ;
		set riconsegnato_daily /*(where=( data_gas_shift le "09JUL2020"d))*/;
		/*add 1h to data_gas */
		data_gas_shift = intnx("day", data_gas, 1);
		format data_gas_shift date9.;
	run;

	proc contents data=riconsegnato_daily (drop=area: dat:) 
		out=vars (keep=name) noprint;
	run;

	data x_lag;
		set vars;
		length pfx $5;
		if name ne "ric" then do;
			pfx = "bil_";
			name_new = cat(compress(name), "_");
		end;
		else do;
			pfx = "rtot_";
			name_new = "";
		end;
		do x=1 to 13;
			output;
		end;
		do x=362 to 365;
			output;
		end;
	run;

	data x_lag (keep=name: x_str d_str pfx);
		set x_lag;
		x_str = put(x, $3.);
		d_str = put(x+1, $3.);
	run;

	proc sql noprint;
		select distinct cat("convert ", compress(name) ," = ", compress(pfx), "lag_", compress(name_new),
				compress(d_str), "/ transformout=(lag ", compress(x_str), ");") 
		into:lg separated by ' ' 
		from x_lag;
	quit;

	proc expand data=riconsegnato_daily
		out=riconsegnato_daily_lg (drop=time);
		by area_id area_label;
		id data_gas_shift;
		&lg;
	run;

	proc contents data=riconsegnato_daily_lg (keep=dlp exp imp pnl stc trs) out=cont(keep=name) noprint;
	run;

	proc sql noprint;
		select compress(cat(name,"=","bil_lag_",name,"_1")) into: rename_list
			separated by " "
		from cont
		;
	quit;

	data riconsegnato_daily_lg_rn;
		set riconsegnato_daily_lg;
		rename ric = rtot_lag_1 &rename_list;
	run;

	/* mvg sul target */	
	data x_mvg (keep=x_str);
		do x=2 to 14;
			x_str=put(x,$3.);
			output;
		end;
	run;

	proc sql noprint;
		select cat("rtot_mvg_", compress(x_str) ," = ", "mean(of rtot_lag_1 - ", 
		"rtot_lag_", compress(x_str), ");")
		into :mvgavg separated by ' ' 
		from x_mvg;
	quit;

	data riconsegnato_daily_lg_mvg;
		set riconsegnato_daily_lg_rn;
		&mvgavg;
	run;

	/* FFILL */
	proc contents data=riconsegnato_daily_lg_mvg (keep=bil_lag: rtot_:)
		out=cont(keep=name) noprint;
	run;

	proc sql noprint;
		select distinct name 
			  , cat(compress(name),"old") 
			into: droplist separated by " " 
				, :savelist separated by " "
		from cont;
	quit;

	proc sort data=riconsegnato_daily_lg_mvg;
		by area_id;
	run;

	data riconsegnato_daily_lg_mvg_ff (drop= i j &savelist);
		set	riconsegnato_daily_lg_mvg;
		array old &droplist;
		array bkp &savelist;
		retain old;
		by area_id;
		if first.area_id then do;
			do i=1 to dim(old);
				bkp(i)=old(i);
			end;
		end;
		else do;
			do j=1 to dim(old);
				if not missing(old(j)) then bkp(j)=old(j);				
				else if missing(old(j)) then old(j)=bkp(j);
			end;
		end;
	run;

	/* Aggiungo record per ogni modello (G e G1) */
	data geo_bilancio_g geo_bilancio_g1;
		length modello $3;
		set riconsegnato_daily_lg_mvg_ff (drop=data_gas);
		do key_modello = 0 to 1;
			if key_modello = 0 then do;
				data_gas_rif= intnx("day", data_gas_shift, key_modello);
				datetime_riferimento_dati = dhms(data_gas_rif, 0,0,0);
				modello = "G";
				format datetime_riferimento_dati datetime19. 
				data_gas_rif date9.;
				output geo_bilancio_g;
			end;
			else if key_modello = 1 then do;
				data_gas_rif = intnx("day", data_gas_shift, key_modello);
				datetime_riferimento_dati = dhms(data_gas_rif, 0,0,0);
				modello = "G1";
				format datetime_riferimento_dati datetime19. 
				data_gas_rif date9.;
				output geo_bilancio_g1;
			end;
		end;
		rename data_gas_shift=data_gas;
	run;

	data &ds_out;
		retain key_modello modello area_id area_label data_gas 
		data_gas_rif datetime_riferimento_dati;
		set geo_bilancio_g geo_bilancio_g1;
	run;

	proc sort data=&ds_out;
		by area_id data_gas key_modello;
	run;

%mend bilancio_dailyprocess;

%macro round_input_vars(dset, vars);

	data &dset(drop=i);
		set &dset;
		array FIX_LIST {*} &vars;
		do i =1 to dim(FIX_LIST);
			FIX_LIST[i]=round(FIX_LIST[i],0.00001);
		end;
	run;

%mend;


%macro merge_components(flg_trainingset=, g_gas=, g1_solare=, shift_type=, shift_value=,
					in_calendario=, in_scadacons=, in_meteocelle=, in_meteocitta=,
					in_terna_cons=, in_terna_prev=, in_bil_d=, in_bil_h=, in_target=, output_dset=,
					output_dset_cut=, output_dset_base=);

/*%let g_gas=&g_gas;*/
/*%let g1_solare=&g1_solare;*/
/*%let shift_type=month;*/
/*%let shift_value=44;*/
/*%let in_calendario=&output_geo_calendar;*/
/*%let in_scadacons=&output_scadacons;*/
/*%let in_meteocelle=&output_celle;*/
/*%let in_meteocitta=&output_meteocitta;*/
/*%let in_terna_cons=&output_consuntivo_terna_aree;*/
/*%let in_terna_prev=&output_previsione_terna_aree;*/
/*%let in_bil_d=&output_bilancio_daily;*/
/*%let in_bil_h=&output_bilancio_hourly;*/
/*%let in_target=&geo_target;*/
/*%let output_dset=for_svil.geo_trainingset;*/

	%set_startdates_calendar(g_gas=&g_gas, g1_solare=&g1_solare, shift_type=&shift_type, 
							 shift_value=&shift_value, out_calendar=calendar);

	/* Calendario */
	proc sort data=&in_calendario(where=(datetime_solare ge "&start_datetime_char"dt))
		out=in_calendario;
		by key_modello area_id datetime_solare;
	run;

	/* ScadaCons*/
	proc sort data=&in_scadacons(where=(datetime_solare ge "&start_datetime_char"dt))
		out=in_scadacons;
		by key_modello area_id datetime_solare;
	run;

	/* Meteo citt? */
	proc sort data=&in_meteocitta(where=(datetime_solare ge "&start_datetime_char"dt))
		out=in_meteocitta;
		by key_modello area_id datetime_solare;
	run;

	/* Meteo celle */
	proc sort data=&in_meteocelle(where=(datetime_solare ge "&start_datetime_char"dt))
		out=in_meteocelle;
		by key_modello area_id datetime_solare;
	run;

	/* Terna cons */
	proc sort data=&in_terna_cons(where=(datetime_solare ge "&start_datetime_char"dt))
		out=in_terna_cons;
		by key_modello area_id datetime_solare;
	run;

	/* Terna prev */
	proc sort data=&in_terna_prev(where=(datetime_solare ge "&start_datetime_char"dt))
		out=in_terna_prev;
		by key_modello area_id datetime_solare;
	run;

	/* Bilancio orario */
	proc sort data=&in_bil_h(where=(datetime_solare ge "&start_datetime_char"dt))
		out=in_bil_h;
		by key_modello area_id datetime_solare;
	run;

	/*Bilancio giornaliero */
	proc sort data=&in_bil_d(where=(data_gas ge "&start_date_char"d))
		out=in_bil_d;
		by key_modello area_id data_gas;
	run;

	/* Merge all */
	data merge_1;
		merge in_calendario in_scadacons in_meteocitta
				in_meteocelle in_terna_cons in_terna_prev in_bil_h;
		by key_modello area_id datetime_solare;
		b_imputed = 0;
		if datetime_solare < "02JUL2019:00:00:00"dt then b_imputed = 1;
	run;

	/* Add bil daily lag */
	proc sort data=merge_1;
		by key_modello area_id data_gas;
	run;

	data merge_2;
		merge merge_1 in_bil_d;
		by key_modello area_id data_gas;
	run;

	/* Se devo creare il trainingset inserisco anche il target + check duplicati + append ultimi mesi con cut_date
		opportuna */
	%if &flg_trainingset=1 %then %do;

		%put Aggiungo target giornaliero;

		proc sort data=merge_2;
			by area_id data_gas_rif;
		run;

		proc sort data=&geo_target out=geo_target;
			by area_id data_gas_rif;
		run;

		data merge_final;
			merge merge_2(in=L) geo_target(in=R);
			by area_id data_gas_rif;
			if R;
		run;

		%round_input_vars(merge_final, bil: cel: rtot: scada: terna: urb:);

		%if &dataset_presente=1 %then %do;

				data &output_dset_cut(where=(data_gas>=&cut_date));
					set merge_final;
				run;

				data scarto_add;
					length datetime_calcolo_train 8;
					datetime_calcolo_train=&dt_solare;
					set &output_dset_base(where=(data_gas>=&cut_date));
					format datetime_calcolo_train datetime19.;
				run;

				proc append base=&output_dset_base data=scarto_add force;
				run;

				data &output_dset;
					set &output_dset_base(where=(data_gas<&cut_date)) &output_dset_cut;
				run;

		%end;

		%else %if &dataset_presente=0 %then %do;

				data &output_dset;
					set merge_final;
				run;

		%end;

	%end;

	/* Se devo creare riga di input filtro per hour corrente e data_gas_rif=&g_gas e appendo */

	%else %do;
	
		data merge_final;
			set merge_2;
			if data_gas=&g_gas and hour=hour(&dt_solare);
		run;

		/* Arrotondamento finale*/
		%round_input_vars(merge_final, bil: cel: rtot: scada: terna: urb:);

		%if &dataset_presente=1 %then %do;

			proc append base=&output_dset data=merge_final force;
			run;

		%end;
		%else %do;

			data &output_dset;
				set merge_final;
			run;

		%end;
	%end;



%mend merge_components;

%macro dset_phy2mem(dset_name=, lib_phy=, lib_mem=, TDAY_GAS=);
	%checkds(&lib_mem..&dset_name);

	%if &dataset_presente=1 %then
		%do;
			%PUT DELETING &dset_name IN &lib_mem;

			proc casutil;
				DROPTABLE casdata="&dset_name" incaslib="&lib_mem";
			run;

		%end;

	data load;
		set &lib_phy..&dset_name(where=(data_gas_rif>=intnx("month", &TDAY_GAS, -59, 'B')));
	run;

	%PUT LOADING &dset_name IN &lib_mem;

	proc casutil;
		load data=work.load outcaslib="&lib_mem" casout="&dset_name" promote;
	quit;

%mend;

%macro remove_dupp_tset(dataset=, output=, dupp=, key_vars=);

	proc sort data=&dataset out=&output nodupkey dupout=&dupp;
		by &key_vars;
	run;

	proc sql noprint;
		select count(*) into :n_dupp
			from &dupp;
	quit;

	%put IL DATASET %dataset CONTENEVA %sysfunc(compress(&n_dupp)) DUPLICATI;
%mend;
