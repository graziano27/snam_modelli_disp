
/* options nomlogic nomprint nosymbolgen; */
/* options mlogic mprint symbolgen msglevel=i; */
/* ods exclude all; */
/*ods exclude none;*/

/*
estive g --> 11 17
invernali g --> 6 12 18
estive g1 --> 5 21
invernali g1 --> 22

*/


/*libname for_svil "/sasdata/modelli_geo/deploy_test";*/
/*%let start="15JUL2020:21:00:00"dt;*/
/*%let lib_physical=for_svil;*/
/*%let geo_output_final_name=geo_inputmodelli;*/
/**/
/*%let lib_path=/sasdata/modelli_geo/deploy_test;*/


/*============ Macro variabili =================================*/
/* Definizione macro variabili Globali */
%let dt_solare=&start;
%let libreria_input=&lib_physical;
%let libreria_output=&lib_physical;
%let libreria_inmemory=casuser;

%let name_grid=geo_configurazioni;
%let name_grid_inm=configurazioni_tmp;

%let name_input = &geo_output_final_name;
%let physic_input = &libreria_input..&name_input;
%let physic_grid = &libreria_input..&name_grid;
%let inmemory_name = riga_di_input;
%let inmemory_input = &libreria_inmemory..&inmemory_name;
%let inmemory_grid = &libreria_inmemory..&name_grid_inm;
%let time_id = data_gas_rif;
%let model_path=&lib_path; * dichiarato nel prevedi_viya;
%let ext = .txt;


%let output_name = geo_previsioni_riconsegnato;
%let output_prev = &libreria_output..&output_name;

/* Prendo l'ultima riga del dataset di input */
proc sql;
	select max(datetime_solare) format datetime19. into: max_dt
	from &physic_input
	;
quit;

proc sort data=&physic_input (where=(datetime_solare="&max_dt"dt)) 
	out=&inmemory_input;
	by datetime_solare key_modello area_id;
run;


/*============ Date per i modelli =================================*/
/* Seleziono l'ora con cui selezionare il modello da utilizzare */

data date_to_pred;
	dt_now =&dt_solare;
	date_now = datepart(dt_now);
	hour_now = hour(dt_now);
	data_gas_now = ifn(hour_now<6, intnx('day', date_now, -1), date_now);
	hour_pred = hour_now;
	/* Modello G */
	date_pred_g = data_gas_now;
	mese_pred_g = month(date_pred_g);
	anno_pred_g = year(date_pred_g);
	mese_prec_g = month(intnx('month',date_pred_g, -1));
	anno_prec_g = year(intnx('month',date_pred_g, -1));
	anno_prec2_g = year(intnx('month',date_pred_g, -2));
	mese_prec2_g = month(intnx('month',date_pred_g, -2));
	/* Modello G1 */
	date_pred_g1 = intnx("day",data_gas_now,1);
	mese_pred_g1 = month(date_pred_g1);
	anno_pred_g1 = year(date_pred_g1);
	mese_prec_g1 = month(intnx('month',date_pred_g1, -1));
	anno_prec_g1 = year(intnx('month',date_pred_g1, -1));
	anno_prec2_g1 = year(intnx('month',date_pred_g1, -2));
	mese_prec2_g1 = month(intnx('month',date_pred_g1, -2));
	/* Variables */
	call symputx("hour_pred", put(hour_pred, z2.0));
	/* Modello G */
	call symputx("mese_pred_g", put(mese_pred_g, z2.0));
	call symputx("anno_pred_g", anno_pred_g);
	call symputx("date_pred_g", date_pred_g);
	call symputx("mese_prec_g", put(mese_prec_g, z2.0)); 
	call symputx("anno_prec_g", anno_prec_g);
	call symputx("mese_prec2_g", put(mese_prec2_g, z2.0));
	call symputx("anno_prec2_g", anno_prec2_g);
	/* Modello G1 */
	call symputx("mese_pred_g1", put(mese_pred_g1, z2.0));
	call symputx("anno_pred_g1", anno_pred_g1);
	call symputx("date_pred_g1", date_pred_g1);
	call symputx("mese_prec_g1", put(mese_prec_g1, z2.0)); 
	call symputx("anno_prec_g1", anno_prec_g1);
	call symputx("mese_prec2_g1", put(mese_prec2_g1, z2.0));
	call symputx("anno_prec2_g1", anno_prec2_g1);
	format da: date9.;
	format dt: datetime19.;
run;

%macro previsione(hour_pred, area_pred);

/* %let hour_pred=&hour_pred; */
/* %let area_pred=ND; */

	/* Filtro la griglia delle configurazioni per l'ora che voglio prevedere e l'area */
	data &inmemory_grid;
		length base_model_string $18;
		set &physic_grid;
		hour_pred_try="&hour_pred";
		area_pred_try="&area_pred";
		if index(hour_cluster,"&hour_pred") and index(area_cluster,"&area_pred");
		base_model_string = compress(catx('_', approccio, modello, key_area_cluster, approccio_modello, tipo_modello));
	/*		drop nhidden -- force_vars;*/
	run;

	/* model_approach key_area_cluster */
	data _null_;
		set &inmemory_grid;
		call execute ('%NRSTR (%doing_previsione('||base_model_string||', '||key_hour_cluster||', '||approccio||', 
		'||modello||', '||id||'))');
	run;

%mend previsione;

%macro doing_previsione(base_model_string, key_hour_cluster, approccio, modello, id);

	/*============ ESEGUO LA PREVISIONE ===================*/

	data name_model;
		modello="&modello";
		if modello="G" then do;
			anno_pred="&anno_pred_g";
			anno_prec="&anno_prec_g";
			anno_prec2="&anno_prec2_g";
			mese_pred="&mese_pred_g";
			mese_prec="&mese_prec_g";
			mese_prec2="&mese_prec2_g";
		end;
		else do;
			anno_pred="&anno_pred_g1";
			anno_prec="&anno_prec_g1";
			anno_prec2="&anno_prec2_g1";
			mese_pred="&mese_pred_g1";
			mese_prec="&mese_prec_g1";
			mese_prec2="&mese_prec2_g1";
		end;
		geo_model_prefix0=compress(catx("_","&base_model_string", anno_pred, mese_pred, "&key_hour_cluster.&ext"));
		geo_model_prefix1=compress(catx("_","&base_model_string", anno_prec, mese_prec, "&key_hour_cluster.&ext"));
		geo_model_prefix2=compress(catx("_","&base_model_string", anno_prec2, mese_prec2, "&key_hour_cluster.&ext"));
		call symputx("geo_model0", geo_model_prefix0);
		call symputx("geo_model1", geo_model_prefix1);
		call symputx("geo_model2", geo_model_prefix2);
	run;

	%if %sysfunc(fileexist("&model_path./&geo_model0.")) %then %do;
		data _null_;
			call symputx("geo_model_file","&model_path./&geo_model0.");
		run;
	%end;

	%else %do;
			%if %sysfunc(fileexist("&model_path./&geo_model1.")) %then %do;
				data _null_;
					call symputx("geo_model_file","&model_path./&geo_model1.");
				run;
			%end;

			%else %do;
				data _null_;
					call symputx("geo_model_file","&model_path./&geo_model2.");
				run;
			%end;
	%end;

	%put -- Utilizzo modello "&geo_model_file" --;

	data casuser.score_out  (keep=p_tgt_riconsegnato_totale &time_id modello key_modello area_id area_label datetime_solare hour);
		set &inmemory_input(where=(modello="&modello" and area_label="&area_pred"));
		%include "&geo_model_file.";
	run;
	
	/* APPEND DEI RISULTATI IN &libreria_output..&output_prev */ ;
	
	data score_out_complete;
		set casuser.score_out;
		rename p_riconsegnato_totale=previsione;
		id=&id.;
		approccio="&approccio.";
	run;

	%if %sysfunc(exist(&output_prev)) %then %do;

		proc append base=&output_prev data=score_out_complete force;
		run;

	%end;

	%else %do;

		data &output_prev;
			length datetime_solare data_gas_rif hour id 8 modello $2. key_modello 8 approccio $8 area_id 8 area_label $2.     
			p_tgt_riconsegnato_totale 8 ; 
			format datetime_solare datetime19. data_gas_rif date9.; 
			stop;
		run;

		proc append base=&output_prev data=score_out_complete force;
		run;

	%end;
	

%mend doing_previsione;

%previsione(&hour_pred,ND);
%previsione(&hour_pred,NO);
%previsione(&hour_pred,NE);
%previsione(&hour_pred,TI);
%previsione(&hour_pred,CA);
%previsione(&hour_pred,SA);
%previsione(&hour_pred,SC);