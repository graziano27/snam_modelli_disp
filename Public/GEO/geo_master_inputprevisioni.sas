/*===============================================================*/
/***** rilascio 2020-XX-XX - versione v1 *****/
/***** autori: Binda, Gregori, Rizzolo *****/
/***** BIP S.p.a. *****/
/*===============================================================*/


/* Inserire macro clean_all?
 inserire monitoraggio tempi? */



/*============ General Options and Creation of CAS Session ========*/
options casdatalimit=ALL;
cas GeoSession sessopts=(caslib=casuser timeout=31536000 locale="en_US");
caslib _all_ assign;

options mprint mlogic symbolgen linesize=max;
ods exclude all;

/*============ Global Vars ========*/
%global dataset_presente start_date start_date_char start_datetime start_datetime_char;

%let start=%sysfunc(datetime());
/* %let start = '09OCT2020:05:30:00'dt; */
%let lib_physical=for_svil;
%let lib_memory=public;
%let geo_output_final_name=geo_inputmodelli;
%let geo_trainingset=&lib_physical..geo_trainingset;

filename geofcn FILESRVC folderpath="&logic_path" filename='geo_functions.sas';
filename geoinp FILESRVC folderpath="&logic_path" filename='geo_input.sas';
filename geoprv FILESRVC folderpath="&logic_path" filename='geo_previsioni.sas';
filename geoens FILESRVC folderpath="&logic_path" filename='geo_ensemble.sas';
filename geomrg FILESRVC folderpath="&logic_path" filename='geo_merge.sas';


data flg_previsione;
	dt_now=&start;
	hour=hour(dt_now);
	date_now = datepart(dt_now);
	hour = (hour(dt_now));
	if hour<6 then g_gas=intnx("day",date_now,-1);
	else g_gas=date_now;
	if ((nwkdom(5, 1, 3, year(g_gas)) <= g_gas and  
		g_gas< nwkdom(5, 1, 10, year(g_gas)))) then
		do;
		flg_estate=1;
		hour_previsione="(5 11 17 21)";
		if hour in (5 11 17 21) then flg_previsione=1;
		else flg_previsione=0;
	end;
	else if ((g_gas < nwkdom(5, 1, 3, year(g_gas)) or g_gas >= nwkdom(5, 1,10, year(g_gas)))) then
		do;
		flg_estate=0;
		hour_previsione="(6 12 18 22)";
		if hour in (6 12 18 22) then flg_previsione=1;
		else flg_previsione=0;
	end;
	call symputx("flg_previsione",flg_previsione);
	format dt: datetime19. dat: g_gas date9.;
run;


%macro input_prediction(flg_previsione);

	%if %sysfunc(exist(&geo_trainingset)) %then %do;

		%include geofcn;
		%include geoinp;

		%if &flg_previsione=1 %then %do;

			%put -- Eseguo la previsione --;
			%include geoprv;
			%include geoens;
			%include geomrg;		
		%end;
		%else %do;
			%put -- Non eseguo la previsione --;
		%end;

	%end;

%mend input_prediction;

%input_prediction(&flg_previsione);

cas GeoSession terminate; 


