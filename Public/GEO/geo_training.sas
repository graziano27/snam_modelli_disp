/*===============================================================*/
/***** rilascio 2020-XX-XX - versione v1.0 *****/
/***** autori: Binda, Gregori, Rizzolo *****/
/***** BIP S.p.a. *****/
/*===============================================================*/


%let model_path=&lib_path; /* Dichiarato nel geo_faitraining */
%let ext = .txt;
%let lib_input=&lib_physical;
%let lib_input_inm=&lib_memory;
%let lib_output=&lib_physical;
%let input_dset=&geo_output_final_name;
%let sim_dset=casuser.simulation_dataset;
%let grid=geo_configurazioni;
%let zip_path = &model_path;
%let ds_path = &model_path;
%let time_id=data_gas_rif;
%let target=tgt_riconsegnato_totale;
%let output_varsel=geo_varsel;

/*============== DATES ==============*/

proc sql noprint;
	select max(&time_id) into: max_date
	from &lib_input..&input_dset (where=(&target ne .))
	;
quit;

data dates;
	max_data_gas_rif=&max_date;
	mese_val=month(intnx('month', max_data_gas_rif, 1));
	mese_pred=put(mese_val,z2.);
	anno_pred=year(max_data_gas_rif);
	mese_prec=put(month(max_data_gas_rif),z2.);
	anno_prec=year(max_data_gas_rif);
	mese_prec2=put(month(intnx("month",max_data_gas_rif,-1)),z2.);
	anno_prec2=year(intnx("month",max_data_gas_rif,-1));
	call symputx("last_date",cat("'",put(max_data_gas_rif,date9.),"'d"));
	call symputx("mese_val",mese_val);
	call symputx("mese_pred",mese_pred);
	call symputx("anno_pred",anno_pred);
	call symputx("mese_prec",mese_prec);
	call symputx("anno_prec",anno_prec);
	call symputx("mese_prec2",mese_prec2);
	call symputx("anno_prec2",anno_prec2);
	format max_data_gas_rif date9.;
run;



%macro archivia_modelli(model_string=);

/* %let model_string=&geo_model_base; */

	%do previous = 1 %to 2;
	
		data name_model;
			if &previous = 1 then 
				do;
					geo_model_old = compress(catx("_","&model_string","&anno_prec", 
					"&mese_prec", "&key_hour_cluster.&ext"));
					call symputx("geo_model_old", geo_model_old);
				end;
			else if &previous = 2 then 
				do;
					geo_model_old = compress(catx("_","&model_string","&anno_prec2", 
					"&mese_prec2", "&key_hour_cluster.&ext"));
					call symputx("geo_model_old", geo_model_old);
				end;
		run;
	
		%if %sysfunc(fileexist("&model_path./&geo_model_old.")) %then
			%do;
			%put --- ARCHIVIAZIONE MODELLO &geo_model_old;
				%archiviazione(&geo_model_old);
				%clean_models(model=&geo_model_old)
			%end;

	%end;
	
%mend archivia_modelli;


%macro clean_models(model=);

	filename myDir "&model_path./";

	data get;
		did=dopen("myDir");
		filecount=dnum(did);
		do i = 1 to filecount;
			file_ = dread(did, i);
			output;
		end;
		rc=dclose(did);
	run;

	data to_delete;
		set get;
		where file_ contains "&model";
		fname="tempfile";
		rc=filename(fname, cat("&model_path./", compress(file_)));
		rc=fdelete(fname);
	run;

%mend clean_models;

%macro archiviazione(geo_model_old);
	
	filename glob "&ds_path./&geo_model_old";
	filename addfile zip "&zip_path./ds.zip" 
		member="models_geo/&geo_model_old";

	data _null_;
		infile glob recfm=n;
		file addfile recfm=n;
		input byte $char1. @;
		put  byte $char1. @;
	run;

	filename addfile clear;

%mend archiviazione;

%macro do_regselect(training_set=,
					int_var=, class_var=, n_var_class=,
					model_path=, model_out=);

		proc sort data=&training_set. out=training_eff;
			by &time_id hour area_id;
		run;

		data &training_set.;
			set training_eff;
		run;

		proc regselect data=&training_set.;
			%if &n_var_class. ne 0 %then %do;
				class &class_var.;
			%end;
		    model &target = &int_var. /stb; 
			code file="&model_path./&model_out.&ext";
			selection method =lasso /*(gamma=1 lscoeffs maxeffects=n maxsteps=n slentry=0.05 slstay=0.05 stop= )*/;
		run;

%mend do_regselect;


%macro do_nnet_1m(n_var_class=, training_set=, int_var=, class_var=, 
				  algo_nn=, maxiter_nn=, regl1_init=, regl2_init=, layer_string_init=,
				  model_path=, model_out=);

	proc sort data=&training_set out=work.temp;
		by &time_id hour area_id;
	run;
		
	data &training_set;
		set work.temp;
	run;

	proc nnet data=&training_set standardize=midrange missing=mean nthreads=16;
		%if &n_var_class ne 0 %then %do;
			input &class_var. /level=NOM;
		%end;
		input &int_var / level=int;
		architecture mlp;
		target &target / level=int;
		train outmodel=casuser.model_neural seed=12345;
		optimization algorithm=&algo_nn maxiter=&maxiter_nn REGL1=&regl1_init REGL2=&regl2_init;
		&layer_string_init;
		code file="&model_path./&model_out.&ext";
	run;

%mend do_nnet_1m;



%macro get_train_back(date_to=, horizon=, i=, input_dataset=, output_dataset=);

/* %put &=last_date; */
/* %let date_to=&last_date;  */
/* %let horizon=&horizon;  */
/* %let i=0; */
/* %let input_dataset=casuser.dataset1;  */
/* %let output_dataset=casuser.training_eff; */

		data lim_inf_train;
			/* Limiti trainingset */
			lim_inf_train=intnx('month', &date_to, &i-&horizon);
			call symputx("lim_inf_train",lim_inf_train);
			format lim_: date9.;
		run;

		/* TRAININGSET */
		data &output_dataset;
			set &input_dataset;
			if &time_id ge &lim_inf_train;
		run;

%mend get_train_back;

%macro get_months_train(mese, train_width, dset_string);

/*%let mese=&mese_val;*/
/*%let train_width=&season_width;*/
/*%let dset_string=dset_string_temp;*/

	data &dset_string(keep=string);
		ref_month=&mese;
		width=min(&train_width,6);
		lim_inf=IFN(ref_month-width<1,ref_month-width+12,ref_month-width);
		lim_sup=IFN(ref_month+width>12,ref_month+width-12,ref_month+width);

		if lim_inf<lim_sup then
			string=cat("if month(&time_id) >=", lim_inf, " and month(&time_id) <=", lim_sup);
		else string=cat("if month(&time_id) >=", lim_inf, " or month(&time_id) <=", lim_sup);
	run;

%mend;


%macro do_var_reduction(id=,training_varsel=, 
				flg_std_vars=, std_vars=,
				flg_maxeffects=, maxeffects=,
				varreduce_vars=, varreduce_class_vars=, varexp=, 
				flg_forced_vars=, forced_vars=, forced_class_vars=,
				modello=, key_area_cluster=, key_hour_cluster=,
				hour_cluster=, area_cluster=,
				save_file=, out_varsel_table=, mese_pred=, anno_pred=);
	
	/* Decido se standardizzare o no il ds di partenza */
	%if (&flg_std_vars=1) %then %do;
		
		data training_varsel;
			set &training_varsel;
		run;
		
		proc stdize data=training_varsel out=casuser.training_varsel_final 
				method=range;
			var &std_vars;
		run;
	%end;

	%else %do;
		data casuser.training_varsel_final;
			set &training_varsel;
		run;
	%end;

	%if &flg_maxeffects=1 %then %do;

		/* NOTA: varexp=1 essenziale altrimenti non fa il maxef */
		proc varreduce data=casuser.training_varsel_final technique=var;
			class &varreduce_class_vars;
			reduce supervised &target=&varreduce_vars / maxeffects=&maxeffects varexp=1;
			displayout SelectionSummary=var_exp_selection_eff;
		run;

	%end;
	%else %do;
			
		proc varreduce data=casuser.training_varsel_final technique=var;
			class &varreduce_class_vars;
			reduce supervised &target=&varreduce_vars / varexp=&varexp;
			displayout SelectionSummary=var_exp_selection_eff
			;
		run;

	%end;

	/* Decido se aggiungere variabili forzatamente a quelle selezionate tramite varreduce */
	%if &flg_forced_vars=1 %then %do;

	/* Aggiungo le variabili forzate (sia le time che non)*/
		%let count_fv=%sysfunc(countw(&forced_vars));
		%let count_ft=%sysfunc(countw(&forced_class_vars));

		data var_exp_selection_eff_add(keep=variable level);
			length variable $32.;

			%do i_force=1 %to &count_fv;
				%let value=%scan(&forced_vars, &i_force);
				variable="&value";
				level="-";
				output;
			%end;

			%do i_force=1 %to &count_ft;
				%let value=%scan(&forced_class_vars, &i_force);
				variable="&value";
				level="";
				output;
			%end;
		run;

		data casuser.var_exp_selection_eff;
			length variable $ 32;
			set casuser.var_exp_selection_eff var_exp_selection_eff_add;
		run;

	%end;

	/* Inserisco livello, modello, anno e mese di previsione, etc.. */
	data casuser.var_exp_selection_eff;
		length level $ 32 modello $2 key_hour_cluster $2 key_area_cluster $3 hour_cluster area_cluster $100;
		set casuser.var_exp_selection_eff;
		if scan(parameter, 2, ' ') ne '' then level='';
		else level="-";
		mese_pred=compress("&mese_pred");
		anno_pred=compress("&anno_pred");
		modello="&modello";
		key_hour_cluster="&key_hour_cluster";
		key_area_cluster="&key_area_cluster";
		hour_cluster="&hour_cluster";
		area_cluster="&area_cluster";
		id=&id;
	run;

	data casuser.var_exp_selection_eff;
		set casuser.var_exp_selection_eff;
		variable=lowcase(variable);
	run;

	proc sort data=casuser.var_exp_selection_eff out=var_exp_selection_eff nodupkey;
		by variable;
	run;
	
	/* Salvo i risultati mettendoli in append in &lib_output */
	%if %sysfunc(exist(&save_file)) %then
		%do;

			proc append base=&save_file
						data=var_exp_selection_eff force;
			run;

		%end;
	%else
		%do;

		data &save_file;
			stop;
			length id 8 modello $2 key_area_cluster $3  area_cluster $100
				   key_hour_cluster $2 hour_cluster $100 
				   mese_pred $8 anno_pred $8 level $ 32 variable $32 ;
		run;

		proc append base=&save_file 
					data=var_exp_selection_eff force;
		run;

		%end;

	/* Dataset di output con variabili selezionate per anno, mese, modello in esame */
	data &out_varsel_table;
		set var_exp_selection_eff (keep=variable level);
	run;

%mend do_var_reduction;

%macro geo_training(approccio=, tipo_modello=, id=, key_modello=, modello=, area_cluster=, key_area_cluster=,
						  hour_cluster=, key_hour_cluster=, 
						  flg_drop_vars=,
						  drop_vars=, flg_std_vars=, std_vars=, flg_maxeffects=, maxeffects=,
						  varexp=, varreduce_vars=, varreduce_class_vars=, flg_forced_vars=, forced_vars=,
						  forced_class_vars=, horizon=, season_width=, regl1=, regl2=, layer_string=, algo_nn=, 
							maxiter_nn=,split_approach=,approccio_modello=, b_imputed=,
							epsilon_svr=,degree_svr=,maxiter_svr=,
							learningrate_gb=,ntrees_gb=,minleafsize_gb=,maxdepth_gb=);

	/* Modello di output */
	data stringa_modello;
		geo_model=upcase(catx("_", "&approccio", "&modello", "&key_area_cluster", "&approccio_modello", "&tipo_modello", "&anno_pred", "&mese_pred", "&key_hour_cluster"));
		geo_model_base=upcase(catx("_", "&approccio", "&modello", "&key_area_cluster", "&approccio_modello","&tipo_modello"));
		call symputx("geo_model_base", geo_model_base);
		call symputx("geo_model", geo_model);
	run;

	/* 1. Filtro il dataset di training per modello e per Area/cluster di aree
	   2. Elimino set di variabili a seconda di parametri nella griglia di configurazione */

	data casuser.dataset1;
		set &lib_input..&input_dset (where=(modello="&modello" and area_label in &area_cluster. 
								and hour in &hour_cluster ));
		%if &flg_drop_vars=1 %then %do;
			drop &drop_vars;
		%end;
	run;

	/* Taglio DS di training */
	%get_train_back(date_to=&last_date, horizon=&horizon, i=0,
					input_dataset=casuser.dataset1, output_dataset=casuser.training_eff);

	/* Prendo intorno di +season_width,-season_width rispetto a mese di previsione */
	%get_months_train(&mese_val, &season_width, dset_string_temp);

	data dset_string_temp;
		set dset_string_temp;
		call symputx("string_tr", string);
	run;

	data casuser.training_eff;
		set casuser.training_eff;
		&string_tr;
	run;

	data casuser.training_var_sel;
		set casuser.training_eff;
		&string_tr;
	run;

	/* Variable Selection */
	%do_var_reduction(id=&id, training_varsel=casuser.training_var_sel, 
			flg_std_vars=&flg_std_vars, std_vars=&std_vars,
			flg_maxeffects=&flg_maxeffects, maxeffects=&maxeffects,
			varreduce_vars=&varreduce_vars, varreduce_class_vars=&varreduce_class_vars, 
			varexp=&varexp, 
			flg_forced_vars=&flg_forced_vars, forced_vars=&forced_vars, forced_class_vars=&forced_class_vars,
			modello=&modello, key_area_cluster=&key_area_cluster, key_hour_cluster=&key_hour_cluster,
			hour_cluster=&hour_cluster, area_cluster=&area_cluster,
			save_file=&lib_output..&output_varsel, out_varsel_table=var_exp_selection_final,
			mese_pred=&mese_val, anno_pred=&anno_pred);

	/* Creo selected_variables_interval e selected_variables_class */
	proc sql noprint;
		select distinct variable into: selected_variables_interval
		separated by " "
		from var_exp_selection_final (where = (level='-'))
		;
	quit;

	proc sql noprint;
		select distinct variable into: selected_variables_class
		separated by " "
		from var_exp_selection_final (where = (level=''))
		;
	quit;

	proc sql noprint;
		select count(*) into :n_var_class 
		from var_exp_selection_final (where = (level=''));
	quit;
	
	%if "&approccio_modello" = "NN" %then %do;

		%do_nnet_1m(n_var_class=&n_var_class, training_set=casuser.training_eff,
					int_var=&selected_variables_interval, class_var=&selected_variables_class, 
					algo_nn=&algo_nn, maxiter_nn=&maxiter_nn, 
					regl1_init=&regl1, regl2_init=&regl2, layer_string_init=&layer_string,
					model_path=&model_path, model_out=&geo_model);
	%end;

	%else %if "&approccio_modello" = "LR" %then %do;

		%do_regselect(training_set=casuser.training_eff,	
					int_var=&selected_variables_interval, class_var=&selected_variables_class,
					n_var_class=&n_var_class, model_path=&model_path, model_out=&geo_model);

	%end;

	%archivia_modelli(model_string=&geo_model_base);


%mend geo_training;



%macro execute_configs(input_grid, modello_totrain, hour_pred);

/* %let input_grid=&lib_input..&grid; */
/* %let modello_totrain=&modello_totrain; */
/* %let hour_pred=&hour_pred; */

	/* Filtro la griglia per modello e hour da prevedere */
	data input_grid_conf;
		set &input_grid (where=(modello="&modello_totrain" and index(hour_cluster, "&hour_pred")));
		/* Se ho un modello a Rete Neurale creo la stringa da inserire nella NNET */
		if nhidden ne . then do;
			array layers[*] nunits:;
			array actfun[*] act: ; 
			do i = 1 to nhidden;
				if i = 1 then layer_string = cat("hidden ", compress(put(layers[i], $32.)), "/ act=", compress(actfun[i]),";");
				else layer_string = catx(" ", layer_string, cat("hidden ", compress(put(layers[i], $32.)), "/ act=",compress(actfun[i]) ,";"));
			end;
			drop i; 
		end;
		else do;
			layer_string="";
		end;
	run;

/* data _null_; */
/* 	set input_grid_conf(obs=1); */
/* 	call symputx("approccio",approccio); */
/* 	call symputx("tipo_modello",tipo_modello); */
/* 	call symputx("id",id); */
/* 	call symputx("key_modello",key_modello);	 */
/* 	call symputx("modello",modello); */
/* 	call symputx("area_cluster",area_cluster);	 */
/* 	call symputx("key_area_cluster",key_area_cluster);	 */
/* 	call symputx("hour_cluster",hour_cluster);		 */
/* 	call symputx("key_hour_cluster",key_hour_cluster); */
/* 	call symputx("flg_drop_vars",flg_drop_vars); */
/* 	call symputx("drop_vars",drop_vars); */
/* 	call symputx("flg_std_vars",flg_std_vars); */
/* 	call symputx("std_vars",std_vars);	 */
/* 	call symputx("flg_maxeffects",flg_maxeffects); */
/* 	call symputx("maxeffects",maxeffects);	 */
/* 	call symputx("varexp",varexp);	 */
/* 	call symputx("varreduce_vars",varreduce_vars);		 */
/* 	call symputx("varreduce_class_vars",varreduce_class_vars); */
/* 	call symputx("flg_forced_vars",flg_forced_vars); */
/* 	call symputx("forced_vars",forced_vars); */
/* 	call symputx("forced_class_vars",forced_class_vars); */
/* 	call symputx("horizon",horizon); */
/* 	call symputx("season_width",season_width); */
/* 	call symputx("regl1",regl1); */
/* 	call symputx("regl2",regl2); */
/* 	call symputx("layer_string",layer_string); */
/* 	call symputx("algo_nn",algo_nn); */
/* 	call symputx("maxiter_nn",maxiter_nn); */
/* 	call symputx("approccio_modello",approccio_modello); */
/* run; */
/*  */
/* %put &=approccio; */
/* %put &=tipo_modello; */
/* %put &=id; */
/* %put &=key_modello	; */
/* %put &=modello; */
/* %put &=area_cluster; */
/* %put &=key_area_cluster;	 */
/* %put &=hour_cluster	; */
/* %put &=key_hour_cluster; */
/* %put &=flg_drop_vars; */
/* %put &=drop_vars; */
/* %put &=flg_std_vars; */
/* %put &=std_vars; */
/* %put &=flg_maxeffects; */
/* %put &=maxeffects; */
/* %put &=varexp; */
/* %put &=varreduce_vars	; */
/* %put &=varreduce_class_vars; */
/* %put &=flg_forced_vars; */
/* %put &=forced_vars; */
/* %put &=forced_class_vars; */
/* %put &=horizon; */
/* %put &=season_width; */
/* %put &=regl1; */
/* %put &=regl2; */
/* %put &=layer_string; */
/* %put &=algo_nn; */
/* %put &=maxiter_nn; */
/* %put &=approccio_modello; */



	data _null_;
		set input_grid_conf;
		call execute ('%NRSTR (%geo_training(approccio='||approccio||',tipo_modello='||tipo_modello||',id='||id||',
				key_modello='||key_modello||',modello='||modello||',
				area_cluster='||area_cluster||',key_area_cluster='||key_area_cluster||',
				hour_cluster='||hour_cluster||',key_hour_cluster='||key_hour_cluster||',
				flg_drop_vars='||flg_drop_vars||',drop_vars='||drop_vars||',
				flg_std_vars='||flg_std_vars||',std_vars='||std_vars||',
				flg_maxeffects='||flg_maxeffects||',maxeffects='||maxeffects||',varexp='||varexp||',
				varreduce_vars='||varreduce_vars||', varreduce_class_vars='||varreduce_class_vars||',
				flg_forced_vars='||flg_forced_vars||',forced_vars='||forced_vars||',forced_class_vars='||forced_class_vars||',
				horizon='||horizon||',season_width='||season_width||',
				regl1='||regl1||',regl2='||regl2||',layer_string='||layer_string||', algo_nn='||algo_nn||',maxiter_nn='||maxiter_nn||'
				,approccio_modello='||approccio_modello||'))');
	run;

%mend execute_configs;

/* %let modello_totrain=G1; */
/* %let hour_pred=05; */

/*============ Esecuzione funzione master =================*/
%execute_configs(&lib_input..&grid, &modello_totrain, &hour_pred);

