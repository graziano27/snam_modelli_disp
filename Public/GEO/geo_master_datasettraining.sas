/*===============================================================*/
/***** rilascio 2020-XX-XX - versione v1.0 *****/
/***** autori: Binda, Gregori, Rizzolo *****/
/***** BIP S.p.a. *****/
/*===============================================================*/

/* Note: inserire macro clean_all e put varie */

/*============ General Options and Creation of CAS Session ========*/
options casdatalimit=ALL;
cas GeoSession sessopts=(caslib=casuser timeout=31536000 locale="en_US");
caslib _all_ assign;

options minoperator; /* Necessary to recognize the IN operator within a macro */
options mprint mlogic symbolgen linesize=max;
ods exclude all;

/*============ Global Vars ========*/
%global dataset_presente start_date start_date_char start_datetime start_datetime_char;

%let start = %sysfunc(datetime());
/*!! PER IL TEST! POI TOGLIERE */
/* %let start="01JUL2020:11:00:00"dt; */
/* %let cut_train=30JUN2020:05:00:00; */

/* %let start="30JUN2020:21:00:00"dt; */
/* %let cut_train=29JUN2020:05:00:00; */

%let lib_physical=for_svil;
%let lib_memory=public;
%let geo_output_final_name=geo_trainingset;

filename geofcn FILESRVC folderpath="&logic_path" filename='geo_functions.sas';
filename geodat FILESRVC folderpath="&logic_path" filename='geo_dataset.sas';
filename geotrn FILESRVC folderpath="&logic_path" filename='geo_training.sas'; 

/*ora in cui viene generato il dataset (fra hour_dset:00 e hour_dset:59)*/
%let hour_limit=4;

data dates;
	length modello_totrain $2.;
/* 	dt_now=&start; */
	dt_now=dhms(datepart(&start),hour(datetime()),minute(datetime()),second(datetime()));
	g_flg_training=0;
	g1_flg_training=0;
	date_now=datepart(dt_now);
	hour_now=hour(dt_now);
	hour_pred=hour(intnx("hour",dt_now,1));
	hour_limit=&hour_limit;
	g_limit_inf=dhms(intnx("month",date_now,0,"B"),hour_limit,0,0);
	g_limit_sup=intnx("dtday",g_limit_inf,1,"S");
	g1_limit_inf1=dhms(intnx("month",date_now,-1,"E"),hour_limit,0,0);
	g1_limit_sup1=dhms(intnx("month",date_now,0,"B"),hour_limit,0,0);;
	g1_limit_inf2=dhms(intnx("month",date_now,0,"E"),hour_limit,0,0);
	g1_limit_sup2=dhms(intnx("month",date_now,1,"B"),hour_limit,0,0);
	if (dt_now ge g_limit_inf and dt_now lt g_limit_sup) then do;
		g_flg_training=1;
		modello_totrain="G";
		hour_list="(6 11 12 17 18)";
	end;
	if (dt_now ge g1_limit_inf1 and dt_now lt g1_limit_sup1) or 
		(dt_now ge g1_limit_inf2 and dt_now lt g1_limit_sup2) then do;
		g1_flg_training=1;
		modello_totrain="G1";
		hour_list="(5 21 22)";
	end;
	call symputx("hour_now",hour_now);
	call symputx("hour_pred", put(hour_pred, z2.0));
	call symputx("hour_pred_num",hour_pred);
	call symputx("modello_totrain",modello_totrain);
	call symputx("g_flg_training",g_flg_training);
	call symputx("g1_flg_training",g1_flg_training);
	call symputx("hour_list",hour_list);
	format dt_now datetime19. date_now date9. g_limit: g1_limit: datetime19.;
run;

/* Macro clean all (?) */


%macro geo_dataset_training();

	%put --- &=g_flg_training &=g1_flg_training;
	%if (&g_flg_training = 1 or &g1_flg_training = 1) %then %do;
		
		%put --- SONO NELLA FINESTRA DI TRAINING DEL MODELLO &modello_totrain AD AREE GEOGRAFICHE ---;
		%include geofcn;

		%if (&hour_now eq &hour_limit) %then %do;
			%put --- SONO LE ORE &hour_now: CREO IL DATASET DI TRAINING;
			%include geodat;
			%if &hour_pred_num in &hour_list %then %do;
				%put --- ORA DA PREVEDERE &hour_pred_num: SI TROVA IN &hour_list.. GENERO I MODELLI CORRISPONDENTI;
				%include geotrn;
			%end;
			%else %do;
				%put --- ORA DA PREVEDERE &hour_pred_num: NON SI TROVA IN &hour_list.. NON GENERO ALCUN MODELLO;
			%end;
		%end;

		%else %do;
			%put --- SONO LE ORE &hour_now: NON CREO IL DATASET DI TRAINING;
			%if &hour_pred_num in &hour_list %then %do;
				%put --- ORA DA PREVEDERE &hour_pred_num: SI TROVA IN &hour_list.. GENERO I MODELLI CORRISPONDENTI;
				%include geotrn;
			%end;
			%else %do;
				%put --- ORA DA PREVEDERE &hour_pred_num: NON SI TROVA IN &hour_list.. NON GENERO ALCUN MODELLO;
			%end;
		%end;

	%end;
	%else %do;
		%put --- NON SONO NELLA FINESTRA DI TRAINING PER I MODELLI AD AREE GEOGRAFICHE ---;
		%put --- NON ESEGUO IL PROCESSO DI TRAINING ---;
	%end;


%mend geo_dataset_training;


%geo_dataset_training();


cas GeoSession terminate; 
