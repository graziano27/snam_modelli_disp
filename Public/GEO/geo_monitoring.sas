
libname dati "/sasdata/modelli_geo";
%macro compute_stats(predictions=, dset_target=, output_ds_mape=, 
		output_ds_mae=, hour=, type_discr=);

/* %let predictions=&lib_output..&output_preds; */
/* %let dset_target=casuser.dataset1; */
/* %let save_stats_mape= &lib_output..&output_stats_mape; */
/* %let save_stats_mae=&lib_output..&output_stats_mae;  */

	proc sql;
		create table score as 
		select t1.area_label
			  , t1.area_id
			  , t1.&hour.
			  , t1.&time_id
			  , t1.&type_discr.
			  , t1.P_&target_id./(1000000) as P_&target_id._Msmc
			  , t2.&target_id./(1000000) as &target_id._Msmc
			  , abs(t2.&target_id.-P_&target_id.)/abs(t2.&target_id.) as mape
			  , abs(t2.&target_id.-P_&target_id.)/(1000000) as mae
		from &predictions t1
		   , &dset_target t2 
		where ( t1.&time_id=t2.&time_id and t1.area_id=t2.area_id) 
		order by t1.&time_id, t1.area_id
		;
	quit;


	data Scoreout_tune;
		set score;
		flag5=0;
		flag3=0;
		flag1=0;
		flag1smc=0;
		flag2o5smc=0;
		flag5smc=0;
		if mape<=0.01 then
			flag1=1;

		if mape<=0.03 then
			flag3=1;

		if mape<=0.05 then
			flag5=1;

		if mae<=1 then
			flag1smc=1;

		if mae<=2.5 then
			flag2o5smc=1;

		if mae<=5 then
			flag5smc=1;
	run;

	proc sql;
		create table &output_ds_mape as 
		select distinct  &type_discr.
			  ,&hour.
			  , area_id
			  , area_label
			  , count(mape) as conteggio
			  , mean(mape) as mape_medio
			  , max(mape) as mape_max
			  , std(mape) as mape_std
			  , sum(flag1)/count(flag1) as flag1_tot
			  , sum(flag3)/count(flag3) as flag3_tot
			  , sum(flag5)/count(flag5) as flag5_tot
		from scoreout_tune 
		group by  &type_discr., &hour., area_id 
		;
	quit;

	proc sql;
		create table &output_ds_mae. as 
		select distinct  &type_discr.
			  , &hour.
			  , area_id
			  , area_label
			 , count(mae) as conteggio
			 , mean(mae) as mae_medio
			 , median(mae) as mae_mediana
			 , max(mae) as mae_max 
			 , std(mae) as mae_std
		     , sum(flag1smc)/count(flag1smc) as flag1smc_tot
			 , sum(flag2o5smc)/count(flag2o5smc) as flag2o5smc_tot
			 , sum(flag5smc)/count(flag5smc) as flag5smc_tot
		from scoreout_tune 
	 	group by  &type_discr., &hour., area_id 
		;
	quit;

	

%mend compute_stats;

%let start_date='13OCT20'd;
%let cut_date=datepart(datetime());
%let target_id=tgt_riconsegnato_totale;
%let time_id=data_gas_rif;


/*check non ci siano missing o valori anomali  */
data geo_previsioni_riconsegnato;
set dati.geo_previsioni_riconsegnato;
where data_gas_rif >= &start_date and data_gas_rif <= &cut_date;
run;

/*prendo il target dall'output intermedio geo_target*/
data geo_target;
set dati.geo_target;
where data_gas_rif >= &start_date and data_gas_rif <= &cut_date;
run;

data geo_output_models;
set dati.geo_output_models;
hour = hour(dt_calcolo);
data_gas_rif=datepart(dt_riferimento_dati);
if month(data_gas_rif)>=10 then
			anno_termico=cat(year(data_gas_rif),'/',year(data_gas_rif)+1);
else anno_termico=cat(year(data_gas_rif)-1,'/',year(data_gas_rif));
anno_mese = catx('_',year(datepart(dt_riferimento_dati)),month(datepart(dt_riferimento_dati)));
where datepart(dt_riferimento_dati) >= &start_date and
		datepart(dt_riferimento_dati) <= &cut_date;
format data_gas_rif date9.;
rename PREV=P_&target_id;
run;

/*Compute stats detail*/
%compute_stats(predictions=geo_output_models, dset_target=geo_target,
		output_ds_mape=stats_mape_dettagio, output_ds_mae=stats_mae_dettaglio, hour=hour, type_discr=anno_mese);

/*Compute stats ora solare  */
data geo_output_models_ora_clock;
	set geo_output_models;
	/*if summer*/
	if dhms(nwkdom(5, 1, 3, year(datepart(dt_calcolo))),6,0,0) < dt_calcolo < dhms(nwkdom(5, 1, 10, year(datepart(dt_calcolo))),6,0,0 ) then
			do;
				hour_final=hour+1;
			end;
		else
			do;
				hour_final=hour;
		end;
	if hour_final = 6 and hour = 5 and key_modello = 1 then 
		do;
			key_modello = 0;
			modello = "G";
		end;
run;

%compute_stats(predictions=geo_output_models_ora_clock, dset_target=geo_target,
		output_ds_mape=stats_mape, output_ds_mae=stats_mae, hour=hour_final, type_discr=anno_mese);



/* Final table for the put is stats_mae   */
proc sort data=stats_mae;
	by area_label hour_final;
run;

proc sort data=dati.mae_gaia_baseline;
	by area_label hour_final;
run;

data geo_stats_mae_AM;
	merge stats_mae(in=L) dati.mae_gaia_baseline;
	by area_label hour_final;
	if hour_final=22 then hour_index=1;
	else if hour_final=6 then hour_index=2;
	else if hour_final=12 then hour_index=3;
	else if hour_final=18 then hour_index=4;
	if L;
run;
proc sort data=geo_stats_mae_AM;
	by anno_mese area_id hour_index ;
run;

%compute_stats(predictions=geo_output_models_ora_clock, dset_target=geo_target,
		output_ds_mape=stats_mape_AT, output_ds_mae=stats_mae_AT, hour=hour_final, type_discr=anno_termico);

proc sort data=stats_mae_AT;
	by area_label hour_final;
run;

proc sort data=dati.mae_gaia_baseline;
	by area_label hour_final;
run;

data geo_stats_mae_AT;
	merge stats_mae_AT(in=L) dati.mae_gaia_baseline;
	by area_label hour_final;
	if hour_final=22 then hour_index=1;
	else if hour_final=6 then hour_index=2;
	else if hour_final=12 then hour_index=3;
	else if hour_final=18 then hour_index=4;
	if L;
run;
proc sort data=geo_stats_mae_AT;
	by anno_termico area_id hour_index ;
run;

/* Save stats */
data dati.geo_stats_mae_AT;
set geo_stats_mae_AT;
run;
data dati.geo_stats_mae_AM;
set geo_stats_mae_AM;
run;