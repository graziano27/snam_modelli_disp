/*===============================================================*/
/***** rilascio 2020-01-XX - versione v1.0 *****/
/***** autori: Binda, Rizzolo, Gregori *****/
/***** BIP S.p.a. *****/
/*===============================================================*/

/* options nomlogic nomprint nosymbolgen; */
/* options mlogic mprint symbolgen msglevel=i; */
/* ods exclude all; */
/*ods exclude none;*/

/*============ Macro variabili =================================*/
/* Definizione macro variabili Globali */

/*LIBNAME for_svil "C:\Users\CBinda\Bip\SNAM Analytics - General\01-Modelli_Geografici\01_Deliverable\Rilascio\Dataset";*/
/*%let start = %sysfunc(datetime());*/
%let lib_physical = for_svil;
%let dt_solare=&start;
%let dset_input_name = geo_previsioni_riconsegnato;
%let dataset_input = &lib_physical..&dset_input_name;
%let ens_conf_name = geo_configurazioni_ensemble;
%let ens_conf = &lib_physical..&ens_conf_name;
%let time_id = datetime_solare;

%macro ensemble_prediction(dataset_input=, ens_conf=, time_id=);

	proc sort data=&dataset_input;
		by &time_id;
	run;

	data time;
		set &dataset_input end=eof;

		if eof then
			do;
				call symputx("last_hour",hour);
				call symputx("last_date",datepart(datetime_solare));
/*				call symputx("hour_pred",(put(hour(&dt_solare),z2.)));*/
/*				call symputx("date_pred",compress(put(datepart(&dt_solare),date9.)));*/
			end;
	run;

	data preds_to_use;
		set &dataset_input;
		where hour = &last_hour and 
		datepart(datetime_solare) = &last_date;
	run;

	proc sort data=preds_to_use out=preds_to_use_sort;
		by area_id hour id;
	run;

	proc sort data=&ens_conf out=ens_conf_sort;
		by area_id hour id;
	run;

	data preds_weighted;
		merge preds_to_use_sort(in=L) ens_conf_sort(in=R drop=approccio_label);
		by area_id hour id;
		if L;
		P_weight=p_tgt_riconsegnato_totale*weight;
	run;

	proc sql;
		create table preds_ensemble as 
		select data_gas_rif
			 , datetime_solare 
			 , hour
			 , modello
			 , key_modello
			 , area_label
			 , area_id
			 , sum(p_weight) as p_ensemble
		from preds_weighted
		group by 1,2,3,4,5,6,7
		;
	quit;

%mend;

%macro save_ensemble_prediction(preds_ensemble=, time_id=);

/*	%let preds_ensemble=preds_ensemble;*/

	data preds_ensemble_ok;
		set &preds_ensemble;
/*		tipo_modello = 'EN';*/
		id = 7;
		approccio = "GEO07";
		rename p_ensemble=p_tgt_riconsegnato_totale;
	run;		
		
	data &dataset_input;
		set &dataset_input preds_ensemble_ok;
	run;

	proc sort data=&dataset_input;
		by &time_id area_id;
	run;

%mend;

%ensemble_prediction(dataset_input=&dataset_input, 
	ens_conf=&ens_conf, time_id=&time_id);

%save_ensemble_prediction(preds_ensemble=preds_ensemble, time_id=&time_id);