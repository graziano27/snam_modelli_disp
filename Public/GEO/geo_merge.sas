
/*===============================================================*/
/***** rilascio 2020-XX-XX - versione v0 *****/
/***** autori: Binda, Gregori, Rizzolo *****/
/***** BIP S.p.a. *****/
/*===============================================================*/

/* options nomlogic nomprint nosymbolgen; */
/* options mlogic mprint symbolgen msglevel=i; */
/* ods exclude all; */
/*ods exclude none;*/

/*============ Macro variabili =================================*/
/* Definizione macro variabili Globali */
%let libreria_input = &lib_physical;
%let dset_input_name = geo_previsioni_riconsegnato; 
%let dataset_input = &libreria_input..&dset_input_name;
%let libreria_output = &lib_physical;
%let dset_output_name = geo_output_models; 
%let dataset_output = &libreria_output..&dset_output_name;
%let final_pred_id = 7;


%macro get_predictions_geo(dataset_input=);

	%if %sysfunc(exist(&dataset_input)) %then
		%do;

		/* Prendo la max datetime_solare */
		proc sql noprint;
			select max(datetime_solare) format datetime19. into: max_dt
			from &dataset_input
			;
		quit;

		proc sort data=&dataset_input (where=(datetime_solare = "&max_dt"dt)) out=last_preds (drop=modello);
			by datetime_solare key_modello area_id;
		run;

		data preds_geo_topub(keep=PREV DT_RIFERIMENTO_DATI unita_misura Tipologia_previsione
								modello dt_calcolo area_id area_label);
			length DT_CALCOLO 8 DT_RIFERIMENTO_DATI 8 PREV 8 UNITA_MISURA $ 30 
					Tipologia_Previsione $ 30 MODELLO $ 30 area_id 8 area_label $2;
			set last_preds(Rename=(p_tgt_riconsegnato_totale=PREV));
			where id = &final_pred_id;
			dt_calcolo = datetime();
			DT_RIFERIMENTO_DATI = dhms(data_gas_rif, 0, 0, 0);
			UNITA_MISURA = 'SMC';
			Tipologia_Previsione = 'RiconsTot';
			if key_modello=0 then MODELLO = 'Predizione G per G';
			else if key_modello=1 then MODELLO='Predizione G per G+1';
			format dt: datetime19.;
		run;

	%end;

%mend;

%macro prev(dataset_output);
	/* controllo l'esistenza della tabella finale, in caso contrario la definisco */
	%if not(%sysfunc(exist(&dataset_output))) %then
		%do;

			data &dataset_output;
				stop;
				length DT_CALCOLO 8 DT_RIFERIMENTO_DATI 8 PREV 8 UNITA_MISURA $ 30 
						Tipologia_Previsione $ 30 MODELLO $ 30 area_id 8 area_label $2;
				format DT_CALCOLO datetime19. DT_RIFERIMENTO_DATI datetime19.;
			run;

		%end;

	/* raccolta previsioni geo */
	%get_predictions_geo(dataset_input=&dataset_input);

	data &dataset_output;
		set &dataset_output preds_geo_topub;
	run;
	
	proc sort data= &dataset_output;
		by DT_CALCOLO DT_RIFERIMENTO_DATI MODELLO area_id;
	run;

%mend;

%prev(&dataset_output);
