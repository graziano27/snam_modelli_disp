/*libname for_svil "/sasdata/modelli_geo/input";*/
/* %include "/sas/sasdata/modelli/Librerie.sas"; */

/* To monitor the TOTAL EXECUTION TIME */
data _null_;
	call symputx("dt_run_all",datetime());
	call symputx("dt_run_all_char",put(datetime(),datetime19.));
run;
%put --- START PROCESSING FUNCTION &function &dt_run_all_char ---;


/* da inserire nel geo_master_datasettraining */
/* %global dataset_presente start_date start_date_char start_datetime start_datetime_char; */
/* %let start=%sysfunc(datetime()); */
%let start_date_all = "01JAN2017"d;
%let start_dt_all = "01JAN2017:06:00:00"dt;

/* %let lib_physical=for_svil; */
/* %let lib_memory=public; */
/* %let geo_output_final_name=geo_inputmodelli; */

/******/
%let keyout=score;
%let flg_trainingset=0;
%let dt_solare=&start;
%let libreria_output=&lib_physical;
%let libreria_memory=&lib_memory;
%let geo_output_final=&libreria_output..&geo_output_final_name;

%let geo_output_base=&geo_output_final;
%let geo_output_removed=&libreria_output..&geo_output_final_name._prov;
%let geo_dupp=&libreria_output..&geo_output_final_name._dup;
%let geo_output_cut=work.input_cutds;
%let datetime_var_dup=datetime_solare;
%let key_vars = key_modello area_id datetime_solare;


/* Flag anomalie = 0 significa no check anomalie */
%let flg_anomalie = 1;

/* ORALPBP2 Tables */
/* Meteo Celle */
%let input_celle_cons=oralpbp2.d_e_meteo_consuntivo;
%let input_celle_prev=oralpbp2.d_e_dati_meteo_aree;
/* Meteo Citt? */
%let input_meteocitta=oralpbp2.d_e_meteo_calc;
/* Terna */
%let input_terna=oralpbp2.d_e_terna;
%let input_up=oralpbp2.d_e_up;
%let input_remi_termo=oralpbp2.d_e_up_new;
/* Entry Exit */
/* Nota: non c'? libreria ORALPBP2 perch? nel codice la prende con pass through */
%let input_entry_exit=flussifisici; 
/* Scada cons */
%let input_misuratori_scada=oralpbp2.misuratori_scada;
/* DLP */
%let input_dlp=oralpbp2.linepack_geo_vw;


/* WEBDIPS Tables */
/* !! Da cambiare puntamento non appena le tabelle saranno disponibili in WEBDIPS */
%let input_transiti_entryexit = oralpbp2.misuratori_scada_geo_rec;

%let input_anag_entry_exit=webdips.GEO_ANAG_ENTRYEXIT;
%let input_anag_dlp=webdips.GEO_ANAG_DLP;
%let input_anag_transiti=webdips.GEO_ANAG_TRANSITI;


/* FOR_SVIL Tables */
/* Meteo Celle */
%let grouping_celle=&libreria_output..geo_grouping_meteocelle;
%let anag_meteo_celle=&libreria_output..geo_anag_meteocelle;
%let output_celle=&libreria_output..geo_output_meteocelle_&keyout.;
/* Meteo Citt? */
%let grouping_citta=&libreria_output..geo_grouping_meteocitta;
%let anag_meteo_citta=&libreria_output..geo_anag_meteocitta;
%let output_meteocitta=&libreria_output..geo_output_meteocitta_&keyout.;
/* Terna */
%let anag_remi_terna_aree=&libreria_output..geo_anag_terna;
%let output_consuntivo_terna_aree=&libreria_output..geo_output_ternacons_&keyout.;
%let output_previsione_terna_aree=&libreria_output..geo_output_ternaprev_&keyout.;
/* Scada cons */
%let anag_scadacons=&libreria_output..geo_anag_scadacons;
%let output_scadacons=&libreria_output..geo_output_scadacons_&keyout.;
/* Variabili calendario */
%let output_geo_calendar=&libreria_output..geo_output_calendar_&keyout.;
/* Bilancio - Consuntivi (Tab di appoggio, storico da 02JUL2019) */
%let geo_bilancio=&libreria_output..geo_bilancio;
/* Target - Consuntivi giornalieri */
%let geo_target=&libreria_output..geo_target;
/* Bilancio e Target Lag e Mvg (daily and hourly) */
%let output_bilancio_daily=&libreria_output..geo_output_bilancio_d_&keyout.;
%let output_bilancio_hourly=&libreria_output..geo_output_bilancio_h_&keyout.;
%let input_geo_ricorario_imputato = &libreria_output..geo_ricorario_imputed_new2;
/* Transiti + Entry exit intermedio */
%let output_transiti_entryexit = &libreria_output..input_transiti_entryexit_&keyout;
/* DLP */
%let output_dlp=&libreria_output..geo_output_dlp_&keyout.;
/* TRANSITI */
%let output_transiti=&libreria_output..geo_output_transiti_&keyout.;
/* Entry Exit */
%let output_entry_exit=&libreria_output..geo_output_entryexit_&keyout.;

/* STORICIZZAZIONE ANOMALIE */
/* Anomalie Terna*/
%let output_anomalie_terna=&libreria_output..geo_anomalie_terna;
/* Anomalie scada cons*/
%let output_anomalie_scadacons=&libreria_output..geo_anomalie_scadacons;
/* Anomalie bilancio*/
%let output_anomalie_entryexit=&libreria_output..geo_anomalie_entryexit;
%let output_anomalie_dlp=&libreria_output..geo_anomalie_dlp;
%let output_anomalie_transiti=&libreria_output..geo_anomalie_transiti;
%let output_anomalie_bilancio=&libreria_output..geo_anomalie_bilancio;
/* Tabella finale Anomalie*/
%let output_anomalie_totale=&libreria_output..geo_anomalie_orarie;
/* Calendar only dates */
%let input_calendario_dates=calendar_dates;


/* Init Dates */
data init_dates;
	g_solare=datepart(&dt_solare);
	g_gas=ifn(hour(&dt_solare)<6, intnx("day", g_solare, -1), g_solare);
	g1_solare=intnx("day",g_solare,1);
	g1_gas=intnx("day", g_gas,1);
	call symputx("g_solare",g_solare);
	call symputx("g_gas",g_gas);
	call symputx("g1_solare",g1_solare);
	call symputx("g1_gas",g1_gas);
	format g: date9.;
run;

%macro create_input_components(shift_type_short=, shift_type_long=, shift_value_short=, shift_value_long=,
		output_geo_calendar=, output_scadacons=,
		output_celle= , output_meteocitta=, 
		output_consuntivo_terna_aree=, output_previsione_terna_aree=, 
		output_entry_exit=, output_transiti=, output_dlp=,
		output_bilancio_hourly=, output_bilancio_daily=, geo_target=);

	%put_run_date_init(CALENDAR);
	%calendar(shift_type=&shift_type_short, shift_value=&shift_value_short, output_geo_calendar=&output_geo_calendar);
	%put_run_time(CALENDAR);


	%put_run_date_init(SCADACONS);
	%scadacons(shift_type=&shift_type_long, shift_value=&shift_value_short, input_anagrafica=&anag_scadacons, 
			input_calendario_dates=&input_calendario_dates, 
			input_misuratori_scada=&input_misuratori_scada, g_gas=&g_gas, g1_solare=&g1_solare,
			prefix=sc_, cons_output=&output_scadacons);
	%put_run_time(SCADACONS);


	%put_run_date_init(METEOCELLE);
	%meteocelle(g_gas=&g_gas, g1_solare=&g1_solare, shift_type=&shift_type_long, shift_value=&shift_value_short,
			input_celle_cons=&input_celle_cons, input_celle_prev=&input_celle_prev, 
			input_calendario_dates=&input_calendario_dates, anag_meteo_celle=&anag_meteo_celle,
			grouping_all=&grouping_celle, output_final=&output_celle);
	%put_run_time(METEOCELLE);


	%put_run_date_init(METEOCITTA);
	%meteocitta(g_gas=&g_gas, g1_solare=&g1_solare, shift_type=&shift_type_long, shift_value=&shift_value_short,
					input_dset=&input_meteocitta, input_grouping_citta=&grouping_citta, 
					input_calendario_dates=&input_calendario_dates, 
					anag_meteo_citta=&anag_meteo_citta, pfx=mci_, output_dset=&output_meteocitta);
	%put_run_time(METEOCITTA);


	%put_run_date_init(TERNA);
	%terna(g_gas=&g_gas, g1_solare=&g1_solare, shift_type=&shift_type_long, shift_value=&shift_value_short, 
			input_calendario_dates=&input_calendario_dates, 
			input_terna=&input_terna, input_up=&input_up, input_remi_termo=&input_remi_termo, 
			output_consuntivo_terna_aree=&output_consuntivo_terna_aree, 
			output_previsione_terna_aree=&output_previsione_terna_aree);
	%put_run_time(TERNA);

	%read_transiti_entryexit(input_transiti_entryexit=&input_transiti_entryexit, 
	shift_type=&shift_type_short, shift_value=&shift_value_short, 
	output_transiti_entryexit=&output_transiti_entryexit);

	%put_run_date_init(ENTRYEXIT);
	/* NOTA: PER LA PROD DEVE ESSERE shift_type_short*/
	/* NOTA: aggiornare shift_value di lettura a 2 giorni	 */
	%entryexit(g_gas=&g_gas, g1_solare=&g1_solare, shift_type=&shift_type_short, shift_value=&shift_value_short,  
			input_ff=&input_entry_exit, input_transiti_entryexit=&output_transiti_entryexit, 
			input_anagrafica=&input_anag_entry_exit,
			input_calendario_dates=&input_calendario_dates,  output_ds=&output_entry_exit);
	%put_run_time(ENTRYEXIT);


	%put_run_date_init(DELTALINEPACK);
	%deltalinepack(g_gas=&g_gas, g1_solare=&g1_solare, shift_type=&shift_type_short, shift_value=&shift_value_short,
					input_calendario_dates=&input_calendario_dates, ds_in=&input_dlp, 
				prefix=dlp_, anagrafica_dlp=&input_anag_dlp, ds_output=&output_dlp);
	%put_run_time(DELTALINEPACK);


	%put_run_date_init(TRANSITI);
	/* NOTA: PER LA PROD DEVE ESSERE shift_type_short*/
	/* NOTA: aggiornare shift_value di lettura a 2 giorni	 */
	%transiti(g_gas=&g_gas, g1_solare=&g1_solare, shift_type=&shift_type_short, shift_value=&shift_value_short, 
		  input_calendario_dates=&input_calendario_dates, input_transiti=&output_transiti_entryexit, 
			anag_in=&input_anag_transiti, ds_output=&output_transiti);
	%put_run_time(TRANSITI);


	%put_run_date_init(BILANCIO);
	%bilancio(geo_bilancio=&geo_bilancio, shift_type=&shift_type_long, shift_value=&shift_value_long,  
		  geo_target=&geo_target, input_trs=&output_transiti, input_dlp=&output_dlp,
		  input_entryexit=&output_entry_exit, g_gas=&g_gas, g1_solare=&g1_solare, 
		  input_geo_ricorario_imputato=&input_geo_ricorario_imputato,
		  output_bilancio_h_lg_mvg=&output_bilancio_hourly, 
			output_bilancio_d_lg_mvg=&output_bilancio_daily); 
	%put_run_time(BILANCIO);


	%merge_anomalie(lib_in=&libreria_output., ds_out=&output_anomalie_totale.);

%mend create_input_components;

%macro create_geo_input(g_gas=);

	%checkds(&geo_output_final);
	%create_input_components(shift_type_short=day, shift_type_long=month, 
							 shift_value_short=2, shift_value_long=12,
		output_geo_calendar=&output_geo_calendar, output_scadacons=&output_scadacons,
		output_celle=&output_celle, output_meteocitta=&output_meteocitta, 
		output_consuntivo_terna_aree=&output_consuntivo_terna_aree, 
		output_previsione_terna_aree=&output_previsione_terna_aree, output_entry_exit=&output_entry_exit,
		output_transiti=&output_transiti, output_dlp=&output_dlp,
		output_bilancio_hourly=&output_bilancio_hourly, output_bilancio_daily=&output_bilancio_daily,
		geo_target=&geo_target);

	%put_run_date_init(MERGE);
	%merge_components(flg_trainingset=&flg_trainingset, g_gas=&g_gas, g1_solare=&g1_solare, shift_type=day, 
		shift_value=2, in_calendario=&output_geo_calendar, 
		in_scadacons=&output_scadacons, 
		in_meteocelle=&output_celle, in_meteocitta=&output_meteocitta,
		in_terna_cons=&output_consuntivo_terna_aree, in_terna_prev=&output_previsione_terna_aree, 
		in_bil_d=&output_bilancio_daily, in_bil_h=&output_bilancio_hourly, in_target=&geo_target, 
		output_dset=&geo_output_final, output_dset_base=&geo_output_base, 
		output_dset_cut=&geo_output_cut);
	%put_run_time(MERGE);

%mend create_geo_input;

%create_geo_input(g_gas=&g_gas);


/* To monitor the TOTAL EXECUTION TIME */
data _null_;
	time_exe=datetime()-&dt_run_all;
	call symputx("time_exe",put(time_exe,time.));
run;
%put --- END PROCESSING. TOTAL PROCESSING TIME: &time_exe. ---;
