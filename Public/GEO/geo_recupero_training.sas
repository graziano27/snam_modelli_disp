/*===============================================================*/
/***** rilascio 2020-09-XX - versione v1.0 *****/
/***** autori: Binda, Gregori, Rizzolo *****/
/***** BIP S.p.a. *****/
/*===============================================================*/

/*============ General Options and Creation of CAS Session ========*/
options casdatalimit=ALL;
cas GeoSession sessopts=(caslib=casuser timeout=31536000 locale="en_US");
caslib _all_ assign;

options minoperator; /* Necessary to recognize the IN operator within a macro */
options mprint mlogic symbolgen linesize=max;
ods exclude all;
/* options nosymbolgen nomprint nomlogic; */

/*============ Global Vars ========*/
%global dataset_presente start_date start_date_char start_datetime start_datetime_char;

%let start = %sysfunc(datetime());
%let start_fix = %sysfunc(datetime());
%let diff_end_month=-1;

%let lib_physical=for_svil;
%let lib_memory=public;

%let geo_output_final_name = geo_trainingset;


%put --- GENERAZIONE DATASET DI TRAINING E MODELLI GEOGRAFICI ---;

%let logic_path = /Public/GEO/;
%let lib_path = /sasdata/modelli_geo;
/*Clean current models  */
/* %let model_path = &lib_path; */

filename geotrn FILESRVC folderpath="&logic_path" filename='geo_training.sas'; 
filename geolib FILESRVC folderpath="&logic_path" filename='librerie_viya.sas'; 

/*ora in cui viene generato il dataset (fra hour_dset:00 e hour_dset:59)*/
%let hour_limit=4;

%macro clean_current_models();

	filename myDir "&model_path./";

	data get;
		did=dopen("myDir");
		filecount=dnum(did);
		do i = 1 to filecount;
			file_ = dread(did, i);
			output;
		end;
		rc=dclose(did);
	run;

	data to_delete;
		set get;
		where (file_ contains "NN" or file_ contains "LR")  and file_ contains "&anno_pred_g._&mese_pred_g.";
		fname="tempfile";
		rc=filename(fname, cat("&model_path./", compress(file_)));
		rc=fdelete(fname);
	run;

%mend;

%macro clean_all();

	proc datasets lib=work kill nolist memtype=data;
	quit;

	Data MVars ( Keep = Name );
		Set SasHelp.VMacro;
		Where Scope = 'GLOBAL';

		if find(Name, "SAS") = 0;

		if find(Name, "SYS") = 0;

		if find(Name, "CLIENT") = 0;

		if find(Name, "SQL") = 0;

		if find(Name, "EG") = 0;

		IF NAME NOT IN ("CURRENT_HOUR", "HOUR_TRAIN", "START", "START_FIX", "HOUR_END", 
		"GRAPHTERM", "G_OUTPUT_FINAL_NAME", "LOGIC_PATH", "START", "PFX", "LIB_PHYSICAL", "LIB_MEMORY",
		"LIB_PATH", "DATASET_PRESENTE");
	Run;

	Data _Null_;
		Set MVars;
		Call Symdel( Name );
	Run;

%mend clean_all;

%macro increase_start(dt_start=);
	
	data start;
		start = intnx("hour", &dt_start, 1);
		call symputx("start", start);
		format start datetime19.;
		put start;
	run;
	
%mend;

%macro Emergenza_Training();

/* 	%clean_current_models(); */
	
	%include geolib;

	%do hour_TRAIN = %sysfunc(hour(&start)) %to 23;
		
		%if %eval(&hour_TRAIN in 6 11 12 17 18) %then %do;
			data dates;
				length modello_totrain $2.;
				hour_pred=&hour_TRAIN;
				modello_totrain="G";
				call symputx("hour_pred", put(hour_pred, z2.0));
				call symputx("modello_totrain",modello_totrain);
			run;
	
			%include geotrn;
/* 			%clean_all(); */
			%increase_start(dt_start=&start);	
		%end;
		%else %if %eval(&hour_TRAIN in 5 21 22) %then %do;
			data dates;
				length modello_totrain $2.;
				hour_pred=&hour_TRAIN;
				modello_totrain="G1";
				call symputx("hour_pred", put(hour_pred, z2.0));
				call symputx("modello_totrain",modello_totrain);
			run;
	
			%include geotrn;
/* 			%clean_all(); */
			%increase_start(dt_start=&start);
		%end;
	%end;
	
	data end;
		hour_end = hour(&start_fix) - 1;
		call symputx("hour_end", hour_end);
	run;

	%do hour_TRAIN = 0 %to &hour_end;
		%if %eval(&hour_TRAIN in 6 11 12 17 18) %then %do;
			data dates;
				length modello_totrain $2.;
				hour_pred=&hour_TRAIN;
				modello_totrain="G";
				call symputx("hour_pred", put(hour_pred, z2.0));
				call symputx("modello_totrain",modello_totrain);
			run;
	
			%include geotrn;
/* 			%clean_all(); */
			%increase_start(dt_start=dhms(datepart(&start_fix), &hour_TRAIN, 0, 0));
		%end;
		%else %if %eval(&hour_TRAIN in 5 21 22) %then %do;
			data dates;
				length modello_totrain $2.;
				hour_pred=&hour_TRAIN;
				modello_totrain="G1";
				call symputx("hour_pred", put(hour_pred, z2.0));
				call symputx("modello_totrain",modello_totrain);
			run;
	
			%include geotrn;
/* 			%clean_all(); */
			%increase_start(dt_start=dhms(datepart(&start_fix), &hour_TRAIN, 0, 0));
		%end;
		
	%end;
	
%mend Emergenza_Training;

/* in produzione ho l'ora solare quindi non devo fare aggiustamenti */

%Emergenza_Training();

data time_execution;
	dt_start = &start_fix;
	dt_end = datetime();
	time_execution = dt_end - dt_start;
	format dt: datetime19.;
	call symputx('time_execution', time_execution);
run;

%put "========================================================================";
%put "IL CODICE SAS E' GIUNTO AL TERMINE IN: &time_execution. SECONDI";

cas mySession terminate;
