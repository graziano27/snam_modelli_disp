proc sort data=GEO_SIM.BEST_PREDS_GAIA out=BEST_PREDS_GAIA_sort;
	by area_id area_label hour_orologio;
run;

proc means data=BEST_PREDS_GAIA_sort noprint;
	by area_id area_label hour_orologio ;
	var mae_Msmc;
	output out=gaia_median median=median_mae_Msmc;
run;

data gaia_median;
	set gaia_median;
	if hour_orologio = 22 then key_modello = 1;
	else key_modello = 0;
run;

proc sort data=gaia_median out=gaia_median_sort(drop=_TYPE_ _FREQ_);
	by area_id area_label descending key_modello hour_orologio;
run;

proc transpose data=gaia_median_sort out= gaia_median_sort_t(drop=_NAME_);
	by area_id area_label;
	var median_mae_Msmc;
	id hour_orologio;
run;

data GEO_SIM.gaia_median_by_area;
	set gaia_median_sort_t;
run;
