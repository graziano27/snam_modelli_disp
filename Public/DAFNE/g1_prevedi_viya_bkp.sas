%put DOING GENERAZIONE RIGA DI INPUT, PREVISIONE, ENSEMBLE E SCRITTURA PREVISIONE G1;

%global logic_path lib_path;

%let logic_path = /Public/DAFNE/; 
%let lib_path = /opt/sas/viya/config/data/cas/default/dafne; 

filename P1 FILESRVC folderpath="&logic_path" filename='librerie_viya.sas';
filename P2 FILESRVC folderpath="&logic_path" filename='g1_master_inputprevisioni.sas';
%include P1;
%include P2;
