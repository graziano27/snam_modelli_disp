/*===============================================================*/
/***** rilascio 2020-04-XX - versione v1.0 *****/
/***** autori: Binda, Gregori, Oliva, Rizzolo *****/
/***** BIP S.p.a. *****/
/*===============================================================*/

/*============ Initialization dates ========*/
%let datetime_base=%sysfunc(datetime());
/* %let datetime_base="24MAR2020:13:25:00"dt; */

%let ora_incentivo=15;
%let fix_pubb=1;
%let hour_list= (8, 13, 14, 15, 21, 5);

data _null_;
/*	to_mail="'silvia.lameri@snam.it' 'alessia.borroni@snam.it'";*/
/*	cc_mail="'alessandro.melillo@snam.it' 'fabio.castiglione@snam.it' 'marco.merli@snam.it'*/
/*			'supporto.siprogas@snam.it' 'andrea.magatti@mail-bip.com' 'carlo.binda@mail-bip.com'*/
/*			'clio.rizzolo@mail-bip.com' 'giacomo.gregori@mail-bip.com'";*/
	to_mail="'clio.rizzolo@mail-bip.com'";
	cc_mail="";
	call symput("to_mail",to_mail);
	call symput("cc_mail",cc_mail);
run;

data set_datetime_hour;
	dt_now=&datetime_base;
	hour_base=hour(dt_now);
	min_base=minute(dt_now);
	tday_base=datepart(dt_now);
	dt_now_fix=intnx("dthour",dt_now,0, 'B');
	hour_fix=hour(dt_now_fix);
	call symputx('hour',hour_fix);
	call symputx('datetime_now',dt_now_fix);
	format dt: datetime19. tday_base date9.;
run;

data dates;
	dt_now=&datetime_now;
	gday_sistema = datepart(dt_now);
	hour = (hour(dt_now));
	
	if hour<6 then gday_gas_sistema=intnx("day",gday_sistema,-1);
	else gday_gas_sistema=gday_sistema;

	g1day_gas_sistema=intnx("day",gday_gas_sistema,1);
	g2day_gas_sistema=intnx("day",gday_gas_sistema,2);
	g3day_gas_sistema=intnx("day",gday_gas_sistema,3);
	g4day_gas_sistema=intnx("day",gday_gas_sistema,4);

	gm1day_gas_sistema=intnx("day",gday_gas_sistema,-1);
	gm2day_gas_sistema=intnx("day",gday_gas_sistema,-2);


	if ((nwkdom(5, 1, 3, year(gm1day_gas_sistema)) <= gm1day_gas_sistema and  
		gm1day_gas_sistema< nwkdom(5, 1, 10, year(gm1day_gas_sistema)))) then
		do;
			hour_pre_incentivo = &ora_incentivo-3;
			hour_incentivo = hour_pre_incentivo+1;
		end;

	else if ((gm1day_gas < nwkdom(5, 1, 3, year(gm1day_gas_sistema)) or gm1day_gas_sistema >= nwkdom(5, 1,10, year(gm1day_gas_sistema)))) then
		do;
			hour_pre_incentivo = &ora_incentivo-2;
			hour_incentivo =hour_pre_incentivo+1;
	end;


	if ((nwkdom(5, 1, 3, year(gm2day_gas_sistema)) <= gm2day_gas_sistema and  
		gm2day_gas_sistema< nwkdom(5, 1, 10, year(gm2day_gas_sistema)))) then
		do;
			/* Se vera significa che siamo in stagione estiva */
			hour_pre_incentivo_yday2 = &ora_incentivo-3;

			*12;
			hour_incentivo_yday2 = hour_pre_incentivo+1;

			*13;
		end;
	else if ((gm2day_gas_sistema < nwkdom(5, 1, 3, year(gm2day_gas_sistema)) or gm2day_gas_sistema >= nwkdom(5, 1,10, year(gm2day_gas_sistema)))) then
		do;
			/* Se vera significa che siamo in stagione invernale */
			hour_pre_incentivo_yday2 = &ora_incentivo-2;

			*13;
			hour_incentivo_yday2 = hour_pre_incentivo+1;

			*14;
	end;

	dt_ora_legale_inizio=dhms(nwkdom(5, 1, 3, year(gday_sistema)),2,0,0);
	dt_ora_legale_fine=dhms(nwkdom(5, 1,10, year(gday_sistema)),2,0,0);

	if dt_now >= dt_ora_legale_inizio and  dt_now < dt_ora_legale_fine then
		do;
			/*	ora sistema = ora orologio - 1->  porto avanti di 1 ora l'ora di sistema (es: alle 3 d'orologio sono alle 2 di sistema)*/
			dt_orologio=intnx("dthour", dt_now, 1);
		end;
	else if dt_now < dt_ora_legale_inizio or dt_now >= dt_ora_legale_fine then
		do;
			/*	ora sistema = ora orologio->  porto avanti di 1 ora;*/
			dt_orologio=intnx("dthour", dt_now, 0);
		end;

	dt_pubblicazione = intnx("dthour", dt_orologio, 1);
	hour_pubb = (hour(dt_pubblicazione));
	gday_pubb = datepart(dt_pubblicazione);

	if hour_pubb<6 then
		gday_gas_pubb=intnx("day",gday_pubb,-1);
	else gday_gas_pubb=gday_pubb;


	if gday_gas_sistema = gday_gas_pubb then
		flag_pubb_calc=1;
	else flag_pubb_calc=0;

	if &fix_pubb=1 then do;
		gday=gday_pubb;
		gday_gas=gday_gas_pubb;
	end;
	else do;
		flag_pubb_calc=1;
	end;

	g1day_gas=intnx("day",gday_gas,1);
	g2day_gas=intnx("day",gday_gas,2);
	g3day_gas=intnx("day",gday_gas,3);
	g4day_gas=intnx("day",gday_gas,4);
	gm1day_gas=intnx("day",gday_gas,-1);
	gm2day_gas=intnx("day",gday_gas,-2);
	call symputx("hour_pubb", hour_pubb);
	call symput('hour',hour);
	call symputx("hour_inc_yday", hour_incentivo);
	call symputx("hour_inc_yday2", hour_incentivo_yday2);
	call symput('gday',gday);
	call symput('gday_gas',gday_gas);
	call symput('g1day_gas', g1day_gas);
	call symput('g2day_gas', g2day_gas);
	call symput('g3day_gas', g3day_gas);
	call symput('g4day_gas', g4day_gas);
	call symput('gm1day_gas', gm1day_gas);
	call symput('gm2day_gas', gm2day_gas);
	call symput('gday_gas_sistema',gday_gas_sistema);
	call symput('g1day_gas_sistema', g1day_gas_sistema);
	call symput('gm1day_gas_sistema', gm1day_gas_sistema);
	call symputx("flag_pubb_calc", flag_pubb_calc);
	format dt: datetime19.;
	format g: date9.;
run;

/* Report dates */
data dates_update_change;
	length pdf_filename $30.;
	gday=&gday;
	hh=&hour;
	hh_pub=&hour_pubb;
	gday_gas=&gday_gas;
	dayg=strip(put(gday_gas, ITADFWDX17.));
	dayg1=strip(put(&g1day_gas, ITADFWDX17.));
	dayg2=strip(put(&g2day_gas, ITADFWDX17.));
	dayg3=strip(put(&g3day_gas, ITADFWDX17.));
	dayg4=strip(put(&g4day_gas, ITADFWDX17.));
	daygm1=strip(put(&gm1day_gas, ITADFWDX17.));
	temp_date=tranwrd(put(gday_gas, yymmddp10.),".","_");;
	pdf_filename=compress(cat("Report_",temp_date,"_h", put(hh_pub,z2.0),".pdf"));
	call symput('dayg',dayg);
	call symput('dayg1', dayg1);
	call symput('dayg2', dayg2);
	call symput('dayg3', dayg3);
	call symput('dayg4', dayg4);
	call symput('daygm1',daygm1);
	call symput('pdf_filename',compress(pdf_filename));
	format g: date9.;
run;


/*============ Input Parameters ========*/

/* !!MODIFY!! Da cambiare con %let automatiche partendo da tabella */

/* Bilancio Commerciale */
%let input_bilanciosnam1=webdips.prev_bilancio;
%let input_bilanciosnam=work.bilanciosnam_final;

/* Riconsegnato Totale da SCADA */
%let input_riconsegnato1=oralpbp2.riconsegnato;

/* Previsioni */
%let tabella_input_preds = webdips.prev_giornaliere;
%let tabella_input_preds_viya=webdips.prev_giornaliere_viya;

/* Input Terna */
%let input_termo=oralpbp2.d_e_up_new;
%let input_terna=oralpbp2.d_e_terna;
%let input_up=oralpbp2.d_e_up;

/* input remi */
%let input_anag_remi=webdips.anag_remi;
%let input_anag_remi_attr=webdips.anag_remi_attr;
%let input_vol_val=oralpbp2.d_e_volumi_validati;
%let input_sime2=oralpbp2.d_e_misure_sime_2;
%let input_sime=oralpbp2.d_e_misure_sime;

/* Model Lists */
%let modelli_list_g_diana = ("Predizione G per G - DIANA");
%let modelli_list_g1 = ("Predizione G per G+1", "Predizione G per G+1 - 422", "Predizione G per G+1 - B", "Predizione G per G+1 - Autoreg",
	"Predizione G per G+1 - Full", "Predizione G per G+1 - Terna",  "Predizione G per G+1 - Sim");
%let modelli_list_g1_dafne = ("Predizione G per G+1");
%let tipoprev_list_utenze= ("Termoelettrico", "Civile", "Industriale+Residuo");
%let modelli_list_g2 = ("Predizione G per G+2");
%let modelli_list_g3 = ("Predizione G per G+3");
%let modelli_list_g4 = ("Predizione G per G+4");
%let modelli_list_rictot=("Predizione G per G - DIANA", "Predizione G per G+1", "Predizione G per G+2",
	"Predizione G per G+3", "Predizione G per G+4");

/* Temp Datasets */
%let output_riconsegnato=output_riconsegnato;
%let output_predictions_wide=output_predictions_wide;
%let output_predictions_long=output_predictions_long;
%let predictions_targets_tot_long=predictions_targets_tot_long;
%let predictions_targets_ut_long=predictions_targets_ut_long;
%let predictions_targets_wide=predictions_targets_wide;

/* Temp Variables */
%let ora_incentivo=15;
%let palette_list1=(navy CX1349BD CX2E6EB6 CX3f87bd cx5f9ebd );
%let palette_list2=(CX3f87bd CXd9534f CX373d54 CX5bc0de  CX5cb85c CX966842 CXffc425 CXd6d7d3);
%let palette_list3=(CX3f87bd cx5f9ebd );
%let palette_list_ut=(CX4db449 CXb2b2b2 CXf9ff00);
%let marker_list=(CircleFilled DiamondFilled SquareFilled StarFilled TriangleFilled );

/* parametri per incentivi */
%let start_per2 = "01OCT2017"d;
%let start_per3 = "01MAR2019"d;
%let end_per3 = "31DEC2021"d;
%let mesi_inverno = (11,12,1,2,3);
%let mesi_estate = (4,5,6,7,8,9,10);
%let m_1617 = 14000*100;
%let interc_old = 70000;
%let mape_max = 0.1;
%let m1_1718 = &m_1617;
%let m2_1718 = 22500*100;
%let interc1_inv_1718 = &interc_old;
%let interc2_inv_1718 = 155000;
%let interc1_est_1718 = 77000;
%let interc2_est_1718 = 162000;
%let mape_max_est_19 = 0.09581;
%let m1_est_19 = 13127.27*100;
%let interc1_est_19 = 72200;

/* soglie  modello G */
%let soglia_mae=26.432;
%let soglia_salto_prev=26.432;

data mymap;
	length id $40 value $40 markersymbol $40;
	infile datalines delimiter=',';
	retain id "mytest";
	input value $ markersymbol $;
	datalines;
	p_riconsegnato_tot_g4, CircleFilled
	p_riconsegnato_tot_g3, DiamondFilled
	p_riconsegnato_tot_g2, SquareFilled
	p_riconsegnato_tot_g1, StarFilled
	p_riconsegnato_tot_g_diana, TriangleFilled
;
run;