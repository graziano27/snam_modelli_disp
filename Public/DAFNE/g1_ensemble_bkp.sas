
/*============ Definizione Path ================================*/
%let lib_input=&lib_physical;
%let lib_output=&lib_physical;

/*============ Definizione Macro Variabili =====================*/
%global terna_new cut_date n_records terna_pre13;
%let dt_solare=&start;
%let id_model_select = 35 36 37 38 39 40; /* id dei sottomodelli su cui fare ensemble */
%let cut_date_storico="01OCT2016"d;

/*============ Definizione Tabelle =====================*/
%let preds_in = &lib_output..g1_previsioni_riconsegnato;
%let riga_input = &lib_output..&output_final_name;
%let ensemble_dataset = &lib_output..g1_rictot_ensemble_dataset;
%let input_ensemble_prev = &lib_output..g1_rictot_inputensemble;
%let ensemble_dataset_fortrain = ensemble_dataset_c; /* copia in work del dataset ensemble_dataset + performance (da usare per il training e la prev) */

%let input_bilanciosnam1=webdips.prev_bilancio;
%let input_bilanciosnam=work.bilanciosnam_final;
%let output_target=work.riconsegnato_uff_target;
%let input_dlp_orario=oralpbp2.analisi_dlp_orario;
%let input_pcs_misura=webdips.pcs_misura_aop9100;

%let step = 14;
%let types = mvgavg month timeg1 timeg2 timefest;

%let this_step = 14;
%let types = mvgavg month timeg1 timeg2 timefest;
%let M_Weight_mvgavg_s = 0.34;
%let M_Weight_time_s = 0.33;
%let Festivi_balance = 0.8;
%let M_Weight_month = 0.5;
%let M_Weight_mvgavg = 0.6;

%let k=1.77;
%let Weight_Strong1 = 1;
%let Weight_Strong2 = 0.95;
%let Weight_Strong3 = 0.9;
%let Weight_Strong4 = 0.85;
%let Weight_Strong5 = 0.8;


/**********************************************/
/* step 0 
0) ricavare informazioni temporali e logica che userò per ensemble (media semplice o DAFNE) */

data time_info;
	G_solare=datepart(&dt_solare);
	hour=hour(&dt_solare);
	G_gas=ifn(hour<6, intnx("day", G_solare, -1), G_solare);
	data_gas_g1=intnx("day", G_gas,1);
	month = month(data_gas_g1);
	time_day = weekday(data_gas_g1);
	* giorno della settimana;
	month_day = day(G_solare - 1);
	* giorno del mese di cui ho effettivamente i consuntivi;
	time_day_group1 = 0;
	* il gruppo 1 sono i giorni feriali;
	time_day_group2 = 0;
	* il gruppo 2 sono i weekend;
	time_festivo = 0;
	* giorni festivi;
	if day(data_gas_g1) = 1 and month(data_gas_g1) = 1 then
		time_festivo = 1;
	if day(data_gas_g1) = 6 and month(data_gas_g1) = 1 then
		time_festivo = 1;
	if day(data_gas_g1) = 25 and month(data_gas_g1) = 4 then
		time_festivo = 1;
	if day(data_gas_g1) = 1 and month(data_gas_g1) = 5 then
		time_festivo = 1;
	if day(data_gas_g1) = 2 and month(data_gas_g1) = 6 then
		time_festivo = 1;
	if day(data_gas_g1) = 15 and month(data_gas_g1) = 8 then
		time_festivo = 1;
	if day(data_gas_g1) = 1 and month(data_gas_g1) = 11 then
		time_festivo = 1;
	if day(data_gas_g1) = 8 and month(data_gas_g1) = 12 then
		time_festivo = 1;
	if data_gas_g1 = holiday('EASTER',year(data_gas_g1)) then
		time_festivo = 1;
	if data_gas_g1 = intnx('day',holiday('EASTER',year(data_gas_g1)),1) then
		time_festivo = 1;
	if day(data_gas_g1) = 25 and month(data_gas_g1) = 12 then
		time_festivo = 1;
	if day(data_gas_g1) = 26 and month(data_gas_g1) = 12 then
		time_festivo = 1;
	if day(data_gas_g1) = 31 and month(data_gas_g1) = 12 then
		time_festivo = 1;
	if time_day in (2, 3, 4, 5, 6) and time_festivo = 0 then
		time_day_group1 = 1;
	if time_day in (1, 7) or time_festivo = 1 then
		time_day_group2 = 1;
	call symputx("hour_pred", hour);
	call symputx("month_pred", month);
	call symputx("time_day_group1_pred", time_day_group1);
	call symputx("time_day_group2_pred", time_day_group2);
	call symputx("time_festivo_pred", time_festivo);
	call symputx("month_day", month_day);
	call symputx("date_pred", data_gas_g1);
	call symputx("G_gas",G_gas);
	call symputx("G_solare",G_solare);
	format data_gas_g1 G: date9.;
run;

data hour_inc;
	set time_info;
	if ((nwkdom(5, 1, 3, year(&G_solare))) <= &G_solare and  
		&G_solare< nwkdom(5, 1, 10, year(&G_solare))) then
		do;
			/* Se vera significa che siamo in stagione estiva */
			hour_inc = 13;
			hour_pre_inc = 12;
		end;
	else if (&G_solare < nwkdom(5, 1, 3, year(&G_solare)) or &G_solare >= nwkdom(5, 1,10, year(&G_solare))) then
		do;
			hour_inc = 14;
			hour_pre_inc = 13;

		end;
	call symputx("hour_inc", hour_inc);
	call symputx("hour_pre_inc", hour_pre_inc);
run;


/* Step 1 
- Prendo le previsioni dei sottomodelli e le traspongo salvandole nella tabella work.preds_merge;
- Salvo lo storico dell orario da incentivo delle previsioni dei sottomodelli G1 TOT nella tab &ensemble_dataset */

%macro merge_predictions(preds_in, id_model_select, preds_out);

	data en_preds;
		set &preds_in. (where=(id in (&id_model_select.)));
	run;

	proc sort data=en_preds;
		by datetime_solare;
	run;

	data _null_;
		set en_preds end=eof;
		by datetime_solare;
		if eof;
		call symputx("datetime_last",datetime_solare);
	run;

	data last_preds (drop=sottomodello rename=(sottomodello_new=sottomodello));
		length sottomodello_new $10;
		set en_preds;
		if datetime_solare=&datetime_last;
		if sottomodello="AR" then sottomodello_new="AutoReg";
		else if sottomodello="T" then sottomodello_new="Terna";
		else if sottomodello in ("S1" "S2") then sottomodello_new="Similarity";
		else if sottomodello="O" then sottomodello_new="Old422";
		else if sottomodello="F" then sottomodello_new="Full";
		else if sottomodello="B" then sottomodello_new="ModB";
	run; 

	proc sort data=last_preds nodupkey;
		by datetime_solare id;
	run;

	proc transpose data=last_preds out=&preds_out. (drop=_name_ datetime_solare
				rename=(data_gas_rif=data_gas_g1)) prefix=P_;
		by datetime_solare data_gas_rif hour;
		var previsione;
		id sottomodello;
	run;

%mend merge_predictions;

%macro update_dataset(ensemble_dataset, hour_pred,hour_inc, ds_to_add);

	%if &hour_pred. = &hour_inc %then %do;

		proc append base=&ensemble_dataset. data=&ds_to_add. force;
		run;

	%end;

%mend;

%merge_predictions(&preds_in, &id_model_select, preds_merge);
%update_dataset(&ensemble_dataset, &hour_pred, &hour_inc, preds_merge);


/* Step 2 
- joinare con il target e calcolo relative performance (MAPE) */

/* macro per il calcolo del consuntivo, se lanciato prima del caricamento del file di bilancio
viene preso automaticamente il valore del riconsegnato SCADA */

%macro GxG_create_dataset_target();

	data bilancio_lim;
		if day(&G_gas)>10 then
			filter=intnx("month", &G_gas,-2, "END");
		else filter=intnx("month", &G_gas,-3, "END");
		call symput("bilancio_limit", filter);
		format filter date9.;
	run;

	data &input_bilanciosnam;
		set &input_bilanciosnam1(where=(fl_deleted eq "0"));
		drop k_storico data_elaborazione fl_deleted k_prev_bilancio fl_azione utente_azione dt_azione data;
		date=datepart(data);
		if date<=&bilancio_limit then validato=1;
			else validato=0;
		if validato eq . then validato = 0;
		format date date9.;
	run;

	proc sort data=&input_bilanciosnam out=&input_bilanciosnam;
		by date;
	run;

	/*prendo il bilancio ufficiale snam*/
	data bilancio_snam;
		set &input_bilanciosnam(rename=(date=data));
		keep data Importazioni Entrata_Tarvisio	Entrata_Gela Entrata_Gorizia Entrata_Mazara	Entrata_P_Gries	
			GNL_Cavarzere	GNL_Livorno	GNL_Panigaglia	Produzione_Nazionale Sistemi_di_stoccaggio Stogit Edison_Stoccaggio
			Totale_Immesso Riconsegne_rete_Snam_Rete_Gas Industriale Termoelettrico Reti_di_distribuzione Uscita_Bizzarone
			Uscita_Gorizia	Uscita_P_Gries	Uscita_S_Marino	Uscita_Tarvisio Riconsegnato_ad_altre_Reti_tr;
		rename Riconsegnato_ad_altre_Reti_tr=Ric_Reti_terzi;
	run;

	data riconsegnato;
		set bilancio_snam(where=(data>="01JAN2011"d));
		rename data=data_gas_merge;
		uscite=sum(of uscit:);
		target_con_dlp=sum(Totale_Immesso,-uscite);
	run;

	/* ordino i dati per datetime solare, che Ã¨ riferito a scada.
	il dato del giorno G alle ore H mi dice il gas associato a una certa tipologia punto che Ã¨ stato misurato
	fra le ore H e le H:59 */

	/*da analisi_dlp_orario*/
	data dlp_orariowhere(where=(data_gas>="01JAN2011"d AND data_gas<today()));
		set &input_dlp_orario;
		data_gas=ifn(hour(dt_calcolo)<6, intnx("day",datepart(dt_calcolo),-1), datepart(dt_calcolo));
		format data_gas date9.;
	run;

	proc sql noprint;
		CREATE TABLE WORK.dlp_day AS 
			SELECT t1.data_gas, 
				/* SUM_of_DELTA_LINEPACK */
		(SUM(t1.DELTA_LINEPACK)) AS DELTA_LINEPACK_ab FROM WORK.DLP_ORARIOWHERE t1 GROUP BY t1.data_gas;
	QUIT;

	proc sort data=dlp_day out=dlp_2_smc;
		by data_gas;
	run;

	/*da convertire con PCS di misura!*/
	data pcs_misura;
		set &input_pcs_misura;
		data_gas=datepart(data);
		keep data_gas PCS_kwh_Sm3;
		format data_gas date9.;

		if PCS_kwh_Sm3 ne .;
	run;

	proc sort data=pcs_misura out=pcs_misura_sort nodupkey;
		by data_gas;
	run;

	data dlp_2(keep=data_gas dlp_ab);
		merge dlp_2_smc(in=p) pcs_misura_sort(in=q);
		by data_gas;
		dlp_ab=delta_linepack_ab* PCS_kwh_Sm3;

		if p and q;
	run;

	/*da bilancio ufficiale quando Ã¨ validato*/
	data bilancio;
		set &input_bilanciosnam(rename=(date=data_gas));
		keep data_gas Delta_Line_Pack_SRG;

		if Validato=1 and Delta_Line_Pack_SRG ne .;
		Delta_Line_Pack_SRG=Delta_Line_Pack_SRG*1000000;
	run;

	proc sort data=bilancio out=dlp_3;
		by data_gas;
	run;

	/* merge con calendario giorno*/
	%let end_date=&date_pred;

	data calendario_giorno;
		data_gas="01OCT2015"d;

		do while (data_gas<&end_date);
			output;
			data_gas=intnx("day", data_gas, 1, 's');
		end;

		format data_gas date9.;
	run;

	data dlp_final(keep=data_gas deltalinepack_final rename=(data_gas=data_gas_merge));
		merge calendario_giorno(in=p where=(data_gas>="01JAN2011"d)) dlp_2 dlp_3;
		by data_gas;

		if (Delta_Line_Pack_SRG ne .) then
			deltalinepack_final=Delta_Line_Pack_SRG;
		else if (dlp_ab ne .) then
			deltalinepack_final=dlp_ab;
	run;

	/*mette dlp con risultato del bilancio*/
	proc sort data=riconsegnato;
		by data_gas_merge;
	run;

	data riconsegnato_final;
		merge riconsegnato(keep=target_con_dlp data_gas_merge in=p)  dlp_final(in=q);
		by data_gas_merge;

		if p and q;
		target_con_dlp=target_con_dlp*1000000;
		target=target_con_dlp-deltalinepack_final;
	run;

	/*ho cosi il target ricostruito da Marzo 2011*/
	data riconsegnato_final;
		set riconsegnato_final;
		rename target=Riconsegnato_Consuntivo_KWH data_gas_merge=data_gas_g1;
	run;

%mend GxG_create_dataset_target;

%GxG_create_dataset_target();


/* Aggiornamento Performance della &ensemble_dataset */
data &ensemble_dataset;
	merge &ensemble_dataset(in=L) riconsegnato_final(in=R keep=Riconsegnato_Consuntivo_KWH data_gas_g1);
	by data_gas_g1;
	if L;
	array models[6] P_:;
	array mape[6] mape:;
	do i = 1 to dim(mape);
		mape[i] = abs(models[i] - Riconsegnato_Consuntivo_KWH)/Riconsegnato_Consuntivo_KWH;
	end;

	drop i Riconsegnato_Consuntivo_KWH;
run;


/* Finisce qui la parte di scrittura sul dataset di partenza, da usare per il training e la previsione */
data &ensemble_dataset_fortrain;
	set &ensemble_dataset;
run;

/* Step 3 
verificare:
1. se il nuovo dato terna è arrivato
2. uscito il bilancio di ieri

--> terna_mgp_giorn Ã¨ G1_terna_mgp_day
VERIFICO CHE terna_mgp_giorn nel ds &input.T(	where data_gas_g1 = &date_pred) Ã¨ la stessa di 
G1_terna_mgp_day nel inputmodelli_viya (G1_data_gas_rif= &date_pred)
*/

proc sql noprint;
	select count(*) into: flag_bil
	from &ensemble_dataset
	where data_gas_g1 = intnx("day", &date_pred, -2) and mape_Old422 NE .;
quit;

%macro gestione_terna_pre13(input_ds, var_terna, time_id, date_pred );

/* %let input_ds=&riga_input.; */
/* %let var_terna=G1_terna_mgp_day;  */
/* %let time_id=G1_data_gas_rif;  */
/* %let date_pred=&date_pred; */

	proc sql noprint;
		select count(*) into: n_records
			from &input_ds.;
	quit;


	%IF &n_records > 24 %THEN
		%DO;

			proc sql noprint;
				select round(&var_terna.,0.1) into: terna_pre13
					from &input_ds.
					where &time_id. = &date_pred. and hour between 6 and 12 and &var_terna. not is missing;
			quit;

		%END;
	%ELSE
		%DO;

			data _null_;
				call symputx("terna_pre13", -1);
			run;

		%END;

	data check_T;
		set &riga_input. end=eof;
		if &time_id. >= intnx("day", &date_pred, -2);
		terna_pre13=&terna_pre13;
		terna_mgp_giorn_round=round(&var_terna.,0.1);
		if eof then
			do;
				if hour >= 13 or hour < 6 then
					do;
						if terna_mgp_giorn_round NE terna_pre13 and terna_mgp_giorn_round NE . then
							do;
								terna_new = 1;
								call symputx("terna_new", terna_new);
							end;
						else
							do;
								terna_new = 0;
								call symputx("terna_new", 0);
							end;
					end;
				else if hour >= 6 and hour <= 12 then
					do;
						terna_new = 1;
						call symputx("terna_new", terna_new);
					end;

				output;
			end;
		keep &time_id. hour &var_terna. terna_new;
	run;

%mend;

%gestione_terna_pre13(&riga_input., G1_terna_mgp_day, G1_data_gas_rif, &date_pred);

/* Step 4 */
/* Inizio scegliendo la logica di ensembling:
- DAFNE si attiva solo nelle ore pre-incentivo ed incentivo, verificato l'arrivo del dato Terna 
- nelle altre ore è operativo l'ensemble a media semplice 
- la media semplice interviene anche nelle ore pre-incentivo ed incentivo, nel caso in cui il dato Terna non sia arrivato:
in questo caso, infatti, è opportuno far intervenire solo i modelli che non usano il dato Terna. Avendo questi modelli
performance simili, un ensemble a media semplice è opportuno */

data method;
	length ensemble_method $ 32;
	if &hour_pred in (&hour_pre_inc, &hour_inc) and &terna_new = 1 then do;
		ensemble_method = 'DAFNE';
		flag_DAFNE = 1;
	end;
	else if &hour_pred in (&hour_pre_inc, &hour_inc) and &terna_new = 0 then do;
		ensemble_method = 'Media Semplice';
		flag_DAFNE = 0;
	end;
	else if &hour_pred not in (&hour_pre_inc, &hour_inc) then do;
		ensemble_method = 'Media Semplice';
		flag_DAFNE = 0;
	end;
	call symputx("ensemble_method", ensemble_method);
	call symputx("flag_DAFNE", flag_DAFNE);
run;

%MACRO prepare_data(ds_in=, ds_out1=, ds_out2=,k=); 

	data &ds_out1;
		set &ds_in;
/*		it = _N_;*/
		time_day = weekday(data_gas_g1);
		time_day_group1 = 0;
		time_day_group2 = 0;
		time_festivo = 0;

		if day(data_gas_g1) = 1 and month(data_gas_g1) = 1 then
			time_festivo = 1;

		if day(data_gas_g1) = 6 and month(data_gas_g1) = 1 then
			time_festivo = 1;

		if day(data_gas_g1) = 25 and month(data_gas_g1) = 4 then
			time_festivo = 1;

		if day(data_gas_g1) = 1 and month(data_gas_g1) = 5 then
			time_festivo = 1;

		if day(data_gas_g1) = 2 and month(data_gas_g1) = 6 then
			time_festivo = 1;

		if day(data_gas_g1) = 15 and month(data_gas_g1) = 8 then
			time_festivo = 1;

		if day(data_gas_g1) = 1 and month(data_gas_g1) = 11 then
			time_festivo = 1;

		if day(data_gas_g1) = 8 and month(data_gas_g1) = 12 then
			time_festivo = 1;

		if data_gas_g1 = holiday('EASTER',year(data_gas_g1)) then
			time_festivo = 1;

		if data_gas_g1 = intnx('day',holiday('EASTER',year(data_gas_g1)),1) then
			time_festivo = 1;

		if day(data_gas_g1) = 25 and month(data_gas_g1) = 12 then
			time_festivo = 1;

		if day(data_gas_g1) = 26 and month(data_gas_g1) = 12 then
			time_festivo = 1;

		if day(data_gas_g1) = 31 and month(data_gas_g1) = 12 then
			time_festivo = 1;

		if time_day in (2, 3, 4, 5, 6) and time_festivo = 0 then
			time_day_group1 = 1;

		if time_day in (1, 7) or time_festivo = 1 then
			time_day_group2 = 1;
		month = month(data_gas_g1);
		month_day = day(data_gas_g1);
		days_of_mape = month_day - 2;
	run;

	/* Se previsione si trova al di fuori dell'IC viene sostituita con la media 
		delle previsioni che si trovano all'interno dell'IC
		NB: I MAPE RIMANGONO UGUALI INVECE */

	data &ds_out2;
		set &ds_out1;
		array preds[*] P_:;
		mean_preds = mean(of preds[*]);
		std_preds = std(of preds[*]);
		cum_ok=0;
		count_ok=0;
		lower_limit = mean_preds - &k*std_preds;
		upper_limit = mean_preds + &k*std_preds;

		do i = 1 to dim(preds);
			if  preds[i] > lower_limit and preds[i] < upper_limit then
				do;
					cum_ok + preds[i];
					count_ok + 1;
				end;
		end;

		mean_ok = cum_ok/count_ok;

		do i = 1 to dim(preds);
			if preds[i] > upper_limit or preds[i] < lower_limit then
				preds[i] = mean_ok;
			else do;
				preds[i] = preds[i];
			end;
		end;
		drop i;
	run;

%MEND;


%MACRO get_train_back(cut_date_storico, dataset, this_data=current_date);

	proc datasets lib=work;
		delete train:;
	run;

	%IF &flag_bil = 1 %THEN
		%DO;

			data train;
				set &dataset end=eof;
				where data_gas_G1 <= intnx("day", &this_data, -2);
			run;

			data _null_;
				call symputx("cut_date", intnx("day", &this_data, -2));
			run;

		%END;

	%IF &flag_bil = 0 %THEN
		%DO;

			data train;
				set &dataset end=eof;
				where data_gas_G1 <= intnx("day", &this_data, -3);
			run;

			data _null_;
				call symputx("cut_date", intnx("day", &this_data, -3));
			run;

		%END;

	data train_mvg_avg train_month train_timeday_g1 train_timeday_g2 train_festivi;
		set train (where=(data_gas_g1 ge &cut_date_storico));
		array mape[6] mape:;

		if month = month(&this_data) and year(data_gas_g1) = year(&this_data) then
			do;
				do i = 1 to dim(mape);
					if mape[i] > 0.4 then
						mape[i] = 0.4;
				end;

				output train_mvg_avg;
			end;

		if month = month(&this_data) then
			do;
				do i = 1 to dim(mape);
					if mape[i] > 0.4 then
						mape[i] = 0.4;
				end;

				output train_month;
			end;

		if time_day_group1 = 1 then
			do;
				do i = 1 to dim(mape);
					if mape[i] > 0.4 then
						mape[i] = 0.4;
				end;

				output train_timeday_g1;
			end;

		if time_day_group2 = 1 then
			do;
				do i = 1 to dim(mape);
					if mape[i] > 0.4 then
						mape[i] = 0.4;
				end;

				output train_timeday_g2;
			end;

		if time_festivo = 1 then
			do;
				do i = 1 to dim(mape);
					if mape[i] > 0.4 then
						mape[i] = 0.4;
				end;

				output train_festivi;
			end;

		keep data_gas_G1 mape:;
	run;

%MEND;

%MACRO get_mape_types(step=step, this_cut_date=cut_date);
	/* calcolo i pesi: 
	- step Ã¨ il parametro di window della moving average
	- cut_date invece Ã¨ la data di taglio dei vari training set, ovvero la data dell'ultimo consuntivo 
	disponibile */

	/* moving average mape */
	proc sql noprint;
		select count(*) into: n_records_mvgavg
			from train_mvg_avg;
	quit;

	%IF &n_records_mvgavg > 0 %THEN
		%DO;

			proc contents data=train_mvg_avg(keep=mape:) out=vars_mape(keep=NAME);
			run;

			data text;
				length name_str $ 2000;
				set vars_mape end=eof;
				retain name_str;

				if _N_ = 1 then
					name_str = catx(" ", "convert", NAME, "=", cat("mvgavg_", strip(NAME), " / 
					transout=(movave ", "&step);"));
				else
					do;
						name_new = catx(" ", "convert", NAME, "=", cat("mvgavg_", strip(NAME), " / 
							transout=(movave ", "&step);"));
						name_str = catx(" ", name_str, name_new);
					end;

				if eof then
					call symputx("text_expand", name_str);
			run;

			proc expand data=train_mvg_avg out=mape_mvgavg(keep=data_gas_g1 mvg: where=(data_gas_g1 = &this_cut_date)) 
				method=none;
				id data_gas_G1;
				&text_expand;
			run;

		%END;

	%IF &n_records_mvgavg = 0 %THEN
		%DO;

			data mape_mvgavg;
				data_gas_g1 = &this_cut_date;
				mvgavg_mape_AutoReg = .;
				mvgavg_mape_Full = .;
				mvgavg_mape_ModB = .;
				mvgavg_mape_Old422 = .;
				mvgavg_mape_Similarity = .;
				mvgavg_mape_Terna = .;
				format data: date9.;
			run;

		%END;

	/* Month mape */
	proc sql noprint;
		create table mape_month as
			select T.* from (
			select &this_cut_date format=date9. as data_gas_g1, 
				mean(mape_autoreg) as month_mape_autoreg,
				mean(mape_full) as month_mape_full,
				mean(mape_modb) as month_mape_modb,
				mean(mape_old422) as month_mape_old422,
				mean(mape_similarity) as month_mape_similarity,
				mean(mape_terna) as month_mape_terna
			from train_month) T;
	run;

	/* Time day groups mape */
	proc sql noprint;
		create table mape_timeg1 as
			select T.* from ( 
			select &this_cut_date format=date9. as data_gas_g1,
				mean(mape_autoreg) as timeg1_mape_autoreg,
				mean(mape_full) as timeg1_mape_full,
				mean(mape_modb) as timeg1_mape_modb,
				mean(mape_old422) as timeg1_mape_old422,
				mean(mape_similarity) as timeg1_mape_similarity,
				mean(mape_terna) as timeg1_mape_terna
			from train_timeday_g1) T;
	run;

	proc sql noprint;
		create table mape_timeg2 as
			select T.* from ( 
			select &this_cut_date format=date9. as data_gas_g1,
				mean(mape_autoreg) as timeg2_mape_autoreg,
				mean(mape_full) as timeg2_mape_full,
				mean(mape_modb) as timeg2_mape_modb,
				mean(mape_old422) as timeg2_mape_old422,
				mean(mape_similarity) as timeg2_mape_similarity,
				mean(mape_terna) as timeg2_mape_terna
			from train_timeday_g2) T;
	run;

	proc sql noprint;
		create table mape_timefest as 	
			select T.* from ( 
			select &cut_date format=date9. as data_gas_g1,
				mean(mape_autoreg) as timefestivo_mape_autoreg,
				mean(mape_full) as timefestivo_mape_full,
				mean(mape_modb) as timefestivo_mape_modb,
				mean(mape_old422) as timefestivo_mape_old422,
				mean(mape_similarity) as timefestivo_mape_similarity,
				mean(mape_terna) as timefestivo_mape_terna
			from train_festivi) T;
	run;

%MEND;

%MACRO calculate_weights(type);

	data merge_mape_&type;
		set mape_&type end=eof;

		/*	by data_gas_g1;*/
		sum_mape_&type = sum(of &type:);
		array mape[6] &type:;
		array inv_weight[6];
		array weight_nn[6];
		array weight_simple[6];
		array weight[6] Weight_&type._mape_AutoReg Weight_&type._mape_Full Weight_&type._mape_ModB
			Weight_&type._mape_Old422 Weight_&type._mape_Similarity Weight_&type._mape_Terna;

		do i = 1 to dim(mape);
			inv_weight[i] = mape[i]/sum_mape_&type;
			weight_nn[i] = 1 - inv_weight[i];
		end;

		sum_weigth_nn = sum(of weight_nn:);

		do i = 1 to dim(mape);
			weight_simple[i] = weight_nn[i]/sum_weigth_nn;
		end;

		weight_mean = mean(of weight_simple[*]);

		do i = 1 to dim(weight);
			if weight_simple[i] < weight_mean then
				count + 1;
		end;

		if count = 1 then
			Weight_Strong = &Weight_Strong1;
		else if count = 2 then
			Weight_Strong = &Weight_Strong2;
		else if count = 3 then
			Weight_Strong = &Weight_Strong3;
		else if count = 4 then
			Weight_Strong = &Weight_Strong4;
		else if count = 5 then
			Weight_Strong = &Weight_Strong5;

		do i = 1 to dim(weight);
			if weight_simple[i] < weight_mean then
				weight[i] = (1 - Weight_Strong)/count;
			else weight[i] = Weight_Strong/(dim(weight)-count);
		end;

		drop i;
		format &type: sum: weight: inv: percent8.2;
		keep data_gas_g1 &type: weight_&type:;
	run;

%MEND;

%MACRO merge_weights();
	%let count = %sysfunc(countw(&types));

	%do i = 1 %to &count;
		%let family=%scan(&types,&i);

		%calculate_weights(&family);
	%end;

	data weights_all;
		merge merge_mape_mvgavg merge_mape_month merge_mape_timeg1 merge_mape_timeg2 merge_mape_timefest;
		by data_gas_g1;
	run;

%MEND;

%MACRO merge_with_predictions(dataset_in=, this_data=, dataset_out=);

/*%let this_data=&date_pred;*/
	proc datasets lib=work;
		delete &dataset_out. date_corrected;
	run;

	data date_corrected;
		set weights_all;
		data_gas_g1 = &this_data;
	run;

	%IF &hour_pred = &hour_inc. %THEN
		%DO;

			data &dataset_out.;
				length data_gas_G1 8 P_AutoReg 8 P_Full 8 P_ModB 8 P_Old422 8 P_Similarity 8 P_Terna 8 ;
				merge &dataset_in.(where=(data_gas_g1=&this_data) drop=hour mape:) 
					date_corrected;
				by data_gas_g1;

				if &flag_bil = 0 then
					days_of_mape = days_of_mape - 1;
				else days_of_mape = days_of_mape;
			run;

		%END;
	%ELSE /* se non sono le 13, nel dataset non sono state memorizzate le info di domani, quindi il merge 
		avrebbe delle colonne missing --> recupero queste info dal preds_merge */
		%DO;

			%prepare_data(ds_in=preds_merge, ds_out1=preds_merge, ds_out2=preds_merge_no_outlier, k=&k); 

			data &dataset_out.;
				length data_gas_G1 8 P_AutoReg 8 P_Full 8 P_ModB 8 P_Old422 8 P_Similarity 8 P_Terna 8 ;
				merge &dataset_in.(where=(data_gas_g1=&this_data) drop=hour mape: time: month: days: P_:) 
				date_corrected preds_merge_no_outlier(drop=hour);
				by data_gas_g1;

				if &flag_bil = 0 then
					days_of_mape = days_of_mape - 1;
				else days_of_mape = days_of_mape;
			run;

		%END;


%MEND;

%MACRO adjust_macro_weights();
	/* Devo gestire diverse condizioni:
	1) se tutti i pesi di ogni famiglia di mape sono nulli --> ensemble a media semplice (primi due giorni)
	2) sono a inizio mese
	3) ho superato inizio mese */
	data macro_weights;
		set merge_final;
		array weight_mvgavg[6] Weight_mvgavg:;
		array weight_month[6] Weight_month:;
		array weight_timeg1[6] Weight_timeg1:;
		array weight_timeg2[6] Weight_timeg2:;
		array weight_timefest[6] Weight_timefest:;

		if days_of_mape <= 0 then
			do;
				/* sono a inizio di un mese */
				macro_weight_mvgavg = 0;
				macro_weight_month = &M_Weight_month;

				if time_day_group1 = 1 then /* Ã¨ un giorno feriale */

					do;
						macro_weight_timeg1 = (1 - &M_Weight_month);
						macro_weight_timeg2 = 0;
						macro_weight_timefest = 0;
					end;
				else if time_day_group2 = 1 and time_festivo = 0 then /* Ã¨ un weekend non festivo */
					do;
						macro_weight_timeg1 = 0;
						macro_weight_timeg2 = (1 - &M_Weight_month);
						macro_weight_timefest = 0;
					end;
				else if time_festivo = 1 then /* Ã¨ un festivo */
					do;
						macro_weight_timeg1 = 0;
						macro_weight_timeg2 = (1 - &M_Weight_month)*(1 - &Festivi_balance);
						macro_weight_timefest = (1 - &M_Weight_month)*(&Festivi_balance);
					end;
			end;
		else if days_of_mape > 0 then /* ho superato inizio mese */

			if days_of_mape < &step then
				do;
					/* ma ho ancora poca memoria, devo fare affidamento anche sui mesi del passato */
					macro_weight_mvgavg = &M_Weight_mvgavg*(days_of_mape/&step);
					macro_weight_month = &M_Weight_mvgavg*((&step - days_of_mape)/&step);

					if time_day_group1 = 1 then /* Ã¨ un giorno feriale */

						do;
							macro_weight_timeg1 = (1 - &M_Weight_mvgavg);
							macro_weight_timeg2 = 0;
							macro_weight_timefest = 0;
						end;
					else if time_day_group2 = 1 and time_festivo = 0 then /* Ã¨ un weekend non festivo */
						do;
							macro_weight_timeg1 = 0;
							macro_weight_timeg2 = (1 - &M_Weight_mvgavg);
							macro_weight_timefest = 0;
						end;
					else if time_festivo = 1 then /* Ã¨ un festivo */
						do;
							macro_weight_timeg1 = 0;
							macro_weight_timeg2 = (1 - &M_Weight_mvgavg)*(1 - &Festivi_balance);
							macro_weight_timefest = (1 - &M_Weight_mvgavg)*(&Festivi_balance);
						end;
				end;
			else
				do;
					/* ho sufficiente memoria, considero solo il mvgavg */
					macro_weight_mvgavg = &M_Weight_mvgavg;
					macro_weight_month = 0;

					if time_day_group1 = 1 then /* Ã¨ un giorno feriale */

						do;
							macro_weight_timeg1 = (1 - &M_Weight_mvgavg);
							macro_weight_timeg2 = 0;
							macro_weight_timefest = 0;
						end;
					else if time_day_group2 = 1 and time_festivo = 0 then /* Ã¨ un weekend non festivo */
						do;
							macro_weight_timeg1 = 0;
							macro_weight_timeg2 = (1 - &M_Weight_mvgavg);
							macro_weight_timefest = 0;
						end;
					else if time_festivo = 1 then /* Ã¨ un festivo */
						do;
							macro_weight_timeg1 = 0;
							macro_weight_timeg2 = (1 - &M_Weight_mvgavg)*(1 - &Festivi_balance);
							macro_weight_timefest = (1 - &M_Weight_mvgavg)*(&Festivi_balance);
						end;
				end;
	run;

%MEND;

%MACRO ensemble_components(type);

	data ensemble_prediction_&type;
		set macro_weights;
		keep data_gas_g1 Weight_&type: macro_weight_&type Pred: Ens:;
		array models_pred[6] P_:;
		array weight_&type[6] Weight_&type:;
		array preds_weighted[6] Pred_weight_&type._AutoReg Pred_weight_&type._Full 
			Pred_weight_&type._ModB Pred_weight_&type._Old422 Pred_weight_&type._Similarity 
			Pred_weight_&type._Terna;

		do i = 1 to dim(models_pred);
			preds_weighted[i] = models_pred[i]*weight_&type[i];
		end;

		Ens_Pred_&type = sum(of pred:);

		if Ens_Pred_&type = . then
			Ens_Pred_&type = 0;
		Ens_Weighted_Pred_&type = macro_weight_&type*Ens_Pred_&type;

		if Ens_Weighted_Pred_&type = . then
			Ens_Weighted_Pred_&type = 0;
	run;

%MEND;

%MACRO calculate_ensemble_prediction();
	%let count = %sysfunc(countw(&types));

	%do i = 1 %to &count;
		%let family=%scan(&types,&i);

		%ensemble_components(&family);
	%end;

	data ensemble_components_all;
		merge ensemble_prediction_mvgavg ensemble_prediction_month ensemble_prediction_timeg1 
			ensemble_prediction_timeg2 ensemble_prediction_timefest;
		by data_gas_g1;
	run;

	data old_vars;
		set macro_weights;
		drop weight: macro:;
	run;

	data ensemble_final_pred;
		length metodo_di_ensemble $ 32 hour 8;
		merge old_vars ensemble_components_all;
		by data_gas_g1;
		Ensemble_final_pred = sum(of Ens_Weighted_Pred_:);
		metodo_di_ensemble = "&ensemble_method";
		hour = &hour_pred;
		flag_terna = &terna_new;
/*		drop Riconsegnato_Consuntivo_KWH;*/
	run;

%MEND;

%MACRO calculate_ensemble_prediction_ms(ds_in=, ds_out=);

	data &ds_out.;
		length metodo_di_ensemble $ 32 hour 8 data_gas_g1 8 P_AutoReg 8 P_Full 8 P_ModB 8 P_Old422 8 P_Similarity 8 
			P_Terna 8 time_day 8 time_day_group1 8 time_day_group2 8 time_festivo 8 month 8 month_day 8 
			days_of_mape 8;
		set &ds_in. end=eof;

		if eof then
			do;
				hour = &hour_pred;
				metodo_di_ensemble = "&ensemble_method";

				if hour >= 6 and hour <= 12 then
					do;
						/* prima delle ore pre_inc e inc si attiva la media semplice con tutti i modelli */
						Ensemble_final_pred = mean(of P_:);
						flag_terna = &terna_new;
						call symputx ('check_ensemble_preds', 1);
					end;
				else if (hour >= 13 or hour < 6) and &terna_new = 0 then
					do;
						/* media semplice si Ã¨ attivato perchÃ¨ manca il nuovo dato terna ai modelli */
						Ensemble_final_pred = mean(P_AutoReg, P_Old422, P_ModB);
						flag_terna = &terna_new;
						call symputx ('check_ensemble_preds', 1);

					end;
				else if (hour >= 13 or hour < 6) and &terna_new = 1 then
					do;
						Ensemble_final_pred = mean(of P_:);
						flag_terna = &terna_new;
						call symputx ('check_ensemble_preds', 0);
					end;

				output;
			end;

		/*	drop mape:;*/
	run;

%put ENSEMBLE PREDICTION &check_ensemble_preds;

%MEND;

%MACRO DAFNE();
	%prepare_data(ds_in=&ensemble_dataset_fortrain, ds_out1=dataset_train, ds_out2=dataset_no_outlier, k=&k); 
	%get_train_back(&cut_date_storico, dataset_no_outlier, this_data=&date_pred); /* costruisco i sotto-dataset di training */
	%get_mape_types(step=&step, this_cut_date=&cut_date); /* calcolo i mape per tipologia */
	%merge_weights(); /* calcolo i mape per tipologia */
	%merge_with_predictions(dataset_in=dataset_no_outlier, this_data=&date_pred, dataset_out=merge_final); /* Unisco la riga delle prev con le nuove feature */
	%adjust_macro_weights(); /* calcolo i macro pesi dell'ensemble */
	%calculate_ensemble_prediction(); /* genero la previsione ensemble */
%MEND;

%MACRO Media_Semplice();
	%prepare_data(ds_in=preds_merge, ds_out1=preds_merge, ds_out2=preds_merge_no_outlier, k=&k); 
	%calculate_ensemble_prediction_ms(ds_in=preds_merge_no_outlier, ds_out=ensemble_final_pred);
%MEND;

%MACRO ensemble();
	%IF &flag_DAFNE = 1 %THEN
		%DO;
			%DAFNE();
		%END;
	%ELSE
		%DO;
			%Media_Semplice();
		%END;
%MEND;

%MACRO store_pred();
	%IF %sysfunc(exist(&input_ensemble_prev)) %THEN
		%DO;

			data &input_ensemble_prev;
				length metodo_di_ensemble $ 32 hour 8 data_gas_g1 8 P_AutoReg 8 P_Full 8 P_ModB 8 P_Old422 8 P_Similarity 8 
					P_Terna 8 time_day 8 time_day_group1 8 time_day_group2 8 time_festivo 8 month 8 month_day 8 
					days_of_mape count_ok lower_limit upper_limit mean_ok 8;
				set &input_ensemble_prev ensemble_final_pred;
			run;

		%END;
	%ELSE
		%DO;

			data &input_ensemble_prev;
				length metodo_di_ensemble $ 32 hour 8 data_gas_g1 8 P_AutoReg 8 P_Full 8 P_ModB 8 P_Old422 8 P_Similarity 8 
					P_Terna 8 time_day 8 time_day_group1 8 time_day_group2 8 time_festivo 8 month 8 month_day 8 
					days_of_mape count_ok lower_limit upper_limit mean_ok 8;
				set ensemble_final_pred;
			run;

		%END;
%MEND;

%MACRO clean_weights_ms();

	proc contents data=&input_ensemble_prev out=input_ens_vars;
	run;

	proc sql;
		select count(*) into: weight_vars
			from input_ens_vars 
				where NAME like "Weight%";
	run;

	%IF &weight_vars > 0 %THEN
		%DO;

			data &input_ensemble_prev;
				set &input_ensemble_prev;

				if metodo_di_ensemble = "Media Semplice" and data_gas_g1 > intnx("day", &date_pred, -3) then
					do;
						array Weight[*] weight:;
						do i = 1 to dim(Weight);
							j + 1;

							if flag_terna = 1 then
								Weight[i] = 1/6;
							else if flag_terna = 0 then
								do;
									if  j in (1, 3, 4) then
										Weight[i] = 1/3;
									else Weight[i] = 0;
								end;

							if mod(j, 6) = 0 then
								j = 0;
						end;

						array Preds_w[*] pred_weight:;
						array Preds[6] P_:;
						do k = 1 to dim(Weight);
							t + 1;
							Preds_w[k] = Preds[t]*Weight[k];

							if mod(t, 6) = 0 then
								do;
									t = 0;
								end;
						end;

						ens_pred_mvgavg = sum(of pred_weight_mvgavg:);
						ens_pred_month = sum(of pred_weight_month:);
						ens_pred_timeg1 = sum(of pred_weight_timeg1:);
						ens_pred_timeg2 = sum(of pred_weight_timeg2:);
						ens_pred_timefest = sum(of pred_weight_timefest:);

						if time_day_group1 = 1 then
							do;
								macro_weight_mvgavg = 1/3;
								macro_weight_month = 1/3;
								macro_weight_timeg1 = 1/3;
								macro_weight_timeg2 = 0;
								macro_weight_timefest = 0;
							end;
						else if time_day_group2 = 1 then
							do;
								if time_festivo = 0 then
									do;
										macro_weight_mvgavg = 1/3;
										macro_weight_month = 1/3;
										macro_weight_timeg1 = 0;
										macro_weight_timeg2 = 1/3;
										macro_weight_timefest = 0;
									end;
								else
									do;
										macro_weight_mvgavg = 1/4;
										macro_weight_month = 1/4;
										macro_weight_timeg1 = 0;
										macro_weight_timeg2 = 1/4;
										macro_weight_timefest = 1/4;
									end;
							end;

						array macro_w[*] macro:;
						array ens_pred_comp[*] ens_pred:;
						array ens_weight_preds[*] ens_weight:;
						do l = 1 to dim(macro_w);
							ens_weight_preds[l] = macro_w[l]*ens_pred_comp[l];
						end;
					end;

				check = sum(of ens_weight_preds[*]);
				drop i j k t l check;
			run;

		%END;
%MEND;

%ensemble();
%store_pred();
%clean_weights_ms();

/* Prendo ultima riga delle previsioni_riconsegnato per fissare datetime e ora */
proc sort data=&preds_in;
	by datetime_solare;
run;

data time (keep=hour datetime_solare data_gas_rif);
	set &preds_in (where=(pred_horizon="G1")) end=eof;
	if eof then output;		
	call symputx("last_hour",hour);
	call symputx("last_datetime",datetime_solare);
	call symputx("last_data_gas",data_gas_rif);
run;

/* Salvo la previsione ensemble nella tabella di previsioni_riconsegnato */
data to_add (drop= hour data_gas_g1 rename=(Ensemble_final_pred=previsione hour_new=hour));
	set &input_ensemble_prev (keep=Ensemble_final_pred data_gas_g1 hour where =(data_gas_g1=&date_pred and hour=&hour_pred));
	datetime_solare=&last_datetime;
	data_gas_rif=&last_data_gas;
	hour_new=&last_hour;
	previsione=Ensemble_final_pred;
	approccio="G1_15";
	pred_horizon="G1";
	sottomodello="";
	id_target="TOT";
	id_modello="EN";
	id=41;
	format datetime_solare datetime19. data_gas_rif date9.;
run;

proc append base=&preds_in data=to_add force;
run;
	
proc sort data=&preds_in;
	by datetime_solare id;
run;


%macro predict_industriale(last_datetime, dataset_input);

		data g1_preds_for_ind;
			set &dataset_input;
			where id in (41, 42, 43) and datetime_solare=&last_datetime;
		run;
		
		proc transpose data=g1_preds_for_ind out=g1_preds_for_ind_t prefix=p_;
			by datetime_solare data_gas_rif hour pred_horizon;
			var previsione;
			id id;
		run;
		
		data g1_industriale;
			set g1_preds_for_ind_t;
			drop _NAME_ p_:;
			previsione = p_41 - p_42 - p_43;
			approccio = "G1_18";
			sottomodello = "IND";
			id_target = "INDR";
			id = 50;
			id_modello = "DF";
		run;
		
		data &dataset_input;
			set &dataset_input g1_industriale;
		run;

%mend;

%predict_industriale(&last_datetime, &preds_in);