%put DOING GENERAZIONE DATASET DI TRAINING E MODELLI;

%global logic_path lib_path;

%let logic_path = /Public/DAFNE/;
%let lib_path = /opt/sas/viya/config/data/cas/default/dafne;

filename C1 FILESRVC folderpath="&logic_path" filename='librerie_viya.sas';
filename C2 FILESRVC folderpath="&logic_path" filename='g_master_datasettraining.sas';
%include C1;
%include C2;