%macro anomalie_scada_cons();

	PROC SQL;
		CREATE TABLE WORK.anomalie_by_dt AS 
			SELECT /* COUNT_DISTINCT_of_ID_SCADA */
		(COUNT(DISTINCT(t1.ID_SCADA))) AS count_MisuratoriCons_missing, t1.DT_RIFERIMENTO FROM WORK.CONS_TMP t1 WHERE t1.flag_ffill = 1 GROUP BY t1.DT_RIFERIMENTO;
	QUIT;

	data anomalie_now;
		set anomalie_by_dt;
		where dt_riferimento = intnx("hour", &dt_now_cons, -1);
		datetime_solare = dhms(datepart(&dt_now_cons), hour(&dt_now_cons), 0, 0);
	run;

	data base_anomalie;
		datetime_solare = dhms(datepart(&dt_now_cons), hour(&dt_now_cons), 0, 0);
		format datetime_solare datetime19.;
	run;

	data anomalie_now_complete;
		merge base_anomalie(in=L) anomalie_now(in=R);
		by datetime_solare;

		if L;

		if missing(count_MisuratoriCons_missing) then
			count_MisuratoriCons_missing = 0;
	run;

	%if %sysfunc(exist(&output_anomalie_cons)) %then
		%do;

			proc append base=&output_anomalie_cons data=anomalie_now_complete force;
			run;

		%end;
	%else
		%do;

			data &output_anomalie_cons;
				length datetime_solare count_MisuratoriCons_missing 8;
				format datetime_solare datetime19.;
				stop;
			run;

			proc append base=&output_anomalie_cons data=anomalie_now_complete force;
			run;

		%end;
%mend anomalie_scada_cons;

%macro gestione_anomalie_bilancio(dt_base=, ds_in=, ds_out=, storico_anomalie_bil=);
	/*	%let dt_base=&start;*/
	/*	%let ds_in=bil;*/
	/*	%let ds_out=bil_;*/
	/*	%let storico_anomalie_bil=&output_anomalie_bilancio;*/
	/* Taglio il ds di input per data_gas < &start e calcolo il confronto a partire da &start */
	/* Definisco data sulla quale tagliare il dataset */
	data stagioni;
		date_now=datepart(&dt_base);
		d1=nwkdom(5, 1, 3, year(date_now));
		d2=nwkdom(5, 1, 10, year(date_now));
		format d1 d2 date9.;
		length stagione $7;

		if (date_now>= d1  and date_now< d2) then
			stagione="estate";
		else if (date_now < d1 or date_now >= d2) then
			stagione="inverno";
		call symput("stagione",strip(stagione));
		format date_now date9.;
	run;

	%put Sono in &stagione;

	data _null_;
		dt_base=&dt_base;
		date_base=datepart(&dt_base);
		stagione="&stagione";

		if stagione="inverno" then
			dt_baseline=dhms(date_base,13,0,0);
		else dt_baseline=dhms(date_base,12,0,0);

		if dt_base<dt_baseline then
			do;
				cut_date=intnx("day",date_base,-2);
			end;
		else
			do;
				cut_date=intnx("day",date_base,-1);
			end;

		call symput("cut_date",cut_date);

		/*		format cut_date date9. date: date9. dt: datetime19.;*/
	run;

	proc sql noprint;
		select max(data_gas) into: max_date_bil
			from &ds_in (where = (data_gas le &cut_date) );
	run;

	data check_bilancio;
		/* Creo date BASELINE su cui effettuare il confronto */
		dt_now=&dt_base;
		date_now=datepart(&dt_base);
		stagione="&stagione";

		if stagione="inverno" then
			dt_baseline=dhms(datepart(dt_now),13,0,0);
		else dt_baseline=dhms(datepart(dt_now),12,0,0);

		/* Se sono prima delle 12 (13 in inverno) di oggi --> 
			ultima data_rif per il bilancio = altro ieri 
		*/
		if dt_now lt dt_baseline then
			do;
				date_baseline_bil=intnx("day",date_now,-2);
			end;

		/* Se sono dopo le 12 (13 in inverno) di oggi --> 
				ultima data_rif per il bilancio = ieri  */
		else
			do;
				date_baseline_bil=intnx("day",date_now,-1);
			end;

		/* Effettuo il confronto */
		max_date_bil=&max_date_bil;
		check_ritardo_bilancio=ifn(max_date_bil=date_baseline_bil,0,1);
		call symput("check_ritardo_bilancio",check_ritardo_bilancio);
		format max_date_bil date: date9. 
			dt: datetime19.;
	run;

	/* Se check_ritardo_bilancio=1 --> Devo fare ffil */
	data anomalie_bilancio;
		datetime_solare=dhms(datepart(&dt_base), hour(&dt_base),0,0);
		check_ritardo_bilancio=&check_ritardo_bilancio;
		format datetime_solare datetime19.;
	run;

	%if &check_ritardo_bilancio=1 %then
		%do;
			%put ===== Bilancio non disponibile =====;

			/* Taglio il ds di input per &cut_date */
			data &ds_out;
				set &ds_in
					(where = (data_gas le &cut_date));
			run;

			/* FFILL del giorno/i mancante/i con l'ultimo valore disponibile */
			data _null_;
				cycle = &cut_date - &max_date_bil;
				call symput("cycle",cycle);
			run;

			data to_add_bil (drop=i);
				set &ds_out end=eof;

				if eof;
				i=1;

				do until (i>&cycle);
					data_gas=intnx("day",&max_date_bil,i);
					output;
					i=i+1;
				end;

				format data_gas date9.;
			run;

			data &ds_out;
				set &ds_out to_add_bil;
			run;

		%end;
	%else
		%do;
			%put ===== Bilancio disponibile =====;
		%end;

	/* STORICIZZO EVENTUALI ANOMALIE */
	%put ===== Storicizzo Eventuali Anomalie Bilancio =====;

	%if %sysfunc(exist(&storico_anomalie_bil)) %then
		%do;

			proc append base=&storico_anomalie_bil data=anomalie_bilancio force;
			run;

		%end;
	%else
		%do;

			data &storico_anomalie_bil;
				set anomalie_bilancio;
			run;

		%end;

%mend gestione_anomalie_bilancio;

%macro gestione_anomalie_scada(dt_base=, ds_in=, ds_out=, storico_anomalie=);

/*%let dt_base=&dt_solare;*/
/*%let ds_in=flussifisici1_nodup;*/
/*%let ds_out=flussifisici1_bonifica_storico;*/
/*%let storico_anomalie=&output_anomalie_scada;*/

	/* Taglio i ds di input per dt_calcolo < &start e calcolo i confronti a partire da &start */
	data _null_;
		cut_date=dhms(datepart(&dt_base),hour(&dt_base),0,0);
		call symput("cut_date",cut_date);
	run;

	proc sql noprint;
		select max(dt_calcolo) format datetime19. into: max_datetime_scada
			from &ds_in (where = (dt_calcolo<&cut_date) );
	run;


	proc sql noprint;
		select datepart(max(dt_calcolo))  into: max_date_scada
			from &ds_in (where = (dt_calcolo<&cut_date) );
	run;

	/* 2 Tipi di controllo:
	- Se ? arrivato il dato scada (circa min 4)
	- Se i valori sono attendibili nell'ultima data disponibile (ric o imp <=0) */

	/* Controllo che l'ultima riga abbia la data e ora desiderata */
	data anomalia_dates;
		date_now=datepart(&cut_date);
		hour_now=hour(&cut_date);
		datetime_now=dhms(date_now,hour_now,0,0);
		datetime_scada_use=intnx("dthour", "&max_datetime_scada"dt,1);
		scada_check_missing=ifn(datetime_scada_use=datetime_now,0,1);
		call symputx('scada_check_missing',scada_check_missing);
		call symput("last_dt_correct",datepart(datetime_scada_use));
		format date_: date9. datetime: datetime19.;
	run;

	/* Check di consistenza su ultima riga */

	/* Se non ho ritardo nei dati --> posso fare un check sui valori contenuti nell'ultima riga */
	%if &scada_check_missing=0 %then %do;

		proc sql noprint;
			select max(dt_calcolo) format datetime19. into: max_dt 
			from &ds_out;
		quit;

		proc sql;
			create table anomalia_valori as 
			select "&max_dt"dt as dt_calcolo format datetime19.
				, sum(flg_imp_ric) as sum_flg_imp_ric
				 , sum(flg_pcs) as sum_flg_pcs
			from &ds_out (where=(dt_calcolo = "&max_dt"dt))
			;
		quit;

		data anomalia_valori;
			set anomalia_valori;
			if sum_flg_imp_ric gt 0 or sum_flg_pcs gt 0 then scada_check_valori_anomali=1;
			else scada_check_valori_anomali=0;
			call symput("scada_check_valori_anomali",scada_check_valori_anomali);
		run;

	%end;

	/* Se ho ritardo nei dati --> imposto automaticamente il check_valori anomali a 0 */
	%else %do;

		data anomalia_valori;
			scada_check_valori_anomali=0;
			call symput("scada_check_valori_anomali",scada_check_valori_anomali);
		run;
		
	%end;

	%if &scada_check_valori_anomali=1 or &scada_check_missing=1 %then
		%do;
			%put ===== Anomalia nei dati scada di bilancio =====;

		%end;
	%else
		%do;
			%put ===== Non sono presenti anomalie nei dati scada di bilancio =====;
		%end;

	/* Eseguo FFILL */
	data _null_;
		dt_solare_scada=dhms(datepart(&cut_date),hour(&cut_date),0,0);
		call symput("dt_solare_scada",dt_solare_scada);
		format dt_solare_scada datetime19.;
	run;

	/* Data di inizio del calendario (min di &ds_out) */
	proc sql;
		select min(dt_calcolo) into: min_cal_temp
		from &ds_out
		;
	quit;

	data temp_calendar(where=(dt_calcolo<&dt_solare_scada and 
						dt_calcolo ge &min_cal_temp));
		date=intnx("day",datepart(&min_cal_temp),0);

		do while (date<=&last_dt_correct);
			do hour=0 to 23;
				dt_calcolo=dhms(date,hour,0,0);
				hour_scada=timepart(dt_calcolo);
				output;
			end;
			date=intnx("day", date, 1, 's');
		end;

		drop hour;
		format dt_calcolo datetime27.6 date date9. hour_scada hhmm.;
	run;

	/* tx_tipologia_punto e type distinti */
	proc sql;
		create table type_distinct as 
		select distinct tx_tipologia_punto
			  , type
		from &ds_out
		;
	quit;

	/* Cross join tra type_distinct e temp_calendar per creare la maschera_scada da cui parto per il ffill */
	proc sql;
		create table maschera_scada as 
		select * from  temp_calendar
		cross join type_distinct;
	quit;

	/* Ordino e merge con il ds bonificato */
	proc sort data=maschera_scada;
		by type dt_calcolo ;
	run;

	proc sort data=&ds_out;
		by type dt_calcolo ;
	run;

	data &ds_out._1;
		merge &ds_out maschera_scada;
		by type dt_calcolo ;
	run;

	/* Ffill */
	data &ds_out. (drop=totale_smc_tmp pcs_tmp);
		set &ds_out._1;
		by type dt_calcolo;
		
		/* RETAIN the new variable */
		retain totale_smc_tmp pcs_tmp;

		/* Reset TEMP when the BY-Group changes */
		if first.type then totale_smc_tmp=.;

		/* Assign TEMP when valore_clean is non-missing */
		if totale_smc ne . then 
			totale_smc_tmp=totale_smc;
		else if totale_smc = . then 
			totale_smc=totale_smc_tmp;

		if pcs gt 0 then 
			pcs_tmp=pcs;
		else if pcs le 0 then
			pcs=pcs_tmp;

		/* calcolo totale_kwh */
		totale_kwh=totale_smc*PCS;
	run;

	/* ===== Storicizzo anomalie ====== */
	%put ===== Storicizzo Eventuali Anomalie scada =====;

	data anomalie_scada;
		datetime_solare=dhms(datepart(&cut_date),hour(&cut_date),0,0);
		check_valori_anomali_scada=&scada_check_valori_anomali;
		check_ritardo_scada=&scada_check_missing;
		format datetime_solare datetime19.;
	run;

	%if %sysfunc(exist(&storico_anomalie)) %then
		%do;

			proc append base=&storico_anomalie data=anomalie_scada force;
			run;

		%end;
	%else
		%do;

			data &storico_anomalie;
				set anomalie_scada;
			run;

		%end;

%mend gestione_anomalie_scada;

%macro gestione_anomalie_terna(dt_base=, ds_in1=, ds_in2=, ds_out1=, ds_out2=, storico_anomalie_terna=);
	/*	%let dt_base=&start;*/
	/*	%let ds_in1=terna_prev;*/
	/*	%let ds_in2=terna_cons;*/
	/*	%let ds_out1=terna_prev;*/
	/*	%let ds_out2=terna_cons;*/
	/*	%let storico_anomalie_terna=&output_anomalie_terna;*/
	/* Fisso a 0 min e 0 secondi il datetime &start*/
	data _null_;
		cut_date=dhms(datepart(&dt_base),hour(&dt_base),0,0);
		call symput("cut_date",cut_date);
	run;

	/* Taglio i ds di input */
	proc sql;
		select max(data_rif) into :date_terna_prev 
			from &ds_in1 
				(where = (data_rif le datepart(&cut_date) ));
	quit;

	proc sql;
		select max(data_rif) into :date_terna_cons 
			from &ds_in2 
				(where = (dt_inserimento_val le &cut_date));
	quit;

	/* Calcolo la stagione in cui sono: a seconda che sia inverno o estate ho una diversa dt_baseline */
/*	data stagioni;*/
/*		date_now=datepart(&cut_date);*/
/*		d1=nwkdom(5, 1, 3, year(date_now));*/
/*		d2=nwkdom(5, 1, 10, year(date_now));*/
/*		format d1 d2 date9.;*/
/*		length stagione $7;*/
/**/
/*		if (date_now>= d1  and date_now< d2) then*/
/*			stagione="estate";*/
/*		else if (date_now < d1 or date_now >= d2) then*/
/*			stagione="inverno";*/
/*		call symput("stagione",strip(stagione));*/
/*		format date_now date9.;*/
/*	run;*/
/**/
/*	%put Sono in &stagione;*/

	data check_terna (keep=date_terna_prev date_terna_cons date_baseline_prev date_baseline_cons
		check_ritardo_terna_prev check_ritardo_terna_cons stagione);
		retain date_baseline_cons date_terna_cons check_ritardo_terna_cons 
			date_baseline_prev date_terna_prev check_ritardo_terna_prev;

		/* Creo date BASELINE su cui effettuare il confronto */
		dt_now=&cut_date;
		date_now=datepart(dt_now);
/*		stagione="&stagione";*/
/*		if stagione="inverno" then*/
/*			dt_baseline=dhms(datepart(dt_now),14,0,0);*/
/*		else dt_baseline=dhms(datepart(dt_now),13,0,0);*/
		dt_baseline=dhms(datepart(dt_now),13,0,0);

		/* La massima data di riferimento delle previsioni deve essere uguale ad oggi */
		date_baseline_prev=date_now;

		/* Se sono prima delle 13 di oggi --> 
			ultima data_rif per i consuntivi = altro ieri 
			(ultima data_rif per le previsioni = oggi)
		*/
		if dt_now lt dt_baseline then
			do;
				date_baseline_cons=intnx("day",date_now,-2);
			end;
		/* Se sono dopo le 13 di oggi --> 
				ultima data_rif per i consuntivi = ieri 
				(ultima data_rif per le previsioni = domani)
		*/
		else
			do;
				date_baseline_cons=intnx("day",date_now,-1);
			end;

		/* Effettuo il confronto */
		date_terna_prev=&date_terna_prev;
		date_terna_cons=&date_terna_cons;
		check_ritardo_terna_prev=ifn(date_terna_prev=date_baseline_prev,0,1);
		check_ritardo_terna_cons=ifn(date_terna_cons=date_baseline_cons,0,1);
		call symput("check_ritardo_terna_prev",check_ritardo_terna_prev);
		call symput("check_ritardo_terna_cons",check_ritardo_terna_cons);
		format date: date9. 
			dt: datetime19.;
	run;

	/* Creo la tabella totale di anomalie */
	data anomalie_terna_all;
		datetime_solare=&cut_date;
		check_ritardo_terna_prev=&check_ritardo_terna_prev;
		check_ritardo_terna_cons=&check_ritardo_terna_cons;
		format datetime_solare datetime19.;
	run;

	/* ====== Gestione eventuali anomalie nelle previsioni terna ====== */
	%if &check_ritardo_terna_prev=1 %then
		%do;
			%put ===== Previsioni terna non disponibili =====;

			/* Taglio il ds di input per &cut_date */
			data &ds_out1;
				set &ds_in1
					(where = (dt_inserimento_val le &cut_date));
			run;

			/* Inserisco dati vecchi di una settimana o di un giorno */
			proc sql noprint;
				select max(data_rif)
					into :date_terna_prev
						from  &ds_out1;
			quit;

			/* Controllo se il lag7 del giorno mancante che devo inserire (date_terna_prev+1gg) ? festivo.
				   Se ? festivo --> inserisco come valori del giorno mancante quelli di ieri
				   Altrimenti --> inserisco come valori del giorno mancante quelli di 1 settimana fa 
			*/
			data check_lag7_festivo_prev;
				date_terna_prev=&date_terna_prev;
				date_to_insert=intnx("day",&date_terna_prev,1);
				lag7=intnx("day",date_to_insert,-7);
				time_festivo = 0;

				if day(lag7) = 1 and month(lag7) = 1 then
					time_festivo = 1;

				if day(lag7) = 6 and month(lag7) = 1 then
					time_festivo = 1;

				if day(lag7) = 25 and month(lag7) = 4 then
					time_festivo = 1;

				if day(lag7) = 1 and month(lag7) = 5 then
					time_festivo = 1;

				if day(lag7) = 2 and month(lag7) = 6 then
					time_festivo = 1;

				if day(lag7) = 15 and month(lag7) = 8 then
					time_festivo = 1;

				if day(lag7) = 1 and month(lag7) = 11 then
					time_festivo = 1;

				if day(lag7) = 8 and month(lag7) = 12 then
					time_festivo = 1;

				if lag7 = holiday('EASTER',year(lag7)) then
					time_festivo = 1;

				if lag7 = intnx('day',holiday('EASTER',year(lag7)),1) then
					time_festivo = 1;

				if lag7 = intnx('day',holiday('EASTER',year(lag7)),2) then
					time_festivo = 1;

				if day(lag7) = 25 and month(lag7) = 12 then
					time_festivo = 1;

				if day(lag7) = 26 and month(lag7) = 12 then
					time_festivo = 1;

				if day(lag7) = 31 and month(lag7) = 12 then
					time_festivo = 1;
				call symput("time_festivo_prev",time_festivo);
				call symput("lag7prev",lag7);
				format date_to_insert date_terna_prev lag7 date9.;
			run;

			data terna_prev_add (drop=dt_inserimento_val data_rif dt_rif increment
				rename=(data_rif_add=data_rif dt_inserimento_val_add=dt_inserimento_val
				dt_rif_add=	dt_rif) );
				set &ds_out1;

				/* Se il lag7 non ? festivo, allora sostituisco il giorno mancante con il lag7 */
				if &time_festivo_prev=0 then
					do;
						if data_rif=&lag7prev;
						increment=7;
					end;

				/* Se il lag7 ? festivo, sostituisco il giorno mancante con ultimo valore disponibile */
				else
					do;
						if data_rif=&date_terna_prev;
						increment=1;
					end;

				data_rif_add=intnx("day",data_rif,increment);
				dt_inserimento_val_add=intnx("dtday",dt_inserimento_val,increment,"s");
				dt_rif_add=intnx("dtday",dt_rif,increment,"s");
				format data_rif_add date9. dt_inserimento_val_add dt_rif_add datetime19.;
			run;

			data &ds_out1;
				set &ds_out1 terna_prev_add;
			run;

		%end;
	%else
		%do;
			%put ===== Previsioni terna disponibili =====;
		%end;

	/*  ====== Gestione eventuali anomalie nei consuntivi terna ====== */
	%if &check_ritardo_terna_cons=1 %then
		%do;
			%put ===== Consuntivi terna non disponibili =====;

			/* Taglio il ds per &cut_date */
			data &ds_out2;
				set &ds_in2
					(where = (dt_inserimento_val le &cut_date));
			run;

			/* Inserisco dati vecchi di una settimana o di un giorno */
			proc sql noprint;
				select max(data_rif)
					into :date_terna_cons
						from &ds_out2;
			quit;

			/* Controllo se il lag7 del giorno mancante che devo inserire (date_terna_cons+1gg) ? festivo.
				   Se ? festivo --> inserisco come valori del giorno mancante quelli di ieri
				   Altrimenti --> inserisco come valori del giorno mancante quelli di 1 settimana fa 
			*/
			data check_lag7_festivo_cons;
				date_terna_cons=&date_terna_cons;
				date_to_insert=intnx("day",&date_terna_cons,1);
				lag7=intnx("day",date_to_insert,-7);
				time_festivo = 0;

				if day(lag7) = 1 and month(lag7) = 1 then
					time_festivo = 1;

				if day(lag7) = 6 and month(lag7) = 1 then
					time_festivo = 1;

				if day(lag7) = 25 and month(lag7) = 4 then
					time_festivo = 1;

				if day(lag7) = 1 and month(lag7) = 5 then
					time_festivo = 1;

				if day(lag7) = 2 and month(lag7) = 6 then
					time_festivo = 1;

				if day(lag7) = 15 and month(lag7) = 8 then
					time_festivo = 1;

				if day(lag7) = 1 and month(lag7) = 11 then
					time_festivo = 1;

				if day(lag7) = 8 and month(lag7) = 12 then
					time_festivo = 1;

				if lag7 = holiday('EASTER',year(lag7)) then
					time_festivo = 1;

				if lag7 = intnx('day',holiday('EASTER',year(lag7)),1) then
					time_festivo = 1;

				if lag7 = intnx('day',holiday('EASTER',year(lag7)),2) then
					time_festivo = 1;

				if day(lag7) = 25 and month(lag7) = 12 then
					time_festivo = 1;

				if day(lag7) = 26 and month(lag7) = 12 then
					time_festivo = 1;

				if day(lag7) = 31 and month(lag7) = 12 then
					time_festivo = 1;
				call symput("time_festivo_cons",time_festivo);
				call symput("lag7cons",lag7);
				format date_to_insert date_terna_cons lag7 date9.;
			run;

			data terna_cons_add (drop=dt_inserimento_val data_rif dt_rif increment
				rename=(data_rif_add=data_rif dt_inserimento_val_add=dt_inserimento_val
				dt_rif_add=	dt_rif) );
				set &ds_out2;

				/* Se il lag7 non ? festivo, allora sostituisco il giorno mancante con il lag7 */
				if &time_festivo_cons=0 then
					do;
						if data_rif=&lag7cons;
						increment=7;
					end;

				/* Se il lag7 ? festivo, sostituisco il giorno mancante con ultimo valore disponibile */
				else
					do;
						if data_rif=&date_terna_cons;
						increment=1;
					end;

				data_rif_add=intnx("day",data_rif,increment);
				dt_inserimento_val_add=intnx("dtday",dt_inserimento_val,increment,"s");
				dt_rif_add=intnx("dtday",dt_rif,increment,"s");
				format data_rif_add date9. dt_inserimento_val_add dt_rif_add datetime19.;
			run;

			data &ds_out2;
				set &ds_out2 terna_cons_add;
			run;

		%end;
	%else
		%do;
			%put ===== Consuntivi terna disponibili =====;
		%end;

	/* Storicizzo eventuali anomalie */
	%put ==== Storicizzo Anomalie Terna ====;

	%if %sysfunc(exist(&storico_anomalie_terna)) %then
		%do;

			proc append base=&storico_anomalie_terna data=anomalie_terna_all force;
			run;

		%end;
	%else
		%do;

			data &storico_anomalie_terna;
				set anomalie_terna_all;
			run;

		%end;

%mend gestione_anomalie_terna;

%macro merge_anomalie(lib_in=, ds_out=);

	data _null_;
		dt_focus=dhms(datepart(&dt_solare),hour(&dt_solare),0,0);
		call symput("dt_focus",dt_focus);
	run;

	proc sql;
		create table lista_anomalie as 
			select distinct catx(".","&lib_in",compress(lowcase(memname))) as tables_anomalie
				from dictionary.columns
					where lowcase(libname)="&lib_in" and 
						lowcase(memname) in ("anomalie_bilancio" "anomalie_scada_bil" "anomalie_scada_cons"
						"anomalie_terna");
	quit;

	proc sql noprint;
		select distinct tables_anomalie into: tables_anomalie
			separated by " " 
		from lista_anomalie
		;
	quit;

	proc sql noprint;
		select count(*) into: n_tabelle_anomalie
			from lista_anomalie
		;
	quit;

	%if &n_tabelle_anomalie >0 %then
		%do;
			%put ===== Merge Anomalie =====;

			data add_bil;
				set &lib_in..anomalie_bilancio end=eof;
				if eof;
				run;
			data add_scada_bil;
				set &lib_in..anomalie_scada_bil end=eof;
				if eof;
				run;
			data add_scada_cons;
				set &lib_in..anomalie_scada_cons end=eof;
				if eof;
				run;
			data add_terna;
				set &lib_in..anomalie_terna end=eof;
				if eof;
				run;
			data add;
				merge add_bil add_scada_bil add_scada_cons add_terna;
				by datetime_solare;
			run;

			proc stdize data=add reponly missing=0 out=add;
			run;

			proc append base=&ds_out data=add force;
			run;



		%end;
%mend merge_anomalie;

%macro gxg_create_scadacons(anagrafica, valori, start_date_cons, cons_output,dt_base);
	%checkds(&GxG_RicTot_base);

	data dt_read;
		start_date_read=intnx("month",&g_gas,-1, 'B');
		start_dt_read=put(dhms(start_date_read,6,0,0), datetime19.);
		start_dt_all=put(dhms(&start_date_cons-1,6,0,0), datetime19.);
		dt_now_cons = dhms(datepart(&dt_base), hour(&dt_base), 0, 0);
		call symput('start_dt_all', start_dt_all);
		call symput('start_dt_read', start_dt_read);
		call symputx('dt_now_cons', dt_now_cons);
	run;

	data ANAGRAFICA_PORTATE_SCADA(drop=FL_DELETED STATO id_remi);
		set &anagrafica(where=(FL_DELETED eq "0" and STATO eq 1 and classe ne ""));
	run;

	/*	data for_svil.ANAGRAFICA_PORTATE_SCADA;*/
	/*		set ANAGRAFICA_PORTATE_SCADA;*/
	/*	run;*/
	proc sort data=ANAGRAFICA_PORTATE_SCADA out=ANAGRAFICA_PORTATE_SCADA_NODUP
		nodupkey;
		by id_scada;
	run;

	data ANAGRAFICA_PORTATE_SCADA_ADJ;
		set ANAGRAFICA_PORTATE_SCADA_NODUP;

		/* RIMOSSA FIX: MODIFICATA ANAGRAFICA 12/02/2020 ore 14.21 */
/* 		if id_scada = 5800 then classe = "C"; */
	run;

	/* leggo valori dei misuratori in base alla presenza dello storico o meno */
	%if &dataset_presente=1 %then
		%do;

			data MISURATORI_SCADA(drop=id_unita_misura);
				set &valori(where=(data_ora_rif>="&start_dt_read"dt));

/* 				if _N_ = 1 then */
/* 					call symputx("min_dt_cons", data_ora_rif); */
			run;
			
			%let min_dt_cons="&start_dt_read"dt;

			data local_read(drop=datetime_solare 
				where=(DT_RIFERIMENTO<"&start_dt_read"dt and dt_riferimento>="&start_dt_all"dt));
				set &GxG_RicTot_base(where=(datetime_solare>="&start_dt_all"dt) 
					keep=datetime_solare scada_cons_a scada_cons_t scada_cons_sum scada_cons_f scada_cons_c scada_cons_i);
				DT_RIFERIMENTO=intnx("dthour",datetime_solare,-1);
				format DT_RIFERIMENTO datetime19.;
			run;

			proc sort data=local_read out=local_read nodupkey;
				by DT_RIFERIMENTO;
			run;

		%end;
	%else
		%do;
	
			data MISURATORI_SCADA(drop=id_unita_misura);
				set &valori(where=(data_ora_rif>="&start_dt_all"dt));

/* 				if _N_ = 1 then */
/* 					call symputx("min_dt_cons", data_ora_rif); */
			run;
			
			%let min_dt_cons="&start_dt_all"dt;

		%end;

	/*	data for_svil.misuratori_scada(compress=yes);*/
	/*		set MISURATORI_SCADA;*/
	/*	run;*/
	/**/
	/*	data misuratori_scada;*/
	/*		set for_svil.misuratori_scada end=eof;*/
	/*		if _N_=1 then call symputx("min_dt_cons", data_ora_rif);*/
	/*	run;*/
	/* Creo il calendario per costruire il prodotto cartesiano */
	data calendario_cons(where=(data_gas_rif<=&g_gas and datetime_solare>=&min_dt_cons));
		date=datepart(&min_dt_cons);

		do while (date<=&g1_solare);
			do hour=0 to 23;
				datetime_solare=dhms(date,hour,0,0);

				if hour<6 then
					data_gas_rif=intnx("day", date, -1);
				else data_gas_rif=date;
				output;
			end;

			date=intnx("day", date, 1, 's');
		end;

		format data_gas_rif date9.;
		format datetime_solare datetime19.;
		drop hour;
	run;

	proc sql noprint;
		create table cross_join as select * from (
			ANAGRAFICA_PORTATE_SCADA_ADJ(keep=id_scada classe segno) cross join 
			calendario_cons(keep=datetime_solare));
	quit;

	proc sort data=cross_join;
		by id_scada datetime_solare;
	run;

	data MISURATORI_SCADA;
		set MISURATORI_SCADA;
		rename data_ora_rif=DT_RIFERIMENTO;
	run;

	proc sort data=MISURATORI_SCADA;
		by id_scada DT_RIFERIMENTO;
	run;

	data MISURATORI_SCADA_JOIN;
		merge cross_join(in=L rename=(datetime_solare=DT_RIFERIMENTO)) 
			MISURATORI_SCADA(in=R);
		by id_scada DT_RIFERIMENTO;

		if L;
	run;

	/*	data for_svil.MISURATORI_SCADA_JOIN(compress=yes);*/
	/*		set MISURATORI_SCADA_JOIN;*/
	/*	run;*/
	/*	proc sort data=MISURATORI_SCADA_JOIN;*/
	/*		by id_scada DT_RIFERIMENTO;*/
	/*	run;*/
	data cons_tmp(keep= id_scada DT_RIFERIMENTO valore_clean classe segno flag_ffill);
		set MISURATORI_SCADA_JOIN;
		valore_clean_tmp=abs(input(scan(valore,1,'**'), 12.));
		valore_string = scan(valore,2,'**');
		by id_scada;
		valore_clean = abs(valore_clean_tmp*1);
		flag_ffill = 0;

		/* RETAIN the new variable */
		retain temp_val;

		/* Reset TEMP when the BY-Group changes */
		if first.id_scada then
			temp_val=.;

		/* Assign TEMP when valore_clean is non-missing */
		if valore_clean ne . then
			temp_val=valore_clean;

		/* When valore_clean is missing, assign the retained value of TEMP into X */
		else if valore_clean=. then
			do;
				valore_clean=temp_val;

				if not missing(temp_val) then
					flag_ffill=1;
			end;

		if missing(valore_clean) then
			valore_clean = 0;
	run;

	/*	data for_svil.cons_tmp_new;*/
	/*		set cons_tmp;*/
	/*	run;*/
	/**/
	/*	data cons_tmp;*/
	/*		set for_svil.cons_tmp_new;*/
	/*	run;*/
	/* Clean preceeding missing by imputing 0, since some pressure reduction stations were not yet constructed */
	/*	proc sort data=ANAGRAFICA_PORTATE_SCADA;*/
	/*		by id_scada;*/
	/*	run;*/
	/*	Registro le anomalie orarie, contanto quanti punti non hanno trasmesso il dato */
	%if &flg_anomalie=1 %then
		%do;
			%put ===== Controllo Anomalie Dati Scada Cons =====;

			%anomalie_scada_cons();
		%end;

	/* Continuo con la Data Preparation */
	data cons_clean_class;
		set cons_tmp(drop=flag_ffill);
		valore_clean=abs(valore_clean);

		if segno eq 0 then
			valore_clean = - valore_clean;

		/*	if missing(cluster) then cluster = 'A';*/
	run;

	/*	data for_svil.cons_clean_class_prod;*/
	/*		set cons_clean_class;*/
	/*	run;*/
	/* Transformation and basic aggregations */
	proc means data=cons_clean_class sum noprint;
		class DT_RIFERIMENTO classe;
		var valore_clean;
		output out=cons_agg(where=(_TYPE_ in (3)) drop= _FREQ_) SUM=cons;
	run;

	/*	data for_svil.cons_agg_prod;*/
	/*		set cons_agg;*/
	/*	run;*/
	PROC SQL;
		CREATE TABLE WORK.cons_agg_sum AS 
			SELECT t1.DT_RIFERIMENTO, 
				(SUM(t1.cons)) AS cons,
				"SUM" as classe
			FROM WORK.CONS_AGG t1
				WHERE t1.CLASSE NOT = 'F'
					GROUP BY t1.DT_RIFERIMENTO;
	QUIT;

	/*	data for_svil.cons_agg_sum_prod;*/
	/*		set cons_agg_sum;*/
	/*	run;*/
	%if &dataset_presente=1 %then
		%do;

			proc transpose data=local_read out=local_read_t(rename=(col1=cons)) name=classe;
				by dt_riferimento;
			run;

			data cons_agg_add(where=(classe="SUM")) cons_agg_sum_add(where=(classe ne "SUM"));
				set local_read_t;
				classe=compress(tranwrd(classe,"scada_cons_",""));
			run;

			data cons_agg;
				set cons_agg cons_agg_add;
			run;

			data cons_agg_sum;
				set cons_agg_sum cons_agg_sum_add;
			run;

		%end;

	/* Features Engineering */
	data cons_agg_ok;
		length classe $ 3;
		set cons_agg(drop=_type_) cons_agg_sum;
		hour=hour(DT_RIFERIMENTO);
		data_gas_rif=ifn(hour>=6, datepart(DT_RIFERIMENTO), datepart(DT_RIFERIMENTO)-1 );
		format data_gas_rif date9.;
	run;

	proc sort data=cons_agg_ok nodupkey dupout=bad;
		by classe DT_RIFERIMENTO;
	run;

	proc expand data=cons_agg_ok out=cons_lag(drop=TIME);
		by classe;
		id DT_RIFERIMENTO;
		convert cons=cons_lag / transformout=(lag 1);
	run;

	/* uso cons_lag perch? in DT_RIFERIMENTO ho il valore precedente */
	/* creo statistiche per giorno gas */
	proc expand data=cons_lag out=cons_eng(drop=TIME);
		by classe data_gas_rif;
		convert cons_lag=cons_mean / transformout=(cuave);
		convert cons_lag=cons_std / transformout=(custd);
		convert cons_lag=cons_max / transformout=(cumax);
		convert cons_lag=cons_min / transformout=(cumin);
	run;

	proc expand data=cons_eng out=cons_eng(drop=TIME);
		by classe;
		id DT_RIFERIMENTO;
		convert cons_lag=cons_lag24 / transformout=(lag 23);
		convert cons_lag=cons_smooth / transformout=(movave 2);
	run;

	proc contents data=cons_eng out=cons_vars(keep=NAME) noprint;
	run;

	proc sort data=cons_vars out=cons_vars SORTSEQ=linguistic;
		by descending name;
	run;

	data vars_ordered;
		length var_list $ 200;
		set cons_vars end=eof;
		retain var_list;

		if _N_ = 1 then
			do;
				var_list = NAME;
			end;
		else
			do;
				new_var = NAME;
				var_list = catx(' ', var_list, new_var);
			end;

		if eof then
			call symputx('reorder_vars', var_list);
	run;

	data cons_eng;
		retain &reorder_vars cons_current_hour;
		set cons_eng;
		by classe data_gas_rif;
		retain cons_cumday;

		if first.data_gas_rif then
			cons_cumday = 0;
		else cons_cumday + cons_lag;
		rename cons_lag=cons cons=cons_current_hour;
	run;

	proc sort data=cons_eng out=cons_eng_sort;
		by dt_riferimento classe;
	run;

	proc transpose data=cons_eng_sort out=cons_eng_t_temp;
		by dt_riferimento classe;
		var cons:;
	run;

	proc transpose data=cons_eng_t_temp out=cons_eng_t(drop=_name_ cons_current_hour:) delimiter=_;
		by dt_riferimento;
		id _NAME_ classe;
		var col1;;
	run;

	/* Save final datasets */
	proc contents data=cons_eng_t out=vars(keep=name) noprint;
	run;

	data vars;
		set vars(where=(name like "cons%"));
		new_name=compress(cat("scada_", name));

		if index(new_name, "_cumday") then
			do;
				new_name=compress(catx("", tranwrd(new_name,"_cumday",""),"_cumday"));
			end;
		else 	if index(new_name, "_max") then
			do;
				new_name=compress(catx("", tranwrd(new_name,"_max",""),"_max"));
			end;
		else 	if index(new_name, "_min") then
			do;
				new_name=compress(catx("", tranwrd(new_name,"_min",""),"_min"));
			end;
		else 	if index(new_name, "_mean") then
			do;
				new_name=compress(catx("", tranwrd(new_name,"_mean",""),"_mean"));
			end;
		else 	if index(new_name, "_lag24") then
			do;
				new_name=compress(catx("", tranwrd(new_name,"_lag24",""),"_lag24"));
			end;
		else 	if index(new_name, "_smooth") then
			do;
				new_name=compress(catx("", tranwrd(new_name,"_smooth",""),"_smooth"));
			end;
		else 	if index(new_name, "_std") then
			do;
				new_name=compress(catx("", tranwrd(new_name,"_std",""),"_std"));
			end;

		rename_String=catx("=", name, new_name);
	run;

	proc sql noprint;
		select rename_String into :rename_list separated by " "
			from vars;
	run;

	%put &rename_list;

	data &cons_output(rename=(dt_riferimento=datetime_calcolo));
		set cons_eng_t(where=(dt_riferimento>=(intnx("dtday", "&start_dt_all"dt,1, 'S'))));
		rename &rename_list;
	run;

%mend gxg_create_scadacons;

%macro GXG_create_terna(input_terna,input_up,
			input_remi_termo, start_date_terna, output_terna_cons, output_terna_prev);
	/*	input_up_mgp_ora_termo*/
	/*input_up_mgp_termo*/
	/*input_up_consuntivo_ora_termo*/
	/*input_up_consuntivo_termo */
	/*output_terna_cons*/
	/*input_cons_lag*/
	/*input_cons_orario_lag*/
	/*input_mgp_lag*/
	/*input_mgp_orario_lag*/
	/*--- tutte hanno dt_calcolo, giorno_gas, giorno_solare*/
	data _null_;
		start_lag=intnx("month",&start_date_terna,-2,'S');
		start_date_terna_p=max(start_lag,"18MAR2016"d);
		dt_char=put(dhms(start_lag-2,0,0,0), datetime19.);
		call symput('start_dt_terna',dt_char);
		call symputx('start_date_terna_p',start_date_terna_p);
		call symputx('start_date_terna_c',start_lag);
	run;

	data calendario_orario(where=(giorno_gas<=&g_gas and giorno_gas>=&start_date_terna_c));
		/* calendario con dt_calcolo della previsione, date e giorno_gas associato*/
		date=&start_date_terna_c;

		do while (date<=&g1_solare);
			do hour=0 to 23;
				dt_calcolo=dhms(date,hour,0,0);

				if hour lt 6 then
					giorno_gas=date-1;
				else giorno_gas=date;
				output;
			end;

			date=intnx("day", date, 1, 's');
		end;

		format giorno_gas date date9.;
		format dt_calcolo datetime16.;
	run;

	data terna_prev(keep= hour_rif data_rif dt_rif dt_inserimento_val 
		energia_programmata k_d_e_up
		where=(data_rif>=&start_date_terna_p)) 
		terna_cons(keep= hour_rif data_rif dt_rif dt_inserimento_val energia_programmata 
		k_d_e_up);
		set &input_terna(where=(DT_DATA_RIFERIMENTO>="&start_dt_terna"dt and 
			K_C_E_SESSIONE in (0 5)	));
		format data_rif DATE9.;
		dt_inserimento_val=intnx("dthour",dt_data_elaborazione,1);
		dt_rif=intnx("dthour",DT_DATA_RIFERIMENTO,0);
		hour_rif = hour(dt_rif);
		data_rif = datepart(DT_DATA_RIFERIMENTO);
		format dt_inserimento_val dt_rif datetime19.;

		if K_C_E_SESSIONE =5 then
			output terna_cons;
		else if K_C_E_SESSIONE =0 then
			output terna_prev;
	run;

	%if &flg_anomalie=1 %then
		%do;
			%put ===== Controllo Anomalie Dati Terna =====;

			%gestione_anomalie_terna(dt_base=&dt_solare,ds_in1=terna_prev, ds_in2=terna_cons, 
				ds_out1=terna_prev, ds_out2=terna_cons,
				storico_anomalie_terna=&output_anomalie_terna);
		%end;

	/*-------- CONSIDERO CONSUNTIVO terna_cons ---------*/
	/* considero il consuntivo */
	proc sql noprint;
		create table consuntivi_up_remi as
			select t1.*, t2.REMI from terna_cons t1 inner join
			(select distinct K_D_E_UP, REMI from &input_up where REMI in 
			(select REMI from &input_remi_termo)) t2
				on t1.K_D_E_UP = t2.K_D_E_UP
			order by remi, K_D_E_UP, dt_rif, dt_inserimento_val;
	quit;

	data consuntivi_up_remi;
		set consuntivi_up_remi;
		by remi K_D_E_UP dt_rif;

		if last.dt_rif then
			output;
	run;

	/* DATI ARRIVANO ALLE 12 O ALLE 13 DEL GIORNO G 
	RIFERITI ALLE VARIE ORE E VARIE UP DEL GIORNO G-1 (giorno
	solare e non giorno gas!) */

	/*consuntivo orario per REMI*/
	/*problemi con remi-> (remi="30314101" or remi="50034301") and consuntivo>1000000 nel 2012*/
	proc sql noprint;
		create table consuntivi_remi as select
			dt_rif, dt_inserimento_val,data_rif, hour_rif, 
			REMI, sum(energia_programmata) as CONSUNTIVO
		from consuntivi_up_remi 
			group by dt_rif, dt_inserimento_val,data_rif, hour_rif, REMI;
	quit;

	data consuntivi_remi_mod;
		set consuntivi_remi;

		if (remi eq "31307801" or remi eq "50034301" or remi eq "30314101") and data_rif<"01JAN2013"d then
			do;
				if consuntivo >=2000 and consuntivo <=10000 then
					consuntivo=consuntivo/10;
				else if consuntivo >10000 and consuntivo <=100000 then
					consuntivo=consuntivo/100;
				else if consuntivo >10000 and consuntivo <=100000 then
					consuntivo=consuntivo/100;
				else if consuntivo >100000 and consuntivo <=1000000 then
					consuntivo=consuntivo/1000;
				else if consuntivo >1000000 and consuntivo <=10000000 then
					consuntivo=consuntivo/10000;
				else if consuntivo >10000000 and consuntivo <=100000000 then
					consuntivo=consuntivo/100000;
				else if consuntivo >100000000 and consuntivo <=1000000000 then
					consuntivo=consuntivo/1000000;
				else if consuntivo >1000000000 and consuntivo <=10000000000 then
					consuntivo=consuntivo/10000000;
				else if consuntivo >10000000000 and consuntivo <=100000000000 then
					consuntivo=consuntivo/100000000;
				else if consuntivo >100000000000 and consuntivo <=1000000000000 then
					consuntivo=consuntivo/1000000000;
				else if consuntivo >1000000000000 and consuntivo <=10000000000000 then
					consuntivo=consuntivo/10000000000;
				else if consuntivo >10000000000000 and consuntivo <=100000000000000 then
					consuntivo=consuntivo/100000000000;
			end;
	run;

	proc sql noprint;
		create table consuntivi_orario as select
			data_rif, hour_rif, sum(CONSUNTIVO) as CONSUNTIVO
		from consuntivi_remi_mod 
			group by data_rif, hour_rif
				order by data_rif, hour_rif;
	quit;

	/*-----*/

	/* faccio ffill per hour di consuntivo_orario, quindi per ogni ora utilizzo
	il valore del giorno prima nella stessa ora*/

	/*&g_gas*/
	PROC SQL;
		select  max(t1.data_rif), min(t1.data_rif) into :max_d_terna, :min_d_terna
			FROM WORK.consuntivi_orario t1;
	QUIT;

	/**/
	data cons_list_calendar;
		data_rif=&min_d_terna;

		do while (data_rif<=&max_d_terna);
			do hour_rif=0 to 23;
				output;
			end;

			data_rif=intnx('day', data_rif, 1);
		end;

		format data_rif date9.;
	run;

	proc sort data=consuntivi_orario out=consuntivi_orario;
		by data_rif hour_rif;
	run;

	data consuntivi_orario_nofill;
		merge consuntivi_orario cons_list_calendar;
		by data_rif hour_rif;
	run;

	proc sort data=consuntivi_orario_nofill out=consuntivi_orario_nofill;
		by hour_rif data_rif;
	run;

	data consuntivi_orario_fill (drop= consuntivo_temp);
		set	consuntivi_orario_nofill;
		by hour_rif data_rif;
		retain consuntivo_temp;

		if first.hour_rif then
			consuntivo_temp=consuntivo;
		else
			do;
				if consuntivo ne . then
					consuntivo_temp=consuntivo;
			end;

		consuntivo=consuntivo_temp;
	run;

	proc sort data=consuntivi_orario_fill out=consuntivi_orario_fill;
		by data_rif hour_rif;
	run;

	data consuntivi_orario_fill(where=(datepart(dt_inserimento_val)>="01JAN2012"d));
		retain data_rif hour dt_inserimento_val data_inserimento_val;
		set consuntivi_orario_fill;
		dt_inserimento_val=dhms(intnx("day",data_rif,1),13,0,0);
		data_inserimento_val=datepart(dhms(intnx("day",data_rif,1),13,0,0));
		format dt_inserimento_val datetime.;
		format data_inserimento_val date9.;
	run;

	/*Ora ho i valori di consuntivo anche sulle ore precedenti alle 13
	in realta quel dato manca prima delle 13! imputo con i valori orari 
	del giorno prima
	****potevo utilizzare 7g fa */
	data consuntivi_orario_yday(keep=data_rif hour_rif CONSUNTIVO);
		set consuntivi_orario_fill;
		fake_data_rif=intnx("DAY",data_rif,1);
		format fake_data: date9.;
		drop data_rif;
		rename fake_data_rif=data_rif;
	run;

	/*consuntivo_post=consuntivo della data di riferimento, ma ho i valori dopo le 13*/
	/*consuntivo_yday=consuntivo del giorno precedente alla data di riferimento che uso */
	/*	per imputare le prime ore della data di riferimento finch? arriva il consuntivo corretto*/
	/*consuntivo_mix=per la data di riferimento usa i valori orari del giorno prima finch?*/
	/*	arriva il consuntivo alle ore 13*/
	data consuntivo_orario_imp(drop=dt_inserimento_val data_inserimento_val);
		merge consuntivi_orario_fill(in=uno rename=(consuntivo=consuntivo_post) ) 
			consuntivi_orario_yday(in=due rename=(consuntivo=consuntivo_yday) );
		by data_rif hour_rif;
		consuntivo_mix=ifn(hour_rif<13,consuntivo_yday,consuntivo_post);

		if consuntivo_mix eq . then
			consuntivo_mix=consuntivo_yday;

		if dt_inserimento_val eq . then
			dt_inserimento_val=dhms(data_rif,13,0,0);

		if hour_rif lt 6 then
			giorno_gas_rif=data_rif-1;
		else giorno_gas_rif=data_rif;
		dt_riferimento=dhms(data_rif,hour_rif,0,0);
		format giorno_gas_rif date9. dt_riferimento datetime19.;
	run;

	/* cumulati devono rispettare queste logiche
	creo la stima usando i consuntivi di ieri fino alle 13 poi calcolo valore reale */
	data consuntivo_orario_cum;
		set consuntivo_orario_imp;
		by data_rif;
		retain cons_day_cum_pre cons_day_cum_post;

		if first.data_rif and consuntivo_mix ne . then
			do;
				cons_day_cum_pre=0;
				cons_day_cum_post=0;
			end;
		else if consuntivo_mix eq . or cons_day_cum_pre eq . then
			do;
				cons_day_cum_pre=.;
				cons_day_cum_post=.;
			end;

		cons_day_cum_pre+consuntivo_mix;
		cons_day_cum_post+consuntivo_post;

		if 0<=hour_rif<=12 or consuntivo_post eq . then
			cons_day_cum=cons_day_cum_pre;
		else cons_day_cum=cons_day_cum_post;
		drop consuntivo_post consuntivo_yday consuntivo_mix
			cons_day_cum_pre;
	run;

	data consuntivo_orario_gcum;
		set consuntivo_orario_imp;
		by giorno_gas_rif;
		retain cons_gday_cum_pre cons_gday_cum_post;

		if first.giorno_gas_rif and consuntivo_mix ne . then
			do;
				cons_gday_cum_pre=0;
				cons_gday_cum_post=0;
			end;
		else if consuntivo_mix eq . or cons_gday_cum_pre eq . then
			do;
				cons_gday_cum_pre=.;
				cons_gday_cum_post=.;
			end;

		cons_gday_cum_pre+consuntivo_mix;
		cons_gday_cum_post+consuntivo_post;

		if 0<=hour_rif<=12 or consuntivo_post eq . then
			cons_gday_cum=cons_gday_cum_pre;
		else cons_gday_cum=cons_gday_cum_post;
		drop consuntivo_post consuntivo_yday consuntivo_mix
			cons_gday_cum_pre;
	run;

	data CONS_ORARIA_TERNA_tmp(drop= giorno_gas_rif data_rif hour_rif  rename= (
		date=giorno_solare ) );
		merge consuntivo_orario_imp(rename=consuntivo_mix=cons
			drop=consuntivo_yday)
			consuntivo_orario_cum
			consuntivo_orario_gcum;
		by dt_riferimento;
		dt_calcolo=intnx("dthour",dt_riferimento,+24);
		format dt: datetime19.;
	run;

	data CONS_ORARIA_TERNA(drop= hour_rif consuntivo_post cons_gday_cum_post cons_day_cum_post  rename= (
		date=giorno_solare ) );
		merge calendario_orario(in=p)
			CONS_ORARIA_TERNA_tmp;
		by dt_calcolo;

		if p;
		format dt: datetime19.;
	run;

	proc sql noprint;
		%let savelist=;
		select compress(cat("terna_", name))
			into :savelist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="CONS_ORARIA_TERNA"
						and name like "cons%";
	quit;

	proc sql noprint;
		%let droplist=;
		select name
			into :droplist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="CONS_ORARIA_TERNA"
						and name like "cons%";
	quit;

	proc sort data=CONS_ORARIA_TERNA out=CONS_ORARIA_TERNA_SORTED;
		by hour giorno_solare;
	run;

	data input_up_consuntivo_ora_termo(drop=a b i j &droplist);
		set	CONS_ORARIA_TERNA_SORTED;
		array new &savelist;
		array old &droplist;
		by hour;
		retain new;
		a=dim(new);
		b=dim(old);

		if first.hour then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;
	run;

	proc sort data=input_up_consuntivo_ora_termo;
		by dt_calcolo;
	run;

	/* primo output della funzione input_up_consuntivo_ora_termo */
	/*-----------------------------------*/
	/*CONSUNTIVO GIORNALIERO*/
	proc sql noprint;
		create table consuntivo_giorno as select
			data_rif, sum(CONSUNTIVO_POST) as CONSUNTIVO,
			sum(CONSUNTIVO_yday) as CONSUNTIVO_yday,
			dhms(intnx("day",data_rif,1),13,0,0) format=datetime19. as dt_inserimento_val
		from consuntivo_orario_imp 
			group by data_rif
				order by data_rif;
	quit;

	data consuntivo_giorno;
		set consuntivo_giorno;

		if _N_=1 then
			dt_inserimento_val=dhms(intnx("day",data_rif,2),6,0,0);
	run;

	/*data consuntivo_giorno;*/
	/*	set consuntivo_giorno;*/
	/*	date=datepart(dt_inserimento_val);*/
	/*	data_inserimento_val=datepart(dt_inserimento_val);*/
	/*	hour_inserimento_val=hour(dt_inserimento_val);*/
	/*	dt_calcolo=intnx("dthour",dt_riferimento,+24);*/
	/*	format dt: datetime19.;* date date9.;*/
	/*run;*/
	data consuntivo_giorn_final(where=(giorno_gas<=&g_gas));
		merge calendario_orario(in=p) 
			consuntivo_giorno(Rename=(dt_inserimento_val=dt_calcolo));
		by dt_calcolo;

		if p;
	run;

	/*ci sono dati ancora da imputare, dove valore mancante in t deve essere uguale a quello precedente
	pi? vicino a livello temporale (quindi t-1, t-2, ecc)*/
	proc sql noprint;
		%let savelist=;
		select compress(cat("terna_", name))
			into :savelist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="CONSUNTIVO_GIORN_FINAL"
						and index(lowcase(name),'cons');
	quit;

	proc sql noprint;
		%let droplist=;
		select name
			into :droplist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="CONSUNTIVO_GIORN_FINAL"
						and index(lowcase(name),'cons');
	quit;

	data CONSUNTIVO_GIORN_FINAL_fill(drop=a b i j &droplist);
		set	CONSUNTIVO_GIORN_FINAL;
		array new &savelist;
		array old &droplist;
		retain new;
		a=dim(new);
		b=dim(old);

		if _N_=1 then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;
	run;

	data input_up_consuntivo_termo(where=(giorno_gas>="03JAN2012"d) 
		drop=terna_CONSUNTIVO_yday data_rif dt_inserimento_val
		rename=(date=giorno_solare));
		set CONSUNTIVO_GIORN_FINAL_fill;
	run;

	/* secondo output della funzione input_up_consuntivo_termo */
	/*------------------------------Previsioni TERNA------------------------------------*/

	/* per modello G uso previsione MGP fatta ieri e riferita a oggi
	ho quel valore per tutto il giorno*/
	proc sort data=TERNA_PREV out=TERNA_mgp_sort;
		by dt_rif dt_inserimento_val k_d_e_up;
	run;

	proc sql noprint;
		create table terna_remi_prev as
			select t1.*, t2.REMI from TERNA_mgp_sort t1 inner join
			(select distinct K_D_E_UP, REMI from &input_up where REMI in 
			(select REMI from &input_remi_termo)) t2
				on t1.K_D_E_UP = t2.K_D_E_UP
			order by dt_rif, remi, K_D_E_UP;
	quit;

	data terna_remi_mgp_sort;
		set terna_remi_prev;
		by dt_rif remi K_D_E_UP dt_inserimento_val;

		if last.dt_inserimento_val then
			output;
	run;

	data terna_remi_mgp_sort(drop=a);
		set terna_remi_mgp_sort;
		a=datepart(dt_rif)-datepart(dt_inserimento_val);

		if a ne 1 then
			dt_inserimento_val=dhms(intnx("day", datepart(dt_rif), -1),13,0,0);
	run;

	/*Previsioni orarie per remi*/
	proc sql noprint;
		create table mgp_remi_orarie as select
			dt_rif, dt_inserimento_val,data_rif, hour_rif, REMI, 
			sum(energia_programmata) as prev_mgp
		from terna_remi_mgp_sort 
			group by dt_rif, dt_inserimento_val,data_rif, hour_rif, REMI;
	quit;

	/*Previsioni orarie*/
	proc sql noprint;
		create table mgp_orario as select
			distinct data_rif, hour_rif,dt_inserimento_val, sum(prev_mgp) as prev_mgp
				from  mgp_remi_orarie  
					group by data_rif, hour_rif
						order by data_rif, hour_rif;
	quit;

	/* faccio ffill per hour di consuntivo_orario, quindi per ogni ora utilizzo
	il valore del giorno prima nella stessa ora*/
	PROC SQL;
		select max(t1.data_rif), min(t1.data_rif) into :max_p_terna, :min_p_terna
			FROM WORK.mgp_orario t1;
	QUIT;

	data prev_list_calendar;
		data_rif=&min_p_terna;

		do while (data_rif<=&max_p_terna);
			do hour_rif=0 to 23;
				dt_inserimento_val_imp=dhms(intnx("day", data_rif, -1),13,0,0);
				output;
			end;

			data_rif=intnx('day', data_rif, 1);
		end;

		format data_rif date9.;
		format dt_inserimento_val_imp datetime19.;
	run;

	proc sort data=mgp_orario out=mgp_orario;
		by data_rif hour_rif;
	run;

	data terna_prev_base_nofill(drop=dt_inserimento_val_imp);
		merge mgp_orario prev_list_calendar;
		by  data_rif hour_rif;

		if dt_inserimento_val = . then
			dt_inserimento_val=dt_inserimento_val_imp;
	run;

	proc sort data=terna_prev_base_nofill out=terna_prev_base_nofill;
		by hour_rif data_rif;
	run;

	data terna_prev_base_fill (drop= prev_mgp_temp);
		set	terna_prev_base_nofill;
		by hour_rif data_rif;
		retain prev_mgp_temp;

		if first.hour_rif then
			prev_mgp_temp=prev_mgp;
		else
			do;
				if prev_mgp ne . then
					prev_mgp_temp=prev_mgp;
			end;

		prev_mgp=prev_mgp_temp;
	run;

	proc sort data=terna_prev_base_fill;
		by data_rif hour_rif;
	run;

	/*----------*/
	proc sql noprint;
		create table mgp_giornaliero as select
			data_rif, dt_inserimento_val, sum(prev_mgp) as prev_mgp_giorn
		from terna_prev_base_fill 
			group by data_rif, dt_inserimento_val
				order by data_rif, dt_inserimento_val;
	quit;

	data mgp_orario(where=(datepart(dt_inserimento_val)>="01JAN2012"d));
		retain data_rif hour dt_inserimento_val data_inserimento_val;
		set terna_prev_base_fill;
		data_inserimento_val=datepart(dt_inserimento_val);
		format dt_inserimento_val datetime.;
		format data_inserimento_val date9.;

		if hour_rif lt 6 then
			giorno_gas_rif=data_rif-1;
		else giorno_gas_rif=data_rif;
		format giorno_gas_rif date9.;
	run;

	data mgp_cumulato_gsolare;
		set mgp_orario;
		by data_rif hour_rif;
		retain mgp_day_cum;

		if first.data_rif then
			mgp_day_cum=0;
		mgp_day_cum+prev_mgp;
	run;

	data mgp_cumulato_ggas;
		set mgp_orario;
		by giorno_gas_rif;
		retain mgp_gday_cum;

		if first.giorno_gas_rif then
			mgp_gday_cum=0;
		mgp_gday_cum+prev_mgp;
	run;

	data mgp_orario_final(rename=(prev_mgp=prev_mgp_mixv));
		merge mgp_cumulato_gsolare(in=uno)
			mgp_cumulato_ggas (in=due);
		by data_inserimento_val hour_rif;

		if uno and due;
	run;

	/* imputo valore orario, cumulato giorno e cumulato giorno gas con i consuntivi
	di quei giorni*/
	data mgp_imputazione_orariocum(rename=consuntivo_mix=cons);
		merge consuntivo_orario_imp consuntivo_orario_gcum consuntivo_orario_cum;
		by data_rif hour_rif;
		keep data_rif hour_rif dt_inserimento_val_c cons_gday_cum	cons_day_cum consuntivo_mix;
		dt_inserimento_val_c=dhms(intnx("day",data_rif,-1),0,0,0);
		format dt_inserimento_val_c datetime19.;
	run;

	data mgp_orario_final_fill(rename=(prev_mgp_mixv=mgp_oraria) drop= dt_inserimento_val_c);
		merge calendario_orario(in=k rename=(date=data_rif hour=hour_rif) drop= dt_calcolo giorno_gas) 
			mgp_orario_final(in=p) mgp_imputazione_orariocum(in=q);
		by data_rif hour_rif;

		if prev_mgp_mixv eq . then
			do;
				prev_mgp_mixv=cons;
				dt_inserimento_val=dt_inserimento_val_c;
				mgp_day_cum=cons_day_cum;
				mgp_gday_cum=cons_gday_cum;
			end;

		data_inserimento_val=datepart(dt_inserimento_val);
		drop cons:;
		drop date dt_calcolo giorno_gas_rif;

		if k;

		/*		if p or q;*/
	run;

	/**/
	/*	proc sql noprint;*/
	/*		select max(data_rif)*/
	/*			into :max_date_prevt*/
	/*				from terna_prev;*/
	/*	run;*/
	/*(where=(data_gas_g lt &max_date_prevt))*/
	data mgp_ORARIA_final(where=(giorno_gas <=&g_gas) drop=	data_inserimento_val
		dt_inserimento_val);
		retain data_solare_G datetime_solare_g	data_gas_G data_rif	hour terna_mgp_giorn;
		merge mgp_orario_final_fill(rename=(data_rif=date hour_rif=hour) in=uno) 
			work.calendario_orario (in=due);
		by date hour;

		if due;
	run;

	proc sql noprint;
		%let savelist=;
		select compress(cat("terna_", name))
			into :savelist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="MGP_ORARIA_FINAL"
						and name like "mgp%";
	quit;

	proc sql noprint;
		%let droplist=;
		select name
			into :droplist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="MGP_ORARIA_FINAL"
						and name like "mgp%";
	quit;

	proc sort data=MGP_ORARIA_FINAL out=MGP_ORARIA_FINAL;
		by hour giorno_gas;
	run;

	data input_up_mgp_ora_termo(drop= a b i j &droplist rename=(date=giorno_solare));
		set	MGP_ORARIA_FINAL;
		array new &savelist;
		array old &droplist;
		by hour giorno_gas;
		retain new;
		a=dim(new);
		b=dim(old);

		if first.hour then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;
	run;

	proc sort data=input_up_mgp_ora_termo;
		by dt_calcolo;
	run;

	/* terzo output della funzione:input_up_mgp_ora_termo */
	/*-----------------------------------------PREVISIONI GIORNO-----------------------------------------*/
	proc sql noprint;
		create table mgp_giornaliero as select
			data_rif, dt_inserimento_val, sum(prev_mgp) as prev_mgp_giorn
		from terna_prev_base_fill 
			group by data_rif, dt_inserimento_val
				order by data_rif, dt_inserimento_val;
	quit;

	/*	data mgp_giornaliero_copy(where=(datepart(dt_inserimento_val)>="01JAN2012"d));*/
	/*		set mgp_giornaliero;*/
	/*		rename prev_mgp_giorn=prev_mgp_mixv;*/
	/*	run;*/
	/* ----------------------------------------------------- */
	/*imputo passato con consuntivi*/
	proc sql noprint;
		create table mgp_imputazione_giorn as select
			intnx("day",giorno_solare,-1) format=date9. as data_rif, 
			terna_CONSUNTIVO as prev_mgp_giorn_imp
		from input_up_consuntivo_termo
			where hour=22 
				order by data_rif;
	quit;

	/*ho il consuntivo riferito al giorno data_rif...poich? sto ricostruendo lo storico
	fingo di sapere la previsione per quel valore dalle 13 del giorno precedente a data_rif da inizio giornata*/
	data mgp_imputazione_giorn(where=(Data_rif<&G_gas and datepart(dt_inserimento_val)>="01JAN2012"d));
		set mgp_imputazione_giorn;
		dt_inserimento_val=dhms(intnx("day",data_rif,-1),13,0,0);
		format dt_inserimento_val datetime19.;
	run;

	/*(rename=(prev_mgp_mixv=mgp_giorn)*/
	data previsioni_giorn2 (drop=prev_mgp_giorn_imp dt_inserimento_val_c);
		merge mgp_giornaliero mgp_imputazione_giorn(rename=(dt_inserimento_val=dt_inserimento_val_c));
		by data_rif;

		if prev_mgp_giorn eq . then
			do;
				prev_mgp_giorn=prev_mgp_giorn_imp;
				dt_inserimento_val=dt_inserimento_val_c;
			end;
	run;

	proc sql noprint;
		select max(data_rif)
			into :max_date_prevt
				from terna_prev;
	run;

	data previsioni_giorn_final( where=(giorno_gas<=&max_date_prevt)
		rename=(prev_mgp_giorn=mgp_giorn));
		merge calendario_orario(In=p) previsioni_giorn2(Rename=(data_rif=date) );
		by date;

		if p;
	run;

	/*dove valore mancante in t deve essere uguale a quello precedente
	pi? vicino a livello temporale (quindi t-1, t-2, ecc)*/
	proc sql noprint;
		%let savelist=;
		select compress(cat("terna_", name))
			into :savelist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="PREVISIONI_GIORN_FINAL"
						and name like "mgp%";
	quit;

	proc sql noprint;
		%let droplist=;
		select name
			into :droplist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="PREVISIONI_GIORN_FINAL"
						and name like "mgp%";
	quit;

	data prevt_GIORN_FINAL_fill(drop= a b i j &droplist);
		set	PREVISIONI_GIORN_FINAL;
		array new &savelist;
		array old &droplist;
		retain new;
		a=dim(new);
		b=dim(old);

		if _N_=1 then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;

		if not missing(dt_calcolo);
	run;

	/*prev fatte in G per G+1*/
	data input_up_mgp_termo;
		set prevt_GIORN_FINAL_fill(where=(giorno_gas>="03JAN2012"d) 
			rename=(date=giorno_solare));
		drop dt_inserimento_val;
	run;

	/* quarto output = input_up_mgp_termo */
	/*------------------------------CONSUNTIVI-------------------------------*/
	/*----------------------------lag giornalieri----------------------------*/
	/*-----------------------------------------------------------------------*/
	data cons_lag_base(drop=hour);
		set input_up_consuntivo_termo(keep=hour giorno_solare terna_consuntivo
			where=(hour=23));
		output;

		if giorno_solare = &g_gas  and giorno_solare < &g_solare then
			do;
				giorno_solare=giorno_solare+1;
				output;
			end;
	run;

	data cons1;
		set cons_lag_base;
		cross_section = 'dummy1';
	run;

	data cons2;
		set cons_lag_base;
		cross_section = 'dummy2';
	run;

	data cons_shift;
		set cons1 cons2;
	run;

	proc sort data=work.cons_shift nodupkey;
		by cross_section giorno_solare;
	run;

	DATA X;
		DO X=1 TO 28;
			OUTPUT;
		END;
	RUN;

	PROC SQL NOPRINT;
		SELECT X INTO:LG SEPARATED BY ' ' FROM X;
	QUIT;

	PROC PANEL DATA=cons_shift;
		ID cross_section giorno_solare;
		LAG terna_CONSUNTIVO(&LG.) /OUT=CONSUNTIVO_shift;
	run;

	data consuntivo_giorno_lag_temp;
		set CONSUNTIVO_shift;

		if cmiss(of _all_) then
			delete;

		if cross_section = "dummy1";
		drop cross_section terna_CONSUNTIVO;
	run;

	data cons_lag_giorn_final;
		merge calendario_orario(Rename=date=giorno_solare in=p) 
			consuntivo_giorno_lag_temp(in=q);
		by giorno_solare;

		if p and q;
	run;

	proc sql noprint;
		%let savelist=;
		select compress(tranwrd(name,"CONSUNTIVO_","cons_day_lag"))
			into :savelist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="CONS_LAG_GIORN_FINAL"
						and name like "terna%";
	quit;

	proc sql noprint;
		%let droplist=;
		select name
			into :droplist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="CONS_LAG_GIORN_FINAL"
						and name like "terna%";
	quit;

	data input_cons_lag(drop=a b i j &droplist);
		set	cons_lag_giorn_final;
		array new &savelist;
		array old &droplist;
		retain new;
		a=dim(new);
		b=dim(old);

		if _N_=1 then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;

		format data: date9.;
	run;

	/*---------------------------CONSUNTIVI--------------------------*/
	/*----------------------------lag orari--------------------------*/
	/*---------------------------------------------------------------*/
	data consuntivo_orario_cum_lag;
		merge CONS_ORARIA_TERNA_tmp calendario_orario(IN=P rename=(date=giorno_solare));
		giorno_solare=datepart(dt_calcolo);
		by dt_calcolo;

		IF P;
		drop cons cons_day_cum cons_gday_cum;
	run;

	data consuntivo_orario_lag(rename=(CONSUNTIVO_post=terna_cons_oraria_lag7d
		cons_day_cum_post=terna_cons_oraria_cumday_lag7d
		cons_gday_cum_post=terna_cons_oraria_cumgday_lag7d)
		drop=dt_riferimento dt_calcolo giorno_solare giorno_gas);
		set consuntivo_orario_cum_lag;
		dt_calcolo_7=intnx("dthour",dt_calcolo,24*7);
		giorno_solare_7=intnx("day",giorno_solare,7);
		giorno_gas_7=intnx("day",giorno_gas,7);
		format giorno: date9. dt_calcolo_7 datetime19.;
	run;

	data input_cons_orario_lag;
		merge	consuntivo_orario_lag(rename=(dt_calcolo_7=dt_calcolo 
			giorno_solare_7= giorno_solare giorno_gas_7=giorno_gas) in=q)
			calendario_orario(in=p keep=dt_calcolo);
		by dt_calcolo;

		if p and q;
	run;

	/*------------------------------PREVISIONI-------------------------------*/
	/*----------------------------lag giornalieri----------------------------*/
	/*-----------------------------------------------------------------------*/
	data prev_lag_base(drop=hour);
		set input_up_mgp_termo(keep=hour giorno_solare terna_mgp_giorn
			where=(hour=23));
		output;

		if giorno_solare = &g_gas  and giorno_solare < &g_solare then
			do;
				giorno_solare=giorno_solare+1;
				output;
			end;
	run;

	data prev1;
		set prev_lag_base;
		cross_section = 'dummy1';
	run;

	data prev2;
		set prev_lag_base;
		cross_section = 'dummy2';
	run;

	data mgp_shift;
		set prev1 prev2;
	run;

	proc sort data=work.mgp_shift nodupkey;
		by cross_section giorno_solare;
	run;

	DATA X;
		DO X=1 TO 28;
			OUTPUT;
		END;
	RUN;

	PROC SQL NOPRINT;
		SELECT X INTO:LG SEPARATED BY ' ' FROM X;
	QUIT;

	PROC PANEL DATA=mgp_shift;
		ID cross_section giorno_solare;
		LAG terna_mgp_giorn(&LG.) /OUT=PREVISIONE_shift;
	run;

	data previsione_giorno_lag;
		set PREVISIONE_shift;

		if cmiss(of _all_) then
			delete;

		if cross_section = "dummy1";
		drop cross_section terna_mgp_giorn;
		dt_inserimento_val=dhms(intnx("day",giorno_solare,-1),13,0,0);
		format dt_inserimento_val datetime.;
	run;

	data previsioni_lag_temp;
		merge work.calendario_orario(Rename=(date =giorno_solare) in=p) 
			previsione_giorno_lag(in=q);
		by giorno_solare;

		if p and q;
	run;

	proc sql noprint;
		%let savelist=;
		select compress(tranwrd(name,"mgp_giorn_","mgp_day_lag"))
			into :savelist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="PREVISIONI_LAG_TEMP"
						and index(lowcase(name),'mgp_giorn');
	quit;

	proc sql noprint;
		%let droplist=;
		select name
			into :droplist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="PREVISIONI_LAG_TEMP"
						and index(lowcase(name),'mgp_giorn');
	quit;

	data input_mgp_lag(drop= a b i j &droplist);
		set	PREVISIONI_LAG_TEMP;
		array new &savelist;
		array old &droplist;
		retain new;
		a=dim(new);
		b=dim(old);

		if _N_=1 then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;

		format data: date9.;
	run;

	/*---------------------------PREVISIONI--------------------------*/
	/*----------------------------lag orari--------------------------*/
	/*---------------------------------------------------------------*/
	data previsione_oraria_lag;
		set input_up_mgp_ora_termo;

		/*		giorno_solare_7=intnx("DAY",giorno_solare,7,"S");*/
		/*		giorno_solare_1=intnx("DAY",giorno_solare,1,"S");*/
		dt_calcolo_1=intnx("dthour",dt_calcolo,24*1);
		dt_calcolo_7=intnx("dthour",dt_calcolo,24*7);
		format giorno_solare: date9. dt_calcolo: datetime19.;
		rename terna_mgp_day_cum=terna_mgp_oraria_cumday
			terna_mgp_gday_cum=terna_mgp_oraria_cumgday;
	run;

	proc sql noprint;
		%let droplist=;
		select compress(cat(name,"_lag1d"))
			into :droplist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="PREVISIONE_ORARIA_LAG"
						and name like "terna%";
	quit;

	proc sql noprint;
		%let savelist=;
		select name
			into :savelist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="PREVISIONE_ORARIA_LAG"
						and name like "terna%";
	quit;

	data previsione_oraria_lag1(drop= a b i j &savelist rename=(dt_calcolo_1=dt_calcolo ));
		set	previsione_oraria_lag(drop=giorno: dt_calcolo dt_calcolo_7 );
		array new &droplist;
		array old &savelist;
		retain new;
		a=dim(new);
		b=dim(old);

		if _N_=1 then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;
	run;

	proc sql noprint;
		%let droplist=;
		select compress(cat(name,"_lag7d"))
			into :droplist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="PREVISIONE_ORARIA_LAG"
						and name like "terna%";
	quit;

	proc sql noprint;
		%let savelist=;
		select name
			into :savelist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="PREVISIONE_ORARIA_LAG"
						and name like "terna%";
	quit;

	data previsione_oraria_lag7(drop= a b i j &savelist rename=(dt_calcolo_7=dt_calcolo ));
		set	previsione_oraria_lag(drop=giorno: dt_calcolo dt_calcolo_1 );
		array new &droplist;
		array old &savelist;
		retain new;
		a=dim(new);
		b=dim(old);

		if _N_=1 then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;
	run;

	data input_mgp_orario_lag;
		merge calendario_orario(in=uno keep=dt_calcolo) previsione_oraria_lag7(in=due)
			previsione_oraria_lag1(in=tre);
		by dt_calcolo;

		if uno and due and tre;
	run;

	proc datasets lib=work nolist;
		delete cons: mgp:;
	run;

	/*-------------------------------merge all-------------------------------*/
	data &output_terna_cons(where=(datepart(dt_calcolo)>=&start_date_terna));
		merge  input_up_consuntivo_termo input_cons_lag input_up_consuntivo_ora_termo 
			input_cons_orario_lag;
		by dt_calcolo;
		drop dt_riferimento;
		rename terna_cons=terna_cons_oraria terna_CONSUNTIVO=terna_cons_day 
			terna_cons_day_cum=terna_cons_oraria_cumday
			terna_cons_gday_cum=terna_cons_oraria_cumgday;
	run;

	data &output_terna_prev(where=(datepart(dt_calcolo)>=&start_date_terna));
		merge  input_up_mgp_termo input_mgp_lag input_up_mgp_ora_termo 
			input_mgp_orario_lag;
		by dt_calcolo;
		drop dt_inserimento_val;
		rename terna_mgp_day_cum=terna_mgp_oraria_cumday 
			terna_mgp_gday_cum=terna_mgp_oraria_cumgday terna_mgp_giorn=terna_mgp_day;
	run;

%mend GXG_create_terna;

%macro checkds(dsn);
	%if %sysfunc(exist(&dsn)) %then
		%do;
			%let dataset_presente=1;
		%end;
	%else
		%do;
			%let dataset_presente=0;
		%end;
%mend checkds;

%macro GxG_create_scada(input_scada, start_date, output);

	data _null_;
		start_date_adj=intnx("month",&start_date,-37, 'S');
		call symput('start_date_adj',start_date_adj);
	run;

	%get_scada_comp(&input_scada,&start_date_adj, scada_bil_temp);

	data scada_bil_temp(drop= hour_scada);
		set scada_bil_temp(rename=(giorno_gas_prev=data_gas_rif));
	run;

	%create_calendar_vars(scada_bil_temp,scada_bil_time_temp );
	%create_scada_sim_vars(scada_bil_time_temp, &start_date, &output);

	data &output;
		set &output;
		drop time: hour:;
	run;

%mend GxG_create_scada;

%macro get_scada_comp(input_bilancioSCADA, start_date_comp, output);
	/*	%let cut_date_fe_scada="03APR2008"d;*/
	data _null_;
		dt_char=put(dhms(&start_date_comp,6,0,0), datetime19.);
		call symput('start_dt_sc',dt_char);
	run;

	data flussifisici1;
		set &input_bilancioSCADA(where=(dt_calcolo>="&start_dt_sc"dt ));
		date=datepart(dt_calcolo);
		format date date9.;
		hour_scada=timepart(dt_calcolo);
		format hour_scada hhmm.;
		rename totale=totale_smc;
		select (tx_tipologia_punto);
			when ("Entry Importazione")				type = "imp";
			when ("Entry Produzione Nazionale")		type = "pnl";
			when ("Exit Esportazione")				type = "exp";
			when ("Immissione a stoccaggio")		type = "ims";
			when ("Riconsegnato")					type = "ric";
			when ("Prelievo da stoccaggio")			type = "prs";
			when ("Delta Linepack")					type = "dlp";
			otherwise;
		end;
	run;

	/* Drop duplicates */
	proc sort data=flussifisici1 out=flussifisici1_nodup nodupkey;
		by type dt_calcolo;
	run;

	/* Bonifica su eventuali valori anomali nella riconsegna e importazioni
		(imp <=0 or ric <=0) ed eventuali valori mancanti nel pcs */
	data flussifisici1_bonifica_storico (drop=totale_smc_tmp pcs_tmp);
		set flussifisici1_nodup ;
		by type dt_calcolo;
		
		/* RETAIN the new variable */
		retain totale_smc_tmp pcs_tmp flg_imp_ric flg_pcs;
		flg_imp_ric=0;

		/* Reset TEMP when the BY-Group changes */
		if first.type then do;
			totale_smc_tmp=.;
			pcs_tmp=.;
		end;

		/* Assign TEMP when valore_clean is non-missing */
		if totale_smc gt 0 and type in ("imp" "ric") then do;
			totale_smc_tmp=totale_smc;
			flg_imp_ric=0;
		end;
		else if totale_smc le 0 and type in ("imp" "ric") then do;
			totale_smc=totale_smc_tmp;
			flg_imp_ric=1;
		end;

		if pcs gt 0 then do;
			pcs_tmp=pcs;
			flg_pcs=0;
		end;
		else if pcs le 0 then do;
			pcs=pcs_tmp;
			flg_pcs=1;
		end;
		/* calcolo totale_kwh */
		totale_kwh=totale_smc*PCS;
	run;
	
	/* Gestione anomalie scada (solo se sono nella creazione riga di input) */
	%if &flg_anomalie=1 %then
		%do;
			%put ===== Controllo Anomalie Dati Scada Bil =====;

			%gestione_anomalie_scada(dt_base=&dt_solare, ds_in=flussifisici1_nodup, ds_out=flussifisici1_bonifica_storico, 
				storico_anomalie=&output_anomalie_scada);
		%end;
		
	proc sort data=flussifisici1_bonifica_storico;
		by dt_calcolo type;
	run;

	/*prendo il valore in smc, in quanto abbiamo visto che ? pi? esplicativo */
	proc transpose data=flussifisici1_bonifica_storico out=flussifisici_t(drop=_LABEL_);
		by dt_calcolo type;
		var totale_smc totale_kwh;
	run;

	data flussifisici_t_ren;
		set flussifisici_t;

		if _NAME_ = "totale_smc" then
			_NAME_ = "bil";
		else _NAME_ = "kwh";
	run;

	proc transpose data=flussifisici_t_ren out=flussifisici_t_ren_2 delim=_;
		by dt_calcolo;
		id _name_ type;
		var col1;
	run;

	/* calcolo il DLP con la formula inversa */
	data flussifisici_t_smc;
		set flussifisici_t_ren_2;
		bil_stc= bil_prs-bil_ims;

		if bil_dlp=. then
			bil_dlp=sum(bil_imp, bil_pnl, bil_stc, -bil_exp,-bil_ric);
		kwh_stc=kwh_prs-kwh_ims;

		if kwh_dlp=. then
			kwh_dlp=sum(kwh_imp,kwh_pnl,kwh_stc,-kwh_exp,-kwh_ric);
		drop bil_ims bil_prs kwh_prs kwh_ims;
		rename kwh_ric=bil_ric_kwh;
	run;

	data flussi_SCADA;
		set flussifisici_t_smc(drop=kwh: _name_);
		hour_SCADA = hour(dt_calcolo);

		if hour(dt_calcolo) < 6 then
			giorno_gas_SCADA = intnx('day',datepart(dt_calcolo),-1);
		else giorno_gas_SCADA = datepart(dt_calcolo);
		format giorno_gas_SCADA date9.;
	run;

	data calendario_orario;
		date=&start_date_comp;

		do while (date<=&G_solare);
			do hour=0 to 23;
				dt_calcolo=dhms(date,hour,0,0);
				output;
			end;

			date=intnx("day", date, 1, 's');
		end;

		format date date9.;
		format dt_calcolo datetime16.;
	run;

	data _null_;
		set flussi_SCADA;
		call symputx("max_scada",max(dt_calcolo));
	run;

	data flussi_SCADA_check(drop=date hour);
		merge flussi_SCADA calendario_orario(in=p);
		by dt_calcolo;

		if p;

		if hour_Scada=. then
			do;
				hour_scada=hour;
				giorno_gas_scada=ifn(hour_scada<6, intnx("day", datepart(dt_calcolo), -1), datepart(dt_calcolo));
			end;

		if dt_calcolo<=&max_scada;
	run;

	data flussi_SCADA_check1;
		set flussi_SCADA_check;
		datetime_prev=intnx("hour",dt_calcolo,1);
		hour_prev=hour(datetime_prev);
		date_prev=datepart(datetime_prev);
		giorno_gas_prev=ifn(hour_prev<6,intnx("day",date_prev,-1),date_prev);
		format date_prev giorno_gas_prev date9.;
		format datetime_prev datetime18.;
	run;

	data flussi_SCADA_cum;
		set flussi_SCADA_check1;

		if hour_prev=6 then
			do;
				bil_ric=0;
				bil_pnl=0;
				bil_imp=0;
				bil_exp=0;
				bil_stc=0;
				bil_dlp=0;
				bil_ric_kwh=0;
			end;
	run;

	/* riempo i missing con zero: mi serve? penso di no */
	PROC STDIZE DATA=WORK.flussi_SCADA_check1 OUT=WORK.flussi_SCADA_check1_fill REPONLY MISSING=0;
		var bil_dlp bil_exp bil_imp bil_pnl bil_ric bil_stc bil_ric_kwh;
	RUN;

	PROC STDIZE DATA=WORK.flussi_SCADA_cum OUT=WORK.flussi_SCADA_cum_fill REPONLY MISSING=0;
		var bil_dlp bil_exp bil_imp bil_pnl bil_ric bil_stc bil_ric_kwh;
	RUN;

	/* statistiche calcolate sulle precedenti 24 osservazioni INCLUSO SE STESSO (QUINDI ME+23 ORE)*/
	/* il valore associato alle ore H mi dice quanto ? avvenuto fre le H:59 e (H-24):59*/
	/*io posso fare questo calcolo per capire quanto ? passato, e non dipende dal giorno gas!*/
	/* calcolo la moving average, moving sum, moving max, moving min e moving dev std per ogni voce*/
	proc expand data=work.flussi_SCADA_check1_fill out=flussi_scada_mov_stat;
		convert bil_imp = bil_imp_mean / METHOD = none TRANSFORMOUT = (MOVAVE 24);
		convert bil_imp = bil_imp_sum / METHOD = none TRANSFORMOUT = (MOVSUM 24);
		convert bil_imp = bil_imp_max / METHOD = none TRANSFORMOUT = (MOVMAX 24);
		convert bil_imp = bil_imp_min / METHOD = none TRANSFORMOUT = (MOVMIN 24);
		convert bil_imp = bil_imp_std / METHOD = none TRANSFORMOUT = (MOVSTD 24);
		convert bil_pnl = bil_pnl_mean / METHOD = none TRANSFORMOUT = (MOVAVE 24);
		convert bil_pnl = bil_pnl_sum / METHOD = none TRANSFORMOUT = (MOVSUM 24);
		convert bil_pnl = bil_pnl_max / METHOD = none TRANSFORMOUT = (MOVMAX 24);
		convert bil_pnl = bil_pnl_min / METHOD = none TRANSFORMOUT = (MOVMIN 24);
		convert bil_pnl = bil_pnl_std / METHOD = none TRANSFORMOUT = (MOVSTD 24);
		convert bil_stc = bil_stc_mean / METHOD = none TRANSFORMOUT = (MOVAVE 24);
		convert bil_stc = bil_stc_sum / METHOD = none TRANSFORMOUT = (MOVSUM 24);
		convert bil_stc = bil_stc_max / METHOD = none TRANSFORMOUT = (MOVMAX 24);
		convert bil_stc = bil_stc_min / METHOD = none TRANSFORMOUT = (MOVMIN 24);
		convert bil_stc = bil_stc_std / METHOD = none TRANSFORMOUT = (MOVSTD 24);
		convert bil_dlp = bil_dlp_mean / METHOD = none TRANSFORMOUT = (MOVAVE 24);
		convert bil_dlp = bil_dlp_sum / METHOD = none TRANSFORMOUT = (MOVSUM 24);
		convert bil_dlp = bil_dlp_max / METHOD = none TRANSFORMOUT = (MOVMAX 24);
		convert bil_dlp = bil_dlp_min / METHOD = none TRANSFORMOUT = (MOVMIN 24);
		convert bil_dlp = bil_dlp_std / METHOD = none TRANSFORMOUT = (MOVSTD 24);
		convert bil_exp = bil_exp_mean / METHOD = none TRANSFORMOUT = (MOVAVE 24);
		convert bil_exp = bil_exp_sum / METHOD = none TRANSFORMOUT = (MOVSUM 24);
		convert bil_exp = bil_exp_max / METHOD = none TRANSFORMOUT = (MOVMAX 24);
		convert bil_exp = bil_exp_min / METHOD = none TRANSFORMOUT = (MOVMIN 24);
		convert bil_exp = bil_exp_std / METHOD = none TRANSFORMOUT = (MOVSTD 24);
		convert bil_ric = bil_ric_mean / METHOD = none TRANSFORMOUT = (MOVAVE 24);
		convert bil_ric = bil_ric_sum / METHOD = none TRANSFORMOUT = (MOVSUM 24);
		convert bil_ric = bil_ric_max / METHOD = none TRANSFORMOUT = (MOVMAX 24);
		convert bil_ric = bil_ric_min / METHOD = none TRANSFORMOUT = (MOVMIN 24);
		convert bil_ric = bil_ric_std / METHOD = none TRANSFORMOUT = (MOVSTD 24);
	run;

	/* imputo i missing values di moving_stat utilizzando il valore precedente pi? vicino */
	proc sql noprint;
		%let droplist=;
		select compress(cat("scada_",name))
			into :droplist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="FLUSSI_SCADA_MOV_STAT"
						and (name like "%pnl%" or name like "%exp%" or name like "%imp%" or name like "%ric%" or name like "%stc%" or name like "%dlp%" or name like "%ric_kwh%");
	quit;

	proc sql noprint;
		%let savelist=;
		select name
			into :savelist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="FLUSSI_SCADA_MOV_STAT"
						and (name like "%pnl%" or name like "%exp%" or name like "%imp%" or name like "%ric%" or name like "%stc%" or name like "%dlp%" or name like "%ric_kwh%");
	quit;

	data FLUSSI_SCADA_MOV_STAT_FINAL_fill(drop= a b i j &savelist _name_ time);
		set	FLUSSI_SCADA_MOV_STAT;
		array new &droplist;
		array old &savelist;
		retain new;
		a=dim(new);
		b=dim(old);

		if _N_=1 then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;
	run;

	/* calcolo il riconsegnato cumulativo giornaliero */
	data flussi_SCADA_cumday;
		set flussi_SCADA_cum_fill;
		by giorno_gas_prev;
		retain bil_ric_cumday bil_stc_cumday bil_imp_cumday bil_pnl_cumday bil_exp_cumday bil_dlp_cumday;

		if first.giorno_gas_prev then
			do;
				bil_ric_cumday=0;
				bil_stc_cumday=0;
				bil_imp_cumday=0;
				bil_pnl_cumday=0;
				bil_exp_cumday=0;
				bil_dlp_cumday=0;
				bil_ric_kwh_cumday=0;
			end;

		bil_ric_cumday+bil_ric;
		bil_stc_cumday+bil_stc;
		bil_imp_cumday+bil_imp;
		bil_pnl_cumday+bil_pnl;
		bil_exp_cumday+bil_exp;
		bil_dlp_cumday+bil_dlp;
		bil_ric_kwh_cumday+bil_ric_kwh;

		/*		drop time;*/
	run;

	/* imputo cumday dove i missing values sono sostituiti col valore precedente pi? vicino */
	proc sql noprint;
		%let droplist=;
		select compress(cat("scada_",name))
			into :droplist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="FLUSSI_SCADA_CUMDAY"
						and (name like "%pnl_cum%" or name like "%exp_cum%" or name like "%imp_cum%" or name like "%ric_cum%" or name like "%stc_cum%" or name like "%dlp_cum%" or name like "%ric_kwh_cum%");
	quit;

	proc sql noprint;
		%let savelist=;
		select name
			into :savelist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="FLUSSI_SCADA_CUMDAY"
						and (name like "%pnl_cum%" or name like "%exp_cum%" or name like "%imp_cum%" or name like "%ric_cum%" or name like "%stc_cum%" or name like "%dlp_cum%" or name like "%ric_kwh_cum%");
	quit;

	data FLUSSI_SCADA_CUMDAY_fill(drop= a b i j &savelist _name_ bil_ric bil_pnl bil_exp bil_imp bil_dlp bil_stc bil_ric_kwh);
		set	FLUSSI_SCADA_CUMDAY;
		array new &droplist;
		array old &savelist;
		retain new;
		a=dim(new);
		b=dim(old);

		if _N_=1 then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;
	run;

	/* ottengo il dataset, che contiene i dati scada orari (dove valore delle ore cada H ? riferito a quanto ? 
	passato fino alle H:59). Ai fini previsivi il valore scada delle H lo posso usare dalle H+1!
	infatti ho introdotto ora prev */

	/* c'? da fare attenzione all'orario legale e solare quando li guardo: la macchina ha un orario continuo
	mentre io, che uso l'ora di orologio, posso essere avanti di un'ora rispetto a scada */

	/*come ora prev ho utilizzato */
	data &output(where=(giorno_gas_prev>=&start_date_comp and giorno_gas_prev <=&G_gas)
		drop =  date_prev hour_prev hour_SCADA);
		merge work.FLUSSI_SCADA_MOV_STAT_FINAL_fill(in=p) FLUSSI_SCADA_CUMDAY_fill(in=q);
		by dt_calcolo;
	run;

%mend get_scada_comp;

%macro create_calendar_vars(input_cal_dset, dset_with_time);

	data temp;
		set &input_cal_dset;
		time_month = month(data_gas_rif);
		time_day = weekday(data_gas_rif);
		time_week = week(data_gas_rif);
	run;

	data temp;
		set temp;
		time_day_7 = 0;
		time_day_6 = 0;
		time_day_1 = 0;
		time_day_2 = 0;
		time_month_7 = 0;
		time_month_4 = 0;
		time_month_12 = 0;
		time_month_5 = 0;
		time_festivo = 0;
		time_prefestivo = 0;

		if day(data_gas_rif) = 1 and month(data_gas_rif) = 1 then
			time_festivo = 1;

		if day(data_gas_rif) = 6 and month(data_gas_rif) = 1 then
			time_festivo = 1;

		if day(data_gas_rif) = 25 and month(data_gas_rif) = 4 then
			time_festivo = 1;

		if day(data_gas_rif) = 1 and month(data_gas_rif) = 5 then
			time_festivo = 1;

		if day(data_gas_rif) = 2 and month(data_gas_rif) = 6 then
			time_festivo = 1;

		if day(data_gas_rif) = 15 and month(data_gas_rif) = 8 then
			time_festivo = 1;

		if day(data_gas_rif) = 1 and month(data_gas_rif) = 11 then
			time_festivo = 1;

		if day(data_gas_rif) = 8 and month(data_gas_rif) = 12 then
			time_festivo = 1;

		if data_gas_rif = holiday('EASTER',year(data_gas_rif)) then
			time_festivo = 1;

		if data_gas_rif = intnx('day',holiday('EASTER',year(data_gas_rif)),1) then
			time_festivo = 1;

		if data_gas_rif = intnx('day',holiday('EASTER',year(data_gas_rif)),2) then
			time_festivo = 1;

		if day(data_gas_rif) = 25 and month(data_gas_rif) = 12 then
			time_festivo = 1;

		if day(data_gas_rif) = 26 and month(data_gas_rif) = 12 then
			time_festivo = 1;

		if day(data_gas_rif) = 31 and month(data_gas_rif) = 12 then
			time_festivo = 1;

		if day(data_gas_rif) = 5 and month(data_gas_rif) = 1 then
			time_prefestivo = 1;

		if day(data_gas_rif) = 24 and month(data_gas_rif) = 4 then
			time_prefestivo = 1;

		*if data_gas_rif = intnx('day',holiday('EASTER',year(data_gas_rif)),-1) then time_prefestivo = 1;
		if day(data_gas_rif) = 30 and month(data_gas_rif) = 4 then
			time_prefestivo = 1;

		if day(data_gas_rif) = 1 and month(data_gas_rif) = 6 then
			time_prefestivo = 1;

		if day(data_gas_rif) = 14 and month(data_gas_rif) = 8 then
			time_prefestivo = 1;

		if day(data_gas_rif) = 31 and month(data_gas_rif) = 10 then
			time_prefestivo = 1;

		if day(data_gas_rif) = 7 and month(data_gas_rif) = 12 then
			time_prefestivo = 1;

		if day(data_gas_rif) = 24 and month(data_gas_rif) = 12 then
			time_prefestivo = 1;

		if day(data_gas_rif) = 30 and month(data_gas_rif) = 12 then
			time_prefestivo = 1;
		time_week_52 = 0;
		time_week_32 = 0;
		time_week_51 = 0;
		time_week_33 = 0;

		if time_day = 7 then
			time_day_7 = 1;

		if time_day = 6 then
			time_day_6 = 1;

		if time_day = 1 then
			time_day_1 = 1;

		if time_day = 2 then
			time_day_2 = 1;

		if time_month = 7 then
			time_month_7 = 1;

		if time_month = 4 then
			time_month_4 = 1;

		if time_month = 12 then
			time_month_12 = 1;

		if time_month = 5 then
			time_month_5 = 1;

		if time_week = 52 then
			time_week_52 = 1;

		if time_week = 32 then
			time_week_32 = 1;

		if time_week = 51 then
			time_week_51 = 1;

		if time_week = 33 then
			time_week_33 = 1;
		drop time_week time_month;
	run;

	data temp_ftd;
		set temp;
		fake_time_day=time_day;
		time_natale=0;
		time_ferragosto=0;

		if time_festivo=1 and time_day=2 then
			fake_time_day=5;

		if time_festivo=1 and time_day=3 then
			fake_time_day=5;

		if time_festivo=1 and time_day=4 then
			fake_time_day=7;

		if time_festivo=1 and time_day=5 then
			fake_time_day=7;

		if time_festivo=1 and time_day=6 then
			fake_time_day=7;

		if data_gas_rif = intnx('day',holiday('EASTER',year(data_gas_rif)),0) then
			fake_time_day = 1;

		if data_gas_rif = intnx('day',holiday('EASTER',year(data_gas_rif)),1) then
			fake_time_day = 5;

		/*natale*/
		/*da sabato prima di natale a domenica dopo befana*/
		if month(data_gas_rif) = 12 and data_gas_rif >= intnx("week", mdy(12, 25, year(data_gas_rif)), -1, 'E') 
			and weekday(mdy(12, 25, year(data_gas_rif))) < 7 then
			do;
				time_natale=1;
			end;
		else if month(data_gas_rif) = 12 and data_gas_rif >= intnx("day", mdy(12, 25, year(data_gas_rif)), -1) 
			and weekday(mdy(12, 25, year(data_gas_rif))) = 7 then
			do;
				time_natale=1;
			end;

		if month(data_gas_rif) = 1 and data_gas_rif<=intnx("week", mdy(1, 6, year(data_gas_rif)), 0, 'E') 
			and weekday(mdy(1, 6, year(data_gas_rif))) > 1 then
			do;
				time_natale=1;
			end;
		else if month(data_gas_rif) = 1 and data_gas_rif <= mdy(1, 6, year(data_gas_rif)) 
			and weekday(mdy(1, 6, year(data_gas_rif))) = 1 then
			do;
				time_natale=1;
			end;

		/*ferragosto*/
		/*tutta la settimana di ferragosto da sabato prima a domenica*/
		if data_gas_rif >= intnx("day", mdy(8,15, year(data_gas_rif)), -2) 
			and data_gas_rif <= intnx("day", mdy(8,15, year(data_gas_rif)), 2) then
			do;
				time_ferragosto=1;
			end;
	run;

	data temp_new_vars;
		set temp_ftd;
		time_weekend = 0;
		time_weekend_ext = 0;
		time_day_7_ext = 0;
		time_day_7_ext_holy = 0;
		time_festivo_ext = 0;
		fake_time_day_group1 = 0;
		fake_time_day_group2 = 0;
		time_risc_zona_A = 0; /* 3 comuni --> approssimo a 0.5 province */
		time_risc_zona_B = 0; /* 8 province */
		time_risc_zona_C = 0; /* 16 province */
		time_risc_zona_D = 0; /* 30 province */
		time_risc_zona_E = 0; /* 45 province */
		time_risc_zona_F = 1; /* 3 province - No limitazioni */
		time_day_group1 = 0;
		time_day_group2 = 0;

		if time_day_7 = 1 or time_festivo = 1 then
			do;
				time_day_7_ext = 1; /* Sabato esteso ai festivi */
				time_day_7_ext_holy = 1; /* Sabato esteso ai festivi e ai periodi di vacanza */
			end;

		if time_festivo = 1 then
			time_festivo_ext = 1;

		if time_ferragosto = 1 or time_natale = 1 then
			do;
				time_day_7_ext_holy = 1;
				time_festivo_ext = 1;
			end;

		if time_day in (1, 7) then
			time_weekend = 1;

		if time_day in (1, 7) or time_festivo = 1 then
			time_weekend_ext = 1;

		if (month(data_gas_rif) in (12, 1, 2)) or (month(data_gas_rif) = 3 and day(data_gas_rif) <= 15) then
			time_risc_zona_A = 1;

		if (month(data_gas_rif) in (12, 1, 2, 3)) then
			time_risc_zona_B = 1;

		if (month(data_gas_rif) in (12, 1, 2, 3)) or (month(data_gas_rif) = 11 and day(data_gas_rif) >= 15) then
			time_risc_zona_C = 1;

		if (month(data_gas_rif) in (11, 12, 1, 2, 3)) or (month(data_gas_rif) = 4 and day(data_gas_rif) <= 15) then
			time_risc_zona_D = 1;

		if (month(data_gas_rif) in (11, 12, 1, 2, 3)) or (month(data_gas_rif) = 4 and day(data_gas_rif) <= 15)
			or (month(data_gas_rif) = 10 and day(data_gas_rif) >= 15) then
			time_risc_zona_E = 1;

		/* Riscaldamento complessivo */
		time_risc_totale = time_risc_zona_A*(0.5/102.5) + time_risc_zona_B*(8/102.5) + time_risc_zona_C*(16/102.5) + 
			time_risc_zona_D*(30/102.5) + time_risc_zona_E*(45/102.5) + time_risc_zona_F*(3/102.5);

		if fake_time_day in (2, 3, 4, 5, 6) then
			fake_time_day_group1 = 1;
		else if fake_time_day in (1,  7) then
			fake_time_day_group2 = 1;

		if time_day in (2, 3, 4, 5, 6) and time_festivo = 0 then
			time_day_group1 = 1;

		if time_day in (1, 7) or time_festivo = 1 then
			time_day_group2= 1;
		drop time_day fake_time_day;
	run;

	data &dset_with_time;
		set temp_new_vars;
		data_gas_rif_364 = intnx("day", data_gas_rif, -364);
		data_gas_rif_366 = intnx("day", data_gas_rif, -366);
		time_festivo_lag = 0;

		if day(data_gas_rif_364) = 1 and month(data_gas_rif_364) = 1 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 1 and month(data_gas_rif_366) = 1 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 6 and month(data_gas_rif_364) = 1 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 6 and month(data_gas_rif_366) = 1 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 25 and month(data_gas_rif_364) = 4 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 25 and month(data_gas_rif_366) = 4 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 1 and month(data_gas_rif_364) = 5 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 1 and month(data_gas_rif_366) = 5 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 2 and month(data_gas_rif_364) = 6 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 2 and month(data_gas_rif_366) = 6 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 15 and month(data_gas_rif_364) = 8 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 15 and month(data_gas_rif_366) = 8 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 1 and month(data_gas_rif_364) = 11 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 1 and month(data_gas_rif_366) = 11 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 8 and month(data_gas_rif_364) = 12 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 8 and month(data_gas_rif_366) = 12 then
			time_festivo_lag = 1;

		if data_gas_rif_364 = holiday('EASTER',year(data_gas_rif_364)) then
			time_festivo_lag = 1;
		else if data_gas_rif_366 = holiday('EASTER',year(data_gas_rif_366)) then
			time_festivo_lag = 1;

		if data_gas_rif_364 = intnx('day',holiday('EASTER',year(data_gas_rif_364)),1) then
			time_festivo_lag = 1;
		else if data_gas_rif_366 = intnx('day',holiday('EASTER',year(data_gas_rif_366)),1) then
			time_festivo_lag = 1;

		if data_gas_rif_364 = intnx('day',holiday('EASTER',year(data_gas_rif_364)),2) then
			time_festivo_lag = 1;
		else if data_gas_rif_366 = intnx('day',holiday('EASTER',year(data_gas_rif_366)),2) then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 25 and month(data_gas_rif_364) = 12 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 25 and month(data_gas_rif_366) = 12 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 26 and month(data_gas_rif_364) = 12 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 26 and month(data_gas_rif_366) = 12 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 31 and month(data_gas_rif_364) = 12 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 31 and month(data_gas_rif_366) = 12 then
			time_festivo_lag = 1;
		format data: date9.;
	run;

	data &dset_with_time;
		set &dset_with_time(drop=fake_time_day_group1 fake_time_day_group2 data_gas_rif_364 data_gas_rif_366);
		hour_0=0;
		hour_1=0;
		hour_2=0;
		hour_3=0;
		hour_4=0;
		hour_5=0;
		hour_6=0;
		hour_7=0;
		hour_8=0;
		hour_9=0;
		hour_10=0;
		hour_11=0;
		hour_12=0;
		hour_13=0;
		hour_14=0;
		hour_15=0;
		hour_16=0;
		hour_17=0;
		hour_18=0;
		hour_19=0;
		hour_20=0;
		hour_21=0;
		hour_22=0;
		hour_23=0;

		if hour=0 then
			hour_0=1;

		if hour=1 then
			hour_1=1;

		if hour=2 then
			hour_2=1;

		if hour=3 then
			hour_3=1;

		if hour=4 then
			hour_4=1;

		if hour=5 then
			hour_5=1;

		if hour=6 then
			hour_6=1;

		if hour=7 then
			hour_7=1;

		if hour=8 then
			hour_8=1;

		if hour=9 then
			hour_9=1;

		if hour=10 then
			hour_10=1;

		if hour=11 then
			hour_11=1;

		if hour=12 then
			hour_12=1;

		if hour=13 then
			hour_13=1;

		if hour=14 then
			hour_14=1;

		if hour=15 then
			hour_15=1;

		if hour=16 then
			hour_16=1;

		if hour=17 then
			hour_17=1;

		if hour=18 then
			hour_18=1;

		if hour=19 then
			hour_19=1;

		if hour=20 then
			hour_20=1;

		if hour=21 then
			hour_21=1;

		if hour=22 then
			hour_22=1;

		if hour=23 then
			hour_23=1;
	run;

%mend create_calendar_vars;

%macro get_scada_sim_vars(shift=, data=, frequency_matrix=);

	data frequency_statement;
		length var_list_mvg $ 500;
		set &frequency_matrix end=eof;
		season_str = compress(put(season, $8.));
		array scada_v[6] $ 14 ("scada_bil_ric", "scada_bil_exp", "scada_bil_imp", "scada_bil_stc", "scada_bil_pnl", "scada_bil_dlp");
		array scada_bil[*] scada_bil:;

		/*	array scada_expr{&dim_} $ 100;*/
		if season = 12 then
			season_pfx = 'hd';

		if season = 24 then
			season_pfx = 'dd';

		if season in (120, 48) then
			season_pfx = 'ww';

		if season in (3144, 1248) then
			season_pfx = 'hy';

		if season in (6240, 2496) then
			season_pfx = 'yy';

		do i = 1 to dim(scada_v);
			do j = 1 to 3;
				if scada_bil[i] ne 0 then
					do;
						if season in (12, 3144, 1248) then
							season_use = season*2*j - season;
						else season_use = season*j - &shift;
						season_use_str = compress(put(season_use, $8.));
						step = compress(put(j, $8.));
						expr = cat('convert ', compress(scada_v[i]), '=',  compress(tranwrd(scada_v[i], "bil", "bil_lag")), '_', 
							season_pfx, '_', step, ' / transform=(lag ', compress(season_use_str), ');');
						scada_var = compress((scada_v[i]));
						group = mod(j,3);

						if group = 1 then
							do;
								var_list_mvg = cat(compress(tranwrd(scada_v[i], "bil", "bil_lag")), '_', season_pfx, '_', step);
							end;
						else if group ne 1 then
							do;
								new_var_mvg = cat(compress(tranwrd(scada_v[i], "bil", "bil_lag")), '_', season_pfx, '_', step);
								var_list_mvg = catx(',', var_list_mvg, new_var_mvg);
							end;

						output;
					end;
			end;
		end;

		drop i j;
	run;

	data frequency_statement_expr;
		length lag_varlist $10000 mvg_avg_varlist $10000;
		set frequency_statement end=eof;
		retain lag_varlist mvg_avg_varlist;

		if _N_ = 1 then
			do;
				lag_varlist = expr;

				if group ne 1 then
					do;
						mvg_avg_varlist = cat(compress(tranwrd(scada_var, "bil", "bil_mvg")), '_', season_pfx, '_', step, '=',
							'mean(', compress(var_list_mvg), ')');
					end;
				else mvg_avg_varlist = '';
				output;
			end;
		else
			do;
				new_var = expr;

				if group ne 1 then
					do;
						new_mvg_avg = cat(compress(tranwrd(scada_var, "bil", "bil_mvg")), '_', season_pfx, '_', step, '=',
							'mean(', compress(var_list_mvg), ')');
					end;
				else new_mvg_avg = '';
				lag_varlist = catx(' ', lag_varlist, new_var);
				mvg_avg_varlist = catx(';', mvg_avg_varlist, new_mvg_avg);
				output;
			end;

		if eof then
			do;
				call symputx("frequency_statement", lag_varlist);
				call symputx("mvg_avg_statement", mvg_avg_varlist);
			end;
	run;

	options symbolgen mlogic;

	proc expand data=&data
		out=&data._new;
		&frequency_statement;
	run;

	data mvgavg_&data._new;
		set &data._new;
		&mvg_avg_statement;
	run;

%mend get_scada_sim_vars;

%macro create_scada_sim_vars(scada_with_time,start_date_sim, output_scada);

	data time1_scada time2_scada;
		set &scada_with_time;

		if time_day_group1 = 1 then
			output time1_scada;
		else output time2_scada;
		keep data_gas_rif hour_prev datetime_prev scada: time: hour_: giorno_gas_scada;
	run;

	/* Declaring frequency matrix where frequencies are already adjusted for working days and holidays*/
	data frequency_time1;
		length season 8 scada_bil_ric 8 scada_bil_exp 8 scada_bil_imp 8 scada_bil_stc 8 scada_bil_pnl 8 scada_bil_dlp 8;
		call symputx("dim_", 6);
		season=12;
		scada_bil_ric=1;
		scada_bil_exp=1;
		scada_bil_imp=0;
		scada_bil_stc=0;
		scada_bil_pnl=0;
		scada_bil_dlp=1;
		output;
		season=24;
		scada_bil_ric=1;
		scada_bil_exp=1;
		scada_bil_imp=0;
		scada_bil_stc=0;
		scada_bil_pnl=0;
		scada_bil_dlp=1;
		output;
		season=120;
		scada_bil_ric=1;
		scada_bil_exp=0;
		scada_bil_imp=1;
		scada_bil_stc=1;
		scada_bil_pnl=0;
		scada_bil_dlp=0;
		output;
		season=3144;
		scada_bil_ric=1;
		scada_bil_exp=0;
		scada_bil_imp=1;
		scada_bil_stc=1;
		scada_bil_pnl=1;
		scada_bil_dlp=0;
		output;
		season=6240;
		scada_bil_ric=1;
		scada_bil_exp=1;
		scada_bil_imp=1;
		scada_bil_stc=1;
		scada_bil_pnl=1;
		scada_bil_dlp=0;
		output;
	run;

	data frequency_time2;
		length season 8 scada_bil_ric 8 scada_bil_exp 8 scada_bil_imp 8 scada_bil_stc 8 scada_bil_pnl 8 scada_bil_dlp 8;
		call symputx("dim_", 6);
		season=12;
		scada_bil_ric=1;
		scada_bil_exp=1;
		scada_bil_imp=0;
		scada_bil_stc=0;
		scada_bil_pnl=0;
		scada_bil_dlp=1;
		output;
		season=24;
		scada_bil_ric=1;
		scada_bil_exp=1;
		scada_bil_imp=0;
		scada_bil_stc=0;
		scada_bil_pnl=0;
		scada_bil_dlp=1;
		output;
		season=48;
		scada_bil_ric=1;
		scada_bil_exp=0;
		scada_bil_imp=1;
		scada_bil_stc=1;
		scada_bil_pnl=0;
		scada_bil_dlp=0;
		output;
		season=1248;
		scada_bil_ric=1;
		scada_bil_exp=0;
		scada_bil_imp=1;
		scada_bil_stc=1;
		scada_bil_pnl=1;
		scada_bil_dlp=0;
		output;
		season=2496;
		scada_bil_ric=1;
		scada_bil_exp=1;
		scada_bil_imp=1;
		scada_bil_stc=1;
		scada_bil_pnl=1;
		scada_bil_dlp=0;
		output;
	run;

	/* Get the new sim vars starting from original SCADA components */
	%get_scada_sim_vars(shift=1, data=time1_scada, frequency_matrix=frequency_time1);
	%get_scada_sim_vars(shift=1, data=time2_scada, frequency_matrix=frequency_time2);

	data scada_all;
		length datetime_prev data_gas_rif 8;
		set mvgavg_time1_scada_new mvgavg_time2_scada_new;
	run;

	/* Save Results */
	proc sort data=scada_all out=scada_all;
		by datetime_prev;
		where data_gas_rif >= &start_date_sim;
	run;

	data &output_scada(drop=time);
		length datetime_prev data_gas_rif 8;
		set scada_all;
		scada_bil_ric_lag1h=lag(scada_bil_ric);
		scada_bil_ric_lag24h=lag23(scada_bil_ric);
		scada_bil_ric_lag7d=lag167(scada_bil_ric);
		scada_bil_ric_smooth=mean(scada_bil_ric,scada_bil_ric_lag1h);
		scada_bil_ric_cumday_lag1h=lag(scada_bil_ric_cumday);
		scada_bil_ric_cumday_lag24h=lag23(scada_bil_ric_cumday);
		scada_bil_ric_cumday_lag7d=lag167(scada_bil_ric_cumday);
	run;

%mend create_scada_sim_vars;

%macro gxg_create_target_rtot(input_bilanciosnam, input_dlp_orario, input_pcs_misura, input_riconsegnato1,
			input_riconsegnato2, output_scada, output_rtot, g_gas, start_date);
	/*%let input_bilanciosnam=&input_bilanciosnam;*/
	/*%let input_dlp_orario=oralpbp2.analisi_dlp_orario;*/
	/*%let input_pcs_misura=webdips.pcs_misura_aop9100;*/
	/*%let input_riconsegnato1=oralpbp2.riconsegnato;*/
	/*%let input_riconsegnato2=oralpbp2.consuntivo_view;*/
	/*%let output_scada=output_scada;*/
	/*%let output_target=output_target;*/
	/*%let g_gas=&g_gas;*/
	/*%let start_date=&start_date;*/
	data _null_;
		start_date_adj=intnx("month",&start_date,-37, 'S');
		start_dt_char=put(dhms(start_date_adj,6,0,0), datetime19.);
		call symput('start_dt_char',start_dt_char);
		call symput('start_date_adj',start_date_adj);
	run;

	data calendario_giorno;
		data_gas=&start_date_adj;

		do while (data_gas<=&G_gas);
			output;
			data_gas=intnx("day", data_gas, 1, 's');
		end;

		format data_gas date9.;
	run;

	/******************************/
	/* RECUPERO DELTA LINE PACK*/

	/* 1. Recupero DLP (voce delta_line_pack_srg) da bilancio ufficiale commerciale (webdips.prev_bilancio) 
	quando ? validato (validato=1) */
	data dlp_1 (keep=data_gas Delta_Line_Pack_SRG);
		set &input_bilanciosnam(rename=(date=data_gas) 
			where=(validato=1 and Delta_Line_Pack_SRG ne . and data_gas>=&start_date_adj));
		Delta_Line_Pack_SRG=Delta_Line_Pack_SRG*1000000;
	run;

	proc sort data=dlp_1;
		by data_gas;
	run;

	/* 2. Recupero DLP da oralpbp2.analisi_dlp_orario e lo converto con PCS di misura tramite la 
	tabella webdips.pcs_misura_aop9100 */
	data dlp_orariowhere(where=(data_gas>="01JAN2011"d and data_gas<&G_gas));
		set &input_dlp_orario(where=(dt_calcolo>="&start_dt_char"dt));
		data_gas=ifn(hour(dt_calcolo)<6, intnx("day",datepart(dt_calcolo),-1), datepart(dt_calcolo));
		format data_gas date9.;
	run;

	proc sql;
		create table dlp_2_smc as 
			select data_gas
				, sum(delta_linepack) as delta_linepack_ab
			from dlp_orariowhere
				group by data_gas
					order by data_gas
		;
	quit;

	/* Lo converto con PCS di misura */
	data pcs_misura (keep=data_gas pcs_kwh_sm3);
		set &input_pcs_misura (where=(pcs_kwh_sm3 ne .));
		data_gas=datepart(data);
		format data_gas date9.;
	run;

	proc sort data=pcs_misura nodupkey;
		by data_gas;
	run;

	data dlp_2(keep=data_gas dlp_2);
		merge dlp_2_smc(in=p) pcs_misura(in=q);
		by data_gas;
		dlp_2=delta_linepack_ab* pcs_kwh_sm3;

		if p and q;
	run;

	/* NOTA: Potrei prendere DLP anche da oralpbp2.analisi_bilancio ma sembra essere molto meno preciso
	(differenza alta con il dlp del bilancio commerciale) */

	/* MERGE con il calendario giorno */
	data dlp_final(keep=data_gas deltalinepack_final );
		merge calendario_giorno(in=p where=(data_gas>="01JAN2011"d)) dlp_1 dlp_2;
		by data_gas;

		if (delta_line_pack_srg ne .) then
			deltalinepack_final=delta_line_pack_srg;
		else if (dlp_2 ne .) then
			deltalinepack_final=dlp_2;
	run;

	/******************************/
	/* Calcolo il Riconsegnato_Consuntivo_KWH utilizzando il delta line pack recuperato da JAN2011 */
	%let to_keep = date entrata: gnl: uscita: 
		Importazioni Produzione_Nazionale Sistemi_di_stoccaggio Stogit Edison_Stoccaggio
		Totale_Immesso Riconsegne_rete_Snam_Rete_Gas Industriale Termoelettrico Reti_di_distribuzione 
		Riconsegnato_ad_altre_Reti_tr;

	data riconsegnato;
		set &input_bilanciosnam(keep = &to_keep 
			where=(date ge "01JAN2011"d and date >= &start_date_adj));
		rename date=data_gas 
			Riconsegnato_ad_altre_Reti_tr=Ric_Reti_terzi 
			Riconsegne_rete_Snam_Rete_Gas=Riconsegne_SRG 
			Sistemi_di_stoccaggio=stoccaggio
			Reti_di_distribuzione=reti_distrib;
		uscite=sum(of uscita:);
		target_con_dlp=sum(Totale_Immesso,-uscite);
	run;

	data riconsegnato;
		merge riconsegnato(in=p) dlp_final(in=q);
		by data_gas;

		if p and q;
		target_con_dlp=target_con_dlp*1000000;
		Riconsegnato_Consuntivo_KWH = target_con_dlp-deltalinepack_final;
	run;

	/******************************/
	/* Calcolo il Riconsegnato SCADA (per imputare gli ultimi valori missing del riconsegnato consuntivo KWH) */
	data riconsegnato_1 (rename=(tot_smc= tot_smc_1));
		set &input_riconsegnato1(keep=data_rif tot_smc);
		data_gas=datepart(data_rif);
	run;

	proc sort data=riconsegnato_1 nodupkey;
		by data_gas;
	run;

	data riconsegnato_2 (rename=(tot_smc= tot_smc_2));
		set &input_riconsegnato2(keep= data_rif tot_smc pcs_kwh);
		data_gas=datepart(data_rif);
	run;

	proc sort data=riconsegnato_2 nodupkey;
		by data_gas;
	run;

	/* Utilizzando i dati scada orari calcolati in GxG_create_input_scada ? possibile ricavare il Riconsegnato Totale Giornaliero*/
	proc sql;
		create table ric_calcolo_giornaliero as 
			select giorno_gas_scada as data_gas
				, sum(scada_bil_ric) as scada_ric
			from &output_scada (where = (giorno_gas_scada<&G_gas and giorno_gas_scada>="01JAN2012"d) )
				group by giorno_gas_scada
		;
	quit;

	/* Prendo i dati fino a oggi del riconsegnato SCADA giornaliero */
	data riconsegnato_scada (where= (data_gas >= "01JAN2012"d and data_gas < &G_gas));
		merge riconsegnato_1 riconsegnato_2 ric_calcolo_giornaliero;
		by data_gas;
		format  data_gas date9.;

		if scada_ric ne . then
			riconsegnato_scada=scada_ric;
		else if tot_smc_2 ne . then
			riconsegnato_scada=tot_smc_2;
		else if tot_smc_1 ne . then
			riconsegnato_scada=tot_smc_1;

		if (data_gas eq "04MAR2011"d and pcs_kwh ne .) then
			pcs_kwh=10.85;
		riconsegnato_scada_kwh=riconsegnato_scada*pcs_kwh;
	run;

	/******************************/
	/* Output FINALE */
	data &output_rtot(keep=deltalinepack_final data_gas riconsegnato_consuntivo_kwh riconsegnato_scada_kwh);
		merge calendario_giorno(in=p) riconsegnato riconsegnato_scada;
		by data_gas;

		if p;

		if(riconsegnato_consuntivo_kwh eq .) then
			riconsegnato_consuntivo_kwh =riconsegnato_scada_kwh;
	run;

	/* FFill del dlp */
	data &output_rtot(drop=deltalinepack_final rename=(deltalinepack_final_new=deltalinepack_final));
		set &output_rtot;
		retain deltalinepack_final_new;

		if _n_=1 then
			deltalinepack_final_new=deltalinepack_final;
		else if deltalinepack_final ne . then
			deltalinepack_final_new=deltalinepack_final;
	run;

%mend gxg_create_target_rtot;

/* Creazione del Riconsegnato Termoelettrico Terna */
%macro gxg_create_target_termo(start_date, input_termo, input_sime2,
			input_sime, input_vol_val, output_rcomp_termo);

	data t_remi_terna (drop=remi);
		set &input_termo;
		codice_remi = input(remi,best32.);
	run;

	proc sql noprint;
		select codice_remi into: remi_terna
			separated by " "
		from t_remi_terna
		;
	quit;

	data filter;
		if day(datepart(&dt_solare))<=20 then
			filter=intnx("month", datepart(&dt_solare),-2, "end");
		else filter=intnx("month", datepart(&dt_solare),-1, "end");
		call symput("remi_limit", filter);
		format filter date9.;
	run;

	/* Prendo i volumi dal 1 maggio 2016 - sime2 ha validati da questa data*/
	data t_volumi_sime2(drop=k_d_e_misure_sime g_gas codice_punto);
		set &input_sime2(where=(k_d_e_misure_sime>0 ) 
			keep=k_d_e_misure_sime giorno_gas codice_punto pcs_25_15 volume_std);
		g_gas = datepart(giorno_gas);

		if g_gas ge &start_date and g_gas le &remi_limit;
		volume_kwh=volume_std*pcs_25_15;
		codice_remi=input(codice_punto,8.);
	run;

	proc means nolabels noprint data=t_volumi_sime2;
		var giorno_gas;
		output out=query_minmax_sime min=mindata max=maxdata;
	run;

	data _null_;
		set query_minmax_sime;
		call symput('min_data_SIME2', mindata);
		a=put (mindata, datetime.);
		call symput('max_data_SIME2', maxdata);
		b=put(maxdata, datetime.);
	run;

	/* Prendo solo i terna */
	proc sort data=t_volumi_sime2 ( rename=(giorno_gas = data_riferimento) 
		where=(codice_remi in (&remi_terna)));
		by data_riferimento;
	run;

	data _null_;
		if &start_date>="01MAY2016"d then
			read_s1=0;
		else read_s1=1;
		call symputx('read_s1',read_s1);
	run;

	%if &read_s1 eq 1 %then
		%do;
			/* Prendo volumi_sime validato pre maggio 2016 */
			data remi_sime_validati_parziali (drop=g_gas codice_punto
				where=(giorno_gas lt &min_data_sime2 and codice_remi ne . ));
				set &input_sime(keep=giorno_gas codice_punto pcs_25_15 volume_std);
				g_gas = datepart(giorno_gas);

				if g_gas ge &start_date;
				volume_kwh=volume_std*pcs_25_15;
				codice_remi=input(codice_punto,8.);
			run;

			/* Filtro per i remi termo di terna */
			data remi_sime_validati_parziali (rename=(giorno_gas = data_riferimento));
				set remi_sime_validati_parziali(where=(codice_remi in (&remi_terna)));
			run;

			/* Accodo i due storici (pre e post 2016) */
			data t_remi_termo_target (drop=data_riferimento volume_kwh
				rename=(pcs_25_15=pcs_val volume_std=volume_smc));
				set remi_sime_validati_parziali t_volumi_sime2;
				data_gas=datepart(data_riferimento);
				format data_gas date9.;
				volume_kwh_val=volume_std*pcs_25_15;
			run;

		%end;
	%else
		%do;

			data t_remi_termo_target (drop=data_riferimento volume_kwh
				rename=(pcs_25_15=pcs_val volume_std=volume_smc));
				set t_volumi_sime2;
				data_gas=datepart(data_riferimento);
				format data_gas date9.;
				volume_kwh_val=volume_std*pcs_25_15;
			run;

		%end;

	proc sort data=t_remi_termo_target;
		by codice_remi data_gas;
	run;

	/* Calcolo i pcs_medi (dal 2013 al 2018) per codice_remi */
	proc sql;
		create table pcs_medi as 
			select distinct codice_remi
				,mean(pcs_val) as pcs_mean
			from t_remi_termo_target (where =(data_gas ge '01OCT2013'd and 
				data_gas lt '01OCT2018'd))
			group by codice_remi
		;
	quit;

	/**********/
	data _null_;
		if &start_date>="01MAY2013"d then
			read_vv=0;
		else read_vv=1;
		call symputx('read_vv',read_vv);
	run;

	%if &read_vv eq 1 %then
		%do;
			/* Ricostruisco lo storico (prendo fino al 01MAY2013) */
			data volumi_validati (drop=dt_data_riferimento_dati id_codice_remi
				rename=(nr_volume=smc_vv ));
				set &input_vol_val (where = (input(id_codice_remi,8.) in (&remi_terna))
					keep = dt_data_riferimento_dati id_codice_remi nr_volume);
				data_gas = datepart(dt_data_riferimento_dati);

				if data_gas ge &start_date and data_gas lt '01MAY2013'd;
				format data_gas date9.;
				codice_remi=input(id_codice_remi,8.);
			run;

			proc sql;
				create table storico_remi as 
					select    a.data_gas
						, a.codice_remi
						, b.pcs_mean as pcs_val
						, a.smc_vv * b.pcs_mean as volume_kwh_val
					from volumi_validati as a
						left join pcs_medi as b
							on a.codice_remi = b.codice_remi
						order by a.data_gas
				;
			quit;

			data t_remi_termo_target_all (keep=data_gas codice_remi volume_kwh_val);
				set storico_remi t_remi_termo_target;

				if pcs_val ne .;
			run;

		%end;
	%else
		%do;

			data t_remi_termo_target_all (keep=data_gas codice_remi volume_kwh_val);
				set t_remi_termo_target;

				if pcs_val ne .;
			run;

		%end;

	proc sql;
		create table &output_rcomp_termo as 
			select data_gas
				, sum(volume_kwh_val) as riconsegnato_termo_kwh 
			from t_remi_termo_target_all 
				group by data_gas
					order by data_gas;
	quit;

%mend gxg_create_target_termo;

%macro gxg_create_lagsimile(output_rtot, input_calendario, output_lagsim);

	proc sort data=&output_rtot(drop=deltalinepack_final) out=temp;
		by data_gas;
	run;

	data temp;
		set temp;
		time_year = year(data_gas);

		if month(data_gas)>=10 then
			time_anno_termico=year(data_gas);
		else time_anno_termico=year(data_gas)-1;
		time_month = month(data_gas);
		time_weekday = weekday(data_gas);
		time_week = week(data_gas);
		time_festivo = 0;
		time_prefestivo = 0;

		if day(data_gas) = 1 and month(data_gas) = 1 then
			time_festivo = 1;

		if day(data_gas) = 6 and month(data_gas) = 1 then
			time_festivo = 1;

		if day(data_gas) = 25 and month(data_gas) = 4 then
			time_festivo = 1;

		if day(data_gas) = 1 and month(data_gas) = 5 then
			time_festivo = 1;

		if day(data_gas) = 2 and month(data_gas) = 6 then
			time_festivo = 1;

		if day(data_gas) = 15 and month(data_gas) = 8 then
			time_festivo = 1;

		if day(data_gas) = 1 and month(data_gas) = 11 then
			time_festivo = 1;

		if day(data_gas) = 8 and month(data_gas) = 12 then
			time_festivo = 1;

		if data_gas = holiday('EASTER',year(data_gas)) then
			time_festivo = 1;

		if data_gas = intnx('day',holiday('EASTER',year(data_gas)),1) then
			time_festivo = 1;

		if day(data_gas) = 25 and month(data_gas) = 12 then
			time_festivo = 1;

		if day(data_gas) = 26 and month(data_gas) = 12 then
			time_festivo = 1;

		if day(data_gas) = 31 and month(data_gas) = 12 then
			time_festivo = 1;

		if lag1(time_festivo)=1 and time_festivo=0 then
			time_prefestivo=1;

		if data_gas = holiday('EASTER',year(data_gas)) or data_gas = intnx('day',holiday('EASTER',year(data_gas)),1) then
			time_festivo = 0;

		if data_gas = holiday('EASTER',year(data_gas)) or data_gas = intnx('day',holiday('EASTER',year(data_gas)),1) then
			time_easter = 1;
		else time_easter=0;
		time_natale=0;
		time_ferragosto=0;

		/*da sabato prima di natale a domenica dopo befana*/
		if month(data_gas) = 12 and data_gas >= intnx("week", mdy(12, 25, year(data_gas)), -1, 'E') 
			and weekday(mdy(12, 25, year(data_gas))) < 7 then
			do;
				time_natale=1;
			end;
		else if month(data_gas) = 12 and data_gas >= intnx("day", mdy(12, 25, year(data_gas)), -1) 
			and weekday(mdy(12, 25, year(data_gas))) = 7 then
			do;
				time_natale=1;
			end;

		if month(data_gas) = 1 and data_gas<=intnx("week", mdy(1, 6, year(data_gas)), 0, 'E') 
			and weekday(mdy(1, 6, year(data_gas))) > 1 then
			do;
				time_natale=1;
			end;
		else if month(data_gas) = 1 and data_gas <= mdy(1, 6, year(data_gas)) 
			and weekday(mdy(1, 6, year(data_gas))) = 1 then
			do;
				time_natale=1;
			end;

		/*ferragosto*/
		/*tutta la settimana di ferragosto da sabato prima a domenica*/
		if data_gas >= intnx("day", mdy(8,15, year(data_gas)), -2) 
			and data_gas <= intnx("day", mdy(8,15, year(data_gas)), 2) then
			do;
				time_ferragosto=1;
			end;

		time_weekend = 0;
		time_festivo_ext = 0;

		if time_festivo = 1 or time_ferragosto = 1 or time_natale = 1 then
			time_festivo_ext = 1;

		if time_weekday in (1, 7) then
			time_weekend = 1;
		time_week2 = week(data_gas, 'V');
	run;

	proc sort data= temp;
		by descending data_gas;
	run;

	data temp;
		set temp;

		if lag1(time_festivo)=1 and time_festivo=0 then
			time_prefestivo=1;
		time_ponte=0;

		if (time_prefestivo=1 or time_ferragosto=1 or time_natale=1) and (time_weekend=0) and (time_festivo=0) then
			time_ponte=1;
	run;

	proc sort data= temp;
		by data_gas;
	run;

	data temp;
		set temp;
		time_lag_festivo1=0;
		time_lag_festivo3=0;
		time_lag_festivo7=0;
		time_lag_festivo364=0;
		time_lag_festivo_17=0;

		if lag1(time_festivo_ext)=1 and time_festivo_ext=0 then
			time_lag_festivo1=1;

		if lag3(time_festivo_ext)=1 and time_festivo_ext=0 then
			time_lag_festivo3=1;

		if lag7(time_festivo_ext)=1 and time_festivo_ext=0 then
			time_lag_festivo7=1;

		if lag364(time_festivo_ext)=1 and time_festivo_ext=0 then
			time_lag_festivo364=1;

		if time_lag_festivo1=1 and time_lag_festivo7=1 then
			time_lag_festivo_17=1;
	run;

	proc sort data=temp;
		by data_gas;
	run;

	data temp;
		set temp;

		/*	weekday3 a 6*/
		lag_rictot_1scada=lag1(riconsegnato_scada_kwh);
		lag_rictot_1=lag1(riconsegnato_consuntivo_kwh);

		/*	weekday2*/
		lag_rictot_3=lag3(riconsegnato_consuntivo_kwh);

		/* temp */
		lag_rictot_7=lag7(riconsegnato_consuntivo_kwh);
		lag_rictot_364=lag364(riconsegnato_consuntivo_kwh);
		lag_rictot_365=lag365(riconsegnato_consuntivo_kwh);

		/*	weekday1*/
		lag_rictot_1scada_7=mean(lag_rictot_1scada,lag_rictot_7);
		lag_rictot_1_7=mean(lag_rictot_1,lag_rictot_7);

		/*	Easter e Ferragosto noFestivi*/
		lag_rictot_1scada_364=mean(lag_rictot_1scada,lag_rictot_364);
		lag_rictot_1_364=mean(lag_rictot_1,lag_rictot_364);

		/*	weekday7 e Natale noFestivi*/
		lag_rictot_1scada_7_364=mean(lag_rictot_1scada,lag_rictot_7,lag_rictot_364);
		lag_rictot_1_7_364=mean(lag_rictot_1,lag_rictot_7,lag_rictot_364);

		/* Festivo */
		lag_rictot_1scada_365=mean(lag_rictot_1scada,lag_rictot_365);
		lag_rictot_1_365=mean(lag_rictot_1,lag_rictot_365);

		if time_weekday in (3,4,5,6) then
			do;
				lag_rictot_best=lag_rictot_1;
				lag_rictot_best_scada=lag_rictot_1scada;
			end;

		if time_weekday in (1) then
			do;
				lag_rictot_best=lag_rictot_1_7;
				lag_rictot_best_scada=lag_rictot_1scada_7;
			end;

		if time_weekday in (2) then
			do;
				lag_rictot_best=lag_rictot_3;
				lag_rictot_best_scada=lag_rictot_3;
			end;

		if time_weekday in (7) or time_natale = 1 then
			do;
				lag_rictot_best=lag_rictot_1_7_364;
				lag_rictot_best_scada=lag_rictot_1scada_7_364;
			end;

		if time_ferragosto=1 then
			do;
				lag_rictot_best=lag_rictot_1_364;
				lag_rictot_best_scada=lag_rictot_1scada_364;
			end;

		if time_festivo=1 then
			do;
				lag_rictot_best=lag_rictot_1_365;
				lag_rictot_best_scada=lag_rictot_1scada_365;
			end;

		if time_easter=1 then
			do;
				lag_rictot_best=lag_rictot_1_364;
				lag_rictot_best_scada=lag_rictot_1scada_364;
			end;

		if lag_rictot_1 eq . then
			lag_rictot_best = .;
	run;

	data temp_nomiss_scada;
		set temp(keep=data_gas lag_rictot_best_scada);
		datetime_solare=dhms(intnx("day",data_gas,0),6,0,0);
		format datetime_solare datetime19.;
	run;

	data temp_nomiss;
		set temp(keep=data_gas lag_rictot_best);
		datetime_solare=dhms(intnx("day",data_gas,0),13,0,0);
		format datetime_solare datetime19.;
	run;

	data temp_final;
		set temp_nomiss_scada(rename=(lag_rictot_best_scada=rictot_simile)) temp_nomiss(rename=(lag_rictot_best=rictot_simile));
	run;

	proc sort data=temp_final out=temp1(drop=data_gas);
		by datetime_solare;
	run;

	/*--*/
	proc sql noprint;
		%let droplist=;
		select compress(cat("lag_" , name))
			into :droplist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="TEMP1"
						and (name like "rictot_%");
	quit;

	proc sql noprint;
		%let savelist=;
		select name
			into :savelist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="TEMP1"
						and (name like "rictot_%");
	quit;

	data &output_lagsim (drop=a b i j &savelist);
		set	TEMP1;
		array new &droplist;
		array old &savelist;
		retain new;
		a=dim(new);
		b=dim(old);

		if _N_=1 then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;
	run;

	data &output_lagsim;
		set  &output_lagsim (rename=(lag_rictot_simile=rtot_lag_simile));
	run;

	/* Merge col calendario e Foward Fill */
	data &output_lagsim;
		merge &input_calendario (in=c) &output_lagsim;
		by datetime_solare;

		if c;
	run;

	data &output_lagsim;
		set &output_lagsim;
		retain ffill;

		if not missing(rtot_lag_simile) then
			ffill=rtot_lag_simile;
		else if missing(rtot_lag_simile) then
			rtot_lag_simile=ffill;
		drop ffill;
	run;

%mend gxg_create_lagsimile;

%macro gxg_create_rtot_lag_mvg(output_rtot, output_lagsim, start_date, output_rtot_lag_mvg);

	data target1 (drop=cross_section2
		rename=(cross_section1=cross_section))
		target2 (drop=cross_section1
		rename=(cross_section2=cross_section));
		set &output_rtot;
		keep data_gas riconsegnato_consuntivo_kwh cross_section1 cross_section2;
		rename data_gas=data_gas_merge 
			riconsegnato_consuntivo_kwh=rtot_lag;

		if riconsegnato_consuntivo_kwh eq . then
			riconsegnato_consuntivo_kwh=-1;
		cross_section1 = 'dummy1';
		cross_section2 = 'dummy2';
	run;

	data target_shift (where=(data_gas_merge ge "01JAN2012"d));
		set target1 target2;
	run;

	/**************/
	/* Lag da 2 a 60 del riconsegnato_consuntivo_kwh */
	data x;
		do x=2 to 60;
			output;
		end;
	run;

	proc sql noprint;
		select x into:lg separated by ' ' from x;
	quit;

	proc panel data=target_shift;
		id cross_section data_gas_merge;
		lag rtot_lag(&lg.)/out=consuntivo_shift;
	run;

	data consuntivo_shift1 (drop=cross_section rtot_lag);
		set consuntivo_shift (where=(cross_section = "dummy1"));

		if cmiss(of _all_) then
			delete;
	run;

	proc sort data=consuntivo_shift1;
		by data_gas_merge;
	run;

	/**************/
	/* Lag da 364 a 366 del riconsegnato_consuntivo_kwh */
	data x;
		do x=364 to 366;
			output;
		end;
	run;

	proc sql noprint;
		select x into:lg separated by ' ' from x;
	quit;

	proc panel data=target_shift;
		id cross_section data_gas_merge;
		lag rtot_lag(&lg.)/out=consuntivo_shift_anno;
	run;

	data consuntivo_shift2 (drop=cross_section rtot_lag);
		set consuntivo_shift_anno(where=(cross_section = "dummy1"));

		if cmiss(of _all_) then
			delete;
	run;

	proc sort data=consuntivo_shift2;
		by data_gas_merge;
	run;

	/**************/

	/* Lag1 del riconsegnato_consuntivo_kwh
	NOTA BENE:
	--> se sono tra le 6 e le 13:
	- utilizzo lag1 del riconsegnato_scada_kwh 
	- se manca il riconsegnato_scada_kwh, utilizzo il lag giorno simile
	--> se sono dopo 13:
	- utilizzo il lag1 del riconsegnato_consuntivo_kwh
	- se manca il lag1 del riconsegnato_consuntivo_kwh, utilizzo lag1 del riconsegnato_scada_kwh
	- se manca il riconsegnato_scada_kwh, utilizzo il lag giorno simile
	*/

	/* Prendo il lagsimile delle ore 13 che equivale al lag1 o 7 o altro del riconsegnato_consuntivo_kwh
	---> scalo indietro di un giorno perch? posso usare questa var un giorno prima */
	data lagsim (drop=datetime_solare);
		set &output_lagsim (where=(hour(datetime_solare)=13));
		data_gas = intnx("day",datepart(datetime_solare),1);
		format data_gas date9.;
	run;

	proc sort data=lagsim;
		by data_gas;
	run;

	proc sort data=&output_rtot;
		by data_gas;
	run;

	data rfill (drop=riconsegnato_scada_kwh riconsegnato_consuntivo_kwh);
		merge &output_rtot(drop=deltalinepack_final) lagsim;
		by data_gas;
		lag_scada_1=lag(riconsegnato_scada_kwh);
		lag_rtot_1=lag(riconsegnato_consuntivo_kwh);
	run;

	data rfill_6(keep=datetime_solare6 rfill_6
		rename=(datetime_solare6=datetime_solare rfill_6=rtot_lag_1)) 
		rfill_13(keep=datetime_solare13 rfill_13
		rename=(datetime_solare13=datetime_solare rfill_13=rtot_lag_1));
		set rfill;

		/* Ore 6: utilizzo il riconsegnato scada G-1 se presente, altrimenti il lag giorno simile */
		if lag_scada_1 ne . then
			rfill_6=lag_scada_1;
		else rfill_6=rtot_lag_simile;

		/* Ore 13: utilizzo il riconsengato uff G-1 se presente, altrimenti il rfill_6*/
		if lag_rtot_1 ne . then
			rfill_13=lag_rtot_1;
		else rfill_13=rfill_6;
		datetime_solare6=dhms(data_gas,6,0,0);
		datetime_solare13=dhms(data_gas,13,0,0);
		format datetime_solare6 datetime_solare13 datetime19.;
	run;

	proc sort data=rfill_6;
		by datetime_solare;
	run;

	proc sort data=rfill_13;
		by datetime_solare;
	run;

	data rtot_lag_1;
		merge rfill_6 rfill_13;
		by datetime_solare;

		if cmiss(of _all_) then
			delete;
		data_gas_merge=datepart(datetime_solare);
		format data_gas_merge date9.;
	run;

	proc sort data=rtot_lag_1;
		by data_gas_merge;
	run;

	/* NOTA: Ho dei record duplicati per data_gas_merge (due valori diversi in base all'ora)*/
	/**************/

	/* Moving average utilizzando i lag 1-30 costruiti in precedenza:
	moving average K usa K+1 valori: G-(1), G-(2), ..., G-(K+1)*/
	data final_lag;
		merge rtot_lag_1 consuntivo_shift1 consuntivo_shift2;
		by data_gas_merge;
	run;

	data out (where=(data_gas_merge>&start_date));
		set final_lag;

		%do i=2 %to 30;
			rtot_mvg_&i=mean(of rtot_lag_1 -- rtot_lag_&i);
		%end;
	run;

	/* Merge col calendario e foward fill */
	data out;
		merge &input_calendario (in=c) out(rename=(data_gas_merge=data_gas_rif));
		by datetime_solare;

		if c;
	run;

	proc sql noprint;
		select compress(substr(lowcase(name),6,length(lowcase(name))))
			into :savelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="out"
						and lowcase(name) like "rtot_%";
	quit;

	proc sql noprint;
		select lowcase(name)
			into :droplist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="out"
						and lowcase(name) like "rtot_%";
	quit;

	data out(drop= a b i j &droplist);
		set	out;
		array new &savelist;
		array old &droplist;
		retain new;
		a=dim(new);
		b=dim(old);

		if _n_=1 then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;
	run;

	/* Riaggiungo prefisso bil_ */
	proc sql noprint;
		select cat(lowcase(name),"=rtot_",lowcase(name))
			into :renamelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="out"
						and (lowcase(name) like "lag_%" or lowcase(name) like "mvg%");
	quit;

	data &output_rtot_lag_mvg;
		set out;
		rename &renamelist;
	run;

%mend gxg_create_rtot_lag_mvg;
%macro gxg_create_bil_lag_mvg(input_bilanciosnam, output_rtot, output_rcomp_termo,output_rall, 
			input_calendario,  output_bil_lag_mvg, start_date);

	%let to_keep_bil = date industriale reti_di_distribuzione riconsegnato_ad_altre_reti_tr 
		termoelettrico
		consumi_perdite_gnc_srg altro esportazioni_reti_di_terzi esp_rdt_consumi  
		totale_immesso uscit: sistemi_di_stoccaggio importazioni produzione_nazionale
		entrata: gnl:;

	data bil (drop=uscita: entrata: gnl_cavarzere gnl_livorno gnl_panigaglia
				rename=(date=data_gas
				Riconsegnato_ad_altre_Reti_tr=Ric_Reti_terzi
				Sistemi_di_stoccaggio=stoccaggio
				Reti_di_distribuzione=reti_distrib
				produzione_nazionale = produzione_naz));
		set &input_bilanciosnam(keep=&to_keep_bil);
		uscite=sum(of uscita:);
		entrate = sum(of entrata:);
		gnl = sum(of gnl:);
		ric_distrib_terzi = sum(reti_di_distribuzione,riconsegnato_ad_altre_reti_tr);
	run;

	/* Gestione Anomalie Bilancio */
	%if &flg_anomalie=1 %then
		%do;
			%put ===== Controllo Anomalie Dati Bilancio =====;

			%gestione_anomalie_bilancio(dt_base=&dt_solare, ds_in=bil, ds_out=bil, 
				storico_anomalie_bil=&output_anomalie_bilancio);
	%end;

	/* Tengo come master &input_calendario */
	proc sort data=&input_calendario(keep=data_gas_rif) 
		out=calendar_days(rename=(data_gas_rif=data_gas)) nodupkey;
		by data_gas_rif;
	run;

	data ric_all (rename=(deltalinepack_final=dlp) );
		merge calendar_days (in=p) 
			bil 
			&output_rtot  
			&output_rcomp_termo ;
		by data_gas;
		dlp_gwh = deltalinepack_final/1000000;
		if p;
	run;

	/* Eseguo FFILL dei valori del bilancio a missing sugli ultimi giorni */
	proc contents data=ric_all (drop=riconsegnato_termo_kwh esp_rdt_consumi data_gas) 
		out=cont_ric_all(keep=name) noprint;
	run;

	proc sql noprint;
		select lowcase(name) into: droplist
		separated by " "
		from cont_ric_all
		;
	quit;

	proc sql noprint;
		select compress(cat(lowcase(name),"_")) into: savelist
		separated by " "
		from cont_ric_all
		;
	quit;

	proc sql noprint;
		select compress(cat(lowcase(name),"_","=",lowcase(name))) into: renamelist
		separated by " "
		from cont_ric_all
		;
	quit;

	data ric_all_fill(rename=(&renamelist)
						drop= a b i j &droplist );
		set	ric_all;
		array new &savelist;
		array old &droplist;
		retain new;
		a=dim(new);
		b=dim(old);

		if _n_=1 then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;
	run;

	/* 
	1. Ricostruzione voce altro_consumi_perdite_gnc 
	--> NOTA BENE: Le voci del bilancio commerciale sono in gwh 
	--> Devo utilizzare il dlp_gwh
	2. Tengo il termoelettrico terna (rcomp_termo_kwh) e il termoelettrico commerciale
	3. Inserisco il delta tra termoelettrico commerciale e termoelettrico terna nella voce altro_consumi_perdite_gnc,
	creando la voce altro_c_p_gnc_termo
	*/
	data ric_all_fill1 (drop= delta_termo consumi_perdite_gnc_srg altro esportazioni_reti_di_terzi 
		esp_rdt_consumi rcom_termo_gwh dlp_gwh
		rename=(altro_consumi_perdite_gnc=altro_c_p_gnc));
		retain data_gas;
		set ric_all_fill (rename=(riconsegnato_termo_kwh = rcomp_termo_kwh));

		/* Dal 01jan2012 al 01may2016:
			   a) consumi_perdite_gnc_srg e altro sono missing. 
			   ---> non riesco a recuperarli singolarmente e costruisco un'unica voce altro_consumi_perdite_gnc
			   b) esportazioni_reti_di_terzi missing.
			   ---> non riesco a recuperarla
		*/
		if data_gas lt '01may2016'd then
			do;
				altro_consumi_perdite_gnc = sum(esp_rdt_consumi, -uscite, - ric_reti_terzi, -dlp_gwh);
			end;
		else if (data_gas ge '01may2016'd and data_gas le '31may2016'd) or (data_gas ge '01jul2016'd and data_gas le '31oct2017'd) then
			do;
				altro = sum(esportazioni_reti_di_terzi, - uscite, - ric_reti_terzi);
				altro_consumi_perdite_gnc = sum(altro, consumi_perdite_gnc_srg);
			end;
		else if data_gas ge '01jun2016'd and data_gas le '30jun2016'd then
			do;
				altro = sum(esportazioni_reti_di_terzi, - uscite, - ric_reti_terzi);

				/*consumi_perdite_gnc_srg lo prendo dalla formula indiretta */
				consumi_perdite_gnc_srg =  sum(totale_immesso, -uscite, -dlp_gwh, -reti_distrib, -industriale, -termoelettrico, -ric_reti_terzi, -altro);
				altro_consumi_perdite_gnc = sum(altro,consumi_perdite_gnc_srg);
			end;

		/* dal 01nov2017 ho altro, consumi_perdite_gnc_srg (e anche esportazioni_reti_di_terzi) --> non c'ï¿½ bisogno di ricostruire nulla,
				calcolo solo la somma di altro e consumi_perdite_gnc per completezza */
		else if data_gas ge '01nov2017'd then
			do;
				altro_consumi_perdite_gnc = sum(altro,consumi_perdite_gnc_srg);
			end;

		rcom_termo_gwh=rcomp_termo_kwh/1000000;
		delta_termo = termoelettrico - rcom_termo_gwh;
		altro_c_p_gnc_termo = altro_consumi_perdite_gnc+delta_termo;
		tgt_residuo_terna=altro_c_p_gnc_termo+ric_reti_terzi;
	run;

	/* Converto tutto in kwh */
	proc contents data=ric_all_fill1(drop=dlp riconsegnato: rcomp_termo_kwh data_gas) 
		out=to_convert(keep=name) noprint;
	run;

	proc sql noprint;
		select distinct name into: to_convert_vars
			separated by " "
		from to_convert
		;
	quit;

	data ric_all_fill2 (drop=i to_kwh);
		set ric_all_fill1;
		array var[*] &to_convert_vars;
		to_kwh=1000000;
		do i=1 to dim(var);
			var[i] = var[i]*to_kwh;
		end;
	run;


	/* Costruisco i targets */
	data out (rename=(data_gas=data_gas_rif));
		set ric_all_fill2 (keep=data_gas riconsegnato_consuntivo_kwh tgt_residuo_terna rcomp_termo_kwh 
							industriale reti_distrib);
		rename riconsegnato_consuntivo_kwh=tgt_riconsegnato_totale 
			rcomp_termo_kwh=tgt_termoelettrico_terna
			industriale=tgt_industriale reti_distrib=tgt_civile;
		datetime_solare=dhms(data_gas,6,0,0);
		format datetime_solare datetime19.;
	run;

	/* Lacio missing il tgt_termo per gli ultimi giorni del ds dove non ho ancora il valore */
	proc sql noprint;
		select max(data_gas_rif) format date9. into: max_data_gas_termo  
			from out
				where tgt_termoelettrico_terna ne .
		;
	quit;

	/* Merge col calendario e foward fill */
	data out;
		merge &input_calendario (in=c) out;
		by datetime_solare;
		if c;
	run;

	proc sql noprint;
		select compress(substr(lowcase(name),5,length(lowcase(name))))
			into :savelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="out"
						and lowcase(name) like "tgt%";
	quit;

	proc sql noprint;
		select lowcase(name)
			into :droplist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="out"
						and lowcase(name) like "tgt%";
	quit;

	data out(drop= a b i j &droplist );
		set	out;
		array new &savelist;
		array old &droplist;
		retain new;
		a=dim(new);
		b=dim(old);

		if _n_=1 then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;
	run;

	/* Riaggiungo prefisso tgt_ */
	proc sql noprint;
		select cat(lowcase(name),"=tgt_",lowcase(name))
			into :renamelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="out"
						and lowcase(name) in ("industriale" "civile" "riconsegnato_totale"
						"termoelettrico_terna" "residuo_terna");
	quit;

	data &output_rall;
		set out;
		rename &renamelist;
	run;

	/* Risetto a missing il termo terna degli ultimi giorni di cui non conosco il valore */
	data &output_rall;
		set &output_rall;

		if data_gas_rif gt "&max_data_gas_termo"d then
			do;
				tgt_termoelettrico_terna=.;
				tgt_residuo_terna=.;
			end;
	run;

	/* Non considero il riconsegnato_consuntivo_kwh e riconsegnato_scada_kwh (lag e mvg giï¿½
	calcolati nelle funzioni precedenti) e neanche il tgt_residuo_terna*/
	data ric_all_fill3;
		set ric_all_fill2 (drop=riconsegnato_consuntivo_kwh riconsegnato_scada_kwh tgt_residuo_terna);
	run;

	data ric_all_fill3 (drop=dlp rename=(dlp_new=dlp));
		set	ric_all_fill3;
		retain dlp_new;

		if _n_=1 then
			dlp_new=dlp;
		else if dlp ne . then
			dlp_new=dlp;
	run;


	/*************************/
	/* Lag da 2 a 33
	NOTA:
	- escludo altro_c_p_gnc_termo e rcomp_termo_kwh perchï¿½ non ho questi valori l'ultimo mese */
	proc sql noprint;
		select cats(lowcase(name),'=','bil_lag_',lowcase(name))
			into :rename_list separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="ric_all_fill3" and 
						lowcase(name) not in ('data_gas' 'altro_c_p_gnc_termo' 'rcomp_termo_kwh');
	quit;

	proc sql noprint;
		select cats('bil_lag_',lowcase(name))
			into :drop_list separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="ric_all_fill3" and 
						lowcase(name) not in ('data_gas' 'altro_c_p_gnc_termo' 'rcomp_termo_kwh');
	quit;

	proc sql noprint;
		select cats("bil_lag_",lowcase(name),'(&lg.)')
			into :listalg
				separated by ' '
			from dictionary.columns
				where lowcase(libname)="work" and lowcase(memname)="ric_all_fill3" and 
					lowcase(name) not in ('data_gas' 'altro_c_p_gnc_termo' 'rcomp_termo_kwh');
	quit;

	data bil_1 (drop=cross_section2 rename=(cross_section1=cross_section)) 
		bil_2 (drop=cross_section1 rename=(cross_section2=cross_section));
		set ric_all_fill3 (drop=altro_c_p_gnc_termo rcomp_termo_kwh);
		rename &rename_list;
		cross_section1 = 'dummy1';
		cross_section2 = 'dummy2';
	run;

	data bil_shift;
		set bil_1 bil_2;
	run;

	data x;
		do x=2 to 33;
			output;
		end;
	run;

	proc sql noprint;
		select x into:lg separated by ' ' 
			from x;
	quit;

	proc panel data=bil_shift;
		id cross_section data_gas;
		lag &listalg/out=consuntivo_shift;
	run;

	data consuntivo_shift1 (drop=cross_section &drop_list);
		set consuntivo_shift (where=(data_gas>=&start_date));
		if cross_section = "dummy1";
	run;

	/**********************/
	/* Lag da 364 a 366
	NOTA:
	- posso utilizzare anche altro_c_p_gnc_termo e rcomp_termo_kwh */
	proc sql noprint;
		select cats(lowcase(name),'=','bil_lag_',lowcase(name))
			into :rename_list separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="ric_all_fill3" and 
						lowcase(name) not in ('data_gas' 'altro_c_p_gnc' 'termoelettrico');
	quit;

	proc sql noprint;
		select cats('bil_lag_',lowcase(name))
			into :drop_list separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="ric_all_fill3" and 
						lowcase(name) not in ('data_gas' 'altro_c_p_gnc' 'termoelettrico');
	quit;

	proc sql noprint;
		select cats("bil_lag_",lowcase(name),'(&lg.)')
			into :listalg
				separated by ' '
			from dictionary.columns
				where lowcase(libname)="work" and lowcase(memname)="ric_all_fill3" and 
					lowcase(name) not in ('data_gas' 'altro_c_p_gnc' 'termoelettrico');
	quit;

	data bil_1 (drop=cross_section2 rename=(cross_section1=cross_section)) 
		bil_2 (drop=cross_section1 rename=(cross_section2=cross_section));
		set ric_all_fill3 (drop=altro_c_p_gnc termoelettrico);
		rename &rename_list;
		cross_section1 = 'dummy1';
		cross_section2 = 'dummy2';

		if rcomp_termo_kwh= . then
			do;
				rcomp_termo_kwh=1;
				altro_c_p_gnc_termo=1;
			end;
	run;

	data bil_shift;
		set bil_1 bil_2;
	run;

	data x;
		do x=364 to 366;
			output;
		end;
	run;

	proc sql noprint;
		select x into:lg separated by ' ' 
			from x;
	quit;

	proc panel data=bil_shift;
		id cross_section data_gas;
		lag &listalg/out=consuntivo_shift;
	run;

	data consuntivo_shift2 (drop=cross_section &drop_list);
		set consuntivo_shift (where=(data_gas>=&start_date));
		if cross_section = "dummy1";
	run;

	/**********************/
	/* Unisco consuntivo_shift1 e consuntivo_shif2 e aggiungo datetime_solare_g alle ore 6 */
	data final_bil_lag (rename = (data_gas=data_gas_g) );
		merge consuntivo_shift1 consuntivo_shift2;
		by data_gas;
		datetime_solare_g=dhms(data_gas,6,0,0);
		format datetime_solare_g datetime19.;
	run;

	/**********************/
	/* Lag 1 
	NOTE:
	- I dati di bilancio G-1 sono disponibili dalle ore 13 ora solare. Prima di quell'ora i dati 
	corrispondono all'ultimo valore disponibile (G-2)
	- Non ho mai rcomp_termo_kwh e altro_c_p_gnc_termo --> li escludo */
	proc sql noprint;
		select cats(lowcase(name),'=','bil_lag_',lowcase(name), '_1')
			into :rename_list
				separated by ' '
			from dictionary.columns
				where lowcase(libname)="work" and lowcase(memname)="ric_all_fill3" 
					and lowcase(name) not in ("data_gas" "rcomp_termo_kwh" "altro_c_p_gnc_termo");
	quit;

	data bil_lag1 (drop=data_gas where=(data_gas_g >= (&start_date-1)));
		set ric_all_fill3 (drop=rcomp_termo_kwh altro_c_p_gnc_termo);
		data_gas_g=intnx("day",data_gas,1);
		datetime_solare_g= dhms(data_gas_g,13,0,0);
		format data_gas_g date9. datetime_solare_g datetime18.;
		rename &rename_list;
	run;

	/* Unisco ds lag1, lag2-33, lag364-366 usando come maschera &input_calendario */
	data final_bil;
		merge &input_calendario (in=c rename=(data_gas_rif=data_gas_g datetime_solare = datetime_solare_g)) 
			final_bil_lag bil_lag1;
		by datetime_solare_g;

		if c;
	run;

	proc sql noprint;
		drop table final_bil_lag, bil_lag1;
	quit;

	/**********************/

	/* Lag costruiti a partire dalle stagionalitï¿½ individuate tramite analisi spettrale
	NOTE:
	- Per quanto riguarda altro_c_p_gnc_termo e rcomp_termo_kwh non posso
	fare i lag < 60 perchï¿½ non ho quei dati (utilizzo rispettivamente altro_c_p_gnc e termoelettrico (comm))
	- Se alcuni lag li ho giï¿½ costruiti prima non li rifaccio..!
	*/

	/*
	Per altro_c_p_gnc_termo non posso fare lag7 perchï¿½ non ce l'ho
	Per rcomp_termo_kwh utilizzo solo i lag del termoelettrico
	*/

	/* Maschera per creare lag significativi individuati tramite spectra */
	data stagionalita_mvg;
		length var $40 lag 8;
		var="altro_c_p_gnc";
		lag=7;
		output;
		var="altro_c_p_gnc_termo";
		lag=122;
		output;
		var="altro_c_p_gnc_termo";
		lag=183;
		output;
		var="altro_c_p_gnc_termo";
		lag=365;
		output;
		var="termoelettrico";
		lag=4;
		output;
		var="termoelettrico";
		lag=7;
		output;
		var="termoelettrico";
		lag=365;
		output;
		var="dlp";
		lag=4;
		output;
		var="dlp";
		lag=5;
		output;
		var="dlp";
		lag=7;
		output;
		var="dlp";
		lag=8;
		output;
		var="entrate";
		lag=7;
		output;
		var="entrate";
		lag=183;
		output;
		var="entrate";
		lag=365;
		output;
		var="gnl";
		lag=69;
		output;
		var="gnl";
		lag=219;
		output;
		var="gnl";
		lag=274;
		output;
		var="gnl";
		lag=365;
		output;
		var="importazioni";
		lag=7;
		output;
		var="importazioni";
		lag=183;
		output;
		var="industriale";
		lag=7;
		output;
		var="industriale";
		lag=73;
		output;
		var="industriale";
		lag=122;
		output;
		var="industriale";
		lag=365;
		output;
		var="produzione_naz";
		lag=183;
		output;
		var="produzione_naz";
		lag=274;
		output;
		var="produzione_naz";
		lag=365;
		output;
		var="reti_distrib";
		lag=91;
		output;
		var="reti_distrib";
		lag=100;
		output;
		var="reti_distrib";
		lag=137;
		output;
		var="reti_distrib";
		lag=183;
		output;
		var="reti_distrib";
		lag=365;
		output;
		var="ric_distrib_terzi";
		lag=91;
		output;
		var="ric_distrib_terzi";
		lag=100;
		output;
		var="ric_distrib_terzi";
		lag=137;
		output;
		var="ric_distrib_terzi";
		lag=183;
		output;
		var="ric_distrib_terzi";
		lag=365;
		output;
		var="ric_reti_terzi";
		lag=4;
		output;
		var="ric_reti_terzi";
		lag=7;
		output;
		var="ric_reti_terzi";
		lag=183;
		output;
		var="ric_reti_terzi";
		lag=365;
		output;
		var="stoccaggio";
		lag=7;
		output;
		var="stoccaggio";
		lag=122;
		output;
		var="stoccaggio";
		lag=137;
		output;
		var="stoccaggio";
		lag=183;
		output;
		var="stoccaggio";
		lag=365;
		output;
		var="totale_immesso";
		lag=4;
		output;
		var="totale_immesso";
		lag=7;
		output;
		var="totale_immesso";
		lag=183;
		output;
		var="totale_immesso";
		lag=365;
		output;
		var="uscite";
		lag=78;
		output;
		var="uscite";
		lag=84;
		output;
		var="uscite";
		lag=100;
		output;
		var="uscite";
		lag=183;
		output;
		var="uscite";
		lag=365;
		output;
	run;

	data stagionalita_lg;
		set stagionalita_mvg;
		n=_n_;
	run;

	proc sql noprint;
		select count(*) into: n_max
			from stagionalita_lg
		;
	quit;

	data bil_1_lag;
		set ric_all_fill3;
	run;


	%do i=1 %to &n_max;

		data _null_;
			set stagionalita_lg (where=(N=&i));
			call symput("var",compress(var));
			call symput("lag",compress(lag));
			call symput("mavg",compress(2));
		run;

		proc expand data=bil_1_lag  out=bil_1_lag method=none;
			id data_gas;
			convert &var = bil_lag_&var._&lag./ transformout=(lag &lag);
		run;

		/* Eseguo mooving average da 1 a 2 sulle variabili lag */
		%do j=1 %to &mavg;

			data _null_;
				lag_new = &lag*&j;
				m=&j+1;
				call symput('lag_new',compress(lag_new));
				call symput('m',compress(m));
			run;

			proc expand data=bil_1_lag out=bil_1_lag method=none;
				id data_gas;
				convert bil_lag_&var._&lag. = bil_lag&lag._&m._&var. / transformout=(lag &lag_new);
			run;

			proc contents data=bil_1_lag (drop=&var) out=cont (keep=name 
				where=(index(name,"&var") and substr(name,1,7) ne 'bil_mvg' and 
				(index(name, cat("bil_lag","&lag")) or name = cat("bil_lag_","&var","_","&lag") ) )) noprint;
			run;

			proc sql noprint;
				select name into: sel_var
					separated by " "
				from cont
				;
			quit;

			data bil_1_lag (drop=miss_dummy);
				set bil_1_lag;
				miss_dummy = nmiss(of &sel_var );

				if miss_dummy=0 then
					bil_mvg&m._&var._&lag. = mean(of &sel_var);
				else bil_mvg&m._&var._&lag.=.;
			run;

		%end;
	%end;


	data output;
		set bil_1_lag (keep=data_gas bil_lag_: bil_mvg:);
		datetime_solare_g=dhms(data_gas,6,0,0);
		format datetime_solare_g datetime19.;
	run;

	proc sql;
		drop table bil_1_lag;
	quit;

	data out_bilancio;
		merge output(rename=(data_gas=data_gas_g)) final_bil;
		by datetime_solare_g;
	run;


	/* Foward Fill */
	proc sql noprint;
		select compress(substr(lowcase(name),5,length(lowcase(name))))
			into :savelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="out_bilancio"
						and lowcase(name) like "bil_%";
	quit;

	proc sql noprint;
		select lowcase(name)
			into :droplist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="out_bilancio"
						and lowcase(name) like "bil_%";
	quit;

	data out_bilancio(drop= a b i j &droplist where=(data_gas_g ge &start_date ));
		set	out_bilancio;
		array new &savelist;
		array old &droplist;
		retain new;
		a=dim(new);
		b=dim(old);

		if _n_=1 then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;
	run;


	/* Riaggiungo prefisso bil_ */
	proc sql noprint;
		select cat(lowcase(name),"=bil_",lowcase(name))
			into :renamelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="out_bilancio"
						and (lowcase(name) like "lag_%" or lowcase(name) like "mvg%");
	quit;

	data out_bilancio;
		set out_bilancio;
		rename &renamelist;
	run;

	proc contents data=out_bilancio out=cont(keep=name) noprint;
	run;

	proc sql noprint;
		select name into: to_drop_mvg3
			separated by " "
		from cont 
			where scan(name,2,"_")="mvg3" and scan(name,-1,"_") in ("137" "183" "219" "274" "365" )
		;
	quit;

	proc sql noprint;
		select name into: to_drop_mvg2
			separated by " "
		from cont 
			where scan(name,2,"_")="mvg2" and scan(name,-1,"_") in ("219" "274" "365" )
		;
	quit;

	data &output_bil_lag_mvg;
		set out_bilancio (drop=&to_drop_mvg3 &to_drop_mvg2);
	run;

%mend gxg_create_bil_lag_mvg;

/* Funzioni meteo per generazione dataset */
%macro add_meteo_vars(input,grouping, output);
	/*	%let input=meteo_cons_citta_out;*/
	/*%let output=meteo_cons_cluster;*/
	proc sort data=&input(keep=data_gas_rif) out= &output nodupkey;
		by data_gas_rif;
	run;

	proc sort data=&input out=meteo_city;
		by tX_city data_gas_rif;
	run;

	data grouping_meteo;
		set &grouping;
	run;

	data input_clusters;
		merge meteo_city(in=p) grouping_meteo;
		by tx_city;

		if p;
	run;

	proc sql noprint;
		%let list_clusters=;
		select name
			into :list_clusters separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="GROUPING_METEO"
						and lowcase(name) not like "tx_city%";
	quit;

	%let count=%sysfunc(countw(&list_clusters));

	%do i = 1 %to &count;
		%let value=%scan(&list_clusters,&i);

		/*		%let i=1;*/
		/*		%let value=Top10_popolazione;*/
		data input_clusters_run;
			set input_clusters(where=(&value ne "") keep=data_gas_rif tx_city tm: &value);
		run;

		proc sql noprint;
			create table new_vars1_cluster
				as select t1.&value, 
					t1.data_gas_rif as data_gas_rif,
					max(t1.tmax) as tmax_max,
					min(t1.tmin) as tmin_min,
					/*					mean(t1.tmed) as tmed_med,*/
			std(t1.tmed) as tmed_std
			from input_clusters_run t1
				group by &value, data_gas_rif
					order by &value, data_gas_rif;
		quit;

		proc sort data=input_clusters_run out=input_clusters_run;
			by &value tX_city data_gas_rif;
		run;

		proc transpose data=input_clusters_run out=input_clusters_run_t(rename=(col1=value));
			by &value tX_city data_gas_rif;
			var tmax tmed tmin;
		run;

		proc sql noprint;
			create table new_vars2_cluster
				as select t1.&value, 
					t1.data_gas_rif as data_gas_rif,
					mean(t1.value) as tall_med
					/*					,std(t1.value) as tall_std*/
			from input_clusters_run_t t1
				group by &value, data_gas_rif
					order by &value, data_gas_rif;
		quit;

		proc sql noprint;
			create table RUN_OUTPUT as select * 
				from new_vars1_cluster t1 inner join new_vars2_cluster t2
					on t1.&value=t2.&value and t1.data_gas_rif=t2.data_gas_rif
				order by data_gas_rif, &value;
		quit;

		proc transpose data=RUN_OUTPUT out=RUN_OUTPUT_t;
			by data_gas_rif &value;
		run;

		data RUN_OUTPUT_t;
			set RUN_OUTPUT_t;
			key=cats(_name_ , "_", &value);
		run;

		proc transpose data=RUN_OUTPUT_t out=RUN_OUTPUT_t_add(drop= _name_);
			id key;
			by data_gas_rif;
		run;

		data &output;
			merge &output(in=p) RUN_OUTPUT_t_add;
			by data_gas_rif;
		run;

	%end;
%mend add_meteo_vars;

%macro fill_rename_meteocitta(input_dset,input_calendario, prefix, output_dset, start_cal);
	/*	%let input_dset=meteo_cons;*/
	/*	%let prefix="cons";*/
	/*	%let output_dset= meteo_consuntivo_final;*/
	/*	%let start_cal=&start_date;*/
	proc sort data=&input_dset out=temp;
		by datetime_solare;
	run;

	data temp;
		merge temp &input_calendario(in=p where=(datepart(datetime_solare)>=&start_cal) keep=datetime_solare);
		by datetime_solare;

		if p;
	run;

	/*--*/
	proc sql noprint;
		%let droplist=;
		select compress(cat(&prefix, "_" , name))
			into :droplist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="TEMP"
						and (name like "tm%" or name like "tall%");
	quit;

	proc sql noprint;
		%let savelist=;
		select name
			into :savelist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="TEMP"
						and (name like "tm%" or name like "tall%");
	quit;

	data &output_dset(drop=a b i j &savelist);
		set	TEMP;
		array new &droplist;
		array old &savelist;
		retain new;
		a=dim(new);
		b=dim(old);

		if _N_=1 then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;
	run;

%mend fill_rename_meteocitta;

%macro GxG_create_input_meteo_citta(input_dset, groups_meteo_citta, input_calendario, start_date, output);

	data _null_;
		start_date_adj=intnx("month",&start_date,-1, 'S');
		call symput('start_date_adj',start_date_adj);
	run;

	data meteo_prev_citta(rename= (nr_temp_max=tmax nr_temp_min=tmin nr_temp_med=tmed));
		set &input_dset(where=(id_tipo_dato="PREV" and 
			datepart(dt_data_elaborazione)>=&start_date_adj));
		distanza_insert_rif=datepart(dt_data_riferimento)-datepart(dt_data_elaborazione);
		data_prev=datepart(dt_data_riferimento);
		data_calc=datepart(dt_data_elaborazione);

		if tx_city="Forli'" then
			tx_city="Forli";

		if tx_city="L'Aquila" then
			tx_city="Aquila";

		if tx_city="La Spezia" then
			tx_city="LaSpezia";
		tx_city=compress(tx_city);
		format data_prev data_calc date9.;
	run;

	data meteo_cons_citta(rename= (nr_temp_max=tmax nr_temp_min=tmin nr_temp_med=tmed));
		set &input_dset(where=(id_tipo_dato="CONS" and 
			datepart(dt_data_elaborazione)>=&start_date_adj));
		distanza_insert_rif=datepart(dt_data_riferimento)-datepart(dt_data_elaborazione);
		data_rif=datepart(dt_data_riferimento);
		data_calc=datepart(dt_data_elaborazione);

		if tx_city="Forli'" then
			tx_city="Forli";

		if tx_city="L'Aquila" then
			tx_city="Aquila";

		if tx_city="La Spezia" then
			tx_city="LaSpezia";
		tx_city=compress(tx_city);
		format data_rif data_calc date9.;
	run;

	/* ------------ consuntivo ------------------ */
	proc sort data=meteo_cons_citta out=meteo_cons_citta_out;
		by tX_city data_rif dt_creazione;
	run;

	data meteo_cons_citta_out(rename=(data_rif=data_gas_rif) drop=id_tipo_dato k_d_e: dt: distanza:);
		set meteo_cons_citta_out;
		by tX_city data_rif;

		if last.data_rif;
	run;

	proc sort data=meteo_cons_citta_out;
		by data_gas_rif tX_city;
	run;

	proc transpose data=meteo_cons_citta_out out=meteo_cons_citta_t(Drop=_label_);
		by data_gas_rif tx_city;
	run;

	data meteo_cons_citta_t;
		set meteo_cons_citta_t;

		if _name_ ne "data_calc";
		key=cats(_name_, "_", tx_city);
	run;

	proc transpose data=meteo_cons_citta_t out=meteo_cons_citta_t(drop= _name_);
		id key;
		by data_gas_rif;
	run;

	data meteo_cons_citta_t;
		set meteo_cons_citta_t;
		data_calc=intnx("day",data_gas_rif,+1);
		miss=nmiss(of tm:);
		format data_calc date9.;
	run;

	%add_meteo_vars(meteo_cons_citta_out,&groups_meteo_citta, meteo_cons_cluster);

	data meteo_cons_citta_t;
		merge meteo_cons_citta_t meteo_cons_cluster;
		by data_gas_rif;
	run;

	/* ------------ previsioni ------------------ */
	/*prendo le previsioni fatte in G per G */
	data meteo_prev_citta_g;
		set meteo_prev_citta;

		if distanza_insert_rif = 0;
	run;

	proc sort data=meteo_prev_citta_g out=meteo_prev_citta_g;
		by tX_city data_prev dt_creazione;
	run;

	data meteo_prev_citta_g(rename=(data_prev=data_gas_rif) drop=id_tipo_dato k_d_e: dt: distanza:);
		set meteo_prev_citta_g;
		by tX_city data_prev;

		if last.data_prev;
	run;

	proc sort data=meteo_prev_citta_g out=meteo_prev_citta_g;
		by data_gas_rif tX_city;
	run;

	proc transpose data=meteo_prev_citta_g out=meteo_prev_citta_g_t(Drop=_label_);
		by data_gas_rif tx_city;
	run;

	data meteo_prev_citta_g_t;
		set meteo_prev_citta_g_t;

		if _name_ ne "data_calc";
		key=cats(_name_, "_", tx_city);
	run;

	proc transpose data=meteo_prev_citta_g_t out=meteo_prev_citta_g_t(drop= _name_);
		id key;
		by data_gas_rif;
	run;

	data meteo_prev_citta_g_t;
		set meteo_prev_citta_g_t;
		data_calc=intnx("day",data_gas_rif,0);
		format data_calc date9.;
		miss=nmiss(of tm:);
	run;

	%add_meteo_vars(meteo_prev_citta_g,&groups_meteo_citta, meteo_prev_cluster_g);

	data meteo_prev_citta_g_t;
		merge meteo_prev_citta_g_t meteo_prev_cluster_g;
		by data_gas_rif;
	run;

	/* ------------ previsioni g1 ------------------ */
	/*prendo le previsioni fatte in G per G+1 */
	data meteo_prev_citta_g1;
		set meteo_prev_citta;

		if distanza_insert_rif = 1;
	run;

	proc sort data=meteo_prev_citta_g1 out=meteo_prev_citta_g1;
		by tX_city data_prev dt_creazione;
	run;

	data meteo_prev_citta_g1(rename=(data_prev=data_gas_rif) drop=id_tipo_dato k_d_e: dt: distanza:);
		set meteo_prev_citta_g1;
		by tX_city data_prev;

		if last.data_prev;
	run;

	proc sort data=meteo_prev_citta_g1;
		by data_gas_rif tX_city;
	run;

	proc transpose data=meteo_prev_citta_g1 out=meteo_prev_citta_g1_t(Drop=_label_);
		by data_gas_rif tx_city;
	run;

	data meteo_prev_citta_g1_t;
		set meteo_prev_citta_g1_t;

		if _name_ ne "data_calc";
		key=cats(_name_, "_", tx_city);
	run;

	proc transpose data=meteo_prev_citta_g1_t out=meteo_prev_citta_g1_t(drop= _name_);
		id key;
		by data_gas_rif;
	run;

	data meteo_prev_citta_g1_t;
		set meteo_prev_citta_g1_t;
		data_calc=intnx("day",data_gas_rif,-1);
		format data_calc date9.;
		miss=nmiss(of tm:);
	run;

	%add_meteo_vars(meteo_prev_citta_g1,&groups_meteo_citta, meteo_prev_cluster_g1);

	data meteo_prev_citta_g1_t;
		merge meteo_prev_citta_g1_t meteo_prev_cluster_g1;
		by data_gas_rif;
	run;

	/*================================*/
	data meteo_cons_cons;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set meteo_cons_citta_t;
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(data_calc,10,0,0);
		format datetime_solare datetime_calc datetime19.;
	run;

	data meteo_cons_prevg;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set meteo_prev_citta_g_t;
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(intnx("day",data_gas_rif,+1),6,0,0);

		*g per g che uso la mattina dopo finch? arriva il vero consuntivo;
		format datetime_solare datetime_calc datetime19.;
	run;

	data meteo_cons;
		set meteo_cons_cons meteo_cons_prevg;
	run;

	proc sort data=meteo_cons;
		by data_gas_rif datetime_solare;
	run;

	/*================================*/
	/*fix*/
	data meteo_prev_prevg;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set meteo_prev_citta_g_t;
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(data_calc,10,0,0);
		format datetime_solare datetime_calc datetime19.;
	run;

	data meteo_prev_prevg1;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set meteo_prev_citta_g1_t;
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(intnx("day",data_gas_rif,0),6,0,0);
		format datetime_solare datetime_calc datetime19.;
	run;

	data meteo_prev;
		set meteo_prev_prevg meteo_prev_prevg1;
	run;

	proc sort data=meteo_prev;
		by data_gas_rif datetime_solare;
	run;

	/*================================*/
	%fill_rename_meteocitta(meteo_cons, &input_calendario, "urb_c", output_cons, &start_date_adj);
	%fill_rename_meteocitta(meteo_prev,  &input_calendario, "urb_g", output_prev_g, &start_date_adj);

	data &output(Rename=(date=data_solare_calc) 
		where=(datetime_solare >= dhms(&start_date, 6,0,0)));
		length datetime_solare hour data_solare_rif data_gas_rif 8.;
		merge output_cons(in=q 	drop=datetime_calc data: miss) 
			output_prev_g(in=p drop=datetime_calc data: miss);
		by datetime_solare;
		data_solare_rif=datepart(datetime_solare);
		hour=hour(datetime_solare);
		data_gas_rif=ifn(hour<6, data_solare_rif-1, data_solare_rif);

		if q and p;
		format data: date9.;
	run;

%mend GxG_create_input_meteo_citta;

%macro prepare_meteo_celle_dset(dset_meteo,grouping_all, dset_meteo_out);

	proc sort data=&dset_meteo out=&dset_meteo nodupkey;
		by data_rif cella;
	run;

	%add_celle_vars_all(&dset_meteo,&grouping_all, output_cons);

	proc sql noprint;
		%let var_groups=;
		select lowcase(name)
			into :var_groups separated by ' '
				from dictionary.columns
					where libname="WORK" and memname=upcase("&dset_meteo")
						and (lowcase(name) like "t_med%" or lowcase(name) like "t_gg%" or
						lowcase(name) like "altezza_min%" or lowcase(name) like "t_max_percepita%" 
						or lowcase(name) like "umidita%" );
	quit;

	%let count_var=%sysfunc(countw(&var_groups));

	proc sort data=&dset_meteo(keep=data_rif) out= output_selectvar nodupkey;
		by data_rif;
	run;

	%do j = 1 %to &count_var;
		%let var_cycle=%scan(&var_groups,&j);

		%add_celle_vars_selectvar(&dset_meteo,&&grouping_&var_cycle., 
			output_cons_&var_cycle,&var_cycle);

		data output_selectvar;
			merge output_selectvar(in=p) output_cons_&var_cycle;
			by data_rif;
		run;

	%end;

	/*output_cons e output_selectvar sono gli output*/
	/*devo mergarli su data_rif*/
	/* --------------------------- */
	data &dset_meteo_out;
		merge output_cons(in=p) output_selectvar;
		by data_rif;
	run;

%mend;

%macro fill_rename_meteocelle(input_dset, prefix, output_dset, start_cal);

	proc sort data=&input_dset out=temp;
		by data_gas_rif;
	run;

	data temp(drop=date hour);
		merge temp &input_calendario(in=p where=(data_gas_rif>=&start_cal));
		by datetime_solare;

		if p;
	run;

	/*--*/
	proc sql noprint;
		%let droplist1=;
		select compress(cat(&prefix, "_" , lowcase(name)))
			into :droplist1 separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="TEMP"
						and (lowcase(name) not like "dat%" 
						and lowcase(name) not like "sup%");
	quit;

	proc sql noprint;
		%let savelist1=;
		select lowcase(name)
			into :savelist1 separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="TEMP"
						and (lowcase(name) not like "dat%" 
						and lowcase(name) not like "sup%");
	quit;

	proc sql noprint;
		%let droplist2=;
		select compress(cat(&prefix, "_" , lowcase(name)))
			into :droplist2 separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="TEMP"
						and (lowcase(name) not like "dat%" 
						and lowcase(name) like "sup%");
	quit;

	proc sql noprint;
		%let savelist2=;
		select lowcase(name)
			into :savelist2 separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="TEMP"
						and (lowcase(name) not like "dat%" 
						and lowcase(name) like "sup%");
	quit;

	data &output_dset(drop=a b i j &savelist1 &savelist2);
		set	TEMP;
		array new &droplist1 &droplist2;
		array old &savelist1 &savelist2;
		retain new;
		a=dim(new);
		b=dim(old);

		if _N_=1 then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;
	run;

%mend;

/* serve aggiungere le funzioni di grouping/clustering */
%macro add_celle_vars_all(input,grouping, output);

	proc sort data=&input(keep=data_rif) out= &output nodupkey;
		by data_rif;
	run;

	proc sort data=&input out=meteo_city(drop=superficie riga colonna
		rename=(t_max_percepita=t_max_perc altezza_min=alt_min));
		by cella data_rif;
	run;

	data grouping_meteo;
		set &grouping;
	run;

	data input_clusters;
		merge meteo_city(in=p) grouping_meteo;
		by cella;

		if p;
	run;

	proc sql noprint;
		%let list_clusters=;
		select name
			into :list_clusters separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="GROUPING_METEO"
						and lowcase(name) not like "cella%";
	quit;

	%let count=%sysfunc(countw(&list_clusters));

	%do i = 1 %to &count;
		%let value=%scan(&list_clusters,&i);

		proc sort data=input_clusters(where=(&value ne "") 
			keep=data_rif cella t: UMIDITA alt_min PRECIPITAZIONI NEVE &value)
			out=input_clusters_run;
			by  &value data_rif;
		run;

		proc means data=input_clusters_run noprint;
			output out=new_vars1_cluster(drop=_TYPE_ _FREQ_);
			var t: UMIDITA alt_min;
			by &value data_rif;
		run;

		proc sort data=new_vars1_cluster out=new_vars1_cluster;
			by &value data_rif  _stat_;
		run;

		proc transpose data=new_vars1_cluster(where=(_stat_ not in ("N"))) out=new_vars1_cluster_t;
			by &value data_rif _stat_;
		run;

		data new_vars1_cluster_t(drop=_label_ _name_ _stat_);
			set new_vars1_cluster_t(rename=(col1=value));
			key = catx("_",_name_, _stat_);
		run;

		proc sort data=new_vars1_cluster_t out=new_vars1_cluster_t;
			by   &value data_rif  key;
		run;

		proc transpose data=new_vars1_cluster_t out=new_vars1_cluster_final(Drop=_name_);
			id key;
			by &value data_rif;
		run;

		proc sql noprint;
			create table new_vars2_cluster
				as select t1.&value, 
					t1.data_rif as data_rif,
					mean(t1.PRECIPITAZIONI) as prec_mean
					,sum(t1.PRECIPITAZIONI) as prec_sum,
					mean(t1.neve) as neve_mean
					,sum(t1.neve) as neve_sum
				from input_clusters_run t1
					group by &value, data_rif
						order by &value, data_rif;
		quit;

		proc sql noprint;
			create table output_run as select * 
				from new_vars1_cluster_final t1 inner join new_vars2_cluster t2
					on t1.&value=t2.&value and t1.data_rif=t2.data_rif
				order by data_rif, &value;
		quit;

		proc transpose data=output_run out=output_run_t;
			by data_rif &value;
		run;

		data output_run_t;
			set output_run_t;
			key=cats(&value,  "_", _name_);
		run;

		proc transpose data=output_run_t out=output(drop= _name_);
			id key;
			by data_rif;
		run;

		data &output;
			merge &output(in=p) output;
			by data_rif;
		run;

	%end;
%mend;

/* serve aggiungere le funzioni di grouping/clustering */
%macro add_celle_vars_selectvar(input,grouping, output, selectvar);

	proc sort data=&input(keep=data_rif) out= &output nodupkey;
		by data_rif;
	run;

	proc sort data=&input out=meteo_city(drop=superficie riga colonna);
		by cella data_rif;
	run;

	data grouping_meteo;
		set &grouping;
	run;

	data input_clusters;
		merge meteo_city(in=p) grouping_meteo;
		by cella;

		if p;
	run;

	proc sql noprint;
		%let list_clusters=;
		select name
			into :list_clusters separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="GROUPING_METEO"
						and lowcase(name) not like "cella%";
	quit;

	%let count=%sysfunc(countw(&list_clusters));

	%do i = 1 %to &count;
		%let value=%scan(&list_clusters,&i);

		data check;
			value="&value";

			if index(value,"bestHP_label")>0 then
				singlecell=1;
			else singlecell=0;
			call symput("flag",singlecell);
		run;

		%if &flag eq 0 %then
			%do;

				proc sort data=input_clusters(where=(&value ne "") 
					keep=data_rif cella &selectvar &value)
					out=input_clusters_run;
					by  &value data_rif;
				run;

				proc means data=input_clusters_run noprint;
					output out=new_vars1_cluster(drop=_TYPE_ _FREQ_);
					var &selectvar;
					by &value data_rif;
				run;

				proc sort data=new_vars1_cluster;
					by   &value data_rif  _stat_;
				run;

				proc transpose data=new_vars1_cluster(where=(_stat_ not in ("N"))) out=new_vars1_cluster_t;
					by &value data_rif _stat_;
				run;

				data new_vars1_cluster_t(drop=_label_ _name_ _stat_);
					set new_vars1_cluster_t(rename=(col1=value));
					key = catx("_",&value, _stat_);
				run;

				proc sort data=new_vars1_cluster_t out=new_vars1_cluster_t;
					by   &value data_rif  key;
				run;

				proc transpose data=new_vars1_cluster_t out=output_run(Drop=_name_);
					id key;
					by &value data_rif;
				run;

				proc sort data=output_run out=output_tempc(drop=&value);
					by data_rif  &value;
				run;

			%end;
		%else
			%do;

				proc sort data=input_clusters(where=(&value ne "") 
					keep=data_rif cella &selectvar &value)
					out=input_clusters_run;
					by  &value data_rif;
				run;

				data input_clusters_run;
					set input_clusters_run;
					key_name=catx("_",&value,"&selectvar");
				run;

				proc sort data=input_clusters_run out=new_vars1_cluster;
					by   data_rif key_name;
				run;

				proc transpose data=new_vars1_cluster out=output_tempc(Drop=_:);
					by  data_rif;
					id key_name;
				run;

			%end;

		data &output;
			merge &output(in=p) output_tempc;
			by data_rif;
		run;

	%end;
%mend;

%macro GxG_create_input_meteo_celle(input_celle_cons, input_celle_prev, celle_start, 
			grouping_all, output_final);

	data _null_;
		start_cell_adj=intnx("month",&celle_start,-1);
		call symput('start_cell_adj',start_cell_adj);
		start_date_celle_p=max(start_cell,"21MAY2016"d);
		call symputx('start_date_celle_p',start_date_celle_p);
	run;

	data consuntivi(drop=data where=(data_rif>=&start_cell_adj));
		set &input_celle_cons(drop=k_d_e_meteo_consuntivo);
		cella=cat("s",put(superficie,z2.0),"_","r",put(riga,z2.0),"c",put(colonna,z2.0));
		data_rif=datepart(data);
		format data_rif date9.;
		t_med=(t_max+t_min)/2;
		t_gg=max(0,20-t_med);
	run;

	data previsioni(where=(data_calc>=&start_date_celle_p));
		set &input_celle_prev(drop=k_d_e_dati_meteo_aree);
		cella=cat("s",put(superficie,z2.0),"_","r",put(riga,z2.0),"c",put(colonna,z2.0));
		data_prev=datepart(data);
		data_calc=datepart(dt_insert);
		distanza_insert_rif=data_prev-data_calc;
		t_med=(t_max+t_min)/2;
		t_gg=max(0,20-t_med);
		format data_prev data_calc date9.;
	run;

	PROC SQL;
		CREATE TABLE WORK.map_celle_cons AS 
			SELECT t1.SUPERFICIE, 
				(COUNT(DISTINCT(t1.RIGA))) AS COUNT_RIGA, 
				(COUNT(DISTINCT(t1.COLONNA))) AS COUNT_COLONNA,
				max(data_rif) format=date9. as max_data,
				min(data_rif) format=date9. as min_data
			FROM WORK.CONSUNTIVI t1
				GROUP BY t1.SUPERFICIE;
	QUIT;

	/*============================================================================*/
	/*======== consuntivi =========*/
	/*============================================================================*/
	/*non ho aggiornamento del dato, perlomeno non lo vedo (vedo solo quello "attivo" cio? l'ultimo)*/
	%prepare_meteo_celle_dset(consuntivi, &grouping_all,consuntivi_out);

	/*============================================================================*/
	/*======== previsioni g=========*/
	/*============================================================================*/
	/* ho aggiornamento del dato, perlomeno non lo vedo (vedo solo quello "attivo" cio? l'ultimo)*/
	proc sort data=previsioni(where=(distanza_insert_rif=0)) out=previsioni_g nodupkey dupout=dup;
		by cella data_calc  data_prev dt_insert;
	run;

	data previsioni_g(rename=(data_prev=data_rif));
		set previsioni_g;
		by cella data_calc  data_prev;

		if  last.data_prev;
	run;

	%prepare_meteo_celle_dset(previsioni_g,&grouping_all,previsioni_g_out);

	/*============================================================================*/
	/*======== previsioni g1=========*/
	/*============================================================================*/
	/* ho aggiornamento del dato, perlomeno non lo vedo (vedo solo quello "attivo" cio? l'ultimo)*/
	proc sort data=previsioni(where=(distanza_insert_rif=1)) out=previsioni_g1 nodupkey dupout=dup;
		by cella data_calc  data_prev dt_insert;
	run;

	data previsioni_g1(rename=(data_prev=data_rif));
		set previsioni_g1;
		by cella data_calc  data_prev;

		if  last.data_prev;
	run;

	%prepare_meteo_celle_dset(previsioni_g1,&grouping_all,previsioni_g1_out);

	/*------------------*/
	data previsioni_g_out_fill;
		set consuntivi_out(where=(data_rif<&start_date_celle_p)) previsioni_g_out;
	run;

	data previsioni_g1_out_fill;
		set consuntivi_out(where=(data_rif<&start_date_celle_p)) previsioni_g1_out;
	run;

	/*================================*/
	/*================================*/
	data meteocelle_cons_cons;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set consuntivi_out(rename=(data_rif=data_gas_rif));
		data_calc=intnx("day",data_gas_rif,+1);
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(data_calc,10,0,0);
		format data_calc date9. datetime_solare datetime_calc datetime19.;
	run;

	data meteocelle_cons_prevg;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set previsioni_g_out_fill(rename=(data_rif=data_gas_rif));
		data_calc=intnx("day",data_gas_rif,+0);
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(intnx("day",data_gas_rif,+1),6,0,0);

		*g per g che uso la mattina dopo finch? arriva il vero consuntivo;
		format data_calc date9. datetime_solare datetime_calc datetime19.;
	run;

	data meteocelle_cons;
		set meteocelle_cons_cons meteocelle_cons_prevg;
	run;

	proc sort data=meteocelle_cons;
		by data_gas_rif datetime_solare;
	run;

	/*================================*/
	/*fix*/
	data meteocelle_prev_prevg;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set previsioni_g_out_fill(rename=(data_rif=data_gas_rif));
		data_calc=intnx("day",data_gas_rif,+0);
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(data_calc,10,0,0);
		format data_calc date9. datetime_solare datetime_calc datetime19.;
	run;

	data meteocelle_prev_prevg1;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set previsioni_g1_out_fill(rename=(data_rif=data_gas_rif));
		data_calc=intnx("day",data_gas_rif,-1);
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(intnx("day",data_gas_rif,0),6,0,0);
		format data_calc date9. datetime_solare datetime_calc datetime19.;
	run;

	data meteocelle_prev;
		set meteocelle_prev_prevg meteocelle_prev_prevg1;
	run;

	proc sort data=meteocelle_prev;
		by data_gas_rif datetime_solare;
	run;

	/*================================*/
	%fill_rename_meteocelle(meteocelle_cons, "met_c", meteocelle_cons1, &celle_start);
	%fill_rename_meteocelle(meteocelle_prev, "met_g", meteocelle_prev1, &celle_start);

	data &output_final(Rename=(date=data_solare_calc) where=(datetime_solare >=dhms(&celle_start, 6,0,0) ));
		length datetime_solare hour data_solare_rif data_gas_rif 8.;
		merge meteocelle_cons1(in=q 	drop=datetime_calc data: ) 
			meteocelle_prev1(in=p drop=datetime_calc data: );
		by datetime_solare;
		data_solare_rif=datepart(datetime_solare);
		hour=hour(datetime_solare);
		data_gas_rif=ifn(hour<6, data_solare_rif-1, data_solare_rif);

		if q and p;
		format data: date9.;
	run;

%mend GxG_create_input_meteo_celle;

%macro gxg_create_tgt_rimanente(in_rtot, in_scada, ds_out);

	data add_tgt_rimanente1;
		set &in_scada (keep = scada_bil_ric_kwh_cumday datetime_prev data_gas_rif);
		rename datetime_prev=datetime_solare_g;
	run;

	data &ds_out (rename=data_gas=data_gas_rif drop=scada_bil_ric_kwh_cumday);
		merge add_tgt_rimanente1(in=a) &in_rtot (keep=Riconsegnato_Consuntivo_KWH data_gas rename=(data_gas=data_gas_rif));
		by data_gas_rif;

		if a;
		tgt_riconsegnato_diff=Riconsegnato_Consuntivo_KWH-scada_bil_ric_kwh_cumday;
	run;

%mend gxg_create_tgt_rimanente;

%macro GxG_create_input_merge();
	/* LA CHIAVE --> datetime_solare */
	data &input_calendario(where=(data_gas_rif<=&G_gas));
		data_solare_rif= intnx("month",&G_solare,0, 'B');

		do while (data_solare_rif<=&G1_solare);
			do hour=0 to 23;
				datetime_solare=dhms(data_solare_rif,hour,0,0);

				if hour<6 then
					data_gas_rif=intnx("day", data_solare_rif, -1);
				else data_gas_rif=data_solare_rif;
				output;
			end;

			data_solare_rif=intnx("day", data_solare_rif, 1, 's');
		end;

		format data_solare_rif date9.;
		format data_gas_rif date9.;
		format datetime_solare datetime16.;
	run;

	%create_calendar_vars(&input_calendario,&input_calendario );

	/*merge METEO citt?*/
	data meteo_citta_merge(drop= data_gas_rif data_solare_rif);
		set &output_citta;
	run;

	/*previsioni meteo celle*/
	data meteo_celle_merge;
		set &output_celle;
		drop data: hour;
	run;

	/*merge SCADA ORARIO*/
	data SCADA_MERGE;
		SET &output_scada;
		drop data_gas_rif giorno_gas_scada;
		rename datetime_prev=datetime_solare;
	run;

	data SCADA_coNS_MERGE;
		SET &output_scada_cons;
		rename datetime_calcolo=datetime_solare;
	run;

	/*merge LAG e MVG BIL*/
	data billagmvg_merge1;
		set &output_bil_lag_mvg(drop=data_gas_g date hour);
		rename datetime_solare_g=datetime_solare;
	run;

	/*merge LOG E MVG TARGET*/
	data billagmvg_merge2;
		set &output_rtot_lag_mvg(drop=data_gas_rif date hour);
	run;

	/*merge LAG SIM*/
	data LAGSIM_merge(drop=data_gas_rif date hour);
		set &output_lagsim;
	run;

	/*terna CONS*/
	data terna_cons_merge(Drop=hour giorno: );
		set &output_consuntivo_terna;
		rename dt_calcolo=datetime_solare;
	run;

	/*terna prev*/
	data terna_prev_merge(Drop=hour data: giorno: );
		set &output_previsione_terna;
		rename dt_calcolo=datetime_solare;
	run;

	data merge1;
		merge &input_calendario(in=p) 
			meteo_citta_merge(in=q) meteo_celle_merge(in=q)
			SCADA_MERGE SCADA_coNS_MERGE billagmvg_merge1 billagmvg_merge2 LAGSIM_merge
			LAGSIM_merge terna_cons_merge terna_prev_merge;
		by datetime_solare;

		if p;
	run;

	/*######################*/
	data input_final;
		set merge1;

		if data_gas_rif=&G_gas and hour=hour(&dt_solare);
	run;

	%round_input_vars(input_final);
	%checkds(&GxG_output_final);

	%if &dataset_presente=1 %then
		%do;

			data &GxG_output_final;
				set &GxG_output_final input_final;
			run;

		%end;
	%else %if &dataset_presente=0 %then
		%do;

			data &GxG_output_final;
				set input_final;
			run;

		%end;
%mend GxG_create_input_merge;

%macro GxG_create_dataset_meteo_citta(input_dset, groups_meteo_citta, input_calendario, start_date, output);

	data _null_;
		start_date_adj=intnx("month",&start_date,-1, 'S');
		call symput('start_date_adj',start_date_adj);
	run;

	data meteo_prev_citta(rename= (nr_temp_max=tmax nr_temp_min=tmin nr_temp_med=tmed));
		set &input_dset(where=(id_tipo_dato="PREV" and 
			datepart(dt_data_elaborazione)>=&start_date_adj));
		distanza_insert_rif=datepart(dt_data_riferimento)-datepart(dt_data_elaborazione);
		data_prev=datepart(dt_data_riferimento);
		data_calc=datepart(dt_data_elaborazione);

		if tx_city="Forli'" then
			tx_city="Forli";

		if tx_city="L'Aquila" then
			tx_city="Aquila";

		if tx_city="La Spezia" then
			tx_city="LaSpezia";
		tx_city=compress(tx_city);
		format data_prev data_calc date9.;
	run;

	data meteo_cons_citta(rename= (nr_temp_max=tmax nr_temp_min=tmin nr_temp_med=tmed));
		set &input_dset(where=(id_tipo_dato="CONS" and 
			datepart(dt_data_elaborazione)>=&start_date_adj));
		distanza_insert_rif=datepart(dt_data_riferimento)-datepart(dt_data_elaborazione);
		data_rif=datepart(dt_data_riferimento);
		data_calc=datepart(dt_data_elaborazione);

		if tx_city="Forli'" then
			tx_city="Forli";

		if tx_city="L'Aquila" then
			tx_city="Aquila";

		if tx_city="La Spezia" then
			tx_city="LaSpezia";
		tx_city=compress(tx_city);
		format data_rif data_calc date9.;
	run;

	/* ------------ consuntivo ------------------ */
	proc sort data=meteo_cons_citta out=meteo_cons_citta_out;
		by tX_city data_rif dt_creazione;
	run;

	data meteo_cons_citta_out(rename=(data_rif=data_gas_rif) drop=id_tipo_dato k_d_e: dt: distanza:);
		set meteo_cons_citta_out;
		by tX_city data_rif;

		if last.data_rif;
	run;

	proc sort data=meteo_cons_citta_out;
		by data_gas_rif tX_city;
	run;

	proc transpose data=meteo_cons_citta_out out=meteo_cons_citta_t(Drop=_label_);
		by data_gas_rif tx_city;
	run;

	data meteo_cons_citta_t;
		set meteo_cons_citta_t;

		if _name_ ne "data_calc";
		key=cats(_name_, "_", tx_city);
	run;

	proc transpose data=meteo_cons_citta_t out=meteo_cons_citta_t(drop= _name_);
		id key;
		by data_gas_rif;
	run;

	data meteo_cons_citta_t;
		set meteo_cons_citta_t;
		data_calc=intnx("day",data_gas_rif,+1);
		miss=nmiss(of tm:);
		format data_calc date9.;
	run;

	%add_meteo_vars(meteo_cons_citta_out,&groups_meteo_citta, meteo_cons_cluster);

	data meteo_cons_citta_t;
		merge meteo_cons_citta_t meteo_cons_cluster;
		by data_gas_rif;
	run;

	/* ------------ previsioni ------------------ */
	/*prendo le previsioni fatte in G per G */
	data meteo_prev_citta_g;
		set meteo_prev_citta;

		if distanza_insert_rif = 0;
	run;

	proc sort data=meteo_prev_citta_g out=meteo_prev_citta_g;
		by tX_city data_prev dt_creazione;
	run;

	data meteo_prev_citta_g(rename=(data_prev=data_gas_rif) drop=id_tipo_dato k_d_e: dt: distanza:);
		set meteo_prev_citta_g;
		by tX_city data_prev;

		if last.data_prev;
	run;

	proc sort data=meteo_prev_citta_g out=meteo_prev_citta_g;
		by data_gas_rif tX_city;
	run;

	proc transpose data=meteo_prev_citta_g out=meteo_prev_citta_g_t(Drop=_label_);
		by data_gas_rif tx_city;
	run;

	data meteo_prev_citta_g_t;
		set meteo_prev_citta_g_t;

		if _name_ ne "data_calc";
		key=cats(_name_, "_", tx_city);
	run;

	proc transpose data=meteo_prev_citta_g_t out=meteo_prev_citta_g_t(drop= _name_);
		id key;
		by data_gas_rif;
	run;

	data meteo_prev_citta_g_t;
		set meteo_prev_citta_g_t;
		data_calc=intnx("day",data_gas_rif,0);
		format data_calc date9.;
		miss=nmiss(of tm:);
	run;

	%add_meteo_vars(meteo_prev_citta_g,&groups_meteo_citta, meteo_prev_cluster_g);

	data meteo_prev_citta_g_t;
		merge meteo_prev_citta_g_t meteo_prev_cluster_g;
		by data_gas_rif;
	run;

	/* ------------ previsioni g1 ------------------ */
	/*prendo le previsioni fatte in G per G+1 */
	data meteo_prev_citta_g1;
		set meteo_prev_citta;

		if distanza_insert_rif = 1;
	run;

	proc sort data=meteo_prev_citta_g1 out=meteo_prev_citta_g1;
		by tX_city data_prev dt_creazione;
	run;

	data meteo_prev_citta_g1(rename=(data_prev=data_gas_rif) drop=id_tipo_dato k_d_e: dt: distanza:);
		set meteo_prev_citta_g1;
		by tX_city data_prev;

		if last.data_prev;
	run;

	proc sort data=meteo_prev_citta_g1;
		by data_gas_rif tX_city;
	run;

	proc transpose data=meteo_prev_citta_g1 out=meteo_prev_citta_g1_t(Drop=_label_);
		by data_gas_rif tx_city;
	run;

	data meteo_prev_citta_g1_t;
		set meteo_prev_citta_g1_t;

		if _name_ ne "data_calc";
		key=cats(_name_, "_", tx_city);
	run;

	proc transpose data=meteo_prev_citta_g1_t out=meteo_prev_citta_g1_t(drop= _name_);
		id key;
		by data_gas_rif;
	run;

	data meteo_prev_citta_g1_t;
		set meteo_prev_citta_g1_t;
		data_calc=intnx("day",data_gas_rif,-1);
		format data_calc date9.;
		miss=nmiss(of tm:);
	run;

	%add_meteo_vars(meteo_prev_citta_g1,&groups_meteo_citta, meteo_prev_cluster_g1);

	data meteo_prev_citta_g1_t;
		merge meteo_prev_citta_g1_t meteo_prev_cluster_g1;
		by data_gas_rif;
	run;

	/*================================*/
	data meteo_cons_cons;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set meteo_cons_citta_t;
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(data_calc,10,0,0);
		format datetime_solare datetime_calc datetime19.;
	run;

	data meteo_cons_prevg;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set meteo_prev_citta_g_t;
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(intnx("day",data_gas_rif,+1),6,0,0);

		*g per g che uso la mattina dopo finch? arriva il vero consuntivo;
		format datetime_solare datetime_calc datetime19.;
	run;

	data meteo_cons;
		set meteo_cons_cons meteo_cons_prevg;
	run;

	proc sort data=meteo_cons;
		by data_gas_rif datetime_solare;
	run;

	/*================================*/
	/*fix*/
	data meteo_prev_prevg;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set meteo_prev_citta_g_t;
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(data_calc,10,0,0);
		format datetime_solare datetime_calc datetime19.;
	run;

	data meteo_prev_prevg1;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set meteo_prev_citta_g1_t;
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(intnx("day",data_gas_rif,0),6,0,0);
		format datetime_solare datetime_calc datetime19.;
	run;

	data meteo_prev;
		set meteo_prev_prevg meteo_prev_prevg1;
	run;

	proc sort data=meteo_prev;
		by data_gas_rif datetime_solare;
	run;

	/*================================*/
	%fill_rename_meteocitta(meteo_cons, &input_calendario, "urb_c", output_cons, &start_date_adj);
	%fill_rename_meteocitta(meteo_prev,  &input_calendario, "urb_g", output_prev_g, &start_date_adj);

	data &output(Rename=(date=data_solare_calc) 
		where=(datetime_solare >= dhms(&start_date, 6,0,0)));
		length datetime_solare hour data_solare_rif data_gas_rif 8.;
		merge output_cons(in=q 	drop=datetime_calc data: miss) 
			output_prev_g(in=p drop=datetime_calc data: miss);
		by datetime_solare;
		data_solare_rif=datepart(datetime_solare);
		hour=hour(datetime_solare);
		data_gas_rif=ifn(hour<6, data_solare_rif-1, data_solare_rif);

		if q and p;
		format data: date9.;
	run;

%mend GxG_create_dataset_meteo_citta;

%macro GxG_create_dataset_meteo_celle(input_celle_cons, input_celle_prev, celle_start, 
			grouping_all, output_final);

	data _null_;
		start_cell_adj=intnx("month",&celle_start,-1);
		call symput('start_cell_adj',start_cell_adj);
		start_date_celle_p=max(start_cell,"21MAY2016"d);
		call symputx('start_date_celle_p',start_date_celle_p);
	run;

	data consuntivi(drop=data where=(data_rif>=&start_cell_adj));
		set &input_celle_cons(drop=k_d_e_meteo_consuntivo);
		cella=cat("s",put(superficie,z2.0),"_","r",put(riga,z2.0),"c",put(colonna,z2.0));
		data_rif=datepart(data);
		format data_rif date9.;
		t_med=(t_max+t_min)/2;
		t_gg=max(0,20-t_med);
	run;

	data previsioni(where=(data_calc>=&start_date_celle_p));
		set &input_celle_prev(drop=k_d_e_dati_meteo_aree);
		cella=cat("s",put(superficie,z2.0),"_","r",put(riga,z2.0),"c",put(colonna,z2.0));
		data_prev=datepart(data);
		data_calc=datepart(dt_insert);
		distanza_insert_rif=data_prev-data_calc;
		t_med=(t_max+t_min)/2;
		t_gg=max(0,20-t_med);
		format data_prev data_calc date9.;
	run;

	PROC SQL;
		CREATE TABLE WORK.map_celle_cons AS 
			SELECT t1.SUPERFICIE, 
				(COUNT(DISTINCT(t1.RIGA))) AS COUNT_RIGA, 
				(COUNT(DISTINCT(t1.COLONNA))) AS COUNT_COLONNA,
				max(data_rif) format=date9. as max_data,
				min(data_rif) format=date9. as min_data
			FROM WORK.CONSUNTIVI t1
				GROUP BY t1.SUPERFICIE;
	QUIT;

	/*============================================================================*/
	/*======== consuntivi =========*/
	/*============================================================================*/
	/*non ho aggiornamento del dato, perlomeno non lo vedo (vedo solo quello "attivo" cio? l'ultimo)*/
	%prepare_meteo_celle_dset(consuntivi, &grouping_all,consuntivi_out);

	/*============================================================================*/
	/*======== previsioni g=========*/
	/*============================================================================*/
	/* ho aggiornamento del dato, perlomeno non lo vedo (vedo solo quello "attivo" cio? l'ultimo)*/
	proc sort data=previsioni(where=(distanza_insert_rif=0)) out=previsioni_g nodupkey dupout=dup;
		by cella data_calc  data_prev dt_insert;
	run;

	data previsioni_g(rename=(data_prev=data_rif));
		set previsioni_g;
		by cella data_calc  data_prev;

		if  last.data_prev;
	run;

	%prepare_meteo_celle_dset(previsioni_g,&grouping_all,previsioni_g_out);

	/*============================================================================*/
	/*======== previsioni g1=========*/
	/*============================================================================*/
	/* ho aggiornamento del dato, perlomeno non lo vedo (vedo solo quello "attivo" cio? l'ultimo)*/
	proc sort data=previsioni(where=(distanza_insert_rif=1)) out=previsioni_g1 nodupkey dupout=dup;
		by cella data_calc  data_prev dt_insert;
	run;

	data previsioni_g1(rename=(data_prev=data_rif));
		set previsioni_g1;
		by cella data_calc  data_prev;

		if  last.data_prev;
	run;

	%prepare_meteo_celle_dset(previsioni_g1,&grouping_all,previsioni_g1_out);

	/*------------------*/
	data previsioni_g_out_fill;
		set consuntivi_out(where=(data_rif<&start_date_celle_p)) previsioni_g_out;
	run;

	data previsioni_g1_out_fill;
		set consuntivi_out(where=(data_rif<&start_date_celle_p)) previsioni_g1_out;
	run;

	/*================================*/
	/*================================*/
	data meteocelle_cons_cons;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set consuntivi_out(rename=(data_rif=data_gas_rif));
		data_calc=intnx("day",data_gas_rif,+1);
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(data_calc,10,0,0);
		format data_calc date9. datetime_solare datetime_calc datetime19.;
	run;

	data meteocelle_cons_prevg;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set previsioni_g_out_fill(rename=(data_rif=data_gas_rif));
		data_calc=intnx("day",data_gas_rif,+0);
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(intnx("day",data_gas_rif,+1),6,0,0);

		*g per g che uso la mattina dopo finch? arriva il vero consuntivo;
		format data_calc date9. datetime_solare datetime_calc datetime19.;
	run;

	data meteocelle_cons;
		set meteocelle_cons_cons meteocelle_cons_prevg;
	run;

	proc sort data=meteocelle_cons;
		by data_gas_rif datetime_solare;
	run;

	/*================================*/
	/*fix*/
	data meteocelle_prev_prevg;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set previsioni_g_out_fill(rename=(data_rif=data_gas_rif));
		data_calc=intnx("day",data_gas_rif,+0);
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(data_calc,10,0,0);
		format data_calc date9. datetime_solare datetime_calc datetime19.;
	run;

	data meteocelle_prev_prevg1;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set previsioni_g1_out_fill(rename=(data_rif=data_gas_rif));
		data_calc=intnx("day",data_gas_rif,-1);
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(intnx("day",data_gas_rif,0),6,0,0);
		format data_calc date9. datetime_solare datetime_calc datetime19.;
	run;

	data meteocelle_prev;
		set meteocelle_prev_prevg meteocelle_prev_prevg1;
	run;

	proc sort data=meteocelle_prev;
		by data_gas_rif datetime_solare;
	run;

	/*================================*/
	%fill_rename_meteocelle(meteocelle_cons, "met_c", meteocelle_cons1, &celle_start);
	%fill_rename_meteocelle(meteocelle_prev, "met_g", meteocelle_prev1, &celle_start);

	data &output_final(Rename=(date=data_solare_calc) where=(datetime_solare >=dhms(&celle_start, 6,0,0) ));
		length datetime_solare hour data_solare_rif data_gas_rif 8.;
		merge meteocelle_cons1(in=q 	drop=datetime_calc data: ) 
			meteocelle_prev1(in=p drop=datetime_calc data: );
		by datetime_solare;
		data_solare_rif=datepart(datetime_solare);
		hour=hour(datetime_solare);
		data_gas_rif=ifn(hour<6, data_solare_rif-1, data_solare_rif);

		if q and p;
		format data: date9.;
	run;

%mend GxG_create_dataset_meteo_celle;

%macro GxG_create_dataset_merge(cut_date);
	/* LA CHIAVE --> datetime_solare */
	data _null_;
		if hour(&dt_solare)<14 and hour(&dt_solare)>=6 then
			call symputx("end_date_merge", intnx("day", &G_gas, -1));
		else call symputx("end_date_merge", &G_gas);
	run;

	data &input_calendario(where=(data_gas_rif<&end_date_merge));
		data_solare_rif=&start_date;

		do while (data_solare_rif<=&G1_solare);
			do hour=0 to 23;
				datetime_solare=dhms(data_solare_rif,hour,0,0);

				if hour<6 then
					data_gas_rif=intnx("day", data_solare_rif, -1);
				else data_gas_rif=data_solare_rif;
				output;
			end;

			data_solare_rif=intnx("day", data_solare_rif, 1, 's');
		end;

		format data_solare_rif date9.;
		format data_gas_rif date9.;
		format datetime_solare datetime16.;
	run;

	%create_calendar_vars(&input_calendario,&input_calendario );

	/*merge METEO citt?*/
	data meteo_citta_merge(drop= data_gas_rif data_solare_rif);
		set &output_citta;
	run;

	/*previsioni meteo celle*/
	data meteo_celle_merge;
		set &output_celle;
		drop data: hour;
	run;

	/*merge SCADA ORARIO*/
	data SCADA_MERGE;
		SET &output_scada;
		drop data_gas_rif giorno_gas_scada;
		rename datetime_prev=datetime_solare;
	run;

	data SCADA_coNS_MERGE;
		SET &output_scada_cons;
		rename datetime_calcolo=datetime_solare;
	run;

	/*merge LAG e MVG BIL*/
	data billagmvg_merge1;
		set &output_bil_lag_mvg(drop=data_gas_g date hour);
		rename datetime_solare_g=datetime_solare;
	run;

	/*merge LOG E MVG TARGET*/
	data billagmvg_merge2;
		set &output_rtot_lag_mvg(drop=data_gas_rif date hour);
	run;

	/*merge LAG SIM*/
	data LAGSIM_merge(drop=data_gas_rif date hour);
		set &output_lagsim;
	run;

	/*terna CONS*/
	data terna_cons_merge(Drop=hour giorno: );
		set &output_consuntivo_terna;
		rename dt_calcolo=datetime_solare;
	run;

	/*terna prev*/
	data terna_prev_merge(Drop=hour giorno: );
		set &output_previsione_terna;
		rename dt_calcolo=datetime_solare;
	run;

	data merge1;
		merge &input_calendario(in=p) 
			meteo_citta_merge(in=q) meteo_celle_merge(in=q)
			SCADA_MERGE SCADA_coNS_MERGE billagmvg_merge1 billagmvg_merge2 LAGSIM_merge
			LAGSIM_merge terna_cons_merge terna_prev_merge;
		by datetime_solare;

		if p;
	run;

	/*######################*/
	/*target*/
	data riconsegnato;
		set &output_rall(drop=date hour data_gas_rif);
	run;

	data merge2(where=(data_gas_rif>=&start_date));
		merge merge1(in=p) riconsegnato(in=q);
		by datetime_solare;

		if p and q;
	run;

	/*######################*/
	/*aggiungo il target per differenza*/
	data merge_final;
		merge merge2 (in=a) &output_rdif (rename=(datetime_solare_g=datetime_solare)
			drop=data_gas_rif Riconsegnato_Consuntivo_KWH);
		by datetime_solare;

		if a;
	run;

	%round_input_vars(merge_final);

	%if &dataset_presente=1 %then
		%do;

			data &GxG_RicTot_cut(where=(data_gas_rif>=&cut_date));
				set merge_final;
			run;

			data scarto_add;
				length datetime_calcolo_train 8;
				datetime_calcolo_train=&dt_solare;
				set &GxG_RicTot_base(where=(data_gas_rif>=&cut_date));
				format datetime_calcolo_train datetime19.;
			run;

			proc append base=&GxG_RicTot_removed data=scarto_add force;
			run;

			data &GxG_output_final;
				set &GxG_RicTot_base(where=(data_gas_rif<&cut_date)) &GxG_RicTot_cut;
			run;

		%end;
	%else %if &dataset_presente=0 %then
		%do;

			data &GxG_output_final;
				set merge_final;
			run;

		%end;
%mend GxG_create_dataset_merge;

%macro remove_dupp_tset(dataset, output, dupp, datetime_var);

	proc sort data=&dataset out=&output nodupkey dupout=&dupp;
		by &datetime_var;
	run;

	proc sql noprint;
		select count(*) into :n_dupp
			from &dupp;
	quit;

	%put IL DATASET %dataset CONTENEVA %sysfunc(compress(&n_dupp)) DUPLICATI;
%mend;

%macro dset_phy2mem(dset_name, lib_phy, lib_mem, TDAY_GAS);
	%checkds(&lib_mem..&dset_name);

	%if &dataset_presente=1 %then
		%do;
			%PUT DELETING &dset_name IN &lib_mem;

			proc casutil;
				DROPTABLE casdata="&dset_name" incaslib="&lib_mem";
			run;

		%end;

	data load;
		set &lib_phy..&dset_name(where=(data_gas_rif>=intnx("month", &TDAY_GAS, -59, 'B')));
	run;

	%PUT LOADING &dset_name IN &lib_mem;

	proc casutil;
		load data=work.load outcaslib="&lib_mem" casout="&dset_name" promote;
	quit;

%mend;

%macro round_input_vars(dset);

	data &dset(drop=i);
		set &dset;
		array FIX_LIST {*} terna: met: bil: urb: tgt: scada: rtot: ;
		do i =1 to dim(FIX_LIST);
			FIX_LIST[i]=round(FIX_LIST[i],0.00001);
		end;
	run;

%mend;