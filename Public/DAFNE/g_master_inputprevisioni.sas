/*===============================================================*/
/***** rilascio 2020-01-XX - versione v1.0 *****/
/***** autori: Binda, Oliva, Rizzolo *****/
/***** BIP S.p.a. *****/
/*===============================================================*/

/*============ General Options and Creation of CAS Session ========*/
options casdatalimit=ALL;
cas mySession sessopts=(caslib=casuser timeout=31536000 locale="en_US");
caslib _all_ assign;

options mprint mlogic symbolgen linesize=max;
ods exclude all;

/*============ Global Vars ========*/
/*============ Global Vars ========*/
%global dataset_presente lib_physical lib_memory g_output_final_name gxg_rictot_trainingset;
%global ensemble_id;

%let start = %sysfunc(datetime());
%let pfx=;
%let lib_physical=for_svil;
%let lib_memory=public;
%let g_output_final_name=&pfx.g_inputmodelli_viya;
%let ensemble_id=9 10 11 12;
%let gxg_rictot_trainingset=&lib_physical..&pfx.g_trainingset_viya;

filename gfcn FILESRVC folderpath="&logic_path" filename='g_functions.sas';
filename ginp FILESRVC folderpath="&logic_path" filename='g_input.sas';
filename gprv FILESRVC folderpath="&logic_path" filename='g_previsioni.sas';
filename gens FILESRVC folderpath="&logic_path" filename='g_ensemble.sas';
filename gmer FILESRVC folderpath="&logic_path" filename='g_merge.sas';


%macro clean_all();

	proc datasets lib=work kill nolist memtype=data;
	quit;

	Data MVars ( Keep = Name );
		Set SasHelp.VMacro;
		Where Scope = 'GLOBAL';

		if find(Name, "SAS") = 0;

		if find(Name, "SYS") = 0;

		if find(Name, "CLIENT") = 0;

		if find(Name, "SQL") = 0;

		if find(Name, "EG") = 0;

		if find(Name, "ORA") = 0;

		IF NAME NOT IN ("GXG_RICTOT_TRAININGSET", "ENSEMBLE_ID", "LIB_PHYSICAL", "LIB_MEMORY", "LIB_PATH",
						"G_OUTPUT_FINAL_NAME","START", "GINP", "GFCN", "GENS", "GPRV", "GMER", "PFX");
	Run;

	Data _Null_;
		Set MVars;
		Call Symdel( Name );
	Run;

%mend clean_all;

%macro input_prediction();
	%put '=================== INIZIO PREVISIONE ====================';

	%if %sysfunc(exist(&gxg_rictot_trainingset)) %then
		%do;
			%include gfcn;
			%include ginp;
				%clean_all();
			%include gprv;
				%clean_all();
			%include gens;
				%clean_all();
			%include gmer;
				%clean_all();
		%end;

%mend input_prediction;

%input_prediction();

data time_execution;
	dt_start = &start;
	dt_end = datetime();
	time_execution = dt_end - dt_start;
	format dt: datetime19.;
	call symputx('time_execution', time_execution);
run;

%put "========================================================================";
%put "IL CODICE SAS E' GIUNTO AL TERMINE IN: &time_execution. SECONDI";

cas mySession terminate; 
