/*===============================================================*/
/***** rilascio 2020-01-XX - versione v1.0 *****/
/***** autori: Binda, Oliva, Rizzolo *****/
/***** BIP S.p.a. *****/
/*===============================================================*/

/*============ General Options and Creation of CAS Session ========*/
options casdatalimit=ALL;
cas mySession sessopts=(caslib=casuser timeout=31536000 locale="en_US");
caslib _all_ assign;

options mprint mlogic symbolgen linesize=max;
ods exclude all;

/*============ Global Vars ========*/
/* %global dataset_presente lib_physical lib_memory g_output_final_name; */
/* %global logic_path lib_path; */
%global dataset_presente;

%let start = %sysfunc(datetime());
%let diff_end_month=-1;
%let pfx=;
%let lib_physical=for_svil;
%let lib_memory=public;

/**************************/
/* PER IL TEST */
/* %let g_output_final_name=&pfx.g_trainingset_viya_test; */
%let g_output_final_name=&pfx.g_trainingset_viya;

%let lib_path = /opt/sas/viya/config/data/cas/default/dafne; 
%let logic_path = /Public/DAFNE/;
/* %let lib_path = /opt/sas/viya/config/data/cas/default/dafne_bkp;  */
/* %let logic_path = /Public/DAFNE_BKP/; */
filename glib FILESRVC folderpath="&logic_path" filename='librerie_viya.sas';
filename gfcn FILESRVC folderpath="&logic_path" filename='g_functions.sas';
filename gdat FILESRVC folderpath="&logic_path" filename='g_dataset.sas';
filename getr FILESRVC folderpath="&logic_path" filename='g_recupero_training.sas';

data g_limit_day;
	g_limit_day=intnx("day",datepart(intnx("dtmonth", &start,-1,"E")),&diff_end_month);
	call symputx("g_limit_day",g_limit_day);
	format g_limit_day date9.;
run;

 
%macro clean_all();

	proc datasets lib=work kill nolist memtype=data;
	quit;

	Data MVars ( Keep = Name );
		Set SasHelp.VMacro;
		Where Scope = 'GLOBAL';

		if find(Name, "SAS") = 0;

		if find(Name, "SYS") = 0;

		if find(Name, "CLIENT") = 0;

		if find(Name, "SQL") = 0;

		if find(Name, "EG") = 0;

		if find(Name, "ORA") = 0;

		IF NAME NOT IN ("G_OUTPUT_FINAL_NAME", "LOGIC_PATH", "START", "PFX", "LIB_PHYSICAL", "LIB_MEMORY",
		"LIB_PATH", "DATASET_PRESENTE", "GRAPHTERM");
	Run;

	Data _Null_;
		Set MVars;
		Call Symdel( Name );
	Run;

%mend clean_all;

%macro Emergenza_Dataset_Training(lib_phys=, lib_mem=, dset=, date_cut=);

	/* Allocazione librerie */
	%include glib;

/* 	Elimino trainingset esistente in &lib_phys e &lib_mem */
	proc datasets library=&lib_phys;
		delete &dset;
	run;

	proc casutil;
  		droptable casdata="&dset" incaslib="&lib_mem" quiet;
	run;

	%put "G EMERGENZA GENERAZIONE DATASET INIZIO";
/*  */
/* 	Richiamo codici per ricreare il trainingset */
	%include gfcn;
	%include gdat;

	/* Taglio opportunamente il trainingset */
	%put "G fa training il primo del mese quindi taglio dset sulla data dell ultimo consuntivo 
			del mese predecente g_limit_day che corrisponde a fine mese - 1";

	data &lib_phys..&dset;
		set &lib_phys..&dset;
		if data_gas_rif <=&date_cut;
	run;
/*  */
/* 	Carico dset in public promuovendolo */
	data load;
		set &lib_phys..&dset;
	run;
	
	proc casutil;
   		droptable casdata="&dset" incaslib="&lib_mem" quiet;
	run;

	proc casutil;
		load data=load outcaslib="&lib_mem" casout="&dset" promote;
	quit;

	%put "G GENERAZIONE DATASET TERMINATO";

	%clean_all();
	
	%put "G TRAINING MODELLI";
	
	%include getr;
	
	%put "G TRAINING MODELLI TERMINATO";

%mend Emergenza_Dataset_Training;

%Emergenza_Dataset_Training(lib_phys=&lib_physical, lib_mem=&lib_memory, dset=&g_output_final_name, date_cut=&g_limit_day);

/* data time_execution; */
/* 	dt_start = &start; */
/* 	dt_end = datetime(); */
/* 	time_execution = dt_end - dt_start; */
/* 	format dt: datetime19.; */
/* 	call symputx('time_execution', time_execution); */
/* run; */
/*  */
/* %put "========================================================================"; */
/* %put "IL CODICE SAS E' GIUNTO AL TERMINE IN: &time_execution. SECONDI"; */
/*  */
/* cas mySession terminate;  */
