%Macro Adeguamento_Input(dsinput, dsoutput);

data  tmp_output_models(keep=DT_CALCOLO DT_RIFERIMENTO_DATI MODELLO PREV TIPOLOGIA_PREVISIONE ora data);
SET &dsinput.;
ora=hour(DT_CALCOLO);
data=datepart(dt_calcolo);
run;

PROC SQL;
SELECT MAX(data) INTO:Max_dt 
FROM tmp_output_models ;
QUIT;

DATA tmp_output_models;
SET tmp_output_models;
IF data=&Max_dt.;
UNITA_MISURA=62;
RUN;

PROC SQL;
SELECT MAX(ora) INTO:Max_ora
FROM tmp_output_models;
quit;

data tmp_output_models;
set tmp_output_models;
IF ORA=&Max_ora.;
run;

data &dsoutput. ;
/*attrib K_PREV_GIORNALIERE 	    length=8.   format=commax13.3  						label='K_PREV_GIORNALIERE';*/
attrib PREV_INF  				length=8.   format=commax13.3  						label='PREV_INF';
attrib PREV                     length=8.   format=commax13.3  						label='PREV';
attrib PREV_SUP                 length=8.   format=commax13.3  						label='PREV_SUP';
attrib FK_ANAG_UNITA_MISURA		length=8.   format=commax13.3  						label='FK_ANAG_UNITA_MISURA';
attrib PCS_ORA_KWH_SM3          length=8.   format=commax13.3  						label='PCS_ORA_KWH_SM3';
attrib PCS_ORA_KJ_SM3	        length=8.   format=commax13.3 						label='PCS_ORA_KJ_SM3';
attrib PCS_ORA_KWH_NM3          length=8.   format=commax13.3  						label='PCS_ORA_KWH_NM3';
attrib DT_CALCOLO				length=8.   format=datetime20. informat=datetime20. label='DT_CALCOLO';
attrib DT_RIFERIMENTO_DATI		length=8.   format=datetime20. informat=datetime20. label='DT_RIFERIMENTO_DATI';
attrib VERSIONE                 length=8.   format=commax13.3  						label='VERSIONE';
attrib TIPOLOGIA_PREVISIONE     length=$30.   format=$30. 		   informat=$30.  	label='TIPOLOGIA_PREVISIONE';
attrib MODELLO 		            length=$30.   format=$30. 		   informat=$30.  	label='MODELLO';
/*attrib K_STORICO 	            length=8.   format=commax13.3  						label='K_STORICO';*/
*attrib FL_AZIONE				length=$1.   format=$1. 		   informat=$1.  		label='FL_AZIONE';
*attrib UTENTE_AZIONE            length=$30.   format=$30. 		   informat=$30.  	label='UTENTE_AZIONE';
*attrib FL_DELETED 	            length=$1.   format=$1. 		   informat=$1.  		label='FL_DELETED';
*attrib DT_AZIONE				length=8.   format=datetime20. informat=datetime20. label='DT_AZIONE';
attrib UNITA_MISURA				length=8.   format=8.  label='UNITA_MISURA';


call missing(of _all_);
stop;
run;

PROC APPEND DATA=tmp_output_models BASE=&dsoutput. FORCE; RUN;

%Mend;


%Adeguamento_Input (for_svil.output_models_viya, output_models_viya);

%Macro Historicizing(dsinput, dsoutput);


/* Libreria contenenete le tabelle Oracle di input/output */

LIBNAME WEBDIPS ORACLE  DB_OBJECTS=(TABLES SYNONYMS VIEWS)  PATH=WEBDICS SCHEMA=SVWEBV06  USER=SVWEBV06 PASSWORD=SVWEBV06;
LIBNAME PCS ORACLE  DB_OBJECTS=(TABLES SYNONYMS VIEWS)  PATH=WEBDICS SCHEMA=SVWEBV06  USER=SVWEBV06 PASSWORD=SVWEBV06;

%if %sysfunc(exist(WEBDIPS.&dsoutput.)) %then %do;

proc append data=&dsinput.  base=WEBDIPS.PREV_TEMP_GIORNALIERE_VIYA  force; quit;

/*PROC SQL NECESSARIA ALLA VALORIZZAZIONE DEI CAMPI PCS*/
 proc sql noprint;
          select 
                     t.pcs_ora_kj_sm3 as pcs_ora_kj_sm3 format=best32.,   
                     t.pcs_ora_kwh_sm3 as pcs_ora_kwh_sm3 format=best32.,  
                     t.pcs_ora_kwh_nm3 as pcs_ora_kwh_nm3 format=best32.
                 into :pcs_ora_kj_sm3, :pcs_ora_kwh_sm3, :pcs_ora_kwh_nm3
          from PCS.bil_pcs_medio_ora t
          where t.k_bil_pcs_medio_ora = ( select max(s.k_bil_pcs_medio_ora)
                            from PCS.bil_pcs_medio_ora  s); quit;

proc sql noprint;
connect to oracle  (PATH='WEBDICS' USER='SVWEBV06' PASSWORD='SVWEBV06');

execute(update &dsoutput. o
set o.fl_deleted='1'
where to_char((o.dt_calcolo-(5/24)),'YYYYMMDDHH24')||to_char(o.dt_riferimento_dati,'YYYYMMDD')||o.tipologia_previsione||o.modello in (
select to_char((n.dt_calcolo-(5/24)),'YYYYMMDDHH24')||to_char(n.dt_riferimento_dati,'YYYYMMDD')||n.tipologia_previsione||n.modello from PREV_TEMP_GIORNALIERE_VIYA n) ) by oracle;

execute (insert into &dsoutput. (K_PREV_GIORNALIERE, K_STORICO, PREV_INF, PREV, PREV_SUP, FK_ANAG_UNITA_MISURA, PCS_ORA_KWH_SM3, pcs_ora_kj_sm3, PCS_ORA_KWH_NM3, DT_CALCOLO, DT_RIFERIMENTO_DATI, VERSIONE, TIPOLOGIA_PREVISIONE, MODELLO, FL_AZIONE, UTENTE_AZIONE, FL_DELETED, DT_AZIONE,UNITA_MISURA)
(select SPREV_GIORNALIERE_VIYA.currval AS K_PREV_GIORNALIERE,
SPREV_GIORNALIERE_VIYA.nextval as K_STORICO,
b.PREV_INF, 
b.PREV, 
b.PREV_SUP, 
b.FK_ANAG_UNITA_MISURA, 
&pcs_ora_kwh_sm3 as PCS_ORA_KWH_SM3,
&pcs_ora_kj_sm3 as pcs_ora_kj_sm3,
&pcs_ora_kwh_nm3 as PCS_ORA_KWH_NM3, 
b.DT_CALCOLO, 
b.DT_RIFERIMENTO_DATI, 
b.VERSIONE, 
b.TIPOLOGIA_PREVISIONE, 
b.MODELLO, 
'I' as FL_AZIONE, 
'USER_AUTOMATIC' as UTENTE_AZIONE, 
'0' as FL_DELETED, 
sysdate as DT_AZIONE,
b.UNITA_MISURA
from PREV_TEMP_GIORNALIERE_VIYA b)) by oracle;
disconnect from oracle;
quit;


proc sql noprint;
connect to oracle  (PATH='WEBDICS' USER='SVWEBV06' PASSWORD='SVWEBV06');
execute (delete PREV_TEMP_GIORNALIERE_VIYA) by oracle;
disconnect from oracle;
quit;

%end;
	%else %do;

proc append data=&dsinput.  base= WEBDIPS.PREV_TEMP_GIORNALIERE_VIYA  force; quit;

proc sql noprint;
connect to oracle (PATH='WEBDICS' USER='SVWEBV06' PASSWORD='SVWEBV06');
execute (create table &dsoutput. as
(select SPREV_GIORNALIERE_VIYA.currval AS K_PREV_GIORNALIERE,
SPREV_GIORNALIERE_VIYA.nextval as K_STORICO,
b.PREV_INF, 
b.PREV, 
b.PREV_SUP, 
b.FK_ANAG_UNITA_MISURA, 
&pcs_ora_kwh_sm3 as PCS_ORA_KWH_SM3,
&pcs_ora_kj_sm3 as pcs_ora_kj_sm3,
&pcs_ora_kwh_nm3 as PCS_ORA_KWH_NM3, 
b.DT_CALCOLO, 
b.DT_RIFERIMENTO_DATI, 
b.VERSIONE, 
b.TIPOLOGIA_PREVISIONE, 
b.MODELLO, 
'I' as FL_AZIONE, 
'USER_AUTOMATIC' as UTENTE_AZIONE, 
'0' as FL_DELETED, 
sysdate as DT_AZIONE,
b.UNITA_MISURA
from PREV_TEMP_GIORNALIERE_VIYA b)) by oracle;
disconnect from oracle;
quit;


proc sql noprint;
connect to oracle  (PATH='WEBDICS' USER='SVWEBV06' PASSWORD='SVWEBV06');
execute (delete PREV_TEMP_GIORNALIERE_VIYA) by oracle;
disconnect from oracle;
quit;

	%end;
%MEND;

%Historicizing(dsinput=OUTPUT_MODELS_VIYA ,dsoutput= PREV_GIORNALIERE_VIYA);
