/*===============================================================*/
/***** rilascio 2021-10-XX - versione v1.0 *****/
/***** autori:  *****/
/***** BIP S.p.a. *****/
/*===============================================================*/
/* %let start=%sysfunc(datetime()); */

/*============ Opzioni Generali ================================*/
options mlogic mprint;
ods exclude all;

/*============ Definizione Path ================================*/
%let lib_input=&lib_physical;
%let lib_output=&lib_physical;

/*============ Pulizia Libreria casuser e work =================*/
proc datasets lib=casuser kill nolist;
quit;

proc datasets lib=work kill nolist;
quit;

/*============ Dataset di input e output =================*/
%let input_grid=&lib_input..g1_configurazioni;
%let input_dset=&lib_input..&output_final_name.;
%let output_varsel=&lib_output..g1_varsel;
%let time_id=data_gas_rif;

/*============ Macro variabili globali =================*/
%global sel_int_vars n_sel_class_vars sel_class_vars;


/*============ Assegnazione Date ===============================*/
data time;
	datetime_solare=&start;
	dt_train=intnx("hour",datetime_solare,1);
	hour_pred=hour(dt_train);
	data_gas_train = ifn(hour_pred<6, intnx('day', datepart(dt_train), -1), datepart(dt_train));
	/* devo portarmi all'inizio del mese dopo: vado in avanti di 5 giorni (va bene sia per il g4,g3,g2 e g1) */
	data_base_model=mdy(month(intnx("day",data_gas_train,5)),1,year(intnx("day",data_gas_train,5)));
	mese_pr=month(data_base_model);
	anno_pr=year(data_base_model);
	mese_pr_old=month(intnx("month",data_base_model,-1));
	anno_pr_old=year(intnx("month",data_base_model,-1));
	mese_pr_old2=month(intnx("month",data_base_model,-2));
	anno_pr_old2=year(intnx("month",data_base_model,-2));
	call symputx('data_gas_train', put(data_gas_train, date9.));
	call symputx("hour_pred", hour_pred);
	call symputx("mese_pr",mese_pr);
	call symputx("anno_pr",anno_pr);
	call symputx("mese_old",mese_pr_old);
	call symputx("anno_old",anno_pr_old);
	call symputx("mese_old2",mese_pr_old2);
	call symputx("anno_old2",anno_pr_old2);
	format datetime_solare dt_train datetime19. data_gas_train data_base_model date9.;
run;

%macro preprocessing_ds(input_ds, modello_totrain, output_ds);

	data _null_;
		modello_totrain="&modello_totrain";
		keep_vars=compress(cat(compress(modello_totrain),":"));
		tgt=compress(cat(compress(modello_totrain),"_tgt_riconsegnato_totale"));
		call symputx("keep_vars",keep_vars);
		call symputx("tgt",tgt);
	run;

	data input_ds;
		set &input_ds. (where = (&tgt ne .) 
						keep=datetime_solare hour &keep_vars);
	run;

	%if "&modello_totrain" eq "G1" or "&modello_totrain" eq "G2" %then %do;
	
		%rename_cols(work.input_ds,
		cat(name, ' = ', substr(name,4)),
		upcase(name) like "&modello_totrain._MET_C%" and name contains "sup");
	
		%rename_cols(work.input_ds,
		cat(name, ' = ', substr(name,4)),
		upcase(name) like "&modello_totrain._MET_P%" and name contains "sup");
	
	%end;
	
	%rename_cols(work.input_ds,
	cat(name, ' = ', substr(name,4)),
	upcase(name) like "&modello_totrain._SCADA%");
	
	%rename_cols(work.input_ds,
	cat(name, ' = ', substr(name,4)),
	upcase(name) like "&modello_totrain._BIL%");
	
	%rename_cols(work.input_ds,
	cat(name, ' = ', substr(name,4)),
	upcase(name) like "&modello_totrain._%");
	
	%round_input_vars(work.input_ds);

	data &output_ds;
		set work.input_ds;
	run;

%mend preprocessing_ds;



%macro do_training(hour_pred, anno_pr, mese_pr, id, approccio, target, pred_horizon, sottomodello, modello, id_modello, id_rete, horizon, 
							 flg_mesi_omologhi, 
							  flg_old_dafne, mesi_omologhi, mesi_val, mesi_train, varreduce_int_vars, varreduce_class_vars,
							 flg_drop_vars, drop_vars, flg_forced_vars, forced_int_vars, forced_class_vars, flg_maxeffects, maxeffects, varexp, flg_std_vars, 
							 std_vars, algo_nn, maxiter_nn, regl1, regl2, layer_string);

	/*=============== Variabile di "controllo" per il sotto-modello SIMILARITA' ===================*/		
	data time_day_similarita;
		sottomodello="&sottomodello";
		if sottomodello="S1" then time_day_similarita="time_day_group1";
		else if sottomodello="S2" then time_day_similarita="time_day_group2";
		else time_day_similarita="none";
		call symputx("time_day_similarita",time_day_similarita);
	run;

	/*=============== Stringa del modello da addestrare e degli ultimi due modelli da archiviare ===================*/
	data modello_to_save;
		retain code_file model_path model_string;
		pred_horizon="&pred_horizon";
		sottomodello="&sottomodello";
		id_modello="&id_modello";
		id_rete=&id_rete;
		R_id_rete=cat("R",id_rete);
		anno_pr=put(&anno_pr,z4.);
		mese_pr=put(&mese_pr,z2.);
		hour_pr=put(&hour_pred,z2.);
		anno_old=put(&anno_old,z4.);
		mese_old=put(&mese_old,z2.);
		anno_old2=put(&anno_old2,z4.);
		mese_old2=put(&mese_old2,z2.);
		/* Nuovo modello */
		model_path="&model_path";
		model_string=catx("_",pred_horizon,sottomodello,id_modello,R_id_rete,anno_pr,mese_pr,hour_pr);
		code_file=compress(catx("",model_path,"/",model_string,".txt"));
		/* Modello mese precedente */
		model_string_old=catx("_",pred_horizon,sottomodello,id_modello,R_id_rete,anno_old,mese_old,hour_pr);
		code_file_old=compress(catx("",model_path,"/",model_string_old,".txt"));
		/* Modello due mesi fa */
		model_string_old2=catx("_",pred_horizon,sottomodello,id_modello,R_id_rete,anno_old2,mese_old2,hour_pr);
		code_file_old2=compress(catx("",model_path,"/",model_string_old2,".txt"));
		call symputx("model_string",model_string);
		call symputx("code_file",code_file);
		call symputx("model_string_old",model_string_old);
		call symputx("code_file_old",code_file_old);
		call symputx("model_string_old2",model_string_old2);
		call symputx("code_file_old2",code_file_old2);
		drop id_rete;
	run;

	/*=============== Pre Processing ds di input ===================*/	
	/* 1. Subset hour da prevedere */
	
	%if "&target."="tgt_termoelettrico_terna" %then %do;

		data dataset1;
			set input_ds (where=(hour=&hour_pred));
			if nmiss (of _NUMERIC_) eq 0;								
		run;

	%end;

	%else %do;

		data dataset1;
			set input_ds (drop=tgt_termoelettrico_terna tgt_residuo_terna 
			                 where=(hour=&hour_pred));
			if nmiss (of _NUMERIC_) eq 0;								
		run;

	%end;

	/*=============== end_date ===================*/
	proc sql noprint;
		select intnx("month",max(data_gas_rif),1,"b") 
	/* 				format date9.  */
					into: end_date
		from dataset1
		;
	quit;	
   
	%get_train_back(date_to=&end_date, horizon=&horizon, pred_horizon=&pred_horizon, sottomodello=&sottomodello,
					time_day_similarita=&time_day_similarita,
					flg_mesi_omologhi=&flg_mesi_omologhi, mesi_omologhi=&mesi_omologhi,
					flg_old_dafne=&flg_old_dafne, mesi_train=&mesi_train,
					input_dataset=work.dataset1, output_dataset=casuser.training_eff);


	%do_var_selection(target=&target, id=&id, id_rete=&id_rete, pred_horizon=&pred_horizon, modello=&modello, 
						approccio=&approccio, flg_mesi_omologhi=&flg_mesi_omologhi, mesi_omologhi=&mesi_omologhi, mesi_val=&mesi_val,
						 mese_pr=&mese_pr, anno_pr=&anno_pr,
							  hour_pred=&hour_pred, flg_drop_vars=&flg_drop_vars, drop_vars=&drop_vars,
							  flg_std_vars=&flg_std_vars, std_vars=&std_vars,
							  flg_maxeffects=&flg_maxeffects, maxeffects=&maxeffects, varexp=&varexp,
							  flg_forced_vars=&flg_forced_vars, forced_int_vars=&forced_int_vars, 
							  forced_class_vars=&forced_class_vars,  
							  varreduce_int_vars=&varreduce_int_vars, varreduce_class_vars=&varreduce_class_vars, 
							  input_dataset=casuser.training_eff, output_dataset=&output_varsel);

	%do_nnet(target=&target, input_dataset_train=casuser.training_eff,
				algo_nn=&algo_nn, maxiter_nn=&maxiter_nn, regl1=&regl1, regl2=&regl2, layer_string=&layer_string,code_file=&code_file);

	%archivia_modelli(&model_path, &model_string_old, &code_file_old);
	%archivia_modelli(&model_path, &model_string_old2, &code_file_old2);

%mend do_training;

%macro get_train_back(date_to=, horizon=, pred_horizon=,
					  sottomodello=, time_day_similarita=, 
						flg_mesi_omologhi=, mesi_omologhi=,
						flg_old_dafne=,mesi_train=,
						input_dataset=, output_dataset=);

/* %let input_dataset=work.dataset1;  */
/* %let output_dataset=casuser.training_eff; */

		data limiti_trainingset;
			pred_horizon=compress("&pred_horizon");
			lim_inf_train=intnx('month', &date_to, -&horizon);
			/* !! Controllare bene data limite sup */
			if pred_horizon="G" then minus_days=1;
			else if pred_horizon="G1" then minus_days=2;
			else if pred_horizon="G2" then minus_days=3;
			else if pred_horizon="G3" then minus_days=4;
			else if pred_horizon="G4" then minus_days=5;
			lim_sup_train=intnx("day",&date_to,-minus_days,"s");
			call symput("lim_inf_train",lim_inf_train);
			call symput("lim_sup_train",lim_sup_train);
			call symput("lim_inf_train_str",put(lim_inf_train,date9.));
			call symput("lim_sup_train_str",put(lim_sup_train,date9.));
			format lim_: date9.;
		run;

		%put ====> FINESTRA TRAININGSET: DA &lim_inf_train_str INCLUSO A &lim_sup_train_str INCLUSO;

		data train_temp;
			set &input_dataset (where=(&lim_inf_train <=&time_id <= &lim_sup_train));
		run;

		%if &flg_mesi_omologhi=1 %then %do;

			%put ====> FINESTRA TRAININGSET: SOLO I MESI &mesi_omologhi;

			data train_temp;
				set train_temp (where=(month(&time_id) in &mesi_omologhi));
			run;

		%end;

		%if &flg_old_dafne=1 %then %do;

			%put ====> FINESTRA TRAININGSET: SOLO I MESI &mesi_train;

			data train_temp;
				set train_temp (where=(month(&time_id) in &mesi_train));
			run;

		%end;

		%if "&time_day_similarita" ne "none" %then %do;

			%put ====> FINESTRA TRAININGSET: FILTRO PER &time_day_similarita = 1 (SOTTOMODELLO &sottomodello);

			data train_temp;
				set train_temp (where=(&time_day_similarita = 1));
			run;

		%end;

		data &output_dataset;
			set train_temp;
		run;


%mend get_train_back;


%macro do_var_selection(target=, id=, id_rete=, pred_horizon=, modello=, approccio=, flg_mesi_omologhi=,
						mesi_omologhi=, mesi_val=,
						mese_pr=, anno_pr=,
						hour_pred=, flg_drop_vars=, drop_vars=,
						flg_std_vars=, std_vars=,
						flg_maxeffects=, maxeffects=, varexp=,
						flg_forced_vars=, forced_int_vars=, forced_class_vars=,  
						varreduce_int_vars=, varreduce_class_vars=, 
						input_dataset=, output_dataset= );

/* %let input_dataset=casuser.training_eff; */
/* %let output_dataset=&lib_output..&output_varsel; */

	data training_varsel;
		set &input_dataset;
	run;

	/* Standardizzo / Non Standardizzo */
	%if (&flg_std_vars=1) %then %do;
		
		%put ====> VARIABLE SELECTION: STANDARDIZZO SET DI VARIABILI INDICATE NELLA GRIGLIA;

		proc stdize data=training_varsel out=casuser.training_varsel 
				method=range;
			var &std_vars;
		run;

	%end;

	%else %do;

		%put ====> VARIABLE SELECTION: NON STANDARDIZZO VARIABILI IN INPUT;

		data casuser.training_varsel;
			set training_varsel;
		run;

	%end;

	/* Drop / no drop variabili */
	%if &flg_drop_vars=1 %then %do;

		%put ====> VARIABLE SELECTION: ELIMINO SET DI VARIABILI INDICATE NELLA GRIGLIA;

		data casuser.training_varsel;
			set casuser.training_varsel;
			drop &drop_vars;
		run;

	%end;
	
	%else %do;

		%put ====> VARIABLE SELECTION: NESSUNA VARIABILE DA ELIMINARE;

	%end;

	/* Variable Selection scegliendo n. variabili da selezionare / %varianza del target da spiegare */
	%if &flg_maxeffects=1 %then %do;

		%put ====> VARIABLE SELECTION: SCELGO &maxeffects VARIABILI;
		/* NOTA: varexp=1 essenziale altrimenti non fa il maxef */
		proc varreduce data=casuser.training_varsel technique=var;
			class &varreduce_class_vars;
			reduce supervised &target=&varreduce_int_vars / maxeffects=&maxeffects varexp=1;
			displayout SELECTEDEFFECTS=vars_selected;
		run;

	%end;

	%else %do;
			
		%put ====> VARIABLE SELECTION: SCELGO SET DI VARIABILI CHE SPIEGA LO &varexp DELLA VARIABILITA DEL TARGET;

		proc varreduce data=casuser.training_varsel technique=var;
			class &varreduce_class_vars;
			reduce supervised &target=&varreduce_int_vars / varexp=&varexp;
			displayout SELECTEDEFFECTS=vars_selected;
		run;

	%end;

	/* Aggiungo variabili forzate / Non aggiungo variabili forzate */
	%if &flg_forced_vars=1 %then %do;

		%put ====> VARIABLE SELECTION: FORZO SET DI VARIABILI DEFINITE NELLA GRIGLIA;

		data vars_to_add (keep=variable type);
			length variable $32 type $8;
			forced_int_vars="&forced_int_vars";
			forced_class_vars="&forced_class_vars";
			count_fi=countw(forced_int_vars);
			count_fc=countw(forced_class_vars);
			do i_force=1 to count_fi;
				variable=scan(forced_int_vars, i_force);
				type="INTERVAL";
				output;
			end;
			do c_force=1 to count_fc;
				variable=scan(forced_class_vars, c_force);
				type="CLASS";
				output;
			end;
		run;

		data casuser.vars_selected;
			set casuser.vars_selected vars_to_add;
			keep variable type;
		run;

	%end;

	/* Appendo nella tab di output */
	data casuser.vars_selected;
		length target $24 modello $11 mesi_previsione $28 variable $32 sottomodello $5 hour $100;
		set casuser.vars_selected;
		id=&id;
		pred_horizon="&pred_horizon";
		approccio="&approccio";
		modello="&modello";
		flg_mesi_omologhi=&flg_mesi_omologhi;
		if flg_mesi_omologhi=1 then mesi_previsione="&mesi_omologhi";
		else mesi_previsione="&mesi_val";
		id_rete=&id_rete;
		sottomodello="&sottomodello";
		target="&target";
		variable=lowcase(variable);
		type=lowcase(type);
		anno_pred=put(&anno_pr,z4.);
		mese_pred=put(&mese_pr,z2.);
		hour=cat("(",put(&hour_pred,z2.),")");
		drop flg_mesi_omologhi number;
	run;

	proc sort data=casuser.vars_selected out=vars_selected nodupkey;
		by variable;
	run;
	


	/* Macro var per input ai modelli */
	proc sql noprint;
		select distinct variable
			  into: sel_int_vars separated by " "
		from vars_selected
		where lowcase(type)="interval"
		;
	quit;

	proc sql noprint;
		select count(*)
			  into: n_sel_class_vars
		from vars_selected
		where lowcase(type)="class"
		;
	quit;

	proc sql noprint;
		select distinct variable
			  into:  sel_class_vars separated by " "
		from vars_selected
		where lowcase(type)="class"
		;
	quit;

	/* Salvo il ds finale in append */
	%put ====> VARIABLE SELECTION: SALVO I RISULTATI NELLA TABELLA &output_dataset;

	%if %sysfunc(exist(&output_dataset)) %then %do;

			proc append base=&output_dataset
						data=vars_selected(drop=type) force;
			run;

	%end;
	%else %do;

		data &output_dataset;
			stop;
			length id 8. pred_horizon $2 approccio $5 sottomodello $5 modello $11 mesi_previsione $28 id_rete 8. 
					 target $24 variable $32 mese_pred $2 anno_pred $4 hour $100;
		run;

		proc append base=&output_dataset 
					data=vars_selected(drop=type) force;
		run;

	%end;


%mend do_var_selection;

%macro do_nnet(target=, input_dataset_train=, algo_nn=, maxiter_nn=, regl1=, regl2=, layer_string=,code_file=);

	proc sort data=&input_dataset_train out=work.temp;
		by &time_id;
	run;
		
	data &input_dataset_train;
		set work.temp;
	run;

	/* missing=mean eventuslmente per target terna vicino a nthreads*/
	proc nnet data=&input_dataset_train standardize=midrange nthreads=16;
		%if &n_sel_class_vars ne 0 %then %do;
			input &sel_class_vars /level=NOM;
		%end;
		input &sel_int_vars / level=int;
		architecture mlp;
		target &target / level=int;
		train outmodel=casuser.model_neural seed=12345;
		optimization algorithm=&algo_nn maxiter=&maxiter_nn REGL1=&regl1 REGL2=&regl2;
		&layer_string;
		code file="&code_file";
	run;


%mend do_nnet;

%macro archivia_modelli(model_path, stringa_modello, file_modello);

	%if %sysfunc(fileexist("&file_modello")) %then %do;

		filename glob "&file_modello";
		filename addfile zip "&model_path./old_models.zip" member="models/&stringa_modello";

		data _null_;
			infile glob recfm=n;
			file addfile recfm=n;
			input byte $char1. @;
			put  byte $char1. @;
		run;

		filename addfile clear;

		%clean_models(model=&stringa_modello);

	%end;
	
%mend;

%macro clean_models(model=);

	filename myDir "&model_path./";

	data get;
		did=dopen("myDir");
		filecount=dnum(did);
		do i = 1 to filecount;
			file_ = dread(did, i);
			output;
		end;
		rc=dclose(did);
	run;

	data to_delete;
		set get;
		where file_ contains "&model";
		fname="tempfile";
		rc=filename(fname, cat("&model_path./", compress(file_)));
		rc=fdelete(fname);
	run;

%mend;

%macro execute_configs();

	/* Filtro record della griglia per ore 13 o 14 */
	data input_grid(where=(flg_filter=1));
		set &input_grid;
		hour_pred="&hour_pred.";
		hour_pred_num=input(hour_pred,best32.);
		if pred_horizon="G1" and id_target="TOT" then do;
			if hour_pred=14 then do;
				if hour=14 or mesi_omologhi="(5 6 8)" or mesi_val="(4 5 6 7 8 9)" or sottomodello="AR" then flg_filter=1;
				else flg_filter=0;
			end;
			else do;
				if hour=13 or mesi_omologhi="(1 2 12)" then flg_filter=1;
				else flg_filter=0;
			end;
		end;
		else do;
			if hour=13 then flg_filter=1;
			else flg_filter=0;
		end;
	run;

	/* Creo la stringa da inserire nella NNET a partire dalle colonne a nhidden e act e FILTRO per modello da trainare */
	data input_grid;
		set input_grid(where=(pred_horizon="&modello_totrain"));
		if nhidden ne . then do;
			array layers[*] nunits:;
			array actfun[*] act: ; 
			do i = 1 to nhidden;
				if i = 1 then layer_string = cat("hidden ", compress(put(layers[i], $32.)), "/ act=", compress(actfun[i]),";");
				else layer_string = catx(" ", layer_string, cat("hidden ", compress(put(layers[i], $32.)), "/ act=",compress(actfun[i]) ,";"));
			end;
			drop i; 
		end;
		else do;
			layer_string="";
		end;
	run;

/* Per DEBUG */
/* 	data null_; */
/* 		set input_grid(obs=3 firstobs=3); */
/* 		call symputx("id",id); */
/* 		call symputx("approccio",approccio); */
/* 		call symputx("target",target); */
/* 		call symputx("pred_horizon",pred_horizon); */
/* 		call symputx("sottomodello",sottomodello); */
/* 		call symputx("modello",modello); */
/* 		call symputx("id_modello",id_modello); */
/* 		call symputx("id_rete",id_rete); */
/* 		call symputx("horizon",horizon); */
/* 		call symputx("flg_mesi_omologhi",flg_mesi_omologhi); */
/* 		call symputx("flg_season_width",flg_season_width); */
/* 		call symputx("flg_old_dafne",flg_old_dafne); */
/* 		call symputx("mesi_omologhi",mesi_omologhi); */
/* 		call symputx("season_width",season_width); */
/* 		call symputx("mesi_val",mesi_val); */
/* 		call symputx("mesi_train",mesi_train); */
/* 		call symputx("varreduce_int_vars",varreduce_int_vars); */
/* 		call symputx("varreduce_class_vars",varreduce_class_vars); */
/* 		call symputx("flg_drop_vars",flg_drop_vars); */
/* 		call symputx("drop_vars",drop_vars); */
/* 		call symputx("flg_forced_vars",flg_forced_vars); */
/* 		call symputx("forced_int_vars",forced_int_vars); */
/* 		call symputx("forced_class_vars",forced_class_vars); */
/* 		call symputx("flg_maxeffects",flg_maxeffects); */
/* 		call symputx("maxeffects",maxeffects); */
/* 		call symputx("varexp",varexp); */
/* 		call symputx("flg_std_vars",flg_std_vars); */
/* 		call symputx("std_vars",std_vars); */
/* 		call symputx("algo_nn",algo_nn); */
/* 		call symputx("maxiter_nn",maxiter_nn); */
/* 		call symputx("regl1",regl1); */
/* 		call symputx("regl2",regl2); */
/* 		call symputx("layer_string",layer_string); */
/* 	run; */

	data _null_;
		set input_grid;
		call execute ('%NRSTR (%do_training(&hour_pred, &anno_pr, &mese_pr,'||id||', '||approccio||', '||target||', 
					'||pred_horizon||', '||sottomodello||','||modello||','||id_modello||', '||id_rete||', '||horizon||',
					 '||flg_mesi_omologhi||', '||flg_old_dafne||', 
					'||mesi_omologhi||',  '||mesi_val||', '||mesi_train||', '||varreduce_int_vars||', 
					'||varreduce_class_vars||', '||flg_drop_vars||', 
					'||drop_vars||', '||flg_forced_vars||', '||forced_int_vars||', '||forced_class_vars||', 
					'||flg_maxeffects||', '||maxeffects||', 
					'||varexp||', '||flg_std_vars||', '||std_vars||',  '||algo_nn||', 
					'||maxiter_nn||', '||regl1||', '||regl2||', 
					'||layer_string||'))');
	run;


%mend execute_configs;

/*0. Filtro per le colonne del modello da trainare */
%preprocessing_ds(&input_dset, &modello_totrain, work.input_ds);

/* Addestro modelli della griglia inerenti all'orizzonte di previsione in esame */
%execute_configs(); 






