%macro insert_info (dsinput, dsoutput);

LIBNAME WEBDIPS ORACLE  DB_OBJECTS=(TABLES SYNONYMS VIEWS)  PATH=WEBDICS SCHEMA=SVWEBV06  USER=SVWEBV06 PASSWORD=SVWEBV06;

%let DATA_R= %sysfunc(datetime(),datetime20.);
%put &DATA_R;

data insert ;
set &dsoutput.;
run;

proc sql;
connect to oracle as oralib ( PATH='WEBDICS' USER='SVWEBV06' PASSWORD='SVWEBV06');
    create table seq1 as select * from connection to oralib ( SELECT SBPM_RUN_TASK.nextval as seq1
from dual)
;

disconnect from oralib;
    quit; 

     

data seq;
merge seq1;

run;
proc sql;
select seq1
                 into :seq1
          from seq  ;
quit;

data insert;

if 1=2 then do;
     set &dsoutput.;
end;
K_BPM_RUN_TASK=&seq1.;
ID_FLUSSO="FASE3_PREVISIONE_VIYA";
DATE_RIF="&DATA_R."dt ;
FL_AVVIATO="0";
K_STORICO=K_BPM_RUN_TASK;
FL_AZIONE="I";
DT_AZIONE="&DATA_R."dt;
UTENTE_AZIONE="USER AUTOMATIC";
FL_DELETED="0";
output;

run;

proc append data=&dsinput. base=&dsoutput. force;
run;

%mend;

%insert_info (dsinput=insert, dsoutput=webdips.BPM_RUN_TASK);
