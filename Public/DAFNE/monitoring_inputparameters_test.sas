
/*===============================================================*/
/***** rilascio 2020-04-XX - versione v1.0 *****/
/***** autori: Binda, Gregori, Oliva, Rizzolo *****/
/***** BIP S.p.a. *****/
/*===============================================================*/

options locale='it_IT';
%let datetime_base=%sysfunc(datetime());
%let ora_incentivo=15;
%let fix_pubb = 1;

/*============ Initialization dates ========*/
data set_datetime_hour;
	dt_now=&datetime_base;
	hour_base=hour(dt_now);
	min_base=minute(dt_now);
	tday_base=datepart(dt_now);
	dt_now_fix=intnx("dthour",dt_now,0, 'B');
	hour_fix=hour(dt_now_fix);
	call symputx('hour',hour_fix);
	call symputx('datetime_now',dt_now_fix);
	format dt: datetime19. tday_base date9.;
run;

data dates;
	dt_now=&datetime_now;
	gday_sistema = datepart(dt_now);
	hour = (hour(dt_now));
	
	if hour<6 then gday_gas_sistema=intnx("day",gday_sistema,-1);
	else gday_gas_sistema=gday_sistema;

	g1day_gas_sistema=intnx("day",gday_gas_sistema,1);
	g2day_gas_sistema=intnx("day",gday_gas_sistema,2);
	g3day_gas_sistema=intnx("day",gday_gas_sistema,3);
	g4day_gas_sistema=intnx("day",gday_gas_sistema,4);

	gm1day_gas_sistema=intnx("day",gday_gas_sistema,-1);
	gm2day_gas_sistema=intnx("day",gday_gas_sistema,-2);

	/* Ora incentivo e pre-incentivo riferite a oggi */
	if ((nwkdom(5, 1, 3, year(gday_gas_sistema)) <= gday_gas_sistema and  
		gday_gas_sistema< nwkdom(5, 1, 10, year(gday_gas_sistema)))) then
		do;
			hour_pre_incentivo_tday = &ora_incentivo-3;
			hour_incentivo_tday = hour_pre_incentivo_tday+1;
		end;

	else if ((gday_gas_sistema < nwkdom(5, 1, 3, year(gday_gas_sistema)) or 
			gday_gas_sistema >= nwkdom(5, 1,10, year(gday_gas_sistema)))) then
		do;
			hour_pre_incentivo_tday = &ora_incentivo-2;
			hour_incentivo_tday =hour_pre_incentivo_tday+1;
	end;

	/* Ora incentivo e pre-incentivo riferite a ieri */
	if ((nwkdom(5, 1, 3, year(gm1day_gas_sistema)) <= gm1day_gas_sistema and  
		gm1day_gas_sistema< nwkdom(5, 1, 10, year(gm1day_gas_sistema)))) then
		do;
			hour_pre_incentivo = &ora_incentivo-3;
			hour_incentivo = hour_pre_incentivo+1;
		end;

	else if ((gm1day_gas_sistema < nwkdom(5, 1, 3, year(gm1day_gas_sistema)) or gm1day_gas_sistema >= nwkdom(5, 1,10, year(gm1day_gas_sistema)))) then
		do;
			hour_pre_incentivo = &ora_incentivo-2;
			hour_incentivo =hour_pre_incentivo+1;
	end;

	/* Ora incentivo e pre-incentivo riferite all'altro ieri */
	if ((nwkdom(5, 1, 3, year(gm2day_gas_sistema)) <= gm2day_gas_sistema and  
		gm2day_gas_sistema< nwkdom(5, 1, 10, year(gm2day_gas_sistema)))) then
		do;
			/* Se vera significa che siamo in stagione estiva */
			hour_pre_incentivo_yday2 = &ora_incentivo-3;

			*12;
			hour_incentivo_yday2 = hour_pre_incentivo+1;

			*13;
		end;
	else if ((gm2day_gas_sistema < nwkdom(5, 1, 3, year(gm2day_gas_sistema)) or gm2day_gas_sistema >= nwkdom(5, 1,10, year(gm2day_gas_sistema)))) then
		do;
			/* Se vera significa che siamo in stagione invernale */
			hour_pre_incentivo_yday2 = &ora_incentivo-2;

			*13;
			hour_incentivo_yday2 = hour_pre_incentivo+1;

			*14;
	end;

	dt_ora_legale_inizio=dhms(nwkdom(5, 1, 3, year(gday_sistema)),2,0,0);
	dt_ora_legale_fine=dhms(nwkdom(5, 1,10, year(gday_sistema)),2,0,0);

	if dt_now >= dt_ora_legale_inizio and  dt_now < dt_ora_legale_fine then
		do;
			/*	ora sistema = ora orologio - 1->  porto avanti di 1 ora l'ora di sistema (es: alle 3 d'orologio sono alle 2 di sistema)*/
			dt_orologio=intnx("dthour", dt_now, 1);
		end;
	else if dt_now < dt_ora_legale_inizio or dt_now >= dt_ora_legale_fine then
		do;
			/*	ora sistema = ora orologio->  porto avanti di 1 ora;*/
			dt_orologio=intnx("dthour", dt_now, 0);
		end;

	dt_pubblicazione = intnx("dthour", dt_orologio, 1);
	hour_pubb = (hour(dt_pubblicazione));
	gday_pubb = datepart(dt_pubblicazione);

	if hour_pubb<6 then
		gday_gas_pubb=intnx("day",gday_pubb,-1);
	else gday_gas_pubb=gday_pubb;


	if gday_gas_sistema = gday_gas_pubb then
		flag_pubb_calc=1;
	else flag_pubb_calc=0;

	if &fix_pubb=1 then do;
		gday=gday_pubb;
		gday_gas=gday_gas_pubb;
	end;
	else do;
		flag_pubb_calc=1;
	end;

	g1day_gas=intnx("day",gday_gas,1);
	g2day_gas=intnx("day",gday_gas,2);
	g3day_gas=intnx("day",gday_gas,3);
	g4day_gas=intnx("day",gday_gas,4);
	gm1day_gas=intnx("day",gday_gas,-1);
	gm2day_gas=intnx("day",gday_gas,-2);
	call symputx("hour_pubb", hour_pubb);
	call symput('hour',hour);
	call symputx("hour_inc_tday", hour_incentivo_tday);
	call symputx("hour_inc_yday", hour_incentivo);
	call symputx("hour_inc_yday2", hour_incentivo_yday2);
	call symput('gday',gday);
	call symput('gday_gas',gday_gas);
	call symput('g1day_gas', g1day_gas);
	call symput('g2day_gas', g2day_gas);
	call symput('g3day_gas', g3day_gas);
	call symput('g4day_gas', g4day_gas);
	call symput('gm1day_gas', gm1day_gas);
	call symput('gm2day_gas', gm2day_gas);
	call symput('gday_gas_sistema',gday_gas_sistema);
	call symput('g1day_gas_sistema', g1day_gas_sistema);
	call symput('gm1day_gas_sistema', gm1day_gas_sistema);
	call symputx("flag_pubb_calc", flag_pubb_calc);
	format dt: datetime19.;
	format g: date9.;
run;

/*============ Input Parameters ========*/

data vars_list;
	ora_pre_incentivo=&ora_incentivo. -1;

	if &hour_pubb=&ora_incentivo or &hour_pubb=ora_pre_incentivo then
		to_mail="'alessia.borroni@snam.it' 'Andrea.Magatti@mail-bip.com' 'Luca.Miglioli@mail-bip.com' 'Luca.Martignoni@mail-bip.com'";
	else
		to_mail="'alessia.borroni@snam.it' 'Andrea.Magatti@mail-bip.com' 'Luca.Miglioli@mail-bip.com' ";
	
	cc_mail="'carlo.binda@mail-bip.com' 'Gabriele.Oliva@mail-bip.com'
			'clio.rizzolo@mail-bip.com' 'dario.canette@snam.it' 'silvia.lameri@snam.it' 'alessandro.pascali@mail-bip.com'";
/* 	to_mail="'clio.rizzolo@mail-bip.com'"; */
/* 	cc_mail="'clio.rizzolo@mail-bip.com'"; */
	
	ds_path="&report_path";
	zip_path="&report_path";
	/* Bilancio Commerciale */
	input_bilanciosnam1='webdips.prev_bilancio';
	input_bilanciosnam='work.bilanciosnam_final';

	/* Riconsegnato Totale da SCADA */
	input_riconsegnato1='oralpbp2.riconsegnato';
	
	/* Previsioni */
	tabella_input_preds = 'webdips.prev_giornaliere';
	tabella_input_preds_viya='webdips.prev_giornaliere_viya';

	/* Input Terna */
    input_termo='oralpbp2.d_e_up_new';
    input_terna='oralpbp2.d_e_terna';
    input_up='oralpbp2.d_e_up';

	/* input remi */
    input_anag_remi='webdips.anag_remi';
    input_anag_remi_attr='webdips.anag_remi_attr';
    input_vol_val='oralpbp2.d_e_volumi_validati';
    input_sime2='oralpbp2.d_e_misure_sime_2';
    input_sime='oralpbp2.d_e_misure_sime';

	/* Model Lists */
	modelli_list_g_diana ='("Predizione G per G - DIANA")';
	modelli_list_g1='("Predizione G per G+1", "Predizione G per G+1 - 422", "Predizione G per G+1 - B", "Predizione G per G+1 - Autoreg",
	"Predizione G per G+1 - Full", "Predizione G per G+1 - Terna",  "Predizione G per G+1 - Sim")';
	modelli_list_g1_dafne='("Predizione G per G+1")';
	tipoprev_list_utenze='("Termoelettrico", "Civile", "Industriale+Residuo")';
	modelli_list_g2='("Predizione G per G+2")';
	modelli_list_g3='("Predizione G per G+3")';
	modelli_list_g4='("Predizione G per G+4")';
	modelli_list_rictot='("Predizione G per G - DIANA","Predizione G per G+1", "Predizione G per G+2",
	"Predizione G per G+3", "Predizione G per G+4")';

	/* Temp Datasets */
    output_riconsegnato='output_riconsegnato';
    output_predictions_wide='output_predictions_wide';
    output_predictions_long='output_predictions_long';
    predictions_targets_tot_long='predictions_targets_tot_long';
    predictions_targets_ut_long='predictions_targets_ut_long';
    predictions_targets_wide='predictions_targets_wide';

	/* Temp Variables */
    palette_list1='(navy CX1349BD CX2E6EB6 CX3f87bd cx5f9ebd )';
	palette_list2a ='(CXd6d7d3 CXd9534f CX373d54 CX3f87bd  CX5cb85c CX966842 CXffc425 CX5bc0de)';
	palette_list2='(CX5bc0de CXd9534f CX373d54 CX3f87bd  CX5cb85c CX966842 CXffc425 G)';
	palette_list3='(CX3f87bd cx5f9ebd )';
	palette_list4='(CX5bc0de G )';
    palette_list_ut='(CX4db449 CXb2b2b2 CXf9ff00)';
    marker_list='(CircleFilled DiamondFilled SquareFilled StarFilled TriangleFilled)';

	/* parametri per incentivi */
    start_per2 = "01OCT2017"d;
    start_per3 = "01MAR2019"d;
    end_per3 = "31DEC2021"d;
    mesi_inverno = '(11,12,1,2,3)';
    mesi_estate = '(4,5,6,7,8,9,10)';
    m_1617 = 14000*100;
    interc_old = 70000;
    mape_max = 0.1;
    m1_1718 = m_1617;
    m2_1718 = 22500*100;
    interc1_inv_1718 = interc_old;
    interc2_inv_1718 = 155000;
    interc1_est_1718 = 77000;
    interc2_est_1718 = 162000;
    mape_max_est_19 = 0.09581;
    m1_est_19 = 13127.27*100;
    interc1_est_19 = 72200;

	/* soglie  modello G */
    soglia_mae=26.432;
    soglia_salto_prev=26.432;
   
	/* storicizzazione delle pubblicazioni SNAM     */
	base_url="https://www.snam.it/repository-srg/file/it/business-servizi/dati-operativi-business/dati_operativi_bilanciamento_sistema";
/* 	store_path=compress("&report_path"); */
/* 	xls_file="pubblicazionisnam_temp.xls"; */
/* 	to_save_xls=compress(cat(store_path, compress(xls_file))); */
run;


proc contents data=vars_list out=cont (keep=name) noprint;
run;

proc sql noprint;
	select distinct name into: vars
	separated by " "
	from cont
	;
quit;

proc transpose data=vars_list out=macro_all (rename=(_name_=varname col1=value));
	var &vars;
run;

data _null_;
	set macro_all;
	varname = trim(varname);
	varname = %nrstr(varname);
	value = right(value);
	value = %nrstr(value);
	call symput(varname, value);
run;




/* Report dates */
data dates_update_change;
	length pdf_filename $30.;
	gday=&gday;
	hh=&hour;
	hh_pub=&hour_pubb;
	gday_gas=&gday_gas;
	dayg=strip(put(gday_gas, ITADFWDX17.));
	dayg1=strip(put(&g1day_gas, ITADFWDX17.));
	dayg2=strip(put(&g2day_gas, ITADFWDX17.));
	dayg3=strip(put(&g3day_gas, ITADFWDX17.));
	dayg4=strip(put(&g4day_gas, ITADFWDX17.));
	daygm1=strip(put(&gm1day_gas, ITADFWDX17.));
	temp_date=tranwrd(put(gday_gas, yymmddp10.),".","_");;
	pdf_filename=compress(cat("Report_",temp_date,"_h", put(hh_pub,z2.0),".pdf"));
	call symput('dayg',dayg);
	call symput('dayg1', dayg1);
	call symput('dayg2', dayg2);
	call symput('dayg3', dayg3);
	call symput('dayg4', dayg4);
	call symput('daygm1',daygm1);
	call symput('pdf_filename',compress(pdf_filename));
	format g: date9.;
run;

data mymap;
	length id $40 value $40 markersymbol $40;
	infile datalines delimiter=',';
	retain id "mytest";
	input value $ markersymbol $;
	datalines;
	p_riconsegnato_tot_g4, CircleFilled
	p_riconsegnato_tot_g3, DiamondFilled
	p_riconsegnato_tot_g2, SquareFilled
	p_riconsegnato_tot_g1, StarFilled
	p_riconsegnato_tot_g_diana, TriangleFilled
;
run;

