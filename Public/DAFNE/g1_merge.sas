
/*===============================================================*/
/***** rilascio 2020-09-XX - versione v1.0 *****/
/***** autori:  *****/
/***** BIP S.p.a. *****/
/*===============================================================*/

/* options nomlogic nomprint nosymbolgen; */
/* options mlogic mprint symbolgen msglevel=i; */
/* ods exclude all; */
/*ods exclude none;*/

/*============ Macro variabili =================================*/
/* Definizione macro variabili Globali */
%let dt_solare=&start;
%let libreria_input = &lib_physical;
%let dset_input_name = g1_previsioni_riconsegnato; 
%let dataset_input = &libreria_input..&dset_input_name;
%let libreria_output = &lib_physical;
%let dset_output_name = g1_output_models_viya; 
%let dataset_output = &libreria_output..&dset_output_name;
/* %let ensemble_id = 9 10 11 12 41 42 43 44 45 46; */
%let g_finalpred_id = &ensemble_id;


%macro prev();
	/* controllo l'esistenza della tabella finale (output_models_viya), in caso contrario la definisco */
	%if not(%sysfunc(exist(&dataset_output))) %then
		%do;

			data &dataset_output;
				stop;
				length DT_CALCOLO 8 DT_RIFERIMENTO_DATI 8 PREV 8 UNITA_MISURA $ 30 Tipologia_Previsione $ 30 MODELLO $ 30;
				format DT_CALCOLO datetime19. DT_RIFERIMENTO_DATI datetime19.;
			run;

	%end;

	/* prendo ultima riga di previsione e la appendo al ds già esistente */
	%if %sysfunc(exist(&dataset_input)) %then
		%do;

		proc sort data=&dataset_input;
			by datetime_solare;
		run;

		data time;
			set &dataset_input end=eof;
			if eof then do;
				call symputx("last_hour",compress(put(hour,z2.)));
				call symputx("last_dt_solare",datetime_solare);
			end;
			if eof then output;
		run;

		data last_prediction;
			set &dataset_input (where=(id in (&g_finalpred_id) and hour=&last_hour
						and datetime_solare=&last_dt_solare));
		run;

		data preds_topub(keep= PREV DT_RIFERIMENTO_DATI unita_misura Tipologia_previsione
								modello dt_calcolo);
			length DT_CALCOLO 8 DT_RIFERIMENTO_DATI 8 PREV 8 UNITA_MISURA $ 30 
					Tipologia_Previsione $ 30 MODELLO $ 30;
			set last_prediction (rename=(previsione=PREV));
			dt_calcolo = datetime();
			DT_RIFERIMENTO_DATI = dhms(data_gas_rif, 0, 0, 0);
			UNITA_MISURA = 'KWH';
			if id_target = "IND" then Tipologia_Previsione = 'Industriale';
			if id_target = "INDR" then Tipologia_Previsione = 'Industriale+Residuo';
			if id_target = "CIV" then Tipologia_Previsione = 'Civile';
			if id_target = "TERMO" then Tipologia_Previsione = 'Termoelettrico';
			if id_target = "TOT" then Tipologia_Previsione = 'RiconsTot';

			if pred_horizon="G" then modello = 'Predizione G per G';
			else if pred_horizon="G1" then do;
				if id=35 then modello="Predizione G per G+1 - Autoreg";
				else if id=36 then modello="Predizione G per G+1 - Terna";
				else if id=37 then modello="Predizione G per G+1 - Sim";
				else if id=38 then modello="Predizione G per G+1 - 422";
				else if id=39 then modello="Predizione G per G+1 - Full";
				else if id=40 then modello="Predizione G per G+1 - B";
				else modello="Predizione G per G+1";
			end;

			else if pred_horizon="G2" then modello="Predizione G per G+2";
			else if pred_horizon="G3" then modello="Predizione G per G+3";
			else if pred_horizon="G4" then modello="Predizione G per G+4";
			format dt: datetime19.;
		run;

		proc sort data=preds_topub;
			by DT_CALCOLO DT_RIFERIMENTO_DATI MODELLO Tipologia_Previsione;
		run;

		proc append base=&dataset_output data=preds_topub force;
		run;

	%end;

%mend;

%prev();
