



/* =================================================================== */
/* ------------------------ Functions ------------------------- */
/* =================================================================== */

/*viene calcolata la variabile target, quindi il Riconsegnato totale, secondo la formula Totale Immesso - DLP - Uscite 
Va fatta una precisazione sul DLP: corrisponde al valore sul Bilancio se il Bilancio è validato, altrimenti corrisponde
al DLP del dispacciamento convertito in KWh utilizzando il PCS di Misura. Se non dovesse esserci neanche il DLP
del disp (tabella analisi_dlp) uso un terzo valore in Kwh (preso da analisi_bilancio)
Inoltre, poichè il DLP appare sul Bilancio da metà 2016, lo storico è stato ricostruito con DLP 
del disp per PCS di misura. */
%macro utenze_input_target_civ(output);

	data c_target_civile;
		set &input_bilanciosnam(keep=data_gas Reti_di_distribuzione 
								where=(data_gas>=&start_cut_date_prev));
		Reti_di_distribuzione=Reti_di_distribuzione*1000000;
		rename Reti_di_distribuzione = Riconsegnato_civile_kwh;
	run;

	data &output;
		set c_target_civile;
	run;

%mend utenze_input_target_civ;

%macro UTENZE_input_target_termo(output);

	data t_remi_terna(drop=REMI);
		set &input_termo;
		codice_remi = REMI*1;
		terna=1;
	run;

	Data ANAG_REMI;
		set &input_anag_remi (where=(compress(id_remi, "1234567890")="" ));
		codice_remi=input(id_remi,best32.);

		if fl_deleted=0;
	run;

	proc sort data=anag_remi;
		by codice_remi;
	run;

	data anag_remi1;
		merge anag_remi t_remi_terna(in=p);
		by codice_remi;

		if p;
	run;

	Data ANAG_REMI_ATTR;
		set &input_anag_remi_attr; /*Attributi in decorrenza*/
	run;

	data t_anag_remi_filter(where=(FL_DELETED="0"));
		set ANAG_REMI_ATTR(where=(nome_attr="cod_macro_set" or nome_attr="stato_tecnico" or nome_attr="codice_ateco") Rename=FK_ANAG_REMI=K_ANAG_REMI);

		if dt_fine_dec>dhms(intnx("year", &gday, 2),1,0,0);
	run;

	proc sort data=t_anag_remi_filter out=t_anag_remi_filter nodupkey;
		by k_anag_remi nome_attr;
	run;

	proc transpose data=t_anag_remi_filter out=t_anag_remi_filter_t(drop=_name_ _label_);
		by k_anag_remi;
		id nome_attr;
		var val_attr;
	run;

	proc sort data=ANAG_REMI1 out=t_ANAG_REMI_sort nodupkey;
		by k_anag_remi;
	run;

	data t_ANAG_REMI_BIP;
		merge t_ANAG_REMI_sort(in=p) t_anag_remi_filter_t;
		by K_ANAG_REMI;

		if p;
		codice_remi=id_remi*1;
		keep codice_remi id_remi codice_ateco cod_macro_set stato_tecnico;
	run;

	proc sort data=t_ANAG_REMI_BIP out=t_ANAG_REMI_BIP_sort;
		by id_remi;
	run;

	data t_remi_termo_2;
		set t_ANAG_REMI_BIP_sort;
		keep id_remi codice_remi cod_macro_set stato_tecnico;
	run;

	data filter;
		if day(date())<=20 then
			filter=intnx("month", date(),-2, "END");
		else filter=intnx("month", date(),-1, "END");
		call symput("remi_limit", filter);
		format filter date9.;
	run;

	/* PRENDO I VOLUMI */
	data t_volumi_sime2(rename =(codice_punto=codice_remi_char));
		set &input_sime2( where= (datepart(giorno_gas)>=&start_cut_date_prev and datepart(giorno_gas)<=&remi_limit
			and k_d_e_misure_sime>0) keep= k_d_e_misure_sime giorno_gas codice_punto pcs_25_15 volume_std );
		volume_kwh=volume_std*pcs_25_15;
		drop k_d_e_misure_sime;
		codice_remi=input( codice_punto,8.);
	run;

	data t_REMI_validati_final_sort;
		rename giorno_gas=data_riferimento;
		label  giorno_gas="data_riferimento";
		set t_volumi_sime2;
	run;

	proc sort data=t_REMI_validati_final_sort out=t_REMI_validati_final_sort;
		by codice_remi;
	run;

	/*============================*/
	/* prendo solo i REMI Termo di Terna */
	data t_confronto_remi_new(rename= (pcs_25_15=pcs_validato volume_std=volume_smc));
		merge t_remi_termo_2(in=p) t_REMI_validati_final_sort(in=q);
		by codice_remi;

		if p and q;
		giorno_gas=datepart(data_riferimento);
		format giorno_Gas date9.;
	run;

	data t_remi_termo_target(rename=pcs_validato=pcs_val);
		set t_confronto_remi_new;
		keep giorno_gas volume_smc pcs_validato codice_remi volume_kwh_val;
		volume_kwh_val=volume_smc*pcs_validato;
	run;

	proc sql noprint;
		CREATE TABLE WORK.t_target_termo_giorn AS 
			SELECT t1.giorno_gas, 
				/* SUM_of_volume_kwh_val */
		(SUM(t1.volume_kwh_val)) AS riconsegnato_termo_kwh FROM WORK.t_remi_termo_target t1 WHERE t1.pcs_val NOT IS MISSING GROUP BY t1.giorno_gas;
	QUIT;

	data t_target_termo2(rename=data_gas=giorno_gas);
		set &input_bilanciosnam(keep=data_gas Termoelettrico where=(data_gas>=&start_cut_date_prev));
		Termoelettrico=Termoelettrico*1000000;
	run;

	data &output(rename=(giorno_gas=data_gas ) drop=riconsegnato_termo_comm_kwh);
		merge t_target_termo_giorn t_target_termo2(rename=(Termoelettrico=riconsegnato_termo_comm_kwh));
		by giorno_gas;
	run;

%mend UTENZE_input_target_termo;

/* function to create target */
%macro create_riconsegnato(output);

	/**********************************/
	/* Riconseganto da Bilancio Commerciale: 
		Prendo da webdips.prev_bilancio e calcolo 
		riconsegnato_tradizionale = Totale_Immesso - uscite - delta_line_pack_srg
		riconsegnato_autorita = Ric_Reti_terzi (riconsegnato ad altre reti terzi) + Riconsegne_rete_Snam_Rete_Gas */

	data &input_bilanciosnam(rename=(date=data_gas));
		set &input_bilanciosnam1(where=(fl_deleted eq "0" and dt_azione<= &datetime_now and datepart(data)<&gday_gas_sistema));
		drop k_storico data_elaborazione fl_deleted k_prev_bilancio fl_azione utente_azione dt_azione data;
		date=datepart(data);
		if (validato eq .) then validato = 0;
		format date date9.;
	run;

	/* Check su ultimo consuntivo disponibile per:
		- prendere solo AT corrente
		- decidere se spegnere la reportistica sul vecchio modello G
	*/
	%get_atcorrente(&input_bilanciosnam);

	proc sort data=&input_bilanciosnam out=&input_bilanciosnam;
		by data_gas;
	run;

	data bilancio_snam;
		set &input_bilanciosnam(keep = data_gas Importazioni Entrata_Tarvisio	Entrata_Gela Entrata_Gorizia Entrata_Mazara	Entrata_P_Gries	
			GNL_Cavarzere	GNL_Livorno	GNL_Panigaglia	Produzione_Nazionale Sistemi_di_stoccaggio Stogit Edison_Stoccaggio
			Totale_Immesso Riconsegne_rete_Snam_Rete_Gas Industriale Termoelettrico Reti_di_distribuzione Uscita_Bizzarone
			Uscita_Gorizia	Uscita_P_Gries	Uscita_S_Marino	Uscita_Tarvisio Riconsegnato_ad_altre_Reti_tr delta_line_pack_srg
			where=(missing(delta_line_pack_srg) ne 1 and missing(Totale_Immesso) ne 1 and missing(Uscita_P_Gries) ne 1)
			rename=( Riconsegnato_ad_altre_Reti_tr=Ric_Reti_terzi));

		if Totale_Immesso>0;
		;
		uscite=sum(of uscit:);
		riconsegnato_tradizionale=Totale_Immesso-uscite-delta_line_pack_srg;
		riconsegnato_autorita=Ric_Reti_terzi+Riconsegne_rete_Snam_Rete_Gas;
	run;

	/**********************************/
	/* Riconsegnato Totale da SCADA --> da oralpbp2.riconsegnato */
	data riconsegnato_scada;
		set &input_riconsegnato1(where=(datepart(data_rif)>= &start_cut_date_prev and datepart(data_rif)< &gday_gas_sistema));
		data_gas=datepart(data_rif);
		if datepart(data_rif);
		format data_gas date9.;
		riconsegnato_scada=tot_kwh/1000000;
		keep data_gas riconsegnato_scada;
	run;

	proc sort data=riconsegnato_scada nodupkey;
		by data_gas;
	run;

	/**********************************/
	/* Riconsegnato Civile: voce Reti_di_distribuzione da Bilancio Commerciale */
	/* Riconsegnato Termoelettrico: da ORALPBP2.D_E_UP_NEW etc */
	%UTENZE_input_target_civ(riconsegnato_civile);
	%UTENZE_input_target_termo(riconsegnato_termo);

	proc datasets lib=work;
		delete c_: t_: anag:;
	run;

	data calendario_giorno;
		data_gas=&start_cut_date_prev;
		end_date=&gm1day_gas;

		do while (data_gas<=end_date);
			output;
			data_gas=intnx("day", data_gas, 1, 's');
		end;

		format data_gas date9.;
		keep data_Gas;
	run;

	/**********************************/
	/* Metto tutti insieme i target aggiungendo:
		Riconsegnato Industriale (voce Industriale da Bilancio Commerciale)
		Termoelettrico Commerciale (voce Termoelettrico da Bilancio Commerciale)
	*/
	data &output;
		merge calendario_giorno(in=q)  
			bilancio_snam(keep=data_gas riconsegnato: termoelettrico industriale 
			rename=(termoelettrico=riconsegnato_termo_comm industriale=riconsegnato_ind_comm
			riconsegnato_tradizionale=riconsegnato_tot_trad riconsegnato_autorita=riconsegnato_tot_aut)) 
			riconsegnato_scada(rename=(riconsegnato_scada=riconsegnato_tot_scada)) 	
			riconsegnato_civile riconsegnato_termo;
		by data_gas;

		if q;
		riconsegnato_termo_comm=riconsegnato_termo_comm*1000000;
		riconsegnato_ind_comm=riconsegnato_ind_comm*1000000;
		riconsegnato_tot_trad=riconsegnato_tot_trad*1000000;
		riconsegnato_tot_aut=riconsegnato_tot_aut*1000000;
		riconsegnato_tot_scada=riconsegnato_tot_scada*1000000;
		riconsegnato_industriale=riconsegnato_tot_trad-riconsegnato_civile_kwh-riconsegnato_termo_kwh;
		riconsegnato_ind_comm_fill=riconsegnato_tot_trad-riconsegnato_civile_kwh-riconsegnato_termo_comm;
	run;

	data &output;
		set &output(rename=(riconsegnato_civile_kwh=riconsegnato_civile));
		keep data_gas riconsegnato_tot_trad riconsegnato_tot_aut riconsegnato_tot_scada riconsegnato_civile
			riconsegnato_termo_fill riconsegnato_ind_fill fill_termo_ind;
		riconsegnato_termo_fill=riconsegnato_termo_kwh;
		riconsegnato_ind_fill=riconsegnato_industriale;
		fill_termo_ind=0;

		if missing(riconsegnato_termo_fill) then
			do;
				riconsegnato_termo_fill=riconsegnato_termo_comm;
				riconsegnato_ind_fill=riconsegnato_ind_comm_fill;
				fill_termo_ind=1;
			end;
	run;

%mend create_riconsegnato;


%macro get_atcorrente(ds_in);

	proc sql noprint;
		select max(data_gas) into: max_date_target
		from &ds_in
		;
	quit;

	data last_consuntivo (keep=data_gas start_cut_date_prev anno_termico am_corso);
		set &ds_in (where=(data_gas=&max_date_target));
		if month(data_gas)>=10 then do;
			ys=year(data_gas);
			anno_termico=cat("AT",put(year(data_gas), 4.0),"/", put(year(data_gas)+1, 4.0));
			start_cut_date_prev=mdy(10,1,ys);
		end;
		else do;
			ys=year(data_gas)-1;
			anno_termico=cat("AT",put(year(data_gas)-1, 4.0),"/", put(year(data_gas), 4.0));
			start_cut_date_prev=mdy(10,1,ys);
		end;
		if month(data_gas) in (10 11 12) then am_corso=cat(ys,"-",put(month(data_gas),z2.0));
		else am_corso=cat(ys+1,"-",put(month(data_gas),z2.0));
		format data_gas start_cut_date_prev date9.;
	run;

	data _null_;
		set last_consuntivo;
		call symput("am_corso",am_corso);
		call symput("at_corrente",anno_termico);
		call symput("start_cut_date_prev",start_cut_date_prev);
	run;

	/* Prendo solo AT corrente */
	data &ds_in;
		set &ds_in (where=(data_gas ge &start_cut_date_prev));
	run;

%mend get_atcorrente;


%macro fix_predictions(output);

	data &output;
		set &output;

		if data_gas_rif eq "28FEB2019"d and hour(Dt_calcolo)=14 and modello="Predizione G per G+1" then
			prev=2854888714;

		if data_gas_rif eq "04MAR2019"d and hour(Dt_calcolo)=14 and modello="Predizione G per G+1" then
			prev=2633622540;
		if data_gas_rif eq "16OCT2019"d and hour(Dt_calcolo)=13 and modello="Predizione G per G+1" then
			prev=1899259528;
	run;

	data add_missing_prev;
		length prev dt_calcolo dt_riferimento_dati data_gas_rif 8 tipologia_previsione $30 modello $30;
		dt_calcolo="12MAR2020:13:00:00"dt;
		prev=2357419281.4;
		dt_riferimento_dati="12MAR2020:00:00:00"dt;
		modello="Predizione G per G - DIANA";
		tipologia_previsione="RiconsTot";
		data_gas_rif=datepart(dt_riferimento_dati);
		format data_gas_rif date9. dt: datetime19.;
	run;

	data &output;
		set &output add_missing_prev;
	run;

	proc sort data=&output;
		by dt_calcolo dt_riferimento_dati tipologia_previsione modello;
	run;

%mend;
%macro calculate_incentivo(ds_in=, ds_in_pub=, ds_out=);

/*%let ds_in=predictions_targets_wide;*/
/*%let ds_in_pub=for_svil.pubblicazioni_snam;*/
/*%let ds_out=preds_target_tot_inc_perf;*/

	data preds_target_tot_inc;
		set &ds_in(where=(data_gas_rif <=&gm1day_gas) 
			keep= ric: p_riconsegnato_civi_g1 p_riconsegnato_term_g1 p_riconsegnato_tot_g1: hour: data_Gas_rif);

		data_calcolo=ifn(hour_calcolo<6,data_gas_rif,data_gas_rif-1);
		dt_calcolo=dhms(data_calcolo,hour_calcolo,0,0);

		/* ESTATE --> incentivo=13, preincentivo=12 */
		if ((nwkdom(5, 1, 3, year(data_calcolo)) <= data_calcolo and  
			data_calcolo< nwkdom(5, 1, 10, year(data_calcolo)))) then
			do;
				hour_pre_incentivo = &ora_incentivo-3;
				hour_incentivo = hour_pre_incentivo+1;

		end;

		/* INVERNO --> incentivo=14, preincentivo=13 */
		else if ((data_calcolo < nwkdom(5, 1, 3, year(data_calcolo)) or data_calcolo >= nwkdom(5, 1,10, year(data_calcolo)))) then
			do;
				hour_pre_incentivo = &ora_incentivo-2;
				hour_incentivo =hour_pre_incentivo+1;
			end;

		if hour_calcolo=hour_incentivo;
		format dt: datetime19. data: date9.;
	run;

	proc sql;
		create table preds_target_tot_inc as 
		select a.*
			 ,b.g1 as p_riconsegnato_tot_g1_pub
		from preds_target_tot_inc as a
		left join &ds_in_pub as b 
		on a.data_gas_rif=b.data_gas_rif
		;
	quit;

	data &ds_out(drop=i);
		set preds_target_tot_inc;
		array ric{3} riconsegnato_tot_trad riconsegnato_tot_aut	riconsegnato_tot_scada;
		* Mape e Incentivo sulle previsioni ;
		array mape{3} mape_tradizionale	   mape_autorita        mape_scada;
		array incentivo{3} incentivo_tradizionale incentivo_autorita incentivo_scada;
		* Mape e Incentivo su dati pubblicati;
		array mape_pub{3} mape_tradizionale_pub mape_autorita_pub mape_scada_pub;
		array incentivo_pub{3} incentivo_tradizionale_pub incentivo_autorita_pub incentivo_scada_pub;

		anno_mese=cat(year(data_gas_rif),"-",put(month(data_gas_rif),z2.0));

		do i = 1 to dim(ric);
			mape[i]=abs(ric[i]-p_riconsegnato_tot_g1)/ric[i];
			mape_pub[i]=abs(ric[i]-p_riconsegnato_tot_g1_pub)/ric[i];

			/* 1° periodo di incentivazione: prima del 01OCT2017 escluso -- DELIBERA AT16/17 
			    start_per2 = "01OCT2017"d
			    interc_old = 70000
				m_1617 = 14000*100
			*/

			if data_gas_rif<&start_per2 then do;
				incentivo[i] = &interc_old-mape[i]*&m_1617;
				incentivo_pub[i] = &interc_old-mape_pub[i]*&m_1617;
				periodo_inc=1;
			end;
			
			/* 2° periodo di incentivazione: [01OCT2017, 01MAR2019) -- DELIBERA AT17/18+AT18/19 fino Feb19

			   m1_1718=14000*100
			   mape_max = 0.1
			   m2_1718=22500*100

			   mesi_inverno=(11,12,1,2,3)
			   interc1_inv_1718=70000
			   interc2_inv_1718=155000

				mesi_estate = (4,5,6,7,8,9,10)
				interc1_est_1718

			*/
			else if data_gas_rif>=&start_per2 and data_gas_rif<&start_per3 then do;
				periodo_inc=2;
				/* Inverno */
				if month(data_gas_rif) in &mesi_inverno then do;
					incentivo[i] = &interc1_inv_1718-mape[i]*&m1_1718;
					incentivo_pub[i] = &interc1_inv_1718-mape_pub[i]*&m1_1718;

					if mape[i] >=&mape_max then incentivo[i] = -&m2_1718*mape[i] +&interc2_inv_1718;
					if mape_pub[i] >=&mape_max then incentivo_pub[i] = -&m2_1718*mape_pub[i] +&interc2_inv_1718;
				end;
				/* Estate */
				if month(data_gas_rif) in &mesi_estate then do;
					incentivo[i] = -&m1_1718*mape[i] +&interc1_est_1718;
					incentivo_pub[i] = -&m1_1718*mape_pub[i] +&interc1_est_1718;

					if mape[i] >=&mape_max then
								incentivo[i] = -&m2_1718*mape[i] +&interc2_est_1718;
					if mape_pub[i] >=&mape_max then
								incentivo_pub[i] = -&m2_1718*mape_pub[i] +&interc2_est_1718;
				end;
			end;

			/* 3° periodo di incentivo: [01MAR2019, 31DEC2021] -- DELIBERA DA MAR19 
				cambia solo per i mesi estivi (per i mesi invernali resta uguale al 2° incentivo)
				m1_est_19 = 13127.27*100
				interc1_est_19 = 72200
				mape_max_est_19= 0.09581
			*/
			else if data_gas_rif>=&start_per3 and data_gas_rif<=&end_per3 then do;
				periodo_inc=3;
				/* Inverno */
				if month(data_gas_rif) in &mesi_inverno then do;
					incentivo[i] = &interc1_inv_1718-mape[i]*&m1_1718;
					incentivo_pub[i] = &interc1_inv_1718-mape_pub[i]*&m1_1718;

					if mape[i] >=&mape_max then
								incentivo[i] = -&m2_1718*mape[i] +&interc2_inv_1718;
				
					if mape_pub[i] >=&mape_max then
								incentivo_pub[i] = -&m2_1718*mape_pub[i] +&interc2_inv_1718;
				end;
				/* Estate */
				if month(data_gas_rif) in &mesi_estate then do;
					incentivo[i] = -&m1_est_19*mape[i] +&interc1_est_19;
					incentivo_pub[i] = -&m1_est_19*mape_pub[i] +&interc1_est_19;

					if mape[i] >=&mape_max_est_19 then
								incentivo[i] = -&m2_1718*mape[i] +&interc2_est_1718;
					if mape_pub[i] >=&mape_max_est_19 then
								incentivo_pub[i] = -&m2_1718*mape_pub[i] +&interc2_est_1718;
				end;
			end;
		end;

		/* PER I PERIODI 1° e 2° di INCENTIVI: si basa su RICONSEGNATO AUTORITA'
		   PER IL TERZO PERIODO DI INCENTIVAZIONE: mi baso su RICONSEGNATO TRADIZIONALE 
		*/
		if periodo_inc<3 then do;
				riconsegnato_valutato=riconsegnato_tot_aut;
				MAPE_valutato=mape_autorita;
				Incentivo_valutato=incentivo_autorita;
				MAPE_valutato_pub=mape_autorita_pub;
				Incentivo_valutato_pub=incentivo_autorita_pub;
		end;
		else if periodo_inc=3 then do;
				riconsegnato_valutato=riconsegnato_tot_trad;
				MAPE_valutato=mape_tradizionale;
				Incentivo_valutato=incentivo_tradizionale;
				MAPE_valutato_pub=mape_tradizionale_pub;
				Incentivo_valutato_pub=incentivo_tradizionale_pub;
		end;

		/* creo var anno_termico =AT solita */
		if month(data_gas_rif)>=10 then
			anno_termico=cat("AT",put(year(data_gas_rif), 4.0),"/", put(year(data_gas_rif)+1, 4.0));
		else anno_termico=cat("AT",put(year(data_gas_rif)-1, 4.0),"/", put(year(data_gas_rif), 4.0));
		format mape: percent8.1 incenti: comma9.2;
	run;

%mend calculate_incentivo;

%macro get_predictions(preds_in_dafne=, preds_in_diana=, output_wide=, output_long=);

/*%let preds_in_dafne=&tabella_input_preds;*/
/*%let preds_in_diana=&tabella_input_preds_viya;*/
/*%let output_wide=&output_predictions_wide;*/
/*%let output_long=&output_predictions_long;*/

	/* Prev_giornaliere --> Previsioni modelli G+1, G+2, G+3 e G+4 */
	data pubblicazioni_real;
		set &tabella_input_preds(where= (datepart(dt_riferimento_dati)>=&start_cut_date_prev) 
								keep= dt_calcolo dt_riferimento_dati prev tipologia_previsione modello);
		if prev<0 or (prev<650000000 and Tipologia_Previsione="RiconsTot") then delete;
		if modello="Predizione G per G" then delete;
		if modello="Predizione G per G+1 - Autoreg" and dt_calcolo="16Jan2019:19:00:00"dt and
				dt_riferimento_dati="18JAN2019:00:00:00"dt then delete;
		dt_calcolo=intnx('hour',dt_calcolo, 0, 'B');
	run;

	/* Prev_giornaliere_viya --> Previsioni modello G DIANA AT corrente */
	data previsioni_diana;
		set &tabella_input_preds_viya ( where=(datepart(dt_riferimento_dati)>=&start_cut_date_prev
											and tipologia_previsione="RiconsTot")
										keep=tipologia_previsione modello dt_riferimento_dati 
										dt_calcolo prev) ;
		dt_calcolo=intnx('hour',dt_calcolo, 0, 'B');
		if prev ne .;
		modello="Predizione G per G - DIANA";
	run;

	/* Unisco le previsioni */
	data pubblicazione_merge;
		set pubblicazioni_real previsioni_diana;
		data_gas_rif=datepart(dt_riferimento_dati);
		format data_gas_rif date9.;
	run;

	proc sort data=pubblicazione_merge out=pubblicazione_all nodupkey;
		by dt_calcolo dt_riferimento_dati tipologia_previsione modello;
	run;

	/* Aggiungo eventuali previsioni vecchie mancanti */
	%fix_predictions(pubblicazione_all);

	data &output_long(drop=a);
		set pubblicazione_all (where=(dt_calcolo <= &datetime_now and 
									datepart(dt_riferimento_dati)>=&start_cut_date_prev ));
		a=compress(tranwrd(modello,"+",''));

		/* se tipologia_previsione in ("Termoelettrico", "Civile", "Industriale+Residuo")*/
		if Tipologia_Previsione in &tipoprev_list_utenze then
			key=cat("P_riconsegnato_", substr(tipologia_previsione,1,4), "_g1");
		/* se modello in ("Predizione G per G") */
/*		else if modello in &modelli_list_g then*/
/*			key=cat("P_riconsegnato_tot_g");*/
		/* se modello in ("Predizione G per G+1", "Predizione G per G+1 - 422", "Predizione G per G+1 - B", 
							"Predizione G per G+1 - Autoreg",
	                      "Predizione G per G+1 - Full", "Predizione G per G+1 - Terna",  "Predizione G per G+1 - Sim")
		 e non in ("Predizione G per G+1") */
		else if (modello in &modelli_list_g1 and modello not in &modelli_list_g1_dafne) then
			key=cat("P_riconsegnato_tot_g1_", substr(a,index(a,'-')+1));
		else if modello in &modelli_list_g_diana then key="P_riconsegnato_tot_g_diana";
		else key=cat("P_riconsegnato_tot_g", substr(a,length(a),1));
		key=lowcase(key);

		hour_gas=hour(dt_calcolo)-6;

		if hour_gas<0 then hour_gas=hour_gas+24;
		dt_rif_hourgas=dhms(data_Gas_rif,hour_gas,0,0);
		format dt_rif_hourgas datetime19.;
	run;

	proc sort data=&output_long out=pubblicazione_all_sort nodupkey dupout=bad;
		by dt_rif_hourgas key;
	run;

	proc transpose data=pubblicazione_all_sort out=pubb_output_all(drop=_name_ _label_);
		id key;
		by dt_rif_hourgas;
		var prev;
	run;

	proc sql noprint;
		select datepart(max(dt_riferimento_dati)),
				datepart(min(dt_riferimento_dati))
			into : max_datecalc_prev, : min_datecalc_prev
				from pubblicazione_merge;
	run;

	data calendario_datetime;
		data_gas=&min_datecalc_prev;
		end_date=&max_datecalc_prev;

		do while (data_gas<=end_date);
			do hour_gas=0 to 23;
				dt_rif_hourgas=dhms(data_gas,hour_gas,0,0);
				output;
			end;

			data_gas=intnx("day", data_gas, 1, 's');
		end;

		format data_gas data_solare date9. dt_rif_hourgas datetime19.;
		keep dt_rif_hourgas;
	run;

	data &output_wide(where=(data_gas_rif>=&start_cut_date_prev));
		length dt_rif_hourgas data_Gas_rif hour_gas hour_calcolo 
			p_riconsegnato_tot_g_diana
			p_riconsegnato_tot_g1
			p_riconsegnato_tot_g1_422
			p_riconsegnato_tot_g1_autoreg
			p_riconsegnato_tot_g1_b
			p_riconsegnato_tot_g1_full
			p_riconsegnato_tot_g1_sim
			p_riconsegnato_tot_g1_terna
			p_riconsegnato_civi_g1
			p_riconsegnato_indu_g1
			p_riconsegnato_term_g1
			p_riconsegnato_tot_g2
			p_riconsegnato_tot_g3
			p_riconsegnato_tot_g4 8;
		merge pubb_output_all calendario_datetime;
		by dt_rif_hourgas;
		data_Gas_rif=datepart(dt_rif_hourgas);
		hour_gas=hour(dt_rif_hourgas);
		hour_calcolo=ifn(hour_gas+6>23,hour_gas+6-24,hour_gas+6);
		format dt_rif_hourgas datetime19. data_Gas_rif date9.;
	run;

	proc datasets lib=work noprint;
		delete pubb:;
	run;

%mend get_predictions;

%macro ffill_rows_group(ds,target);

/*%let ds=&output_predictions_long;*/
/*%let target=targets;*/

	proc sql;
		create table modelli as 
		select distinct modello
			  , tipologia_previsione
			  , key
		from &ds
		;
	quit;

	proc sql;
		create table modelli as 
		select distinct modello
			  , tipologia_previsione
			  , key
		from &ds
		;
	quit;

	proc sql;
		select min(data_gas_rif)
			 , max(data_gas_rif)
				into: min_data_gas, :max_data_gas
		from &ds
		;
	quit;

	data calendario_long (drop=dt_calcolo);
		data_gas_rif=&min_data_gas;

		do while (data_gas_rif<=&max_data_gas);
			do hour_gas=0 to 23;
				dt_rif_hourgas=dhms(data_gas_rif,hour_gas,0,0);
				dt_riferimento_dati=intnx("dtday",dt_rif_hourgas,0,"b");
				output;
			end;
			data_gas_rif=intnx("day", data_gas_rif, 1, 's');
		end;

		format data_gas_rif date9. dt_calcolo dt_riferimento_dati dt_rif_hourgas datetime19.;
	run;

	proc sql;
		create table maschera_long as 
		select *
		from calendario_long 
		cross join modelli
		order by key, dt_rif_hourgas
		;
	quit;

	data maschera_long (drop=dt_calcolo_gas);
		set maschera_long;
		if index(modello,"Predizione G per G+1") 
			then dt_calcolo_gas=intnx("dtday",dt_rif_hourgas,-1,"s");
		else if index(modello,"Predizione G per G+2") 
			then dt_calcolo_gas=intnx("dtday",dt_rif_hourgas,-2,"s");
		else if index(modello,"Predizione G per G+3") 
			then dt_calcolo_gas=intnx("dtday",dt_rif_hourgas,-3,"s");
		else if index(modello,"Predizione G per G+4") 
			then dt_calcolo_gas=intnx("dtday",dt_rif_hourgas,-4,"s");
		else if modello in ("Predizione G per G - DIANA") 
			then dt_calcolo_gas=intnx("dtday",dt_rif_hourgas,0,"s");
		dt_calcolo=intnx("hour",dt_calcolo_gas,6,"s");
		format dt_calcolo datetime19.;
	run;

	/* Aggancio previsioni alla maschera */
	proc sql;
		create table prev_long_fill as 
		select a.*
			 , b.prev
		from maschera_long as a
		left join &ds as b
		on a.key=b.key and a.dt_rif_hourgas=b.dt_rif_hourgas
		;
	quit;

	/* Check su n. di giorni ensemble completo --> prendo modello full ora da incentivo */
	data filter_inc;
		set prev_long_fill(where=(data_gas_rif <=&gm1day_gas
			and key="p_riconsegnato_tot_g1_full") 
			keep= hour: data_gas_rif prev key dt_calcolo);
		data_calcolo=datepart(dt_calcolo);
		hour_calcolo=hour(dt_calcolo);
		/* orario da incentivo stagione estiva */
		if ((nwkdom(5, 1, 3, year(data_calcolo)) <= data_calcolo and  
			data_calcolo< nwkdom(5, 1, 10, year(data_calcolo)))) then
			do;
				hour_pre_incentivo = &ora_incentivo-3;
				*12;
				hour_incentivo = hour_pre_incentivo+1;
				*13;
			end;
		/* orario da incentivo stagione invernale */
		else if ((data_calcolo < nwkdom(5, 1, 3, year(data_calcolo)) or data_calcolo >= nwkdom(5, 1,10, year(data_calcolo)))) then
			do;
				hour_pre_incentivo = &ora_incentivo-2;
				*13;
				hour_incentivo =hour_pre_incentivo+1;
				*14;
			end;

		if hour_calcolo=hour_incentivo;
		format data: date9.;
	run;

	proc sql;
		create table filter_inc (where=(riconsegnato_tot_trad ne .)) as 
		select a.*
			 , b.riconsegnato_tot_trad
		from filter_inc as a
		left join &target as b 
		on a.data_gas_rif = b.data_gas_rif
		;
	quit;

	proc sql;
		select count(*) into: n_giorni_full
		from filter_inc (where = (prev ne .))
		;
	quit;

	%put &n_giorni_full;
	/* fffill delle previsioni mancanti per key */

	data &ds;
		set prev_long_fill;
		by key;
		retain prev_new;
		if first.key then prev_new=prev;
		else do;
			if prev ne . then prev_new=prev;
			prev=prev_new;
		end;
		drop prev_new;
	run;

	proc sql noprint;
		drop table prev_long_fill
			, maschera_long
			, calendario_long
			, modelli
			,filter_inc
		;
	quit;

%mend ffill_rows_group;

%macro ffill_col(ds);

	proc sql noprint;
		select compress(cat("t_", name))
			into :droplist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="&ds"
						and lowcase(name) like "p_%";
	quit;

	proc sql noprint;
		select name
			into :savelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="&ds"
						and lowcase(name) like "p_%";
	quit;

	data &ds(drop=a b i j &droplist);
		set	&ds;
		array new &droplist;
		array old &savelist;
		retain new;
		a=dim(new);
		b=dim(old);

		if _N_=1 then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
					old(j)=new(j);
				end;
			end;
	run;

%mend ffill_col;


%macro get_predictions_target();

	/* Calcolo i target: Riconsegnato tot tradizionale e autorità da Bilancio Commerciale, Riconsegnato Scada, Utenze
	   riferito all' AT in corso */
	%create_riconsegnato(&output_riconsegnato); 
	proc sort data=&output_riconsegnato out=targets(Rename=(data_Gas=data_Gas_rif));
		by data_Gas;
	run;

	/* Prendo le previsioni da webdips.prev_giornaliere e for_svil.output_models per AT corrente mettendole in wide e long */
	%get_predictions(preds_in_dafne=&tabella_input_preds, preds_in_diana=&tabella_input_preds_viya, 
						output_wide=&output_predictions_wide, output_long=&output_predictions_long);

	/* Tengo preds_long con missing per valorizzare eventuali prev missing in DIANA
		(per il G+1 pescando da prev_giornaliere che fa fill a mano non ho questa evidenza
		ed è inoltre molto più raro */
	proc sort data=&output_predictions_long out=preds_long;
		by data_Gas_rif dt_calcolo key;
	run;

	data predictions_targets_tot_long_mis(where=(data_gas_rif<=&g4day_gas));
		merge preds_long (where=(tipologia_previsione="RiconsTot") in=p) targets(keep=data_Gas_rif riconsegnato_tot:);
		by data_Gas_rif;
		if p;
	run;

	/* Ffill per le previsioni mancanti (sia sul ds long sia sul ds wide) */
	%ffill_col(&output_predictions_wide);
	/* Controllo anche n. giorni ensemble completo */
	%ffill_rows_group(&output_predictions_long,targets);

	/* 1. Aggancio target alle previsioni wide */
	proc sort data=&output_predictions_wide out=preds_wide;
		by data_Gas_rif dt_rif_hourgas;
	run;

	data predictions_targets_wide;
		merge preds_wide targets;
		by data_Gas_rif;
	run;

	/* 2. Aggancio target alle previsioni long */

	/* 2A. Escludo il termoelettrico: prendo solo riconsegnato tot (G+1, sim, autoreg, etc) */
	proc sort data=&output_predictions_long out=preds_long;
		by data_Gas_rif dt_calcolo key;
	run;

	data predictions_targets_tot_long(where=(data_gas_rif<=&g4day_gas));
		merge preds_long(where=(tipologia_previsione="RiconsTot") in=p) targets(keep=data_Gas_rif riconsegnato_tot:);
		by data_Gas_rif;
		if p;
	run;

	/* 2B. Recupero solo utenza termoelettrica */
	data predictions_targets_ut_long(where=(data_gas_rif<=&g4day_gas));
		merge preds_long(where=(tipologia_previsione ne "RiconsTot") in=p) targets(drop= riconsegnato_tot: fill:);
		by data_Gas_rif;

		if p;
	run;
	
	/* Calcolo incentivo */
	%calculate_incentivo(ds_in=predictions_targets_wide, ds_in_pub=for_svil.pubblicazioni_snam,
							ds_out=preds_target_tot_inc_perf);

%mend get_predictions_target;







%macro graph_g1_utenze(dataset_wide,dataset_long_ut, date);

/*%let dataset_wide=predictions_targets_wide;*/
/*%let dataset_long_ut=predictions_targets_ut_long;*/
/*%let date= &g1day_gas;*/

	data prev_names;
		date=&date;

		if  date = &gday_gas then
			add="";
		else if date = &g1day_gas then
			add="1";
		else if date = &g2day_gas then
			add="2";
		else if date = &g3day_gas then
			add="3";
		else if date = &g4day_gas then
			add="4";
		p_tot=compress(cat("p_riconsegnato_tot_g",add));
		p_civ=compress(cat("p_riconsegnato_civi_g",add));
		p_ind=compress(cat("p_riconsegnato_indu_g",add));
		p_term=compress(cat("p_riconsegnato_term_g",add));
		call symputx('delta',add);
		call symputx('p_tot',p_tot);
		call symputx('p_civ',p_civ);
		call symputx('p_ind',p_ind);
		call symputx('p_term',p_term);
	run;

	/* prendo solo previsioni calcolate oggi per il giorno date */
	data calendario_plot_date;
		data_gas=&gday_gas;

		*inizio a prevedere 4 giorni in anticipo;
		end_date=data_gas;

		do while (data_gas<=end_date);
			do hour=0 to 23;
				hour_solare=hour+6;
				data_solare=data_gas;

				if hour_solare>23 then
					do;
						hour_solare=hour_solare-24;
						data_solare=data_gas+1;
					end;

				dt_calcolo=dhms(data_solare,hour_solare,0,0);
				output;
			end;

			data_gas=intnx("day", data_gas, 1, 's');
		end;

		format dt_calcolo datetime19.;
		keep dt_calcolo;
	run;

	data dataset_utenze;
		set &dataset_long_ut(where=(data_gas_rif = &date and dt_calcolo<=&datetime_now));
		prev=prev/1000000;
		drop ric:;
	run;

	data dataset_wide_date(drop=i where=(dt_calcolo<=&datetime_now));
		set &dataset_wide(keep=data_Gas_rif dt: hour: &p_civ &p_ind &p_term &p_tot);
		n_vals=n(of p:);
		array temp &p_civ &p_ind &p_term &p_tot;

		do i = 1 to dim(temp);
			if missing(temp[i])=0 then
				temp[i]=temp[i]/1000000;
		end;

		if data_gas_rif = &date;

		if hour_calcolo>5 then
			dt_calcolo=dhms(&gday_gas,hour_calcolo,0,0);
		else dt_calcolo=dhms(&gday_gas+1,hour_calcolo,0,0);
		format dt: datetime19.;
	run;

	proc sql noprint;
		select max(n_vals) into :max_vals from dataset_wide_date;
	run;

	%if &max_vals > 0 %then
		%do;

			data param_graph;
				hour=5;
				min=55;
				day=put(&date, ITADFWDX17.);
				g4_end=dhms(&date-3,hour,min,0);
				g3_end=dhms(&date-2,hour,min,0);
				g2_end=dhms(&date-1,hour,min,0);
				g1_end=dhms(&date-0,hour,min,0);
				call symputx('day_title',day);
				call symputx('g4_end',g4_end);
				call symputx('g3_end',g3_end);
				call symputx('g2_end',g2_end);
				call symputx('g1_end',g1_end);
				date_incentivo=&date-1;

				if ((nwkdom(5, 1, 3, year(date_incentivo)) <= date_incentivo and  
					date_incentivo< nwkdom(5, 1, 10, year(date_incentivo)))) then
					do;
						hour_pre_incentivo = &ora_incentivo-3;
						hour_incentivo = hour_pre_incentivo+1;
					end;
				else if ((date_incentivo < nwkdom(5, 1, 3, year(date_incentivo)) or 
					date_incentivo >= nwkdom(5, 1,10, year(date_incentivo)))) then
					do;
						hour_pre_incentivo = &ora_incentivo-2;
						hour_incentivo = hour_pre_incentivo+1;
					end;

				call symputx('dt_inc',dhms(date_incentivo, hour_incentivo,0,0));
				format g: datetime19. date_incentivo date9.;
			run;

			data dataset_graph_ut_long;
				merge dataset_utenze calendario_plot_date(in=p);
				by dt_calcolo;
				axis_up=round(prev+150,25);

				if dt_calcolo = &dt_inc then
					p_incentivo=prev;

				if prev eq . then
					do;
						/*						prev=0;*/
						do tipologia_previsione = "Termoelettrico", "Civile", "Industriale+Residuo";
							output;
						END;
					end;
				else 	output;
				format prev comma4.0;
			run;

			data dataset_graph_ut_long;
				set dataset_graph_ut_long;
				dt_inc=&dt_inc;
				format dt_inc datetime19.;

				if &datetime_now > dt_inc then
					do;
						if hour(&datetime_now) > hour(dt_inc) then
							do;
								if dt_calcolo>= dt_inc and hour(dt_calcolo)<= 23 and hour(dt_calcolo)>= hour(dt_inc);
							end;
						else
							do;
								if dt_calcolo>= dt_inc and hour(dt_calcolo)<= 5;
							end;
					end;
				else if dt_calcolo<= dt_inc;
			run;

			/* ho totale e utenze */
			data dataset_graph_ut;
				merge dataset_wide_date calendario_plot_date(in=p);
				by dt_calcolo;
				axis_up=round(&p_tot+150,25);

				if dt_calcolo = &dt_inc then
					p_incentivo=&p_tot;
			run;

			proc sql noprint;
				select max(axis_up) into :max_ax from dataset_graph_ut;
			run;

			proc sgplot data=dataset_graph_ut_long noautolegend;
				title2 height=16pt underlin=0 bold "Previsioni del Riconsegnato per Utenza";
				xaxis grid type=time interval=hour label= "Data di Calcolo" discreteorder=data labelattrs=(size=14 weight=bold);
				yaxis grid integer label= "Riconsegnato [GWh]" values=(0 to &max_ax by 25) labelattrs=(size=14 weight=bold);
				vbar dt_calcolo / response=prev group=tipologia_previsione name="utenze" datalabel datalabelattrs=(size=12 weight=bold) datalabelpos=data seglabel SEGLABELATTRS=(size=11 weight=bold) FILL FILLTYPE=solid DATASKIN=none OUTLINE OUTLINEATTRS=(COLOR=black THICKNESS=1);
				keylegend "utenze" / AUTOITEMSIZE  valueattrs=(size=12 ) border down=1  location=outside  position=bottomleft;
				styleattrs datacolors= &palette_list_ut;
			run;

		%end;
%mend;

%macro graph_g1_tot_allmodels(dataset_wide,dataset_tot, date);

	data prev_names;
		length add $40.;
		date=&date;

		if  date = &gday_gas then
			add="Predizione G per G DIANA";
		else if date = &g1day_gas then
			add="Predizione G per G+1";
		else if date = &g2day_gas then
			add="Predizione G per G+2";
		else if date = &g3day_gas then
			add="Predizione G per G+3";
		else if date = &g4day_gas then
			add="Predizione G per G+4";
		call symput('p_tot',STRIP(add));
	run;

	data tot_long;
		set &dataset_tot(where=(index(modello,"&p_tot")>0 and tipologia_previsione="RiconsTot"
			and data_gas_rif eq &date and dt_calcolo<=&datetime_now));
	run;

	proc sql noprint;
		select count(*), count(distinct modello) into :n_preds, :dist_preds from tot_long;
	run;

	/*%put &n_preds &dist_preds ;*/
	%if &n_preds > 0 & &dist_preds >1 %then
		%do;
			/* prendo solo previsioni calcolate oggi per il giorno date */
			data calendario_plot_date;
				data_gas=&gday_gas;

				*inizio a prevedere 4 giorni in anticipo;
				end_date=data_gas;

				do while (data_gas<=end_date);
					do hour=0 to 23;
						hour_solare=hour+6;
						data_solare=data_gas;

						if hour_solare>23 then
							do;
								hour_solare=hour_solare-24;
								data_solare=data_gas+1;
							end;

						dt_calcolo=dhms(data_solare,hour_solare,0,0);
						output;
					end;

					data_gas=intnx("day", data_gas, 1, 's');
				end;

				format dt_calcolo datetime19.;
				keep dt_calcolo;
			run;

			data param_graph;
				hour=5;
				min=55;
				day=put(&date, ITADFWDX17.);
				g4_end=dhms(&date-3,hour,min,0);
				g3_end=dhms(&date-2,hour,min,0);
				g2_end=dhms(&date-1,hour,min,0);
				g1_end=dhms(&date-0,hour,min,0);
				call symputx('day_title',day);
				call symputx('g4_end',g4_end);
				call symputx('g3_end',g3_end);
				call symputx('g2_end',g2_end);
				call symputx('g1_end',g1_end);
				date_incentivo=&date-1;

				if ((nwkdom(5, 1, 3, year(date_incentivo)) <= date_incentivo and  
					date_incentivo< nwkdom(5, 1, 10, year(date_incentivo)))) then
					do;
						hour_pre_incentivo = &ora_incentivo-3;
						hour_incentivo = hour_pre_incentivo+1;
					end;
				else if ((date_incentivo < nwkdom(5, 1, 3, year(date_incentivo)) or 
					date_incentivo >= nwkdom(5, 1,10, year(date_incentivo)))) then
					do;
						hour_pre_incentivo = &ora_incentivo-2;
						hour_incentivo = hour_pre_incentivo+1;
					end;

				call symputx('dt_inc',dhms(date_incentivo, hour_incentivo,0,0));
				call symputx('hour_inc',hour_incentivo);
				format g: datetime19. date_incentivo date9.;
			run;

			data dataset_graph_sottomodelli;
				merge tot_long calendario_plot_date(in=p);
				by dt_calcolo;
				prev=prev/1000000;
				axis_up=round(prev+25,25);
				axis_down=round(prev-25,25);
				dt_inc=&dt_inc;
				format dt_inc datetime19.;

				if &datetime_now > dt_inc then
					do;
						if hour(&datetime_now) > hour(dt_inc) then
							do;
								if dt_calcolo>= dt_inc and hour(dt_calcolo)<= 23 and hour(dt_calcolo)>= hour(dt_inc);
							end;
						else
							do;
								if dt_calcolo>= dt_inc and hour(dt_calcolo)<= 5;
							end;
					end;
				else if dt_calcolo<= dt_inc;
			run;

			proc sql noprint;
				select max(axis_up), min(axis_down) into :max_ax, :min_ax from dataset_graph_sottomodelli;
			run;

			proc sgplot data=dataset_graph_sottomodelli noautolegend;
				/*				title2 height=16pt underlin=0 bold "Previsioni DAFNE riferite al giorno gas &day_title";*/
				title2 height=16pt underlin=0 bold "Previsioni DAFNE";
				xaxis grid type=time interval=hour label= "Data di Calcolo" discreteorder=data labelattrs=(size=14 weight=bold);
				yaxis grid integer label= "Riconsegnato [GWh]" values=(&min_ax to &max_ax by 25) labelattrs=(size=14 weight=bold);
				vbar dt_calcolo / name="Series" response=prev group=modello groupdisplay=cluster 
					stat=mean dataskin=matte missing clusterwidth=0.7 barwidth=1 fill;
				keylegend "Series" / AUTOITEMSIZE  valueattrs=(size=12 ) border exclude=("") down=3 location=outside  position=bottomleft;
				styleattrs datacolors= &palette_list2a;
			run;

		%end;
%mend;

%macro graph_predstday_g(dataset_wide, dataset_tot);

	data tot_long;
		set &dataset_tot(where=(modello in ("Predizione G per G+1", "Predizione G per G - DIANA") 
						and tipologia_previsione="RiconsTot"
			and data_gas_rif eq &gday_gas and dt_calcolo<=&datetime_now) drop=ric:);
		prev=prev/1000000;
		drop dt_riferimento_dati dt_rif_hourgas tipologia_previsione;
		rename prev=p_riconsegnato_pubb;
	run;

	data dataset_wide_date(where=(dt_calcolo<=&datetime_now));
		set &dataset_wide(where=(data_gas_rif = &gday_gas) keep=data_gas_rif hour: p_riconsegnato_tot_g_diana p_riconsegnato_tot_g1);
		p_riconsegnato_tot_g_diana=p_riconsegnato_tot_g_diana/1000000;
		p_riconsegnato_tot_g1=p_riconsegnato_tot_g1/1000000;

		if hour_calcolo>5 then
			dt_calcolo=dhms(&gday_gas,hour_calcolo,0,0);
		else dt_calcolo=dhms(&gday_gas+1,hour_calcolo,0,0);
		format dt: datetime19.;
	run;

	data calendario_plot_date;
		data_gas=&gday_gas-1;
		end_date=&gday_gas;

		do while (data_gas<=end_date);
			do hour=0 to 23;
				hour_solare=hour+6;
				data_solare=data_gas;

				if hour_solare>23 then
					do;
						hour_solare=hour_solare-24;
						data_solare=data_gas+1;
					end;

				dt_calcolo=dhms(data_solare,hour_solare,0,0);
				output;
			end;

			data_gas=intnx("day", data_gas, 1, 's');
		end;

		format dt_calcolo datetime19.;
		keep dt_calcolo;
	run;

	data dataset_wide_date_complete(drop=i p_incentivo1 where=(data_gas_pubb<=&gday_gas and dt_pubblicazione>=dhms(&gday_gas-1,15,0,0)));
		merge tot_long calendario_plot_date(in=p);
		by dt_calcolo;
		array prev p:;
		do i = 1 to dim(prev);
			prev[i]=prev[i]/10.57275;
		end;

		retain p_incentivo1;
		axis_up=round(max(of p_:)+5,5);
		axis_down=round(min(of p_:)-10,5);
		data_solare=datepart(dt_calcolo);
		dt_ora_legale_inizio=dhms(nwkdom(5, 1, 3, year(data_solare)),2,0,0);
		dt_ora_legale_fine=dhms(nwkdom(5, 1,10, year(data_solare)),2,0,0);

		if dt_calcolo >= dt_ora_legale_inizio and  dt_calcolo < dt_ora_legale_fine then
			do;
				dt_orologio=intnx("dthour", dt_calcolo, 1);
			end;
		else if dt_calcolo < dt_ora_legale_inizio or dt_calcolo >= dt_ora_legale_fine then
			do;
				dt_orologio=intnx("dthour", dt_calcolo, 0);
			end;

		dt_pubblicazione = intnx("dthour", dt_orologio, 1);
		data_gas_pubb=ifn(hour(dt_pubblicazione)>=6, datepart(dt_pubblicazione), datepart(dt_pubblicazione)-1);

		if dt_pubblicazione=dhms(&gday_gas-1,15,0,0) then
			do;
				call symput('prev_inc',p_riconsegnato_pubb);
				p_incentivo=p_riconsegnato_pubb;
				p_incentivo1=p_riconsegnato_pubb;
			end;

		if modello eq "Predizione G per G - DIANA" then
			do;
				pg_smooth=p_riconsegnato_pubb;
				mape_incentivo_stima=abs(p_incentivo1-p_riconsegnato_pubb)/p_riconsegnato_pubb;

				if dt_calcolo = &datetime_now then
					p_last=pg_smooth;
			end;

		format dt: datetime19. data: date9. mape_incentivo_stima percent8.1 p_: 5.0 pg_smooth 5.0;
	run;

	proc sql noprint;
		select count(*) into :n_preds from tot_long where modello="Predizione G per G - DIANA";
	run;

	data _null_;
		hour=5;
		min=55;
		day=put(&gday_gas, ITADFWDX17.);
		call symputx('day_title',day);
		g1_end=dhms(&gday_gas,hour,min,0);
		call symputx('g1_end',g1_end);
	run;

	%if &n_preds > 0 %then
		%do;

			proc sql noprint;
				select max(axis_up), min(axis_down) into :max_ax, :min_ax from dataset_wide_date_complete;
			run;

			data dataset_wide_date_complete;
				set dataset_wide_date_complete;
				if modello="Predizione G per G+1" then modello="Previsione G+1";
				else if modello="Predizione G per G - DIANA" then modello="Previsione G DIANA";
			run;

			proc sgplot data=dataset_wide_date_complete noautolegend dattrmap=mymap;
				/*				title2  height=16pt underlin=0 bold "Andamento giorno gas in corso &day_title";*/
				title2  height=16pt underlin=0 bold "Andamento nel giorno gas in corso";
				xaxis grid type=time interval=hour label= "Data di Pubblicazione" discreteorder=data labelattrs=(size=14 weight=bold);
				yaxis grid integer label= "Riconsegnato [M smc]" values=(&min_ax to &max_ax by 5) labelattrs=(size=14 weight=bold);
				y2axis min=0 max=0.2 label= "MAPE Incentivo Stimato[%]" labelattrs=(size=14 weight=bold);
				refline &g1_end  / axis=x lineattrs=(thickness=2 color=gray pattern=mediumdash);
				reg x=dt_pubblicazione y=p_riconsegnato_pubb / lineattrs=(thickness=0) CLM degree=2 alpha=0.05 nomarkers clmattrs=(CLMFILLATTRS=(COLOR=lightskyblue)) CLMTRANSPARENCY=0.7;

				/*				refline &prev_inc  / axis=y lineattrs=(thickness=5 color=yellow pattern=solid);*/
				VBARPARM CATEGORY=dt_pubblicazione RESPONSE=mape_incentivo_stima / y2axis datalabel BARWIDTH=0.5 datalabelattrs=(size=8 weight=bold);
			
				series x= dt_pubblicazione y=p_riconsegnato_pubb / group=modello dataskin=sheen legendlabel="Previsione G Pubblicata" name="Series_s" lineattrs=(thickness=4 COLOR=cx5f9ebd) markers markerattrs=(SIZE=12) groupms=key  msattrid=mytest;
				scatter x=dt_pubblicazione y=pg_smooth / markerattrs=(size=0);
				scatter x=dt_pubblicazione y=p_last / datalabel datalabelpos=right datalabelattrs=(size=12 weight=bold)  markerattrs=(size=0);
				scatter x=dt_pubblicazione y=p_incentivo / datalabel datalabelpos=top datalabelattrs=(size=12 weight=bold) legendlabel="Incentivo G+1" name="Incentivo" dataskin=sheen FILLEDOUTLINEDMARKERS markerattrs=(symbol= StarFilled size=15) markerfillattrs=(color=gold);
				keylegend "Series_s" "Series_p" "mape" "Incentivo" / AUTOITEMSIZE valueattrs=(size=12 ) border exclude=("") down=1 location=outside  position=bottomleft type=marker;
				styleattrs datacontrastcolors=&palette_list3;
			run;

		%end;
%mend;

%macro analisi_andamento_giornaliero(input_dset, now, modello, output_dset);


/*%let input_dset=predictions_targets_tot_long;*/
/*%let now=&datetime_now;*/
/*%let modello="Predizione G per G";*/
/*%let output_dset=output_g;*/


/* Prendo la riga delle previsioni con dt_calcolo = &now and modello eq &modello */
	data base;
		set &input_dset(where=(dt_calcolo = &now and modello eq &modello));
	run;

	proc sql noprint;
		select count(*) into :count_prev_analisi 
		from base;
	quit;

	/* Se una previsione è missing imposto outlier_final=2 */
	%if &count_prev_analisi = 0 %then %do;

		data &output_dset;
			dt_calcolo=&now;
			outlier_final=2;
			no_prev=1;
		run;

	%end;
	%else %do;

		data now_pred;
			set base;
			now=&now;
	
			if &modello in ("Predizione G per G - DIANA") then	do;
					day_rif=datepart(now);
					modello_prec="Predizione G per G+1";
					delta_day=0;
			end;
			else do;
					day_rif=datepart(now)+1;
					modello_prec="Predizione G per G+2";
					delta_day=1;
			end;

			call symput('modello_prec',modello_prec);
			call symput('day_rif',day_rif);
			call symput('hour_run',hour_gas);
			call symput('delta_day',delta_day);
			format now datetime19. day_rif date9.;
		run;

		/* Prendo le previsioni precedenti e le uso per valorizzare eventuali previsioni mancanti nello storico */
		proc sql noprint;
			create table recupero_preds2 as
				select data_gas_rif
						  , modello
						  , dt_calcolo
						  , prev
						  , -1 as hour_gas 
			from &input_dset
			where dt_calcolo <= &now and modello eq "&modello_prec" and data_gas_rif>="01OCT2018"d
			group by data_gas_rif
			having dt_calcolo=max(dt_calcolo);
		quit;

		proc sql noprint;
			select  min(dt_calcolo) into :min_dt_calcolo 
			from &input_dset
			where datepart(dt_calcolo)>="01OCT2018"d;
		run;

		data calendario_dt;
			dt_calcolo=&min_dt_calcolo;

			do while (dt_calcolo<&now);
				output;
				dt_calcolo=intnx('dthour',dt_calcolo,1,'B');
			end;

			format dt_calcolo datetime19.;
		run;

		data input_errors_fill;
			merge &input_dset(drop=ric: where=(modello eq &modello and data_gas_rif>="01OCT2018"d and dt_calcolo <= &now)) 
				  calendario_dt;
			by dt_calcolo;
		run;

		data input_errors_fill(Drop=key dt_riferimento_dati tipologia_previsione dt_rif_hourgas);
			set input_errors_fill recupero_preds2;

			if missing(prev) then
				do;
					modello=&modello;
					data_gas_rif=ifn(hour(Dt_calcolo)<6,datepart(Dt_calcolo)-1+&delta_day,datepart(dt_calcolo)+&delta_day);
					hour_gas=hour(dt_calcolo)-6;

					if hour_gas<0 then
						hour_gas=hour_gas+24;
				end;
		run;

		proc sort data=input_errors_fill out=input_errors_fill;
			by data_gas_rif dt_calcolo;
		run;

		data input_errors_fill;
			set input_errors_fill;
			retain fill_prev day_prev;

			if _N_=1 then
				do;
					fill_prev=prev;
					day_prev=data_gas_rif;
				end;
			else
				do;
					if day_prev=data_gas_rif then
						do;
							if missing(prev) then
								do;
									prev=fill_prev;
									data_gas_rif=day_prev;
								end;
						end;

					fill_prev=prev;
					day_prev=data_gas_rif;
				end;
		run;

		/* Per individuazioni di eventuali outlier */
		data input_errors;
			set input_errors_fill;

			if missing(prev)=0 and hour_gas ne -1;
			retain sum;
			precedente=lag1(prev);

			if hour_Gas=0 then
				do;
					sum=0;
					media_mobile=.;
					precedente=.;
				end;
			else
				do;
					sum=precedente+sum;
					media_mobile=sum/hour_Gas;
				end;

			delta_prec=prev-precedente;
			delta_prec_perc=delta_prec/precedente;
			delta_mob=prev-media_mobile;
			delta_mob_perc=delta_mob/media_mobile;
			format delta_mob_perc delta_prec_perc percent8.1;
			drop sum;
		run;

		proc expand data=input_errors out=input_errors;
			id dt_calcolo;
			convert delta_prec_perc=std_delta_prec_perc / transformout=( custd );
			convert delta_mob_perc=std_delta_mob_perc / transformout=( custd );
		run;

		data input_errors;
			set input_errors;
			alfa=2.7;
			soglia_delta=75;
			soglia_jump=26;
			soglia_vol=850;
			outlier_prec=ifn(hour_gas<1,0,ifn(abs(delta_prec_perc)<alfa*std_delta_prec_perc,0,1));
			outlier_mob=ifn(hour_gas<1,0,ifn(abs(delta_mob_perc)<alfa*std_delta_mob_perc,0,1));
			outlier_p1=ifn(hour_Gas<4,0,outlier_prec*outlier_mob);
			flag_jump=ifn(abs(delta_prec)/1000000>soglia_jump,1,0);

			if hour_gas<2 then
				do;
					outlier_delta_p=0;
					outlier_delta_m=0;
				end;
			else
				do;
					outlier_delta_p=ifn(abs(delta_prec)/1000000>soglia_delta,1,0);
					outlier_delta_m=ifn(abs(delta_mob) /1000000>soglia_delta,1,0);
				end;

			outlier_p2=outlier_delta_p*outlier_delta_m;
			flag_volume=ifn(prev/1000000<soglia_vol,1,0);;
			outlier_final=min(1,outlier_p2+outlier_p1*flag_jump+flag_volume);
		run;

		data &output_dset;
			set input_errors;
			if dt_calcolo=&now;
			keep dt_calcolo prev outlier_final hour_gas modello data_gas_rif precedente media_mobile;
		run;

	%end;

%mend;

%macro graph_preds_rictot_refdate(dataset_tot, dataset_wide, date, flag);

/*%let dataset_tot=predictions_targets_tot_long;*/
/*%let dataset_wide=predictions_targets_wide;*/
/*%let date=&g1day_gas;*/
/*%let flag=&outlier_g1_flag;*/

	data calendario_plot;
		/* Parto da &g1day_gas -4*/
		data_gas=&date-4;
		*inizio a prevedere 4 giorni in anticipo;
		end_date=min(&date, &gday_gas);

		do while (data_gas<=end_date);
			do hour=0 to 23;
				hour_solare=hour+6;
				data_solare=data_gas;

				if hour_solare>23 then
					do;
						hour_solare=hour_solare-24;
						data_solare=data_gas+1;
					end;

				dt_calcolo=dhms(data_solare,hour_solare,0,0);
				output;
			end;

			data_gas=intnx("day", data_gas, 1, 's');
		end;

		format dt_calcolo datetime19.;
		keep dt_calcolo;
	run;

	data dataset_wide_date (keep=data_gas_rif riconsegnato_tot_trad riconsegnato_tot_scada);
		set &dataset_wide;
		n_vals=n(of p:);
		if data_gas_rif = &date and n_vals>0 ;
		riconsegnato_tot_trad=riconsegnato_tot_trad/1000000;
		riconsegnato_tot_scada=riconsegnato_tot_scada/1000000;
	run;

	/* Prendo previsioni riferite a oggi di tutti i modelli */
	data dataset_tot_date;
		set &dataset_tot(drop=riconsegnato:
		where=(modello in &modelli_list_rictot and dt_calcolo<=&datetime_now));
		if data_gas_rif = &date and missing(prev)=0;
	run;

	proc sql noprint;
		select count(*) into :count_wide from dataset_wide_date
			where data_gas_rif = &date;
	run;

	proc sql noprint;
		select count(*) into :count_rictot from dataset_tot_date
			where data_gas_rif = &date;
	run;

	%if &count_wide eq 0 %then
		%do;
			/* non ho previsioni -> non faccio niente */
		%end;
	%else %if &count_rictot > 0 %then
		%do;

			data param_graph;
				hour=5;
				min=55;
				day=put(&date, ITADFWDX17.);
				g4_end=dhms(&date-3,hour,min,0);
				g3_end=dhms(&date-2,hour,min,0);
				g2_end=dhms(&date-1,hour,min,0);
				g1_end=dhms(&date-0,hour,min,0);
				call symputx('day_title',day);
				call symputx('g4_end',g4_end);
				call symputx('g3_end',g3_end);
				call symputx('g2_end',g2_end);
				call symputx('g1_end',g1_end);
				date_incentivo=&date-1;

				if ((nwkdom(5, 1, 3, year(date_incentivo)) <= date_incentivo and  
					date_incentivo< nwkdom(5, 1, 10, year(date_incentivo)))) then
					do;
						hour_pre_incentivo = &ora_incentivo-3;
						hour_incentivo = hour_pre_incentivo+1;
					end;
				else if ((date_incentivo < nwkdom(5, 1, 3, year(date_incentivo)) or 
					date_incentivo >= nwkdom(5, 1,10, year(date_incentivo)))) then
					do;
						hour_pre_incentivo = &ora_incentivo-2;
						hour_incentivo = hour_pre_incentivo+1;
					end;

				call symputx('dt_inc',dhms(date_incentivo, hour_incentivo,0,0));

				/* flag è 1 se la previsione è un outlier */
				if &flag in (1 2) then
					call symput('color_last',"red");
				else call symput('color_last',"blue");
				format g: datetime19. date_incentivo date9.;
			run;


			data dataset_graph;
				merge dataset_tot_date calendario_plot;
				by dt_calcolo;
				data_gas_rif = &date;
				retain prev_p;
				p_plot_gwh=prev/1000000;
				prev_p=p_plot_g_gwh;
				length key_label $18.;
				if key="p_riconsegnato_tot_g4" then
					do;
						p_plot_g4_gwh=p_plot_gwh;
						key_label="Previsione G+4";
					end;
				else if key="p_riconsegnato_tot_g3" then
					do;
						p_plot_g3_gwh=p_plot_gwh;
						key_label="Previsione G+3";

						if hour_gas=0 then
							p_plot_g4_gwh=p_plot_gwh;
					end;
				else if key="p_riconsegnato_tot_g2" then
					do;
						p_plot_g2_gwh=p_plot_gwh;
						key_label="Previsione G+2";

						if hour_gas=0 then
							p_plot_g3_gwh=p_plot_gwh;
					end;
				else if key="p_riconsegnato_tot_g1" then
					do;
						p_plot_g1_gwh=p_plot_gwh;
						key_label="Previsione G+1";

						if hour_gas=0 then
							p_plot_g2_gwh=p_plot_gwh;
					end;
/*				else if key="p_riconsegnato_tot_g" then*/
/*					do;*/
/*						p_plot_g_gwh=p_plot_gwh;*/
/*						key_label="Previsione G";*/
/**/
/*						if hour_gas=0 then*/
/*							p_plot_g1_gwh=p_plot_gwh;*/
/*				end;*/
				else if key="p_riconsegnato_tot_g_diana" then
					do;
						p_plot_g_gwh=p_plot_gwh;
						key_label="Previsione G DIANA";

						if hour_gas=0 then p_plot_g1_gwh=p_plot_gwh;
				end;

				axis_up=round(p_plot_gwh+25,25);
				axis_down=round(p_plot_gwh-25,25);

				if dt_calcolo = &dt_inc then
					p_incentivo=p_plot_gwh;

				if dt_calcolo = &datetime_now then
					p_last=p_plot_gwh;
				format p: 5.0;
			run;

			proc sql noprint;
				select max(axis_up), min(axis_down) into :max_ax, :min_ax from dataset_graph;
			run;

			/* aggiungo a dataset_graph il target */
			data dataset_graph;
				merge dataset_graph(in=p) dataset_wide_date;
				by data_gas_rif;

				if p;

/*				if hour_gas = 23 and modello="Predizione G per G" then do;*/
/*					ric_scada_last=riconsegnato_tot_scada;*/
/*					ric_trad_last=riconsegnato_tot_trad;*/
/*				end;*/
				if hour_gas = 23 and modello="Predizione G per G - DIANA" then do;
					ric_scada_last=riconsegnato_tot_scada;
					ric_trad_last=riconsegnato_tot_trad;
				end;
				format ric: 5.0;
			run;

			proc sql noprint;
				select count(*) into :flag_rictot_scada from dataset_graph where riconsegnato_tot_scada ne .;
			quit;

			proc sql noprint;
				select count(*) into :flag_rictot from dataset_graph where riconsegnato_tot_trad ne .;
			quit;

			proc sgplot data=dataset_graph noautolegend dattrmap=mymap;
				title2 height=16pt underlin=0 bold "Previsioni del Riconsegnato Totale";
				xaxis grid type=time interval=hour label= "Data di Calcolo" discreteorder=data labelattrs=(size=14 weight=bold);
				yaxis grid integer label= "Riconsegnato [GWh]" values=(&min_ax to &max_ax by 25) labelattrs=(size=14 weight=bold);
				refline &g4_end  / axis=x lineattrs=(thickness=2 color=gray pattern=mediumdash);
				refline &g3_end  / axis=x lineattrs=(thickness=2 color=gray pattern=mediumdash);
				refline &g2_end  / axis=x lineattrs=(thickness=2 color=gray pattern=mediumdash);
				refline &g1_end  / axis=x lineattrs=(thickness=2 color=gray pattern=mediumdash);

				%if &date = &gm1day_gas and &flag_rictot_scada > 0 %then
					%do;
						refline riconsegnato_tot_scada  / axis=y lineattrs=(thickness=3 color=orange pattern=solid) name="ric_scada" legendlabel="Consuntivo SCADA";

						%if &flag_rictot eq 0 %then
							%do;
								scatter x=dt_calcolo y=ric_scada_last / datalabel datalabelpos=top datalabelattrs=(size=13 weight=bold)  markerattrs=(size=0);
								keylegend "ric_scada" "Series" "Incentivo" / linelength=16 fillheight=6 valueattrs=(size=12 ) border exclude=("")  down=3  location=outside  position=bottomleft;
							%end;
						%else
							%do;
								refline riconsegnato_tot_trad  / axis=y lineattrs=(thickness=4 color=vilg pattern=solid) name="ric_trad" legendlabel="Consuntivo";
								scatter x=dt_calcolo y=ric_trad_last / datalabel datalabelpos=top datalabelattrs=(size=13 weight=bold)  markerattrs=(size=0);
								keylegend "ric_trad" "ric_scada" "Series" "Incentivo" / linelength=16 fillheight=6 valueattrs=(size=12 ) border exclude=("")  down=3  location=outside  position=bottomleft;
							%end;
					%end;

				/*				refline &dt_inc  / axis=x lineattrs=(thickness=8 color=green ) transparency=0.8 ;*/
				%else %if &date = &g4day_gas %then
					%do;
						/*						pbspline x=dt_calcolo y=p_plot_gwh / nomarkers alpha=0.1 lineattrs=(thickness=3 color=deepskyblue) CLM clmattrs=(CLMFILLATTRS=(COLOR=lightskyblue)) CLMTRANSPARENCY=0.7;*/
						keylegend "Series" "Incentivo" / AUTOITEMSIZE  valueattrs=(size=12 ) border exclude=("")  down=1  location=outside  position=bottomleft type=marker;
					%end;
				%else %if &date = &g3day_gas %then
					%do;
						pbspline x=dt_calcolo y=p_plot_gwh / nomarkers alpha=0.06 lineattrs=(thickness=3 color=deepskyblue) CLM clmattrs=(CLMFILLATTRS=(COLOR=lightskyblue)) CLMTRANSPARENCY=0.7;
						keylegend "Series" "Incentivo" / AUTOITEMSIZE  valueattrs=(size=12 ) border exclude=("")  down=1  location=outside  position=bottomleft type=marker;
					%end;
				%else %if &date = &gday_gas %then
					%do;
						pbspline x=dt_calcolo y=p_plot_gwh / nomarkers alpha=0.04 lineattrs=(thickness=3 color=deepskyblue) CLM clmattrs=(CLMFILLATTRS=(COLOR=lightskyblue)) CLMTRANSPARENCY=0.7;
						keylegend "Series" "Incentivo" / AUTOITEMSIZE  valueattrs=(size=12 ) border exclude=("")  down=2  location=outside  position=bottomleft type=marker;
					%end;
				%else
					%do;
						pbspline x=dt_calcolo y=p_plot_gwh / nomarkers alpha=0.04 lineattrs=(thickness=3 color=deepskyblue) CLM clmattrs=(CLMFILLATTRS=(COLOR=lightskyblue)) CLMTRANSPARENCY=0.7;
						keylegend "Series" "Incentivo" / AUTOITEMSIZE  valueattrs=(size=12 ) border exclude=("")  down=1  location=outside  position=bottomleft type=marker;
					%end;

				styleattrs datacontrastcolors=&palette_list1;

				* DATASYMBOLS=&marker_list;
				series x= dt_calcolo y=p_plot_gwh / dataskin=sheen name="Series" group=key_label  markers markerattrs=(symbol=CircleFilled SIZE=10) groupms=key  msattrid=mytest;

				%if &datetime_now ne &dt_inc %then
					%do;
						scatter x=dt_calcolo y=p_last / dataskin=sheen datalabel datalabelpos=right datalabelattrs=(size=12 weight=bold)  FILLEDOUTLINEDMARKERS markerattrs=(symbol=CircleFilled SIZE=13) markerfillattrs=(color=&color_last);
					%end;

				scatter x=dt_calcolo y=p_incentivo / datalabel datalabelpos=top datalabelattrs=(size=12 weight=bold) legendlabel="Incentivo G+1" name="Incentivo" dataskin=sheen FILLEDOUTLINEDMARKERS markerattrs=(symbol= StarFilled size=15) markerfillattrs=(color=gold);

				/*				keylegend "Series" "Incentivo" / AUTOITEMSIZE  valueattrs=(size=12 ) border exclude=("")  down=1  location=outside  position=bottomleft type=marker;*/
			run;

		%end;
%mend;

%macro graph_predstday_g_new(dataset_wide, dataset_tot, g_perf, date);

/*%let dataset_wide=predictions_targets_wide;*/
/*%let dataset_tot=predictions_targets_tot_long;*/
/*%let g_perf=output_g_perf_giorn;*/
/*%let date=&gm1day_gas;*/

	data g_perf_oraconvgiorn;
		set &g_perf(keep=data_gas_rif ora_convergenza);
		rename ora_convergenza=ora_pubb_conv;
	run;

	data tot_long;
		set &dataset_tot(where=(modello in ("Predizione G per G+1", "Predizione G per G - DIANA") 
			and tipologia_previsione="RiconsTot"
			and data_gas_rif eq &date and dt_calcolo<=&datetime_now) drop=riconsegnato_tot_aut  );
		prev=prev/1000000;
		riconsegnato_tot_trad=riconsegnato_tot_trad/1000000;
		riconsegnato_tot_scada=riconsegnato_tot_scada/1000000;
		drop dt_riferimento_dati dt_rif_hourgas tipologia_previsione;
		rename prev=p_riconsegnato_pubb;
	run;

	data dataset_wide_date(where=(dt_calcolo<=&datetime_now));
		set &dataset_wide(where=(data_gas_rif = &date) keep=data_gas_rif 
			hour: p_riconsegnato_tot_g_diana p_riconsegnato_tot_g1 riconsegnato_tot_trad);
		p_riconsegnato_tot_g_diana=p_riconsegnato_tot_g_diana/1000000;
		p_riconsegnato_tot_g1=p_riconsegnato_tot_g1/1000000;

		if hour_calcolo>5 then
			dt_calcolo=dhms(&date,hour_calcolo,0,0);
		else dt_calcolo=dhms(&date+1,hour_calcolo,0,0);
		format dt: datetime19.;
	run;

	data calendario_plot_date;
		data_gas=&date-1;
		end_date=&date;

		do while (data_gas<=end_date);
			do hour=0 to 23;
				hour_solare=hour+6;
				data_solare=data_gas;

				if hour_solare>23 then
					do;
						hour_solare=hour_solare-24;
						data_solare=data_gas+1;
					end;

				dt_calcolo=dhms(data_solare,hour_solare,0,0);
				output;
			end;

			data_gas=intnx("day", data_gas, 1, 's');
		end;

		format dt_calcolo datetime19.;
		keep dt_calcolo;
	run;

	data dataset_wide_date_complete(drop=i where=(data_gas_pubb<=&date and dt_pubblicazione>=dhms(&date-1,15,0,0)));
		merge tot_long calendario_plot_date(in=p);
		by dt_calcolo;
		data_gas_rif = &date;
		array prev p:;
		do i = 1 to dim(prev);
			prev[i]=prev[i]/10.57275;
		end;

		array ric ricon:;
		do i = 1 to dim(ric);
			ric[i]=ric[i]/10.57275;
		end;

		if riconsegnato_tot_trad ne . then
			do;
				limit_upper=riconsegnato_tot_trad+2.5;
				limit_lower=riconsegnato_tot_trad-2.5;
			end;
		else
			do;
				limit_upper=riconsegnato_tot_scada+2.5;
				limit_lower=riconsegnato_tot_scada-2.5;
			end;

		axis_up=round(max(of p_:)+5,5);
		axis_down=round(min(of p_:)-10,5);
		data_solare=datepart(dt_calcolo);
		dt_ora_legale_inizio=dhms(nwkdom(5, 1, 3, year(data_solare)),2,0,0);
		dt_ora_legale_fine=dhms(nwkdom(5, 1,10, year(data_solare)),2,0,0);

		if dt_calcolo >= dt_ora_legale_inizio and  dt_calcolo < dt_ora_legale_fine then
			do;
				dt_orologio=intnx("dthour", dt_calcolo, 1);
			end;
		else if dt_calcolo < dt_ora_legale_inizio or dt_calcolo >= dt_ora_legale_fine then
			do;
				dt_orologio=intnx("dthour", dt_calcolo, 0);
			end;

		dt_pubblicazione = intnx("dthour", dt_orologio, 1);
		data_gas_pubb=ifn(hour(dt_pubblicazione)>=6, datepart(dt_pubblicazione), datepart(dt_pubblicazione)-1);
		call symputx('dt_inc',dhms(&date-1,15,0,0));

		if dt_pubblicazione=dhms(&date-1,15,0,0) then
			do;
				call symput('prev_inc',p_riconsegnato_pubb);
				p_incentivo=p_riconsegnato_pubb;
				mape_valutato=abs(p_incentivo-riconsegnato_tot_trad)/riconsegnato_tot_trad;
				mape_stima_scada=abs(p_incentivo-riconsegnato_tot_scada)/riconsegnato_tot_scada;
				plot_text_val=p_incentivo+1;

				if riconsegnato_tot_trad eq . then
					do;
						plot_text=catx("","Prev=",put(p_incentivo,5.0), '0A'x, "MAPE=",put(mape_stima_scada,percent8.1));
					end;
				else
					do;
						plot_text=cat("Prev=",put(p_incentivo,5.0) , '0A'x, "MAPE=",put(mape_valutato,percent8.1) );
					end;

				plot_text=compress(plot_text," ");
			end;

		if modello eq "Predizione G per G - DIANA" then
			do;
				pg_smooth=p_riconsegnato_pubb;
				mae_valutato=abs(p_riconsegnato_pubb-riconsegnato_tot_trad);
				mae_stima_scada=abs(p_riconsegnato_pubb-riconsegnato_tot_scada);

				if hour(dt_pubblicazione) = 5 then
					do;
						p_last=pg_smooth;
						ric_scada_last=riconsegnato_tot_scada;
						ric_trad_last=riconsegnato_tot_trad;
					end;
			end;

		format dt: datetime19. data: date9. mape: percent8.1 p_: mae: pg_smooth ric: 5.0;
	run;

	proc sql noprint;
		select count(*) into :n_preds from tot_long where modello="Predizione G per G - DIANA"  
							and (riconsegnato_tot_scada ne . or riconsegnato_tot_trad ne .);
	quit;

	proc sql noprint;
		select count(*) into :flag_rictot from tot_long 
			where riconsegnato_tot_trad ne .;
	quit;

	%put &flag_rictot;

	data _null_;
		hour=6;

		*5;
		min=0;

		*55;
		day=put(&date, ITADFWDX17.);
		call symputx('day_title',day);
		g1_end=dhms(&date,hour,min,0);
		call symputx('g1_end',g1_end);
	run;

	%if &n_preds > 0 %then
		%do;

			proc sql noprint;
				select max(axis_up), min(axis_down) into :max_ax, :min_ax 
				from dataset_wide_date_complete;
			run;

			data dataset_wide_date_complete_oc;
				merge g_perf_oraconvgiorn dataset_wide_date_complete(in=p);
				by data_gas_rif;

				if p;
				flag_convergenza=0;

				if  modello eq "Predizione G per G - DIANA" then
					do;
						if ora_pubb_conv=hour(dt_pubblicazione) then
							do;
								flag_convergenza=1;
								call symput('dt_conv', dt_pubblicazione);
								plot_text_val_g=&max_ax-2;
								plot_text_g=catx("","Convergenza", '0A'x, "ore ",put(hour(dt_pubblicazione),2.));
							end;
						else if ora_pubb_conv= -1 and hour(dt_pubblicazione) = 5 then
							do;
								call symput('dt_conv', 0);
								plot_text_val_g=&max_ax-2;
								plot_text_g=catx("","Niente", '0A'x, "convergenza");
							end;
					end;
			run;

			data dataset_wide_date_complete_oc;
				set dataset_wide_date_complete_oc;
				if modello="Predizione G per G+1" then modello="Previsione G+1";
				else if modello="Predizione G per G - DIANA" then modello="Previsione G DIANA";
			run;

			proc sgplot data=dataset_wide_date_complete_oc noautolegend dattrmap=mymap;
				/*				title2  height=16pt underlin=0 bold "Andamento giorno gas in corso &day_title";*/
				title2  height=16pt underlin=0 bold "Andamento dall'orario d'incentivo";
				xaxis grid type=time interval=hour label= "Data di Pubblicazione" discreteorder=data labelattrs=(size=14 weight=bold);
				yaxis integer label= "Riconsegnato [M smc]" values=(&min_ax to &max_ax by 5) labelattrs=(size=14 weight=bold);
				y2axis min=0 max=20 label= "MAE [M smc]" labelattrs=(size=14 weight=bold);
				refline &g1_end  / axis=x lineattrs=(thickness=2 color=gray pattern=shortdash);

				%if &flag_rictot = 0 %then
					%do;
						band x=dt_pubblicazione lower=limit_lower upper=limit_upper / transparency=.7 fillattrs=(color=sandybrown) legendlabel="Banda di Tolleranza" name="band";
						refline riconsegnato_tot_scada  / axis=y name="ric_scada" legendlabel="Consuntivo SCADA" lineattrs=(thickness=4 color=orange pattern=solid);
						VBARPARM CATEGORY=dt_pubblicazione RESPONSE=mae_stima_scada / legendlabel="MAE SCADA" name="mae" y2axis datalabel BARWIDTH=0.5 datalabelattrs=(size=8 weight=bold);
						scatter x=dt_pubblicazione y=ric_scada_last / datalabel datalabelpos=topright datalabelattrs=(size=13 weight=bold)  markerattrs=(size=0);
						refline limit_upper  /  axis=y lineattrs=(thickness=2 color=sandybrown pattern=ShortDash) transparency=0.7;
						refline limit_lower  / axis=y lineattrs=(thickness=2 color=sandybrown pattern=ShortDash) transparency=0.7;
						keylegend "ric_scada" "Series_s" / linelength=16 fillheight=6 valueattrs=(size=12 ) border down=2 location=outside  position=bottomleft exclude=("");

						* type=marker;
					%end;
				%else
					%do;
						*x=dt_pubblicazione;
						refline &dt_conv  / name="ora_conv" legendlabel="Ora di Convergenza" axis=x lineattrs=(thickness=3 color=dodgerblue pattern=solid) transparency=0.7;
						band x=dt_pubblicazione lower=limit_lower upper=limit_upper / transparency=.7 fillattrs=(color=lightgreen) legendlabel="Banda di Tolleranza" name="band";
						refline riconsegnato_tot_trad  / axis=y name="ric" legendlabel="Consuntivo" lineattrs=(thickness=5 color=vilg pattern=solid);
						VBARPARM CATEGORY=dt_pubblicazione RESPONSE=mae_valutato / legendlabel="MAE" name="mae" y2axis datalabel BARWIDTH=0.5 datalabelattrs=(size=8 weight=bold);
						refline riconsegnato_tot_scada  / axis=y name="ric_scada" legendlabel="Riconsegnato SCADA" lineattrs=(thickness=3 color=orange pattern=solid);
						scatter x=dt_pubblicazione y=ric_trad_last / datalabel datalabelpos=topright datalabelattrs=(size=13 weight=bold)  markerattrs=(size=0);
						refline limit_upper  / axis=y lineattrs=(thickness=2 color=lightgreen pattern=ShortDash) transparency=0.7;
						refline limit_lower  / axis=y lineattrs=(thickness=2 color=lightgreen pattern=ShortDash) transparency=0.7;
						keylegend "ric" "ric_scada" "Series_s" / linelength=16 fillheight=6 valueattrs=(size=12 ) border down=2 location=outside  position=bottomleft exclude=("");
						TEXT X=dt_pubblicazione Y=plot_text_val_g text=plot_text_g /  outline backfill POSITION=bottom textattrs=(size=10 weight=bold) fillattrs=(color=AZURE);

						* type=marker;
					%end;

				/*				series x= dt_pubblicazione y=p_riconsegnato_tot_g_punt / dataskin=sheen legendlabel="Previsione G Puntuale" name="Series_p" lineattrs=(thickness=4 COLOR=darkorange) markers markerattrs=(SIZE=10 COLOR=darkorange symbol=CircleFilled);*/
				series x= dt_pubblicazione y=p_riconsegnato_pubb / group=modello dataskin=sheen legendlabel="Previsione G Pubblicata" name="Series_s" lineattrs=(thickness=4 COLOR=cx5f9ebd) markers markerattrs=(SIZE=12) groupms=key  msattrid=mytest;
				scatter x=dt_pubblicazione y=pg_smooth / markerattrs=(size=0);
				scatter x=dt_pubblicazione y=p_incentivo / legendlabel="Incentivo G+1" name="Incentivo" dataskin=sheen FILLEDOUTLINEDMARKERS markerattrs=(symbol= StarFilled size=15) markerfillattrs=(color=gold);
				TEXT X=dt_pubblicazione Y=plot_text_val text=plot_text /  outline backfill POSITION=top textattrs=(size=10 weight=bold) fillattrs=(color=AZURE);

				*datalabel datalabelpos=top datalabelattrs=(size=12 weight=bold);
				styleattrs datacontrastcolors=&palette_list3;
			run;

		%end;
%mend;

/*================================*/
/*======= TOMORROW  =========*/
/*================================*/
%macro get_preds_g1(ds_in=, ds_in_pub=, ds_out=, date_rif=);

/*%let ds_in=predictions_targets_wide;*/
/*%let date_rif=&g1day_gas_sistema;*/
/*%let ds_in_pub= for_svil.pubblicazioni_snam;*/
/*%let ds_out=dset_filter_G1_tmw;*/

	data G1_tmw;
		set &ds_in(where=(data_gas_rif= &date_rif and hour_calcolo = hour(&datetime_now)) 
			keep=p_riconsegnato_tot_g1 p_riconsegnato_civi_g1 p_riconsegnato_term_g1 
			data_Gas_rif hour_gas hour_calcolo
			);
		dt_calcolo=&datetime_now;
		rename p_riconsegnato_tot_g1=p_tot_g1_tmw p_riconsegnato_civi_g1=p_civ_g1_tmw
				p_riconsegnato_term_g1=p_term_g1_tmw;
		format dt_calcolo datetime19.;
		drop hour:;
	run;

	/* Prendo previsione pubblicata da incentivo riferita al gm1day_gas_sistema*/
/*	proc sql noprint;*/
/*		select max(data_gas_rif) format date9. into: max_dt_pub*/
/*		from &ds_in_pub*/
/*		;*/
/*	quit;*/

	data G1_tmw_inc_pub;
		set &ds_in_pub;
		if data_gas_rif = &date_rif;
		rename G1=p_tot_g1_tmw_inc_pub;
	run;

	/* Prendo ultima previsione DAFNE da incentivo */
	data G1_tmw_inc;
		set &ds_in(where=(data_gas_rif= &date_rif and hour_calcolo = &hour_inc_tday) 
			keep=p_riconsegnato_tot_g1 data_Gas_rif hour_gas hour_calcolo
			);
		dt_calcolo=&datetime_now;
		dt_to_keep=dhms(datepart(&datetime_now),&hour_inc_tday,0,0);
		rename p_riconsegnato_tot_g1=p_tot_g1_tmw_inc;
		format dt: datetime19.;
		drop hour:;
	run;

	proc sql;
		create table &ds_out as 
		select a.*
			  ,b.p_tot_g1_tmw_inc
			  ,c.p_tot_g1_tmw_inc_pub
		from G1_tmw as a
		left join G1_tmw_inc as b 
		on a.data_gas_rif = b.data_gas_rif 
		left join G1_tmw_inc_pub as c
		on a.data_gas_rif=c.data_gas_rif
		;
	quit;

%mend get_preds_g1;

/*================================*/
/*======= TODAY  =========*/
/*================================*/
%macro get_preds_g_g1inc_oggi(ds_in=, ds_in_pub=, ds_out=);


/*%let ds_in=predictions_targets_wide;*/
/*%let ds_in_pub=for_svil.pubblicazioni_snam;*/
/*%let ds_out=dset_filter_tday;*/

	data dset_filter_g1_tday;
		set &ds_in(where=(data_gas_rif= &gday_gas and hour_calcolo = &hour_inc_yday) 
			keep=p_riconsegnato_tot_g1 p_riconsegnato_civi_g1 p_riconsegnato_term_g1 
			data_gas_rif hour_gas hour_calcolo);
		dt_calcolo=&datetime_now;
		rename p_riconsegnato_tot_g1=p_tot_g1_tday 
			   p_riconsegnato_civi_g1=p_civ_g1_tday
			   p_riconsegnato_term_g1=p_term_g1_tday;
		format dt_calcolo datetime19.;
	run;

	data dset_filter_g_tday;
		set &ds_in(where=(data_gas_rif= &gday_gas_sistema and hour_calcolo = hour(&datetime_now)) 
			keep=p_riconsegnato_tot_g_diana
				 data_gas_rif hour_gas hour_calcolo
			);
		dt_calcolo=&datetime_now;
		rename p_riconsegnato_tot_g_diana=p_tot_g_tday;
		format dt_calcolo datetime19.;
	run;

	data dset_filter_pub_tday (keep=data_gas_rif p_tot_g1_tday_pub dt_calcolo);
		set &ds_in_pub (where=(data_gas_rif= &gday_gas) );
		rename g1= p_tot_g1_tday_pub;
		dt_calcolo=&datetime_now;
		format dt_calcolo datetime19.;
	run;

	data &ds_out;
		retain data_gas_rif dt_calcolo;
		merge dset_filter_g1_tday dset_filter_pub_tday dset_filter_g_tday;
		by dt_calcolo;
		drop hour:;
		mape_g1_g_tday=abs(p_tot_g_tday-p_tot_g1_tday)/p_tot_g_tday;
		mape_g1_g_tday_pub=abs(p_tot_g_tday-p_tot_g1_tday_pub)/p_tot_g_tday;
	run;

%mend get_preds_g_g1inc_oggi;
/* PERFORMANCE PASSATE G1 */
%macro get_perf_g1(ds_in= , ds_out=);

/*%let ds_in=preds_target_tot_inc_perf;*/
/*%let ds_out=dset_filter_g1_yday;*/

	/* PERFORMANCE DEL GIORNO GAS DI IERI */
	data preds_target_tot_inc_yday;
		set &ds_in(where=(data_gas_rif eq &gm1day_gas_sistema));
		keep anno_termico dt_calcolo data_gas_rif 
			p_riconsegnato_tot_g1 p_riconsegnato_tot_g1_pub
			riconsegnato_tot:  
			riconsegnato_civile riconsegnato_termo_fill riconsegnato_ind_fill
			mape_tradizionale: mape_autorita: mape_scada: 
			incentivo_tradizionale: incentivo_autorita: 
			periodo_inc 
			
		;
		rename p_riconsegnato_tot_g1=p_tot_g1_yday
			   p_riconsegnato_tot_g1_pub=p_tot_g1_yday_pub
			   riconsegnato_tot_trad=ric_tot_trad_yday
			   riconsegnato_tot_aut=ric_tot_aut_yday 
			   riconsegnato_tot_scada=ric_tot_scada_yday
			   riconsegnato_civile=ric_civ_yday 
			   riconsegnato_termo_fill=ric_term_yday 
			   riconsegnato_ind_fill=ric_ind_yday

			   mape_tradizionale=mape_trad_yday 
			   mape_tradizionale_pub=mape_trad_yday_pub
			   mape_autorita=mape_aut_yday 
			   mape_autorita_pub=mape_aut_yday_pub
			   mape_scada=mape_scada_yday
			   mape_scada_pub=mape_scada_yday_pub
			   incentivo_tradizionale=incentivo_trad_yday 
			   incentivo_tradizionale_pub=incentivo_trad_yday_pub
			   incentivo_autorita=incentivo_aut_yday
			   incentivo_autorita_pub=incentivo_aut_yday_pub;

		dt_calcolo=&datetime_now;
		call symput('at_yday', anno_termico);
	run;


	/* PERFORMANCE PASSATE da 01OCT2018 --> mean, max e sum del mape_valutato per anno_termico 
		(mape_valutato è mape_tradizionale se periodo incentivo=3 altrimenti mape_autorita )*/
	proc sql;
		create table perf_prod as 
			select anno_termico
				, count(data_gas_rif) as count_prod
				, (mean(mape_valutato)) format=percent8.2 as mean_mape_prod 
				, (max(mape_valutato)) format=percent8.1 as max_mape_prod
				, (sum(incentivo_valutato)) format=comma9.2 as sum_incentivo_prod
			from &ds_in 
			where data_gas_rif>="01OCT2018"d and mape_valutato ne .
			group by anno_termico;
	quit;

	proc sql;
		create table perf_pub as 
			select anno_termico
				, count(data_gas_rif) as count_prod
				, (mean(mape_valutato_pub)) format=percent8.2 as mean_mape_prod_pub 
				, (max(mape_valutato_pub)) format=percent8.1 as max_mape_prod_pub
				, (sum(incentivo_valutato_pub)) format=comma9.2 as sum_incentivo_prod_pub
			from &ds_in 
			where data_gas_rif>="01OCT2018"d and mape_valutato_pub ne .
			group by anno_termico;
	quit;

	/* PERFORMANCE MESE IN CORSO --> mean, max e sum del mape_valutato per anno_mese
		(mape_valutato è mape_tradizionale se periodo incentivo=3 altrimenti mape_autorita )	*/
	proc sql;
		create table perf_mese_corso as 
			select anno_termico
				, anno_mese as anno_mese_mc
				, count(data_gas_rif) as count_mc
				, (mean(mape_valutato)) format=percent8.2 as mean_mape_mc
				, (max(mape_valutato)) format=percent8.1 as max_mape_mc
				, (sum(incentivo_valutato)) format=comma9.2 as sum_incentivo_mc
			from &ds_in (where = (anno_mese="&am_corso" and mape_valutato ne .))
			group by anno_mese, anno_termico;
	quit;

	proc sql;
		create table perf_mese_corso_pub as 
			select anno_termico
				, anno_mese as anno_mese_mc
				, count(data_gas_rif) as count_mc
				, (mean(mape_valutato_pub)) format=percent8.2 as mean_mape_mc_pub
				, (max(mape_valutato_pub)) format=percent8.1 as max_mape_mc_pub
				, (sum(incentivo_valutato_pub)) format=comma9.2 as sum_incentivo_mc_pub
			from &ds_in (where = (anno_mese="&am_corso" and mape_valutato_pub ne .))
			group by anno_mese, anno_termico;
	quit;

	data &ds_out;
		merge perf_mese_corso perf_mese_corso_pub perf_prod perf_pub preds_target_tot_inc_yday(in=p);
		by anno_termico;
		if p;
	run;

%mend;

/* PERFORMANCE PASSATE G */
%macro get_perf_g(dset,output_g_giorn, output_g_stats, modello);

/*	%let dset=predictions_targets_wide;*/
/*	%let output_g_giorn=output_g_diana_perf_giorn;*/
/*	%let output_g_stats=output_g_diana_perf_stats;*/
/*	%let modello=gdiana;*/

	data _null_;
		modello="&modello";
		if modello="gold" then do;
			call symput("prevg","p_riconsegnato_tot_g");
		end;
		else do;
			call symput("prevg","p_riconsegnato_tot_g_diana");
		end;
	run;

	%put ---- Calcolo performance passate per il modello &modello ----;

	/* Prendo le previsioni riferite a ieri del modello G e calcolo mae e mape tradizionale e scada */
	data preds_target_tot_g(drop=i);
		set &dset(where=(data_gas_rif <=&gm1day_gas_sistema and &prevg ne .) 
			keep=ric: &prevg hour: data_Gas_rif);

		data_calcolo=ifn(hour_calcolo<6,data_gas_rif+1,data_gas_rif);
		dt_calcolo=dhms(data_calcolo,hour_calcolo,0,0);

		array ric{2} riconsegnato_tot_trad	riconsegnato_tot_scada;
		array mae{2} mae_tradizionale	mae_scada;
		array mape{2} mape_tradizionale	mape_scada;
		anno_mese=cat(year(data_gas_rif),"_",put(month(data_gas_rif),z2.0));
		do i = 1 to dim(ric);
			mae[i]=abs(ric[i]-&prevg);
			mape[i]=mae[i]/ric[i];
		end;
		if "&modello"="gdiana" then do;
			if data_gas_rif ge "11MAR2020"d;
		end;
		format dt: datetime19. data: date9. mape: percent8.1;
	run;

	/* Calcolo ora di pubblicazione rispetto all'ora di sistema di calcolo
	(+1h se sono in inverno, +2h se sono in estate)
	(in inverno se hour_calcolo in 5 ora_pub=.; in estate se hour_calcolo in (4,5) ora_pub=.) */
	data preds_target_tot_g_conv;
		set preds_target_tot_g;
		if nwkdom(5, 1, 3, year(data_calcolo)) <= data_calcolo <= nwkdom(5, 1,10, year(data_calcolo)) then do;
			ora_pub = hour_calcolo + 2;
			if hour_calcolo ge 22 then ora_pub = hour_calcolo - 22;
			dt_pubblicazione=intnx("dthour", dt_calcolo, 2);
			if hour_calcolo in (4,5) then do;
				ora_pub=.;
			end;
		end;
		else do;
			ora_pub = hour_calcolo+1;
			if hour_calcolo ge 23 then ora_pub = hour_calcolo - 23;
			dt_pubblicazione=intnx("dthour", dt_calcolo, 1);
			if hour_calcolo = 5 then do;
				ora_pub=.;
			end;
		end;

		data_Gas_pubb=ifn(hour(dt_pubblicazione)<6,intnx("day", datepart(dt_pubblicazione), -1), datepart(dt_pubblicazione));

		/* calcolo variabile modello: cat(hour_gas, ora effettiva di calcolo) */
		if hour_calcolo ge 6 then
			modello=hour_calcolo-6;
		else modello=hour_calcolo+18;

		if modello < 10 then
			modello_c =cat(0,modello,"ore",hour_calcolo);
		else modello_c =cat(modello,"ore",hour_calcolo);
		format data: date9.;
		format dt: datetime19.;
	run;

	/* Per calcolare la convergenza escludo le ore che non vengono pubblicate (inverno le ore 5 e in estate le ore 4,5 )*/
	data preds_target_tot_g_conv;
		set preds_target_tot_g_conv;
		where not missing(ora_pub) and data_Gas_pubb=data_gas_rif;

		/* creo ora_pub_key per partire dalle ore 06 */
		if ora_pub ge 6 then
			ora_pub_key=ora_pub-6;
		else ora_pub_key=ora_pub+18;

		if ora_pub_key < 10 then
			ora_pub_key_c =cat(0,ora_pub_key,"ore",ora_pub);
		else ora_pub_key_c =cat(ora_pub_key,"ore",ora_pub);

		if &prevg ne .;
	run;

	/* soglie  modello G */
	/*%let soglia_mae=26.432;*/
	/*%let soglia_salto_prev=26.432;*/

	data preds_target_tot_g_conv(drop=date_prec mape_prec ora_conv_prec mae_prec pred_prec flag_convergenza_prec);
		length data_gas_rif dt_pubblicazione 8. ora_pub_key_c $200. ora_pub_key
			&prevg mae_tradizionale flag_salto flag_mae flag_convergenza ora_convergenza 8.;
		set preds_target_tot_g_conv;
		retain ora_conv_prec flag_convergenza_prec date_prec pred_prec mae_prec mape_prec;
		soglia_mae=&soglia_mae*1000000;
		soglia_salto_prev=&soglia_salto_prev*1000000;

		if _N_=1 or date_prec ne data_gas_pubb then
			do;
				date_prec=data_gas_pubb;
				flag_convergenza=0;
				mape_prec=mape_tradizionale;
				mae_prec=mae_tradizionale;
				ora_convergenza=ora_pub_key;
				ora_conv_prec=ora_convergenza;
				pred_prec=&prevg;
				flag_salto=1;
				flag_mae=ifn(mae_tradizionale <=soglia_mae,1,0);

				if flag_mae=1 then
					flag_convergenza=1;
				flag_convergenza_prec=flag_convergenza;
			end;
		else
			do;
				flag_convergenza = 0;
				flag_salto=ifn(abs(&prevg-pred_prec)<=soglia_salto_prev,1,0);
				flag_mae=ifn(mae_tradizionale <=soglia_mae,1,0);

				if flag_salto=0 then
					flag_convergenza_prec=0;

				if flag_mae=1 then
					flag_convergenza=1;
				date_prec= data_gas_pubb;
				mape_prec=mape_tradizionale;
				mae_prec=mae_tradizionale;
				pred_prec=&prevg;

				if flag_convergenza = 1 and flag_convergenza_prec=1 then
					do;
						ora_convergenza=ora_conv_prec;
					end;
				else if flag_convergenza = 1 and flag_convergenza_prec=0 then
					do;
						ora_convergenza=ora_pub_key;
					end;
				else
					do;
						ora_convergenza=ora_pub_key;
					end;

				ora_conv_prec=ora_convergenza;
				flag_convergenza_prec=flag_convergenza;
			end;
	run;

	/* Creo tabella con ora di convergenza giornaliera per data_gas_rif */
	proc sql;
		create table ora_convergenza_giornaliera as 
		select  *
		from work.preds_target_tot_g_conv t1
		group by t1.data_gas_rif
		having t1.ora_convergenza=max(t1.ora_convergenza) and t1.riconsegnato_tot_trad ne .
				and ora_pub_key=ora_convergenza and flag_convergenza=1
				order by t1.data_gas_rif;
	quit;

	proc sql noprint;
		select max(data_gas_rif), min(data_gas_rif)
			into :max_gday, :min_gday
				from preds_target_tot_g_conv;
	quit;

	/* Creo tabella performance per data_gas_rif  */
	proc sql;
		create table perf_allday as
			select 
				data_gas_rif,
				mean(mae_tradizionale) as mean_mae_trad_allday,
				max(mae_tradizionale) as max_mae_trad_allday,
				mean(mape_tradizionale) format=percent8.2 as mean_mape_trad_allday
			from preds_target_tot_g
			group by data_gas_rif;
	quit;

	/* creo maschera di calendario che va fino a gm1day_gas_sistema */
	data calendario(where=(data_gas_rif<=&max_gday));
		data_gas_rif=&min_gday;
		output;

		do while (data_gas_rif<=&max_gday);
			data_gas_rif=intnx("day", data_gas_rif, 1, 's');
			output;
		end;

		format data_gas_rif date9.;
	run;

	/* MERGE del calendario con ora_convergenza_giornaliera e perf_allday --> &output_g_giorn*/

	data &output_g_giorn(rename=(ora_pub=ora_convergenza) where=(mean_mae_trad_allday ne .));
		merge ora_convergenza_giornaliera(keep= flag_convergenza data_gas_rif ora_pub ora_pub_key) 
			 calendario
			perf_allday;

		*perf_6_17;
		by data_gas_rif;

		if ora_pub= . then
			do;
				flag_convergenza=0;
				ora_pub=-1;
				ora_pub_key=24;
			end;

		dt_calcolo=&datetime_now;
		format dt: datetime19.;
	run;

	/**************************************/
	/* Ora convergenza media per AT */
	data master_ore;
		do ora_pub_key=1 to 24;
			ora_convergenza=ora_pub_key+6;

			if ora_convergenza>23 then
				ora_convergenza=ora_convergenza-24;

			if ora_pub_key=24 then
				ora_convergenza=-1;
			output;
		end;
	run;

	proc sql;
		create table g_distrib_conv as 
		select ora_pub_key as ora_pub_key
			 , ora_convergenza as ora_convergenza
			 , (count(data_gas_rif)) as count_days 
		from &output_g_giorn  
		group by ora_pub_key, ora_convergenza
		;
	quit;

	data g_distrib_conv;
		merge master_ore g_distrib_conv;
		by ora_pub_key ora_convergenza;
		if COUNT_days eq . then COUNT_days=0;
	run;

	proc sql noprint;
		select sum( count_days) into :tot_days
			from g_distrib_conv;
	run;

	data g_distrib_conv(drop=old_prob);
		set g_distrib_conv;
		retain old_prob prob_cum;
		prob=count_days/&tot_days;
		tot_days=&tot_days;
		call symputx('tot_days',tot_days);

		if _N_=1 then
			do;
				old_prob=prob;
				prob_cum=prob;
			end;
		else
			do;
				prob_cum=prob+prob_cum;
				old_prob=prob;
			end;

		soglia_mae_gwh=&soglia_mae;
		soglia_salto_prev_gwh=&soglia_salto_prev;
		format prob: percent8.0;
	run;

	proc sql noprint;
		select min(ora_pub_key) into : ora_conv_pub_at 
		from g_distrib_conv (where = (prob_cum ge 0.5))
		;
	run;

	proc sql noprint;
		select sum(count_days) into: count_dates_at
		from g_distrib_conv
		;
	quit;

	data stats_oreconv_at (keep=median_ora_conv_prod);
		set g_distrib_conv (where = (ora_pub_key=&ora_conv_pub_at));
		median_ora_conv_prod=ora_convergenza;
	run;

/*	data stats_oreconv_at (drop=ora_conv_pub_at);*/
/*		ora_conv_pub_at=&ora_conv_pub_at;*/
/*		if ora_conv_pub_at le 18 then median_ora_conv_prod=ora_conv_pub_at+6;*/
/*		else median_ora_conv_prod=ora_conv_pub_at-18;*/
/*	run;*/


	/**************************************/
	/* Ora convergenza media per MESE IN CORSO */

	data &output_g_giorn;
		set &output_g_giorn;
		anno_mese=cat(year(data_gas_rif),"-",put(month(data_gas_rif),z2.));
	run;

	proc sql;
		create table g_distrib_conv as 
		select ora_pub_key as ora_pub_key
			 , ora_convergenza as ora_convergenza
			 , (count(data_gas_rif)) as count_days 
		from &output_g_giorn (where = (anno_mese="&am_corso"))  
		group by ora_pub_key, ora_convergenza
		;
	quit;

	data g_distrib_conv;
		merge master_ore g_distrib_conv;
		by ora_pub_key ora_convergenza;
		if COUNT_days eq . then COUNT_days=0;
	run;

	proc sql noprint;
		select sum( count_days) into :tot_days
			from g_distrib_conv;
	run;

	data g_distrib_conv(drop=old_prob);
		set g_distrib_conv;
		retain old_prob prob_cum;
		prob=count_days/&tot_days;
		tot_days=&tot_days;
		call symputx('tot_days',tot_days);

		if _N_=1 then
			do;
				old_prob=prob;
				prob_cum=prob;
			end;
		else
			do;
				prob_cum=prob+prob_cum;
				old_prob=prob;
			end;

		soglia_mae_gwh=&soglia_mae;
		soglia_salto_prev_gwh=&soglia_salto_prev;
		format prob: percent8.0;
	run;

	proc sql noprint;
		select min(ora_pub_key) into : ora_conv_pub_mc 
		from g_distrib_conv (where = (prob_cum ge 0.5))
		;
	run;

	proc sql noprint;
		select sum(count_days) into: count_dates_mc
		from g_distrib_conv
		;
	quit;

/*	data stats_oreconv_mc (drop=ora_conv_pub_mc);*/
/*		ora_conv_pub_mc=&ora_conv_pub_mc;*/
/*		if ora_conv_pub_mc le 18 then median_ora_conv_mc=ora_conv_pub_mc+6;*/
/*		else median_ora_conv_mc=ora_conv_pub_mc-18;*/
/*		count_dates_mc=&count_dates_mc;*/
/*	run;*/

	data stats_oreconv_mc (keep=median_ora_conv_mc count_dates_mc);
		set g_distrib_conv (where = (ora_pub_key=&ora_conv_pub_mc));
		median_ora_conv_mc=ora_convergenza;
		count_dates_mc=&count_dates_mc;
	run;

	/********************************/
	/* Performance AT in corso e MC */

	proc sql;
		create table perf_allday as
			select
				mean(mae_tradizionale)/1000000 as mean_mae_trad_allday_prod
			from preds_target_tot_g (where=(mae_tradizionale ne .));
	quit;

	proc sql;
		create table perf_allday_mc as
		select mean(mae_tradizionale)/1000000 as mean_mae_trad_allday_mc
		from preds_target_tot_g (where = (month(data_gas_rif) eq month(&gday_gas)
									and year(data_gas_rif) eq 
									year(&gday_gas) and  mae_tradizionale ne .)) 
			;
	quit;


	data &output_g_stats;
		retain dt_calcolo;
		merge perf_allday perf_allday_mc stats_oreconv_mc stats_oreconv_at;
		dt_calcolo=&datetime_now;
		format dt: datetime19.;
	run;

%mend;
%macro graph_g1mods_perf(preds_g1= ,preds_pub= );

/*	%let preds_g1=predictions_targets_wide;*/
/*	%let preds_pub=for_svil.pubblicazioni_snam;*/

	data preds_target_inc;
		set &preds_g1(where=(data_gas_rif <=&gm1day_gas) 
			keep=ric: p_riconsegnato_civi_g1
			p_riconsegnato_term_g1 p_riconsegnato_tot_g1: hour: data_gas_rif);
		data_calcolo=ifn(hour_calcolo<6,data_gas_rif,data_gas_rif-1);
		dt_calcolo=dhms(data_calcolo,hour_calcolo,0,0);

		if ((nwkdom(5, 1, 3, year(data_calcolo)) <= data_calcolo and  
			data_calcolo< nwkdom(5, 1, 10, year(data_calcolo)))) then
			do;
				/* Se vera significa che siamo in stagione estiva */
				hour_pre_incentivo = &ora_incentivo-3;

				*12;
				hour_incentivo = hour_pre_incentivo+1;

				*13;
			end;
		else if ((data_calcolo < nwkdom(5, 1, 3, year(data_calcolo)) or data_calcolo >= nwkdom(5, 1,10, year(data_calcolo)))) then
			do;
				/* Se vera significa che siamo in stagione invernale */
				hour_pre_incentivo = &ora_incentivo-2;

				*13;
				hour_incentivo =hour_pre_incentivo+1;

				*14;
			end;

		if hour_calcolo=hour_incentivo;
		format dt: datetime19. data: date9.;
	run;

	proc sql;
		create table preds_target_inc as 
		select a.*
			  ,b.G1 as p_riconsegnato_tot_g1_pub
		from preds_target_inc as a
		left join &preds_pub as b
		on a.data_gas_rif = b.data_gas_rif
		;
	quit;

	data g1_long;
		set preds_target_inc(where=(data_gas_rif < &gday_gas) 
			keep=data_Gas_rif data_calcolo p_riconsegnato_tot_g1_pub p_riconsegnato_tot: riconsegnato_tot:);
		anno_termico="&at_corrente";
	run;

	proc transpose data=g1_long out=g1_long_t(rename=(col1=prev)) name=modello;
		by data_Gas_rif;
		var p_riconsegnato_tot:;
	run;

	data g1_previsione_long;
		merge g1_long_t g1_long(keep=data_gas_rif data_calcolo ric: anno:);
		by data_Gas_rif;

		if prev ne .;
	run;

	data g1_performance_long;
		set g1_previsione_long;
		array ric{*} riconsegnato_tot_trad riconsegnato_tot_aut riconsegnato_tot_scada;
		array mape{*} mape_tradizionale	mape_autorita mape_scada;
		array incentivo{*} incentivo_tradizionale incentivo_autorita incentivo_scada;
		anno_mese=cat(yeaR(data_gas_rif),"-",put(month(data_gas_rif),z2.0));

		do i = 1 to dim(ric);
			mape[i]=abs(ric[i]-prev)/ric[i];

			/* calcolo incentivi per delibera AT16/17 */
			if data_gas_rif<&start_per2 then
				do;
					incentivo[i] = &interc_old-mape[i]*&m_1617;
					periodo_inc=1;
				end;
			else if data_gas_rif>=&start_per2 and data_gas_rif<&start_per3 then
				do;
					periodo_inc=2;

					/* calcolo incentivi per delibera AT17/18+AT18/19 fino Feb19 */
					if month(data_gas_rif) in &mesi_inverno then
						do;
							incentivo[i] = &interc1_inv_1718-mape[i]*&m1_1718;

							if mape[i] >=&mape_max then
								incentivo[i] = -&m2_1718*mape[i] +&interc2_inv_1718;
						end;

					if month(data_gas_rif) in &mesi_estate then
						do;
							incentivo[i] = -&m1_1718*mape[i] +&interc1_est_1718;

							if mape[i] >=&mape_max then
								incentivo[i] = -&m2_1718*mape[i] +&interc2_est_1718;
						end;
				end;
			else if data_gas_rif>=&start_per3 and data_gas_rif<=&end_per3 then
				do;
					periodo_inc=3;

					/* calcolo incentivi per delibera da Mar19 */
					if month(data_gas_rif) in &mesi_inverno then
						do;
							incentivo[i] = &interc1_inv_1718-mape[i]*&m1_1718;

							if mape[i] >=&mape_max then
								incentivo[i] = -&m2_1718*mape[i] +&interc2_inv_1718;
						end;

					if month(data_gas_rif) in &mesi_estate then
						do;
							incentivo[i] = -&m1_est_19*mape[i] +&interc1_est_19;

							if mape[i] >=&mape_max_est_19 then
								incentivo[i] = -&m2_1718*mape[i] +&interc2_est_1718;
						end;
				end;
		end;

		if periodo_inc<3 then
			do;
				riconsegnato_valutato=riconsegnato_tot_aut;
				MAPE_valutato=mape_autorita;
				Incentivo_valutato=incentivo_autorita;
			end;
		else if periodo_inc=3 then
			do;
				riconsegnato_valutato=riconsegnato_tot_trad;
				MAPE_valutato=mape_tradizionale;
				Incentivo_valutato=incentivo_tradizionale;
			end;

		drop data_calcolo i;
		format mape: percent8.1 inc: commax15.1;
	run;

	data g1_performance_long_at(drop=anno_termico_yday modello_old);
		set g1_performance_long;

		if month(&gm1day_gas)>=10 then
			anno_termico_yday=cat("AT",put(year(&gm1day_gas), 4.0),"/", put(year(&gm1day_gas)+1, 4.0));
		else anno_termico_yday=cat("AT",put(year(&gm1day_gas)-1, 4.0),"/", put(year(&gm1day_gas), 4.0));
		flag10_val=ifn(mape_valutato>=0.1,1,0);
		/* filtro su AT in corso */
		if anno_termico eq anno_termico_yday;
		modello_old=modello;
		select (modello);
			when ("p_riconsegnato_tot_g1")			Modello = "DAFNE";
			when ("p_riconsegnato_tot_g1_pub") 	    Modello = "SNAM";
			when ("p_riconsegnato_tot_g1_422")		Modello = "Modello 422";
			when ("p_riconsegnato_tot_g1_autoreg")	Modello = "Modello Autoreg";
			when ("p_riconsegnato_tot_g1_b")		Modello = "Modello B";
			when ("p_riconsegnato_tot_g1_full")		Modello = "Modello Full";
			when ("p_riconsegnato_tot_g1_sim")		Modello = "Modello Sim";
			when ("p_riconsegnato_tot_g1_terna")	Modello = "Modello Terna";
			otherwise;
		end;

		call symputx('anno_termico_yday',anno_termico_yday);

		if riconsegnato_tot_trad ne . and modello ne "";
	run;

	proc sql;
		create table count_mods as 
			select data_gas_rif 
				, (count(distinct(modello))) as count
			from g1_performance_long_at 
			group by data_gas_rif;
	quit;

	proc sql noprint;
		select count(*)
			, count(distinct modello) 
			, count(distinct data_gas_rif)
			into :n_preds, :dist_preds , :dist_dates
			from g1_performance_long_at;
	run;

	/*%put &n_preds &dist_preds ;*/
	%if &n_preds > 0 & &dist_preds >1 %then %do;

			PROC SQL;
				CREATE TABLE WORK.riassunto_overall AS 
					SELECT t1.MODELLO, 
						(count(t1.data_gas_rif))  AS n_giorni,  
						(AVG(t1.mape_valutato)) FORMAT=PERCENT8.2 AS 'MAPE Medio'n, 
						(SUM(t1.incentivo_valutato)) FORMAT=COMMAX15.1 AS Incentivo,
						std(t1.mape_valutato) FORMAT=PERCENT9.2 AS 'MAPE DevStd'n,
						max(t1.mape_valutato) FORMAT=PERCENT9.2 AS 'MAPE Max'n,
						(mean(flag10_val)) FORMAT=PERCENT8.2  as 'Errori>10%'n,
						(1-mean(flag10_val)) FORMAT=PERCENT8.2  as Flag10,
						sum(flag10_val) as count_days_flag10
					FROM g1_performance_long_at t1 
						GROUP BY t1.MODELLO;
			QUIT;

			proc transpose data=riassunto_overall(Drop=Incentivo n_giorni count_days_flag10 Flag10) 
				out=riassunto_overall_plot(rename=(col1=value)) name=stat;
				by modello;
			run;

			data _null_;
				set riassunto_overall;
				if modello eq "DAFNE" then
					call symputx('n_giorni_all',n_giorni);
			run;

			data riassunto_overall_plot;
				set riassunto_overall_plot;
				format value percent8.1;
			run;

			ods graphics on / width=1600 height=900;

			proc sgplot data=riassunto_overall_plot noautolegend;
				title2 height=16pt underlin=0 bold "Modelli G+1 | Performance &anno_termico_yday";
				yaxis type=linear label= "Errore Percentuale [%]" values=(0 to 0.4 by 0.05) labelattrs=(size=14 weight=bold);
				xaxis label= "Metriche di Performance"  discreteorder=data 
					labelattrs=(size=14 weight=bold) VALUEATTRS=(Size=12 );
				vbar stat  / name="perf" response=value group=modello  GROUPDISPLAY=CLUSTER grouporder=data datalabel datalabelattrs=(size=12 weight=bold)
					datalabelfitpolicy=split splitchar="." datalabelpos=data
					FILL FILLTYPE=solid DATASKIN=none OUTLINE OUTLINEATTRS=(COLOR=black THICKNESS=1);
				keylegend "perf"/ AUTOITEMSIZE  valueattrs=(size=12 ) border exclude=("") 
					down=3 location=outside  position=bottomleft;
				styleattrs datacolors= &palette_list2;
				INSET "N° Giorni Totali = &n_giorni_all" "N° Giorni ensemble completo = &n_giorni_full"  / border TEXTATTRS=(size=11 weight=bold) TITLE="Giorni Valutati" TITLEATTRS=(size=12 weight=bold);

				/* inserisci box con n giorni valutati per DAFNE e n giorni per Full,Sim,Terna */
			run;

			/*===========================*/
			PROC SQL;
				CREATE TABLE WORK.riassunto_overall_am AS 
					SELECT  t1.MODELLO, t1.anno_mese, 
						(count(t1.data_gas_rif))  AS n_giorni,  
						(AVG(t1.mape_valutato)) FORMAT=PERCENT8.2 AS 'MAPE Medio'n, 
						(SUM(t1.incentivo_valutato)) FORMAT=COMMAX15.1 AS Incentivo,
						std(t1.mape_valutato) FORMAT=PERCENT9.2 AS 'MAPE DevStd'n,
						max(t1.mape_valutato) FORMAT=PERCENT9.2 AS 'MAPE Max'n,
						(mean(flag10_val)) FORMAT=PERCENT8.2  as 'Errori>10%'n,
						(1-mean(flag10_val)) FORMAT=PERCENT8.2  as Flag10,
						sum(flag10_val) as count_days_flag10
					FROM g1_performance_long_at t1 
						GROUP BY t1.MODELLO, t1.anno_mese;
			QUIT;

			data riassunto_overall_am_mc;
				set riassunto_overall_am;
/*				am_corso=cat(yeaR(&gm1day_gas),"-",put(month(&gm1day_gas),z2.0));*/
				if anno_mese="&am_corso";
/*				call symputx('am_corso',am_corso);*/
			run;

			data _null_;
				set riassunto_overall_am_mc;

				if modello eq "DAFNE" then
					call symputx('n_giorni_mc_all',n_giorni);
				%let n_giorni_mc_full=0;

				if modello eq "Modello Full" then
					call symputx('n_giorni_mc_full',n_giorni);
			run;

			proc transpose data=riassunto_overall_am_mc(Drop=Incentivo n_giorni count_days_flag10 Flag10) 
				out=riassunto_mc_plot(rename=(col1=value)) name=stat;
				by modello;
			run;

			data riassunto_mc_plot;
				set riassunto_mc_plot;
				format value percent8.1;
			run;

		/* Eseguo il report sulle performance storiche su am solo se n° giorni >=3 */
			proc sql noprint;
				select max(n_giorni) into: n_giorni_am
				from riassunto_overall_am_mc
				;
			quit;

		%if &n_giorni_am>=3 %then %do;

			/* Rendo dinamica la soglia massima dell'asse y */
				proc sql noprint;
					select max(value) into: max_value
					from riassunto_mc_plot
					;
				quit;

				data _null_;
					max_value=&max_value;
					round_max_value=round(max_value,0.1);
					if round_max_value<max_value then do;
						max_value_perc=sum(round_max_value,0.1);
					end;
					else do;
						max_value_perc=round_max_value;
					end;
					max_value_perc_final=max(0.4,max_value_perc);
					call symput("max_value_y",max_value_perc_final);
				run;

				proc sgplot data=riassunto_mc_plot noautolegend;
					title2 height=16pt underlin=0 bold "Modelli G+1 | Performance &am_corso";
					yaxis type=linear label= "Errore Percentuale [%]" values=(0 to &max_value_y by 0.05) labelattrs=(size=14 weight=bold);
					xaxis label= "Metriche di Performance"  discreteorder=data 
						labelattrs=(size=14 weight=bold) VALUEATTRS=(Size=12 );
					vbar stat  / name="perf" response=value group=modello  GROUPDISPLAY=CLUSTER grouporder=data datalabel datalabelattrs=(size=12 weight=bold)
						datalabelfitpolicy=split splitchar="." datalabelpos=data
						FILL FILLTYPE=solid DATASKIN=none OUTLINE OUTLINEATTRS=(COLOR=black THICKNESS=1);
					keylegend "perf"/ AUTOITEMSIZE  valueattrs=(size=12 ) border exclude=("") 
						down=3 location=outside  position=bottomleft;
					styleattrs datacolors= &palette_list2;
					INSET "N° Giorni Totali = &n_giorni_mc_all" "N° Giorni ensemble completo = &n_giorni_mc_full"  / border TEXTATTRS=(size=11 weight=bold) TITLE="Giorni Valutati" TITLEATTRS=(size=12 weight=bold);

					/* inserisci box con n giorni valutati per DAFNE e n giorni per Full,Sim,Terna */
				run;

			%end;

				/*------------*/
				*incentivo='Incentivo Mensile'n incentivo_cumulato='Incentivo Cumulato'n);
				data riassunto_overall_am_plot(drop=mod_prev);
					length datalabel datalabel_cum $10;
					set riassunto_overall_am(keep= anno_mese modello incentivo n_giorni);
					retain incentivo_cumulato mod_prev;
					incentivo=incentivo/1000;

					/*	incentivo=round(incentivo,10);*/
					if _N_=1 or mod_prev ne modello then
						do;
							mod_prev=modello;
							incentivo_cumulato=incentivo;
/*							datalabel=put(incentivo, eurox8.0);*/
							datalabel=put(incentivo, 8.0);

							/*datalabel_cum=compress(catx(" ", "€",put(incentivo_cumulato, 8.0)));*/
							datalabel_cum="";
							/*compress(put(incentivo_cumulato, 8.0));*/
						end;
					else
						do;
							incentivo_cumulato=incentivo_cumulato+incentivo;

/*							datalabel=put(incentivo, eurox8.0);*/
/*							datalabel_cum=put(incentivo_cumulato, eurox8.0);*/
							datalabel=compress(put(incentivo, 8.0));
							datalabel_cum=compress(put(incentivo_cumulato, 8.0));
						end;

					*€;
					format inc: eurox15.0;
				run;

				data _null_;
					set riassunto_overall_am_plot(where=(modello="DAFNE")) end=eof;
					if eof then do;
							call symputx('n_giorni_mc',n_giorni);
					end;
				run;

				proc sql noprint;
				        select max(incentivo_cumulato)
								, max(incentivo)
								, min(incentivo_cumulato)
								, min(incentivo)
								into: max_inc_cum, :max_inc, 
									:min_inc_cum, :min_inc
				        from riassunto_overall_am_plot
						where modello in ("DAFNE" "SNAM")
				        ;
				quit;
				
				
				data _null_;
					max_yaxe =  round(max(0,&max_inc_cum, &max_inc)/1000)*1000;
					min_yaxe =  round((min(0,&min_inc_cum, &min_inc))/1000)*1000;
					by_val = round(((max_yaxe-min_yaxe)/4)/1000)*1000;
					call symput("min_yaxe", min_yaxe - (by_val/2) );
					call symput("max_yaxe", max_yaxe + by_val );
					call symput("by_val", by_val);
				run;

				proc sgplot data=riassunto_overall_am_plot(where=(modello="DAFNE" or  modello='SNAM')) noautolegend;
				    title2 height=16pt underlin=0 bold "Modello G+1 | Incentivo DAFNE vs SNAM";
				    yaxis type=linear label="Incentivo [10^3 Euro]" labelattrs=(size=14 weight=bold)
				        values=(&min_yaxe to &max_yaxe  by &by_val);
				    xaxis grid type=discrete label= "Anno-Mese" discreteorder=data labelattrs=(size=14 weight=bold) VALUEATTRS=(Size=12 );
				    vbar anno_mese  / response=incentivo name="inc" group=modello GROUPDISPLAY=CLUSTER grouporder=data
				        dataskin=none datalabel=datalabel datalabelattrs=(size=9 weight=bold) datalabelfitpolicy=split
				        datalabelpos=data  FILL FILLTYPE=solid  DATASKIN=none OUTLINE OUTLINEATTRS=(COLOR=black THICKNESS=1);
				    vline anno_mese  / response=incentivo_cumulato name="inc_cum"  group=modello GROUPDISPLAY=CLUSTER grouporder=data
				        dataskin=sheen markers  markerattrs=(symbol=CircleFilled SIZE=12) 
				        datalabel=datalabel_cum datalabelpos=data datalabelattrs=(size=10 weight=bold) lineattrs=( thickness=4);
				    keylegend "inc"  "inc_cum" / AUTOITEMSIZE  valueattrs=(size=12 ) border exclude=("") down=1 location=outside  position=bottomleft;
				    styleattrs datacolors= &palette_list4 datacontrastcolors= &palette_list4;
				    INSET "N° Giorni Totali = &n_giorni_all" "N° Giorni mese in corso = &n_giorni_mc"  / border TEXTATTRS=(size=11 weight=bold) TITLE="Giorni Valutati" 
				        TITLEATTRS=(size=12 weight=bold);
				run;


		%end;
%mend;

%macro graph_gmods_perf(g_ora_conv,title2);

/*%let g_ora_conv=output_g_diana_perf_giorn;*/
/*%let title2=Modello G DIANA | Distribuzione Ora di Convergenza;*/

	data master_ore;
		do ora_pub_key=1 to 24;
			ora_convergenza=ora_pub_key+6;

			if ora_convergenza>23 then
				ora_convergenza=ora_convergenza-24;

			if ora_pub_key=24 then
				ora_convergenza=-1;
			output;
		end;
	run;

	proc sql;
		create table g_distrib_conv as 
		select ora_pub_key as ora_pub_key
			 , ora_convergenza as ora_convergenza
			 , (count(data_gas_rif)) as count_days 
		from &g_ora_conv  
		group by ora_pub_key, ora_convergenza
		;
	quit;

	data g_distrib_conv;
		merge master_ore g_distrib_conv;
		by ora_pub_key ora_convergenza;
		if COUNT_days eq . then COUNT_days=0;
	run;

	proc sql noprint;
		select sum( COUNT_days) into :tot_days
			from g_distrib_conv;
	run;

	data g_distrib_conv(drop=old_prob);
		set g_distrib_conv;
		retain old_prob prob_cum;
		prob=COUNT_days/&tot_days;
		tot_days=&tot_days;
		call symputx('tot_days',tot_days);

		if _N_=1 then
			do;
				old_prob=prob;
				prob_cum=prob;
			end;
		else
			do;
				prob_cum=prob+prob_cum;
				old_prob=prob;
			end;

		soglia_mae_gwh=&soglia_mae;
		soglia_salto_prev_gwh=&soglia_salto_prev;
		format prob: percent8.0;
	run;

	proc sql noprint;
		select min(ora_pub_key) into :ora_conv_med from g_distrib_conv where prob_cum ge 0.5;
	run;

	data g_distrib_conv;
		length ora_convergenza_label $20;
		set g_distrib_conv;

		if ora_pub_key=&ora_conv_med then
			conv_prob=prob_cum;
		else conv_prob=0;

		if ora_convergenza>=0 then
			ora_convergenza_label=catx(" ", "Ore",put(ora_convergenza,2.0));
		else ora_convergenza_label="Non Converge";
	run;

	proc sgplot data=g_distrib_conv noautolegend;
		title2 height=16pt underlin=0 bold "&title2";
		yaxis type=linear label= "Frequenza [%]" values=(0 to 1 by 0.1) labelattrs=(size=14 weight=bold);
		xaxis label= "Ora di Pubblicazione"  discreteorder=data 
			labelattrs=(size=14 weight=bold) VALUEATTRS=(Size=11 );
		vbar ora_convergenza_label  / response=prob_cum name="prob_conv" 
			datalabel datalabelattrs=(size=10.5 weight=bold)
			datalabelfitpolicy=split splitchar="." datalabelpos=data
			FILL FILLTYPE=solid fillattrs=(color=lightskyblue) 
			DATASKIN=none OUTLINE OUTLINEATTRS=(COLOR=black THICKNESS=1);
		vbar ora_convergenza_label  / response=conv_prob
			FILL FILLTYPE=solid fillattrs=(color=styg) 
			DATASKIN=none OUTLINE OUTLINEATTRS=(COLOR=black THICKNESS=1);
		INSET "N° Giorni Totali = &tot_days" "Soglia = 2.5 M smc"  / border TEXTATTRS=(size=11 weight=bold);
	run;

	*TITLE="Giorni Valutati" TITLEATTRS=(size=12 weight=bold);
%mend;
%macro graph_gmods_perf_mc(g_ora_conv,title2);


	/*	%let g_ora_conv=output_g_diana_perf_giorn;*/
	data output_g_perf_giorn_mc;
		set &g_ora_conv;
		anno_mese=cat(year(data_gas_rif),"-",put(month(data_gas_rif),z2.0));
/*		am_corso=cat(year(&gm1day_gas),"-",put(month(&gm1day_gas),z2.0));*/
		if anno_mese="&am_corso" and mean_mae_trad_allday ne .;
/*		call symputx('am_corso',am_corso);*/
	run;

	/* Eseguo grafico solo se n di giorni >=3 */
	proc sql noprint;
		select count(distinct data_gas_rif) into: n_giorni_am
		from output_g_perf_giorn_mc
		;
	quit;

	%if &n_giorni_am >=3 %then %do;

		data master_ore;
			do ora_pub_key=1 to 24;
				ora_convergenza=ora_pub_key+6;

				if ora_convergenza>23 then
					ora_convergenza=ora_convergenza-24;

				if ora_pub_key=24 then
					ora_convergenza=-1;
				output;
			end;
		run;

		proc sql;
			create table g_distrib_conv as 
			select ora_pub_key 
				  ,ora_convergenza 
				  , (count(data_gas_rif)) as count_days 
			from output_g_perf_giorn_mc 
			group by ora_pub_key, ora_convergenza;
		quit;

		data g_distrib_conv;
			merge master_ore g_distrib_conv;
			by ora_pub_key ora_convergenza;
			if count_days eq . then count_days=0;
		run;

		proc sql noprint;
			select sum( count_days) into :tot_days
			from g_distrib_conv;
		run;

		data g_distrib_conv(drop=old_prob);
			set g_distrib_conv;
			retain old_prob prob_cum;
			prob=COUNT_days/&tot_days;
			tot_days=&tot_days;
			call symputx('tot_days',tot_days);

			if _N_=1 then
				do;
					old_prob=prob;
					prob_cum=prob;
				end;
			else
				do;
					prob_cum=prob+prob_cum;
					old_prob=prob;
				end;
			format prob: percent8.0;
		run;

		proc sql noprint;
			select min(ora_pub_key) into :ora_conv_med 
			from g_distrib_conv where prob_cum ge 0.5;
		run;

		data g_distrib_conv;
			length ora_convergenza_label $20;
			set g_distrib_conv;

			if ora_pub_key=&ora_conv_med then
				conv_prob=prob_cum;
			else conv_prob=0;

			if ora_convergenza>=0 then
				ora_convergenza_label=catx(" ", "Ore",put(ora_convergenza,2.0));
			else ora_convergenza_label="Non Converge";
		run;

		proc sgplot data=g_distrib_conv noautolegend;
			title2 height=16pt underlin=0 bold "&title2";
			yaxis type=linear label= "Frequenza [%]" values=(0 to 1 by 0.1) labelattrs=(size=14 weight=bold);
			xaxis label= "Ora di Pubblicazione"  discreteorder=data 
				labelattrs=(size=14 weight=bold) VALUEATTRS=(Size=11 );
			vbar ora_convergenza_label  / response=prob_cum name="prob_conv" 
				datalabel datalabelattrs=(size=10.5 weight=bold)
				datalabelfitpolicy=split splitchar="." datalabelpos=data
				FILL FILLTYPE=solid fillattrs=(color=lightskyblue) 
				DATASKIN=none OUTLINE OUTLINEATTRS=(COLOR=black THICKNESS=1);
			vbar ora_convergenza_label  / response=conv_prob
				FILL FILLTYPE=solid fillattrs=(color=styg) 
				DATASKIN=none OUTLINE OUTLINEATTRS=(COLOR=black THICKNESS=1);
			INSET "N° Giorni Totali = &tot_days" "Soglia = 2.5 M smc"  / border TEXTATTRS=(size=11 weight=bold);
		run;
	%end;
	*TITLE="Giorni Valutati" TITLEATTRS=(size=12 weight=bold);
%mend;


%macro get_mail();

	/* crea dset_filter_G1_tmw contenente le previsioni effettuate adesso (hour_calcolo=hour(&datetime_now))
		riferite a domani (filtro data_gas_rif=&g1day_gas_sistema) 
	  --> contiene previsione p_tot_g1_tmw, p_civ_g1_tmw, p_term_g1_tmw */

	%get_preds_g1(ds_in=predictions_targets_wide, 
				ds_in_pub=for_svil.pubblicazioni_snam, ds_out=dset_filter_G1_tmw, 
				date_rif=&g1day_gas_sistema);

	/* Filtra le predictions_targets_wide:
		- per data_gas_rif= &gday_gas and hour_calcolo = &hour_inc_yday (tengo p_riconsegnato_tot_g1 p_riconsegnato_civi_g1 p_riconsegnato_term_g1 + date)
		- data_gas_rif= &gday_gas_sistema and hour_calcolo = hour(&datetime_now) (tengo p_riconsegnato_tot_g_diana + le date)
	--> merge dei due dataset creando dset_filter_tday creando mape_g1_g_tday=abs(p_tot_g_tday-p_tot_g1_tday)/p_tot_g_tday
	*/
	%get_preds_g_g1inc_oggi(ds_in=predictions_targets_wide, ds_in_pub=for_svil.pubblicazioni_snam, ds_out=dset_filter_tday);


	/* Performance passate g1:
	- prende da preds_target_tot_inc_perf filtrando su data_gas_rif = &gm1day_gas_sistema (giorno gas-1D)
	- statistiche su mape_valutato (mean, max, std) per anno_termico (da 01OCT2019) e per mese in corso,
		che salvo rispettivamente in PERF_PROD (una riga per AT) e PERF_MESE_CORSO (una riga per AT e mese in corso) 
	*/
	%get_perf_g1(ds_in=preds_target_tot_inc_perf, ds_out=dset_filter_g1_yday);

	/* Performance passate g:
	- prende da predictions_targets_wide filtrando su data_gas_rif  <= &gm1day_gas_sistema (giorno gas-1D) 
		e tenendo ric: p_riconsegnato_tot_g hour: data_Gas_rif
	- calcola varie stat + ora di convergenza
	*/
	%get_perf_g(predictions_targets_wide,output_g_diana_perf_giorn, output_g_diana_perf_stats, gdiana);

	/* Crea il dataset tmw_today tramite un merge dset_filter_G1_tmw e dset_filter_tday */
	data tmw_today;
		merge dset_filter_g1_tmw(rename=(data_Gas_rif=data_Gas_rif_tmw)) 
			dset_filter_tday(rename=(data_Gas_rif=data_Gas_rif_tday));
		by dt_calcolo;
	run;

	/* Crea il dataset past tramite una merge dset_filter_g1_yday, output_g_perf_stats e output_g_perf_giorn */

	data past;
			merge dset_filter_g1_yday (keep=dt_calcolo anno_termico ric_tot_trad_yday 
									p_tot_g1_yday p_tot_g1_yday_pub
 									ric_tot_scada_yday 
									ric_civ_yday 
									ric_term_yday ric_ind_yday
								mape_trad_yday mape_trad_yday_pub
								mape_scada_yday mape_scada_yday_pub
								incentivo_trad_yday incentivo_trad_yday_pub
								count_mc mean_mape_mc mean_mape_mc_pub 
								sum_incentivo_mc sum_incentivo_mc_pub 
								count_prod 
								mean_mape_prod mean_mape_prod_pub
								sum_incentivo_prod sum_incentivo_prod_pub)
			output_g_diana_perf_stats
			output_g_diana_perf_giorn (where=(data_gas_rif_g_yday eq &gm1day_gas_sistema) 
									rename=(data_gas_rif=data_gas_rif_g_yday));

		by dt_calcolo;
	run;


%mend get_mail;

%macro create_report();

	/* Check su eventuali previsioni mancanti del modello G DIANA */
	proc sql noprint;
		select count(*) into :count_prev_analisi 
		from predictions_targets_tot_long_mis (where=(dt_calcolo =&datetime_now and modello="Predizione G per G - DIANA"));
	quit;
	
	data miss_diana;
		dt_calcolo=&datetime_now;
		count_prev_analisi=&count_prev_analisi;
		if count_prev_analisi=0 then do;
			call symput("outlier_g_diana_flag",2);
		end;
		else do;
			call symput("outlier_g_diana_flag",0);	
		end;
		format dt_calcolo datetime19.;
	run;

	/* Check valori missing / outlier sul G+1 (segnalo G+1 anche quando G+1 riferito a oggi ha errore alto (valutato col G)) */
	%analisi_andamento_giornaliero(predictions_targets_tot_long, &datetime_now ,"Predizione G per G+1", output_g1);

	data _null_;
		set output_g1;
		call symput('outlier_g1_flag',outlier_final);
	run;

	ods exclude none;
	options
		papersize=A4
		leftmargin=1cm
		rightmargin=1cm
		bottommargin=1cm
		topmargin=1cm
		ORIENTATION=LANDSCAPE;

	ods graphics on / width=1600 height=900;
	ods _all_ close;

	ods pdf file="&report_path.&pdf_filename" author="Business Integration Partners S.p.a." 
		nobookmarkgen nogfootnote nopdfnote notoc nocontents  subject="Monitoring Predictions" 
		style=HTMLBlue 	startpage=no dpi=250 nogtitle;

	/************************************/
	/* PREVISIONI GIORNO GAS G+1 */
	title1  height=24pt underlin=2 bold "Previsioni giorno gas G+1 | %sysfunc(strip(&dayg1))";
	
	/* Previsioni giorno gas G+1 | &g1day_gas 
		per il Riconsegnato Totale 
		Prende da predictions_targets_tot_long le previsioni riferite a domani*/
	%graph_preds_rictot_refdate(dataset_tot=predictions_targets_tot_long, dataset_wide=predictions_targets_wide,
		date=&g1day_gas,flag=&outlier_g1_flag);

	/* Previsioni giorno gas G+1 -- utenze */
	%graph_g1_utenze(predictions_targets_wide,predictions_targets_ut_long, &g1day_gas);

	/* Previsioni giorno gas G+1 -- G+1 e i 6 sotto-modelli */
	%graph_g1_tot_allmodels(predictions_targets_wide, predictions_targets_tot_long, &g1day_gas);
	ods pdf STARTPAGE=NOW;


	/************************************/
	/* PREVISIONI GIORNO GAS G */
	title1  height=24pt underlin=2 bold "Previsioni giorno gas G | %sysfunc(strip(&dayg))";

	/* Previsioni giorno gas g - modelli di trend, G+1 e G*/
	%graph_preds_rictot_refdate(dataset_tot=predictions_targets_tot_long, dataset_wide=predictions_targets_wide,
		date=&gday_gas, flag=&outlier_g_diana_flag);

	/* Andamento giorno gas in corso */
	%graph_predstday_g(predictions_targets_wide,predictions_targets_tot_long);
	ods pdf STARTPAGE=NOW;

	/************************************/
	/* PERFORMANCE giorno gas G-1 */
	title1  height=24pt underlin=2 bold "Performance giorno gas G-1 | %sysfunc(strip(&daygm1))";

	%graph_preds_rictot_refdate(dataset_tot=predictions_targets_tot_long, dataset_wide=predictions_targets_wide,
		date=&gm1day_gas, flag=0);

	%graph_predstday_g_new(predictions_targets_wide,predictions_targets_tot_long, output_g_diana_perf_giorn, &gm1day_gas);

	/************************************/
	/* Performance storiche */
	ods pdf STARTPAGE=NOW;
	title1  height=24pt underlin=2 bold "Performance storiche";

	/* Performance storiche modelli G+1
	1. stat su MAPE per AT in corso 
	2. stat su MAPE per il mese in corso 
	3. incentivo mensile da inizio anno termico
	*/
	%graph_g1mods_perf(preds_g1=predictions_targets_wide, preds_pub=for_svil.pubblicazioni_snam );

	/* Distribuzione ora di convergenza modello G */
	%graph_gmods_perf(output_g_diana_perf_giorn,Modello G DIANA | Distribuzione Ora di Convergenza &at_corrente);
	%graph_gmods_perf_mc(output_g_diana_perf_giorn,Modello G DIANA | Distribuzione Ora di Convergenza &am_corso);
	ods pdf STARTPAGE=NOW;

	/************************************/
	/* Modelli G+2, G+3 e G+4 */
	title1  height=24pt underlin=2 bold "Previsioni giorno gas G+2 | %sysfunc(strip(&dayg2))";
	%graph_preds_rictot_refdate(dataset_tot=predictions_targets_tot_long, dataset_wide=predictions_targets_wide,
		date=&g2day_gas,flag=0);
	ods pdf STARTPAGE=NOW;

	title1  height=24pt underlin=2 bold "Previsioni giorno gas G+3 | %sysfunc(strip(&dayg3))";
	%graph_preds_rictot_refdate(dataset_tot=predictions_targets_tot_long, dataset_wide=predictions_targets_wide,
		date=&g3day_gas,flag=0);
	ods pdf STARTPAGE=NOW;

	title1  height=24pt underlin=2 bold "Previsioni giorno gas G+4 | %sysfunc(strip(&dayg4))";
	%graph_preds_rictot_refdate(dataset_tot=predictions_targets_tot_long, dataset_wide=predictions_targets_wide,
		date=&g4day_gas, flag=0);
	ods pdf close;

%mend create_report;


%macro Invio_mail();

	data flags_tmw_today;
		set tmw_today;
		if p_tot_g1_tmw eq . then flag_tmw=0;
		else flag_tmw=1;

		if p_tot_g_tday eq . then flag_tday_g=0;
		else flag_tday_g=1;

		if p_tot_g1_tday eq . then flag_tday_g1=0;
		else flag_tday_g1=1;

		if data_Gas_rif_tmw = &g1day_gas then flg_prev15_tmw=1;
		else flg_prev15_tmw=0;

		call symputx('flag_tmw',flag_tmw);
		call symputx('flag_tday_g',flag_tday_g);
		call symputx('flag_tday_g1',flag_tday_g1);
		call symput('flg_prev15_tmw',flg_prev15_tmw);
	run;

	data flag_past;
		set past;
		if ric_tot_trad_yday eq . then flag_bil=0;
		else flag_bil=1;

		if ric_tot_scada_yday eq . then flag_scada=0;
		else flag_scada=1;

		if count_mc <2 then flag_past_mc=0;
		else flag_past_mc=1;

		if count_prod <2 then flag_past_prod=0;
		else flag_past_prod=1;

		if p_tot_g1_yday eq . then flag_g1_yday=0;
		else flag_g1_yday=1;

		if count_dates_g_mc <2 then flag_past_g_mc=0;
		else flag_past_g_mc=1;

		call symputx('flag_conv_g_yday',flag_convergenza);
		call symputx('flag_past_mc',flag_past_mc);
		call symputx('flag_past_prod',flag_past_prod);
		call symputx('flag_scada',flag_scada);
		call symputx('flag_bil',flag_bil);
		call symputx('flag_g1_yday',flag_g1_yday);
	run;

	data add_values_yday(keep=dt_calcolo yday: past:);
		set past;
		yday_ric_trad = put(ric_tot_trad_yday, commax21.);
		yday_ric_scada = put(ric_tot_scada_yday, commax21.);
		yday_civ = put(ric_civ_yday, commax21.);
		yday_term = put(ric_term_yday, commax21.);
		yday_ind = put(ric_ind_yday, commax21.);

		yday_p_g1_tot = put(p_tot_g1_yday, commax21.);
		yday_p_g1_tot_pub = put(p_tot_g1_yday_pub, commax21.);

		yday_p_g1_tot_mape_trad = put(mape_trad_yday, percent8.1);
		yday_p_g1_tot_mape_trad_pub = put(mape_trad_yday_pub, percent8.1);
		yday_p_g1_tot_mape_scada = put(mape_scada_yday, percent8.1);
		yday_p_g1_tot_mape_scada_pub = put(mape_scada_yday_pub, percent8.1);
		yday_p_g1_inc = put(incentivo_trad_yday, commax21.);
		yday_p_g1_inc_pub = put(incentivo_trad_yday_pub, commax21.);

		yday_p_g_oraconv=put(ora_convergenza, 2.0);
		yday_p_g_mae=put(mean_mae_trad_allday/(1000000*10.57275), 6.1);
		yday_p_g_mae_max=put(max_mae_trad_allday/(1000000*10.57275), 6.1);

		past_g1_prod_inc=put(sum_incentivo_prod, commax21.);
		past_g1_prod_inc_pub=put(sum_incentivo_prod_pub, commax21.);

		past_g1_prod_mape=put(mean_mape_prod, percent8.2);
		past_g1_prod_mape_pub=put(mean_mape_prod_pub, percent8.2);

		past_g1_mc_inc=put(sum_incentivo_mc, commax21.);
		past_g1_mc_inc_pub=put(sum_incentivo_mc_pub, commax21.);

		past_g1_mc_mape=put(mean_mape_mc, percent8.1);
		past_g1_mc_mape_pub=put(mean_mape_mc_pub, percent8.1);

		past_g_prod_ora=put(median_ora_conv_prod, 2.0);
		past_g_prod_mae=put(mean_mae_trad_allday_prod/10.57275, 6.1);
		past_g_mc_ora=put(median_ora_conv_mc, 2.0);
		past_g_mc_mae=put(mean_mae_trad_allday_mc/10.57275, 6.1);
		past_cons_mc=put(count_mc, 2.0);
		past_cons_prod=put(count_prod, 4.0);
	run;

	data add_values_tmw_today(keep=dt_calcolo tday: tmw:);
		set tmw_today;
		tday_p_g1_tot = put(p_tot_g1_tday, commax21.);
		tday_p_g1_tot_pub = put(p_tot_g1_tday_pub, commax21.);
		tday_p_g1_civ = put(p_civ_g1_tday, commax21.);
		tday_p_g1_term = put(p_term_g1_tday, commax21.);
		tday_p_g_tot = put(p_tot_g_tday, commax21.);
		tday_mape_stim = put(mape_g1_g_tday, percent8.1);
		tday_mape_stim_pub = put(mape_g1_g_tday_pub, percent8.1);
		tmw_p_g1_tot = put(p_tot_g1_tmw, commax21.);
		tmw_p_g1_civ = put(p_civ_g1_tmw, commax21.);
		tmw_p_g1_term = put(p_term_g1_tmw, commax21.);
		tmw_p_g1_tot_inc = put(p_tot_g1_tmw_inc, commax21.);
		tmw_p_g1_tot_inc_pub = put(p_tot_g1_tmw_inc_pub, commax21.);
		call symput("prev_pub_inc",p_tot_g1_tmw_inc_pub);
	run;

	data format_Dates;
		tmw_gas = put(&g1day_gas, eurdfwkx.);
		tday_gas = put(&gday_gas, eurdfwkx.);
		yday_gas = put(&gm1day_gas, eurdfwkx.);
		y2day_gas = put(&gm2day_gas, eurdfwkx.);
		tday_short = put(&gday_gas, ddmmyyp10.);
		call symputx("g1day_gas_format", tmw_gas);
		call symputx("gday_gas_format", tday_gas);
		call symputx("gm1day_gas_format", yday_gas);
		call symputx("gm2day_gas_format", y2day_gas);
		call symputx("gday_gas_short", tday_short);
	run;

	data testo_mail(keep=dt_calcolo  riga:);
		length riga1-riga34 $600;
		merge add_values_tmw_today add_values_yday;
		by dt_calcolo;
		array righe {*} riga:;
		do i=1 to dim(righe);
			righe[i]='';
		end;

		if &flag_tmw=0 then do;
			riga1 = cat("Si sono verificate delle anomalie nell'elaborazione delle previsioni riferite a &g1day_gas_format");
		end;
		else do;
			riga1 = cat("Di seguito le previsioni generate dai modelli G+1 riferite a &g1day_gas_format:");
			riga2 = cat("	- Modello DAFNE: ", compress(tmw_p_g1_tot), " KWh");

			if &outlier_g1_flag=1 then do;
				riga3 = cat("	- la previsione DAFNE risulta un outlier");
			end;

			riga4 = cat("	- Modello Civile: ", compress(tmw_p_g1_civ), " KWh");
			riga5 = cat("	- Modello Termoelettrico: ", compress(tmw_p_g1_term), " KWh");
			riga33 = cat("	- Modello DAFNE (ore 15): ", compress(tmw_p_g1_tot_inc), " KWh");
			riga34 = cat("	- Pubblicazione da incentivo (ore 15): ", compress(tmw_p_g1_tot_inc_pub), " KWh");
		end;

		if &flag_tday_g1=0 and &flag_tday_g=0 then do;
			riga6 = cat("Sono presenti delle anomalie nelle previsioni riferite a oggi");
		end;
		else do;
			if /*&flag_pubb_calc=1 and*/ &hour_pubb ne 6 then do;
				riga6 = cat("Di seguito le previsioni riferite al giorno gas in corso:");
			end;
			else do;
				riga6 = cat("Di seguito le previsioni riferite al giorno gas &gday_gas_format in corso:");
			end;

			if &flag_tday_g=0 then do;
				riga7 = cat("	- Modello G DIANA non disponibile");
			end;
			else do;
				riga7 = cat("	- Modello G DIANA: ", compress(tday_p_g_tot), " KWh");

				if &outlier_g_diana_flag=2 then do;
					riga8 = cat("	- la previsione del modello G DIANA risulta mancante ed è stata sostituita con l'ultima previsione disponibile");
				end;
			end;

			if &flag_tday_g1=0 then do;
				riga9=cat("	- anomalia nelle previsioni generate ieri nella pubblicazione delle 15");
			end;
			else do;
				riga9 = cat("	- Modello DAFNE (ore 15): ", compress(tday_p_g1_tot), " KWh");
				riga10= cat("	- Dato pubblicato (ore 15): ", compress(tday_p_g1_tot_pub), " KWh"); 
				riga11 = cat("	- Modello Civile (ore 15): ", compress(tday_p_g1_civ), " KWh");
				riga12 = cat("	- Modello Termoelettrico (ore 15): ", compress(tday_p_g1_term), " KWh");
			end;

			if &flag_tday_g=1 and &flag_tday_g1=1 then do;
				riga13 = cat("	- la previsione di DAFNE di ieri ha un MAPE stimato del ",compress(tday_mape_stim));
				riga14=cat("	- la previsione pubblicata di ieri ha un MAPE stimato del ",compress(tday_mape_stim_pub));
			end;
		end;

		if &flag_g1_yday= 0 then do;
			riga15 = cat("Sono presenti delle anomalie nelle previsioni riferite a ieri");
		end;
		else do;
			if /*&flag_pubb_calc=1 and*/ &hour_pubb ne 6 then do;
				riga15 = cat("Di seguito i consuntivi e le performance riferite a ieri:");
			end;
			else do;
				riga15 = cat("Di seguito i consuntivi e le performance riferite a due giorni fa:");
			end;

			if &flag_scada =0 and &flag_bil=0 then do;
				riga16 = cat("	- non è presente un consuntivo che permette di valutare le previsioni");
				riga17 = cat("	- la previsione di DAFNE è stata ", compress(yday_p_g1_tot) ," KWh");
				riga18= cat("	- la previsione pubblicata è stata ", compress(yday_p_g1_tot_pub) ," KWh");
			end;
			else if &flag_bil=0 then do;
				riga19=cat("	- c’è stato un consuntivo scada di ", compress(yday_ric_scada) ," KWh");
				riga20 = cat("	- la previsione di DAFNE è stata ", compress(yday_p_g1_tot), " KWh e ha un MAPE scada del ", compress(yday_p_g1_tot_mape_scada));
				riga21=cat("	- la previsione pubblicata è stata ", compress(yday_p_g1_tot_pub), " KWh e ha un MAPE scada del ", compress(yday_p_g1_tot_mape_scada_pub));
			end;
			else do;
				riga19=cat("	- c’è stato un consuntivo tradizionale di ", compress(yday_ric_trad) ," KWh");
				riga20 = cat("	- la previsione di DAFNE è stata ", compress(yday_p_g1_tot), " KWh e ha un MAPE del ", compress(yday_p_g1_tot_mape_trad));
				riga21=cat("	- la previsione pubblicata è stata ", compress(yday_p_g1_tot_pub), " KWh e ha un MAPE del ", compress(yday_p_g1_tot_mape_trad_pub));
				riga22 = cat("	- il riconsegnato Civile corrisponde a ", compress(yday_civ)," KWH, quello Termoelettrico a ", compress(yday_term)," KWH, e quello Industriale a ", compress(yday_ind), " KWh");
				riga23 = cat("	- la performance di DAFNE corrisponde a un incentivo di ", compress(yday_p_g1_inc), " Euro");
				riga24=cat("	- l'incentivo effettivo, calcolato sul dato pubblicato, è stato di ", compress(yday_p_g1_inc_pub), " Euro");

				if &flag_conv_g_yday = 1 then do;
					riga25 = cat("	- il modello G DIANA è andato a convergenza con la pubblicazione delle ore ",compress(yday_p_g_oraconv), ", con un errore medio giornaliero di ",compress(yday_p_g_mae) , " M smc e un picco di ", compress(yday_p_g_mae_max), " M smc");
				end;
				else do;
					riga25 = cat("	- il modello G DIANA non è andato a convergenza, con un errore medio giornaliero di ",compress(yday_p_g_mae) , " M smc e un picco di ", compress(yday_p_g_mae_max), " M smc");
				end;
			end;
		end;

		riga26 = cat("Di seguito le performance storiche:");

		if &flag_past_prod = 1 then do;
			riga27 = cat("	- nell'Anno Termico in corso DAFNE ha un MAPE medio del ",compress(past_g1_prod_mape), " valutato su ", compress(past_cons_prod), " giorni, generando un incentivo di ", compress(past_g1_prod_inc), " Euro");
			riga28 = cat("	- nell'Anno Termico in corso le previsioni pubblicate hanno un MAPE medio del ",compress(past_g1_prod_mape_pub), " valutato su ", compress(past_cons_prod), " giorni, generando un incentivo di ", compress(past_g1_prod_inc_pub), " Euro");
		end;

		riga29 = cat("	- nell'Anno Termico in corso il modello G DIANA converge in media entro la pubblicazione delle ore ",compress(past_g_prod_ora));

		if &flag_past_mc = 1 then do;
			riga30 = cat("	- nel mese in corso DAFNE ha un MAPE medio del ",compress(past_g1_mc_mape)," valutato su ",compress(past_cons_mc)," giorni, generando un incentivo di ", compress(past_g1_mc_inc), " Euro");
			riga31= cat("	- nel mese in corso le previsioni pubblicate hanno un MAPE medio del ",compress(past_g1_mc_mape_pub)," valutato su ",compress(past_cons_mc)," giorni, generando un incentivo di ", compress(past_g1_mc_inc_pub), " Euro");
			riga32 = cat("	- nel mese in corso il modello G DIANA converge in media entro la pubblicazione delle ore ",compress(past_g_mc_ora));		
	end;

		do i=1 to dim(righe);
			it = i;
			call symputx("riga"||strip(it), righe[i]);
		end;
	run;
	

%if &flg_invio_report = 1 %then %do;
	FILENAME MailBox EMAIL 'bilancio.disp@snamretegas.it'
	SUBJECT="SNAM Monitoring Previsioni | Pubblicazione ore &hour_pubb in giorno gas &gday_gas_short"
	attach=("&report_path.&pdf_filename");
	options emailsys=smtp 
	emailid='RISPBILADISP@SNAMRETEGAS.IT'
	emailpw=RISPBILADISP
	EMAILHOST=smtprelay.snam.it
	EMAILPORT=587
	EMAILAUTHPROTOCOL=LOGIN;
%end;
%else %do;
	FILENAME MailBox EMAIL 'bilancio.disp@snamretegas.it'
	SUBJECT="SNAM Monitoring Previsioni | Pubblicazione ore &hour_pubb in giorno gas &gday_gas_short";
	options emailsys=smtp 
	emailid='RISPBILADISP@SNAMRETEGAS.IT'
	emailpw=RISPBILADISP
	EMAILHOST=smtprelay.snam.it
	EMAILPORT=587
	EMAILAUTHPROTOCOL=LOGIN;
%end;
	
	data _null_;
		file MailBox
			from='bilancio.disp@snamretegas.it'
			to=(&to_mail)
			cc=(&cc_mail);
		put 'Buongiorno,';
		put ' ';

		if &flag_tmw=0 then do;
			put "&riga1";
			put ' ';
		end;
		else do;
			if /*&flag_pubb_calc=1 and */ &hour_pubb ne 6 then do;
				put "&riga1";
				put "&riga2";

				if &outlier_g1_flag=1 then do;
					put "&riga3";
				end;

				put "&riga4";
				put "&riga5";
				if &prev_pub_inc ne . and &flg_prev15_tmw=1 then do;
					put "&riga33";
					put "&riga34";
				end;
				put ' ';
			end;
		end;

		if &flag_tday_g1=0 and &flag_tday_g=0 then do;
			put "&riga6";
		end;
		else do;
			put "&riga6";
			if /*&flag_pubb_calc=1 and */ &hour_pubb ne 6 then do;
				if &flag_tday_g=0 then do;
					put "&riga7";
				end;
				else do;
					put "&riga7";

					if &outlier_g_diana_flag=2 then do;
						put "&riga8";
					end;
				end;
			end;

			if &flag_tday_g1=0 then do;
				put "&riga9";
			end;
			else do;
				put "&riga9";
				put "&riga10";
				put "&riga11";
				put "&riga12";
			end;

			if /*&flag_pubb_calc=1 and*/ &hour_pubb ne 6 then do;
				if &flag_tday_g=1 and &flag_tday_g1=1 then do;
					put "&riga13";
					put "&riga14";
				end;
			end;
		end;

		put ' ';

		if &flag_g1_yday= 0 then do;
			put "&riga15";
		end;
		else do;
			put "&riga15";

			if &flag_scada =0 and &flag_bil=0 then do;
				put "&riga19";
				put "&riga20";
				put "&riga21";
			end;
			else if &flag_bil=0 then do;
				put "&riga19";
				put "&riga20";
				put "&riga21";
			end;
			else do;
				put "&riga19";
				put "&riga20";
				put "&riga21";
				put "&riga22";
				put "&riga23";
				put "&riga24";

				if &flag_conv_g_yday = 1 then do;
					put "&riga25";
				end;
				else do;
					put "&riga25";
				end;
			end;
		end;

		put ' ';
		put "&riga26";

		if &flag_past_mc = 1 then do;
			put "&riga30";
			put "&riga31";
		end;

		if &flag_past_prod = 1 then do;
			put "&riga27";
			put "&riga28";
		end;

		if &flag_past_mc = 1 then do;
			put "&riga32";
		end;

		put "&riga29";

		put ' ';
		put 'Grazie';
		put ' ';
		put ' ';
		put 'Email automatica si prega di non rispondere';
	run;


%mend;



%macro clean_report();
	
	filename myDir "&report_path./";
	%put QUI CLEAN;

	data get (keep=file_ where = (index(file_,"Report") and index(file_,"pdf")) );
		did=dopen("myDir");
		filecount=dnum(did);
		do i = 1 to filecount;
			file_ = dread(did, i);
			output;
		end;
		rc=dclose(did);
	run;

	data to_delete;
		set get;
		fname="tempfile";
		rc=filename(fname, cat("&report_path./", compress(file_)));
		rc=fdelete(fname);
	run;

%mend;


%macro archiviazione(report_old);
	/*ricava anno mese giorno dal report*/
	%let anno = %sysfunc(substr("&report_old", 9, 4));
	%let mese = %sysfunc(substr("&report_old", 14, 2));
	%let giorno = %sysfunc(substr("&report_old", 17, 2));
	filename glob "&ds_path./&report_old";
	filename addfile zip "&zip_path./report_&anno._&mese..zip" 
		member="&anno._&mese._&giorno./&report_old";
	%put QUI ARCHIVIAZIONE;

	data _null_;
		infile glob recfm=n;
		file addfile recfm=n;
		input byte $char1. @;
		put  byte $char1. @;
	run;

	filename addfile clear;
%mend;

%macro check_archivia_report();
	
	filename myDir "&report_path./";

	data to_zip (keep=file_ to_archive where=(index(file_,"Report_") and index(file_,"pdf")));
		did=dopen("myDir");
		filecount=dnum(did);

		do i = 1 to filecount;
			file_ = dread(did, i);
			to_archive = 0;
			ora = substr(file_, 20, 2);
			if (ora="05") or (ora="15") or (ora="14") then
				to_archive = 1;
			output;
		end;

		rc=dclose(did);
	run;

	data _null_;
		set to_zip;
		file_ = strip(file_);
		put file_;

		if fileexist(cat("&report_path./",file_)) then
			do;
				if to_archive = 1 then
					do;
						call execute(cats('%archiviazione(',file_,')'));
					end;
			end;
	run;
	
%mend check_archivia_report;

%macro extract_append_pubb(url,date);

	data _null_;
		a="&report_path";
		xls=compress(cat("&report_path","pubblicazionisnam_temp.xls"));
		call symput("to_save_xls",compress(xls));
	run;

	filename resp "&to_save_xls";

	proc http 
		url="&url" 
		out=resp;
	run;

	/* Importo lo scarico della pubblicazione excel */
	proc import out= pubb_inc_today replace
		datafile = "&to_save_xls"
	    dbms = XLS;
	    sheet = "15";
	    getnames = yes;
	run;

	data to_add_pubb (keep = G1 data_gas_rif);
		set pubb_inc_today (keep = "PUBBLICAZIONE DELLE INFORMAZIONI"n J);
		if "PUBBLICAZIONE DELLE INFORMAZIONI"n = "RICONSEGNATO (1)";
		to_kwh=1000;	
		G1=input(j,best32.)*to_kwh;
		data_gas_rif =intnx("day",&date,1);
		format data_gas_rif date9.;
	run;
	
	/* Appendo il giorno allo storico */
	proc append base = for_svil.pubblicazioni_snam data = to_add_pubb force;
	run;

%mend extract_append_pubb;


%macro historicizing_pubblicazioni();
	
	%if &hour_pubb=&ora_incentivo+1 %then %do;
	
		/* Controllo ultima data della tabella di storico e aggiungo eventuali missing */
		proc sql noprint;
			select max(data_gas_rif) into: start_date_pub_snam 
			from for_svil.pubblicazioni_snam
			;
		quit;

		data file_list;
			date = &start_date_pub_snam;
			end_date = datepart(&datetime_base);

			do while (date<=end_date);
				output;
				date=intnx("day", date, 1, 's');
			end;
			format date date9.;
			keep date;
		run;

		data file_list;
			retain date url;
			set file_list;
			year=year(date);
			month=compress(put(month(date), z2.) || "_" || lowcase(put(date ,NLDATEMN10.)) );
			day="datiOperativi" || "_" || put(date, yymmddn8.) || "-IT.xls";
			url=catx("/", "&base_url", year, month, day);
			format date date9.;
		run;

		data _null_;
			set file_list;
			call execute ('%NRSTR(%extract_append_pubb(url='||url||',date='||date||'))');
		run;

	%end;

%mend historicizing_pubblicazioni;



