/* Prerequisites: include */

/* Macro to check presence or not of dataset */
%macro checkds(dsn);
	%if %sysfunc(exist(&dsn)) %then
		%do;
			%let dataset_presente=1;
		%end;
	%else
		%do;
			%let dataset_presente=0;
		%end;
%mend checkds;

/* Macro to drop duplicates*/
%macro remove_dupp_tset(dataset, output, dupp, datetime_var);

	proc sort data=&dataset out=&output nodupkey dupout=&dupp;
		by &datetime_var;
	run;

	proc sql noprint;
		select count(*) into :n_dupp
			from &dupp;
	quit;

	%put IL DATASET &dataset CONTENEVA %sysfunc(compress(&n_dupp)) DUPLICATI;
%mend;

%macro dset_phy2mem(dset_name, lib_phy, lib_mem, TDAY_GAS);
	%checkds(&lib_mem..&dset_name);

	%if &dataset_presente=1 %then
		%do;
			%PUT DELETING &dset_name IN &lib_mem;

			proc casutil;
				DROPTABLE casdata="&dset_name" incaslib="&lib_mem";
			run;

		%end;

	data load;
		set &lib_phy..&dset_name /*(where=(data_gas_rif>=intnx("month", &TDAY_GAS, -59, 'B')))*/;
	run;

	%PUT LOADING &dset_name IN &lib_mem;

	proc casutil;
		load data=work.load outcaslib="&lib_mem" casout="&dset_name" promote;
	quit;

%mend;


%macro round_input_vars(dset);

	proc contents data=&dset out=cont_round(keep=name);
	run;
	
	data cont_round;
		set cont_round;
		prefix=scan(name,1,"_");
	run;
	
	proc sort data=cont_round nodupkey;
		by prefix;
	run;
	
	proc sql noprint;
		select distinct compress(cat(prefix,":")) into: families_to_round
		separated by " "
		from cont_round (where = (prefix not in ("data" "datetime" "ff" "hour" "time") ) )
	;
	quit;

	data &dset(drop=i);
		set &dset;
		array FIX_LIST {*} &families_to_round.;
		do i =1 to dim(FIX_LIST);
			FIX_LIST[i]=round(FIX_LIST[i],0.00001);
		end;
	run;

%mend;

/*Foward fill general with criterion, prefix and or suffix*/
%macro ffill_general(input,output,frame,criterion);
	%let lib = %upcase(%scan("&input",1,"."));
	%let ds = %upcase(%scan("&input",-1,"."));

	proc sql noprint;
		%let droplist=;
		select &frame
			into :droplist separated by ' '
				from dictionary.columns
					where libname="&lib"
						and memname="&ds"
						and (&criterion);
	quit;

	proc sql noprint;
		%let savelist=;
		select name
			into :savelist separated by ' '
				from dictionary.columns
					where libname="&lib"
						and memname="&ds"
						and (&criterion);
	quit;

	data &output(drop= i j &savelist);
		set  &input;
		array new &droplist;
		array old &savelist;
		retain new;

		if _N_= 1 then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;
	run;

%mend;

/* Macro to rename columns with prefix, suffix or whatever*/
%macro rename_cols(input, frame, criterion);
	%let lib = %upcase(%scan("&input",1,"."));
	%let ds = %upcase(%scan("&input",-1,"."));

	proc sql noprint;
		select &frame
			into :renames separated by ' '
				from dictionary.columns
					where libname="&lib"
						and memname="&ds"
						and (&criterion);
	quit;

	data &input;
		set &input;
		rename &renames;
	run;

%mend;

/* Macro to append (merge by col) columns with prefix, suffix or whatever*/
%macro append_rename_cols(input, frame, criterion);
	%let lib = %upcase(%scan("&input",1,"."));
	%let ds = %upcase(%scan("&input",-1,"."));

	proc sql;
		select &frame
			into :creates separated by ' '
				from dictionary.columns
					where libname="&lib"
						and memname="&ds"
						and (&criterion);
	quit;

	data &input;
		set &input;
		&creates.
	run;

%mend;

/*Macro to drop missing columns*/
%macro DROPMISS( DSNIN /* name of input SAS dataset
			*/
			, DSNOUT /* name of output SAS dataset
			*/
			, NODROP= /* [optional] variables to be omitted from dropping even if
			they have only missing values */
			);
	/* PURPOSE: To find both Character and Numeric the variables that have only
	missing values and drop them if
	* they are not in &NONDROP
	 *
	 * NOTE: if there are no variables in the dataset, produce no variables
	processing code
	*
	 *
	 * EXAMPLE OF USE:
	 * %DROPMISS( DSNIN, DSNOUT )
	 * %DROPMISS( DSNIN, DSNOUT, NODROP=A B C D--H X1-X100 )
	 * %DROPMISS( DSNIN, DSNOUT, NODROP=_numeric_ )
	 * %DROPMISS( DSNIN, DSNOUT, NOdrop=_character_ )
	 */
	%local I;

	%if "&DSNIN" = "&DSNOUT" %then
		%do;
			%put /------------------------------------------------\;
			%put | ERROR from DROPMISS: |;
			%put | Input Dataset has same name as Output Dataset. |;
			%put | Execution terminating forthwith. |;
			%put \------------------------------------------------/;
			%goto L9999;
		%end;

	/*###################################################################*/

	/* begin executable code
	/*####################################################################/
	/*===================================================================*/

	/* Create dataset of variable names that have only missing values
	/* exclude from the computation all names in &NODROP
	/*===================================================================*/
	proc contents data=&DSNIN( drop=&NODROP ) memtype=data noprint out=_cntnts_( keep=
		name type );
	run;

	%let N_CHAR = 0;
	%let N_NUM = 0;

	data _null_;
		set _cntnts_ end=lastobs nobs=nobs;

		if nobs = 0 then
			stop;
		n_char + ( type = 2 );
		n_num + ( type = 1 );

		/* create macro vars containing final # of char, numeric variables */
		if lastobs then
			do;
				call symput( 'N_CHAR', left( put( n_char, 5. )));
				call symput( 'N_NUM' , left( put( n_num , 5. )));
			end;
	run;

	/*===================================================================*/

	/* if there are no variables in dataset, stop further processing
	/*===================================================================*/
	%if %eval( &N_NUM + &N_CHAR ) = 0 %then
		%do;
			%put /----------------------------------\;
			%put | ERROR from DROPMISS: |;
			%put | No variables in dataset. |;
			%put | Execution terminating forthwith. |;
			%put \----------------------------------/;
			%goto L9999;
		%end;

	/*===================================================================*/

	/* put global macro names into global symbol table for later retrieval
	/*===================================================================*/
	%LET NUM0 =0;
	%LET CHAR0 = 0;

	%IF &N_NUM >0 %THEN
		%DO;
			%do I = 1 %to &N_NUM;
				%global NUM&I;
			%end;
		%END;

	%if &N_CHAR > 0 %THEN
		%DO;
			%do I = 1 %to &N_CHAR;
				%global CHAR&I;
			%end;
		%END;

	/*===================================================================*/

	/* create macro vars containing variable names
	/* efficiency note: could compute n_char, n_num here, but must declare macro names
	to be
	global b4 stuffing them
	/*
	/*===================================================================*/
	proc sql noprint;
		%if &N_CHAR > 0 %then
			%str( select name into :CHAR1 - :CHAR&N_CHAR from _cntnts_ where type = 2 ; );

		%if &N_NUM > 0 %then
			%str( select name into :NUM1 - :NUM&N_NUM from _cntnts_ where type = 1 ; );
	quit;

	/*===================================================================*/

	/* Determine the variables that are missing

	/*
	/*===================================================================*/
	%IF &N_CHAR > 1 %THEN
		%DO;
			%let N_CHAR_1 = %EVAL(&N_CHAR - 1);
		%END;

	Proc sql;
		select

			%do I= 1 %to &N_NUM;
				max (&&NUM&I) ,
			%end;

		%IF &N_CHAR > 1 %THEN
			%DO;
				%do I= 1 %to &N_CHAR_1;
					max(&&CHAR&I),
				%END;
			%end;

		MAX(&&CHAR&N_CHAR)
		into

			%do I= 1 %to &N_NUM;
				:NUMMAX&I ,
			%END;

		%IF &N_CHAR > 1 %THEN
			%DO;
				%do I= 1 %to &N_CHAR_1;
					:CHARMAX&I,
				%END;
			%END;

		:CHARMAX&N_CHAR
		from &DSNIN;
	quit;

	/*===================================================================*/

	/* initialize DROP_NUM, DROP_CHAR global macro vars
	/*===================================================================*/
	%let DROP_NUM =;
	%let DROP_CHAR =;

	%if &N_NUM > 0 %THEN
		%DO;

			DATA _NULL_;
				%do I = 1 %to &N_NUM;
					%IF &&NUMMAX&I =. %THEN
						%DO;
							%let DROP_NUM = &DROP_NUM %qtrim( &&NUM&I );
						%END;
				%end;
			RUN;

		%END;

	%IF &N_CHAR > 0 %THEN
		%DO;

			DATA _NULL_;
				%do I = 1 %to &N_CHAR;
					%IF "%qtrim(&&CHARMAX&I)" eq "" %THEN
						%DO;
							%let DROP_CHAR = &DROP_CHAR %qtrim( &&CHAR&I );
						%END;
				%end;
			RUN;

		%END;

	/*===================================================================*/

	/* Create output dataset
	/*===================================================================*/
	data &DSNOUT;
		%if &DROP_CHAR ^= %then
			%str(DROP &DROP_CHAR ; );

		/* drop char variables
		   that
		   have only missing values */
		%if &DROP_NUM ^= %then
			%str(DROP &DROP_NUM ; );

		/* drop num variables
		   that
		   have only missing values */
		set &DSNIN;

		%if &DROP_CHAR ^= or &DROP_NUM ^= %then
			%do;
				%put /----------------------------------\;
				%put | Variables dropped are &DROP_CHAR &DROP_NUM |;
				%put \----------------------------------/;
			%end;

		%if &DROP_CHAR = and &DROP_NUM = %then
			%do;
				%put /----------------------------------\;
				%put | No variables are dropped |;
				%put \----------------------------------/;
			%end;
	run;

%L9999:
%mend DROPMISS;


/*Macro to drop missing columns*/
%macro drop_missing_vars(DSET);
	* check missings vars;
	ods select none;
	ods output nlevels=_MISSINGVARS (where=(nmisslevels>0 and nnonmisslevels=0));

	proc freq data=&DSET nlevels;
	run;

	* get missing vars names;
	%let _MISSINGVARS=;

	proc sql noprint;
		select TABLEVAR into :_MISSINGVARS separated by ' ' from _MISSINGVARS;
	quit;

	* if there is any, drop;
	%if %str(&_MISSINGVARS) ne %then
		%do;

			data &DSET;
				set &DSET (drop=&_MISSINGVARS);
			run;

		%end;
%mend drop_missing_vars;

%macro put_run_date_init(function);

 

    %global dt_run;

 

    data _null_;
        call symputx("dt_run",datetime());
        call symputx("dt_run_char",put(datetime(),datetime19.));
    run;
    %put --- START PROCESSING FUNCTION &function &dt_run_char ---;

 

%mend put_run_date_init;

 

%macro put_run_time(function);

 

    data _null_;
        time_exe=datetime()-&dt_run;
        call symputx("time_exe",put(time_exe,time.));
    run;
    %put --- END PROCESSING FUNCTION &function. TOTAL PROCESSING TIME: &time_exe. ---;

 

%mend put_run_time;
/**/
/*%macro merge_anomalie(lib_in=, ds_out=);*/
/**/
/*	data _null_;*/
/*		dt_focus=dhms(datepart(&dt_solare),hour(&dt_solare),0,0);*/
/*		call symput("dt_focus",dt_focus);*/
/*	run;*/
/**/
/*	proc sql;*/
/*		create table lista_anomalie as */
/*			select distinct catx(".","&lib_in",compress(lowcase(memname))) as tables_anomalie*/
/*				from dictionary.columns*/
/*					where lowcase(libname)="&lib_in" and */
/*						lowcase(memname) in ("anomalie_bilancio" "anomalie_scada_bil" "anomalie_scada_cons"*/
/*						"anomalie_terna");*/
/*	quit;*/
/**/
/*	proc sql noprint;*/
/*		select distinct tables_anomalie into: tables_anomalie*/
/*			separated by " " */
/*		from lista_anomalie*/
/*		;*/
/*	quit;*/
/**/
/*	proc sql noprint;*/
/*		select count(*) into: n_tabelle_anomalie*/
/*			from lista_anomalie*/
/*		;*/
/*	quit;*/
/**/
/*	%if &n_tabelle_anomalie >0 %then*/
/*		%do;*/
/*			%put ===== Merge Anomalie =====;*/
/**/
/*			data add_bil;*/
/*				set &lib_in..anomalie_bilancio end=eof;*/
/*				if eof;*/
/*				run;*/
/*			data add_scada_bil;*/
/*				set &lib_in..anomalie_scada_bil end=eof;*/
/*				if eof;*/
/*				run;*/
/*			data add_scada_cons;*/
/*				set &lib_in..anomalie_scada_cons end=eof;*/
/*				if eof;*/
/*				run;*/
/*			data add_terna;*/
/*				set &lib_in..anomalie_terna end=eof;*/
/*				if eof;*/
/*				run;*/
/*			data add;*/
/*				merge add_bil add_scada_bil add_scada_cons add_terna;*/
/*				by datetime_solare;*/
/*			run;*/
/**/
/*			proc stdize data=add reponly missing=0 out=add;*/
/*			run;*/
/**/
/*			proc append base=&ds_out data=add force;*/
/*			run;*/
/**/
/**/
/**/
/*		%end;*/
/*%mend merge_anomalie;*/