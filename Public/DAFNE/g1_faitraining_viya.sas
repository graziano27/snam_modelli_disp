%put =========================================================;
%put ===== INIZIO GESTIONE LOG =====;

%let project_id= DAFNE_TR; /* <--- ATTENZIONE: per la gestione del naming dei log modificare SOLO QUESTA RIGA */

data _null_;
	jobName = cat("&project_id.","_",put(datepart(datetime()),date9.),"_",putn(hour(datetime()),'Z2.'),putn(minute(datetime()),'Z2.'), putn(second(datetime()),'Z2.'),"_","&SYSJOBID.");
	call symputx("jobName",jobName);
run;

%put &=jobName.;

proc printto log="/opt/sas/viya/config/var/log/compsrv/log_modelli/&jobName..log";
run;

%put =========================================================;
%put ===== FINE GESTIONE LOG =====;


%put DOING GENERAZIONE DATASET DI TRAINING E MODELLI G1;

%global logic_path lib_path;

%let logic_path = /BIP/Dafne_2/Test_passaggio_produzione; 

%let lib_path = /sasdata/simulazioni_dafne_2/test_passaggio_produzione; 

filename C1 FILESRVC folderpath="&logic_path" filename='librerie_viya.sas';
filename C2 FILESRVC folderpath="&logic_path" filename='g1_master_datasettraining.sas';
%include C1;
%include C2;