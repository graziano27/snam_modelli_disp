/*===============================================================*/
/***** rilascio 2020-09-XX - versione v1.0 *****/
/***** autori: *****/
/***** BIP S.p.a. *****/
/*===============================================================*/

/*============ General Options and Creation of CAS Session ========*/
options casdatalimit=ALL;
cas mySession sessopts=(caslib=casuser timeout=31536000 locale="en_US");
caslib _all_ assign;

options mprint mlogic symbolgen linesize=max;
ods exclude all;

/*============ Global Vars ========*/
%global dataset_presente;

%let start = %sysfunc(datetime());
%let diff_end_month=-1;
%let pfx=;
%let lib_physical=for_svil;
%let lib_memory=public;
%let keyprocess=tr;

/**************************/
/* %let output_final_name=&pfx.g1_trainingset_viya_sim; */
%let output_final_name=&pfx.g1_trainingset_viya; 

%let logic_path = /Public/DAFNE/; 
/*%let logic_path = /Public/Porting_deploy/prod/; */

%let lib_path = /opt/sas/viya/config/data/cas/default/dafne; 
/*%let lib_path = /sasdata/porting_viya;*/

/* TEST */
%let logic_path=/BIP/Dafne_2/Test_passaggio_produzione/; 
%let lib_path=/sasdata/simulazioni_dafne_2/test_passaggio_produzione; 
%let model_path=/sasdata/simulazioni_dafne_2/test_passaggio_produzione/modelli;

filename lib FILESRVC folderpath="&logic_path" filename='librerie_viya.sas';
filename utl FILESRVC folderpath="&logic_path" filename='g1_utils.sas'; 
filename fcn FILESRVC folderpath="&logic_path" filename='g1_functions.sas';
filename dat FILESRVC folderpath="&logic_path" filename='g1_dataset.sas';

/* filename rec FILESRVC folderpath="&logic_path" filename='g1_recupero_training.sas'; */
/* filename rec FILESRVC folderpath="&logic_path" filename='g1_recupero_training_newdate.sas'; */

data g1_limit_day;
	g1_limit_day=intnx("day",datepart(intnx("dtmonth", &start,-1,"E")),&diff_end_month);
/* 	g1_limit_day = "09APR2021"d; */
	call symputx("g1_limit_day",g1_limit_day);
	format g1_limit_day date9.;
run;

/* */
/*%macro clean_all();*/
/**/
/*	proc datasets lib=work kill nolist memtype=data;*/
/*	quit;*/
/**/
/*	Data MVars ( Keep = Name );*/
/*		Set SasHelp.VMacro;*/
/*		Where Scope = 'GLOBAL';*/
/**/
/*		if find(Name, "SAS") = 0;*/
/**/
/*		if find(Name, "SYS") = 0;*/
/**/
/*		if find(Name, "CLIENT") = 0;*/
/**/
/*		if find(Name, "SQL") = 0;*/
/**/
/*		if find(Name, "EG") = 0;*/
/**/
/*		if find(Name, "ORA") = 0;*/
/**/
/*		IF NAME NOT IN ("G_OUTPUT_FINAL_NAME", "LOGIC_PATH", "START", "PFX", "LIB_PHYSICAL", "LIB_MEMORY",*/
/*		"LIB_PATH", "DATASET_PRESENTE", "GRAPHTERM");*/
/*	Run;*/
/**/
/*	Data _Null_;*/
/*		Set MVars;*/
/*		Call Symdel( Name );*/
/*	Run;*/
/**/
/*%mend clean_all;*/

%macro Emergenza_Dataset_Training(lib_phys=, lib_mem=, dset=, date_cut=);

	/* Allocazione librerie */
	%include lib;

/* 	Elimino trainingset esistente in &lib_phys e &lib_mem */
 	proc datasets library=&lib_phys; 
 		delete &dset; 
 	run; 

 	proc casutil; 
   		droptable casdata="&dset" incaslib="&lib_mem" quiet; 
 	run; 

	%put "G1 EMERGENZA GENERAZIONE DATASET INIZIO";

/* 	Richiamo codici per ricreare il trainingset */
	%include fcn;
	%include utl;
 	%include dat; 

	/* Taglio opportunamente il trainingset */
 	data &lib_phys..&dset; 
 		set &lib_phys..&dset; 
 		if data_gas_rif <=&date_cut; 
 	run; 

/* 	Carico dset in public promuovendolo */
 	data load; 
 		set &lib_phys..&dset; 
 	run; 
 	 
 	proc casutil; 
    		droptable casdata="&dset" incaslib="&lib_mem" quiet; 
 	run; 
  
 	proc casutil; 
 		load data=load outcaslib="&lib_mem" casout="&dset" promote; 
 	quit; 

	%put "G1 GENERAZIONE DATASET TERMINATO";

/*	%clean_all();*/
	
	%put "G1 TRAINING MODELLI";
	
/* 	 Recuperare i modelli lanciando g1_recuper_training */
/* 	%include rec; */
	
	%put "G1 TRAINING MODELLI TERMINATO";

%mend Emergenza_Dataset_Training;

%Emergenza_Dataset_Training(lib_phys=&lib_physical, lib_mem=&lib_memory, dset=&output_final_name, date_cut=&g1_limit_day);

 data time_execution; 
 	dt_start = &start; 
 	dt_end = datetime(); 
 	time_execution = dt_end - dt_start; 
 	format dt: datetime19.; 
 	call symputx('time_execution', time_execution); 
 run; 
  
 %put "========================================================================"; 
 %put "IL CODICE SAS E' GIUNTO AL TERMINE IN: &time_execution. SECONDI"; 
  
 cas mySession terminate;  
