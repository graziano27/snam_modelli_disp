%put ---------- Cancello i report pdf dal percorso &report_path ----------;
%clean_report;

%put ---------- Recupero le previsioni pubblicate da incentivo e le storicizzo ----------;
%historicizing_pubblicazioni;

%put ---------- Creo il report di monitoring ----------;
%get_predictions_target;
%get_mail;
%create_report;


data _null_;
	if &hour_pubb in &hour_mail_list then do;
		flg_invio_mail=1;
	end;
	else do;
		flg_invio_mail=0;
	end;
	if &hour_pubb in &hour_report_list then do;
		flg_invio_report=1;
	end;
	else do;
		flg_invio_report=0;	
	end;
	call symput("flg_invio_mail",flg_invio_mail);
	call symput("flg_invio_report",flg_invio_report);
run;

%if &flg_invio_mail = 1 %then %do;
	%put ---------- Invio MAIL ----------;
	%Invio_mail;
%end;


%put ---------- Storicizzazione dei report ----------;
%check_archivia_report;

cas mySession terminate;




