/*===============================================================*/
/***** rilascio 2020-01-XX - versione v1.0 *****/
/***** autori: Binda, Oliva, Rizzolo *****/
/***** BIP S.p.a. *****/
/*===============================================================*/

/* options nomlogic nomprint nosymbolgen; */
/* options mlogic mprint symbolgen msglevel=i; */
/* ods exclude all; */
/*ods exclude none;*/

/*============ Macro variabili =================================*/
/* Definizione macro variabili Globali */
%let dt_solare=&start;
%let dset_input_name = previsioni_riconsegnato;
%let dataset_input = &lib_physical..&dset_input_name;
%let dset_ensemble_name = ensemble_DIANA;
%let dataset_input = &lib_physical..&dset_input_name;
%let dataset_output_ensemble = &lib_physical..&dset_ensemble_name;
%let conf_width = 2;
%let time_id = datetime_solare;
%let lambda_1 = 0.7;
%let lambda_2 = 0.225;
%let ora_pub_key_2level = 8;

data weight;
	total_weight = 1.35;
	w_bg = 0.25;
	w_bs = 0.3;
	w_dg = 0.4;
	w_ds = 0.4;
	array weights[*] w_:;
	do i = 1 to dim(weights);
		weights[i] = weights[i]/total_weight;
		call symputx(vname(weights[i]), weights[i]);
	end;

	total_weights_norm =  sum(of weights[*]);
	drop i;
run;

/* Prendo l'ultima riga del dataset di input */
%macro get_ensemble_data(dataset_input=, time_id=);

	proc sort data=&dataset_input;
		by &time_id;
	run;

	data time;
		set &dataset_input end=eof;

		if eof then
			do;
				call symputx("last_hour",compress(put(hour,z2.)));
				call symputx("last_date",compress(put(data_gas_rif,date9.)));
				call symputx("hour_pred",(put(hour(&dt_solare),z2.)));
				call symputx("date_pred",compress(put(datepart(&dt_solare),date9.)));
			end;
	run;

	data preds_today;
		set &dataset_input;
		where id in (1 2 3 4)
			and data_gas_rif = "&last_date"d;
		weight = &w_bg;

		if target = 'TOT' and modello = 'ST' then
			weight = &w_bs;

		if target = 'DIFF' and modello = 'GL' then
			weight = &w_dg;

		if target = 'DIFF' and modello = 'ST' then
			weight = &w_ds;
	run;

	data preds_for_ensemble;
		set preds_today;
		where hour = &last_hour;
	run;

%mend;

%macro clean_outlier_preds(conf_width=, time_id=);

	proc sort data=preds_today out=preds_today_sort sortseq=italian;
		by &time_id target modello;
	run;

	proc expand data=preds_today_sort out=preds_today_std;
		by data_gas_rif;
		convert previsione = P_std / transformout=(custd);
		convert previsione = P_mean / transformout=(cuave);
	run;

	data preds_today_limit;
		set preds_today_std;
		by &time_id;
		upr_limit = P_mean + &conf_width*P_std;
		lwr_limit = P_mean - &conf_width*P_std;

		if last.&time_id then
			output;
		keep &time_id P_mean P_std upr_limit lwr_limit;
	run;

	data preds_today_weight;
		merge preds_today(in=L) preds_today_limit(in=R);
		by &time_id;
	run;

	proc sort data=preds_today_weight out=preds_today_weight sortseq=italian;
		by &time_id target modello;
	run;

	data preds_today_new_weight;
		set preds_today_weight;
		retain total_weight_new;
		weight_new = weight;
		by &time_id;

		if previsione > upr_limit or previsione < lwr_limit then
			weight_new = 0;

		if first.&time_id then
			total_weight_new = weight_new;
		else total_weight_new + weight_new;

		if last.&time_id then
			final_total_weight = total_weight_new;
	run;

	proc sort data=preds_today_new_weight out=preds_today_new_weight_inv sortseq=italian;
		by &time_id descending target descending modello;
	run;

	data preds_today_new_weight_inv2;
		set preds_today_new_weight_inv;
		retain final_total_weight_new;

		if not missing(final_total_weight) then
			final_total_weight_new=final_total_weight;
		weight_new = weight_new/final_total_weight_new;

		if missing(weight_new) then
			weight_new=0;
	run;

%mend;

%macro ensemble_preds(dataset_input=, time_id=);

	proc sort data=preds_for_ensemble out=preds_for_ensemble_sort;
		by &time_id descending target descending modello;
	run;

	data preds_for_ensemble_new_weight;
		merge preds_for_ensemble_sort(in=L) preds_today_new_weight_inv2(in=R 
			keep=&time_id weight_new upr_limit lwr_limit); 
		by &time_id;

		if L and R;
	run;

	proc means data=preds_for_ensemble_new_weight noprint;
		by &time_id;
		weight weight_new;
		var previsione;
		output out=preds_ensemble(drop=_FREQ_ _TYPE_);
		id data_gas_rif hour;
	run;

	data preds_ensemble_ok;
		set preds_ensemble;
		where _stat_ = "MEAN";
		drop _stat_;

		/* 		&time_id = dhms("&date_pred"d, &hour_pred, 0, 0); */
		/* 		data_calcolo = "&date_pred"d; */
		/* 		data_gas_rif = ifn(&hour_pred < 6, intnx('day', "&date_pred"d, -1), "&date_pred"d); */
		modello = 'EN';
		target = 'TOT';
		id = 8;
		approccio = "G_04";

		/*  	hour = &hour_pred; */
	run;
	
	data preds_ensemble_all;
		set preds_for_ensemble_new_weight preds_ensemble_ok;
	run;
	
	data preds_ensemble_all_ok;
		set preds_ensemble_all;
		retain upr_limit_ffill lwr_limit_ffill;
		flag_outlier=0;
		if not missing(upr_limit) then 
			upr_limit_ffill=upr_limit;
		if not missing(upr_limit) then 
			lwr_limit_ffill=lwr_limit;
		if weight_new = 0 then flag_outlier=1;
		if id = 8 then
			do;
				weight=1;
				weight_new=1;
				if missing(upr_limit) then
					upr_limit=upr_limit_ffill;
				if missing(lwr_limit) then
					lwr_limit=lwr_limit_ffill;
			end;
		drop upr_limit_ffill lwr_limit_ffill;
	run;
	
	%if %sysfunc(exist(&dataset_output_ensemble)) %then
		%do;

		data &dataset_output_ensemble;
			set &dataset_output_ensemble preds_ensemble_all_ok;
		run;

		%end;
	%else
		%do;

			data &dataset_output_ensemble;
				length datetime_solare data_gas_rif hour previsione 8 approccio target modello $ 8 id weight
				upr_limit lwr_limit weight_new flag_outlier 8;
				format datetime_solare datetime19.;
				format data_gas_rif date9.;				
				stop;
			run;

			data &dataset_output_ensemble;
				set preds_ensemble_all_ok;
			run;
			
		%end;
		
		
	data &dataset_input;
		set &dataset_input preds_ensemble_ok;
	run;

	proc sort data=&dataset_input;
		by &time_id;
	run;

	data previsioni_riconsegnato_temp;
		set &dataset_input;
		retain previsione_ens_ffill;

		if modello = 'EN' and not missing(previsione) and previsione ne 0 then
			previsione_ens_ffill=previsione;
		else if modello = 'EN' and (missing(previsione) or previsione = 0) then
			previsione=previsione_ens_ffill;		
		drop previsione_ens_ffill;
	run;
	
	data &dataset_input;
		set previsioni_riconsegnato_temp;
	run;

%mend;

%macro get_prevs_fill(dataset_g, time_id, n_id, output_prev);

	proc sort data=&dataset_g(where=(not missing(previsione))) 
		out=predictions_g_all nodupkey;
		by &time_id id;
	run;

	proc transpose data=predictions_g_all out=predictions_g_all_t( drop=_name_) prefix=id;
		by &time_id;
		id id;
		var previsione;
	run;

	data calendario_dt(drop=date where=(data_gas_rif<=&g_gas and data_gas_rif>intnx("day",&g_gas,-7)));
		date=intnx("day",&g_solare,-7);

		do while (date<=&g1_solare);
			do hour=0 to 23;
				datetime_solare=dhms(date,hour,0,0);

				if hour<6 then
					data_gas_rif=intnx("day", date, -1);
				else data_gas_rif=date;
				output;
			end;

			date=intnx("day", date, 1, 's');
		end;

		format date date9.;
		format data_gas_rif date9.;
		format datetime_solare datetime19.;
	run;

	data predictions_g_all_t_nofill(where=(datetime_solare<=&dt_solare));
		merge calendario_dt(in=p) predictions_g_all_t;
		by datetime_solare;

		if p;
	run;

	data predictions_g_all_t_fill(drop=i fill:);
		set predictions_g_all_t_nofill;
		array id{&n_id};
		array fill{&n_id};

		do i = 1 to &n_id;
			fill[i]=lag1(id[i]);
		end;

		if hour ne 6 then
			do;
				if id[8] eq . then
					id[8]=fill[9];

				if id[5] eq . then
					id[5]=fill[10];

				if id[6] eq . then
					id[6]=fill[11];

				if id[7] eq . then
					id[7]=fill[12];
			end;
	run;

	/*8 fill con 9*/
	/*5 fill con 10*/
	/*6 con 11*/
	/*7 con 12*/
	proc transpose data=predictions_g_all_t_fill out=predictions_g_all_fill(Rename=(col1=previsione))
		name=id_char;
		by datetime_solare data_gas_rif hour;
	run;

	data &output_prev(drop=id_char);
		set predictions_g_all_fill;
		id=input(tranwrd(id_char,"id",""),8.);
	run;

%mend;

%macro gxg_smoothing_2lvl(dataset_g, time_id, in_id, lambda1, lambda2, start_pk2,
			out_target, out_approccio, out_id, out_row);

	proc sort data=&dataset_g(where=(id = &in_id and not missing(previsione))) 
		out=predictions_g nodupkey;
		by &time_id;
	run;

	/*conto numero di previsioni generate dal rilascio*/
	proc sql noprint;
		select count(hour) into :count_preds_g
			from predictions_g where data_Gas_rif=&g_gas;
	run;

	%if (&count_preds_g eq 1) %then
		%do;

			data prediction_g_smooth;
				set predictions_g end=eof;

				if eof;
				target="&out_target";
				approccio="&out_approccio";
				modello="SM";
				ID=&out_id;
			run;

		%end;
	%else %if (&count_preds_g > 1) %then
		%do;
			/*smth_expsmooth_new_pk(predictions, output_predictions, prev_var,start_pk, */
			/*			prev_var_new, lambda=0.5)*/
			%smth_expsmooth_new_pk(predictions_g, 
				predictions_g_smth1, previsione, 0, previsione1, lambda=&lambda1);
			%smth_expsmooth_new_pk(predictions_g_smth1, 
				predictions_g_smth2, previsione1, &start_pk2, previsione2, lambda=&lambda2);

			data prediction_g_smooth(rename=(previsione2=previsione));
				set predictions_g_smth2(drop=previsione previsione1)  end=eof;

				if eof;
				target="&out_target";
				approccio="&out_approccio";
				modello="SM";
				ID=&out_id;
			run;

		%end;

	data &out_row;
		set prediction_g_smooth;
	run;

%mend gxg_smoothing_2lvl;

%macro gxg_smoothing_ind(prev_dset, time, out);

	data _null_;
		if &hour_prev in (13 14 15 16 17 18) then
			flag=1;
		else flag=0;
		call symputx('flag_ind',flag);
	run;

	%if &flag_ind eq 1 %then
		%gxg_smoothing_1lvl(&prev_dset, &time, 7, 0.7, IND, G_06, 12, &out);
	%else %gxg_smoothing_1lvl(&prev_dset, &time, 7, 0.2, IND, G_06, 12, &out);
%mend;

%macro gxg_smoothing_civ(prev_dset, time, out);

	data _null_;
		if &hour_prev in (10 11 12 13 14 15 16 17 18) then
			flag=1;
		else flag=0;
		call symputx('flag_civ',flag);
	run;

	%if &flag_civ eq 1 %then
		%gxg_smoothing_1lvl(&prev_dset, &time, 6, 0.7, CIV, G_06, 11, &out);
	%else %gxg_smoothing_1lvl(&prev_dset, &time, 6, 0.2, CIV, G_06, 11, &out);
%mend;

%macro gxg_smoothing_term(prev_dset, time, out);

	data _null_;
		if &hour_prev in (6 7 8 9 10 11 12 13 14 15 16 17 18) then
			flag=1;
		else flag=0;
		call symputx('flag_term',flag);
	run;

	%if &flag_term eq 1 %then
		%gxg_smoothing_1lvl(&prev_dset, &time, 5, 0.5, TERM, G_06, 10, &out);
	%else %gxg_smoothing_1lvl(&prev_dset, &time, 5, 0.3, TERM, G_06, 10, &out);
%mend;

%macro gxg_smoothing_1lvl(dataset_g, time_id, in_id, lambda1,
			out_target, out_approccio, out_id, out_row);

	proc sort data=&dataset_g(where=(id = &in_id and not missing(previsione))) 
		out=predictions_g nodupkey;
		by &time_id;
	run;

	/*conto numero di previsioni generate dal rilascio*/
	proc sql noprint;
		select count(hour) into :count_preds_g
			from predictions_g where data_Gas_rif=&g_gas;
	run;

	%if (&count_preds_g eq 1) %then
		%do;

			data prediction_g_smooth;
				set predictions_g end=eof;

				if eof;
				target="&out_target";
				approccio="&out_approccio";
				modello="SM";
				ID=&out_id;
			run;

		%end;
	%else %if (&count_preds_g > 1) %then
		%do;
			/*smth_expsmooth_new_pk(predictions, output_predictions, prev_var,start_pk, */
			/*			prev_var_new, lambda=0.5)*/
			%smth_expsmooth_new_pk(predictions_g, 
				predictions_g_smth1, previsione, 0, previsione1, lambda=&lambda1);

			data prediction_g_smooth(rename=(previsione1=previsione));
				set predictions_g_smth1(drop=previsione)  end=eof;

				if eof;
				target="&out_target";
				approccio="&out_approccio";
				modello="SM";
				ID=&out_id;
			run;

		%end;

	data &out_row;
		set prediction_g_smooth;
	run;

%mend gxg_smoothing_1lvl;

%macro gxg_ensemble();
	%get_ensemble_data(dataset_input=&dataset_input, time_id=datetime_solare);
	%clean_outlier_preds(conf_width=&conf_width, time_id=datetime_solare);
	%ensemble_preds(dataset_input=&dataset_input, time_id=datetime_solare);
%mend;

%macro gxg_smoothing();

	data init_dates;
		g_solare=datepart(&dt_solare);
		g_gas=ifn(hour(&dt_solare)<6, intnx("day", g_solare, -1), g_solare);
		g1_solare=intnx("day",g_solare,1);
		g1_gas=intnx("day", g_gas,1);
		hour_prev=hour(intnx("dthour",&dt_solare,1));
		call symput("g_solare",g_solare);
		call symput("g_gas",g_gas);
		call symput("g1_solare",g1_solare);
		call symput("g1_gas",g1_gas);
		call symput("hour_prev",hour_prev);
		format g: date9.;
	run;

	%get_prevs_fill(&dataset_input, datetime_solare, 12, prev_fill);

	proc sql noprint;
		select count(hour) into :count_preds_all
			from prev_fill where data_Gas_rif=&g_gas and previsione ne .;
	run;

	%if (&count_preds_all >0 ) %then
		%do;
			%put ======= APPLYING SMOOTHING ===========;

			%gxg_smoothing_2lvl(prev_fill, datetime_solare, 8, &lambda_1, &lambda_2, &ora_pub_key_2level,
				TOT, G_05, 9, row_id9);
			%gxg_smoothing_term(prev_fill, datetime_solare, row_id10);
			%gxg_smoothing_civ(prev_fill, datetime_solare, row_id11);
			%gxg_smoothing_ind(prev_fill, datetime_solare, row_id12);

			data &dataset_input;
				set &dataset_input row_id9 row_id10 row_id11 row_id12;
			run;

		%end;
	%else %put ======= NO SMOOTHING ===========;

%mend;

%macro smth_expsmooth_new_pk(predictions, output_predictions, prev_var,start_pk, 
			prev_var_new, lambda=0.5);

	data naming;
		lambda_fix=min(&lambda,1);
		lambda_str=TRANSTRN(lambda_fix,'.',trimn(' '));
		call symputx('lambda_fix', lambda_fix);
		call symputx('lambda_str', lambda_str);
	run;

	data predictions_dset;
		set &predictions(keep=datetime_solare data_gas_rif hour &prev_var);
		data_calcolo=datepart(datetime_solare);

		/*		if month(data_gas_rif)>=10 then*/
		/*			periodo=cat("AT", put(year(data_gas_rif), 4.0), "/", */
		/*			put(year(data_gas_rif)+1, 4.0));*/
		/*		else periodo=cat("AT", put(year(data_gas_rif)-1, 4.0), "/", put(year(data_gas_rif), 4.0));*/
		if nwkdom(5, 1, 3, year(data_calcolo)) <=data_calcolo <=nwkdom(5, 1, 10, 
			year(data_calcolo)) then
			do;
				ora_pubblicazione=hour + 2;

				if hour ge 22 then
					ora_pubblicazione=hour - 22;
				dt_pubblicazione=intnx("dthour", datetime_solare, 2);

				if hour in (4, 5) then
					do;
						ora_pubblicazione=.;
					end;
			end;
		else
			do;
				ora_pubblicazione=hour+1;

				if hour ge 23 then
					ora_pubblicazione=hour - 23;
				dt_pubblicazione=intnx("dthour", datetime_solare, 1);

				if hour=5 then
					do;
						ora_pubblicazione=.;
					end;
			end;

		data_Gas_pubb=ifn(hour(dt_pubblicazione)<6, intnx("day", 
			datepart(dt_pubblicazione), -1), datepart(dt_pubblicazione));

		if hour ge 6 then
			ora_sist_key=hour-6;
		else ora_sist_key=hour+18;

		if ora_sist_key < 10 then
			ora_sist_key_c=cat(0, ora_sist_key, "ore", hour);
		else ora_sist_key_c=cat(ora_sist_key, "ore", hour);

		if ora_pubblicazione ge 6 then
			ora_pub_key=ora_pubblicazione-6;
		else ora_pub_key=ora_pubblicazione+18;

		if ora_pub_key eq . then
			ora_pub_key_c="invalid";
		else if ora_pub_key < 10 then
			ora_pub_key_c=cat(0, ora_pub_key, "ore", ora_pubblicazione);
		else ora_pub_key_c=cat(ora_pub_key, "ore", ora_pubblicazione);
		format data: date9. dt: datetime19.;
	run;

	proc sort data=predictions_dset out=predictions_dset nodupkey;
		by data_gas_rif ora_sist_key;
	run;

	data predictions_dset;
		set predictions_dset;
		retain smth_lag;
		ww=&lambda_fix;
		by data_gas_rif;

		if ora_sist_key=0 or _N_=1 then
			do;
				prediction_smth=&prev_var;
				smth_lag=prediction_smth;
			end;
		else
			do;
				if ora_pub_key < &start_pk and ora_pub_key ne . then
					do;
						prediction_smth=&prev_var;
						smth_lag=prediction_smth;
					end;
				else
					do;
						prediction_smth_lag=smth_lag;
						prediction_smth=sum(ww*&prev_var,prediction_smth_lag*(1-ww));
						smth_lag=prediction_smth;
					end;
			end;

		&prev_var_new=prediction_smth;
		drop smth_lag;
	run;

	data &output_predictions;
		merge &predictions predictions_dset(keep= datetime_solare &prev_var_new);
		by datetime_solare;
	run;

%mend smth_expsmooth_new_pk;

/*proc sort data=&dataset_input(where=(not missing(previsione))) */
/*	out=&dataset_input nodupkey;*/
/*	by &time_id id;*/
/*run;*/

%gxg_ensemble();
%gxg_smoothing();

/* data &dataset_input; */
/* 	set &dataset_input; */
/* 	if _N_ < 8; */
/* 	drop data_gas_rif; */
/* run; */