/* Prerequisites: include, utils */
%macro create_calendar_vars(mod, input_cal_dset, dset_with_time)
			/ minoperator mindelimiter=',';

	data temp;
		set &input_cal_dset;
		data_gas_rif=intnx("day",data_gas_rif, &mod); /*data_gas_rif for the specific model*/
		time_month = month(data_gas_rif);
		time_day = weekday(data_gas_rif);
		time_week = week(data_gas_rif);
		format data_gas_rif date9.;
	run;

	data temp;
		set temp;
		time_day_7 = 0;
		time_day_6 = 0;
		time_day_1 = 0;
		time_day_2 = 0;
		time_month_7 = 0;
		time_month_4 = 0;
		time_month_12 = 0;
		time_month_5 = 0;
		time_festivo = 0;
		time_prefestivo = 0;

		if day(data_gas_rif) = 1 and month(data_gas_rif) = 1 then
			time_festivo = 1;

		if day(data_gas_rif) = 6 and month(data_gas_rif) = 1 then
			time_festivo = 1;

		if day(data_gas_rif) = 25 and month(data_gas_rif) = 4 then
			time_festivo = 1;

		if day(data_gas_rif) = 1 and month(data_gas_rif) = 5 then
			time_festivo = 1;

		if day(data_gas_rif) = 2 and month(data_gas_rif) = 6 then
			time_festivo = 1;

		if day(data_gas_rif) = 15 and month(data_gas_rif) = 8 then
			time_festivo = 1;

		if day(data_gas_rif) = 1 and month(data_gas_rif) = 11 then
			time_festivo = 1;

		if day(data_gas_rif) = 8 and month(data_gas_rif) = 12 then
			time_festivo = 1;

		if data_gas_rif = holiday('EASTER',year(data_gas_rif)) then
			time_festivo = 1;

		if data_gas_rif = intnx('day',holiday('EASTER',year(data_gas_rif)),1) then
			time_festivo = 1;

		%if &input_cal_dset=scada_bil_temp %then %do;

            if data_gas_rif = intnx('day',holiday('EASTER',year(data_gas_rif)),2) then
			time_festivo = 1;

		%end;

		if day(data_gas_rif) = 25 and month(data_gas_rif) = 12 then
			time_festivo = 1;

		if day(data_gas_rif) = 26 and month(data_gas_rif) = 12 then
			time_festivo = 1;

		if day(data_gas_rif) = 31 and month(data_gas_rif) = 12 then
			time_festivo = 1;

		if day(data_gas_rif) = 5 and month(data_gas_rif) = 1 then
			time_prefestivo = 1;

		if day(data_gas_rif) = 24 and month(data_gas_rif) = 4 then
			time_prefestivo = 1;

		*if data_gas_rif = intnx('day',holiday('EASTER',year(data_gas_rif)),-1) then time_prefestivo = 1;
		if day(data_gas_rif) = 30 and month(data_gas_rif) = 4 then
			time_prefestivo = 1;

		if day(data_gas_rif) = 1 and month(data_gas_rif) = 6 then
			time_prefestivo = 1;

		if day(data_gas_rif) = 14 and month(data_gas_rif) = 8 then
			time_prefestivo = 1;

		if day(data_gas_rif) = 31 and month(data_gas_rif) = 10 then
			time_prefestivo = 1;

		if day(data_gas_rif) = 7 and month(data_gas_rif) = 12 then
			time_prefestivo = 1;

		if day(data_gas_rif) = 24 and month(data_gas_rif) = 12 then
			time_prefestivo = 1;

		if day(data_gas_rif) = 30 and month(data_gas_rif) = 12 then
			time_prefestivo = 1;
		time_week_52 = 0;
		time_week_32 = 0;
		time_week_51 = 0;
		time_week_33 = 0;

		if time_day = 7 then
			time_day_7 = 1;

		if time_day = 6 then
			time_day_6 = 1;

		if time_day = 1 then
			time_day_1 = 1;

		if time_day = 2 then
			time_day_2 = 1;

		if time_month = 7 then
			time_month_7 = 1;

		if time_month = 4 then
			time_month_4 = 1;

		if time_month = 12 then
			time_month_12 = 1;

		if time_month = 5 then
			time_month_5 = 1;

		if time_week = 52 then
			time_week_52 = 1;

		if time_week = 32 then
			time_week_32 = 1;

		if time_week = 51 then
			time_week_51 = 1;

		if time_week = 33 then
			time_week_33 = 1;
		drop time_week time_month;
	run;

	data temp_ftd;
		set temp;
		fake_time_day=time_day;
		time_natale=0;
		time_ferragosto=0;

		if time_festivo=1 and time_day=2 then
			fake_time_day=5;

		if time_festivo=1 and time_day=3 then
			fake_time_day=5;

		if time_festivo=1 and time_day=4 then
			fake_time_day=7;

		if time_festivo=1 and time_day=5 then
			fake_time_day=7;

		if time_festivo=1 and time_day=6 then
			fake_time_day=7;

		if data_gas_rif = intnx('day',holiday('EASTER',year(data_gas_rif)),0) then
			fake_time_day = 1;

		if data_gas_rif = intnx('day',holiday('EASTER',year(data_gas_rif)),1) then
			fake_time_day = 5;

		/*natale*/
		/*da sabato prima di natale a domenica dopo befana*/
		if month(data_gas_rif) = 12 and data_gas_rif >= intnx("week", mdy(12, 25, year(data_gas_rif)), -1, 'E') 
			and weekday(mdy(12, 25, year(data_gas_rif))) < 7 then
			do;
				time_natale=1;
			end;
		else if month(data_gas_rif) = 12 and data_gas_rif >= intnx("day", mdy(12, 25, year(data_gas_rif)), -1) 
			and weekday(mdy(12, 25, year(data_gas_rif))) = 7 then
			do;
				time_natale=1;
			end;

		if month(data_gas_rif) = 1 and data_gas_rif<=intnx("week", mdy(1, 6, year(data_gas_rif)), 0, 'E') 
			and weekday(mdy(1, 6, year(data_gas_rif))) > 1 then
			do;
				time_natale=1;
			end;
		else if month(data_gas_rif) = 1 and data_gas_rif <= mdy(1, 6, year(data_gas_rif)) 
			and weekday(mdy(1, 6, year(data_gas_rif))) = 1 then
			do;
				time_natale=1;
			end;

		/*ferragosto*/
		/*tutta la settimana di ferragosto da sabato prima a domenica*/
		if data_gas_rif >= intnx("day", mdy(8,15, year(data_gas_rif)), -2) 
			and data_gas_rif <= intnx("day", mdy(8,15, year(data_gas_rif)), 2) then
			do;
				time_ferragosto=1;
			end;
	run;
	
data temp_ftd;
	set temp_ftd;
	time_festivo_lag1=lag24(time_festivo);
	time_2_ext=0;
	if time_festivo_lag1=1 then time_2_ext=1;
	if weekday(data_gas_rif) in (1, 7) and time_festivo_lag1=1 then time_2_ext=0;
	if weekday(data_gas_rif)=2 then time_2_ext=1;
	if data_gas_rif=holiday("easter", year(data_gas_rif))+3 then time_2_ext=0;
	drop time_festivo_lag1; 
run;

	data temp_new_vars;
		set temp_ftd;
		time_weekend = 0;
		time_weekend_ext = 0;
		time_day_7_ext = 0;
		time_day_7_ext_holy = 0;
		time_festivo_ext = 0;
		fake_time_day_group1 = 0;
		fake_time_day_group2 = 0;
		time_risc_zona_A = 0; /* 3 comuni --> approssimo a 0.5 province */
		time_risc_zona_B = 0; /* 8 province */
		time_risc_zona_C = 0; /* 16 province */
		time_risc_zona_D = 0; /* 30 province */
		time_risc_zona_E = 0; /* 45 province */
		time_risc_zona_F = 1; /* 3 province - No limitazioni */
		time_day_group1 = 0;
		time_day_group2 = 0;

		if time_day_7 = 1 or time_festivo = 1 then
			do;
				time_day_7_ext = 1; /* Sabato esteso ai festivi */
				time_day_7_ext_holy = 1; /* Sabato esteso ai festivi e ai periodi di vacanza */
			end;

		if time_festivo = 1 then
			time_festivo_ext = 1;

		if time_ferragosto = 1 or time_natale = 1 then
			do;
				time_day_7_ext_holy = 1;
				time_festivo_ext = 1;
			end;

		if time_day in (1, 7) then
			time_weekend = 1;

		if time_day in (1, 7) or time_festivo = 1 then
			time_weekend_ext = 1;

		if (month(data_gas_rif) in (12, 1, 2)) or (month(data_gas_rif) = 3 and day(data_gas_rif) <= 15) then
			time_risc_zona_A = 1;

		if (month(data_gas_rif) in (12, 1, 2, 3)) then
			time_risc_zona_B = 1;

		if (month(data_gas_rif) in (12, 1, 2, 3)) or (month(data_gas_rif) = 11 and day(data_gas_rif) >= 15) then
			time_risc_zona_C = 1;

		if (month(data_gas_rif) in (11, 12, 1, 2, 3)) or (month(data_gas_rif) = 4 and day(data_gas_rif) <= 15) then
			time_risc_zona_D = 1;

		if (month(data_gas_rif) in (11, 12, 1, 2, 3)) or (month(data_gas_rif) = 4 and day(data_gas_rif) <= 15)
			or (month(data_gas_rif) = 10 and day(data_gas_rif) >= 15) then
			time_risc_zona_E = 1;

		/* Riscaldamento complessivo */
		time_risc_totale = time_risc_zona_A*(0.5/102.5) + time_risc_zona_B*(8/102.5) + time_risc_zona_C*(16/102.5) + 
			time_risc_zona_D*(30/102.5) + time_risc_zona_E*(45/102.5) + time_risc_zona_F*(3/102.5);

		if fake_time_day in (2, 3, 4, 5, 6) then
			fake_time_day_group1 = 1;
		else if fake_time_day in (1,  7) then
			fake_time_day_group2 = 1;

		if time_day in (2, 3, 4, 5, 6) and time_festivo = 0 then
			time_day_group1 = 1;

		if time_day in (1, 7) or time_festivo = 1 then
			time_day_group2= 1;
		drop time_day fake_time_day;
	run;

	data &dset_with_time;
		set temp_new_vars;
		data_gas_rif_364 = intnx("day", data_gas_rif, -364);
		data_gas_rif_366 = intnx("day", data_gas_rif, -366);
		time_festivo_lag = 0;

		if day(data_gas_rif_364) = 1 and month(data_gas_rif_364) = 1 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 1 and month(data_gas_rif_366) = 1 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 6 and month(data_gas_rif_364) = 1 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 6 and month(data_gas_rif_366) = 1 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 25 and month(data_gas_rif_364) = 4 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 25 and month(data_gas_rif_366) = 4 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 1 and month(data_gas_rif_364) = 5 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 1 and month(data_gas_rif_366) = 5 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 2 and month(data_gas_rif_364) = 6 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 2 and month(data_gas_rif_366) = 6 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 15 and month(data_gas_rif_364) = 8 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 15 and month(data_gas_rif_366) = 8 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 1 and month(data_gas_rif_364) = 11 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 1 and month(data_gas_rif_366) = 11 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 8 and month(data_gas_rif_364) = 12 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 8 and month(data_gas_rif_366) = 12 then
			time_festivo_lag = 1;

		if data_gas_rif_364 = holiday('EASTER',year(data_gas_rif_364)) then
			time_festivo_lag = 1;
		else if data_gas_rif_366 = holiday('EASTER',year(data_gas_rif_366)) then
			time_festivo_lag = 1;

		if data_gas_rif_364 = intnx('day',holiday('EASTER',year(data_gas_rif_364)),1) then
			time_festivo_lag = 1;
		else if data_gas_rif_366 = intnx('day',holiday('EASTER',year(data_gas_rif_366)),1) then
			time_festivo_lag = 1;

		if data_gas_rif_364 = intnx('day',holiday('EASTER',year(data_gas_rif_364)),2) then
			time_festivo_lag = 1;
		else if data_gas_rif_366 = intnx('day',holiday('EASTER',year(data_gas_rif_366)),2) then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 25 and month(data_gas_rif_364) = 12 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 25 and month(data_gas_rif_366) = 12 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 26 and month(data_gas_rif_364) = 12 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 26 and month(data_gas_rif_366) = 12 then
			time_festivo_lag = 1;

		if day(data_gas_rif_364) = 31 and month(data_gas_rif_364) = 12 then
			time_festivo_lag = 1;
		else if day(data_gas_rif_366) = 31 and month(data_gas_rif_366) = 12 then
			time_festivo_lag = 1;
		format data: date9.;
	run;

	%if not(&mod in (1,2,3,4)) %then
		%do;

			data &dset_with_time(drop = i);
				set &dset_with_time(drop= fake_time_day_group1 fake_time_day_group2 data_gas_rif_364 data_gas_rif_366);
				array hour_array(24) G_hour_0 - G_hour_23;

				do i=1 to 24;
					hour_array{i} =ifn(hour = i-1,1,0);
				end;
			run;

		%end;
	%else
		%do;

			data &dset_with_time(rename=(data_gas_rif = G&mod._data_gas_rif));
				set &dset_with_time(drop= fake_time_day_group1 fake_time_day_group2 data_gas_rif_364 data_gas_rif_366);
			run;

		%end;
%mend create_calendar_vars;

%macro create_all_calendar_vars(input_cal, calendario_tot);
	%do i=0 %to 4;
		%if &i = 0 %then
			%do;
				%create_calendar_vars(0,&input_cal,&input_calendario_G);
				%rename_cols(&input_calendario_G,
					cat(name, ' = ',cats("G_",name)),
					name like "time%");
			%end;
		%else
			%do;
				%create_calendar_vars(&i,&input_cal,&&input_calendario_G&i);
				%rename_cols(&&input_calendario_G&i,
					cat(name, ' = ',cats("G&i._",name)),
					name like "time%");
			%end;
	%end;

	data &calendario_tot;
		merge &input_calendario_G:;

		/*		&input_calendario_G1*/
		/*		&input_calendario_G2*/
		/*		&input_calendario_G3*/
		/*		&input_calendario_G4;*/
		by datetime_solare date hour;
	run;

	data &calendario_tot;
		set &calendario_tot
			(keep = datetime_solare date hour G1_data_gas_rif G2_data_gas_rif G3_data_gas_rif G4_data_gas_rif);
		set &calendario_tot;
	run;

%mend;

%macro create_scada(input_scada, start_date, output);
		data _null_;
		start_date_adj=intnx("month",&start_date,-37, 'S');
		call symput('start_date_adj',start_date_adj);
	run;

	%get_scada_comp(&input_scada,&start_date_adj, scada_bil_temp);

	data scada_bil_temp(drop= hour_scada);
		set scada_bil_temp(rename=(giorno_gas_prev=data_gas_rif));
	run;

	%do i=0 %to 4;
		%create_calendar_vars(&i, scada_bil_temp,scada_bil_time_temp);

		%if &i ne 0 %then
			%do;

				data scada_bil_time_temp(rename= G&i._data_gas_rif = data_gas_rif);
					set scada_bil_time_temp;
				run;

				%create_scada_sim_vars(%eval(24*&i), scada_bil_time_temp, &start_date, &output_scada_g&i);

				data &output_scada_g&i;
					set &output_scada_g&i;
					drop time: data_gas_rif;
				run;

				%rename_cols(&output_scada_g&i,
					cat(name, ' = ',cats("G&i._",name)),
					name like "scada_bil%");
			%end;
		%else
			%do;
				%create_scada_sim_vars(%eval(24*&i), scada_bil_time_temp, &start_date, &output_scada);

				data &output_scada;
					set &output_scada;
					drop time: G_hour: ;
				run;

			%end;
	%end;

	data &output;
		merge &output_scada
			&output_scada_g:;
		by datetime_prev;
	run;

	%rename_cols(&output,
		cat(name, ' = ',cats("G_",name)),
		name like "scada_bil%");

	/*data _null_;*/
	/*  	dur = datetime() - &_timer_start;*/
	/*  	put 30*'-' / ' TOTAL DURATION:' dur time13.2 / 30*'-';*/
	/*run;*/
%mend create_scada;

%macro get_scada_comp(input_bilancioSCADA, start_date_comp, output);
	/*	%let cut_date_fe_scada="03APR2008"d;*/
	/*	%let start_date_comp = &start_date_adj;*/
	/*	%let output = scada_bil_temp;*/
	data _null_;
		dt_char=put(dhms(&start_date_comp,6,0,0), datetime19.);
		call symput('start_dt_sc',dt_char);
	run;

	data flussifisici1;
		set &input_bilancioSCADA(where=(dt_calcolo>="&start_dt_sc"dt ));
		date=datepart(dt_calcolo);
		format date date9.;
		hour_scada=timepart(dt_calcolo);
		format hour_scada hhmm.;
		rename totale=totale_smc;
		select (tx_tipologia_punto);
			when ("Entry Importazione")				type = "imp";
			when ("Entry Produzione Nazionale")		type = "pnl";
			when ("Exit Esportazione")				type = "exp";
			when ("Immissione a stoccaggio")		type = "ims";
			when ("Riconsegnato")					type = "ric";
			when ("Prelievo da stoccaggio")			type = "prs";
			when ("Delta Linepack")					type = "dlp";
			otherwise;
		end;
	run;

	/* Drop duplicates */
	proc sort data=flussifisici1 out=flussifisici1_nodup nodupkey;
		by type dt_calcolo;
	run;

	/* Bonifica su eventuali valori anomali nella riconsegna e importazioni
	(imp <=0 or ric <=0) ed eventuali valori mancanti nel pcs */
	data flussifisici1_bonifica_storico (drop=totale_smc_tmp pcs_tmp);
		set flussifisici1_nodup;
		by type dt_calcolo;

		/* RETAIN the new variable */
		retain totale_smc_tmp pcs_tmp flg_imp_ric flg_pcs;
		flg_imp_ric=0;

		/* Reset TEMP when the BY-Group changes */
		if first.type then
			do;
				totale_smc_tmp=.;
				pcs_tmp=.;
			end;

		/* Assign TEMP when valore_clean is non-missing */
		if totale_smc gt 0 and type in ("imp" "ric") then
			do;
				totale_smc_tmp=totale_smc;
				flg_imp_ric=0;
			end;
		else if totale_smc le 0 and type in ("imp" "ric") then
			do;
				totale_smc=totale_smc_tmp;
				flg_imp_ric=1;
			end;

		if pcs gt 0 then
			do;
				pcs_tmp=pcs;
				flg_pcs=0;
			end;
		else if pcs le 0 then
			do;
				pcs=pcs_tmp;
				flg_pcs=1;
			end;

		/* calcolo totale_kwh */
		totale_kwh=totale_smc*PCS;
	run;

	/* Gestione anomalie scada (solo se sono nella creazione riga di input) */
	%if &flg_anomalie=1 %then
		%do;
			%put ===== Controllo Anomalie Dati Scada Bil =====;

			%gestione_anomalie_scada(dt_base=&dt_solare, ds_in=flussifisici1_nodup, ds_out=flussifisici1_bonifica_storico);
 
/*				,storico_anomalie=&output_anomalie_scada);*/
		%end;

	proc sort data=flussifisici1_bonifica_storico;
		by dt_calcolo type;
	run;

	/*prendo il valore in smc, in quanto abbiamo visto che ? pi? esplicativo */
	proc transpose data=flussifisici1_bonifica_storico out=flussifisici_t(drop=_LABEL_);
		by dt_calcolo type;
		var totale_smc totale_kwh;
	run;

	data flussifisici_t_ren;
		set flussifisici_t;

		if _NAME_ = "totale_smc" then
			_NAME_ = "bil";
		else _NAME_ = "kwh";
	run;

	proc transpose data=flussifisici_t_ren out=flussifisici_t_ren_2 delim=_;
		by dt_calcolo;
		id _name_ type;
		var col1;
	run;

	/* Compute DLP with inverse formula */
	data flussifisici_t_smc;
		set flussifisici_t_ren_2;
		bil_stc= bil_prs-bil_ims;

		if bil_dlp=. then
			bil_dlp=sum(bil_imp, bil_pnl, bil_stc, -bil_exp,-bil_ric);
		kwh_stc=kwh_prs-kwh_ims;

		if kwh_dlp=. then
			kwh_dlp=sum(kwh_imp,kwh_pnl,kwh_stc,-kwh_exp,-kwh_ric);
		drop bil_ims bil_prs kwh_prs kwh_ims;
		rename kwh_ric=bil_ric_kwh;
	run;

	data flussi_SCADA;
		set flussifisici_t_smc(drop=kwh: _name_);
		hour_SCADA = hour(dt_calcolo);

		if hour(dt_calcolo) < 6 then
			giorno_gas_SCADA = intnx('day',datepart(dt_calcolo),-1);
		else giorno_gas_SCADA = datepart(dt_calcolo);
		format giorno_gas_SCADA date9.;
	run;

	data calendario_orario;
		date=&start_date_comp;

		do while (date<=&G_solare);
			do hour=0 to 23;
				dt_calcolo=dhms(date,hour,0,0);
				output;
			end;

			date=intnx("day", date, 1, 's');
		end;

		format date date9.;
		format dt_calcolo datetime16.;
	run;

	data _null_;
		set flussi_SCADA;
		call symputx("max_scada",max(dt_calcolo));
	run;

	data flussi_SCADA_check(drop=date hour);
		merge flussi_SCADA calendario_orario(in=p);
		by dt_calcolo;

		if p;

		if hour_Scada=. then
			do;
				hour_scada=hour;
				giorno_gas_scada=ifn(hour_scada<6, intnx("day", datepart(dt_calcolo), -1), datepart(dt_calcolo));
			end;

		if dt_calcolo<=&max_scada;
	run;

	data flussi_SCADA_check1;
		set flussi_SCADA_check;
		datetime_prev=intnx("hour",dt_calcolo,1);
		hour_prev=hour(datetime_prev);
		date_prev=datepart(datetime_prev);
		giorno_gas_prev=ifn(hour_prev<6,intnx("day",date_prev,-1),date_prev);
		format date_prev giorno_gas_prev date9.;
		format datetime_prev datetime18.;
	run;

	data flussi_SCADA_cum;
		set flussi_SCADA_check1;

		if hour_prev=6 then
			do;
				bil_ric=0;
				bil_pnl=0;
				bil_imp=0;
				bil_exp=0;
				bil_stc=0;
				bil_dlp=0;
				bil_ric_kwh=0;
			end;
	run;

	/* riempo i missing con zero: mi serve? penso di no */
	PROC STDIZE DATA=WORK.flussi_SCADA_check1 OUT=WORK.flussi_SCADA_check1_fill REPONLY MISSING=0;
		var bil_dlp bil_exp bil_imp bil_pnl bil_ric bil_stc bil_ric_kwh;
	RUN;

	PROC STDIZE DATA=WORK.flussi_SCADA_cum OUT=WORK.flussi_SCADA_cum_fill REPONLY MISSING=0;
		var bil_dlp bil_exp bil_imp bil_pnl bil_ric bil_stc bil_ric_kwh;
	RUN;

	/* statistiche calcolate sulle precedenti 24 osservazioni INCLUSO SE STESSO (QUINDI ME+23 ORE)*/
	/* il valore associato alle ore H mi dice quanto ? avvenuto fre le H:59 e (H-24):59*/
	/*io posso fare questo calcolo per capire quanto ? passato, e non dipende dal giorno gas!*/
	/* calcolo la moving average, moving sum, moving max, moving min e moving dev std per ogni voce*/
	proc expand data=work.flussi_SCADA_check1_fill out=flussi_scada_mov_stat;
		convert bil_imp = bil_imp_mean / METHOD = none TRANSFORMOUT = (MOVAVE 24);
		convert bil_imp = bil_imp_sum / METHOD = none TRANSFORMOUT = (MOVSUM 24);
		convert bil_imp = bil_imp_max / METHOD = none TRANSFORMOUT = (MOVMAX 24);
		convert bil_imp = bil_imp_min / METHOD = none TRANSFORMOUT = (MOVMIN 24);
		convert bil_imp = bil_imp_std / METHOD = none TRANSFORMOUT = (MOVSTD 24);
		convert bil_pnl = bil_pnl_mean / METHOD = none TRANSFORMOUT = (MOVAVE 24);
		convert bil_pnl = bil_pnl_sum / METHOD = none TRANSFORMOUT = (MOVSUM 24);
		convert bil_pnl = bil_pnl_max / METHOD = none TRANSFORMOUT = (MOVMAX 24);
		convert bil_pnl = bil_pnl_min / METHOD = none TRANSFORMOUT = (MOVMIN 24);
		convert bil_pnl = bil_pnl_std / METHOD = none TRANSFORMOUT = (MOVSTD 24);
		convert bil_stc = bil_stc_mean / METHOD = none TRANSFORMOUT = (MOVAVE 24);
		convert bil_stc = bil_stc_sum / METHOD = none TRANSFORMOUT = (MOVSUM 24);
		convert bil_stc = bil_stc_max / METHOD = none TRANSFORMOUT = (MOVMAX 24);
		convert bil_stc = bil_stc_min / METHOD = none TRANSFORMOUT = (MOVMIN 24);
		convert bil_stc = bil_stc_std / METHOD = none TRANSFORMOUT = (MOVSTD 24);
		convert bil_dlp = bil_dlp_mean / METHOD = none TRANSFORMOUT = (MOVAVE 24);
		convert bil_dlp = bil_dlp_sum / METHOD = none TRANSFORMOUT = (MOVSUM 24);
		convert bil_dlp = bil_dlp_max / METHOD = none TRANSFORMOUT = (MOVMAX 24);
		convert bil_dlp = bil_dlp_min / METHOD = none TRANSFORMOUT = (MOVMIN 24);
		convert bil_dlp = bil_dlp_std / METHOD = none TRANSFORMOUT = (MOVSTD 24);
		convert bil_exp = bil_exp_mean / METHOD = none TRANSFORMOUT = (MOVAVE 24);
		convert bil_exp = bil_exp_sum / METHOD = none TRANSFORMOUT = (MOVSUM 24);
		convert bil_exp = bil_exp_max / METHOD = none TRANSFORMOUT = (MOVMAX 24);
		convert bil_exp = bil_exp_min / METHOD = none TRANSFORMOUT = (MOVMIN 24);
		convert bil_exp = bil_exp_std / METHOD = none TRANSFORMOUT = (MOVSTD 24);
		convert bil_ric = bil_ric_mean / METHOD = none TRANSFORMOUT = (MOVAVE 24);
		convert bil_ric = bil_ric_sum / METHOD = none TRANSFORMOUT = (MOVSUM 24);
		convert bil_ric = bil_ric_max / METHOD = none TRANSFORMOUT = (MOVMAX 24);
		convert bil_ric = bil_ric_min / METHOD = none TRANSFORMOUT = (MOVMIN 24);
		convert bil_ric = bil_ric_std / METHOD = none TRANSFORMOUT = (MOVSTD 24);
	run;

	/* imputo i missing values di moving_stat utilizzando il valore precedente pi? vicino */
	%ffill_general(WORK.FLUSSI_SCADA_MOV_STAT,WORK.FLUSSI_SCADA_MOV_STAT_FINAL_fill,
		compress(cat("scada_",name)),
		name like "%pnl%" or name like "%exp%" or name like "%imp%" or name like "%ric%" or name like "%stc%" or name like "%dlp%" or name like "%ric_kwh%");

	data FLUSSI_SCADA_MOV_STAT_FINAL_fill(drop=_name_ time);
		set FLUSSI_SCADA_MOV_STAT_FINAL_fill;
	run;

	/* calcolo il riconsegnato cumulativo giornaliero */
	data flussi_SCADA_cumday;
		set flussi_SCADA_cum_fill;
		by giorno_gas_prev;
		retain bil_ric_cumday bil_stc_cumday bil_imp_cumday bil_pnl_cumday bil_exp_cumday bil_dlp_cumday;

		if first.giorno_gas_prev then
			do;
				bil_ric_cumday=0;
				bil_stc_cumday=0;
				bil_imp_cumday=0;
				bil_pnl_cumday=0;
				bil_exp_cumday=0;
				bil_dlp_cumday=0;
				bil_ric_kwh_cumday=0;
			end;

		bil_ric_cumday+bil_ric;
		bil_stc_cumday+bil_stc;
		bil_imp_cumday+bil_imp;
		bil_pnl_cumday+bil_pnl;
		bil_exp_cumday+bil_exp;
		bil_dlp_cumday+bil_dlp;
		bil_ric_kwh_cumday+bil_ric_kwh;

		/*		drop time;*/
	run;

	%ffill_general(WORK.FLUSSI_SCADA_CUMDAY,WORK.FLUSSI_SCADA_CUMDAY_fill,
		compress(cat("scada_",name)),
		name like "%pnl_cum%" or name like "%exp_cum%" or name like "%imp_cum%" or name like "%ric_cum%" or name like "%stc_cum%" or name like "%dlp_cum%" or name like "%ric_kwh_cum%");

	data FLUSSI_SCADA_CUMDAY_fill(drop= _name_ bil_ric bil_pnl bil_exp bil_imp bil_dlp bil_stc bil_ric_kwh);
		set FLUSSI_SCADA_CUMDAY_FILL;
	run;

	/* ottengo il dataset, che contiene i dati scada orari (dove valore delle ore cada H ? riferito a quanto ? 
	passato fino alle H:59). Ai fini previsivi il valore scada delle H lo posso usare dalle H+1!
	infatti ho introdotto ora prev */

	/* c'? da fare attenzione all'orario legale e solare quando li guardo: la macchina ha un orario continuo
	mentre io, che uso l'ora di orologio, posso essere avanti di un'ora rispetto a scada */

	/*come ora prev ho utilizzato */
	data &output(where=(giorno_gas_prev>=&start_date_comp and giorno_gas_prev <=&G_gas)
		drop =  date_prev hour_prev hour_SCADA);
		merge work.FLUSSI_SCADA_MOV_STAT_FINAL_fill(in=p) FLUSSI_SCADA_CUMDAY_fill(in=q);
		by dt_calcolo;
	run;

%mend get_scada_comp;

%macro get_scada_sim_vars(x, shift=, data=, frequency_matrix=);
	/*	%get_scada_sim_vars(shift=1, data=time1_scada, frequency_matrix=frequency_time1);*/
	/*	%let shift = 1;*/
	/*	%let data=time1_scada;*/
	/*	%let frequency_matrix=frequency_time1;*/
	data frequency_statement;
		length var_list_mvg $ 500;
		set &frequency_matrix end=eof;
		season_str = compress(put(season, $8.));
		array scada_v[6] $ 14 ("scada_bil_ric", "scada_bil_exp", "scada_bil_imp", "scada_bil_stc", "scada_bil_pnl", "scada_bil_dlp");
		array scada_bil[*] scada_bil:;

		/*	array scada_expr{&dim_} $ 100;*/
		if season = 12 then /*stagionalit� semigiornaliera-> 12 h prima*/
			season_pfx = 'hd';

			if season = 24  and _N_ = 2 then /*stagionalit� giornaliera-> 24 h prima*/
				season_pfx = 'dd';

				if season in (%eval(120- &x), 48) and _N_ = 3 then /*settimana lavorativa (120) +we (48), g1 = 120-24*/
					season_pfx = 'ww';

					if season in (%eval(3144 - &x), %eval(1248 - &x)) then /*semestre lavorativo +we, g1 = 3144-24*/
						season_pfx = 'hy';

						if season in (%eval(6240 - &x), %eval(2496 - &x)) then /*anno lavorativo +we, g1 = 6240-24*/
							season_pfx = 'yy';

							do i = 1 to dim(scada_v);
								do j = 1 to 3;
									if scada_bil[i] ne 0 then
										do;
											if season in (12, %eval(3144- &x), %eval(1248- &x)) then
												season_use = season*2*j - season;
											else season_use = season*j - &shift;
											season_use_str = compress(put(season_use, $8.));
											step = compress(put(j, $8.));
											expr = cat('convert ', compress(scada_v[i]), '=',  compress(tranwrd(scada_v[i], "bil", "bil_lag")), '_', 
												season_pfx, '_', step, ' / transform=(lag ', compress(season_use_str), ');');
											scada_var = compress((scada_v[i]));
											group = mod(j,3);

											if group = 1 then
												do;
													var_list_mvg = cat(compress(tranwrd(scada_v[i], "bil", "bil_lag")), '_', season_pfx, '_', step);
												end;
											else if group ne 1 then
												do;
													new_var_mvg = cat(compress(tranwrd(scada_v[i], "bil", "bil_lag")), '_', season_pfx, '_', step);
													var_list_mvg = catx(',', var_list_mvg, new_var_mvg);
												end;

											output;
										end;
								end;
							end;

							drop i j;
	run;

	data frequency_statement_expr;
		length lag_varlist $10000 mvg_avg_varlist $10000;
		set frequency_statement end=eof;
		retain lag_varlist mvg_avg_varlist;

		if _N_ = 1 then
			do;
				lag_varlist = expr;

				if group ne 1 then
					do;
						mvg_avg_varlist = cat(compress(tranwrd(scada_var, "bil", "bil_mvg")), '_', season_pfx, '_', step, '=',
							'mean(', compress(var_list_mvg), ')');
					end;
				else mvg_avg_varlist = '';
				output;
			end;
		else
			do;
				new_var = expr;

				if group ne 1 then
					do;
						new_mvg_avg = cat(compress(tranwrd(scada_var, "bil", "bil_mvg")), '_', season_pfx, '_', step, '=',
							'mean(', compress(var_list_mvg), ')');
					end;
				else new_mvg_avg = '';
				lag_varlist = catx(' ', lag_varlist, new_var);
				mvg_avg_varlist = catx(';', mvg_avg_varlist, new_mvg_avg);
				output;
			end;

		if eof then
			do;
				call symputx("frequency_statement", lag_varlist);
				call symputx("mvg_avg_statement", mvg_avg_varlist);
			end;
	run;

	options symbolgen mlogic;

	proc expand data=&data
		out=&data._new;
		&frequency_statement;
	run;

	data mvgavg_&data._new;
		set &data._new;
		&mvg_avg_statement;
	run;

%mend get_scada_sim_vars;

%macro create_scada_sim_vars(x, scada_with_time,start_date_sim, output_scada);
	/*	%create_scada_sim_vars(scada_bil_time_temp, &start_date, &output_scada);*/
	/*	%let x =0;*/
	/*	%let scada_with_time =  scada_bil_time_temp;*/
	/*	%let start_date_sim = &start_date;*/
	data time1_scada time2_scada;
		set &scada_with_time;

		if time_day_group1 = 1 then
			output time1_scada;
		else output time2_scada;
		keep data_gas_rif hour_prev datetime_prev scada: time: hour_: giorno_gas_scada;
	run;

	/* Declaring frequency matrix where frequencies are already adjusted for working days and holidays*/
	data frequency_time1;
		length season 8 scada_bil_ric 8 scada_bil_exp 8 scada_bil_imp 8 scada_bil_stc 8 scada_bil_pnl 8 scada_bil_dlp 8;
		call symputx("dim_", 6);
		season=12;
		scada_bil_ric=1;
		scada_bil_exp=1;
		scada_bil_imp=0;
		scada_bil_stc=0;
		scada_bil_pnl=0;
		scada_bil_dlp=1;
		output;
		season=24;
		scada_bil_ric=1;
		scada_bil_exp=1;
		scada_bil_imp=0;
		scada_bil_stc=0;
		scada_bil_pnl=0;
		scada_bil_dlp=1;
		output;
		season=120 - &x;
		scada_bil_ric=1;
		scada_bil_exp=0;
		scada_bil_imp=1;
		scada_bil_stc=1;
		scada_bil_pnl=0;
		scada_bil_dlp=0;
		output;
		season=3144 - &x;
		scada_bil_ric=1;
		scada_bil_exp=0;
		scada_bil_imp=1;
		scada_bil_stc=1;
		scada_bil_pnl=1;
		scada_bil_dlp=0;
		output;
		season=6240 - &x;
		scada_bil_ric=1;
		scada_bil_exp=1;
		scada_bil_imp=1;
		scada_bil_stc=1;
		scada_bil_pnl=1;
		scada_bil_dlp=0;
		output;
	run;

	data frequency_time2;
		length season 8 scada_bil_ric 8 scada_bil_exp 8 scada_bil_imp 8 scada_bil_stc 8 scada_bil_pnl 8 scada_bil_dlp 8;
		call symputx("dim_", 6);
		season=12;
		scada_bil_ric=1;
		scada_bil_exp=1;
		scada_bil_imp=0;
		scada_bil_stc=0;
		scada_bil_pnl=0;
		scada_bil_dlp=1;
		output;
		season=24;
		scada_bil_ric=1;
		scada_bil_exp=1;
		scada_bil_imp=0;
		scada_bil_stc=0;
		scada_bil_pnl=0;
		scada_bil_dlp=1;
		output;
		season=48;
		scada_bil_ric=1;
		scada_bil_exp=0;
		scada_bil_imp=1;
		scada_bil_stc=1;
		scada_bil_pnl=0;
		scada_bil_dlp=0;
		output;
		season=1248 - &x;
		scada_bil_ric=1;
		scada_bil_exp=0;
		scada_bil_imp=1;
		scada_bil_stc=1;
		scada_bil_pnl=1;
		scada_bil_dlp=0;
		output;
		season=2496 - &x;
		scada_bil_ric=1;
		scada_bil_exp=1;
		scada_bil_imp=1;
		scada_bil_stc=1;
		scada_bil_pnl=1;
		scada_bil_dlp=0;
		output;
	run;

	/* Get the new sim vars starting from original SCADA components */
	%get_scada_sim_vars(&x, shift=1, data=time1_scada, frequency_matrix=frequency_time1);
	%get_scada_sim_vars(&x, shift=1, data=time2_scada, frequency_matrix=frequency_time2);

	data scada_all;
		length datetime_prev data_gas_rif 8;
		set mvgavg_time1_scada_new mvgavg_time2_scada_new;
	run;

	/* Save Results */
	proc sort data=scada_all out=scada_all;
		by datetime_prev;
		where data_gas_rif >= &start_date_sim;
	run;

	%let lag_value = %eval(167 - &x);
	%let lag_day = %eval(7- &i);

	data &output_scada(drop=time);
		length datetime_prev data_gas_rif 8;
		set scada_all;
		scada_bil_ric_lag1h=lag(scada_bil_ric); /*copy for G1*/
		scada_bil_ric_lag24h=lag23(scada_bil_ric); /*copy for G1*/
		scada_bil_ric_lag&lag_day.d=lag&lag_value(scada_bil_ric); /*adapt for G1 -> -24*/
		scada_bil_ric_smooth=mean(scada_bil_ric,scada_bil_ric_lag1h); /*copy for G1*/
		scada_bil_ric_cumday_lag1h=lag(scada_bil_ric_cumday); /*copy for G1*/
		scada_bil_ric_cumday_lag24h=lag23(scada_bil_ric_cumday); /*copy for G1*/
		scada_bil_ric_cumday_lag&lag_day.d=lag&lag_value(scada_bil_ric_cumday); /*adapt for G1 -> -24*/
	run;

%mend create_scada_sim_vars;

%macro gestione_anomalie_scada(dt_base=, ds_in=, ds_out=);
/*, storico_anomalie=);*/
	/*%let dt_base=&dt_solare;*/
	/*%let ds_in=flussifisici1_nodup;*/
	/*%let ds_out=flussifisici1_bonifica_storico;*/
	/*%let storico_anomalie=&output_anomalie_scada;*/
	/* Taglio i ds di input per dt_calcolo < &start e calcolo i confronti a partire da &start */
	data _null_;
		cut_date=dhms(datepart(&dt_base),hour(&dt_base),0,0);
		call symput("cut_date",cut_date);
	run;

	proc sql noprint;
		select max(dt_calcolo) format datetime19. into: max_datetime_scada
			from &ds_in (where = (dt_calcolo<&cut_date) );
	run;

	proc sql noprint;
		select datepart(max(dt_calcolo))  into: max_date_scada
			from &ds_in (where = (dt_calcolo<&cut_date) );
	run;

	/* 2 Tipi di controllo:
	- Se ? arrivato il dato scada (circa min 4)
	- Se i valori sono attendibili nell'ultima data disponibile (ric o imp <=0) */

	/* Controllo che l'ultima riga abbia la data e ora desiderata */
	data anomalia_dates;
		date_now=datepart(&cut_date);
		hour_now=hour(&cut_date);
		datetime_now=dhms(date_now,hour_now,0,0);
		datetime_scada_use=intnx("dthour", "&max_datetime_scada"dt,1);
		scada_check_missing=ifn(datetime_scada_use=datetime_now,0,1);
		call symputx('scada_check_missing',scada_check_missing);
		format date_: date9. datetime: datetime19.;
	run;

	/* Check di consistenza su ultima riga */
	/* Se non ho ritardo nei dati --> posso fare un check sui valori contenuti nell'ultima riga */
	%if &scada_check_missing=0 %then
		%do;

			proc sql noprint;
				select max(dt_calcolo) format datetime19. into: max_dt 
					from &ds_out;
			quit;

			proc sql;
				create table anomalia_valori as 
					select "&max_dt"dt as dt_calcolo format datetime19.
						, sum(flg_imp_ric) as sum_flg_imp_ric
						, sum(flg_pcs) as sum_flg_pcs
					from &ds_out (where=(dt_calcolo = "&max_dt"dt))
				;
			quit;

			data anomalia_valori;
				set anomalia_valori;

				if sum_flg_imp_ric gt 0 or sum_flg_pcs gt 0 then
					scada_check_valori_anomali=1;
				else scada_check_valori_anomali=0;
				call symput("scada_check_valori_anomali",scada_check_valori_anomali);
			run;

		%end;

	/* Se ho ritardo nei dati --> imposto automaticamente il check_valori anomali a 0 */
	%else
		%do;

			data anomalia_valori;
				scada_check_valori_anomali=0;
				call symput("scada_check_valori_anomali",scada_check_valori_anomali);
			run;

		%end;

	%if &scada_check_valori_anomali=1 or &scada_check_missing=1 %then
		%do;
			%put ===== Anomalia nei dati scada di bilancio =====;
		%end;
	%else
		%do;
			%put ===== Non sono presenti anomalie nei dati scada di bilancio =====;
		%end;

	/* Eseguo FFILL */
	data _null_;
		dt_solare_scada=dhms(datepart(&cut_date),hour(&cut_date),0,0);
		call symput("dt_solare_scada",dt_solare_scada);
		format dt_solare_scada datetime19.;
	run;

	/* Data di inizio del calendario (min di &ds_out) */
	proc sql;
		select min(dt_calcolo) into: min_cal_temp
			from &ds_out
		;
	quit;

	data temp_calendar(where=(dt_calcolo<&dt_solare_scada and 
		dt_calcolo ge &min_cal_temp));
		date=intnx("day",datepart(&min_cal_temp),0);

		do while (date<=&max_date_scada);
			do hour=0 to 23;
				dt_calcolo=dhms(date,hour,0,0);
				hour_scada=timepart(dt_calcolo);
				output;
			end;

			date=intnx("day", date, 1, 's');
		end;

		drop hour;
		format dt_calcolo datetime27.6 date date9. hour_scada hhmm.;
	run;

	/* tx_tipologia_punto e type distinti */
	proc sql;
		create table type_distinct as 
			select distinct tx_tipologia_punto
				, type
			from &ds_out
		;
	quit;

	/* Cross join tra type_distinct e temp_calendar per creare la maschera_scada da cui parto per il ffill */
	proc sql;
		create table maschera_scada as 
			select * from  temp_calendar
				cross join type_distinct;
	quit;

	/* Ordino e merge con il ds bonificato */
	proc sort data=maschera_scada;
		by type dt_calcolo;
	run;

	proc sort data=&ds_out;
		by type dt_calcolo;
	run;

	data &ds_out._1;
		merge &ds_out maschera_scada;
		by type dt_calcolo;
	run;

	/* Ffill */
	data &ds_out. (drop=totale_smc_tmp pcs_tmp);
		set &ds_out._1;
		by type dt_calcolo;

		/* RETAIN the new variable */
		retain totale_smc_tmp pcs_tmp;

		/* Reset TEMP when the BY-Group changes */
		if first.type then
			totale_smc_tmp=.;

		/* Assign TEMP when valore_clean is non-missing */
		if totale_smc ne . then
			totale_smc_tmp=totale_smc;
		else if totale_smc = . then
			totale_smc=totale_smc_tmp;

		if pcs gt 0 then
			pcs_tmp=pcs;
		else if pcs le 0 then
			pcs=pcs_tmp;

		/* calcolo totale_kwh */
		totale_kwh=totale_smc*PCS;
	run;

	/* ===== Storicizzo anomalie ====== */
/*	%put ===== Storicizzo Eventuali Anomalie scada =====;*/
/**/
/*	data anomalie_scada;*/
/*		datetime_solare=dhms(datepart(&cut_date),hour(&cut_date),0,0);*/
/*		check_valori_anomali_scada=&scada_check_valori_anomali;*/
/*		check_ritardo_scada=&scada_check_missing;*/
/*		format datetime_solare datetime19.;*/
/*	run;*/
/**/
/*	%if %sysfunc(exist(&storico_anomalie)) %then*/
/*		%do;*/
/**/
/*			proc append base=&storico_anomalie data=anomalie_scada force;*/
/*			run;*/
/**/
/*		%end;*/
/*	%else*/
/*		%do;*/
/**/
/*			data &storico_anomalie;*/
/*				set anomalie_scada;*/
/*			run;*/
/**/
/*		%end;*/
%mend gestione_anomalie_scada;

%macro anomalie_scada_cons();

/*	PROC SQL;*/
/*		CREATE TABLE WORK.anomalie_by_dt AS */
/*			SELECT*/
			/* COUNT_DISTINCT_of_ID_SCADA */
/*		(COUNT(DISTINCT(t1.ID_SCADA))) AS count_MisuratoriCons_missing, t1.DT_RIFERIMENTO FROM WORK.CONS_TMP t1 WHERE t1.flag_ffill = 1 GROUP BY t1.DT_RIFERIMENTO;*/
/*	QUIT;*/
/**/
/*	data anomalie_now;*/
/*		set anomalie_by_dt;*/
/*		where dt_riferimento = intnx("hour", &dt_now_cons, -1);*/
/*		datetime_solare = dhms(datepart(&dt_now_cons), hour(&dt_now_cons), 0, 0);*/
/*	run;*/
/**/
/*	data base_anomalie;*/
/*		datetime_solare = dhms(datepart(&dt_now_cons), hour(&dt_now_cons), 0, 0);*/
/*		format datetime_solare datetime19.;*/
/*	run;*/
/**/
/*	data anomalie_now_complete;*/
/*		merge base_anomalie(in=L) anomalie_now(in=R);*/
/*		by datetime_solare;*/
/**/
/*		if L;*/
/**/
/*		if missing(count_MisuratoriCons_missing) then*/
/*			count_MisuratoriCons_missing = 0;*/
/*	run;*/
/**/
/*	%if %sysfunc(exist(&output_anomalie_cons)) %then*/
/*		%do;*/
/**/
/*			proc append base=&output_anomalie_cons data=anomalie_now_complete force;*/
/*			run;*/
/**/
/*		%end;*/
/*	%else*/
/*		%do;*/
/**/
/*			data &output_anomalie_cons;*/
/*				length datetime_solare count_MisuratoriCons_missing 8;*/
/*				format datetime_solare datetime19.;*/
/*				stop;*/
/*			run;*/
/**/
/*			proc append base=&output_anomalie_cons data=anomalie_now_complete force;*/
/*			run;*/
/**/
/*		%end;*/
%mend anomalie_scada_cons;

%macro create_scadacons(anagrafica, valori, start_date_cons, cons_output,dt_base);
	%checkds(&RicTot_base);

	/*%let _timer_start = %sysfunc(datetime());*/
	data dt_read;
		start_date_read=intnx("month",&g_gas,-1, 'B');
		start_dt_read=put(dhms(start_date_read,6,0,0), datetime19.);
		start_dt_all=put(dhms(&start_date_cons-1,6,0,0), datetime19.);
		dt_now_cons = dhms(datepart(&dt_base), hour(&dt_base), 0, 0);
		call symput('start_dt_all', start_dt_all);
		call symput('start_dt_read', start_dt_read);
		call symputx('dt_now_cons', dt_now_cons);
	run;

	data ANAGRAFICA_PORTATE_SCADA(drop=FL_DELETED STATO id_remi);
		set &anagrafica(where=(FL_DELETED eq "0" and STATO eq 1 and classe ne ""));
	run;

	/*	data for_svil.ANAGRAFICA_PORTATE_SCADA;*/
	/*		set ANAGRAFICA_PORTATE_SCADA;*/
	/*	run;*/
	proc sort data=ANAGRAFICA_PORTATE_SCADA out=ANAGRAFICA_PORTATE_SCADA_NODUP
		nodupkey;
		by id_scada;
	run;

	data ANAGRAFICA_PORTATE_SCADA_ADJ;
		set ANAGRAFICA_PORTATE_SCADA_NODUP;

		/* RIMOSSA FIX: MODIFICATA ANAGRAFICA 12/02/2020 ore 14.21 */
		/* 		if id_scada = 5800 then classe = "C"; */
	run;

	/* leggo valori dei misuratori in base alla presenza dello storico o meno */
	%if &dataset_presente=1 %then
		%do;

			data MISURATORI_SCADA(drop=id_unita_misura);
				set &valori(where=(data_ora_rif>="&start_dt_read"dt));

				/* 				if _N_ = 1 then */
				/* 					call symputx("min_dt_cons", data_ora_rif); */
			run;

			%let min_dt_cons="&start_dt_read"dt;

			data local_read(drop=datetime_solare 
				where=(DT_RIFERIMENTO<"&start_dt_read"dt and dt_riferimento>="&start_dt_all"dt));
				set &RicTot_base(where=(datetime_solare>="&start_dt_all"dt) /*&trainingset*/
					keep=datetime_solare G_scada_cons_a G_scada_cons_t G_scada_cons_sum G_scada_cons_f G_scada_cons_c G_scada_cons_i);
				DT_RIFERIMENTO=intnx("dthour",datetime_solare,-1);
				format DT_RIFERIMENTO datetime19.;
			run;

			data local_read(rename=(G_scada_cons_a = scada_cons_A
									G_scada_cons_t = scada_cons_t
									G_scada_cons_sum = scada_cons_sum
									G_scada_cons_f = scada_cons_f
									G_scada_cons_c = scada_cons_c
									G_scada_cons_i = scada_cons_i));
			set local_read;
			run;

			proc sort data=local_read out=local_read nodupkey;
				by DT_RIFERIMENTO;
			run;

		%end;
	%else
		%do;

			data MISURATORI_SCADA(drop=id_unita_misura);
				set &valori(where=(data_ora_rif>="&start_dt_all"dt));

				/* 				if _N_ = 1 then */
				/* 					call symputx("min_dt_cons", data_ora_rif); */
			run;

			%let min_dt_cons="&start_dt_all"dt;
		%end;

	/*	data for_svil.misuratori_scada(compress=yes);*/
	/*		set MISURATORI_SCADA;*/
	/*	run;*/
	/**/
	/*	data misuratori_scada;*/
	/*		set for_svil.misuratori_scada end=eof;*/
	/*		if _N_=1 then call symputx("min_dt_cons", data_ora_rif);*/
	/*	run;*/
	/* Creo il calendario per costruire il prodotto cartesiano */
	data calendario_cons(where=(data_gas_rif<=&g_gas and datetime_solare>=&min_dt_cons));
		date=datepart(&min_dt_cons);

		do while (date<=&g1_solare);
			do hour=0 to 23;
				datetime_solare=dhms(date,hour,0,0);

				if hour<6 then
					data_gas_rif=intnx("day", date, -1);
				else data_gas_rif=date;
				output;
			end;

			date=intnx("day", date, 1, 's');
		end;

		format data_gas_rif date9.;
		format datetime_solare datetime19.;
		drop hour;
	run;

	proc sql noprint;
		create table cross_join as select * from (
			ANAGRAFICA_PORTATE_SCADA_ADJ(keep=id_scada classe segno) cross join 
			calendario_cons(keep=datetime_solare));
	quit;

	proc sort data=cross_join;
		by id_scada datetime_solare;
	run;

	data MISURATORI_SCADA;
		set MISURATORI_SCADA;
		rename data_ora_rif=DT_RIFERIMENTO;
	run;

	proc sort data=MISURATORI_SCADA;
		by id_scada DT_RIFERIMENTO;
	run;

	data MISURATORI_SCADA_JOIN;
		merge cross_join(in=L rename=(datetime_solare=DT_RIFERIMENTO)) 
			MISURATORI_SCADA(in=R);
		by id_scada DT_RIFERIMENTO;

		if L;
	run;

	/*	data for_svil.MISURATORI_SCADA_JOIN(compress=yes);*/
	/*		set MISURATORI_SCADA_JOIN;*/
	/*	run;*/
	/*	proc sort data=MISURATORI_SCADA_JOIN;*/
	/*		by id_scada DT_RIFERIMENTO;*/
	/*	run;*/
	data cons_tmp(keep= id_scada DT_RIFERIMENTO valore_clean classe segno flag_ffill);
		set MISURATORI_SCADA_JOIN;
		valore_clean_tmp=abs(input(scan(valore,1,'**'), 12.));
		valore_string = scan(valore,2,'**');
		by id_scada;
		valore_clean = abs(valore_clean_tmp*1);
		flag_ffill = 0;

		/* RETAIN the new variable */
		retain temp_val;

		/* Reset TEMP when the BY-Group changes */
		if first.id_scada then
			temp_val=.;

		/* Assign TEMP when valore_clean is non-missing */
		if valore_clean ne . then
			temp_val=valore_clean;

		/* When valore_clean is missing, assign the retained value of TEMP into X */
		else if valore_clean=. then
			do;
				valore_clean=temp_val;

				if not missing(temp_val) then
					flag_ffill=1;
			end;

		if missing(valore_clean) then
			valore_clean = 0;
	run;

	/*	data for_svil.cons_tmp_new;*/
	/*		set cons_tmp;*/
	/*	run;*/
	/**/
	/*	data cons_tmp;*/
	/*		set for_svil.cons_tmp_new;*/
	/*	run;*/
	/* Clean preceeding missing by imputing 0, since some pressure reduction stations were not yet constructed */
	/*	proc sort data=ANAGRAFICA_PORTATE_SCADA;*/
	/*		by id_scada;*/
	/*	run;*/
	/*	Registro le anomalie orarie, contanto quanti punti non hanno trasmesso il dato */
	%if &flg_anomalie=1 %then
		%do;
			%put ===== Controllo Anomalie Dati Scada Cons =====;

			%anomalie_scada_cons();
		%end;

	/* Continuo con la Data Preparation */
	data cons_clean_class;
		set cons_tmp(drop=flag_ffill);
		valore_clean=abs(valore_clean);

		if segno eq 0 then
			valore_clean = - valore_clean;

		/*	if missing(cluster) then cluster = 'A';*/
	run;

	/*	data for_svil.cons_clean_class_prod;*/
	/*		set cons_clean_class;*/
	/*	run;*/
	/* Transformation and basic aggregations */
	proc means data=cons_clean_class sum noprint;
		class DT_RIFERIMENTO classe;
		var valore_clean;
		output out=cons_agg(where=(_TYPE_ in (3)) drop= _FREQ_) SUM=cons;
	run;

	/*	data for_svil.cons_agg_prod;*/
	/*		set cons_agg;*/
	/*	run;*/
	PROC SQL;
		CREATE TABLE WORK.cons_agg_sum AS 
			SELECT t1.DT_RIFERIMENTO, 
				(SUM(t1.cons)) AS cons,
				"SUM" as classe
			FROM WORK.CONS_AGG t1
				WHERE t1.CLASSE NOT = 'F'
					GROUP BY t1.DT_RIFERIMENTO;
	QUIT;

	/*	data for_svil.cons_agg_sum_prod;*/
	/*		set cons_agg_sum;*/
	/*	run;*/
	%if &dataset_presente=1 %then
		%do;

			proc transpose data=local_read out=local_read_t(rename=(col1=cons)) name=classe;
				by dt_riferimento;
			run;

			data cons_agg_add(where=(lowcase(classe)="sum")) cons_agg_sum_add(where=(lowcase(classe) ne "sum"));
				set local_read_t;
				classe=compress(tranwrd(classe,"scada_cons_",""));
			run;

			data cons_agg;
				set cons_agg cons_agg_add;
			run;

			data cons_agg_sum;
				set cons_agg_sum cons_agg_sum_add;
			run;

		%end;

	/* Features Engineering */
	data cons_agg_ok;
		length classe $ 3;
		set cons_agg(drop=_type_) cons_agg_sum;
		classe=upcase(classe);
		hour=hour(DT_RIFERIMENTO);
		data_gas_rif=ifn(hour>=6, datepart(DT_RIFERIMENTO), datepart(DT_RIFERIMENTO)-1 );
		format data_gas_rif date9.;
	run;

	proc sort data=cons_agg_ok nodupkey dupout=bad;
		by classe DT_RIFERIMENTO;
	run;

	data cons_agg_ok;
	set cons_agg_ok;
	if cmiss(of _all_) then delete;
	run;

	proc expand data=cons_agg_ok out=cons_lag(drop=TIME);
		by classe;
		id DT_RIFERIMENTO;
		convert cons=cons_lag / transformout=(lag 1);
	run;

	/* uso cons_lag perch? in DT_RIFERIMENTO ho il valore precedente */
	/* creo statistiche per giorno gas */
	proc expand data=cons_lag out=cons_eng(drop=TIME);
		by classe data_gas_rif;
		convert cons_lag=cons_mean / transformout=(cuave);
		convert cons_lag=cons_std / transformout=(custd);
		convert cons_lag=cons_max / transformout=(cumax);
		convert cons_lag=cons_min / transformout=(cumin);
	run;

	proc expand data=cons_eng out=cons_eng(drop=TIME);
		by classe;
		id DT_RIFERIMENTO;
		convert cons_lag=cons_lag24 / transformout=(lag 23);
		convert cons_lag=cons_smooth / transformout=(movave 2);
	run;

	proc contents data=cons_eng out=cons_vars(keep=NAME) noprint;
	run;

	proc sort data=cons_vars out=cons_vars SORTSEQ=linguistic;
		by descending name;
	run;

	data vars_ordered;
		length var_list $ 200;
		set cons_vars end=eof;
		retain var_list;

		if _N_ = 1 then
			do;
				var_list = NAME;
			end;
		else
			do;
				new_var = NAME;
				var_list = catx(' ', var_list, new_var);
			end;

		if eof then
			call symputx('reorder_vars', var_list);
	run;

	data cons_eng;
		retain &reorder_vars cons_current_hour;
		set cons_eng;
		by classe data_gas_rif;
		retain cons_cumday;

		if first.data_gas_rif then
			cons_cumday = 0;
		else cons_cumday + cons_lag;
		rename cons_lag=cons cons=cons_current_hour;
	run;

	proc sort data=cons_eng out=cons_eng_sort;
		by dt_riferimento classe;
	run;

	proc transpose data=cons_eng_sort out=cons_eng_t_temp;
		by dt_riferimento classe;
		var cons:;
	run;

	proc transpose data=cons_eng_t_temp out=cons_eng_t(drop=_name_ cons_current_hour:) delimiter=_;
		by dt_riferimento;
		id _NAME_ classe;
		var col1;;
	run;

	/* Save final datasets */
	proc contents data=cons_eng_t out=vars(keep=name) noprint;
	run;

	data vars;
		set vars(where=(name like "cons%"));
		new_name=compress(cat("scada_", name));

		/*		new_name=compress(cat("G_scada_", name));*/
		if index(new_name, "_cumday") then
			do;
				new_name=compress(catx("", tranwrd(new_name,"_cumday",""),"_cumday"));
			end;
		else 	if index(new_name, "_max") then
			do;
				new_name=compress(catx("", tranwrd(new_name,"_max",""),"_max"));
			end;
		else 	if index(new_name, "_min") then
			do;
				new_name=compress(catx("", tranwrd(new_name,"_min",""),"_min"));
			end;
		else 	if index(new_name, "_mean") then
			do;
				new_name=compress(catx("", tranwrd(new_name,"_mean",""),"_mean"));
			end;
		else 	if index(new_name, "_lag24") then
			do;
				new_name=compress(catx("", tranwrd(new_name,"_lag24",""),"_lag24"));
			end;
		else 	if index(new_name, "_smooth") then
			do;
				new_name=compress(catx("", tranwrd(new_name,"_smooth",""),"_smooth"));
			end;
		else 	if index(new_name, "_std") then
			do;
				new_name=compress(catx("", tranwrd(new_name,"_std",""),"_std"));
			end;

		rename_String=catx("=", name, new_name);
	run;

	proc sql noprint;
		select rename_String into :rename_list separated by " "
			from vars;
	run;

	%put &rename_list;

	/*G output not passed as parameter*/
	data &output_scadacons(rename=(dt_riferimento=datetime_calcolo));
		set cons_eng_t(where=(dt_riferimento>=(intnx("dtday", "&start_dt_all"dt,1, 'S'))));
		rename &rename_list;

		/*Model ="G";*/
	run;

	data &cons_output;
		set &output_scadacons;
	run;

	%do i=1 %to 4;
		%append_rename_cols(&cons_output,
			catt("G&i._",name,'=',name,';'),
			name like "scada_cons%");
	%end;

	%rename_cols(&cons_output,
		cat(name, ' = ',cats("G_",name)),
		name like "scada_cons%");

	/*	data _null_;*/
	/*  	dur = datetime() - &_timer_start;*/
	/*  	put 30*'-' / ' TOTAL DURATION:' dur time13.2 / 30*'-';*/
	/*	run;*/
%mend create_scadacons;

/* This dataset is exclusively exploited by G1*/
%macro create_flussifisici(start_date, ff_output);
	/*%let _timer_start = %sysfunc(datetime());*/
	%let StartDateFF = %sysfunc( PutN( &start_date , Date9 ) );

	/*%let EndDateFF = %Sysfunc( PutN( "3APR2020"d , Date9 ) ) ;*/
	%let remi_sel_list = ('50081701','35725601','35801601','35722901','35730301',
		'35727201','35729701','35726101','50013101','35726801','35720301','35725401',
		'35724301','35722201','50071201','35730101','35721901','35725201','35718401',
		'35718301','50029701','35718200','35718701');
	%let SQL = 
		select DT_CALCOLO,
		TX_PUNTO,
		ID_TIPOLOGIA_PUNTO,
		ID_CODICE_REMI_ASS,
		VALORE_SMC,
		PROGRESSIVO_SMC
		from flussifisici
		/*where ((dt_Calcolo)>=TO_DATE('01JAN2012', 'DDMONYYYY'));*/

	/*where ((dt_Calcolo)>=%BQUOTE('&start_date');*/
	/*		where DT_CALCOLO >= TO_DATE('&DateTimeTrial','DDMONYYYY') ;*/
	where ((dt_Calcolo)>=TO_DATE(%nrbquote(')&StartDateFF%nrbquote('), 'DDMONYYYY')
	/*		and	 (dt_Calcolo)<=TO_DATE(%nrbquote(')&EndDateFF%nrbquote('), 'DDMONYYYY')*/
	and ID_CODICE_REMI_ASS in &remi_sel_list);

	proc sql;
		connect to Oracle as x1( user=&ORAUSR password=&ORAPWD path="&ORAPATH");
		%put &sqlxmsg;
		create table WORK_FF as
			select * from connection to x1(&SQL);
		%put &sqlxmsg;
		disconnect from x1;
	quit;

	PROC SQL;
		CREATE TABLE WORK.anagrafica AS 
			SELECT DISTINCT t1.ID_TIPOLOGIA_PUNTO, 
				t1.ID_CODICE_REMI_ASS, 
				t1.TX_PUNTO
			FROM WORK.WORK_FF t1;
	QUIT;

	data work_ff_check(where=(data_Gas_rif>=&start_date));
		set work_ff;
		data_rif=datepart(dt_calcolo);
		hour_rif=hour(dt_calcolo);
		data_Gas_rif=data_rif;

		if hour_rif <6 then
			data_gas_rif=intnx("day",data_gas_rif,-1);
		format data: date9.;

		if id_codice_remi_ass in &remi_sel_list;
	run;

	proc sort data=work.work_ff_check;
		by data_rif hour_rif;
	run;

	/*dati fino a 2 ore fa in quanto 1 ora per ora legale/solare e 1 perch� i dati dell'ora hh dicono quanto gas � 
	passato fra le ore hh:oo e le hh:59*/
	PROC SQL;
		CREATE TABLE WORK.hourly_TIPOLOGIA_PUNTO AS 
			SELECT t1.data_rif, t1.data_gas_rif, t1.hour_rif,
				t1.ID_TIPOLOGIA_PUNTO, 
				/* SUM_of_VALORE_SMC */
		(SUM(t1.VALORE_SMC)) AS SUM_of_VALORE_SMC, /* SUM_of_PROGRESSIVO_SMC */
		(SUM(t1.PROGRESSIVO_SMC)) AS SUM_of_PROGRESSIVO_SMC, count(distinct t1.TX_PUNTO) as count_points FROM WORK.work_ff_check t1 GROUP BY t1.data_rif, t1.data_gas_rif, t1.hour_rif, t1.ID_TIPOLOGIA_PUNTO;
	QUIT;

	PROC SQL;
		CREATE TABLE WORK.daily_TIPOLOGIA_PUNTO AS 
			SELECT t1.data_gas_rif,
				t1.ID_TIPOLOGIA_PUNTO, 
				/* SUM_of_VALORE_SMC */
		(SUM(t1.SUM_of_VALORE_SMC)) AS SUM_of_VALORE_SMC, max(count_points) as count_points, count(distinct t1.hour_rif) as count_hours FROM WORK.hourly_TIPOLOGIA_PUNTO t1 GROUP BY t1.data_gas_rif, t1.ID_TIPOLOGIA_PUNTO;
	QUIT;

	data WORK.WORK_FF_imp;
		set WORK.work_ff_check;

		if id_tipologia_punto = "Entry GNL" then
			id_tipologia_punto= "Entry Importazione";
	run;

	PROC SQL;
		CREATE TABLE WORK.hourly_TIPOLOGIA_PUNTO_imp AS 
			SELECT t1.data_rif, t1.data_gas_rif, t1.hour_rif,
				t1.ID_TIPOLOGIA_PUNTO, 
				/* SUM_of_VALORE_SMC */
		(SUM(t1.VALORE_SMC)) AS SUM_of_VALORE_SMC, /* SUM_of_PROGRESSIVO_SMC */
		(SUM(t1.PROGRESSIVO_SMC)) AS SUM_of_PROGRESSIVO_SMC, count(distinct t1.TX_PUNTO) as count_points FROM WORK.WORK_FF_imp t1 GROUP BY t1.data_rif, t1.data_gas_rif, t1.hour_rif, t1.ID_TIPOLOGIA_PUNTO order by t1.data_rif, t1.hour_rif, t1.id_tipologia_punto;
	QUIT;

	/*flussi fisici distingue fra entry importazione ed entry gnl - analisi bilancio segna come entry importazione
	sia le importazioni che il gnl*/
	data WORK_FF_key;
		set work_ff_check;
		select (id_tipologia_punto);
			when ("Entry Importazione")				type = "imp";
			when ("Entry Produzione Nazionale")		type = "pnl";
			when ("Exit Esportazione")				type = "exp";
			when ("Immissione a stoccaggio")		type = "ims";
			when ("Entry GNL")					type = "gnl";
			when ("Prelievo da stoccaggio")			type = "prs";
			otherwise;
		end;

		key = catx("_", type, id_codice_remi_ass);
		key1 = catx("_", type,"prog", id_codice_remi_ass);
		flag_smc_null=0;

		if valore_smc = 0 then
			flag_smc_null=1;
	run;

	proc sort data=WORK_FF_key;
		by dt_calcolo data_gas_rif hour_rif;
	run;

	/*	PROC SQL;*/
	/*		CREATE TABLE WORK.check_Dates AS */
	/*			SELECT t1.TX_PUNTO, */
	/*				t1.ID_CODICE_REMI_ASS, */
	/*				t1.ID_TIPOLOGIA_PUNTO, */
	/*		(MIN(t1.data_Gas_rif)) FORMAT=DATE9. AS min_data, (MAX(t1.data_Gas_rif)) FORMAT=DATE9. AS max_data, */
	/*		sum(flag_smc_null) as sum_flag_smc_null, count(flag_smc_null) as count_values, */
	/*		sum(flag_smc_null)/count(flag_smc_null) as percent_null, max(valore_smc) as max_valore_smc, min(valore_smc) as min_valore_smc, */
	/*		mean(valore_smc) as mean_valore_smc FROM WORK.WORK_FF_KEY t1 GROUP BY t1.TX_PUNTO, t1.ID_CODICE_REMI_ASS, t1.ID_TIPOLOGIA_PUNTO;*/
	/*	QUIT;*/
	/**/
	/*	data check_Dates_filter(keep=id_codice_remi_ass);*/
	/*		set check_Dates;*/
	/**/
	/*		if percent_null<0.1 and min_data<"01JAN2013"d and max_data >=intnx("day", today(), -7);*/
	/*	run;*/
	data WORK_FF_key_filter;
		set WORK_FF_key;
	run;

	proc transpose data=WORK_FF_key_filter out=WORK_FF_key_filter_T(drop=_NAME_ _LABEL_);
		by dt_calcolo data_gas_rif hour_rif;
		var valore_smc;
		id key;
	run;

	/*devo imputare missing con gam o qualcosa*/
	/*========================================*/
	/**/
	%let end_date=&G_gas;
	%let start_date="01JAN2012"d;

	data calendario_orario;
		date=&start_date;

		do while (date<=&end_date);
			do hour=0 to 23;
				dt_calcolo=dhms(date,hour,0,0);
				output;
			end;

			date=intnx("day", date, 1, 's');
		end;

		format date date9.;
		format dt_calcolo datetime16.;
	run;

	data _null_;
		set WORK_FF_key_filter_T;
		call symputx("max_scada",max(dt_calcolo));
	run;

	data flussi_fisici_alltimes(drop=date hour);
		merge WORK_FF_key_filter_T calendario_orario(in=p);
		by dt_calcolo;

		if p;

		if dt_calcolo<=&max_scada;
	run;

	proc esm data=flussi_fisici_alltimes out=flussi_fisici_impute;
		id dt_calcolo interval=hour;
		forecast gnl: pnl: imp: exp: / model=addwinters use=predict replacemissing;
	run;

	data flussi_fisici_impute(Drop=i);
		set flussi_fisici_impute;
		hour_SCADA = hour(dt_calcolo);
		array w{*} gnl_50081701--exp_35718701;

		do i = 1 to dim(w);
			if w[i]<0 or w[i] eq . then
				w[i]=0;
		end;

		if hour(dt_calcolo) < 6 then
			giorno_gas_SCADA = intnx('day',datepart(dt_calcolo),-1);
		else giorno_gas_SCADA = datepart(dt_calcolo);

		/*	cut_Date="&max_dt_calcolo"dt;*/
		if dt_Calcolo <= &max_scada and giorno_gas_scada >= &start_date;
		format giorno_gas_SCADA date9.;

		/*	format cut_date datetime27.;*/
	run;
	


%if &flg_anomalie=1 %then %do;
	%gestione_anomalie_scadaff(work.flussi_fisici_impute);
%end;


	/*proc means data=flussi_fisici_impute;*/
	/*run;*/
	/*proc stdize data=flussi_fisici_impute reponly missing=0 out=flussi_fisici_impute;*/
	/*run;*/
	/*==============*/
	data flussi_SCADA_check1;
		set flussi_fisici_impute;
		datetime_prev=intnx("hour",dt_calcolo,1);
		hour_prev=hour(datetime_prev);
		date_prev=datepart(datetime_prev);
		giorno_gas_prev=ifn(hour_prev<6,intnx("day",date_prev,-1),date_prev);
		format date_prev giorno_gas_prev date9.;
		format datetime_prev datetime18.;
	run;


	data flussi_SCADA_cum;
		set flussi_SCADA_check1;
		array w{*} gnl_50081701--exp_35718701;

		do i = 1 to dim(w);
			if hour_prev=6 then
				do;
					w[i]=0;
				end;
		end;
	run;

	/* riempo i missing con zero: mi serve? penso di no */
	PROC STDIZE DATA=WORK.flussi_SCADA_check1 OUT=WORK.flussi_SCADA_check1 REPONLY MISSING=0;
	RUN;

	data _null_;
		length var_char $32000 var_char_temp $32000;
		set flussi_SCADA_check1;

		if _N_ = 1;
		array w{*} gnl_50081701--exp_35718701;
		retain var_char;
		var_char="";
		var_char_temp="";

		do i = 1 to dim(w);
			mean = cat("convert ", vname(w[i]), " = " , vname(w[i]), "_mean", " / METHOD = none TRANSFORMOUT = (MOVAVE 24)" );
			sum = cat ("convert ", vname(w[i]), " = " , vname(w[i]), "_sum", " / METHOD = none TRANSFORMOUT = (MOVSUM 24)" );
			max = cat ("convert ", vname(w[i]), " = " , vname(w[i]), "_max", " / METHOD = none TRANSFORMOUT = (MOVMAX 24)" );
			min = cat ("convert ", vname(w[i]), " = " , vname(w[i]), "_min", " / METHOD = none TRANSFORMOUT = (MOVMIN 24)" );
			std = cat ("convert ", vname(w[i]), " = " , vname(w[i]), "_std", " / METHOD = none TRANSFORMOUT = (MOVSTD 24)" );
			var_char_temp = catx (' ; ',mean, sum, max, min, std);

			if i = 1 then
				do;
					var_char =var_char_temp;
				end;
			else var_char = catx(';',var_char, var_char_temp);
		end;

		call symput ('list_Expand', var_char);
	run;

	/* statistiche calcolate sulle precedenti 24 osservazioni INCLUSO SE STESSO (QUINDI ME+23 ORE)*/
	/* il valore associato alle ore H mi dice quanto � avvenuto fre le H:59 e (H-24):59*/
	/*io posso fare questo calcolo per capire quanto � passato, e non dipende dal giorno gas!*/
	/* calcolo la moving average, moving sum, moving max, moving min e moving dev std per ogni voce*/
	proc expand data=work.flussi_SCADA_check1 out=flussi_scada_mov_stat;
		&list_Expand;
	run;

	%ffill_general(WORK.FLUSSI_SCADA_MOV_STAT, WORK.FLUSSI_SCADA_MOV_STAT_FINAL_fill,
		compress(cat("ff_scada_",name)),
		name like "pnl%" or name like "exp%" or name like "imp%" or name like "ric%" or name like "stc%" or name like "gnl%");

	data WORK.FLUSSI_SCADA_MOV_STAT_FINAL_fill;
		set WORK.FLUSSI_SCADA_MOV_STAT_FINAL_fill;
		drop time;
	run;

	/* calcolo il riconsegnato cumulativo giornaliero */
	proc sql noprint;
		%let newnames=;
		select compress(cat("ff_scada_",name, "_cumday"))
			into :newnames separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="FLUSSI_SCADA_CHECK1"
						and (name like "pnl%" or name like "exp%" or name like "imp%" or name like "ric%" or name like "stc%" or name like "gnl%");
	quit;

	data flussi_SCADA_cumday;
		set flussi_SCADA_check1(where=(giorno_gas_prev>="01JAN2011"d));
		by giorno_gas_prev;
		array w{*} gnl_50081701--exp_35718701;
		array new &newnames;
		retain new;

		do i = 1 to dim(w);
			if first.giorno_gas_prev then
				do;
					new[i]=0;
				end;
			else new[i]+w[i];
		end;

		drop gnl_50081701--exp_35718701 i;
	run;

	/* ottengo il dataset, che contiene i dati scada orari (dove valore delle ore cada H � riferito a quanto � 
	passato fino alle H:59). Ai fini previsivi il valore scada delle H lo posso usare dalle H+1!
	infatti ho introdotto ora prev */

	/* c'� da fare attenzione all'orario legale e solare quando li guardo: la macchina ha un orario continuo
	mentre io, che uso l'ora di orologio, posso essere avanti di un'ora rispetto a scada */

	/*come ora prev ho utilizzato */
	proc sql noprint;
		%let orignames=;
		select compress(cat("ff_scada_",name))
			into :orignames separated by ', '
				from dictionary.columns
					where libname="WORK" and memname="FLUSSI_SCADA_CHECK1"
						and (name like "pnl%" or name like "exp%" or name like "imp%" or name like "ric%" or name like "stc%" or name like "gnl%");
	quit;

	data &ff_output(where=(data_gas_g>=&start_date and data_gas_g <=&G_gas) rename=(giorno_gas_prev=data_gas_g hour_prev=hour datetime_prev=datetime_solare_g));
		merge work.FLUSSI_SCADA_MOV_STAT_FINAL_fill(in=p) flussi_SCADA_cumday(in=q);
		by dt_calcolo;
		ff_scada_mix_bestvar=sum(ff_scada_exp_35718701, ff_scada_imp_35718401, ff_scada_imp_35718200, ff_scada_gnl_50081701, 
			ff_scada_pnl_35720301, ff_scada_imp_50029701);
		ff_scada_mix_bestvar2=sum(ff_scada_exp_35718701, ff_scada_imp_35718401, ff_scada_imp_35718200, ff_scada_gnl_50081701);
		ff_scada_mix_bestvar_ALL=sum(&orignames);
		drop date_prev hour_scada giorno_gas_scada dt_calcolo;
	run;

	%rename_cols(&ff_output,
		cat(name, ' = ', cats('G1_', name)),
		name contains "ff_scada");

	/*	data _null_;*/
	/*  	dur = datetime() - &_timer_start;*/
	/*  	put 30*'-' / ' TOTAL DURATION:' dur time13.2 / 30*'-';*/
	/*	run;*/
%mend create_flussifisici;

%macro gestione_anomalie_scadaff(dataset);
	/*	prendo ultimi due giorno, elimino righe con nullo del ric, imp */
	proc sql noprint;
		select datepart(max(dt_calcolo)) into :max_scada_date
			from  &dataset;
	run;

	data temp_calendar(where=(dt_calcolo<&dt_solare));
		date=intnx("day",&max_scada_date,-1);

		do while (date<=&max_scada_date);
			do hour=0 to 23;
				dt_calcolo=dhms(date,hour,0,0);
				output;
			end;

			date=intnx("day", date, 1, 's');
		end;

		drop date hour;
		format dt_calcolo datetime16.;
	run;

	data temp;
		set &dataset;

		if imp_35718401=0 and imp_35718301=0 then
			delete;
	run;

	data temp_update;
		merge temp_calendar temp;
		by dt_calcolo;
	run;

	proc sql noprint;
		select compress(cat("t_", name))
			into :droplist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="TEMP_UPDATE"
						and name not like "dt_%" and name not like "hour_%" and name not like "giorno_gas%";
	quit;

	proc sql noprint;
		select name
			into :savelist separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="TEMP_UPDATE"
						and name not like "dt_%" and name not like "hour_%" and name not like "giorno_gas%";
	quit;

	data &dataset(drop=a b i j &droplist);
		set	temp_update;
		array new &droplist;
		array old &savelist;
		retain new;
		a=dim(new);
		b=dim(old);

		if _N_=1 then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
					old(j)=new(j);
				end;
			end;

		hour_scada=hour(dt_Calcolo);

		if hour(dt_calcolo) < 6 then
			giorno_gas_SCADA = intnx('day',datepart(dt_calcolo),-1);
		else giorno_gas_SCADA = datepart(dt_calcolo);
	run;

/* 	%ffill_general(WORK.TEMP_UPDATE, &dataset, */
/* 	compress(cat("t_", name)), */
/* 	name not like "dt_%" and name not like "hour_%" and name not like "giorno_gas%"); */
/*  */
/* 		 */
/* 	data &dataset; */
/* 	set &dataset; */
/* 	hour_scada=hour(dt_Calcolo); */
/*  */
/* 		if hour(dt_calcolo) < 6 then */
/* 			giorno_gas_SCADA = intnx('day',datepart(dt_calcolo),-1); */
/* 		else giorno_gas_SCADA = datepart(dt_calcolo); */
/* 	run; */


%mend gestione_anomalie_scadaff;


%macro create_meteocitta_param(model_type, hourcalc);
	/*	%let model_type=1;*/
	/*	%let hourcalc = 6;*/
	/*prendo le previsioni fatte in G per G+1 in base all'ora che vengono inserite/create */
	data meteo_prev_citta_g1;
		set initial_meteo_prev_citta;

		if distanza_insert_rif = &model_type; /* 1,2,3,4 for G1,G2,G3,G4 */
		hour_calc=9;
		datetime_solare=dhms(data_calc, hour_calc,0,0);
		format datetime_solare datetime16.;
	run;

	/*DATETIME SOLARE MI DICE ORA E GIORNO QUANDO MI ARRIVANO, DATA_PREV IL RIFERIMENTO DELLA PREV (+1)*/
	proc sort data=meteo_prev_citta_g1(keep= data_prev   datetime_solare tx_city tmax tmed tmin) out=meteo_prev_citta_sort nodupkey;
		by datetime_solare data_prev tx_city;
	run;

	proc transpose data= meteo_prev_citta_sort out=meteo_prev_citta_1_test;
		by datetime_solare tx_city;
	run;

	data meteo_prev_citta_fix;
		set meteo_prev_citta_1_test;

		if _name_ ne "data_prev";
		key=cats(_name_, "_", tx_city);
	run;

	proc transpose data=meteo_prev_citta_fix out=meteo_prev_citta_fix_t(drop= _name_);
		id key;
		by datetime_solare;
	run;

	/*ho i dati associati a datetime_solare, che � il datetime del giorno in cui sono state create 
	le predizioni. Questo significa che sono in G, mentre la previsione vale per G+1 */

	/* qui potrei inserire la previsione di G-1 per G+1! In questo modo ho sempre un dato che conosco in G
	e che � riferito a G+1.
	Questo significa che facendo un fill cosi nel giorno G avr� la predizione fatta in G-1 per G e non
	qualcosa riferito a G+1!*/

	/*per le prime ore (dalle 6 alle 8 incluse) uso g+2 */
	data meteo_prev_citta_g2;
		set initial_meteo_prev_citta;

		if distanza_insert_rif = %eval(&model_type+1); /* 2,3,4,5 for G1,G2,G3,G4 */
		hour_calc=&hourcalc;  /*Here CITTA difference wrt CITTAN */
		datetime_solare=dhms(INTNX("DAY",data_calc,1), hour_calc,0,0);
		format datetime_solare datetime16.;
	run;

	/*le uso il giorno dopo che sono state calcolate, dalle 0*/
	proc sort data=meteo_prev_citta_g2(keep= data_prev   datetime_solare tx_city tmax tmed tmin) out=meteo_prev_citta_sort_g2 nodupkey;
		by datetime_solare data_prev tx_city;
	run;

	proc transpose data= meteo_prev_citta_sort_g2 out=meteo_prev_citta_2_test;
		by datetime_solare tx_city;
	run;

	data meteo_prev_citta_fix_g2;
		set meteo_prev_citta_2_test;

		if _name_ ne "data_prev";
		key=cats(_name_, "_", tx_city);
	run;

	proc transpose data=meteo_prev_citta_fix_g2 out=meteo_prev_citta_fix_g2_t(drop= _name_);
		id key;
		by datetime_solare;
	run;

	data meteo_prev_citta_fin;
		set meteo_prev_citta_fix_t meteo_prev_citta_fix_g2_t;
	run;

	proc sort data=meteo_prev_citta_fin;
		by datetime_solare;
	run;

	data meteo_citta_orario;
		merge meteo_prev_citta_fin &input_calendario(in=p);
		by datetime_solare;

		if p;
		drop date hour;
	run;

	/*--*/
	%if &hourcalc=0 %then
		%do;
			/*urb variables in the old G1-2-3-4 standards are kept
					urb variables according to old standard with a h:0-9 (G1-2-3-4)
					filled values policy
			*/
			%ffill_general(WORK.METEO_CITTA_ORARIO, WORK.METEO_CITTA_ORARIO_fill,
				cat("G&model_type._urb_", name),
				name like "tm%");
		%end;
	%else
		%do;
			/* urbnew variables in the old G1-2-3-4 standards are turned into
					urb_p_ variables according to standard for forecast with a h:6-9 (G1-2-3-4)
					or h:6-10 (G) filled values policy
			*/
			%ffill_general(WORK.METEO_CITTA_ORARIO, WORK.METEO_CITTA_ORARIO_fill,
				cat("G&model_type._urb_p_", name),
				name like "tm%");
		%end;

	/*--ho che il dato ha gi� la data scalata indietro di uno rispetto al target(previsione ric)!----*/

	/* datetime_solare � legato al giorno in cui utilizzo il dato quindi � oggi
	dalle 6 alle 8 uso prev+2 che ho da ieri, dalle 9 uso quella +1 che � stata calcolata oggi--- */

	/*OUTPUT G1 e UTENZA*/
	%if &model_type =1 %then
		%do;
			%if &hourcalc=0 %then
				%do;
					/* CITTA */
					data &&output_meteo_citta_prev( where=(data_gas_G>=&start_date));
						set METEO_CITTA_ORARIO_fill;
						data_solare_G=datepart(datetime_solare);
						hour=hour(datetime_solare);
						data_gas_G=ifn(hour<6, intnx("day", data_solare_G,-1), data_solare_G);
						format data_gas_G data_solare_G date9.;
						drop data_gas_rif;

						/*		Model = "G&model_type";*/
					run;

				%end;
			%else
				%do;
					/* CITTAN */
					data &&output_meteo_citta_prev_new( where=(data_gas_G>=&start_date));
						set METEO_CITTA_ORARIO_fill;
						data_solare_G=datepart(datetime_solare);
						hour=hour(datetime_solare);
						data_gas_G=ifn(hour<6, intnx("day", data_solare_G,-1), data_solare_G);
						format data_gas_G data_solare_G date9.;
						drop data_gas_rif;

						/*		Model = "G&model_type";*/
					run;

				%end;
		%end;

	/*OUTPUT G2, G3, G4*/
	%else
		%do;

			data &&output_meteo_citta_prevnew&model_type( where=(data_gas_G>=&start_date));
				set METEO_CITTA_ORARIO_fill;
				data_solare_G=datepart(datetime_solare);
				hour=hour(datetime_solare);
				data_gas_G=ifn(hour<6, intnx("day", data_solare_G,-1), data_solare_G);
				format data_gas_G data_solare_G date9.;
				drop data_gas_rif;

				/*		Model = "G&model_type";*/
			run;

		%end;

	proc datasets lib=work nolist;
		delete meteo:;
%mend create_meteocitta_param;

%macro GxG_create_meteocitta(groups_meteo_citta, input_calendario, output);
	/*%GxG_create_dataset_meteo_citta(&meteo_citta, &groups_meteo_citta, &input_calendario, */
	/*		&start_date, &output_citta);*/
	/*	%let input_dset = &meteo_citta;*/
	/*	%let output = &output_citta;*/

	/* same processes in between these ds -> possible optimization ?

	input: 	meteo_cons_citta
	meteo_prev_citta_g
	meteo_prev_citta_g1

	output: meteo_cons_citta_t
	meteo_prev_citta_g_t
	meteo_prev_citta_g_t

	*/

	/* ------------ consuntivo ------------------ */
	proc sort data= initial_meteo_cons_citta out=meteo_cons_citta_out;
		by tX_city data_rif dt_creazione;
	run;

	data meteo_cons_citta_out(rename=(data_rif=data_gas_rif) drop=id_tipo_dato k_d_e: dt: distanza:);
		set meteo_cons_citta_out;
		by tX_city data_rif;

		if last.data_rif;
	run;

	proc sort data=meteo_cons_citta_out;
		by data_gas_rif tX_city;
	run;

	proc transpose data=meteo_cons_citta_out out=meteo_cons_citta_t(Drop=_label_);
		by data_gas_rif tx_city;
	run;

	data meteo_cons_citta_t;
		set meteo_cons_citta_t;

		if _name_ ne "data_calc";
		key=cats(_name_, "_", tx_city);
	run;

	proc transpose data=meteo_cons_citta_t out=meteo_cons_citta_t(drop= _name_);
		id key;
		by data_gas_rif;
	run;

	data meteo_cons_citta_t;
		set meteo_cons_citta_t;
		data_calc=intnx("day",data_gas_rif,+1);
		miss=nmiss(of tm:);
		format data_calc date9.;
	run;

	%add_meteo_vars(meteo_cons_citta_out,&groups_meteo_citta, meteo_cons_cluster);

	data meteo_cons_citta_t;
		merge meteo_cons_citta_t meteo_cons_cluster;
		by data_gas_rif;
	run;

	/* ------------ previsioni ------------------ */
	/*prendo le previsioni fatte in G per G */
	data meteo_prev_citta_g;
		set initial_meteo_prev_citta;

		if distanza_insert_rif = 0;
	run;

	proc sort data=meteo_prev_citta_g out=meteo_prev_citta_g;
		by tX_city data_prev dt_creazione;
	run;

	data meteo_prev_citta_g(rename=(data_prev=data_gas_rif) drop=id_tipo_dato k_d_e: dt: distanza:);
		set meteo_prev_citta_g;
		by tX_city data_prev;

		if last.data_prev;
	run;

	proc sort data=meteo_prev_citta_g out=meteo_prev_citta_g;
		by data_gas_rif tX_city;
	run;

	proc transpose data=meteo_prev_citta_g out=meteo_prev_citta_g_t(Drop=_label_);
		by data_gas_rif tx_city;
	run;

	data meteo_prev_citta_g_t;
		set meteo_prev_citta_g_t;

		if _name_ ne "data_calc";
		key=cats(_name_, "_", tx_city);
	run;

	proc transpose data=meteo_prev_citta_g_t out=meteo_prev_citta_g_t(drop= _name_);
		id key;
		by data_gas_rif;
	run;

	data meteo_prev_citta_g_t;
		set meteo_prev_citta_g_t;
		data_calc=intnx("day",data_gas_rif,0);
		format data_calc date9.;
		miss=nmiss(of tm:);
	run;

	%add_meteo_vars(meteo_prev_citta_g,&groups_meteo_citta, meteo_prev_cluster_g);

	data meteo_prev_citta_g_t;
		merge meteo_prev_citta_g_t meteo_prev_cluster_g;
		by data_gas_rif;
	run;

	/* ------------ previsioni g1 ------------------ */
	/*prendo le previsioni fatte in G per G+1 */
	data meteo_prev_citta_g1;
		set initial_meteo_prev_citta;

		if distanza_insert_rif = 1;
	run;

	proc sort data=meteo_prev_citta_g1 out=meteo_prev_citta_g1;
		by tX_city data_prev dt_creazione;
	run;

	data meteo_prev_citta_g1(rename=(data_prev=data_gas_rif) drop=id_tipo_dato k_d_e: dt: distanza:);
		set meteo_prev_citta_g1;
		by tX_city data_prev;

		if last.data_prev;
	run;

	proc sort data=meteo_prev_citta_g1;
		by data_gas_rif tX_city;
	run;

	proc transpose data=meteo_prev_citta_g1 out=meteo_prev_citta_g1_t(Drop=_label_);
		by data_gas_rif tx_city;
	run;

	data meteo_prev_citta_g1_t;
		set meteo_prev_citta_g1_t;

		if _name_ ne "data_calc";
		key=cats(_name_, "_", tx_city);
	run;

	proc transpose data=meteo_prev_citta_g1_t out=meteo_prev_citta_g1_t(drop= _name_);
		id key;
		by data_gas_rif;
	run;

	data meteo_prev_citta_g1_t;
		set meteo_prev_citta_g1_t;
		data_calc=intnx("day",data_gas_rif,-1);
		format data_calc date9.;
		miss=nmiss(of tm:);
	run;

	%add_meteo_vars(meteo_prev_citta_g1,&groups_meteo_citta, meteo_prev_cluster_g1);

	data meteo_prev_citta_g1_t;
		merge meteo_prev_citta_g1_t meteo_prev_cluster_g1;
		by data_gas_rif;
	run;

	/*================================*/
	data meteo_cons_cons;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set meteo_cons_citta_t;
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(data_calc,10,0,0);
		format datetime_solare datetime_calc datetime19.;
	run;

	data meteo_cons_prevg;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set meteo_prev_citta_g_t;
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(intnx("day",data_gas_rif,+1),6,0,0);

		*g per g che uso la mattina dopo finch? arriva il vero consuntivo;
		format datetime_solare datetime_calc datetime19.;
	run;

	data meteo_cons;
		set meteo_cons_cons meteo_cons_prevg;
	run;

	proc sort data=meteo_cons;
		by data_gas_rif datetime_solare;
	run;

	/*================================*/
	/*fix*/
	data meteo_prev_prevg;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set meteo_prev_citta_g_t;
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(data_calc,10,0,0);
		format datetime_solare datetime_calc datetime19.;
	run;

	data meteo_prev_prevg1;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set meteo_prev_citta_g1_t;
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(intnx("day",data_gas_rif,0),6,0,0);
		format datetime_solare datetime_calc datetime19.;
	run;

	data meteo_prev;
		set meteo_prev_prevg meteo_prev_prevg1;
	run;

	proc sort data=meteo_prev;
		by data_gas_rif datetime_solare;
	run;

	/*================================*/
	%fill_rename_meteocitta(meteo_cons, &input_calendario, "G_urb_c", output_cons, &start_date_adj);
	%fill_rename_meteocitta(meteo_prev,  &input_calendario, "G_urb_p", output_prev_g, &start_date_adj);

	data &output(Rename=(date=data_solare_calc) 
		where=(datetime_solare >= dhms(&start_date, 6,0,0)));
		length datetime_solare hour data_solare_rif data_gas_rif 8.;
		merge output_cons(in=q 	drop=datetime_calc data: miss) 
			output_prev_g(in=p drop=datetime_calc data: miss);
		by datetime_solare;
		data_solare_rif=datepart(datetime_solare);
		hour=hour(datetime_solare);
		data_gas_rif=ifn(hour<6, data_solare_rif-1, data_solare_rif);

		if q and p;
		format data: date9.;
	run;

	data &output(drop= hour data_gas_rif data_solare_rif);
		set &output;

		/*	Model ="G";*/
	run;

%mend GxG_create_meteocitta;

%macro fill_rename_meteocitta(input_dset,input_calendario, prefix, output_dset, start_cal);
	/*	%let input_dset=meteo_cons;*/
	/*	%let prefix="cons";*/
	/*	%let output_dset= meteo_consuntivo_final;*/
	/*	%let start_cal=&start_date;*/
	proc sort data=&input_dset out=temp;
		by datetime_solare;
	run;

	data temp;
		merge temp &input_calendario(in=p where=(datepart(datetime_solare)>=&start_cal) keep=datetime_solare);
		by datetime_solare;

		if p;
	run;

	%ffill_general(work.temp, &output_dset,
		compress(cat(&prefix, "_" , name)),
		name like "tm%" or name like "tall%");
%mend fill_rename_meteocitta;

/* Funzioni meteo per generazione dataset */
%macro add_meteo_vars(input,grouping, output);
	/*	%let input=meteo_cons_citta_out;*/
	/*%let output=meteo_cons_cluster;*/
	proc sort data=&input(keep=data_gas_rif) out= &output nodupkey;
		by data_gas_rif;
	run;

	proc sort data=&input out=meteo_city;
		by tX_city data_gas_rif;
	run;

	data grouping_meteo;
		set &grouping;
	run;

	data input_clusters;
		merge meteo_city(in=p) grouping_meteo;
		by tx_city;

		if p;
	run;

	proc sql noprint;
		%let list_clusters=;
		select name
			into :list_clusters separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="GROUPING_METEO"
						and lowcase(name) not like "tx_city%";
	quit;

	%let count=%sysfunc(countw(&list_clusters));

	%do i = 1 %to &count;
		%let value=%scan(&list_clusters,&i);

		/*		%let i=1;*/
		/*		%let value=Top10_popolazione;*/
		data input_clusters_run;
			set input_clusters(where=(&value ne "") keep=data_gas_rif tx_city tm: &value);
		run;

		proc sql noprint;
			create table new_vars1_cluster
				as select t1.&value, 
					t1.data_gas_rif as data_gas_rif,
					max(t1.tmax) as tmax_max,
					min(t1.tmin) as tmin_min,
					/*					mean(t1.tmed) as tmed_med,*/
			std(t1.tmed) as tmed_std
			from input_clusters_run t1
				group by &value, data_gas_rif
					order by &value, data_gas_rif;
		quit;

		proc sort data=input_clusters_run out=input_clusters_run;
			by &value tX_city data_gas_rif;
		run;

		proc transpose data=input_clusters_run out=input_clusters_run_t(rename=(col1=value));
			by &value tX_city data_gas_rif;
			var tmax tmed tmin;
		run;

		proc sql noprint;
			create table new_vars2_cluster
				as select t1.&value, 
					t1.data_gas_rif as data_gas_rif,
					mean(t1.value) as tall_med
					/*					,std(t1.value) as tall_std*/
			from input_clusters_run_t t1
				group by &value, data_gas_rif
					order by &value, data_gas_rif;
		quit;

		proc sql noprint;
			create table RUN_OUTPUT as select * 
				from new_vars1_cluster t1 inner join new_vars2_cluster t2
					on t1.&value=t2.&value and t1.data_gas_rif=t2.data_gas_rif
				order by data_gas_rif, &value;
		quit;

		proc transpose data=RUN_OUTPUT out=RUN_OUTPUT_t;
			by data_gas_rif &value;
		run;

		data RUN_OUTPUT_t;
			set RUN_OUTPUT_t;
			key=cats(_name_ , "_", &value);
		run;

		proc transpose data=RUN_OUTPUT_t out=RUN_OUTPUT_t_add(drop= _name_);
			id key;
			by data_gas_rif;
		run;

		data &output;
			merge &output(in=p) RUN_OUTPUT_t_add;
			by data_gas_rif;
		run;

	%end;
%mend add_meteo_vars;

/*---------------NAMING------------------*/
/*   G: urb_c_ (G), urb_g_ (G)  -> urb_p_ */

/*
/*	 G1,G2,G3,G4: urb (G1), urbnew (G1,G2,G3,G4) -> urb_p_ (see Motivation2) */

/*

/*Motivation1: urb follows h:00 switch*/

/*Motivation2: urbnew and urb_g_ follow h:06 switch. In particolar urb_p_ pretty
close to CITTAN but h:06-10 filling policy instead of h:06-09 (CITTAN)*/
%macro create_meteocitta(input_citta, start_date, output_citta_tot);
	/*%let _timer_start = %sysfunc(datetime());*/
	%let input_citta = &meteo_citta;

	data _null_;
		start_date_adj=intnx("month",&start_date,-1, 'S');
		call symput('start_date_adj',start_date_adj);
	run;

	/*	%let time = %sysfunc(putn(&start_date_adj, datetime20));*/
	/*	%put time=&time;*/
	/*Preprocess meteocitta data*/
	data initial_meteo_citta(rename= (nr_temp_max=tmax nr_temp_min=tmin nr_temp_med=tmed));
		set &input_citta(where=(datepart(dt_data_elaborazione)>=&start_date_adj));
		distanza_insert_rif=datepart(dt_data_riferimento)-datepart(dt_data_elaborazione);
		data_prev=datepart(dt_data_riferimento);
		data_calc=datepart(dt_data_elaborazione);

		if tx_city="Forli'" then
			tx_city="Forli";

		if tx_city="L'Aquila" then
			tx_city="Aquila";

		if tx_city="La Spezia" then
			tx_city="LaSpezia";
		tx_city=compress(tx_city);
		format data_prev data_calc date9.;
	run;

	/*Identify prev and cons data*/
	data initial_meteo_prev_citta initial_meteo_cons_citta(rename=(data_prev=data_rif));
		set initial_meteo_citta;

		if id_tipo_dato="PREV" then
			output initial_meteo_prev_citta;
		else if id_tipo_dato="CONS" then
			output initial_meteo_cons_citta;
	run;

	/*Create dataset for each model*/
	%create_meteocitta_param(1,0); /*G1 - CITTA*/
	%create_meteocitta_param(1,6); /*G1 - CITTAN*/
	%create_meteocitta_param(2,6); /*G2 - CITTAN*/
	%create_meteocitta_param(3,6); /*G3 - CITTAN*/
	%create_meteocitta_param(4,6); /*G4 - CITTAN*/
	%GxG_create_meteocitta(&groups_meteo_citta, &input_calendario,
		&output_citta);

	data &output_citta_tot(drop =  data_solare_G hour data_gas_G);
		merge &output_meteo_citta_prev
			&output_meteo_citta_prev_new
			&output_meteo_citta_prevnew2
			&output_meteo_citta_prevnew3
			&output_meteo_citta_prevnew4
			&output_citta;
		by datetime_solare;
	run;

	/*data _null_;*/
	/*	dur = datetime() - &_timer_start;*/
	/*	put 30*'-' / ' TOTAL DURATION:' dur time13.2 / 30*'-';*/
	/*run;*/
%mend create_meteocitta;

%macro GxG_create_meteocelle(celle_start, grouping_all, output_final);

	PROC SQL;
		CREATE TABLE WORK.map_celle_cons AS 
			SELECT t1.SUPERFICIE, 
				(COUNT(DISTINCT(t1.RIGA))) AS COUNT_RIGA, 
				(COUNT(DISTINCT(t1.COLONNA))) AS COUNT_COLONNA,
				max(data_rif) format=date9. as max_data,
				min(data_rif) format=date9. as min_data
			FROM WORK.CONSUNTIVI t1
				GROUP BY t1.SUPERFICIE;
	QUIT;

	/*============================================================================*/
	/*======== consuntivi =========*/
	/*============================================================================*/
	/*non ho aggiornamento del dato, perlomeno non lo vedo (vedo solo quello "attivo" cio? l'ultimo)*/
	%prepare_meteo_celle_dset(consuntivi, &grouping_all,consuntivi_out);

	/*============================================================================*/
	/*======== previsioni g=========*/
	/*============================================================================*/
	/* ho aggiornamento del dato, perlomeno non lo vedo (vedo solo quello "attivo" cio? l'ultimo)*/
	proc sort data=previsioni(where=(distanza_insert_rif=0)) out=previsioni_g nodupkey dupout=dup;
		by cella data_calc  data_prev dt_insert;
	run;

	data previsioni_g(rename=(data_prev=data_rif));
		set previsioni_g;
		by cella data_calc  data_prev;

		if  last.data_prev;
	run;

	%prepare_meteo_celle_dset(previsioni_g,&grouping_all,previsioni_g_out);

	/*============================================================================*/
	/*======== previsioni g1=========*/
	/*============================================================================*/
	/* ho aggiornamento del dato, perlomeno non lo vedo (vedo solo quello "attivo" cio? l'ultimo)*/
	proc sort data=previsioni(where=(distanza_insert_rif=1)) out=previsioni_g1 nodupkey dupout=dup;
		by cella data_calc  data_prev dt_insert;
	run;

	data previsioni_g1(rename=(data_prev=data_rif));
		set previsioni_g1;
		by cella data_calc  data_prev;

		if  last.data_prev;
	run;

	%prepare_meteo_celle_dset(previsioni_g1,&grouping_all,previsioni_g1_out);

	/*------------------*/
	data previsioni_g_out_fill;
		set consuntivi_out(where=(data_rif<&start_date_celle_p)) previsioni_g_out;
	run;

	data previsioni_g1_out_fill;
		set consuntivi_out(where=(data_rif<&start_date_celle_p)) previsioni_g1_out;
	run;

	/*================================*/
	/*================================*/
	data meteocelle_cons_cons;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set consuntivi_out(rename=(data_rif=data_gas_rif));
		data_calc=intnx("day",data_gas_rif,+1);
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(data_calc,10,0,0);
		format data_calc date9. datetime_solare datetime_calc datetime19.;
	run;

	data meteocelle_cons_prevg;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set previsioni_g_out_fill(rename=(data_rif=data_gas_rif));
		data_calc=intnx("day",data_gas_rif,+0);
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(intnx("day",data_gas_rif,+1),6,0,0);

		*g per g che uso la mattina dopo finch? arriva il vero consuntivo;
		format data_calc date9. datetime_solare datetime_calc datetime19.;
	run;

	data meteocelle_cons;
		set meteocelle_cons_cons meteocelle_cons_prevg;
	run;

	proc sort data=meteocelle_cons;
		by data_gas_rif datetime_solare;
	run;

	/*================================*/
	/*fix*/
	data meteocelle_prev_prevg;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set previsioni_g_out_fill(rename=(data_rif=data_gas_rif));
		data_calc=intnx("day",data_gas_rif,+0);
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(data_calc,10,0,0);
		format data_calc date9. datetime_solare datetime_calc datetime19.;
	run;

	data meteocelle_prev_prevg1;
		length data_gas_rif datetime_solare datetime_calc data_calc 8.;
		set previsioni_g1_out_fill(rename=(data_rif=data_gas_rif));
		data_calc=intnx("day",data_gas_rif,-1);
		datetime_calc=dhms(data_calc,10,0,0);
		datetime_solare=dhms(intnx("day",data_gas_rif,0),6,0,0);
		format data_calc date9. datetime_solare datetime_calc datetime19.;
	run;

	data meteocelle_prev;
		set meteocelle_prev_prevg meteocelle_prev_prevg1;
	run;

	proc sort data=meteocelle_prev;
		by data_gas_rif datetime_solare;
	run;

	/*================================*/
	%fill_rename_meteocelle(meteocelle_cons, "G_met_c", meteocelle_cons1, &celle_start);
	%fill_rename_meteocelle(meteocelle_prev, "G_met_p", meteocelle_prev1, &celle_start);

	data &output_final(Rename=(date=data_solare_calc) where=(datetime_solare >=dhms(&celle_start, 6,0,0) ));
		length datetime_solare hour data_solare_rif data_gas_rif 8.;
		merge meteocelle_cons1(in=q 	drop=datetime_calc data: ) 
			meteocelle_prev1(in=p drop=datetime_calc data: );
		by datetime_solare;
		data_solare_rif=datepart(datetime_solare);
		hour=hour(datetime_solare);
		data_gas_rif=ifn(hour<6, data_solare_rif-1, data_solare_rif);

		if q and p;
		format data: date9.;
	run;

	data &output_final(drop =  data: hour);
		set &output_final;

		/*	Model ="G";*/
	run;

%mend GxG_create_meteocelle;

%macro prepare_meteo_celle_dset(dset_meteo,grouping_all, dset_meteo_out);

	proc sort data=&dset_meteo out=&dset_meteo nodupkey;
		by data_rif cella;
	run;

	%add_celle_vars_all(&dset_meteo,&grouping_all, output_cons);

	proc sql noprint;
		%let var_groups=;
		select lowcase(name)
			into :var_groups separated by ' '
				from dictionary.columns
					where libname="WORK" and memname=upcase("&dset_meteo")
						and (lowcase(name) like "t_med%" or lowcase(name) like "t_gg%" or
						lowcase(name) like "altezza_min%" or lowcase(name) like "t_max_perc%" 
						or lowcase(name) like "umidita%" );  /*"t_max_perc%"*/
	quit;

	%let count_var=%sysfunc(countw(&var_groups));

	proc sort data=&dset_meteo(keep=data_rif) out= output_selectvar nodupkey;
		by data_rif;
	run;

	%do j = 1 %to &count_var;
		%let var_cycle=%scan(&var_groups,&j);

		%add_celle_vars_selectvar(&dset_meteo,&&grouping_&var_cycle., 
			output_cons_&var_cycle,&var_cycle);

		data output_selectvar;
			merge output_selectvar(in=p) output_cons_&var_cycle;
			by data_rif;
		run;

	%end;

	/*output_cons e output_selectvar sono gli output*/
	/*devo mergarli su data_rif*/
	/* --------------------------- */
	data &dset_meteo_out;
		merge output_cons(in=p) output_selectvar;
		by data_rif;
	run;

%mend;

%macro fill_rename_meteocelle(input_dset, prefix, output_dset, start_cal);

	proc sort data=&input_dset out=temp;
		by data_gas_rif;
	run;

	data temp(drop=date hour);
		merge temp &input_calendario(in=p where=(data_gas_rif>=&start_cal));
		by datetime_solare;

		if p;
	run;

	/* Split ffill in two steps as the numbers of variables is too big to save them in
	a macro variable whose name has an upper bound of 65k chars*/

	/*vars != sup*/
	%ffill_general(WORK.TEMP, WORK.TEMP1,
		compress(cat(&prefix, "_" , lowcase(name))),
		lowcase(name) not like "dat%" and lowcase(name) not like "sup%");

	/*vars = sup*/
	%ffill_general(WORK.TEMP1, &output_dset,
		compress(cat(&prefix, "_" , lowcase(name))),
		lowcase(name) not like "dat%" and lowcase(name) like "sup%");
%mend;

/* serve aggiungere le funzioni di grouping/clustering */
%macro add_celle_vars_all(input,grouping, output);

	proc sort data=&input(keep=data_rif) out= &output nodupkey;
		by data_rif;
	run;

	proc sort data=&input out=meteo_city(drop=superficie riga colonna
		rename=( altezza_min=alt_min)); /*rename =(t_max_percepita=t_max_perc)*/
		by cella data_rif;
	run;

	data grouping_meteo;
		set &grouping;
	run;

	data input_clusters;
		merge meteo_city(in=p) grouping_meteo;
		by cella;

		if p;
	run;

	proc sql noprint;
		%let list_clusters=;
		select name
			into :list_clusters separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="GROUPING_METEO"
						and lowcase(name) not like "cella%";
	quit;

	%let count=%sysfunc(countw(&list_clusters));

	%do i = 1 %to &count;
		%let value=%scan(&list_clusters,&i);

		proc sort data=input_clusters(where=(&value ne "") 
			keep=data_rif cella t: UMIDITA alt_min PRECIPITAZIONI NEVE &value)
			out=input_clusters_run;
			by  &value data_rif;
		run;

		proc means data=input_clusters_run noprint;
			output out=new_vars1_cluster(drop=_TYPE_ _FREQ_);
			var t: UMIDITA alt_min;
			by &value data_rif;
		run;

		proc sort data=new_vars1_cluster out=new_vars1_cluster;
			by &value data_rif  _stat_;
		run;

		proc transpose data=new_vars1_cluster(where=(_stat_ not in ("N"))) out=new_vars1_cluster_t;
			by &value data_rif _stat_;
		run;

		data new_vars1_cluster_t(drop=_label_ _name_ _stat_);
			set new_vars1_cluster_t(rename=(col1=value));
			key = catx("_",_name_, _stat_);
		run;

		proc sort data=new_vars1_cluster_t out=new_vars1_cluster_t;
			by   &value data_rif  key;
		run;

		proc transpose data=new_vars1_cluster_t out=new_vars1_cluster_final(Drop=_name_);
			id key;
			by &value data_rif;
		run;

		proc sql noprint;
			create table new_vars2_cluster
				as select t1.&value, 
					t1.data_rif as data_rif,
					mean(t1.PRECIPITAZIONI) as prec_mean
					,sum(t1.PRECIPITAZIONI) as prec_sum,
					mean(t1.neve) as neve_mean
					,sum(t1.neve) as neve_sum
				from input_clusters_run t1
					group by &value, data_rif
						order by &value, data_rif;
		quit;

		proc sql noprint;
			create table output_run as select * 
				from new_vars1_cluster_final t1 inner join new_vars2_cluster t2
					on t1.&value=t2.&value and t1.data_rif=t2.data_rif
				order by data_rif, &value;
		quit;

		proc transpose data=output_run out=output_run_t;
			by data_rif &value;
		run;

		data output_run_t;
			set output_run_t;
			key=cats(&value,  "_", _name_);
		run;

		proc transpose data=output_run_t out=output(drop= _name_);
			id key;
			by data_rif;
		run;

		data &output;
			merge &output(in=p) output;
			by data_rif;
		run;

	%end;
%mend;

%macro add_celle_vars_selectvar(input,grouping, output, selectvar);

	proc sort data=&input(keep=data_rif) out= &output nodupkey;
		by data_rif;
	run;

	proc sort data=&input out=meteo_city(drop=superficie riga colonna);
		by cella data_rif;
	run;

	data grouping_meteo;
		set &grouping;
	run;

	data input_clusters;
		merge meteo_city(in=p) grouping_meteo;
		by cella;

		if p;
	run;

	proc sql noprint;
		%let list_clusters=;
		select name
			into :list_clusters separated by ' '
				from dictionary.columns
					where libname="WORK" and memname="GROUPING_METEO"
						and lowcase(name) not like "cella%";
	quit;

	%let count=%sysfunc(countw(&list_clusters));

	%do i = 1 %to &count;
		%let value=%scan(&list_clusters,&i);

		data check;
			value="&value";

			if index(value,"bestHP_label")>0 then
				singlecell=1;
			else singlecell=0;
			call symput("flag",singlecell);
		run;

		%if &flag eq 0 %then
			%do;

				proc sort data=input_clusters(where=(&value ne "") 
					keep=data_rif cella &selectvar &value)
					out=input_clusters_run;
					by  &value data_rif;
				run;

				proc means data=input_clusters_run noprint;
					output out=new_vars1_cluster(drop=_TYPE_ _FREQ_);
					var &selectvar;
					by &value data_rif;
				run;

				proc sort data=new_vars1_cluster;
					by   &value data_rif  _stat_;
				run;

				proc transpose data=new_vars1_cluster(where=(_stat_ not in ("N"))) out=new_vars1_cluster_t;
					by &value data_rif _stat_;
				run;

				data new_vars1_cluster_t(drop=_label_ _name_ _stat_);
					set new_vars1_cluster_t(rename=(col1=value));
					key = catx("_",&value, _stat_);
				run;

				proc sort data=new_vars1_cluster_t out=new_vars1_cluster_t;
					by   &value data_rif  key;
				run;

				proc transpose data=new_vars1_cluster_t out=output_run(Drop=_name_);
					id key;
					by &value data_rif;
				run;

				proc sort data=output_run out=output_tempc(drop=&value);
					by data_rif  &value;
				run;

			%end;
		%else
			%do;

				proc sort data=input_clusters(where=(&value ne "") 
					keep=data_rif cella &selectvar &value)
					out=input_clusters_run;
					by  &value data_rif;
				run;

				data input_clusters_run;
					set input_clusters_run;
					key_name=catx("_",&value,"&selectvar");
				run;

				proc sort data=input_clusters_run out=new_vars1_cluster;
					by   data_rif key_name;
				run;

				proc transpose data=new_vars1_cluster out=output_tempc(Drop=_:);
					by  data_rif;
					id key_name;
				run;

			%end;

		data &output;
			merge &output(in=p) output_tempc;
			by data_rif;
		run;

	%end;
%mend;

%macro create_meteoprev_param(meteop_flg);
	/**/
	/*	%let meteop_flg =1;*/
	/*	%let p1=1;*/
	/*	%if &meteop_flg =1 %then %do;*/
	/*		%let p1 = 1;*/
	/*	%end;*/
	/*	%else %do;*/
	/*		%let p1 = 2;*/
	/*	%end;*/
	data meteo_prev;
		set initial_previsioni;

		if intnx('day',datepart(dt_insert),&meteop_flg) = datepart(data);
		drop k_d_e_dati_meteo_aree dt_insert;
		data_solare_merge=datepart(data);
		format data_solare_merge date9.;
		format superficie z2.0;
	run;

	proc sort data=meteo_prev out=meteo_prev_final;
		by data_solare_merge;
	run;

	/*imputo previsioni mancanti con i consuntivi meteo*/
	proc sql noprint;
		create table meteo_comp as select
			t1.*,t2.data as data_consuntivo,
			t2.superficie as sup_consuntivo,
			t2.colonna as col_cons,
			t2.riga as riga_cons,
			t2.t_max as t_max_consuntivo,
			t2.t_max_perc as t_max_perc_consuntivo, 
			t2.t_min as t_min_consuntivo,t2.neve as neve_consuntivo,
			t2.umidita as umidita_consuntivo,
			t2.precipitazioni as precipitazioni_consuntivo,
			t2.altezza_min as altezza_min_consuntivo
		from meteo_prev_final t1 right join initial_consuntivi t2
			on datepart(t1.data) = datepart(t2.data) and t1.superficie = t2.superficie
			and t1.riga = t2.riga and t1.colonna = t2.colonna;
	quit;

	data meteo_previsione;
		set meteo_comp;
		keep data_solare_merge data superficie colonna riga t_max t_min t_max_perc umidita altezza_min neve precipitazioni;

		if (data eq .) then
			do;
				superficie = sup_consuntivo;
				colonna = col_cons;
				riga = riga_cons;
				data_solare_merge=datepart(data_consuntivo);

				data=data_consuntivo;
				t_max = t_max_consuntivo;
				t_max_perc = t_max_perc_consuntivo;
				t_min = t_min_consuntivo;
				neve = neve_consuntivo;
				umidita = umidita_consuntivo;
				precipitazioni = precipitazioni_consuntivo;
				altezza_min = altezza_min_consuntivo;
			end;
	run;

	data meteo_prev_new;
		set meteo_prev_final(where=(data_solare_merge>=&G_solare));
	run;

	data meteo_previsione_tot;
		set meteo_previsione meteo_prev_new;
	run;

	proc sql noprint;
		CREATE TABLE D_E_METEO_previsioni_RAGG AS 
			SELECT t1.data, 
				t1.SUPERFICIE, 
				(MAX(t1.T_MAX)) AS t_max_max, 
				(MIN(t1.T_MAX)) AS t_max_min, 
				(MEAN(t1.T_MAX)) AS t_max_mean, 
				(STD(t1.T_MAX)) AS t_max_std, 
				(MAX(t1.T_MIN)) AS t_min_max, 
				(MIN(t1.T_MIN)) AS t_min_min, 
				(MEAN(t1.T_MIN)) AS t_min_mean, 
				(STD(t1.T_MIN)) AS t_min_std, 
				(MAX(t1.T_MAX_PERC)) AS t_max_perc_max, 
				(MIN(t1.T_MAX_PERC)) AS t_max_perc_min, 
				(STD(t1.T_MAX_PERC)) AS t_max_perc_std, 
				(MEAN(t1.T_MAX_PERC)) AS t_max_perc_mean, 
				(MEAN(t1.UMIDITA)) AS umidita_mean, 
				(MAX(t1.UMIDITA)) AS umidita_max, 
				(MIN(t1.UMIDITA)) AS umidita_min, 
				(STD(t1.UMIDITA)) AS umidita_std, 
				(MEAN(t1.ALTEZZA_MIN)) AS alt_min_mean, 
				(MAX(t1.ALTEZZA_MIN)) AS alt_min_max, 
				(MIN(t1.ALTEZZA_MIN)) AS alt_min_min, 
				(STD(t1.ALTEZZA_MIN)) AS alt_min_std, 
				(MEAN(t1.PRECIPITAZIONI)) AS prec_mean,
				(sum(t1.PRECIPITAZIONI)) AS prec_sum, 
				(MEAN(t1.NEVE)) AS neve_mean, 
				(SUM(t1.NEVE)) AS neve_sum
			FROM meteo_previsione_tot t1
				GROUP BY t1.data,
					t1.SUPERFICIE;
	QUIT;

	proc sort data=work.D_E_METEO_previsioni_RAGG out=work.D_E_METEO_previsioni_RAGG;
		by data superficie;
	run;

	proc transpose data=work.D_E_METEO_previsioni_RAGG out=work.prova;
		by data superficie;
	run;

	data work.d_e_meteo_previsioni_cat;
		set prova;
		id_var = cat("sup",put(superficie,z2.0),"_", trim(_NAME_));
	run;

	proc sort data=d_e_meteo_previsioni_cat out=meteo_previsioni_pretrasp;
		by data;
	run;

	proc transpose data=meteo_previsioni_pretrasp out=meteo_previsioni_final_t;
		by data;
		id id_var;
		var col1;
	run;
	
	data input_calendario1(where=(data_gas_rif<=intnx('days', &g_gas, 1) and data_gas_rif>=&celle_start));
	date=&celle_start;

	 do while (date<=intnx('days', &g1_solare, 1));
	do hour=0 to 23;
	datetime_solare=dhms(date,hour,0,0);

	 if hour<6 then
	data_gas_rif=intnx("day", date, -1);
	else data_gas_rif=date;
	output;
	end;

	 date=intnx("day", date, 1, 's');
	end;

	 format date date9.;
	format data_gas_rif date9.;
	format datetime_solare datetime19.;
	run;


	%if &meteop_flg = 1 %then
		%do;

			data meteo_previsioni_final1(drop=_name_);
				set meteo_previsioni_final_t;
				datetime_solare=intnx("hour",data,10);
				format datetime_solare datetime19.;
				drop data;
			run;

			data meteo_previsioni_final(where=(date>="01JAN2012"d and data_gas_rif <= &G1_solare));
				merge input_calendario1(in=p) meteo_previsioni_final1;
				by datetime_solare;

				if p;
			run;

			/* prendo le previsioni dove data solare merge � la data della previsione */
			data meteo_prev_d1;
				set initial_previsioni;

				if intnx('day',datepart(dt_insert),2) = datepart(data);
				drop  k_d_e_dati_meteo_aree dt_insert;
				data_solare_merge=datepart(data);
				format data_solare_merge date9.;
			run;

			proc sort data=meteo_prev_d1 out=meteo_prev_d1_final;
				by data_solare_merge;
			run;

			proc sql noprint;
				CREATE TABLE D_E_METEO_previsioni_RAGG_new AS 
					SELECT t1.data, 
						t1.SUPERFICIE, 
						(MAX(t1.T_MAX)) AS t_max_max, 
						(MIN(t1.T_MAX)) AS t_max_min, 
						(MEAN(t1.T_MAX)) AS t_max_mean, 
						(STD(t1.T_MAX)) AS t_max_std, 
						(MAX(t1.T_MIN)) AS t_min_max, 
						(MIN(t1.T_MIN)) AS t_min_min, 
						(MEAN(t1.T_MIN)) AS t_min_mean, 
						(STD(t1.T_MIN)) AS t_min_std, 
						(MAX(t1.T_MAX_PERC)) AS t_max_perc_max, 
						(MIN(t1.T_MAX_PERC)) AS t_max_perc_min, 
						(STD(t1.T_MAX_PERC)) AS t_max_perc_std, 
						(MEAN(t1.T_MAX_PERC)) AS t_max_perc_mean, 
						(MEAN(t1.UMIDITA)) AS umidita_mean, 
						(MAX(t1.UMIDITA)) AS umidita_max, 
						(MIN(t1.UMIDITA)) AS umidita_min, 
						(STD(t1.UMIDITA)) AS umidita_std, 
						(MEAN(t1.ALTEZZA_MIN)) AS alt_min_mean, 
						(MAX(t1.ALTEZZA_MIN)) AS alt_min_max, 
						(MIN(t1.ALTEZZA_MIN)) AS alt_min_min, 
						(STD(t1.ALTEZZA_MIN)) AS alt_min_std, 
						(MEAN(t1.PRECIPITAZIONI)) AS prec_mean,
						(sum(t1.PRECIPITAZIONI)) AS prec_sum, 
						(MEAN(t1.NEVE)) AS neve_mean, 
						(SUM(t1.NEVE)) AS neve_sum
					FROM meteo_prev_d1_final t1
						GROUP BY t1.data,
							t1.SUPERFICIE;
			QUIT;

			proc sort data=work.D_E_METEO_previsioni_RAGG_new out=work.D_E_METEO_previsioni_RAGG_new;
				by data superficie;
			run;

			proc transpose data=work.D_E_METEO_previsioni_RAGG_new out=work.prova_new;
				by data superficie;
			run;

			data work.d_e_meteo_previsioni_cat_new;
				set prova_new;
				id_var = cat("sup",put(superficie,z2.0),"_", trim(_NAME_));
			run;

			proc sort data=d_e_meteo_previsioni_cat_new out=meteo_previsioni_pretrasp_new;
				by data;
			run;

			proc transpose data=meteo_previsioni_pretrasp_new out=meteo_previsioni_final_t_new;
				by data;
				id id_var;
				var col1;
			run;

			data meteo_prev_d1_final_1(drop=data _name_);
				set meteo_previsioni_final_t_new;
				datetime_solare=intnx("hour",data,0);
				format datetime_solare datetime19.;
			run;

			data meteo_previsioni_final_new(where=(date>="01JAN2012"d and data_gas_rif <= &G1_solare));
				merge meteo_previsioni_final(in=p) meteo_prev_d1_final_1;
				by datetime_solare;

				if p;
			run;

		%end;
	%else
		%do;

			data meteo_previsioni_final1(drop=_name_);
				set meteo_previsioni_final_t;
				datetime_creazione=intnx("dthour", data,-48);
				datetime_solare=intnx("hour",datetime_creazione,10);
				format datetime_solare datetime_creazione datetime19.;
				drop data;
			run;

			data METEO_PREVISIONI_FINAL_NEW(where=(date>="01JAN2012"d));
				merge input_calendario1(in=p) meteo_previsioni_final1;
				by datetime_solare;

				if p;
		%end;

	/*ci sono dati ancora da imputare, dove valore mancante in t deve essere uguale a quello precedente pi� recente
	quindi valore in t-1, t-2, ecc*/
	%ffill_general(work.meteo_previsioni_final_new, work.meteo_previsioni_fill,
		compress(cat("G&meteop_flg._met_p_", name)),
		name like "sup%");

	/* prendo le previsioni con data solare merge la data della previsione */

	/*		data &&output_meteo_celle_prev&meteop_flg(Drop= datetime_creazione);*/
	/*			set meteo_previsioni_fill(where=(data_gas_rif>=&start_date));*/
	/*			drop date hour data_gas_rif;*/
	/*		run;*/

	data &&output_meteo_celle_prev&meteop_flg(Drop= datetime_creazione
		rename= datetime_solare_g = datetime_solare);
		set meteo_previsioni_fill(where=(data_gas_rif>=&celle_start));
		datetime_solare_G = intnx('DThour', datetime_solare, -24);
		format datetime_solare_G datetime19.;
		drop date hour data_gas_rif datetime_solare;
	run;

	proc datasets lib=work nolist;
		delete meteo:;
	run;

%mend create_meteoprev_param;


%macro create_meteocons_param(meteocons_flg);

/*%let meteocons_flg=1;*/

	/*prendo le previsioni G per G come se fossero dei consuntivi,
	perchè si avvicinano ai consuntivi*/
	data previsioni_g_out;
		set previsioni;
		if intnx('day',datepart(dt_insert),0) = datepart(data);
		drop k_d_e_dati_meteo_aree dt_insert;
/*		data_solare_merge=datepart(data);*/
/*		format data_solare_merge date9.;*/
	run;


/*fillo lo storico delle prev g con i consuntivi! Lo storico delle prev � pi� corto di quello dei consuntivi*/
	data meteo_consuntivo1;
		set consuntivi(where=(data_rif<&start_date_celle_p)) previsioni_g_out;
	run;

/*-------------------------------------------------------------------------------------------------------------------*/


/*	proc sort data=meteo_consuntivo1 out=meteo_consuntivo1_new(rename=(data=datetime_solare));*/
/*		by data_solare_merge;*/
/*	run;*/

	PROC SQL noprint;
		CREATE TABLE D_E_METEO_CONSUNTIVO_RAGG AS 
			SELECT t1.data, 
				t1.SUPERFICIE,
				(MAX(t1.T_MAX)) AS t_max_max, 
				(MIN(t1.T_MAX)) AS t_max_min, 
				(MEAN(t1.T_MAX)) AS t_max_mean, 
				(STD(t1.T_MAX)) AS t_max_std, 
				(MAX(t1.T_MIN)) AS t_min_max, 
				(MIN(t1.T_MIN)) AS t_min_min, 
				(MEAN(t1.T_MIN)) AS t_min_mean, 
				(STD(t1.T_MIN)) AS t_min_std, 
				(MAX(t1.T_MAX_PERC)) AS t_max_perc_max, 
				(MIN(t1.T_MAX_PERC)) AS t_max_perc_min, 
				(STD(t1.T_MAX_PERC)) AS t_max_perc_std, 
				(MEAN(t1.T_MAX_PERC)) AS t_max_perc_mean, 
				(MEAN(t1.UMIDITA)) AS umidita_mean, 
				(MAX(t1.UMIDITA)) AS umidita_max, 
				(MIN(t1.UMIDITA)) AS umidita_min, 
				(STD(t1.UMIDITA)) AS umidita_std, 
				(MEAN(t1.ALTEZZA_MIN)) AS alt_min_mean, 
				(MAX(t1.ALTEZZA_MIN)) AS alt_min_max, 
				(MIN(t1.ALTEZZA_MIN)) AS alt_min_min, 
				(STD(t1.ALTEZZA_MIN)) AS alt_min_std, 
				(MEAN(t1.PRECIPITAZIONI)) AS prec_mean,
				(sum(t1.PRECIPITAZIONI)) AS prec_sum, 
				(MEAN(t1.NEVE)) AS neve_mean, 
				(SUM(t1.NEVE)) AS neve_sum
			FROM meteo_consuntivo1 t1
				GROUP BY t1.data,
					t1.SUPERFICIE;
	QUIT;


	/* sort per data (che ï¿½ una data solare) e superficie (sono 21)*/
	proc sort data=work.D_E_METEO_CONSUNTIVO_RAGG out=work.D_E_METEO_CONSUNTIVO_RAGG;
		by data superficie;
	run;

	/*traspongo per trasformare i nomi delle colonne/variabili in una nuova variabile*/
	/* questo perchï¿½ voglio incollare la superficie(identificata da un numero) con il nome della variabile*/
	proc transpose data=work.d_e_meteo_consuntivo_ragg out=work.prova;
		by data superficie;
	run;

	data meteo_cons;
		set prova;
		datetime_solare=intnx("hour",data,10);
		format datetime_solare datetime18.;
	run;

	/*prendo le previsioni meteo con chiave il giorno della previsione*/
	%if &meteocons_flg = 1 %then
		%do;
			/*chiave ï¿½ il giorno della previsione, non il giorno in cui sono state fatte
					sono previsioni fatte in G-1 per G-1 e la sto usando in G*/
			data meteo_prev(where=(data_solare_merge<=&G_solare));
				set initial_previsioni;

				if intnx('day',datepart(dt_insert),1) = datepart(data);
				drop  dt_insert;
				data_solare_merge=datepart(data);
				format data_solare_merge date9.;
			run;

		%end;
	%else
		%do;
			/*chiave ï¿½ il giorno della previsione, non il giorno in cui sono state fatte
					sono previsioni fatte in G per G e la sto usando in G*/
			data meteo_prev;
				set initial_previsioni;
				/*------------2*/
				if intnx('day',datepart(dt_insert),0) = datepart(data);
				drop dt_insert;
				data_solare_merge=datepart(data);
				format data_solare_merge date9.;
			run;

		%end;

	proc sort data=meteo_prev out=meteo_prev_new(rename=(data=datetime_solare));
		by data_solare_merge;
	run;

	proc sql noprint;
		CREATE TABLE D_E_METEO_previsioni_RAGG AS 
			SELECT t1.data_solare_merge, 
				t1.SUPERFICIE,
				t1.datetime_solare, 
				(MAX(t1.T_MAX)) AS t_max_max, 
				(MIN(t1.T_MAX)) AS t_max_min, 
				(MEAN(t1.T_MAX)) AS t_max_mean, 
				(STD(t1.T_MAX)) AS t_max_std, 
				(MAX(t1.T_MIN)) AS t_min_max, 
				(MIN(t1.T_MIN)) AS t_min_min, 
				(MEAN(t1.T_MIN)) AS t_min_mean, 
				(STD(t1.T_MIN)) AS t_min_std, 
				(MAX(t1.T_MAX_PERC)) AS t_max_perc_max, 
				(MIN(t1.T_MAX_PERC)) AS t_max_perc_min, 
				(STD(t1.T_MAX_PERC)) AS t_max_perc_std, 
				(MEAN(t1.T_MAX_PERC)) AS t_max_perc_mean, 
				(MEAN(t1.UMIDITA)) AS umidita_mean, 
				(MAX(t1.UMIDITA)) AS umidita_max, 
				(MIN(t1.UMIDITA)) AS umidita_min, 
				(STD(t1.UMIDITA)) AS umidita_std, 
				(MEAN(t1.ALTEZZA_MIN)) AS alt_min_mean, 
				(MAX(t1.ALTEZZA_MIN)) AS alt_min_max, 
				(MIN(t1.ALTEZZA_MIN)) AS alt_min_min, 
				(STD(t1.ALTEZZA_MIN)) AS alt_min_std, 
				(MEAN(t1.PRECIPITAZIONI)) AS prec_mean,
				(sum(t1.PRECIPITAZIONI)) AS prec_sum, 
				(MEAN(t1.NEVE)) AS neve_mean, 
				(SUM(t1.NEVE)) AS neve_sum
			FROM meteo_prev_new t1
				GROUP BY t1.data_solare_merge, t1.datetime_solare,
					t1.SUPERFICIE;
	QUIT;

	proc sort data=work.D_E_METEO_previsioni_RAGG out=work.D_E_METEO_previsioni_RAGG;
		by datetime_solare superficie;
	run;

	proc transpose data=work.D_E_METEO_previsioni_RAGG(drop=data_solare_merge) out=work.prova1;
		by datetime_solare superficie;
	run;

	data meteo_prev;
		set prova1;
		format datetime_solare datetime18.;
	run;

	data work.d_e_meteo_consuntivo_cat;
		set meteo_prev meteo_cons(drop= data);
		id_var = cat("sup",put(superficie,z2.0),"_", trim(_NAME_));
	run;

	proc sort data=d_e_meteo_consuntivo_cat out=meteo_consuntivo_pretrasp;
		by datetime_solare;
	run;

	/* creo tante colonne quante sono le combinazioni superifice-nomi variabili originali (Es: sup1_temp, ecc)*/
	proc transpose data=meteo_consuntivo_pretrasp out=meteo_consuntivo_final_temp;
		by datetime_solare;
		id id_var;
		var col1;
	run;

	data meteo_consuntivo_final1;
		set meteo_consuntivo_final_temp;
	run;

	/*	%let end_date=today();*/
	data METEO_CONSUNTIVO_FINAL(where=(datepart(datetime_solare)>="01JAN2012"d and data_gas_rif < &G1_gas));
		merge &input_calendario(in=p) meteo_consuntivo_final1;
		by datetime_solare;

		if p;
	run;

	/*ci sono dati ancora da imputare, dove valore mancante in t deve essere uguale a quello precedente
	piï¿½ vicino a livello temporale (quindi t-1, t-2, ecc)*/
	%ffill_general(work.meteo_consuntivo_final, work.meteo_consuntivo_fill,
		compress(cat("G&meteocons_flg._met_c_", name)),
		name like "sup%");

	/*ottengo un dataset con la data solare a cui ï¿½ riferito quel consuntivo meteo...questo significa
	che in ottica di previsione in G di G questi dati sono di G-1 e ho segnato la data G-1!
	fra le 00 e le 9, ma uso la previsione fatta nel giorno prima!*/

	/* oggi sono in G: alle 10 mi arrivano i consuntivi riferiti a G-1. Dalle 0 alle 9 uso
	la previsione fatta in G-1 per G-1 */

	/* nel dataset output_meteo_celle_cons datetime_solare mi dice la data G-1 */
	%if &meteocons_flg = 1  %then
		%do;

			data &&output_meteo_celle_cons&meteocons_flg(rename=(date=data_solare));
				set meteo_consuntivo_fill(where=(date>&celle_start));
				label datetime_solare=datetime_solare;
				drop _NAME_ date data_solare hour data_gas_rif;

				/*	Model ="G&meteocons_flg";*/
			run;

		%end;
	%else
		%do;

			data &&output_meteo_celle_cons&meteocons_flg(rename=(date=data_solare));
				set meteo_consuntivo_fill(where=(date>&celle_start));
				label datetime_solare=datetime_solare;
				drop _NAME_ date data_solare hour data_gas_rif;

				/*	Model ="G&meteocons_flg";*/
			run;

			/*=======*/
			data meteo_prev_new;
				set meteo_prev;
				datetime_solare=intnx("dthour", datetime_solare,6);
			run;

			data work.d_e_meteo_consuntivo_cat_new;
				set meteo_prev_new meteo_cons(drop= data);
				id_var = cat("sup",put(superficie,z2.0),"_", trim(_NAME_));
			run;

			proc sort data=d_e_meteo_consuntivo_cat_new out=meteo_consuntivo_pretrasp_new;
				by datetime_solare;
			run;

			/* creo tante colonne quante sono le combinazioni superifice-nomi variabili originali (Es: sup1_temp, ecc)*/
			proc transpose data=meteo_consuntivo_pretrasp_new out=meteo_consuntivo_final_temp_new;
				by datetime_solare;
				id id_var;
				var col1;
			run;

			data meteo_consuntivo_final1_new;
				set meteo_consuntivo_final_temp_new;
			run;

			/*	%let end_date=today();*/
			data METEO_CONSUNTIVO_FINAL_new(where=(datepart(datetime_solare)>="01JAN2012"d and data_gas_rif < &G1_gas));
				merge &input_calendario(in=p) meteo_consuntivo_final1_new;
				by datetime_solare;

				if p;
			run;

			/*ci sono dati ancora da imputare, dove valore mancante in t deve essere uguale a quello precedente
				piï¿½ vicino a livello temporale (quindi t-1, t-2, ecc)*/
			%ffill_general(work.meteo_consuntivo_final_new, work.meteo_consuntivo_fill_new,
				compress(cat("G&meteocons_flg._met_cn_", name)),
				name like "sup%"); /*meteon in last release*/

			/*datetime_previsione=intnx("dthour",datetime_solare,+24);*/
			/*format datetime_previsione datetime19.;*/
			data &&output_meteo_celle_cons_new&meteocons_flg(rename=(date=data_solare));
				set meteo_consuntivo_fill_new(where=(date>&celle_start));
				label datetime_solare=datetime_solare;
				drop _NAME_ date data_solare hour data_gas_rif;

				/*	Model ="G&meteocons_flg";*/
			run;

		%end;

	proc datasets lib=work nolist;
		delete meteo:;
	run;

%mend create_meteocons_param;

%macro create_meteocelle(input_celle_cons, input_celle_prev, celle_start, celle_output_tot);
/*		%let input_celle_cons = &meteo_celle_cons;*/
/*		%let input_celle_prev = &meteo_celle_prev;*/
/*		%let celle_start = &start_date;*/

	data _null_;
		start_cell_adj=intnx("month",&celle_start,-1);
		call symput('start_cell_adj',start_cell_adj);
		start_date_celle_p=max(start_cell,"21MAY2016"d);
		call symputx('start_date_celle_p',start_date_celle_p);
	run;

	/**/
	/*	%let time = %sysfunc(putn(&start_cell_adj, date9.)):00:00:00;*/
	/*	%put &=time;*/
	/*CONSUNTIVI*/
	data initial_consuntivi;
		set &input_celle_cons;
		rename t_max_percepita = t_max_perc;
		format superficie z2.0;
	run;

	/*Processing for G variables*/
	data consuntivi( where=(data_rif>=&start_cell_adj));
		set initial_consuntivi(drop=k_d_e_meteo_consuntivo);
		cella=cat("s",put(superficie,z2.0),"_","r",put(riga,z2.0),"c",put(colonna,z2.0));
		data_rif=datepart(data);
		format data_rif date9.;
		t_med=(t_max+t_min)/2;
		t_gg=max(0,20-t_med);
	run;

	/*PREVISIONI*/
	data initial_previsioni;
		set &input_celle_prev;
		rename t_max_percepita = t_max_perc;
		format superficie z2.0;
	run;

	/*Processing for G variables*/
	data previsioni(where=(data_calc>=&start_date_celle_p));
		set initial_previsioni(drop=k_d_e_dati_meteo_aree);
		cella=cat("s",put(superficie,z2.0),"_","r",put(riga,z2.0),"c",put(colonna,z2.0));
		data_prev=datepart(data);
		data_calc=datepart(dt_insert);
		distanza_insert_rif=data_prev-data_calc;
		t_med=(t_max+t_min)/2;
		t_gg=max(0,20-t_med);
		format data_prev data_calc date9.;
	run;

	/*G*/
	%GxG_create_meteocelle(&celle_start,&grouping_all, &output_celle);

	/*G1*/
	%create_meteocons_param(1);
	%create_meteoprev_param(1);

	/*G2*/
	%create_meteocons_param(2);
	%create_meteoprev_param(2);

	/*G+G1+G2*/
	data &celle_output_tot;
		merge &output_celle
			&output_meteo_celle_prev1
			&output_meteo_celle_cons1
			&output_meteo_celle_prev2
			&output_meteo_celle_cons2
			&output_meteo_celle_cons_new2;
		by datetime_solare;
	run;

%mend create_meteocelle;

	

%macro create_target_rtot(input_bilanciosnam, input_dlp_orario, input_pcs_misura, input_riconsegnato1,
			input_riconsegnato2, output_scada, output_rtot, g_gas, start_date);

	data _null_;
		start_date_adj=intnx("month",&start_date,-37,'S');
		start_dt_char=put(dhms(start_date_adj,6,0,0),datetime19.);
		call symput('start_dt_char',start_dt_char);
		call symput('start_date_adj',start_date_adj);
	run;

	data calendario_giorno;
		data_gas=&start_date_adj;

		do while (data_gas<=&G_gas);
			output;
			data_gas=intnx("day", data_gas, 1, 's');
		end;

		format data_gas date9.;
	run;

	/******************************/
	/* GETTING DELTA LINE PACK*/

	/* 1. Getting DLP (delta_line_pack_srg) from the official trade balance (webdips.prev_bilancio) 
	when validated (validato=1) */
	data dlp_1 (keep=data_gas dlp_1);
		set &input_bilanciosnam(rename=(date=data_gas) 
			where=(validato=1 and Delta_Line_Pack_SRG ne . and data_gas>=&start_date_adj));
		dlp_1=Delta_Line_Pack_SRG*1000000;
	run;

	proc sort data=dlp_1;
		by data_gas;
	run;

	/* 2. Getting DLP from oralpbp2.analisi_dlp_orario. This measure needs to be converted through PCS
	values obtained from table webdips.pcs_misura_aop9100 */
	data dlp_orariowhere(where=(data_gas>=&start_date and data_gas<&G_gas));
		set &input_dlp_orario(where=(dt_calcolo>="&start_dt_char"dt));
		data_gas=ifn(hour(dt_calcolo)<6, intnx("day",datepart(dt_calcolo),-1), datepart(dt_calcolo));
		format data_gas date9.;
	run;

	proc sql;
		create table dlp_2_smc as 
			select data_gas, sum(delta_linepack) as delta_linepack_ab
				from dlp_orariowhere
					group by data_gas
						order by data_gas;
	quit;

	data pcs_misura (keep=data_gas pcs_kwh_sm3);
		set &input_pcs_misura (where=(pcs_kwh_sm3 ne .));
		data_gas=datepart(data);
		format data_gas date9.;
	run;

	proc sort data=pcs_misura nodupkey;
		by data_gas;
	run;

	data dlp_2(keep=data_gas dlp_2);
		merge dlp_2_smc(in=p) pcs_misura(in=q);
		by data_gas;
		dlp_2=delta_linepack_ab* pcs_kwh_sm3;

		if p and q;
	run;

	/* MERGE with day calendar (calendario_giorno). Fill missing values of dlp_1 with dlp_2 */
	data dlp_final(keep=data_gas deltalinepack_final );
		merge calendario_giorno(in=p where=(data_gas>=&start_date)) dlp_1 dlp_2;
		by data_gas;

		if (dlp_1 ne .) then
			deltalinepack_final=dlp_1;
		else if (dlp_2 ne .) then
			deltalinepack_final=dlp_2;
	run;

	/******************************/
	/* COMPUTING DELIVERED GAS */
	/* First the delivered gas balance sheet is computed from the obtained DLP */
	%let to_keep = date entrata: gnl: uscita: 
		Importazioni Produzione_Nazionale Sistemi_di_stoccaggio Stogit Edison_Stoccaggio
		Totale_Immesso Riconsegne_rete_Snam_Rete_Gas Industriale Termoelettrico Reti_di_distribuzione 
		Riconsegnato_ad_altre_Reti_tr;

	data riconsegnato;
		set &input_bilanciosnam(keep = &to_keep 
			where=(date ge &start_date));
		rename date=data_gas 
			Riconsegnato_ad_altre_Reti_tr=Ric_Reti_terzi 
			Riconsegne_rete_Snam_Rete_Gas=Riconsegne_SRG 
			Sistemi_di_stoccaggio=stoccaggio
			Reti_di_distribuzione=reti_distrib;
		uscite=sum(of uscita:);
		target_con_dlp=sum(Totale_Immesso,-uscite);
	run;

	data riconsegnato;
		merge riconsegnato(in=p) dlp_final(in=q);
		by data_gas;

		if p and q;
		target_con_dlp=target_con_dlp*1000000;
		tgt_riconsegnato_totale = target_con_dlp-deltalinepack_final;
	run;

	/* Computing SCADA values from 3 further data sources in order to fill delivered gas 
	missing values. */
	data riconsegnato_1 (rename=(tot_smc= tot_smc_1));
		set &input_riconsegnato1(keep=data_rif tot_smc);
		data_gas=datepart(data_rif);
	run;

	proc sort data=riconsegnato_1 nodupkey;
		by data_gas;
	run;

	data riconsegnato_2 (rename=(tot_smc= tot_smc_2));
		set &input_riconsegnato2(keep=data_rif tot_smc pcs_kwh);
		data_gas=datepart(data_rif);
	run;

	proc sort data=riconsegnato_2 nodupkey;
		by data_gas;
	run;

	proc sql;
		create table ric_calcolo_giornaliero as 
			select giorno_gas_scada as data_gas, sum(scada_bil_ric) as scada_ric
				from &output_scada(where=(giorno_gas_scada<&G_gas and giorno_gas_scada>=&start_date))
					group by giorno_gas_scada;
	quit;

	/* Taking SCADA data until current date */
	data riconsegnato_scada (where= (data_gas >= &start_date and data_gas < &G_gas) drop=pcs_kwh_tmp);
		retain pcs_kwh_tmp 10.85;
		merge riconsegnato_1 riconsegnato_2 ric_calcolo_giornaliero;
		by data_gas;
		format  data_gas date9.;

		if scada_ric ne . then
			riconsegnato_scada=scada_ric;
		else if tot_smc_2 ne . then
			riconsegnato_scada=tot_smc_2;
		else if tot_smc_1 ne . then
			riconsegnato_scada=tot_smc_1;
		
		if pcs_kwh= . then pcs_kwh=pcs_kwh_tmp;
		else pcs_kwh_tmp=pcs_kwh;
			
		riconsegnato_scada_kwh=riconsegnato_scada*pcs_kwh;
	run;

	data output_G(keep=deltalinepack_final data_gas tgt_riconsegnato_totale riconsegnato_scada_kwh);
		merge calendario_giorno(in=p) riconsegnato riconsegnato_scada;
		by data_gas;

		if p;

		if(tgt_riconsegnato_totale eq .) then
			tgt_riconsegnato_totale =riconsegnato_scada_kwh;
	run;

	/* forward filling DLP values */
	data output_G(drop=deltalinepack_final rename=(deltalinepack_final_new=deltalinepack_final));
		set output_G;
		data_gas_rif = data_gas;
		retain deltalinepack_final_new;

		if _n_=1 then
			deltalinepack_final_new=deltalinepack_final;
		else if deltalinepack_final ne . then
			deltalinepack_final_new=deltalinepack_final;
		format data_gas_rif date9.;
	run;

	/* creating lags from 1 to 4 for all variables */
	proc sort data=output_G;
		by descending data_gas;
	run;

	%DO i=1 %TO 4;

		proc sql;
			select cat("G",&i,"_",name,"= lag",&i,"(",name,");") into: tmp&i separated by " "
				from dictionary.columns
					where libname = "WORK" and memname = "OUTPUT_G" and lowcase(name) ne 'data_gas';
		quit;

	%END;

	proc sql;
		select cat(name, ' = ', cats('G_', name)) into :rename separated by ' ' from
			dictionary.columns where libname = 'WORK' and memname='OUTPUT_G' and lowcase(name) ne 'data_gas';
	quit;

	data output_rtot_pre(rename=(&rename));
		set output_G;

		%do i=1 %to 4;
			&&tmp&i;
			format G&i._data_gas_rif date9.;
		%end;
	run;

	proc sql;
		select cat(name) into :dates separated by ' ' from
			dictionary.columns where libname = 'WORK' and memname='OUTPUT_RTOT_PRE' and index(lowcase(name),'data_gas');
	quit;

	data &output_rtot;
		set output_rtot_pre(keep=&dates);
		set output_rtot_pre(drop=&dates);
	run;

	proc sort data=&output_rtot;
		by data_gas;
	run;

%mend create_target_rtot;

%macro create_tgt_rimanente(in_rtot, in_scada, ds_out);

	data add_tgt_rimanente1;
		set &in_scada (keep = scada_bil_ric_kwh_cumday datetime_prev data_gas_rif);
		rename datetime_prev=datetime_solare_g;
	run;

	data output_rtot;
		set &in_rtot;
	run;

	proc sql;
		select name into :keep_cols separated by ' ' from
			dictionary.columns where libname = 'WORK' and memname='OUTPUT_RTOT' and index(lowcase(name), 'tgt_riconsegnato_totale') > 0;
	quit;

	data &ds_out(rename=data_gas=data_gas_rif drop=scada_bil_ric_kwh_cumday);
		merge add_tgt_rimanente1(in=a rename=(data_gas_rif=data_gas)) &in_rtot (keep=&keep_cols data_gas);
		by data_gas;

		if a;
		G_tgt_riconsegnato_diff=G_tgt_riconsegnato_totale-scada_bil_ric_kwh_cumday;
	run;

%mend create_tgt_rimanente;

%macro create_lagsimile(output_rtot, input_calendario, output_lagsim);

	data tmp;
		set &output_rtot;
	run;

	proc sql;
		select name into :drop_cols separated by ' ' from
			dictionary.columns where libname = 'WORK' and memname='TMP' and index(lowcase(name), 'deltalinepack') > 0;
	quit;

	proc sort data=tmp(drop=&drop_cols) out=temp;
		by data_gas;
	run;

	data temp;
		set temp;
		time_year = year(data_gas);
		time_month = month(data_gas);
		time_weekday = weekday(data_gas);
		time_week = week(data_gas);
		time_festivo = 0;
		time_easter = 0;
		time_natale = 0;
		time_ferragosto = 0;
		time_weekend = 0;
		time_festivo_ext = 0;

		if time_month>=10 then
			time_anno_termico=year(data_gas);
		else time_anno_termico=year(data_gas)-1;
		do i=mdy(1,1,time_year), mdy(1,6,time_year), mdy(4,25,time_year), mdy(5,1,time_year),
			mdy(6,2,time_year), mdy(8,15,time_year), mdy(11,1,time_year), mdy(12,8,time_year),
			mdy(12,25,time_year), mdy(12,26,time_year), mdy(12,31,time_year);
			if data_gas = i then
				time_festivo = 1;
		end;

		do i=holiday('EASTER', time_year), intnx('day',holiday('EASTER',year(data_gas)),1);
			if data_gas = i then
				time_easter = 1;
		end;

		/* Christmas */
		/* from Sat before Dec 25th to Sun after Jan 6th */
		if mdy(12,23,time_year) < data_gas or mdy(1,7,time_year) > data_gas then
			do;
				time_natale=1;
			end;
		else if month(data_gas) = 12 and data_gas >= intnx("week", mdy(12, 25, year(data_gas)), -1, 'E') 
			and weekday(mdy(12, 25, year(data_gas))) < 7 then
			do;
				time_natale=1;
			end;

		if month(data_gas) = 1 and data_gas<=intnx("week", mdy(1, 6, year(data_gas)), 0, 'E') 
			and weekday(mdy(1, 6, year(data_gas))) > 1 then
			do;
				time_natale=1;
			end;

		/*ferragosto*/
		/* all the week of ferragosto */
		if data_gas >= intnx("day", mdy(8,15, year(data_gas)), -2) 
			and data_gas <= intnx("day", mdy(8,15, year(data_gas)), 2) then
			do;
				time_ferragosto=1;
			end;

		if time_festivo = 1 or time_ferragosto = 1 or time_natale = 1 then
			time_festivo_ext = 1;

		if time_weekday in (1, 7) then
			time_weekend = 1;
		time_week2 = week(data_gas, 'V');
	run;

	proc sort data=temp;
		by descending data_gas;
	run;

	data temp;
		set temp;
		time_ponte=0;
		time_prefestivo=0;

		if lag1(time_festivo)=1 and time_festivo=0 then
			time_prefestivo=1;

		if (time_prefestivo=1 or time_ferragosto=1 or time_natale=1) and (time_weekend=0) and (time_festivo=0) then
			time_ponte=1;
	run;

	proc sort data= temp;
		by data_gas;
	run;

	data temp;
		set temp;
		lag_rictot_1scada = lag1(G_riconsegnato_scada_kwh);
		lag_rictot_1 = lag1(G_tgt_riconsegnato_totale);
		lag_rictot_3 = lag3(G_tgt_riconsegnato_totale);
		lag_rictot_7 = lag7(G_tgt_riconsegnato_totale);
		lag_rictot_364 = lag364(G_tgt_riconsegnato_totale);
		lag_rictot_365 = lag365(G_tgt_riconsegnato_totale);
		lag_rictot_1scada_7 = mean(lag_rictot_1scada,lag_rictot_7);
		lag_rictot_1scada_364 = mean(lag_rictot_1scada,lag_rictot_364);
		lag_rictot_1scada_7_364 = mean(lag_rictot_1scada,lag_rictot_7,lag_rictot_364);
		lag_rictot_1scada_365 = mean(lag_rictot_1scada,lag_rictot_365);
		lag_rictot_1_7 = mean(lag_rictot_1,lag_rictot_7);
		lag_rictot_1_364 = mean(lag_rictot_1,lag_rictot_364);
		lag_rictot_1_7_364 = mean(lag_rictot_1,lag_rictot_7,lag_rictot_364);
		lag_rictot_1_365 = mean(lag_rictot_1,lag_rictot_365);

		/* Tue to Fri */
		if time_weekday in (3,4,5,6) then
			do;
				lag_rictot_best=lag_rictot_1;
				lag_rictot_best_scada=lag_rictot_1scada;
			end;

		/* Sun */
		if time_weekday in (1) then
			do;
				lag_rictot_best=lag_rictot_1_7;
				lag_rictot_best_scada=lag_rictot_1scada_7;
			end;

		/* Mon */
		if time_weekday in (2) then
			do;
				lag_rictot_best=lag_rictot_3;
				lag_rictot_best_scada=lag_rictot_3;
			end;

		/* Sat and Christmas*/
		if time_weekday in (7) or time_natale = 1 then
			do;
				lag_rictot_best=lag_rictot_1_7_364;
				lag_rictot_best_scada=lag_rictot_1scada_7_364;
			end;

		/* Ferragosto */
		if time_ferragosto=1 then
			do;
				lag_rictot_best=lag_rictot_1_364;
				lag_rictot_best_scada=lag_rictot_1scada_364;
			end;

		/* Generic holiday */
		if time_festivo=1 then
			do;
				lag_rictot_best=lag_rictot_1_365;
				lag_rictot_best_scada=lag_rictot_1scada_365;
			end;

		/* Easter */
		if time_easter=1 then
			do;
				lag_rictot_best=lag_rictot_1_364;
				lag_rictot_best_scada=lag_rictot_1scada_364;
			end;

		if lag_rictot_1 eq . then
			lag_rictot_best = .;
	run;

	data temp_nomiss_scada (drop=datetime_solare_2 lag_rictot_best rename=(datetime_solare_1=datetime_solare))
		temp_nomiss (drop=datetime_solare_1 lag_rictot_best_scada rename=(datetime_solare_2=datetime_solare));
		set temp;
		keep data_gas lag_rictot_best lag_rictot_best_scada datetime_solare_1 datetime_solare_2;
		datetime_solare_1 = dhms(intnx("day",data_gas,0),6,0,0);
		datetime_solare_2 = dhms(intnx("day",data_gas,0),13,0,0);
		format datetime_solare_1 datetime_solare_2 datetime19.;
	run;

	data temp_final;
		set temp_nomiss_scada(rename=(lag_rictot_best_scada=rictot_simile)) temp_nomiss(rename=(lag_rictot_best=rictot_simile));
	run;

	proc sort data=temp_final out=temp1(drop=data_gas);
		by datetime_solare;
	run;

	%ffill_general(WORK.TEMP1, WORK.OUTPUT_LAGSIM_PRE, catt('lag_', name), name like 'rictot_%');

	data &output_lagsim;
		set output_lagsim_pre(rename=(lag_rictot_simile=G_rtot_lag_simile));
	run;

	/* Merge with calendar and forward fill */
	data &output_lagsim;
		merge &input_calendario(in=c rename=(data_gas_rif=data_gas) drop=date) &output_lagsim;
		by datetime_solare;

		if c;
	run;

	data &output_lagsim;
		set &output_lagsim;
		retain ffill;

		if not missing(G_rtot_lag_simile) then
			ffill=G_rtot_lag_simile;
		else if missing(G_rtot_lag_simile) then
			G_rtot_lag_simile=ffill;
		drop ffill;
	run;

%mend create_lagsimile;

%macro create_lagsimile_G1(output_rtot, input_calendario, G1_output_lagsim);

	data tmp;
		set &output_rtot;
	run;

	proc sql;
		select name into :drop_cols separated by ' ' from
		dictionary.columns where libname = 'WORK' and memname='TMP' and index(lowcase(name), 'deltalinepack') > 0;
	quit;

	proc sort data=tmp(drop=&drop_cols) out=temp;
		by G1_data_gas_rif;
	run;

	data temp;
		set temp;
		time_year = year(G1_data_gas_rif);
		time_month = month(G1_data_gas_rif);
		time_weekday = weekday(G1_data_gas_rif);
		time_week = week(G1_data_gas_rif);

		time_festivo = 0;
		time_easter = 0;
		time_natale = 0;
		time_ferragosto = 0;
		time_weekend = 0;
		time_festivo_ext = 0;

		if time_month>=10 then
			time_anno_termico=year(G1_data_gas_rif);
		else time_anno_termico=year(G1_data_gas_rif)-1;

		do i=mdy(1,1,time_year), mdy(1,6,time_year), mdy(4,25,time_year), mdy(5,1,time_year),
			 mdy(6,2,time_year), mdy(8,15,time_year), mdy(11,1,time_year), mdy(12,8,time_year),
			mdy(12,25,time_year), mdy(12,26,time_year), mdy(12,31,time_year);

			if G1_data_gas_rif = i then
				time_festivo = 1;
		end;

		do i=holiday('EASTER', time_year), intnx('day',holiday('EASTER',year(G1_data_gas_rif)),1);

			if G1_data_gas_rif = i then
				time_easter = 1;
		end;

		/* Christmas */
		/* from Sat before Dec 25th to Sun after Jan 6th */
		if mdy(12,23,time_year) < G1_data_gas_rif or mdy(1,7,time_year) > G1_data_gas_rif then
			do;
				time_natale=1;
			end;

		else if month(G1_data_gas_rif) = 12 and G1_data_gas_rif >= intnx("week", mdy(12, 25, year(G1_data_gas_rif)), -1, 'E') 
			and weekday(mdy(12, 25, year(G1_data_gas_rif))) < 7 then
			do;
				time_natale=1;
			end;

		if month(G1_data_gas_rif) = 1 and G1_data_gas_rif<=intnx("week", mdy(1, 6, year(G1_data_gas_rif)), 0, 'E') 
			and weekday(mdy(1, 6, year(G1_data_gas_rif))) > 1 then
			do;
				time_natale=1;
			end;
			
		/*ferragosto*/
		/* all the week of ferragosto */
		if G1_data_gas_rif >= intnx("day", mdy(8,15, year(G1_data_gas_rif)), -2) 
			and G1_data_gas_rif <= intnx("day", mdy(8,15, year(G1_data_gas_rif)), 2) then
			do;
				time_ferragosto=1;
			end;

		if time_festivo = 1 or time_ferragosto = 1 or time_natale = 1 then
			time_festivo_ext = 1;

		if time_weekday in (1, 7) then
			time_weekend = 1;
		time_week2 = week(G1_data_gas_rif, 'V');
	run;

	proc sort data=temp;
		by descending G1_data_gas_rif;
	run;

	data temp;
		set temp;
		time_ponte=0;
		time_prefestivo=0;

		if lag1(time_festivo)=1 and time_festivo=0 then
			time_prefestivo=1;

		if (time_prefestivo=1 or time_ferragosto=1 or time_natale=1) and (time_weekend=0) and (time_festivo=0) then
			time_ponte=1;
	run;

	proc sort data= temp;
		by G1_data_gas_rif;
	run;

	data temp;
		set temp;

		lag_rictot_2scada = lag2(G1_riconsegnato_scada_kwh);
		lag_rictot_2 = lag2(G1_tgt_riconsegnato_totale);
		lag_rictot_3 = lag3(G1_tgt_riconsegnato_totale);
		lag_rictot_4 = lag4(G1_tgt_riconsegnato_totale);
		lag_rictot_7 = lag7(G1_tgt_riconsegnato_totale);
		lag_rictot_364 = lag364(G1_tgt_riconsegnato_totale);
		lag_rictot_365 = lag365(G1_tgt_riconsegnato_totale);

		lag_rictot_7_364 = mean(lag_rictot_7,lag_rictot_364);
		lag_rictot_2_7_364 = mean(lag_rictot_2,lag_rictot_7,lag_rictot_364);
		lag_rictot_2scada_7_364 = mean(lag_rictot_2scada,lag_rictot_7,lag_rictot_364);

		/* Tue to Fri */
		if time_weekday in (4,5,6) then
			do;
				lag_rictot_best=lag_rictot_2;
				lag_rictot_best_scada=lag_rictot_2scada;
			end;

		/* Sun */
		if time_weekday in (1) then
			do;
				lag_rictot_best=lag_rictot_7_364;
				lag_rictot_best_scada=lag_rictot_7_364;
			end;

		/* Mon */
		if time_weekday in (2) then
			do;
				lag_rictot_best=lag_rictot_3;
				lag_rictot_best_scada=lag_rictot_3;
			end;

		/* Tue */
		if time_weekday in (3) then
			do;
				lag_rictot_best=lag_rictot_4;
				lag_rictot_best_scada=lag_rictot_4;
			end;
		
		/* Sat */
		if time_weekday in (7) then
			do;
				lag_rictot_best=lag_rictot_7_364;
				lag_rictot_best_scada=lag_rictot_7_364;
			end;

		/* Christmas */
		if time_natale = 1 then
		do;
			lag_rictot_best=lag_rictot_2_7_364;
			lag_rictot_best_scada=lag_rictot_2scada_7_364;
		end;

		/* Ferragosto */
		if time_ferragosto=1 then
			do;
				lag_rictot_best=lag_rictot_365;
				lag_rictot_best_scada=lag_rictot_365;
			end;

		/* Generic holiday */
		if time_festivo=1 then
			do;
				lag_rictot_best=lag_rictot_365;
				lag_rictot_best_scada=lag_rictot_365;
			end;

		/* Easter */
		if time_easter=1 then
			do;
				lag_rictot_best=lag_rictot_364;
				lag_rictot_best_scada=lag_rictot_364;
			end;

	run;

	data temp_nomiss_scada (drop=datetime_solare_2 lag_rictot_best rename=(datetime_solare_1=datetime_solare))
		temp_nomiss (drop=datetime_solare_1 lag_rictot_best_scada rename=(datetime_solare_2=datetime_solare));

		set temp;
		keep G_data_gas_rif lag_rictot_best lag_rictot_best_scada datetime_solare_1 datetime_solare_2;

		datetime_solare_1 = dhms(intnx("day",G_data_gas_rif,0),6,0,0);
		datetime_solare_2 = dhms(intnx("day",G_data_gas_rif,0),13,0,0);

		format datetime_solare_1 datetime_solare_2 datetime19.;
	run;

	
	data temp_final;
		set temp_nomiss_scada(rename=(lag_rictot_best_scada=rictot_simile)) temp_nomiss(rename=(lag_rictot_best=rictot_simile));
	run;


	proc sort data=temp_final out=temp1(drop=G_data_gas_rif);
		by datetime_solare;
	run;

	%ffill_general(WORK.TEMP1, WORK.OUTPUT_LAGSIM_PRE, catt('lag_', name), name like 'rictot_%');

	data &G1_output_lagsim;
		set output_lagsim_pre(rename=(lag_rictot_simile=G1_rtot_lag_simile));
	run;

	/* Merge with calendar and forward fill */
	data &G1_output_lagsim;
		merge &input_calendario(in=c rename=(data_gas_rif=data_gas) drop=date) &G1_output_lagsim;
		by datetime_solare;

		if c;
	run;

	data &G1_output_lagsim;
		set &G1_output_lagsim;
		retain ffill;

		if not missing(G1_rtot_lag_simile) then
			ffill=G1_rtot_lag_simile;
		else if missing(G1_rtot_lag_simile) then
			G1_rtot_lag_simile=ffill;
		drop ffill;
	run;

%mend create_lagsimile_G1;

%macro create_rtot_lag_mvg(output_rtot, output_lagsim, start_date, output_rtot_lag_mvg);

	data target1 (drop=cross_section2
		rename=(cross_section1=cross_section))
		target2 (drop=cross_section1
		rename=(cross_section2=cross_section));
		set &output_rtot;
		keep data_gas G_tgt_riconsegnato_totale cross_section1 cross_section2;
		rename data_gas=data_gas_merge 
			G_tgt_riconsegnato_totale=rtot_lag;

		if G_tgt_riconsegnato_totale eq . then
			G_tgt_riconsegnato_totale=-1;
		cross_section1 = 'dummy1';
		cross_section2 = 'dummy2';
	run;

	data target_shift (where=(data_gas_merge ge &start_date));
		set target1 target2;
	run;

	/**************/
	/* Lag from 2 to 60 days */
	data x;
		do x=2 to 60;
			output;
		end;
	run;

	proc sql noprint;
		select x into:lg separated by ' ' from x;
	quit;

	proc panel data=target_shift;
		id cross_section data_gas_merge;
		lag rtot_lag(&lg.)/out=consuntivo_shift;
	run;

	data consuntivo_shift1 (drop=cross_section rtot_lag);
		set consuntivo_shift (where=(cross_section = "dummy1"));

		if cmiss(of _all_) then
			delete;
	run;

	proc sort data=consuntivo_shift1;
		by data_gas_merge;
	run;

	/**************/
	/* Lag from 360 to 366 days */
	data x;
		do x=360 to 366;
			output;
		end;
	run;

	proc sql noprint;
		select x into:lg separated by ' ' from x;
	quit;

	proc panel data=target_shift;
		id cross_section data_gas_merge;
		lag rtot_lag(&lg.)/out=consuntivo_shift_anno;
	run;

	data consuntivo_shift2 (drop=cross_section rtot_lag);
		set consuntivo_shift_anno(where=(cross_section = "dummy1"));

		if cmiss(of _all_) then
			delete;
	run;

	proc sort data=consuntivo_shift2;
		by data_gas_merge;
	run;

	/**************/

	/* Lag1 of tgt_riconsegnato_totale
	PLEASE NOTE:
	--> between 6.00 and 13.00
	- Use lag1 of riconsegnato_scada_kwh
	- if missing, use the similar day (only for G)
	--> after 13.00:
	- use lag1 of tgt_riconsegnato_totale
	- if missing, use lag1 of riconsegnato_scada_kwh
	- if still missing, use the similar day (only for G)

	/* ---> shifting back one day */
	data lagsim (drop=datetime_solare);
		set &output_lagsim (where=(hour(datetime_solare)=13));
		data_gas = intnx("day",datepart(datetime_solare),1);
		format data_gas date9.;
	run;

	proc sort data=lagsim;
		by data_gas;
	run;

	proc sort data=&output_rtot;
		by data_gas;
	run;

	data rfill (drop=riconsegnato_scada_kwh tgt_riconsegnato_totale);
		merge &output_rtot(drop=G_deltalinepack_final) lagsim;
		by data_gas;
		lag_scada_1=lag(G_riconsegnato_scada_kwh);
		lag_rtot_1=lag(G_tgt_riconsegnato_totale);
	run;

	data rfill_6(keep=datetime_solare6 rfill_6 G_rfill_6 G1_rfill_6
		rename=(datetime_solare6=datetime_solare rfill_6=rtot_lag_1 G_rfill_6=G_rtot_lag_1 G1_rfill_6=G1_rtot_lag_1)) 
		rfill_13(keep=datetime_solare13 rfill_13 G_rfill_13 G1_rfill_13
		rename=(datetime_solare13=datetime_solare rfill_13=rtot_lag_1 G_rfill_13=G_rtot_lag_1 G1_rfill_13=G1_rtot_lag_1));
		set rfill;

		/* At 6.00 we use the scada value if present, otherwise (only for G models) we use similar day */
		rfill_6=lag_scada_1;

		if lag_scada_1 ne . then
			G_rfill_6=rfill_6;
		else G_rfill_6=rtot_lag_simile;

		/* At 6.00 we use the balance sheet value if present, otherwise we use the same value used at 6.00 */
		if lag_rtot_1 ne . then
			do;
				rfill_13=lag_rtot_1;
				G_rfill_13=lag_rtot_1;
			end;
		else
			do;
				rfill_13=rfill_6;
				G_rfill_13=G_rfill_6;
			end;

		G1_rfill_6 = rfill_6;
		G1_rfill_13=rfill_6;
		datetime_solare6=dhms(data_gas,6,0,0);
		datetime_solare13=dhms(data_gas,13,0,0);
		format datetime_solare6 datetime_solare13 datetime19.;
	run;

	proc sort data=rfill_6;
		by datetime_solare;
	run;

	proc sort data=rfill_13;
		by datetime_solare;
	run;

	data rtot_lag_1(drop=rtot_lag_1);
		merge rfill_6 rfill_13;
		by datetime_solare;

		if cmiss(of _all_) then
			delete;
		data_gas_merge=datepart(datetime_solare);
		format data_gas_merge date9.;
	run;

	proc sort data=rtot_lag_1;
		by data_gas_merge;
	run;

	/*Computing moving averages by means of lags 1-30 computed before:
	moving average K uses K+1 values [G-1, G-2, ..., G-(k+1)]*/
	data final_lag;
		merge rtot_lag_1(rename=(G_rtot_lag_1=rtot_lag_1)) consuntivo_shift1 consuntivo_shift2;
		by data_gas_merge;
	run;

	data final_lag_1;
		merge rtot_lag_1 consuntivo_shift1 consuntivo_shift2;
		by data_gas_merge;
	run;

	/* HERE */
	proc sql noprint;
		select catt(name,"=",catt("G_", name))
			into :renamelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="final_lag_1"
						and name like "rtot_lag%";
	quit;

	data final_lag_1_models;
		set final_lag_1;
		rename &renamelist;
	run;

	data final_lag_1_models(drop=i G1_rtot_lag_1 G_rtot_lag_360 -- G_rtot_lag_363);
		set final_lag_1_models;
		array g_vars G_rtot_lag_2 - G_rtot_lag_59;
		G1_rtot_lag_2 = G1_rtot_lag_1;
		array g1_vars G1_rtot_lag_3 - G1_rtot_lag_60;
		G2_rtot_lag_3 = G1_rtot_lag_1;
		array g2_vars G2_rtot_lag_4 - G2_rtot_lag_61;
		G3_rtot_lag_4 = G1_rtot_lag_1;
		array g3_vars G3_rtot_lag_5 - G3_rtot_lag_62;
		G4_rtot_lag_5 = G1_rtot_lag_1;
		array g4_vars G4_rtot_lag_6 - G4_rtot_lag_63;

		do i=1 to 58;
			g1_vars(i) = g_vars(i);
			g2_vars(i) = g_vars(i);
			g3_vars(i) = g_vars(i);
			g4_vars(i) = g_vars(i);
		end;

		%do i=1 %to 4;
			%do j=364 %to 366;
				%let k=%eval(&j-&i);
				G&i._rtot_lag_&j = G_rtot_lag_&k;
			%end;
		%end;

		output;
	run;

	data out;
		merge &input_calendario (in=c rename=(data_gas_rif=data_gas) drop=date) final_lag_1_models(rename=(data_gas_merge=data_gas));
		by datetime_solare;

		if c;
	run;

	%ffill_general(WORK.OUT, WORK.OUT, catt(name, '_fill'), index(lowcase(name), 'rtot'));

	/* Back to the original names */
	proc sql noprint;
		select catt(name,"=",substr(name, 1, length(name)-5))
			into :renamelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="out"
						and index(lowcase(name), 'rtot')>0;
	quit;

	data out;
		set out;
		rename &renamelist;
	run;

	data &output_rtot_lag_mvg;
		set out;

		%do i=2 %to 30;
			G_rtot_mvg_&i=mean(of G_rtot_lag_1 -- G_rtot_lag_&i);
			G1_rtot_mvg_&i=mean(of G1_rtot_lag_2 -- G1_rtot_lag_%eval(&i+1));
			G2_rtot_mvg_&i=mean(of G2_rtot_lag_3 -- G2_rtot_lag_%eval(&i+2));
			G3_rtot_mvg_&i=mean(of G3_rtot_lag_4 -- G3_rtot_lag_%eval(&i+3));
			G4_rtot_mvg_&i=mean(of G4_rtot_lag_5 -- G4_rtot_lag_%eval(&i+4));
		%end;

		drop G2_rtot_lag_61 G3_rtot_lag_61 G3_rtot_lag_62 G4_rtot_lag_61 -- G4_rtot_lag_63;
	run;

%mend create_rtot_lag_mvg;

%macro create_target_termo(start_date, input_termo, input_sime2,
			input_sime, input_vol_val, output_rcomp_termo);

	data t_remi_terna (drop=remi);
		set &input_termo;
		codice_remi = input(remi,best32.);
	run;

	proc sql noprint;
		select codice_remi into:remi_terna
			separated by " "
		from t_remi_terna
		;
	quit;

	data _null_;
		if day(datepart(&dt_solare))<=20 then
			filter=intnx("month", datepart(&dt_solare),-2, "end");
		else filter=intnx("month", datepart(&dt_solare),-1, "end");
		call symput("remi_limit", filter);
		format filter date9.;
	run;

	data t_volumi_sime2(drop=k_d_e_misure_sime g_gas codice_punto);
		set &input_sime2(where=(k_d_e_misure_sime>0 ) 
			keep=k_d_e_misure_sime giorno_gas codice_punto pcs_25_15 volume_std);
		g_gas = datepart(giorno_gas);

		if g_gas ge &start_date and g_gas le &remi_limit;
		volume_kwh=volume_std*pcs_25_15;
		codice_remi=input(codice_punto,8.);
	run;

	/* saving min and max date of t_volumi_sime2 */
	proc means nolabels noprint data=t_volumi_sime2;
		var giorno_gas;
		output out=query_minmax_sime min=mindata max=maxdata;
	run;

	data _null_;
		set query_minmax_sime;
		call symput('min_data_SIME2', mindata);
		a=put (mindata, datetime.);
		call symput('max_data_SIME2', maxdata);
		b=put(maxdata, datetime.);
	run;

	proc sort data=t_volumi_sime2 (rename=(giorno_gas = data_riferimento) 
		where=(codice_remi in (&remi_terna)));
		by data_riferimento;
	run;

	data _null_;
		if &start_date>=&max_data_SIME2 then
			read_s1=0;
		else read_s1=1;
		call symputx('read_s1',read_s1);
	run;

	%if &read_s1 eq 1 %then
		%do;
			/* taking validate volumi_sime before may 2016 */
			data remi_sime_validati_parziali (drop=g_gas codice_punto
				where=(giorno_gas lt &min_data_sime2 and codice_remi ne . ));
				set &input_sime(keep=giorno_gas codice_punto pcs_25_15 volume_std);
				g_gas = datepart(giorno_gas);

				if g_gas ge &start_date;
				volume_kwh=volume_std*pcs_25_15;
				codice_remi=input(codice_punto,8.);
			run;

			/* filtering for the same delivery points */
			data remi_sime_validati_parziali (rename=(giorno_gas = data_riferimento));
				set remi_sime_validati_parziali(where=(codice_remi in (&remi_terna)));
			run;

			/* merging before and after 2016 */
			data t_remi_termo_target (drop=data_riferimento volume_kwh
				rename=(pcs_25_15=pcs_val volume_std=volume_smc));
				set remi_sime_validati_parziali t_volumi_sime2;
				data_gas=datepart(data_riferimento);
				format data_gas date9.;
				volume_kwh_val=volume_std*pcs_25_15;
			run;

		%end;
	%else
		%do;

			data t_remi_termo_target (drop=data_riferimento volume_kwh
				rename=(pcs_25_15=pcs_val volume_std=volume_smc));
				set t_volumi_sime2;
				data_gas=datepart(data_riferimento);
				format data_gas date9.;
				volume_kwh_val=volume_std*pcs_25_15;
			run;

		%end;

	proc sort data=t_remi_termo_target;
		by codice_remi data_gas;
	run;

	/* Computing average pcs (from 2013 to 2018) for codice_remi */
	proc sql;
		create table pcs_medi as 
			select distinct codice_remi
				,mean(pcs_val) as pcs_mean
			from t_remi_termo_target (where =(data_gas ge '01OCT2013'd and 
				data_gas lt '01OCT2018'd))
			group by codice_remi
		;
	quit;

	/**********/
	data _null_;
		if &start_date>="01MAY2013"d then
			read_vv=0;
		else read_vv=1;
		call symputx('read_vv',read_vv);
	run;

	%if &read_vv eq 1 %then
		%do;
			/* Rebuild history (take until 01MAY2013) */
			data volumi_validati (drop=dt_data_riferimento_dati id_codice_remi
				rename=(nr_volume=smc_vv ));
				set &input_vol_val (where = (input(id_codice_remi,8.) in (&remi_terna))
					keep = dt_data_riferimento_dati id_codice_remi nr_volume);
				data_gas = datepart(dt_data_riferimento_dati);

				if data_gas ge &start_date and data_gas lt '01MAY2013'd;
				format data_gas date9.;
				codice_remi=input(id_codice_remi,8.);
			run;

			proc sql;
				create table storico_remi as 
					select    a.data_gas
						, a.codice_remi
						, b.pcs_mean as pcs_val
						, a.smc_vv * b.pcs_mean as volume_kwh_val
					from volumi_validati as a
						left join pcs_medi as b
							on a.codice_remi = b.codice_remi
						order by a.data_gas
				;
			quit;

			data t_remi_termo_target_all (keep=data_gas codice_remi volume_kwh_val);
				set storico_remi t_remi_termo_target;

				if pcs_val ne .;
			run;

		%end;
	%else
		%do;

			data t_remi_termo_target_all (keep=data_gas codice_remi volume_kwh_val);
				set t_remi_termo_target;

				if pcs_val ne .;
			run;

		%end;

	proc sql;
		create table &output_rcomp_termo as 
			select data_gas
				, sum(volume_kwh_val) as riconsegnato_termo_kwh 
			from t_remi_termo_target_all 
				group by data_gas
					order by data_gas;
	quit;

%mend create_target_termo;

%macro create_lags_mvg(g, in_ds, out_ds);
	%do i=1 %to &n_max;

		data _null_;
			set stagionalita_lg (where=(N=&i));
			call symput("var",compress(var));
			call symput("lag",compress(lag-&g));
			call symput("mavg",compress(2));
		run;

		proc expand data=&in_ds  out=&in_ds method=none;
			id data_gas;
			convert &var = bil_lag_&var._&lag./ transformout=(lag &lag);
		run;

		/* Eseguo mooving average da 1 a 2 sulle variabili lag */
		%do j=1 %to &mavg;

			data _null_;
				lag_new = &lag*&j;
				m=&j+1;
				call symput('lag_new',compress(lag_new));
				call symput('m',compress(m));
			run;

			proc expand data=&in_ds out=&in_ds method=none;
				id data_gas;
				convert bil_lag_&var._&lag. = bil_lag&lag._&m._&var. / transformout=(lag &lag_new);
			run;

			proc contents data=&in_ds (drop=&var) out=cont (keep=name 
				where=(index(name,"&var") and substr(name,1,7) ne 'bil_mvg' and 
				(index(name, cat("bil_lag","&lag")) or name = cat("bil_lag_","&var","_","&lag") ) )) noprint;
			run;

			proc sql noprint;
				select name into: sel_var
					separated by " "
				from cont
				;
			quit;

			data &out_ds (drop=miss_dummy);
				set &in_ds;
				miss_dummy = nmiss(of &sel_var );

				if miss_dummy=0 then
					bil_mvg&m._&var._&lag. = mean(of &sel_var);
				else bil_mvg&m._&var._&lag.=.;
			run;

		%end;
	%end;
%mend create_lags_mvg;

%macro create_bil_lag_mvg(input_bilanciosnam, output_rtot, rcomp_termo_parziale, output_rall, 
			input_calendario,  output_bil_lag_mvg, start_date);
	%let to_keep_bil = date industriale reti_di_distribuzione riconsegnato_ad_altre_reti_tr 
		termoelettrico Stogit Edison_Stoccaggio
		consumi_perdite_gnc_srg altro esportazioni_reti_di_terzi esp_rdt_consumi  
		totale_immesso uscit: sistemi_di_stoccaggio importazioni produzione_nazionale
		entrata: gnl: Riconsegne_rete_Snam_Rete_Gas;

	data bil (/*drop= uscita: entrata: gnl_cavarzere gnl_livorno gnl_panigaglia*/
		rename=(date=data_gas
		Riconsegnato_ad_altre_Reti_tr=Ric_Reti_terzi
		Sistemi_di_stoccaggio=stoccaggio
		Reti_di_distribuzione=reti_distrib
		produzione_nazionale = produzione_naz entrate=G_entrate gnl=G_gnl
		consumi_perdite_gnc_srg=G_consumi_perdite_gnc_srg altro=G_altro 
		esportazioni_reti_di_terzi=G_esportazioni_reti_di_terzi
		ric_distrib_terzi = G_ric_distrib_terzi 
		esp_rdt_consumi=G_esp_rdt_consumi
		Stogit=G1_Stogit Edison_Stoccaggio=G1_Edison_Stoccaggio
		Riconsegne_rete_Snam_Rete_Gas=G1_Riconsegne_SRG));
		set &input_bilanciosnam(keep=&to_keep_bil);
		uscite=sum(of uscita:);
		entrate = sum(of entrata:);
		gnl = sum(of gnl:);
		ric_distrib_terzi = sum(reti_di_distribuzione,riconsegnato_ad_altre_reti_tr);
	run;

	/* creating another calendar with max_date = today + 4, it will be useful later for aligning models 
		G1 to G4 */
	data input_calendario4(where=(data_gas_rif<=intnx('days', &g_gas, 4) and data_gas_rif>=&start_date));
		date=&start_date;

		do while (date<=intnx('days', &g1_solare, 4));
			do hour=0 to 23;
				datetime_solare=dhms(date,hour,0,0);

				if hour<6 then
					data_gas_rif=intnx("day", date, -1);
				else data_gas_rif=date;
				output;
			end;

			date=intnx("day", date, 1, 's');
		end;

		format date date9.;
		format data_gas_rif date9.;
		format datetime_solare datetime19.;
	run;

	proc sql noprint;
		select catt(name,"=",catt("G1_", name))
			into :renamelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="bil"
						and (lowcase(name) like "entrata%" or lowcase(name) like "uscita%" or lowcase(name) like "gnl_%");
	quit;

	data bil;
		set bil;
		rename &renamelist;
	run;

	/* Handling balance anomalies */
	%if &flg_anomalie=1 %then
		%do;
			%put ===== Controllo Anomalie Dati Bilancio =====;

			%gestione_anomalie_bilancio(dt_base=&dt_solare, ds_in=bil, ds_out=bil);

/*				,storico_anomalie_bil=&output_anomalie_bilancio);*/
		%end;

	/* Keeping input_calendario as master */
	proc sort data=&input_calendario(keep=data_gas_rif) 
		out=calendar_days(rename=(data_gas_rif=data_gas)) nodupkey;
		by data_gas_rif;
	run;

	/* ADAPTING output_rtot */
	/*	data output_rtot_ad(rename=(G_Riconsegnato_Consuntivo_Kwh = Riconsegnato_Consuntivo_Kwh */
	/*								G_riconsegnato_scada_kwh = riconsegnato_scada_kwh*/
	/*								G_deltalinepack_final = deltalinepack_final));*/
	/*		set &output_rtot(keep=data_gas G_Riconsegnato_Consuntivo_Kwh G_riconsegnato_scada_kwh G_deltalinepack_final);*/
	/*	run;*/
	data ric_all (rename=(G_deltalinepack_final=G_dlp G1_deltalinepack_final=G1_dlp G2_deltalinepack_final=G2_dlp
		G3_deltalinepack_final=G3_dlp G4_deltalinepack_final=G4_dlp) );
		merge calendar_days (in=p) 
			bil 
			&output_rtot  
			&rcomp_termo_parziale;
		by data_gas;
		dlp_gwh = G_deltalinepack_final/1000000;

		if p;
	run;

	proc contents data=ric_all /*CHECK*/
		out=cont_ric_all(keep=name) noprint;
	run;
	
	/* Leaving missing tgt_termo for the last days, where we still don't have the value */
	proc sql noprint;
		select max(data_gas) format date9. into: max_data_gas_termo  
			from ric_all
				where riconsegnato_termo_kwh ne .;
	quit;
	

	%ffill_general(WORK.RIC_ALL, WORK.RIC_ALL_FILL, catt(name,'_'), name like '%');

	proc sql noprint;
		select compress(cat(name,"_","=",name)) into: renamelist
			separated by " "
		from cont_ric_all;
	quit;

	data ric_all_fill(rename=(&renamelist));
		set ric_all_fill;
	run;

	data ric_all_fill1 (drop= delta_termo G_consumi_perdite_gnc_srg G_altro G_esportazioni_reti_di_terzi 
		G_esp_rdt_consumi rcom_termo_gwh dlp_gwh
		rename=(altro_consumi_perdite_gnc=G_altro_c_p_gnc));
		retain data_gas;
		set ric_all_fill (rename=(riconsegnato_termo_kwh = G_rcomp_termo_kwh));

		/* Dal 01jan2012 al 01may2016:
		   a) consumi_perdite_gnc_srg e altro sono missing. 
		   ---> non riesco a recuperarli singolarmente e costruisco un'unica voce altro_consumi_perdite_gnc
		   b) esportazioni_reti_di_terzi missing.
		   ---> non riesco a recuperarla
		*/
		if data_gas lt '01may2016'd then
			do;
				altro_consumi_perdite_gnc = sum(G_esp_rdt_consumi, -uscite, -ric_reti_terzi, -dlp_gwh);
			end;
		else if (data_gas ge '01may2016'd and data_gas le '31may2016'd) or (data_gas ge '01jul2016'd and data_gas le '31oct2017'd) then
			do;
				G_altro = sum(G_esportazioni_reti_di_terzi, - uscite, - ric_reti_terzi);
				altro_consumi_perdite_gnc = sum(G_altro, G_consumi_perdite_gnc_srg);
			end;
		else if data_gas ge '01jun2016'd and data_gas le '30jun2016'd then
			do;
				G_altro = sum(G_esportazioni_reti_di_terzi, - uscite, - ric_reti_terzi);

				/*consumi_perdite_gnc_srg lo prendo dalla formula indiretta */
				G_consumi_perdite_gnc_srg =  sum(totale_immesso, -uscite, -dlp_gwh, -reti_distrib, -industriale, -termoelettrico, -ric_reti_terzi, -G_altro);
				altro_consumi_perdite_gnc = sum(G_altro,G_consumi_perdite_gnc_srg);
			end;

		/* dal 01nov2017 ho altro, consumi_perdite_gnc_srg (e anche esportazioni_reti_di_terzi) --> non c'è bisogno di ricostruire nulla,
				calcolo solo la somma di altro e consumi_perdite_gnc per completezza */
		else if data_gas ge '01nov2017'd then
			do;
				altro_consumi_perdite_gnc = sum(G_altro,G_consumi_perdite_gnc_srg);
			end;

		rcom_termo_gwh=G_rcomp_termo_kwh/1000000;
		delta_termo = termoelettrico - rcom_termo_gwh;
		G_altro_c_p_gnc_termo = altro_consumi_perdite_gnc+delta_termo;
		tgt_residuo_terna=G_altro_c_p_gnc_termo+ric_reti_terzi;
		format data_gas date9.;
	run;

	/* converting to kwh */
	proc sql;
		select distinct name into: to_convert_vars separated by " "
			from dictionary.columns
				where lowcase(libname)='work' and lowcase(memname)='ric_all_fill1'
					and index(name, 'riconsegnato')=0 and index(name, 'data_gas')=0
					and index(name, 'dlp')=0 and name ne 'G_rcomp_termo_kwh';
	quit;

	data ric_all_fill2 (drop=i to_kwh);
		set ric_all_fill1;
		array var[*] &to_convert_vars;
		to_kwh=1000000;

		do i=1 to dim(var);
			var[i] = var[i]*to_kwh;
		end;
	run;

	data out_pre;
		set ric_all_fill2 (keep=data_gas tgt_residuo_terna G_rcomp_termo_kwh 
			industriale reti_distrib);
		rename G_rcomp_termo_kwh=tgt_termoelettrico_terna industriale=tgt_industriale
			reti_distrib=tgt_civile;
		datetime_solare=dhms(data_gas,6,0,0);
		format datetime_solare datetime19.;
	run;

	/* building lags from 1 to 4 for the new variables */
	proc sort data=out_pre;
		by descending data_gas;
	run;

	%DO i=1 %TO 4;

		proc sql;
			select cat("G",&i,"_",name,"= lag",&i,"(",name,");") into: tmp&i separated by " "
				from dictionary.columns
					where lowcase(libname) = "work" and lowcase(memname) = "out_pre" and name not like "dat%";
		quit;

	%END;

	proc sql;
		select cat(name, ' = ', catt('G_', name)) into :rename separated by ' ' 
			from dictionary.columns 
				where lowcase(libname) = "work" and lowcase(memname) = "out_pre" and name not like "dat%";
	quit;

	data out_pre(rename=(&rename));
		set out_pre;

		%do i=1 %to 4;
			&&tmp&i;
		%end;
	run;

	proc sort data=out_pre;
		by data_gas;
	run;

	proc sql;
		select distinct name into: keep_vars separated by " "
			from dictionary.columns
				where lowcase(libname)='work' and lowcase(memname)='ric_all_fill2'
					and (index(name, 'tgt_riconsegnato') or index(name, 'data_gas'));
	quit;

	data out;
		merge ric_all_fill2(keep=&keep_vars in=a) out_pre;
		by data_gas;

		if a;
	run;

	/* Merging with calendar and forward filling */
	data out;
		merge &input_calendario (in=c rename=(data_gas_rif=data_gas) drop=date) out;
		by datetime_solare;

		if c;
	run;

	%ffill_general(WORK.OUT, WORK.OUT, catt(name,'_sfx'), name like 'G%');

	/* Back to the original names */
	proc sql noprint;
		select catt(name,"=",substr(name, 1, length(name)-4))
			into :renamelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="out" and name like 'G%';
	quit;

	data output_rall_pre;
		set out;
		rename &renamelist;
		format G_data_gas_rif_sfx G1_data_gas_rif_sfx
			G2_data_gas_rif_sfx G3_data_gas_rif_sfx G4_data_gas_rif_sfx date9.;
	run;

	/* Set to missing Terna values of last days in which actual values are unknown */
	data &output_rall;
		set output_rall_pre;

		%do i=0 %to 4;
			%if &i=0 %then
				%let pfx=G_;
			%else %let pfx=G&i._;

			if &pfx.data_gas_rif gt "&max_data_gas_termo"d then
				do;
					&pfx.tgt_termoelettrico_terna=.;
					&pfx.tgt_residuo_terna=.;
				end;
		%end;
	run;

	/* Not considering riconsegnato_consuntivo_kwh and riconsegnato_scada_kwh because we already computed
	lag and mvg before, as well as tgt_residuo_terna */
	data ric_all_fill2_new;
		set ric_all_fill2(drop=g2: g3: g4: tgt: 
			g1_dlp g_riconsegnato_scada_kwh 
			g1_riconsegnato_scada_kwh
			g_tgt_riconsegnato_totale
			g1_tgt_riconsegnato_totale
			g1_data_gas_rif
			g_data_gas_rif);
	run;

	data ric_all_fill3;
		set ric_all_fill2_new;
	run;

	data ric_all_fill3 (drop=G_dlp rename=(dlp_new=G_dlp));
		set	ric_all_fill3;
		retain dlp_new;

		if _n_=1 then
			dlp_new=G_dlp;
		else if G_dlp ne . then
			dlp_new=G_dlp;
	run;

	proc sort data=input_calendario4(keep=data_gas_rif) 
		out=calendar_days4(rename=(data_gas_rif=data_gas)) nodupkey;
		by data_gas_rif;
	run;

	data ric_all_fill3;
		merge calendar_days4(in=a) ric_all_fill3(in=b);
		by data_gas;
		if a;
	run;

	proc sql noprint;
		select catt(lowcase(name),"_=",name)
			into :renamelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="ric_all_fill3"
						and (name ne "data_gas");
	quit;

	%ffill_general(WORK.RIC_ALL_FILL3, WORK.RIC_ALL_FILL3, catt(name, '_'), name ne 'data_gas');

	/* Back to the original names */

	data ric_all_fill3;
		set ric_all_fill3;
		rename &renamelist;
	run;

	/*************************/

	/* Lag from 2 to 33
	NOTE:
	- excluding altro_c_p_gnc_termo and rcomp_termo_kwh because their value for the last month are
	not available */

	/* renaming altrocp for addressing variable name length problems */
	proc sql noprint;
		select cats(lowcase(name),'=', tranwrd(name, "altro_c_p", "altrocp"))
			into :altrenamelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="ric_all_fill3";
	quit;

	data ric_all_fill3;
		set ric_all_fill3;
		rename &altrenamelist;
	run;

	proc sql noprint;
		select cats(lowcase(name),'=','bil_lag_',lowcase(name))
			into :rename_list separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="ric_all_fill3" and 
						lowcase(name) not in ('data_gas' 'g_altrocp_gnc_termo' 'g_rcomp_termo_kwh');
	quit;

	proc sql noprint;
		select cats('bil_lag_',lowcase(name))
			into :drop_list separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="ric_all_fill3" and 
						lowcase(name) not in ('data_gas' 'g_altrocp_gnc_termo' 'g_rcomp_termo_kwh');
	quit;

	proc sql noprint;
		select cats("bil_lag_",lowcase(name),'(&lg.)')
			into :listalg separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="ric_all_fill3" and 
						lowcase(name) not in ('data_gas' 'g_altrocp_gnc_termo' 'g_rcomp_termo_kwh');
	quit;

	data bil_1 (drop=cross_section2 rename=(cross_section1=cross_section)) 
		bil_2 (drop=cross_section1 rename=(cross_section2=cross_section));
		set ric_all_fill3 (drop=g_altrocp_gnc_termo g_rcomp_termo_kwh);
		rename &rename_list;
		cross_section1 = 'dummy1';
		cross_section2 = 'dummy2';

		if importazioni ne .;
	run;

	data bil_shift;
		set bil_1 bil_2;
	run;

	data x;
		do x=2 to 33;
			output;
		end;
	run;

	proc sql noprint;
		select x into:lg separated by ' ' 
			from x;
	quit;

	proc panel data=bil_shift;
		id cross_section data_gas;
		lag &listalg/out=consuntivo_shift;
	run;

	data consuntivo_shift1 (drop=cross_section &drop_list);
		set consuntivo_shift (where=(data_gas>=&start_date));

		if cross_section = "dummy1";
	run;

	data consuntivo_shift1;
		merge input_calendario4(in=a) consuntivo_shift1(rename=(data_gas=data_gas_rif));
		by data_gas_rif;

		if a;
		data_gas = data_gas_rif;
		format data_gas date9.;
	run;

	data consuntivo_shift1;
		set consuntivo_shift1 (keep=date data_gas);
		set consuntivo_shift1 (drop=date data_gas);
	run;

	proc sql noprint;
		select name
			into :edit_list separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="consuntivo_shift1" and 
						scan(lowcase(name), -1, '_') eq "2" and index(name, '_g_')=0;
	quit;

	proc sql noprint;
		select catt('_', name)
			into :diff_list separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="consuntivo_shift1" and 
						scan(lowcase(name), -1, '_') eq "2" and index(name, '_g_')=0;
	quit;

	proc sql noprint;
		select catt(name, '=', '_', name)
			into :rn_list separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="consuntivo_shift1" and 
						scan(lowcase(name), -1, '_') eq "2" and index(name, '_g_')=0;
	quit;

	proc sql noprint;
		select catt('_', name, '=', name)
			into :rn_back separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="consuntivo_shift1" and 
						scan(lowcase(name), -1, '_') eq "2" and index(name, '_g_')=0;
	quit;

	data consuntivo_shift1_diff(keep=hour date_diff  &diff_list);
		set consuntivo_shift1;

		if 6 <= hour <= 12 then
			date_diff = date + 1;
		else date_diff = date;
		rename &rn_list;
		format date_diff date9.;
	run;

	proc sort data=consuntivo_shift1_diff;
		by date_diff hour;
	run;

	data consuntivo_shift1;
		merge consuntivo_shift1(in=a) consuntivo_shift1_diff(rename=(date_diff=date));
		by date hour;

		if a;
	run;

	/* processing G columns */
	data consuntivo_shift1_G;
		set consuntivo_shift1(drop= &diff_list);
	run;

	proc sql noprint;
		select cats(lowcase(name),'=','G_',tranwrd(name, "_g_", "_"))
			into :grenamelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="consuntivo_shift1_g" and 
						index(lowcase(name), "_g_");
	quit;

	proc sql noprint;
		select cats(lowcase(name),'=','G1_',tranwrd(name, "_g1_", "_"))
			into :g1renamelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="consuntivo_shift1_g" and 
						index(lowcase(name), "_g1_");
	quit;

	data consuntivo_shift1_G(drop=G1:);
		set consuntivo_shift1_G;
		rename &grenamelist &g1renamelist;
	run;

	proc sql noprint;
		select cats(lowcase(name),'=','G_', name)
			into :g_all separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="consuntivo_shift1_g" and lowcase(name) not like "g%"
						and lowcase(name) not like "dat%" and lowcase(name) not like "hour%";
	quit;

	data consuntivo_shift1_Gall(drop=date rename=(data_gas_rif=G_data_gas_rif));
		set consuntivo_shift1_G;
		rename &g_all;
	run;

	/* processing G1 columns */
	data consuntivo_shift1_G1(rename=(&rn_back));
		set consuntivo_shift1(drop= &edit_list);
	run;

	%do i=1 %to 4;

		proc sql noprint;
			select cats(lowcase(name),'=','G_',tranwrd(name, "_g_", "_"))
				into :grenamelist separated by ' '
					from dictionary.columns
						where lowcase(libname)="work" and lowcase(memname)="consuntivo_shift1_g1" and 
							index(lowcase(name), "_g_");
		quit;

		proc sql noprint;
			select cats(lowcase(name),'=','G&i._',tranwrd(name, "_g1_", "_"))
				into :g1renamelist separated by ' '
					from dictionary.columns
						where lowcase(libname)="work" and lowcase(memname)="consuntivo_shift1_g1" and 
							index(lowcase(name), "_g1_");
		quit;

		data tmp(drop=G_:);
			set consuntivo_shift1_G1;
			rename &grenamelist &g1renamelist;
		run;

		proc sql noprint;
			select cats(lowcase(name),'=','G&i._', name)
				into :g1_all separated by ' '
					from dictionary.columns
						where lowcase(libname)="work" and lowcase(memname)="tmp" and lowcase(name) not like "g&i%"
							and lowcase(name) not like "dat%" and lowcase(name) not like "hour%";
		quit;

		data tmp(rename=(data_gas_rif=G&i._data_gas_rif));
			set tmp;
			data_gas = data_gas - &i;
			rename &g1_all;
		run;

		proc sort data=tmp;
			by data_gas hour;
		run;

		proc sort data=consuntivo_shift1_Gall;
			by data_gas hour;
		run;

		data consuntivo_shift1_Gall;
			merge consuntivo_shift1_Gall(in=a) tmp(drop=datetime_solare);
			by data_gas hour;

			if a;
		run;

	%end;

	proc sql noprint;
		select name
			into :drop_g2 separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="consuntivo_shift1_gall" and lowcase(name) like "g2_%" and
						prxmatch("/_2$/",trim(name));
	quit;

	proc sql noprint;
		select name
			into :drop_g3 separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="consuntivo_shift1_gall" and lowcase(name) like "g3_%" and
						(prxmatch("/_2$/",trim(name)) or prxmatch("/_3$/",trim(name)));
	quit;

	proc sql noprint;
		select name
			into :drop_g4 separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="consuntivo_shift1_gall" and lowcase(name) like "g4_%" and
						(prxmatch("/_2$/",trim(name)) or prxmatch("/_3$/",trim(name)) or prxmatch("/_4$/",trim(name)));
	quit;

	data consuntivo_shift1_Gall;
		set consuntivo_shift1_Gall;
		drop &drop_g2 &drop_g3 &drop_g4;
	run;

	/**********************/

	/* Lag from 360 a 366
	NOTE:
	- We can use altro_c_p_gnc_termo and rcomp_termo_kwh this time */

	/* G1 model creates year lags for termoelettrico while G doesn't */
	proc sql;
		select cat(name, ' = ', cats('G1_', name)) into :rename_termo separated by ' ' 
			from dictionary.columns 
				where libname = "WORK" and lowcase(memname) = "ric_all_fill3" and lowcase(name)='termoelettrico';
	quit;

	data ric_all_fill3_edit;
		set ric_all_fill3;
		rename &rename_termo;
	run;

	proc sql noprint;
		select cats(lowcase(name),'=','bil_lag_',lowcase(name))
			into :rename_list separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="ric_all_fill3_edit" and 
						lowcase(name) not in ('data_gas' 'g_altrocp_gnc');
	quit;

	proc sql noprint;
		select cats('bil_lag_',lowcase(name))
			into :drop_list separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="ric_all_fill3_edit" and 
						lowcase(name) not in ('data_gas' 'g_altrocp_gnc');
	quit;

	proc sql noprint;
		select cats("bil_lag_",lowcase(name),'(&lg.)')
			into :listalg separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="ric_all_fill3_edit" and 
						lowcase(name) not in ('data_gas' 'g_altrocp_gnc');
	quit;

	data bil_1 (drop=cross_section2 rename=(cross_section1=cross_section)) 
		bil_2 (drop=cross_section1 rename=(cross_section2=cross_section));
		set ric_all_fill3_edit (drop=G_altrocp_gnc);
		rename &rename_list;
		cross_section1 = 'dummy1';
		cross_section2 = 'dummy2';

		if G_rcomp_termo_kwh= . then
			do;
				G_rcomp_termo_kwh=1;
				G_altrocp_gnc_termo=1;
			end;

		if importazioni ne .;
	run;

	data bil_shift;
		set bil_1 bil_2;
	run;

	data x;
		do x=360 to 366;
			output;
		end;
	run;

	proc sql noprint;
		select x into:lg separated by ' ' 
			from x;
	quit;

	proc panel data=bil_shift;
		id cross_section data_gas;
		lag &listalg/out=consuntivo_shift;
	run;

	data consuntivo_shift2 (drop=cross_section &drop_list);
		set consuntivo_shift (where=(data_gas>=&start_date));

		if cross_section = "dummy1";
		data_gas_rif = data_gas;
		format data_gas_rif date9.;
	run;

	proc sql noprint;
		select cats(lowcase(name),'=','G_',tranwrd(name, "_g_", "_"))
			into :grenamelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="consuntivo_shift2" and 
						index(lowcase(name), "_g_");
	quit;

	proc sql noprint;
		select cats(lowcase(name),'=','G1_',tranwrd(name, "_g1_", "_"))
			into :g1renamelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="consuntivo_shift2" and 
						index(lowcase(name), "_g1_");
	quit;

	/* processing G columns */
	proc sql noprint;
		select name
			into :keep separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="consuntivo_shift2" and 
						(index(lowcase(name), "_364") or index(lowcase(name), "_365") or index(lowcase(name), "_366") or name like "dat%");
	quit;

	data consuntivo_shift2_G(drop=G1:);
		set consuntivo_shift2 (keep=&keep);
		rename &grenamelist &g1renamelist;
	run;

	proc sql noprint;
		select cats(lowcase(name),'=','G_', name)
			into :g_all separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="consuntivo_shift2_g" and lowcase(name) not like "g%"
						and lowcase(name) not like "dat%" and lowcase(name) not like "hour%";
	quit;

	data consuntivo_shift2_Gall(drop=date rename=(data_gas_rif=G_data_gas_rif));
		set consuntivo_shift2_G;
		rename &g_all;
	run;

	/* processing G1 columns */
	%do i=1 %to 4;

		proc sql noprint;
			select name
				into :keep separated by ' '
					from dictionary.columns
						where lowcase(libname)="work" and lowcase(memname)="consuntivo_shift2" and 
							(index(lowcase(name), catt("_", 364)) or index(lowcase(name), catt("_", 365)) 
							or index(lowcase(name), catt("_", 366)) or name like "dat%");
		quit;

		proc sql noprint;
			select cats(lowcase(name),'=','G&i._',tranwrd(name, "_g1_", "_"))
				into :g1renamelist separated by ' '
					from dictionary.columns
						where lowcase(libname)="work" and lowcase(memname)="consuntivo_shift2" and 
							index(lowcase(name), "_g1_");
		quit;

		data tmp(drop=G_:);
			set consuntivo_shift2(keep=&keep);
			rename &grenamelist &g1renamelist;
		run;

		proc sql noprint;
			select cats(lowcase(name),'=','G&i._', name)
				into :g1_all separated by ' '
					from dictionary.columns
						where lowcase(libname)="work" and lowcase(memname)="tmp" and lowcase(name) not like "g&i%"
							and lowcase(name) not like "dat%";
		quit;

		data tmp(rename=(data_gas_rif=G&i._data_gas_rif));
			set tmp;
			data_gas = data_gas - &i;
			rename &g1_all;
		run;

		proc sort data=tmp;
			by data_gas;
		run;

		proc sort data=consuntivo_shift2_Gall;
			by data_gas;
		run;

		data consuntivo_shift2_Gall;
			merge consuntivo_shift2_Gall(in=a) tmp;
			by data_gas;

			if a;
		run;

	%end;

	data consuntivo_shift2_Gall;
		merge calendario_orario_final(in=a drop=date rename=(data_gas_rif=data_gas)) consuntivo_shift2_Gall;
		by data_gas;

		if a;
	run;

	/**********************/
	/* Merging consuntivo_shift1 and consuntivo_shif2, then adding datetime_solare_g at 6.00 */
	proc sort data=consuntivo_shift2_Gall;
		by data_gas hour;
	run;

	data final_bil_lag;
		merge consuntivo_shift1_Gall consuntivo_shift2_Gall;
		by data_gas hour;
	run;

	/**********************/

	/* Lag 1 
	NOTE:
	- Balance sheet data for G-1 are only available starting from 13.00 (solar time). Before then
	data correspond to the last available values (G-2)
	- rcomp_termo_kwh and altro_c_p_gnc_termo still unavailable, then we exclude them */
	proc sql noprint;
		select cats(lowcase(name),'=', 'bil_lag_g_', tranwrd(lowcase(name), "g_", ""), '_1')
			into :rename_list separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="ric_all_fill3" 
						and lowcase(name) not in ("data_gas" "g_rcomp_termo_kwh" "g_altrocp_gnc_termo");
	quit;

	data bil_lag1 (drop=data_gas where=(data_gas_g >= (&start_date-1)));
		set ric_all_fill3 (drop=G_rcomp_termo_kwh G_altrocp_gnc_termo);
		data_gas_g=intnx("day",data_gas,1);
		datetime_solare_g= dhms(data_gas_g,13,0,0);
		format data_gas_g date9. datetime_solare_g datetime18.;
		rename &rename_list;
	run;

	data bil_lag1(drop=data_gas_g bil_lag_g_g1:);
		merge calendario_orario_final(in=a drop=date rename=(data_gas_rif=data_gas)) bil_lag1(rename=(datetime_solare_g=datetime_solare));
		by datetime_solare;

		if a;
	run;

	%ffill_general(WORK.BIL_LAG1, WORK.BIL_LAG1,
		compress(substr(lowcase(name),index(name, '_')+1,length(lowcase(name)))), name like "bil_%");

	/* Back to the original names */
	proc sql noprint;
		select catt(lowcase(name),"=bil_",name)
			into :renamelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="bil_lag1"
						and (name like "lag_%");
	quit;

	data bil_lag1;
		set bil_lag1;
		rename &renamelist;
	run;

	proc sql noprint;
		select cats(lowcase(name),'=','G_',tranwrd(name, "_g_", "_"))
			into :grenamelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="bil_lag1" and 
						index(lowcase(name), "_g_");
	quit;

	data bil_lag1;
		set bil_lag1;
		rename &grenamelist;
	run;

	/* Merging tables lag1, lag2-33, lag360-366 */
	proc sort data=bil_lag1;
		by data_gas hour;
	run;

	data final_bil;
		merge final_bil_lag bil_lag1;
		by data_gas hour;
	run;

	proc sql noprint;
		drop table final_bil_lag, bil_lag1;
	quit;

	/**********************/

	/* The following lags are built starting from seasonality characteristics studied via spectrum analysis
	NOTE:
	- regarding altro_c_p_gnc_termo and rcomp_termo_kwh it's not possible to build lags <60
	 because such data are not available (using instead altro_c_p_gn and termoelettrico)
	- if some lags have already been built before we don't repeat the operation
	*/

	/* Mask for building significant lags spotted through spectra */
	data stagionalita_mvg;
		length var $40 lag 8;
		var="G_altrocp_gnc";
		lag=7;
		output;
		var="G_altrocp_gnc_termo";
		lag=122;
		output;
		var="G_altrocp_gnc_termo";
		lag=183;
		output;
		var="G_altrocp_gnc_termo";
		lag=365;
		output;
		var="termoelettrico";
		lag=4;
		output;
		var="termoelettrico";
		lag=7;
		output;
		var="termoelettrico";
		lag=365;
		output;
		var="G_dlp";
		lag=4;
		output;
		var="G_dlp";
		lag=5;
		output;
		var="G_dlp";
		lag=7;
		output;
		var="G_dlp";
		lag=8;
		output;
		var="G_entrate";
		lag=7;
		output;
		var="G_entrate";
		lag=183;
		output;
		var="G_entrate";
		lag=365;
		output;
		var="G_gnl";
		lag=69;
		output;
		var="G_gnl";
		lag=219;
		output;
		var="G_gnl";
		lag=274;
		output;
		var="G_gnl";
		lag=365;
		output;
		var="importazioni";
		lag=7;
		output;
		var="importazioni";
		lag=183;
		output;
		var="industriale";
		lag=7;
		output;
		var="industriale";
		lag=73;
		output;
		var="industriale";
		lag=122;
		output;
		var="industriale";
		lag=365;
		output;
		var="produzione_naz";
		lag=183;
		output;
		var="produzione_naz";
		lag=274;
		output;
		var="produzione_naz";
		lag=365;
		output;
		var="reti_distrib";
		lag=91;
		output;
		var="reti_distrib";
		lag=100;
		output;
		var="reti_distrib";
		lag=137;
		output;
		var="reti_distrib";
		lag=183;
		output;
		var="reti_distrib";
		lag=365;
		output;
		var="G_ric_distrib_terzi";
		lag=91;
		output;
		var="G_ric_distrib_terzi";
		lag=100;
		output;
		var="G_ric_distrib_terzi";
		lag=137;
		output;
		var="G_ric_distrib_terzi";
		lag=183;
		output;
		var="G_ric_distrib_terzi";
		lag=365;
		output;
		var="ric_reti_terzi";
		lag=4;
		output;
		var="ric_reti_terzi";
		lag=7;
		output;
		var="ric_reti_terzi";
		lag=183;
		output;
		var="ric_reti_terzi";
		lag=365;
		output;
		var="stoccaggio";
		lag=7;
		output;
		var="stoccaggio";
		lag=122;
		output;
		var="stoccaggio";
		lag=137;
		output;
		var="stoccaggio";
		lag=183;
		output;
		var="stoccaggio";
		lag=365;
		output;
		var="totale_immesso";
		lag=4;
		output;
		var="totale_immesso";
		lag=7;
		output;
		var="totale_immesso";
		lag=183;
		output;
		var="totale_immesso";
		lag=365;
		output;
		var="uscite";
		lag=78;
		output;
		var="uscite";
		lag=84;
		output;
		var="uscite";
		lag=100;
		output;
		var="uscite";
		lag=183;
		output;
		var="uscite";
		lag=365;
		output;
	run;

	data stagionalita_lg;
		set stagionalita_mvg;
		n=_n_;
	run;

	proc sql noprint;
		select count(*) into: n_max
			from stagionalita_lg;
	quit;

	data bil_1_lag;
		set ric_all_fill3;
	run;

	%create_lags_mvg(0, bil_1_lag, bil_1_lag);

	data output;
		set bil_1_lag (keep=data_gas bil_lag_: bil_mvg:);
		datetime_solare_g=dhms(data_gas,6,0,0);
		format datetime_solare_g datetime19.;
	run;

	data output(rename=(data_gas_rif=data_gas));
		merge &input_calendario(in=a rename=(datetime_solare=datetime_solare_g)) output(drop=data_gas);
		by datetime_solare_g;

		if a;
	run;

	%ffill_general(WORK.OUTPUT, WORK.OUTPUT,
		compress(substr(lowcase(name),index(name, '_')+1,length(lowcase(name)))), name like "bil_%");

	/* Back to the original names */
	proc sql noprint;
		select catt(lowcase(name),"=bil_",lowcase(name))
			into :renamelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="output"
						and (name like "lag_%" or name like "mvg%");
	quit;

	data output;
		set output;
		rename &renamelist;
	run;

	proc sql noprint;
		select cats(lowcase(name),'=','G_',tranwrd(name, "_g_", "_"))
			into :grenamelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="output" /*
						and index(lowcase(name), "_g_") */
		and name not in ("date" "hour" "datetime_solare_g"
		"data_gas");
	quit;

	data output;
		set output;
		rename &grenamelist;
	run;

	proc sql;
		drop table bil_1_lag;
	quit;

	proc sort data=final_bil;
		by datetime_solare;
	run;

	data out_bilancio;
		merge final_bil(in=b) output(in=a rename=(datetime_solare_g=datetime_solare));
		by datetime_solare;

		if b;
	run;

	proc contents data=out_bilancio out=cont(keep=name) noprint;
	run;

	proc sql noprint;
		select name into: to_drop_mvg3 separated by " "
			from cont 
				where index(name, "mvg3") and scan(name,-1,"_") in ("137" "183" "219" "274" "365" );
	quit;

	proc sql noprint;
		select name into: to_drop_mvg2 separated by " "
			from cont where index(name, "mvg2") and scan(name,-1,"_") in ("219" "274" "365" );
	quit;

	data &output_bil_lag_mvg (drop=n);
		set out_bilancio (drop=&to_drop_mvg3 &to_drop_mvg2);

		where data_gas<=&g_gas;
	run;

%mend create_bil_lag_mvg;

%macro gestione_anomalie_bilancio(dt_base=, ds_in=, ds_out=, storico_anomalie_bil=);
	/*	%let dt_base=&start;*/
	/*	%let ds_in=bil;*/
	/*	%let ds_out=bil_;*/
	/*	%let storico_anomalie_bil=&output_anomalie_bilancio;*/
	/* Taglio il ds di input per data_gas < &start e calcolo il confronto a partire da &start */
	/* Definisco data sulla quale tagliare il dataset */
	data stagioni;
		date_now=datepart(&dt_base);
		d1=nwkdom(5, 1, 3, year(date_now));
		d2=nwkdom(5, 1, 10, year(date_now));
		format d1 d2 date9.;
		length stagione $7;

		if (date_now>= d1  and date_now< d2) then
			stagione="estate";
		else if (date_now < d1 or date_now >= d2) then
			stagione="inverno";
		call symput("stagione",strip(stagione));
		format date_now date9.;
	run;

	%put Sono in &stagione;

	data _null_;
		dt_base=&dt_base;
		date_base=datepart(&dt_base);
		stagione="&stagione";

		if stagione="inverno" then
			dt_baseline=dhms(date_base,13,0,0);
		else dt_baseline=dhms(date_base,12,0,0);

		if dt_base<dt_baseline then
			do;
				cut_date=intnx("day",date_base,-2);
			end;
		else
			do;
				cut_date=intnx("day",date_base,-1);
			end;

		call symput("cut_date",cut_date);

		/*		format cut_date date9. date: date9. dt: datetime19.;*/
	run;

	proc sql noprint;
		select max(data_gas) into: max_date_bil
			from &ds_in (where = (data_gas le &cut_date) );
	run;

	data check_bilancio;
		/* Creo date BASELINE su cui effettuare il confronto */
		dt_now=&dt_base;
		date_now=datepart(&dt_base);
		stagione="&stagione";

		if stagione="inverno" then
			dt_baseline=dhms(datepart(dt_now),13,0,0);
		else dt_baseline=dhms(datepart(dt_now),12,0,0);

		/* Se sono prima delle 12 (13 in inverno) di oggi --> 
			ultima data_rif per il bilancio = altro ieri 
		*/
		if dt_now lt dt_baseline then
			do;
				date_baseline_bil=intnx("day",date_now,-2);
			end;

		/* Se sono dopo le 12 (13 in inverno) di oggi --> 
				ultima data_rif per il bilancio = ieri  */
		else
			do;
				date_baseline_bil=intnx("day",date_now,-1);
			end;

		/* Effettuo il confronto */
		max_date_bil=&max_date_bil;
		check_ritardo_bilancio=ifn(max_date_bil=date_baseline_bil,0,1);
		call symput("check_ritardo_bilancio",check_ritardo_bilancio);
		format max_date_bil date: date9. 
			dt: datetime19.;
	run;

	/* Se check_ritardo_bilancio=1 --> Devo fare ffil */
	data anomalie_bilancio;
		datetime_solare=dhms(datepart(&dt_base), hour(&dt_base),0,0);
		check_ritardo_bilancio=&check_ritardo_bilancio;
		format datetime_solare datetime19.;
	run;

	%if &check_ritardo_bilancio=1 %then
		%do;
			%put ===== Bilancio non disponibile =====;

			/* Taglio il ds di input per &cut_date */
			data &ds_out;
				set &ds_in
					(where = (data_gas le &cut_date));
			run;

			/* FFILL del giorno/i mancante/i con l'ultimo valore disponibile */
			data _null_;
				cycle = &cut_date - &max_date_bil;
				call symput("cycle",cycle);
			run;

			data to_add_bil (drop=i);
				set &ds_out end=eof;

				if eof;
				i=1;

				do until (i>&cycle);
					data_gas=intnx("day",&max_date_bil,i);
					output;
					i=i+1;
				end;

				format data_gas date9.;
			run;

			data &ds_out;
				set &ds_out to_add_bil;
			run;

		%end;
	%else
		%do;
			%put ===== Bilancio disponibile =====;
		%end;

	/* STORICIZZO EVENTUALI ANOMALIE */
/*	%put ===== Storicizzo Eventuali Anomalie Bilancio =====;*/
/**/
/*	%if %sysfunc(exist(&storico_anomalie_bil)) %then*/
/*		%do;*/
/**/
/*			proc append base=&storico_anomalie_bil data=anomalie_bilancio force;*/
/*			run;*/
/**/
/*		%end;*/
/*	%else*/
/*		%do;*/
/**/
/*			data &storico_anomalie_bil;*/
/*				set anomalie_bilancio;*/
/*			run;*/
/**/
/*		%end;*/

%mend gestione_anomalie_bilancio;

%macro create_terna(input_terna, input_up, input_remi_termo, 
			start_date_terna, output_terna_cons, output_terna_prev);

	/*%let start_date_terna=&start_date_short;*/

	data _null_;
		start_lag=intnx("month",&start_date_terna,-2,'S');
		start_date_terna_p=max(start_lag,"18MAR2016"d);
		dt_char=put(dhms(start_lag-2,0,0,0), datetime19.);
		call symput('start_dt_terna',dt_char);
		call symputx('start_date_terna_p',start_date_terna_p);
		call symputx('start_date_terna_c',start_lag);
	run;

	data calendario_orario(where=(giorno_gas<=&g_gas and giorno_gas>=&start_date_terna_c));
		date=&start_date_terna_c;

		do while (date<=&g1_solare);
			do hour=0 to 23;
				dt_calcolo=dhms(date,hour,0,0);

				if hour lt 6 then
					giorno_gas=date-1;
				else giorno_gas=date;
				output;
			end;

			date=intnx("day", date, 1, 's');
		end;

		format giorno_gas date date9.;
		format dt_calcolo datetime16.;
	run;

	/* Importing Terna predictions and balance sheet sources */

	data terna_prev(keep= hour_rif data_rif dt_rif dt_inserimento_val 
							energia_programmata k_d_e_up
					where=(data_rif>=&start_date_terna_p))

		terna_cons(keep= hour_rif data_rif dt_rif dt_inserimento_val energia_programmata k_d_e_up);

		set &input_terna(where=(DT_DATA_RIFERIMENTO>="&start_dt_terna"dt and K_C_E_SESSIONE in (0 5)));

		dt_inserimento_val=intnx("dthour",dt_data_elaborazione,1);
		dt_rif=intnx("dthour",DT_DATA_RIFERIMENTO,0);
		hour_rif = hour(dt_rif);
		data_rif = datepart(DT_DATA_RIFERIMENTO);

		format data_rif DATE9.;
		format dt_inserimento_val dt_rif datetime19.;

		if K_C_E_SESSIONE =5 then
			output terna_cons;
		else if K_C_E_SESSIONE =0 then
			output terna_prev;
	run;

	%if &flg_anomalie=1 %then
		%do;
			%put ===== Controllo Anomalie Dati Terna =====;

			%gestione_anomalie_terna(dt_base=&dt_solare,ds_in1=terna_prev, ds_in2=terna_cons, 
				ds_out1=terna_prev, ds_out2=terna_cons);
/*				,storico_anomalie_terna=&output_anomalie_terna);*/
		%end;


	/*-------- CONSIDERING BALANCE SHEET data: terna_cons ---------*/

	proc sql noprint;
		create table consuntivi_up_remi as
			select t1.*, t2.REMI from terna_cons t1 inner join
			(select distinct K_D_E_UP, REMI from &input_up where REMI in 
			(select REMI from &input_remi_termo)) t2
				on t1.K_D_E_UP = t2.K_D_E_UP
			order by remi, K_D_E_UP, dt_rif, dt_inserimento_val;
	quit;

	data consuntivi_up_remi;
		set consuntivi_up_remi;
		by remi K_D_E_UP dt_rif;

		if last.dt_rif then
			output;
	run;

	/* DATA ARE AVAILABLE AT 12 OR 13 OF DAY G, REFERRED TO EVERY HOUR OF DAY G-1 (SOLAR)*/

	/*REMI hourly balance*/
	/*problems with remi-> (remi="30314101" or remi="50034301") and consuntivo>1000000 in 2012*/
	proc sql noprint;
		create table consuntivi_remi as select
			dt_rif, dt_inserimento_val,data_rif, hour_rif, 
			REMI, sum(energia_programmata) as CONSUNTIVO
		from consuntivi_up_remi 
			group by dt_rif, dt_inserimento_val,data_rif, hour_rif, REMI;
	quit;

	data consuntivi_remi_mod (drop=i);
		set consuntivi_remi;

		if (remi eq "31307801" or remi eq "50034301" or remi eq "30314101") and data_rif<"01JAN2013"d then
			do;
				do i=1 to 1;
					if consuntivo >= 20**(i+2) and consuntivo <=10**(i+3) then
						consuntivo = consuntivo/(10**i);
				end;

				do i=2 to 11;
					if consuntivo >= 10**(i+2) and consuntivo <=10**(i+3) then
						consuntivo = consuntivo/(10**i);
				end;
			end;
	run;


	/* Aggregating balance sheet with hourly granularity */
	proc sql noprint;
		create table consuntivi_orario as 
			select data_rif, hour_rif, sum(CONSUNTIVO) as CONSUNTIVO
				from consuntivi_remi_mod 
					group by data_rif, hour_rif
						order by data_rif, hour_rif;
	quit;


	/* forward filling hour of consuntivo_orario, filling each missing with the value 
	of the same hour of day before */
	PROC SQL;
		select  max(t1.data_rif), min(t1.data_rif) into :max_d_terna, :min_d_terna
			FROM WORK.consuntivi_orario t1;
	QUIT;

	data cons_list_calendar;
		data_rif=&min_d_terna;

		do while (data_rif<=&max_d_terna);
			do hour_rif=0 to 23;
				output;
			end;

			data_rif=intnx('day', data_rif, 1);
		end;

		format data_rif date9.;
	run;

	data consuntivi_orario_nofill;
		merge consuntivi_orario cons_list_calendar;
		by data_rif hour_rif;
	run;

	proc sort data=consuntivi_orario_nofill out=consuntivi_orario_nofill;
		by hour_rif data_rif;
	run;

	%ffill_general(WORK.CONSUNTIVI_ORARIO_NOFILL, WORK.CONSUNTIVI_ORARIO_FILL, catt(name, "_tmp"), lowcase(name)='consuntivo');

	/* Back to the original names */
	proc sql noprint;
		select catt(name,"=",substr(name, 1, length(name)-4))
			into :renamelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="consuntivi_orario_fill" and index(lowcase(name), 'consuntivo')>0;
	quit;

	data consuntivi_orario_fill;
		set consuntivi_orario_fill;
		rename &renamelist;
	run;

	proc sort data=consuntivi_orario_fill out=consuntivi_orario_fill;
		by data_rif hour_rif;
	run;

	/* setting hour of insertion for balance sheet at 13 of the day after */
	data consuntivi_orario_fill(where=(datepart(dt_inserimento_val)>="01JAN2012"d));
		retain data_rif hour dt_inserimento_val data_inserimento_val;
		set consuntivi_orario_fill;
		dt_inserimento_val=dhms(intnx("day",data_rif,1),13,0,0);
		data_inserimento_val=datepart(dhms(intnx("day",data_rif,1),13,0,0));
		format dt_inserimento_val datetime.;
		format data_inserimento_val date9.;
	run;

	/* Now we have balance sheet data at hours before 13 as well, we don't actually 
	have it at that time, so we have to impute with the ones of day before */
	data consuntivi_orario_yday(keep=data_rif hour_rif CONSUNTIVO);
		set consuntivi_orario_fill;
		fake_data_rif=intnx("DAY",data_rif,1);
		format fake_data: date9.;
		drop data_rif;
		rename fake_data_rif=data_rif;
	run;

	data consuntivo_orario_imp(drop=dt_inserimento_val data_inserimento_val);
		merge consuntivi_orario_fill(in=uno rename=(consuntivo=consuntivo_post)) 
			consuntivi_orario_yday(in=due rename=(consuntivo=consuntivo_yday));
		by data_rif hour_rif;
		consuntivo_mix=ifn(hour_rif<13,consuntivo_yday,consuntivo_post);

		if consuntivo_mix eq . then
			consuntivo_mix=consuntivo_yday;

		if dt_inserimento_val eq . then
			dt_inserimento_val=dhms(data_rif,13,0,0);

		if hour_rif lt 6 then
			giorno_gas_rif=data_rif-1;
		else giorno_gas_rif=data_rif;
		dt_riferimento=dhms(data_rif,hour_rif,0,0);
		format giorno_gas_rif date9. dt_riferimento datetime19.;
	run;

	/* creating cumulative variables by following the same rules */
	data consuntivo_orario_cum;
		set consuntivo_orario_imp;
		by data_rif;
		retain cons_day_cum_pre cons_day_cum_post;

		if first.data_rif and consuntivo_mix ne . then
			do;
				cons_day_cum_pre=0;
				cons_day_cum_post=0;
			end;
		else if consuntivo_mix eq . or cons_day_cum_pre eq . then
			do;
				cons_day_cum_pre=.;
				cons_day_cum_post=.;
			end;

		cons_day_cum_pre+consuntivo_mix;
		cons_day_cum_post+consuntivo_post;

		if 0<=hour_rif<=12 or consuntivo_post eq . then
			cons_day_cum=cons_day_cum_pre;
		else cons_day_cum=cons_day_cum_post;
		drop consuntivo_post consuntivo_yday consuntivo_mix
			cons_day_cum_pre;
	run;

	data consuntivo_orario_gcum;
		set consuntivo_orario_imp;
		by giorno_gas_rif;
		retain cons_gday_cum_pre cons_gday_cum_post;

		if first.giorno_gas_rif and consuntivo_mix ne . then
			do;
				cons_gday_cum_pre=0;
				cons_gday_cum_post=0;
			end;
		else if consuntivo_mix eq . or cons_gday_cum_pre eq . then
			do;
				cons_gday_cum_pre=.;
				cons_gday_cum_post=.;
			end;

		cons_gday_cum_pre+consuntivo_mix;
		cons_gday_cum_post+consuntivo_post;

		if 0<=hour_rif<=12 or consuntivo_post eq . then
			cons_gday_cum=cons_gday_cum_pre;
		else cons_gday_cum=cons_gday_cum_post;
		drop consuntivo_post consuntivo_yday consuntivo_mix
			cons_gday_cum_pre;
	run;

	data CONS_ORARIA_TERNA_tmp(drop= giorno_gas_rif data_rif hour_rif);
		merge consuntivo_orario_imp(rename=consuntivo_mix=cons drop=consuntivo_yday)
				consuntivo_orario_cum
				consuntivo_orario_gcum;

		by dt_riferimento;
		dt_calcolo=intnx("dthour",dt_riferimento,+24);
		format dt: datetime19.;
	run;
	
	/* era drop = consuntivo_post + tutte le altre var*/
	data CONS_ORARIA_TERNA(drop= cons_gday_cum_post cons_day_cum_post rename=(date=giorno_solare ) );
		merge calendario_orario(in=p)
				CONS_ORARIA_TERNA_tmp;

		by dt_calcolo;

		if p;
		format dt: datetime19.;
	run;

	/* forward filling with the same logic as before */
	proc sort data=CONS_ORARIA_TERNA out=CONS_ORARIA_TERNA_SORTED;
		by hour giorno_solare;
	run;

	%ffill_general(WORK.CONS_ORARIA_TERNA_SORTED, WORK.INPUT_UP_CONSUNTIVO_ORA_TERMO, 
		catt("terna_", name), lowcase(name) like 'cons%');

	proc sort data=input_up_consuntivo_ora_termo;
		by dt_calcolo;
	run;

	/* duplicating columns to create values for G1 model */
	data input_up_consuntivo_ora_termo(drop=dt_riferimento);
		set input_up_consuntivo_ora_termo;

		G1_terna_cons_ora=terna_cons;
		G1_terna_cons_ora_cumday=terna_cons_day_cum;
		G1_terna_cons_ora_cumgday=terna_cons_gday_cum;

		rename terna_cons=G_terna_cons_ora
				terna_cons_day_cum=G_terna_cons_ora_cumday
				terna_cons_gday_cum=G_terna_cons_ora_cumgday;
	run;

	/********************************************************/
	/* FIRST OUTPUT PRODUCED: input_up_consuntivo_ora_termo */
	/********************************************************/

	/* Changing aggregation from hourly to daily to get daily balance sheet */
	proc sql noprint;
		create table consuntivo_giorno as 
			select data_rif, sum(CONSUNTIVO_POST) as CONSUNTIVO,
				sum(CONSUNTIVO_yday) as CONSUNTIVO_yday,
				dhms(intnx("day",data_rif,1),13,0,0) format=datetime19. as dt_inserimento_val
			from consuntivo_orario_imp 
				group by data_rif
					order by data_rif;
	quit;

	data consuntivo_giorno;
		set consuntivo_giorno;

		if _N_=1 then
			dt_inserimento_val=dhms(intnx("day",data_rif,2),6,0,0);
	run;

	/* need to replicate the value to go back to hourly reference */
	data consuntivo_giorn_final(where=(giorno_gas<=&g_gas));
		merge calendario_orario(in=p) 
			consuntivo_giorno(Rename=(dt_inserimento_val=dt_calcolo));
		by dt_calcolo;

		if p;
	run;

	%ffill_general(WORK.CONSUNTIVO_GIORN_FINAL, WORK.CONSUNTIVO_GIORN_FINAL_FILL, 
		catt("terna_", name), lowcase(name) like 'cons%');

	/* duplicating columns to create values for G1/G4 models */
	data input_up_consuntivo_termo(where=(giorno_gas>="03JAN2012"d) 
		drop=terna_CONSUNTIVO_yday data_rif
		rename=(date=giorno_solare));
		set CONSUNTIVO_GIORN_FINAL_fill;

			G_terna_cons_day = terna_consuntivo;
			G1_terna_cons_day = terna_consuntivo;
			G2_terna_cons_day = terna_consuntivo;
			G3_terna_cons_day = terna_consuntivo;
			G4_terna_cons_day = terna_consuntivo;
	run;

	/*****************************************************/
	/* SECOND OUTPUT PRODUCED: input_up_consuntivo_termo */
	/*****************************************************/

/*	%let input_remi_termo = oralpbp2.D_E_UP;*/

	proc sort data=TERNA_PREV out=TERNA_mgp_sort;
		by dt_rif dt_inserimento_val k_d_e_up;
	run;

	proc sql noprint;
		create table terna_remi_prev as
			select t1.*, t2.REMI from TERNA_mgp_sort t1 inner join
			(select distinct K_D_E_UP, REMI from &input_up where REMI in 
			(select REMI from &input_remi_termo)) t2
				on t1.K_D_E_UP = t2.K_D_E_UP
			order by dt_rif, remi, K_D_E_UP;
	quit;

	data terna_remi_mgp_sort;
		set terna_remi_prev;
		by dt_rif remi K_D_E_UP dt_inserimento_val;

		if last.dt_inserimento_val then
			output;
	run;

	data terna_remi_mgp_sort(drop=a);
		set terna_remi_mgp_sort;
		a=datepart(dt_rif)-datepart(dt_inserimento_val);

		if a ne 1 then
			dt_inserimento_val=dhms(intnx("day", datepart(dt_rif), -1),13,0,0);
	run;

	/* hourly predictions for each REMI */
	proc sql noprint;
		create table mgp_remi_orarie as 
			select dt_rif, dt_inserimento_val,data_rif, hour_rif, REMI, 
				sum(energia_programmata) as prev_mgp
			from terna_remi_mgp_sort 
				group by dt_rif, dt_inserimento_val,data_rif, hour_rif, REMI;
	quit;

	/* aggregated hourly predictions */
	proc sql noprint;
		create table mgp_orario as 
			select distinct data_rif, hour_rif, dt_inserimento_val, sum(prev_mgp) as prev_mgp
				from  mgp_remi_orarie  
					group by data_rif, hour_rif
						order by data_rif, hour_rif;
	quit;


	/* forward filling with the same logic as for balance sheet data */
	PROC SQL;
		select max(t1.data_rif), min(t1.data_rif) into :max_p_terna, :min_p_terna
			FROM WORK.mgp_orario t1;
	QUIT;

	data prev_list_calendar;
		data_rif=&min_p_terna;

		do while (data_rif<=&max_p_terna);
			do hour_rif=0 to 23;
				dt_inserimento_val_imp=dhms(intnx("day", data_rif, -1),13,0,0);
				output;
			end;

			data_rif=intnx('day', data_rif, 1);
		end;

		format data_rif date9.;
		format dt_inserimento_val_imp datetime19.;
	run;

	proc sort data=mgp_orario out=mgp_orario;
		by data_rif hour_rif;
	run;

	data terna_prev_base_nofill(drop=dt_inserimento_val_imp);
		merge mgp_orario prev_list_calendar;
		by  data_rif hour_rif;

		if dt_inserimento_val = . then
			dt_inserimento_val=dt_inserimento_val_imp;
	run;

	proc sort data=terna_prev_base_nofill out=terna_prev_base_nofill;
		by hour_rif data_rif;
	run;

	%ffill_general(WORK.TERNA_PREV_BASE_NOFILL, WORK.TERNA_PREV_BASE_FILL, 
		catt(name, "_tmp"), lowcase(name) like 'prev%');

	/* Back to the original names */
	proc sql noprint;
		select catt(name,"=",substr(name, 1, length(name)-4))
			into :renamelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="terna_prev_base_fill" and index(lowcase(name), 'prev')>0;
	quit;

	data terna_prev_base_fill;
		set terna_prev_base_fill;
		rename &renamelist;
	run;

	proc sort data=terna_prev_base_fill;
		by data_rif hour_rif;
	run;

	data mgp_orario(where=(datepart(dt_inserimento_val)>="01JAN2012"d));
		retain data_rif hour dt_inserimento_val data_inserimento_val;
		set terna_prev_base_fill;
		data_inserimento_val=datepart(dt_inserimento_val);
		format dt_inserimento_val datetime.;
		format data_inserimento_val date9.;

		if hour_rif lt 6 then
			giorno_gas_rif=data_rif-1;
		else giorno_gas_rif=data_rif;
		format giorno_gas_rif date9.;
	run;

	/* creating hourly prediction variables for G1 model. 
		After 13.00 it is possible to use predictions for the day ahead, until
		13.00 we need to fill with same values of day before */

	data mgp_orario_g1_lag;
		set mgp_orario;
		fake_data_rif = intnx("day", data_rif, 1);
		keep fake_data_rif hour_rif prev_mgp;
		format fake_data_rif date9.;
		rename prev_mgp=prev_mgp_lag;
	run;

	data mgp_orario_g_g1;
		merge mgp_orario(in=L rename=(prev_mgp=G_mgp_oraria)) 
				mgp_orario_g1_lag(in=R rename=(fake_data_rif=data_rif));

		by data_rif hour_rif;
		if L;

		prev_mgp_post = G_mgp_oraria;
		prev_mgp_pre = prev_mgp_lag;

		if hour_rif >= 13 then do;
			G1_mgp_oraria = G_mgp_oraria;
			prev_mgp_pre = G_mgp_oraria;
		end;

		else if hour_rif < 13 or missing(G_mgp_oraria) then
			G1_mgp_oraria = prev_mgp_lag;

		data_rif_G1 = data_rif - 1;
		format data_rif_G1 date9.;
	run;

	data mgp_orario_g_g1_all;
		merge mgp_orario_g_g1(in=a drop=data_rif_G1 G1: prev:) 
				mgp_orario_g_g1(in=b drop=data_rif giorno_gas_rif G_: rename=(data_rif_G1=data_rif));
		by data_rif hour_rif;

		if a; * data_rif begins now to assume role of current date;
		if missing(G1_mgp_oraria) then
			G1_mgp_oraria=G_mgp_oraria;

		if missing(prev_mgp_pre) then
			prev_mgp_pre=G_mgp_oraria;
		rename giorno_gas_rif=data_gas;
	run;

	/* creating cumulative variables for hourly mgp */
	data mgp_cumulato_gsolare;
		set mgp_orario_g_g1_all;
		by data_rif hour_rif;
		retain mgp_day_cum;

		if first.data_rif then
			do;
				G_mgp_day_cum=0;
				G1_mgp_day_cum_pre=0;
				G1_mgp_day_cum_post=0;
			end;

		G_mgp_day_cum + G_mgp_oraria;
		G1_mgp_day_cum_pre + prev_mgp_pre;
		G1_mgp_day_cum_post + prev_mgp_post;

		if hour_rif<13 or missing(prev_mgp_post) then
			G1_mgp_day_cum = G1_mgp_day_cum_pre;
		else G1_mgp_day_cum = G1_mgp_day_cum_post;
	run;

	data mgp_cumulato_ggas;
		set mgp_orario_g_g1_all;
		by data_gas;
		retain G_mgp_gday_cum G1_mgp_gday_cum;

		if first.data_gas then
			do;
				G_mgp_gday_cum=0;
				G1_mgp_gday_cum_pre=0;
				G1_mgp_gday_cum_post=0;
			end;

		G_mgp_gday_cum + G_mgp_oraria;
		G1_mgp_gday_cum_pre + prev_mgp_pre;
		G1_mgp_gday_cum_post + prev_mgp_post;

		if hour_rif<13 or missing(prev_mgp_post) then
			G1_mgp_gday_cum = G1_mgp_gday_cum_pre;
		else G1_mgp_gday_cum = G1_mgp_gday_cum_post;
	run;

	data mgp_orario_final;
		merge mgp_cumulato_gsolare(in=uno drop=G1_mgp_day_cum_pre
			G1_mgp_day_cum_post)
			mgp_cumulato_ggas (in=due drop=G1_mgp_gday_cum_pre
			G1_mgp_gday_cum_post);
		by data_rif hour_rif;

		if uno and due;
		rename prev_mgp_post = g1_prev_mgp_post;
	run;

	proc sort data=mgp_orario_final(drop=prev:);
		by data_rif hour_rif;
	run;

	/* imputation of hourly values with balance sheet values of the same days */
	data mgp_imputazione_orariocum(rename=consuntivo_mix=cons);
		merge consuntivo_orario_imp consuntivo_orario_gcum consuntivo_orario_cum;
		by data_rif hour_rif;
		keep data_rif hour_rif dt_inserimento_val_c cons_gday_cum	cons_day_cum consuntivo_mix;
		dt_inserimento_val_c=dhms(intnx("day",data_rif,-1),0,0,0);
		format dt_inserimento_val_c datetime19.;
	run;

	data mgp_orario_final_fill(drop= dt_inserimento_val_c);
		merge calendario_orario(in=k rename=(date=data_rif hour=hour_rif) drop= dt_calcolo giorno_gas) 
			mgp_orario_final(in=p) mgp_imputazione_orariocum(in=q);
		by data_rif hour_rif;

		mgp_oraria=cons;
		mgp_day_cum=cons_day_cum;
		mgp_gday_cum=cons_gday_cum;

		if G_mgp_oraria eq . then 
			dt_inserimento_val=dt_inserimento_val_c;

		data_inserimento_val=datepart(dt_inserimento_val);

		drop cons:;
		drop date dt_calcolo giorno_gas_rif;

		if k;
	run;

	/* Here we impute hourly values for G and G1 models differently, so we need to split
		our computation and follow different logics */
	proc sort data=mgp_orario_final_fill out=mgp_fill_2(drop=data_rif);
		by data_inserimento_val hour_rif;
	run;

	data mgp_fill_2;
		set mgp_fill_2;
		if G1_mgp_oraria eq . then
			do;
				G1_mgp_oraria=mgp_oraria;
				G1_mgp_day_cum=mgp_day_cum;
				G1_mgp_gday_cum=mgp_gday_cum;
			end;
		where data_inserimento_val < intnx('days', &g_solare, -1);
	run;

	proc sort data=mgp_orario_final_fill out=mgp_fill_1(drop=data_inserimento_val);
		by data_rif hour_rif;
	run;

	data mgp_fill_1;
		set mgp_fill_1;

		if G_mgp_oraria eq . then
			do;
				G_mgp_oraria=mgp_oraria;
				G_mgp_day_cum=mgp_day_cum;
				G_mgp_gday_cum=mgp_gday_cum;
			end;

		format G_dt_inserimento_val datetime19.;
	run;

	data mgp_orario_final_fill;
		merge mgp_orario_final_fill(drop=mgp:) mgp_fill_1(in=a drop=G1_: mgp: rename=(
																		G_mgp_oraria=mgp_oraria_g
																		G_mgp_day_cum=mgp_day_cum_g
																		G_mgp_gday_cum=mgp_gday_cum_g))
				mgp_fill_2(drop=G_: mgp: rename=(data_inserimento_val=data_rif
																		G1_mgp_oraria=mgp_oraria_g1
																		G1_mgp_day_cum=mgp_day_cum_g1
																		G1_mgp_gday_cum=mgp_gday_cum_g1));

		by data_rif hour_rif;
		if a;

		if G_mgp_oraria eq . then do;
			G_mgp_oraria = mgp_oraria_g;
			G_mgp_day_cum = mgp_day_cum_g;
			G_mgp_gday_cum = mgp_gday_cum_g;
		end;
		if G1_mgp_oraria eq . then do;
			G1_mgp_oraria = mgp_oraria_g1;
			G1_mgp_day_cum = mgp_day_cum_g1;
			G1_mgp_gday_cum = mgp_gday_cum_g1;
		end;
	run;
	/* end of the imputation */

	data mgp_ORARIA_final(where=(giorno_gas <=&g_gas));
		retain data_solare_G datetime_solare_g	data_gas_G data_rif	hour terna_mgp_giorn;

		merge mgp_orario_final_fill(drop=mgp: rename=(data_rif=date hour_rif=hour) in=uno)
				calendario_orario (in=due);

		by date hour;

		if due;
	run;

	proc sort data=MGP_ORARIA_FINAL out=MGP_ORARIA_FINAL;
		by hour giorno_gas;
	run;

	%ffill_general(WORK.MGP_ORARIA_FINAL, WORK.INPUT_UP_MGP_ORA_TERMO, 
		catt("terna_", name), index(lowcase(name), 'g_mgp'));

	%ffill_general(WORK.INPUT_UP_MGP_ORA_TERMO, WORK.INPUT_UP_MGP_ORA_TERMO, 
		catt("terna_", name), index(lowcase(name), 'g1_mgp'));

	proc sort data=input_up_mgp_ora_termo;
		by dt_calcolo;
	run;

	proc sql noprint;
		select cats(lowcase(name),'=','G_',tranwrd(lowcase(name), "_g_", "_"))
		into :grenamelist separated by ' '
		from dictionary.columns
		where lowcase(libname)="work" and lowcase(memname)="input_up_mgp_ora_termo" and 
				index(lowcase(name), "_g_");
	quit;

	proc sql noprint;
		select cats(lowcase(name),'=','G1_',tranwrd(lowcase(name), "_g1_", "_"))
		into :g1renamelist separated by ' '
		from dictionary.columns
		where lowcase(libname)="work" and lowcase(memname)="input_up_mgp_ora_termo" and 
				index(lowcase(name), "_g1_");
	quit;

	data input_up_mgp_ora_termo;
		set input_up_mgp_ora_termo;
		rename &grenamelist &g1renamelist;
	run;

	proc sql noprint;
		select cats(lowcase(name),'=', tranwrd(tranwrd(tranwrd(name,'_day_cum', '_ora_cumday'), 
			'_gday_cum', '_ora_cumgday'),
			'_oraria', '_ora'))
		into :rn_list separated by ' '
		from dictionary.columns
		where lowcase(libname)="work" and lowcase(memname)="input_up_mgp_ora_termo";
	quit;

	data input_up_mgp_ora_termo(drop= data_gas g_dt: g1_dt: dt_ins:);
		set input_up_mgp_ora_termo;
		rename &rn_list;
	run;

	/*************************************************/
	/* THIRD OUTPUT PRODUCED: input_up_mgp_ora_termo */
	/*************************************************/

	proc sql noprint;
		create table mgp_giornaliero as 
			select data_rif, dt_inserimento_val, sum(prev_mgp) as prev_mgp_giorn
			from terna_prev_base_fill 
			group by data_rif, dt_inserimento_val
			order by data_rif, dt_inserimento_val;
	quit;

	/* imputation of past with balance sheet data*/
	proc sql noprint;
		create table mgp_imputazione_giorn as 
			select intnx("day",giorno_solare,-1) format=date9. as data_rif, 
					terna_CONSUNTIVO as prev_mgp_giorn_imp
			from input_up_consuntivo_termo
			where hour=22 
			order by data_rif;
	quit;


	/* balance is referred to data_rif. Since we are building history we pretend to have prediction 
		of 13 of previous day starting from 00*/
	data mgp_imputazione_giorn(where=(Data_rif<&G_gas and datepart(dt_inserimento_val)>="01JAN2012"d));
		set mgp_imputazione_giorn;
		dt_inserimento_val=dhms(intnx("day",data_rif,-1),13,0,0);
		format dt_inserimento_val datetime19.;
	run;

	data previsioni_giorn2 (drop=prev_mgp_giorn_imp dt_inserimento_val_c);
		merge mgp_giornaliero mgp_imputazione_giorn(rename=(dt_inserimento_val=dt_inserimento_val_c));
		by data_rif;

		if prev_mgp_giorn eq . then do;
			prev_mgp_giorn=prev_mgp_giorn_imp;
			dt_inserimento_val=dt_inserimento_val_c;
		end;
	run;

	proc sql noprint;
		select max(data_rif)
			into :max_date_prevt
				from terna_prev;
	run;

	data previsioni_giorn_final( where=(giorno_gas<=&max_date_prevt)
		rename=(prev_mgp_giorn=mgp_giorn));
		merge calendario_orario(In=p) previsioni_giorn2(Rename=(data_rif=date));
		by date;

		if p;
	run;

	/* forward filling with nearest value with respect to the missing one*/
	%ffill_general(WORK.PREVISIONI_GIORN_FINAL, WORK.PREVT_GIORN_FINAL_FILL, 
		catt("terna_", name), lowcase(name) like 'mgp%');

	data prevt_giorn_final_fill;
		set prevt_giorn_final_fill;
		if not missing(dt_calcolo);
	run;

	/* predictions done at day G for G+1 */
	data input_up_mgp_termo;
		set prevt_GIORN_FINAL_fill(where=(giorno_gas>="03JAN2012"d) 
			rename=(date=giorno_solare));
		drop dt_inserimento_val;
	run;

	/**********************************************/
	/* FOURTH OUTPUT PRODUCED: input_up_mgp_termo */
	/**********************************************/

	/*----------------------------------------------------------------------*/
	/*--------------------------  BALANCE SHEET  ---------------------------*/
	/*----------------------------- daily lags -----------------------------*/
	/*--------------------------------- G ----------------------------------*/
	/*----------------------------------------------------------------------*/
	data cons_lag_base(drop=hour);
		set input_up_consuntivo_termo(keep=hour giorno_solare terna_consuntivo where=(hour=23));
		output;

		if giorno_solare = &g_gas  and giorno_solare < &g_solare then do;
			giorno_solare=giorno_solare+1;
			output;
		end;
	run;

	data cons1;
		set cons_lag_base;
		cross_section = 'dummy1';
	run;

	data cons2;
		set cons_lag_base;
		cross_section = 'dummy2';
	run;

	data cons_shift;
		set cons1 cons2;
	run;

	proc sort data=work.cons_shift nodupkey;
		by cross_section giorno_solare;
	run;

	DATA X;
		DO X=1 TO 34;
			OUTPUT;
		END;
	RUN;

	PROC SQL NOPRINT;
		SELECT X INTO:LG SEPARATED BY ' ' FROM X;
	QUIT;

	PROC PANEL DATA=cons_shift;
		ID cross_section giorno_solare;
		LAG terna_CONSUNTIVO(&LG.) /OUT=CONSUNTIVO_shift;
	run;

	data consuntivo_giorno_lag_temp;
		set CONSUNTIVO_shift;

		if cmiss(of _all_) then
			delete;

		if cross_section = "dummy1";
		drop cross_section terna_CONSUNTIVO;
	run;

	data cons_lag_giorn_final;
		merge calendario_orario(Rename=date=giorno_solare in=p) 
			consuntivo_giorno_lag_temp(in=q);
		by giorno_solare;

		if p and q;
	run;

	%ffill_general(WORK.CONS_LAG_GIORN_FINAL, WORK.INPUT_CONS_LAG, 
		compress(tranwrd(name,"CONSUNTIVO_","cons_day_lag")), lowcase(name) like 'terna%');

	proc sql noprint;
		select catt(name,"=","G_", name)
			into :renamelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="input_cons_lag"
						and name like "terna%";
	quit;

	data input_cons_lag;
		set input_cons_lag;
		rename &renamelist;
	run;

	/*----------------------------------------------------------------------*/
	/*--------------------------  BALANCE SHEET  ---------------------------*/
	/*----------------------------- daily lags -----------------------------*/
	/*--------------------------------- G1 ---------------------------------*/
	/*----------------------------------------------------------------------*/
	data input_cons_lag(drop=i);
		set input_cons_lag;
		array g1_vars G1_terna_cons_day_lag2 - G1_terna_cons_day_lag33;
		array g_vars G_terna_cons_day_lag3 - G_terna_cons_day_lag34;

		do i=1 to 32;
			g1_vars(i) = g_vars(i);
		end;

		G_data_gas_rif = giorno_gas;
		G1_data_gas_rif = giorno_gas + 1;
		format G_data_gas_rif G1_data_gas_rif date9.;
		output;
	run;

	data input_cons_lag(drop=i G_terna_cons_day_lag29 - G_terna_cons_day_lag34);
		set input_cons_lag;
		array g_vars G_terna_cons_day_lag2 - G_terna_cons_day_lag33;
		array g2_vars G2_terna_cons_day_lag2 - G2_terna_cons_day_lag33;
		array g3_vars G3_terna_cons_day_lag2 - G3_terna_cons_day_lag33;
		array g4_vars G4_terna_cons_day_lag2 - G4_terna_cons_day_lag33;

		do i=1 to 32;
			g2_vars(i) = g_vars(i);
			g3_vars(i) = g_vars(i);
			g4_vars(i) = g_vars(i);
		end;

		G2_data_gas_rif = giorno_gas + 2;
		G3_data_gas_rif = giorno_gas + 3;
		G4_data_gas_rif = giorno_gas + 4;
		format G2_data_gas_rif G3_data_gas_rif G4_data_gas_rif date9.;
		output;
	run;

	/*----------------------------------------------------------------------*/
	/*--------------------------  BALANCE SHEET  ---------------------------*/
	/*---------------------------- hourly lags -----------------------------*/
	/*--------------------------------- G ----------------------------------*/
	/*----------------------------------------------------------------------*/
	data consuntivo_orario_cum_lag;
		merge CONS_ORARIA_TERNA_tmp calendario_orario(IN=P rename=(date=giorno_solare));
		giorno_solare=datepart(dt_calcolo);
		by dt_calcolo;

		IF P;
		drop cons cons_day_cum cons_gday_cum;
	run;

	data consuntivo_orario_lag(rename=(CONSUNTIVO_post=G_terna_cons_ora_lag7d
										cons_day_cum_post=G_terna_cons_ora_cumday_lag7d
										cons_gday_cum_post=G_terna_cons_ora_cumgday_lag7d)
								drop=dt_riferimento dt_calcolo giorno_solare giorno_gas);
		set consuntivo_orario_cum_lag;

		dt_calcolo_7=intnx("dthour",dt_calcolo,24*7);
		giorno_solare_7=intnx("day",giorno_solare,7);
		giorno_gas_7=intnx("day",giorno_gas,7);
		format giorno: date9. dt_calcolo_7 datetime19.;
	run;

	data input_cons_orario_lag;
		merge consuntivo_orario_lag(rename=(dt_calcolo_7=dt_calcolo 
											giorno_solare_7= giorno_solare 
											giorno_gas_7=giorno_gas) in=q)
				calendario_orario(in=p keep=dt_calcolo);
		by dt_calcolo;
		if p and q;
	run;

	/*----------------------------------------------------------------------*/
	/*--------------------------  BALANCE SHEET  ---------------------------*/
	/*---------------------------- hourly lags -----------------------------*/
	/*--------------------------------- G1 ---------------------------------*/
	/*----------------------------------------------------------------------*/
	data consuntivo_orario_cum_lag_new;
		set consuntivo_orario_cum_lag;

		if hour < 13 then
			giorno_solare_g1 = giorno_solare + 1;
		else giorno_solare_g1 = giorno_solare;
		format giorno_solare_g1 date9.;
	run;

	proc sort data=consuntivo_orario_cum_lag_new out=consuntivo_orario_cum_lag_1(drop=giorno_solare_g1);
		by giorno_solare hour;
	run;

	proc sort data=consuntivo_orario_cum_lag_new out=consuntivo_orario_cum_lag_2(drop=giorno_solare rename=(giorno_solare_g1=giorno_solare consuntivo_post=G1_consuntivo_post));
		by giorno_solare_G1 hour;
	run;

	data consuntivo_orario_cum_lag_new(rename=(consuntivo_post=G_consuntivo_post));
		merge consuntivo_orario_cum_lag_1(in=a rename=(cons_day_cum_post = G_cons_day_cum_post 
														cons_gday_cum_post = G_cons_gday_cum_post)) 
			consuntivo_orario_cum_lag_2(rename=(cons_day_cum_post = G1_cons_day_cum_post 
												cons_gday_cum_post=cons_gday_old) 
										drop=giorno_gas);
		by giorno_solare hour;
		if a;
	run;

	data consuntivo_orario_cum_lag_new(drop=cons_gday_old cons_gday_new);
		set consuntivo_orario_cum_lag_new;
		by giorno_gas;

		if first.giorno_gas and hour=6 then
			cons_gday_new = 0;
		cons_gday_new + G1_consuntivo_post;

		if hour>=13 then
			G1_cons_gday_cum_post = cons_gday_old;
		else G1_cons_gday_cum_post = cons_gday_new;
	run;

	data consuntivo_orario_lag_g1(rename=(G1_CONSUNTIVO_post=G1_terna_cons_ora_lag7d
											G1_cons_day_cum_post=G1_terna_cons_ora_cumday_lag7d
											G1_cons_gday_cum_post=G1_terna_cons_ora_cumgday_lag7d
											consuntivo_post2=G1_terna_cons_ora_lag2d
											cons_day_cum_post2=G1_terna_cons_ora_cumday_lag2d
											cons_gday_cum_post2=G1_terna_cons_ora_cumgday_lag2d)

									drop=dt_riferimento dt_calcolo giorno_solare giorno_gas);
		set consuntivo_orario_cum_lag_new(drop=G_:);

		consuntivo_post2 = G1_consuntivo_post;
		cons_day_cum_post2 = G1_cons_day_cum_post;
		cons_gday_cum_post2 = G1_cons_gday_cum_post;

		dt_calcolo_8=intnx("dthour",dt_calcolo,24*7);
		giorno_solare_8=intnx("day",giorno_solare,7);
		giorno_gas_8=intnx("day",giorno_gas,7);

		dt_calcolo_3=intnx("dthour",dt_calcolo,24*2);
		giorno_solare_3=intnx("day",giorno_solare,2);
		giorno_gas_3=intnx("day",giorno_gas,2);
		format giorno: date9. dt_calcolo_3 datetime19.;
	run;

	proc sort data=consuntivo_orario_lag_g1;
		by dt_calcolo_3;
	run;

	data input_cons_orario_lag_g1;
		merge consuntivo_orario_lag_g1(rename=(dt_calcolo_8=dt_calcolo 
												giorno_solare_8=giorno_solare 
												giorno_gas_8=giorno_gas) 
										drop=dt_calcolo_3 giorno_solare_3 giorno_gas_3 
												G1_terna_cons_ora_lag2d G1_terna_cons_ora_cumday_lag2d
												G1_terna_cons_ora_cumgday_lag2d
										in=q)
			consuntivo_orario_lag_g1(rename=(dt_calcolo_3=dt_calcolo 
												giorno_solare_3=giorno_solare 
												giorno_gas_3=giorno_gas) 
										drop=dt_calcolo_8 giorno_solare_8 giorno_gas_8 
												G1_terna_cons_ora_lag7d G1_terna_cons_ora_cumday_lag7d
												G1_terna_cons_ora_cumgday_lag7d
										in=r)
			calendario_orario(in=p keep=dt_calcolo);
		by dt_calcolo;
		if p and q and r;
	run;

	proc sort data=input_cons_orario_lag_g1;
		by giorno_solare hour;
	run;

	%macro try;
	%do i=2 %to 4;

		data consuntivo_orario_lag_g&i(rename=(G1_CONSUNTIVO_post=G&i._terna_cons_ora_lag7d
												G1_cons_day_cum_post=G&i._terna_cons_ora_cumday_lag7d
												G1_cons_gday_cum_post=G&i._terna_cons_ora_cumgday_lag7d
												consuntivo_post2=G&i._terna_cons_ora_lag2d
												cons_day_cum_post2=G&i._terna_cons_ora_cumday_lag2d
												cons_gday_cum_post2=G&i._terna_cons_ora_cumgday_lag2d)

										drop=dt_riferimento dt_calcolo giorno_solare giorno_gas);
			set consuntivo_orario_cum_lag_new(drop=G_:);

			consuntivo_post2 = G1_consuntivo_post;
			cons_day_cum_post2 = G1_cons_day_cum_post;
			cons_gday_cum_post2 = G1_cons_gday_cum_post;

			dt_calcolo_7=intnx("dthour",dt_calcolo,24*(7+&i-1));
			giorno_solare_7=intnx("day",giorno_solare,(7+&i-1));
			giorno_gas_7=intnx("day",giorno_gas,(7+&i-1));

			dt_calcolo_2=intnx("dthour",dt_calcolo,24*(2+&i-1));
			giorno_solare_2=intnx("day",giorno_solare,(2+&i-1));
			giorno_gas_2=intnx("day",giorno_gas,(2+&i-1));
			format giorno: date9. dt_calcolo_7 datetime19.;
			format giorno: date9. dt_calcolo_2 datetime19.;
		run;

		proc sort data=consuntivo_orario_lag_G&i;
			by dt_calcolo_2;
		run;

		data input_cons_orario_lag_g&i;
			merge consuntivo_orario_lag_g&i(rename=(dt_calcolo_7=dt_calcolo 
													giorno_solare_7=giorno_solare 
													giorno_gas_7=giorno_gas) 
											drop=dt_calcolo_2 giorno_solare_2 giorno_gas_2
													G&i._terna_cons_ora_lag2d G&i._terna_cons_ora_cumday_lag2d
													G&i._terna_cons_ora_cumgday_lag2d
											in=q)
				consuntivo_orario_lag_g&i(rename=(dt_calcolo_2=dt_calcolo giorno_solare_2=giorno_solare 
													giorno_gas_2=giorno_gas) 
											drop=dt_calcolo_7 giorno_solare_7 giorno_gas_7 
													G&i._terna_cons_ora_lag7d G&i._terna_cons_ora_cumday_lag7d
													G&i._terna_cons_ora_cumgday_lag7d
											in=r)
				calendario_orario(in=p keep=dt_calcolo);
			by dt_calcolo;
			if p and q and r;
		run;

		proc sort data=input_cons_orario_lag_g&i;
			by giorno_solare hour;
		run;

	%end;

	data input_cons_orario_lag_new(where=(giorno_gas <= &g_gas));
		merge input_cons_orario_lag input_cons_orario_lag_g1 input_cons_orario_lag_g2
			input_cons_orario_lag_g3 input_cons_orario_lag_g4;
		by giorno_solare hour;
		dt_calcolo = dhms(giorno_solare,hour,0,0);
	run;

	proc sort data=input_cons_orario_lag_new;
		by dt_calcolo;
	run;
	%mend;
	%try;

	/*----------------------------------------------------------------------*/
	/*----------------------------  FORECASTS  -----------------------------*/
	/*----------------------------- daily lags -----------------------------*/
	/*--------------------------------- G ----------------------------------*/
	/*----------------------------------------------------------------------*/
	data prev_lag_base(drop=hour);
		set input_up_mgp_termo(keep=hour giorno_solare terna_mgp_giorn
								where=(hour=23));
		output;

		if giorno_solare = &g_gas  and giorno_solare < &g_solare then do;
			giorno_solare=giorno_solare+1;
			output;
		end;
	run;

	data prev1;
		set prev_lag_base;
		cross_section = 'dummy1';
	run;

	data prev2;
		set prev_lag_base;
		cross_section = 'dummy2';
	run;

	data mgp_shift;
		set prev1 prev2;
	run;

	proc sort data=work.mgp_shift nodupkey;
		by cross_section giorno_solare;
	run;

	DATA X;
		DO X=1 TO 33;
			OUTPUT;
		END;
	RUN;

	PROC SQL NOPRINT;
		SELECT X INTO:LG SEPARATED BY ' ' FROM X;
	QUIT;

	PROC PANEL DATA=mgp_shift;
		ID cross_section giorno_solare;
		LAG terna_mgp_giorn(&LG.) /OUT=PREVISIONE_shift;
	run;

	data previsione_giorno_lag;
		set PREVISIONE_shift;

		if cmiss(of _all_) then
			delete;

		if cross_section = "dummy1";
		drop cross_section terna_mgp_giorn;
		dt_inserimento_val=dhms(intnx("day",giorno_solare,-1),13,0,0);
		format dt_inserimento_val datetime.;
	run;

	data previsioni_lag_temp;
		merge work.calendario_orario(Rename=(date =giorno_solare) in=p) 
			previsione_giorno_lag(in=q);
		by giorno_solare;

		if p and q;
	run;

	%ffill_general(WORK.PREVISIONI_LAG_TEMP, WORK.INPUT_MGP_LAG, 
		compress(tranwrd(name,"mgp_giorn_","mgp_day_lag")), index(lowcase(name),'mgp_giorn'));

	proc sql noprint;
		select catt(name,"=",catt("G_", name))
			into :renamelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="input_mgp_lag"
						and name like "terna%";
	quit;

	data input_mgp_lag;
		set input_mgp_lag;
		rename &renamelist;
	run;

	/*----------------------------------------------------------------------*/
	/*----------------------------  FORECASTS  -----------------------------*/
	/*----------------------------- daily lags -----------------------------*/
	/*-------------------------------- G1 ----------------------------------*/
	/*----------------------------------------------------------------------*/
	data input_mgp_lag_new(drop=i G_terna_mgp_day_lag29 - G_terna_mgp_day_lag34);
		set input_mgp_lag;
		array g_vars G_terna_mgp_day_lag2 - G_terna_mgp_day_lag33;
		array g1_vars G1_terna_mgp_day_lag2 - G1_terna_mgp_day_lag33;
		array g2_vars G2_terna_mgp_day_lag2 - G2_terna_mgp_day_lag33;
		array g3_vars G3_terna_mgp_day_lag2 - G3_terna_mgp_day_lag33;
		array g4_vars G4_terna_mgp_day_lag2 - G4_terna_mgp_day_lag33;

		do i=1 to 32;
			g1_vars(i) = g_vars(i);
			g2_vars(i) = g_vars(i);
			g3_vars(i) = g_vars(i);
			g4_vars(i) = g_vars(i);
		end;

		G_data_gas_rif = giorno_gas;
		G1_data_gas_rif = giorno_gas + 1;
		G2_data_gas_rif = giorno_gas + 2;
		G3_data_gas_rif = giorno_gas + 3;
		G4_data_gas_rif = giorno_gas + 4;
		format G_data_gas_rif G1_data_gas_rif 
				G2_data_gas_rif G3_data_gas_rif 
				G4_data_gas_rif date9.;
		output;
	run;

	data input_up_mgp_termo_split;
		set input_up_mgp_termo;

		if hour > 13 then
			giorno_solare_g1 = giorno_solare - 1;
		else giorno_solare_g1 = giorno_solare;

		format giorno_solare_g1 date9.;

	run;

	proc sort data=input_up_mgp_termo_split out=input_up_mgp_termo_1(drop=giorno_solare_g1);
		by giorno_solare hour;
	run;

	proc sort data=input_up_mgp_termo_split out=input_up_mgp_termo_2(drop=giorno_solare rename=(giorno_solare_g1=giorno_solare terna_mgp_giorn=G1_terna_mgp_day));
		by descending giorno_solare_G1 descending hour;
	run;

	data input_up_mgp_termo_2_lag;
		set input_up_mgp_termo_2;

		G1_terna_mgp_day_lag = lag24(G1_terna_mgp_day);
	run;

	data input_up_mgp_termo_2_lag(drop=G1_terna_mgp_day_lag);
		set input_up_mgp_termo_2_lag;
		if nwkdom(5, 1, 3, year(giorno_solare)) <=giorno_solare <=nwkdom(5, 1, 10, year(giorno_solare)) and hour=13
			then G1_terna_mgp_day = G1_terna_mgp_day_lag;
	run;

	proc sort data=input_up_mgp_termo_2_lag;
		by giorno_solare hour;
	run;

	data input_up_mgp_termo_split(rename=(terna_mgp_giorn=G_terna_mgp_day));
		merge input_up_mgp_termo_1(in=a) input_up_mgp_termo_2_lag;
		by giorno_solare hour;
		if a;
	run;

	data input_up_mgp_termo;
		set input_up_mgp_termo_split;
		dt_calcolo = dhms(giorno_solare, hour, 0, 0);
		format dt_calcolo datetime.;
	run;

	proc sort data=input_up_mgp_termo;
		by dt_calcolo;
	run;

	/* backward fill for variable g1_terna_mgp_day */
	proc sort data=input_up_mgp_termo out=input_up_mgp_termo_fill;
		by descending dt_calcolo;
	run;

	%ffill_general(WORK.INPUT_UP_MGP_TERMO_FILL, WORK.INPUT_UP_MGP_TERMO_FILL, catt(name, "_"), lowcase(name)='g1_terna_mgp_day');

	proc sort data=input_up_mgp_termo_fill out=input_up_mgp_termo;
		by dt_calcolo;
	run;

	/* Back to the original names */
	proc sql noprint;
		select catt(name,"=",substr(name, 1, length(name)-1))
			into :renamelist separated by ' '
				from dictionary.columns
					where lowcase(libname)="work" and lowcase(memname)="input_up_mgp_termo"
						and index(lowcase(name), 'g1_terna_mgp_day')>0;
	quit;

	data input_up_mgp_termo;
		set input_up_mgp_termo;
		rename &renamelist;
	run;

	/*----------------------------------------------------------------------*/
	/*----------------------------  FORECASTS  -----------------------------*/
	/*---------------------------- hourly lags -----------------------------*/
	/*--------------------------------  G ----------------------------------*/
	/*----------------------------------------------------------------------*/
	data previsione_oraria_lag;
		set input_up_mgp_ora_termo(drop=G1_:);
		dt_calcolo_1=intnx("dthour",dt_calcolo,24*1);
		dt_calcolo_7=intnx("dthour",dt_calcolo,24*7);
		format giorno_solare: date9. dt_calcolo: datetime19.;
		rename terna_mgp_day_cum=terna_mgp_oraria_cumday
				terna_mgp_gday_cum=terna_mgp_oraria_cumgday;
	run;

	%ffill_general(WORK.PREVISIONE_ORARIA_LAG, WORK.PREVISIONE_ORARIA_LAG1, 
		cats(name, '_lag1d'), lowcase(name) like "g_terna%");

	data previsione_oraria_lag1(rename=(dt_calcolo_1=dt_calcolo));
		set previsione_oraria_lag1(drop=giorno: dt_calcolo dt_calcolo_7);
	run;

	%ffill_general(WORK.PREVISIONE_ORARIA_LAG, WORK.PREVISIONE_ORARIA_LAG7, 
		cats(name, '_lag7d'), lowcase(name) like "g_terna%");

	data previsione_oraria_lag7(rename=(dt_calcolo_7=dt_calcolo));
		set previsione_oraria_lag7(drop=giorno: dt_calcolo dt_calcolo_1);
	run;

	data input_mgp_orario_lag;
		merge calendario_orario(in=uno keep=dt_calcolo) previsione_oraria_lag7(in=due)
			previsione_oraria_lag1(in=tre);
		by dt_calcolo;

		if uno and due and tre;
	run;

	/*----------------------------------------------------------------------*/
	/*----------------------------  FORECASTS  -----------------------------*/
	/*---------------------------- hourly lags -----------------------------*/
	/*-------------------------------- G1 ----------------------------------*/
	/*----------------------------------------------------------------------*/
	data input_up_mgp_ora_termo_split;
		set input_up_mgp_ora_termo(drop=G_: g1_prev_mgp_post
			rename= (G1_terna_mgp_ora = terna_mgp_ora
			G1_terna_mgp_ora_cumday= terna_mgp_ora_cumday
			G1_terna_mgp_ora_cumgday= terna_mgp_ora_cumgday)
			);

		if hour < 13 then
			date_g1 = date - 1;
		else date_g1 = date;
		format date_g1 date9.;
	run;

	proc sort data=input_up_mgp_ora_termo_split out=input_up_mgp_ora_termo_1(drop=date_g1
		rename=(date_g1=date
		terna_mgp_ora = G_terna_mgp_ora
		terna_mgp_ora_cumday=G_terna_mgp_ora_cumday
		terna_mgp_ora_cumgday=G_terna_mgp_ora_cumgday 
		));
		by date hour;
	run;

	proc sort data=input_up_mgp_ora_termo_split out=input_up_mgp_ora_termo_2(drop=date 
																			 rename=(date_g1=date
																					 terna_mgp_ora = G1_terna_mgp_ora
																					 terna_mgp_ora_cumday=G1_terna_mgp_ora_cumday));
		by date_g1 hour;
	run;

	data input_up_mgp_ora_termo_split(drop=terna_mgp_ora_cumgday);
		merge input_up_mgp_ora_termo_1(in=a) input_up_mgp_ora_termo_2(drop=giorno_gas);
		by date hour;
		if a;
	run;

	data input_up_mgp_ora_termo_split;
		set input_up_mgp_ora_termo_split;
		by giorno_gas;

		if first.giorno_gas then
			G1_terna_mgp_ora_cumgday = 0;
		G1_terna_mgp_ora_cumgday + G1_terna_mgp_ora;
	run;

	data previsione_oraria_lag_G1;
		set input_up_mgp_ora_termo_split(drop=G_:);
		dt_calcolo = dhms(date, hour, 0, 0);
		dt_calcolo_2=intnx("dthour",dt_calcolo,24*2);
		dt_calcolo_7=intnx("dthour",dt_calcolo,24*7);
		format giorno_solare: date9. dt_calcolo: datetime19.;
	run;

	%ffill_general(WORK.PREVISIONE_ORARIA_LAG_G1, WORK.PREVISIONE_ORARIA_LAG2, 
		cats(name, '_lag2d'), lowcase(name) like "g1_terna%");

	data previsione_oraria_lag2(rename=(dt_calcolo_2=dt_calcolo));
		set previsione_oraria_lag2(drop=giorno: dt_calcolo dt_calcolo_7);
	run;

	%ffill_general(WORK.PREVISIONE_ORARIA_LAG_G1, WORK.PREVISIONE_ORARIA_LAG7, 
		cats(name, '_lag7d'), lowcase(name) like "g1_terna%");

	data previsione_oraria_lag7(rename=(dt_calcolo_7=dt_calcolo));
		set previsione_oraria_lag7(drop=giorno: dt_calcolo dt_calcolo_2);
	run;

	proc sort data=previsione_oraria_lag7;
		by dt_calcolo;
	run;

	proc sort data=previsione_oraria_lag2;
		by dt_calcolo;
	run;

	data input_mgp_orario_lag_G1;
		merge calendario_orario(in=uno keep=dt_calcolo) previsione_oraria_lag7(in=due)
			previsione_oraria_lag2(in=tre);
		by dt_calcolo;

		if uno and due and tre;
	run;

	proc sort data=input_mgp_orario_lag;
		by dt_calcolo;
	run;

	proc sort data=input_mgp_orario_lag_G1;
		by dt_calcolo;
	run;

	data input_mgp_orario_lag_new;
		merge input_mgp_orario_lag input_mgp_orario_lag_G1;
		by dt_calcolo;
	run;

	proc sort data=input_mgp_orario_lag_new;
		by dt_calcolo;
	run;

	proc datasets lib=work nolist;
		delete cons: mgp:;
	run;

	/*---------------------------------------------------------------*/
	/*---------------------------------------------------------------*/
	/*-------------------------- MERGE ALL --------------------------*/
	/*---------------------------------------------------------------*/
	/*---------------------------------------------------------------*/

	data &output_terna_cons(where=(datepart(dt_calcolo)>=&start_date_terna-1));
		merge  input_up_consuntivo_termo(drop=terna_consuntivo) input_cons_lag 
			input_up_consuntivo_ora_termo(drop=terna_consuntivo_post)
			input_cons_orario_lag_new;
		by dt_calcolo;
		G_data_gas_rif = giorno_gas;
		G1_data_gas_rif = giorno_gas + 1;
		G2_data_gas_rif = giorno_gas + 2;
		G3_data_gas_rif = giorno_gas + 3;
		G4_data_gas_rif = giorno_gas + 4;
		format G_data_gas_rif G1_data_gas_rif G2_data_gas_rif 
			G3_data_gas_rif G4_data_gas_rif date9.;
		drop dt_riferimento;
		rename giorno_gas=data_gas giorno_solare=date; /*dt_calcolo=datetime_solare*/
	run;

	data &output_terna_prev(where=(datepart(dt_calcolo)>=&start_date_terna-1));
		merge input_up_mgp_termo input_mgp_lag_new 
			input_up_mgp_ora_termo(drop=g1_prev_mgp_post)
			input_mgp_orario_lag_new;
		by dt_calcolo;
		G_data_gas_rif = giorno_gas;
		G1_data_gas_rif = giorno_gas + 1;
		G2_data_gas_rif = giorno_gas + 2;
		G3_data_gas_rif = giorno_gas + 3;
		G4_data_gas_rif = giorno_gas + 4;
		format G_data_gas_rif G1_data_gas_rif G2_data_gas_rif 
			G3_data_gas_rif G4_data_gas_rif date9.;

		if giorno_solare eq . then
			giorno_solare=datepart(dt_calcolo);
		drop dt_inserimento_val;
		rename giorno_gas=data_gas giorno_solare=date; /*dt_calcolo=datetime_solare*/
	run;

	%hterna(input_up_mgp_ora_termo, input_up_consuntivo_ora_termo,
			&output_terna_prev, &output_terna_cons);

%mend create_terna;

%macro hterna(input_up_mgp_ora_termo, input_up_consuntivo_ora_termo,
				output_previsione_terna, output_consuntivo_terna);

/*	%let input_up_mgp_ora_termo = input_up_mgp_ora_termo;*/
/*	%let input_up_consuntivo_ora_termo = input_up_consuntivo_ora_termo;*/
/*	%let start_date_terna=&start_date_long;*/

	data terna_mgp_orario1(drop=dt_calcolo_shift) terna_mgp_orario2(drop=dt_calcolo);
		set &input_up_mgp_ora_termo;
		keep dt_calcolo dt_calcolo_shift hour G1_terna_mgp_ora G_terna_mgp_ora 
		g1_prev_mgp_post;
		dt_calcolo_shift = intnx("dthour", dt_calcolo, -24);
		format dt_calcolo_shift datetime19.;
	run;

	data terna_mgp_orario_sj;
		merge terna_mgp_orario1(in=L) 
		terna_mgp_orario2(in=R rename=(dt_calcolo_shift=dt_calcolo
		G1_terna_mgp_ora=G1_terna_mgp_ora_shift) drop=G_terna_mgp_ora 
		g1_prev_mgp_post);

		by dt_calcolo hour;
		if L;
		G1_terna_mgp_ora_post = G1_terna_mgp_ora;
		if hour >=0 and hour <= 12 then 
			do;
				G1_terna_mgp_ora_post = G1_terna_mgp_ora_shift;
			end;
		flg = 0 ;
		if g1_prev_mgp_post = G1_terna_mgp_ora_post then flg =1;
		if missing(g1_prev_mgp_post) then 
			g1_prev_mgp_post = G1_terna_mgp_ora_post;
		/* NB uso G1_terna_mgp_ora_post per fillare lo storico, dopodich� la var
			corretta � g1_prev_mgp_post!!! Far� un rename di g1_prev_mgp_post
			in G1_terna_mgp_ora_post, intanto droppo G1_terna_mgp_ora_post orginale! */
		drop G1_terna_mgp_ora_post flg;
	run;

	proc transpose data=terna_mgp_orario_sj(rename=(g1_prev_mgp_post=G1_terna_mgp_ora_post)) 
		out=terna_mgp_orario_sj_up;
		by dt_calcolo hour;
		var G1_terna_mgp_ora G1_terna_mgp_ora_post G_terna_mgp_ora;
	run;

	data terna_mgp_orario_dt;
		length key_model $2.;
		set terna_mgp_orario_sj_up;
		dt_calcolo_new = dhms(datepart(dt_calcolo), 13, 0, 0);
		if _NAME_ = "G_terna_mgp_ora" then 
			dt_calcolo_new = dhms(datepart(dt_calcolo), 0, 0, 0);
		if hour >=0 and hour <= 12 and _NAME_ = "G1_terna_mgp_ora" 
			then 
				do;
					dt_calcolo_new = dhms(datepart(dt_calcolo), 0, 0, 0);
				end;
		if dt_calcolo_new <= dt_calcolo and _NAME_ = "G1_terna_mgp_ora_post"
			then delete; * dalle 13 in poi la prev G1 post ed effettiva coincidono;
		format dt_calcolo_new datetime19.;
		model = compress(tranwrd(substr(_NAME_, 1, 2), "_", ""));
		if model = "G" then key_model = "_";
		else key_model = "_1";
		key = compress(catx("", key_model, "_hterna_mgp_ora_h", put(hour, $8.)));
	run;

	proc sort data=terna_mgp_orario_dt out=terna_mgp_orario_dt_sort;
		by dt_calcolo_new key_model;
	run;

	proc transpose data=terna_mgp_orario_dt_sort out=terna_mgp_orario_t;
		by dt_calcolo_new;
		var COL1;
		id key;
	run;

/*	data terna_mgp_orario_t;*/
/*		set terna_mgp_orario_t;*/
/*		array g [*] __hterna:;*/
/*		array g1[*] _1_hterna:;*/
/*		if _N_ = 1 then */
/*			do;*/
/*				do i = 1 to dim(g);*/
/*					g1[i] = g[i];*/
/*				end;*/
/*			end;*/
/*	run;*/

	data terna_mgp_orario_join;
		merge calendario_orario_final(in=L rename=(datetime_solare=dt_calcolo))
		terna_mgp_orario_t(in=R rename=(dt_calcolo_new=dt_calcolo));

		by dt_calcolo;
		if L;
	run;

	%ffill_general(input=work.terna_mgp_orario_join, output=work.terna_mgp_orario_fill, frame=compress(catt("G", substr(name,2,-1))), criterion=lowcase(name) contains 'hterna');

	data &output_previsione_terna(where=(datepart(dt_calcolo)>=&start_date_terna));
		merge &output_previsione_terna(in=L)
		terna_mgp_orario_fill(in=R
		keep=dt_calcolo g:);
		if L;
		by dt_calcolo;
	run;

	/* CONSUNTIVI */

	data terna_cons_orario1(drop=dt_calcolo_shift) terna_cons_orario2(drop=dt_calcolo);
		set &input_up_consuntivo_ora_termo;
		keep dt_calcolo dt_calcolo_shift hour G1_terna_cons_ora G_terna_cons_ora
		terna_consuntivo_post;
		dt_calcolo_shift = intnx("dthour", dt_calcolo, -24);
		format dt_calcolo_shift datetime19.;
	run;


	/* NB rispetto alla data-ora in cui mi trovo, i consuntivi G e G1 sono il medesimo dato*/

	data terna_cons_orario_sj;
		merge terna_cons_orario1(in=L) 
		terna_cons_orario2(in=R rename=(dt_calcolo_shift=dt_calcolo
		G1_terna_cons_ora=G1_terna_cons_ora_shift) 
		drop=G_terna_cons_ora terna_consuntivo_post);

		by dt_calcolo hour;
		if L;
		G1_terna_cons_ora_post = G1_terna_cons_ora;
		G_terna_cons_ora_post = G_terna_cons_ora;
		if hour >=0 and hour <= 12 then 
			do;
				G1_terna_cons_ora_post = G1_terna_cons_ora_shift;
				G_terna_cons_ora_post = G1_terna_cons_ora_post;
			end;
		flg = 0 ;
		if terna_consuntivo_post = G1_terna_cons_ora_post then flg =1;
		if missing(terna_consuntivo_post) then 
			terna_consuntivo_post = G1_terna_cons_ora_post;
		/* NB uso G1_terna_cons_ora_post per fillare lo storico (SE servisse), 
			dopodich� la varcorretta � terna_consuntivo_post!!! Far� un rename di 
			terna_consuntivo_post in G1_terna_cons_ora_post, intanto droppo G1_terna_cons_ora_post
			orginale! */
		drop G1_terna_cons_ora_post G_terna_cons_ora_post flg;
	run;

	data terna_cons_orario_sj;
		set terna_cons_orario_sj;
		G_terna_cons_ora_post = terna_consuntivo_post;
		G1_terna_cons_ora_post = terna_consuntivo_post;
		drop terna_consuntivo_post;
	run;

	proc transpose data=terna_cons_orario_sj 
		out=terna_cons_orario_sj_up;
		by dt_calcolo hour;
		var  G_terna_cons_ora G_terna_cons_ora_post G1_terna_cons_ora
		G1_terna_cons_ora_post;
	run;

	data terna_cons_orario_dt;
		length key_model $2.;
		set terna_cons_orario_sj_up;
		dt_calcolo_new = dhms(datepart(dt_calcolo), 13, 0, 0);
		if not index(_NAME_,"post") and hour < 13 then 
			dt_calcolo_new = dhms(datepart(dt_calcolo), 0, 0, 0);
		if dt_calcolo_new <= dt_calcolo and index(_NAME_, "post")
			then delete; * dalle 13 in poi il cons post ed effettivo coincidono;
		format dt_calcolo_new datetime19.;
		model = compress(tranwrd(substr(_NAME_, 1, 2), "_", ""));
		if model = "G" then key_model = "_";
		else key_model = "_1";
		key = compress(catx("", key_model, "_hterna_cons_ora_h", put(hour, $8.)));
	run;

	proc sort data=terna_cons_orario_dt out=terna_cons_orario_dt_sort;
		by dt_calcolo_new key_model hour;
	run;

	proc transpose data=terna_cons_orario_dt_sort out=terna_cons_orario_t;
		by dt_calcolo_new;
		var COL1;
		id key;
	run;

	data terna_cons_orario_join;
		merge calendario_orario_final(in=L rename=(datetime_solare=dt_calcolo))
		terna_cons_orario_t(in=R rename=(dt_calcolo_new=dt_calcolo));

		by dt_calcolo;
		if L;
	run;

	%ffill_general(input=work.terna_cons_orario_join, output=work.terna_cons_orario_fill, frame=compress(catt("G", substr(name,2,-1))), criterion=lowcase(name) contains 'hterna');

	data &output_consuntivo_terna(where=(datepart(dt_calcolo)>=&start_date_terna));
		merge &output_consuntivo_terna(in=L)
		terna_cons_orario_fill(in=R
		keep=dt_calcolo g:);
		if L;
		by dt_calcolo;
	run;

%mend hterna;

    




%macro gestione_anomalie_terna(dt_base=, ds_in1=, ds_in2=, ds_out1=, ds_out2=);
/*, storico_anomalie_terna=);*/
	/*	%let dt_base=&start;*/
	/*	%let ds_in1=terna_prev;*/
	/*	%let ds_in2=terna_cons;*/
	/*	%let ds_out1=terna_prev;*/
	/*	%let ds_out2=terna_cons;*/
	/*	%let storico_anomalie_terna=&output_anomalie_terna;*/
	/* Fisso a 0 min e 0 secondi il datetime &start*/
	data _null_;
		cut_date=dhms(datepart(&dt_base),hour(&dt_base),0,0);
		call symput("cut_date",cut_date);
	run;

	/* Taglio i ds di input */
	proc sql;
		select max(data_rif) into :date_terna_prev 
			from &ds_in1 
				(where = (data_rif le datepart(&cut_date) ));
	quit;

	proc sql;
		select max(data_rif) into :date_terna_cons 
			from &ds_in2 
				(where = (dt_inserimento_val le &cut_date));
	quit;

	/* Calcolo la stagione in cui sono: a seconda che sia inverno o estate ho una diversa dt_baseline */
/*	data stagioni;*/
/*		date_now=datepart(&cut_date);*/
/*		d1=nwkdom(5, 1, 3, year(date_now));*/
/*		d2=nwkdom(5, 1, 10, year(date_now));*/
/*		format d1 d2 date9.;*/
/*		length stagione $7;*/
/**/
/*		if (date_now>= d1  and date_now< d2) then*/
/*			stagione="estate";*/
/*		else if (date_now < d1 or date_now >= d2) then*/
/*			stagione="inverno";*/
/*		call symput("stagione",strip(stagione));*/
/*		format date_now date9.;*/
/*	run;*/
/**/
/*	%put Sono in &stagione;*/

	data check_terna (keep=date_terna_prev date_terna_cons date_baseline_prev date_baseline_cons
		check_ritardo_terna_prev check_ritardo_terna_cons stagione);
		retain date_baseline_cons date_terna_cons check_ritardo_terna_cons 
			date_baseline_prev date_terna_prev check_ritardo_terna_prev;

		/* Creo date BASELINE su cui effettuare il confronto */
		dt_now=&cut_date;
		date_now=datepart(dt_now);
/*		stagione="&stagione";*/
/*		if stagione="inverno" then*/
/*			dt_baseline=dhms(datepart(dt_now),14,0,0);*/
/*		else dt_baseline=dhms(datepart(dt_now),13,0,0);*/
		dt_baseline=dhms(datepart(dt_now),13,0,0);

		/* La massima data di riferimento delle previsioni deve essere uguale ad oggi */
		date_baseline_prev=date_now;

		/* Se sono prima delle 13 di oggi --> 
			ultima data_rif per i consuntivi = altro ieri 
			(ultima data_rif per le previsioni = oggi)
		*/
		if dt_now lt dt_baseline then
			do;
				date_baseline_cons=intnx("day",date_now,-2);
			end;
		/* Se sono dopo le 13 di oggi --> 
				ultima data_rif per i consuntivi = ieri 
				(ultima data_rif per le previsioni = domani)
		*/
		else
			do;
				date_baseline_cons=intnx("day",date_now,-1);
			end;

		/* Effettuo il confronto */
		date_terna_prev=&date_terna_prev;
		date_terna_cons=&date_terna_cons;
		check_ritardo_terna_prev=ifn(date_terna_prev=date_baseline_prev,0,1);
		check_ritardo_terna_cons=ifn(date_terna_cons=date_baseline_cons,0,1);
		call symput("check_ritardo_terna_prev",check_ritardo_terna_prev);
		call symput("check_ritardo_terna_cons",check_ritardo_terna_cons);
		format date: date9. 
			dt: datetime19.;
	run;

	/* Creo la tabella totale di anomalie */
	data anomalie_terna_all;
		datetime_solare=&cut_date;
		check_ritardo_terna_prev=&check_ritardo_terna_prev;
		check_ritardo_terna_cons=&check_ritardo_terna_cons;
		format datetime_solare datetime19.;
	run;

	/* ====== Gestione eventuali anomalie nelle previsioni terna ====== */
	%if &check_ritardo_terna_prev=1 %then
		%do;
			%put ===== Previsioni terna non disponibili =====;

			/* Taglio il ds di input per &cut_date */
			data &ds_out1;
				set &ds_in1
					(where = (dt_inserimento_val le &cut_date));
			run;

			/* Inserisco dati vecchi di una settimana o di un giorno */
			proc sql noprint;
				select max(data_rif)
					into :date_terna_prev
						from  &ds_out1;
			quit;

			/* Controllo se il lag7 del giorno mancante che devo inserire (date_terna_prev+1gg) ? festivo.
				   Se ? festivo --> inserisco come valori del giorno mancante quelli di ieri
				   Altrimenti --> inserisco come valori del giorno mancante quelli di 1 settimana fa 
			*/
			data check_lag7_festivo_prev;
				date_terna_prev=&date_terna_prev;
				date_to_insert=intnx("day",&date_terna_prev,1);
				lag7=intnx("day",date_to_insert,-7);
				time_festivo = 0;

				if day(lag7) = 1 and month(lag7) = 1 then
					time_festivo = 1;

				if day(lag7) = 6 and month(lag7) = 1 then
					time_festivo = 1;

				if day(lag7) = 25 and month(lag7) = 4 then
					time_festivo = 1;

				if day(lag7) = 1 and month(lag7) = 5 then
					time_festivo = 1;

				if day(lag7) = 2 and month(lag7) = 6 then
					time_festivo = 1;

				if day(lag7) = 15 and month(lag7) = 8 then
					time_festivo = 1;

				if day(lag7) = 1 and month(lag7) = 11 then
					time_festivo = 1;

				if day(lag7) = 8 and month(lag7) = 12 then
					time_festivo = 1;

				if lag7 = holiday('EASTER',year(lag7)) then
					time_festivo = 1;

				if lag7 = intnx('day',holiday('EASTER',year(lag7)),1) then
					time_festivo = 1;

				if lag7 = intnx('day',holiday('EASTER',year(lag7)),2) then
					time_festivo = 1;

				if day(lag7) = 25 and month(lag7) = 12 then
					time_festivo = 1;

				if day(lag7) = 26 and month(lag7) = 12 then
					time_festivo = 1;

				if day(lag7) = 31 and month(lag7) = 12 then
					time_festivo = 1;
				call symput("time_festivo_prev",time_festivo);
				call symput("lag7prev",lag7);
				format date_to_insert date_terna_prev lag7 date9.;
			run;

			data terna_prev_add (drop=dt_inserimento_val data_rif dt_rif increment
				rename=(data_rif_add=data_rif dt_inserimento_val_add=dt_inserimento_val
				dt_rif_add=	dt_rif) );
				set &ds_out1;

				/* Se il lag7 non ? festivo, allora sostituisco il giorno mancante con il lag7 */
				if &time_festivo_prev=0 then
					do;
						if data_rif=&lag7prev;
						increment=7;
					end;

				/* Se il lag7 ? festivo, sostituisco il giorno mancante con ultimo valore disponibile */
				else
					do;
						if data_rif=&date_terna_prev;
						increment=1;
					end;

				data_rif_add=intnx("day",data_rif,increment);
				dt_inserimento_val_add=intnx("dtday",dt_inserimento_val,increment,"s");
				dt_rif_add=intnx("dtday",dt_rif,increment,"s");
				format data_rif_add date9. dt_inserimento_val_add dt_rif_add datetime19.;
			run;

			data &ds_out1;
				set &ds_out1 terna_prev_add;
			run;

		%end;
	%else
		%do;
			%put ===== Previsioni terna disponibili =====;
		%end;

	/*  ====== Gestione eventuali anomalie nei consuntivi terna ====== */
	%if &check_ritardo_terna_cons=1 %then
		%do;
			%put ===== Consuntivi terna non disponibili =====;

			/* Taglio il ds per &cut_date */
			data &ds_out2;
				set &ds_in2
					(where = (dt_inserimento_val le &cut_date));
			run;

			/* Inserisco dati vecchi di una settimana o di un giorno */
			proc sql noprint;
				select max(data_rif)
					into :date_terna_cons
						from &ds_out2;
			quit;

			/* Controllo se il lag7 del giorno mancante che devo inserire (date_terna_cons+1gg) ? festivo.
				   Se ? festivo --> inserisco come valori del giorno mancante quelli di ieri
				   Altrimenti --> inserisco come valori del giorno mancante quelli di 1 settimana fa 
			*/
			data check_lag7_festivo_cons;
				date_terna_cons=&date_terna_cons;
				date_to_insert=intnx("day",&date_terna_cons,1);
				lag7=intnx("day",date_to_insert,-7);
				time_festivo = 0;

				if day(lag7) = 1 and month(lag7) = 1 then
					time_festivo = 1;

				if day(lag7) = 6 and month(lag7) = 1 then
					time_festivo = 1;

				if day(lag7) = 25 and month(lag7) = 4 then
					time_festivo = 1;

				if day(lag7) = 1 and month(lag7) = 5 then
					time_festivo = 1;

				if day(lag7) = 2 and month(lag7) = 6 then
					time_festivo = 1;

				if day(lag7) = 15 and month(lag7) = 8 then
					time_festivo = 1;

				if day(lag7) = 1 and month(lag7) = 11 then
					time_festivo = 1;

				if day(lag7) = 8 and month(lag7) = 12 then
					time_festivo = 1;

				if lag7 = holiday('EASTER',year(lag7)) then
					time_festivo = 1;

				if lag7 = intnx('day',holiday('EASTER',year(lag7)),1) then
					time_festivo = 1;

				if lag7 = intnx('day',holiday('EASTER',year(lag7)),2) then
					time_festivo = 1;

				if day(lag7) = 25 and month(lag7) = 12 then
					time_festivo = 1;

				if day(lag7) = 26 and month(lag7) = 12 then
					time_festivo = 1;

				if day(lag7) = 31 and month(lag7) = 12 then
					time_festivo = 1;
				call symput("time_festivo_cons",time_festivo);
				call symput("lag7cons",lag7);
				format date_to_insert date_terna_cons lag7 date9.;
			run;

			data terna_cons_add (drop=dt_inserimento_val data_rif dt_rif increment
				rename=(data_rif_add=data_rif dt_inserimento_val_add=dt_inserimento_val
				dt_rif_add=	dt_rif) );
				set &ds_out2;

				/* Se il lag7 non ? festivo, allora sostituisco il giorno mancante con il lag7 */
				if &time_festivo_cons=0 then
					do;
						if data_rif=&lag7cons;
						increment=7;
					end;

				/* Se il lag7 ? festivo, sostituisco il giorno mancante con ultimo valore disponibile */
				else
					do;
						if data_rif=&date_terna_cons;
						increment=1;
					end;

				data_rif_add=intnx("day",data_rif,increment);
				dt_inserimento_val_add=intnx("dtday",dt_inserimento_val,increment,"s");
				dt_rif_add=intnx("dtday",dt_rif,increment,"s");
				format data_rif_add date9. dt_inserimento_val_add dt_rif_add datetime19.;
			run;

			data &ds_out2;
				set &ds_out2 terna_cons_add;
			run;

		%end;
	%else
		%do;
			%put ===== Consuntivi terna disponibili =====;
		%end;

	/* Storicizzo eventuali anomalie */
/*	%put ==== Storicizzo Anomalie Terna ====;*/
/**/
/*	%if %sysfunc(exist(&storico_anomalie_terna)) %then*/
/*		%do;*/
/**/
/*			proc append base=&storico_anomalie_terna data=anomalie_terna_all force;*/
/*			run;*/
/**/
/*		%end;*/
/*	%else*/
/*		%do;*/
/**/
/*			data &storico_anomalie_terna;*/
/*				set anomalie_terna_all;*/
/*			run;*/
/**/
/*		%end;*/

%mend gestione_anomalie_terna;



%macro run_parallel_session(server_name, data_family);

	%put --- PROCESSING &data_family ---;
	%if "&server_name."="server1" %then %do;
		%put_run_date_init(create_scada);
		%create_scada(&input_bilancioscada, &start_date_long, &output_scada_tot);
		%put_run_time(create_scada);

		%put_run_date_init(create_target_rtot);
		%create_target_rtot(&input_bilanciosnam, &input_dlp_orario, 
						&input_pcs_misura, &input_riconsegnato1, &input_riconsegnato2, 
						&output_scada, output_rtot, &g_gas, &start_date_long);
		%put_run_time(create_target_rtot);

		%put_run_date_init(create_tgt_rimanente);
		%create_tgt_rimanente(output_rtot, &output_scada, &output_rdif);
		%put_run_time(create_tgt_rimanente);
			
		%put_run_date_init(create_lagsimile);
		%create_lagsimile(output_rtot, &input_calendario, &output_lagsim);
		%put_run_time(create_lagsimile);

		%put_run_date_init(create_lagsimile_G1);
		%create_lagsimile_G1(output_rtot, &input_calendario, &output_lagsim_G1);
		%put_run_time(create_lagsimile_G1);

		%put_run_date_init(create_rtot_lag_mvg);
		%create_rtot_lag_mvg(output_rtot, &output_lagsim, &start_date_long, &output_rtot_lag_mvg);
		%put_run_time(create_rtot_lag_mvg);

		%put_run_date_init(create_target_termo);
		%create_target_termo(&start_date_long, &input_termo, &input_sime2, &input_sime, &input_vol_val, rcomp_termo_parziale);
		%put_run_time(create_target_termo);

		%put_run_date_init(create_bil_lag_mvg);
		%create_bil_lag_mvg(&input_bilanciosnam, output_rtot, rcomp_termo_parziale, &output_rall, 
						&input_calendario, &output_bil_lag_mvg, &start_date_long);
		%put_run_time(create_bil_lag_mvg);

	%end;
	%else %if "&server_name."="server2" %then %do;
		%put_run_date_init(create_scadacons);
		%create_scadacons(&anagrafica, &valori, &start_date_short, &output_scadacons_tot, &dt_solare);
		%put_run_time(create_scadacons);
		
		%put_run_date_init(create_terna);
		%create_terna(&input_terna,&input_up, &input_remi_termo, &start_date_short, &output_consuntivo_terna, &output_previsione_terna);
		%put_run_time(create_terna);
	%end;
	%else %if "&server_name."="server3" %then %do;
		%put_run_date_init(create_flussifisici);
		%create_flussifisici(&start_date_long, &output_scada_ff); /*&start_date_long or short???*/
		%put_run_time(create_flussifisici);
	%end;
	%else %if "&server_name."="server4" %then %do;
		%put_run_date_init(create_meteocitta);
		%create_meteocitta(&meteo_citta, &start_date_short, &output_citta_tot);
		%put_run_time(create_meteocitta);
	%end;
	%else %if "&server_name."="server5" %then %do;
		%put_run_date_init(create_meteocelle);
		%create_meteocelle(&meteo_celle_cons, &meteo_celle_prev, &start_date_short, &output_celle_tot);
		%put_run_time(create_meteocelle);
	%end;
%mend run_parallel_session;


/* %macro parallel_function(logic_path, lib_path, server_name, data_family); */
/*  */
/* signon &server_name.; */
/*  */
/* rsubmit &server_name. wait=no; */
/*  */
/* 	%let logic_path=/Public/Porting_deploy/new/; */
/* 	%let lib_path = /sasdata/porting_viya;  */
/*  */
/* 	filename s1 FILESRVC folderpath="/Public/Porting_deploy/new/" filename='librerie_viya.sas'; */
/* 	filename utl FILESRVC folderpath="/Public/Porting_deploy/new/" filename='utils.sas';  */
/* 	filename fcn FILESRVC folderpath="/Public/Porting_deploy/new/" filename='create_functions.sas'; */
/* 	filename inp FILESRVC folderpath="/Public/Porting_deploy/new/" filename='initpar_input.sas'; */
/* 	%include s1; */
/* 	%include utl; */
/* 	%include fcn; */
/* 	%include inp; */
/* 	%run_parallel_session(&server_name., &data_family.); */
/*  */
/* endrsubmit; */
/*  */
/* %mend parallel_function; */

%macro parallel_function(server_name, data_family);

signon &server_name.;

%syslput lib_path = &lib_path.;
%syslput server_name = &server_name.;
%syslput data_family = &data_family.;
%syslput librerie_viya = &librerie_viya.;
%syslput g1_utils = &g1_utils.;
%syslput g1_functions = &g1_functions.;
%syslput g1_initpar = &g1_initpar.;

rsubmit &server_name. wait=no;

	%include "&lib_path./&librerie_viya..sas";
	%include "&lib_path./&g1_utils..sas";
	%include "&lib_path./&g1_functions..sas";
	%include "&lib_path./&g1_initpar..sas";
	%run_parallel_session(&server_name., &data_family.);
	cas mySession terminate; 

endrsubmit;

%mend parallel_function;