/*Inizio Sleep Time*/

/* DATA _NULL_; */
/* 	RC=SLEEP(180,1); */
/* RUN; */

/*Aspetto 300 secondi*/
/*DATA _NULL_;
	RC=SLEEP(300,1);
RUN;*/

/*DATA _NULL_;
	RC=SLEEP(300,1);
RUN;*/

/*Fine Sleep Time*/

/*Inizio Processo Previsione*/

%put DOING GENERAZIONE RIGA DI INPUT, PREVISIONE, ENSEMBLE E SCRITTURA PREVISIONE;

/* options mlogic mprint symbolgen; */

%global logic_path lib_path;

%let logic_path = /Public/DAFNE/;
/* %let logic_path = /Public/DAFNE_BKP/; */
/* %let lib_path = /opt/sas/viya/config/data/cas/default/test; * DA CAMBIARE CON IL PATH DEFINITIVO DELLA NUOVA FOR_SVIL; */
%let lib_path = /opt/sas/viya/config/data/cas/default/dafne;

filename C1 FILESRVC folderpath="&logic_path" filename='librerie_viya.sas';
filename C2 FILESRVC folderpath="&logic_path" filename='g_master_inputprevisioni.sas';
/* filename C3 FILESRVC folderpath="&logic_path" filename='Storicizzazione_Prev_Giornaliere_Viya_WDisp_Pre_SDM.sas'; */
/* filename C4 FILESRVC folderpath="&logic_path" filename='Storicizzazione_BPM_RUN_WDisp_Pre_SDM.sas'; */
%include C1;
%include C2;
/* %include C3; */
/* %include C4; */
