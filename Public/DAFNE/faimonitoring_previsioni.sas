%put ----- Monitoraggio Automatico della Qualità delle Previsioni -----;

%global logic_path report_path;

%let logic_path = /Public/DAFNE/;
%let report_path = /opt/sas/viya/config/data/cas/default/report/;
%let lib_path = /opt/sas/viya/config/data/cas/default/dafne;
%let hour_mail_list = (0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23);
%let hour_report_list = (5 9 12 13 14 15 21);

filename M1 FILESRVC folderpath="&logic_path" filename='librerie_viya.sas';
filename M2 FILESRVC folderpath="&logic_path" filename='monitoring_master_previsioni.sas';
%include M1;
%include M2;