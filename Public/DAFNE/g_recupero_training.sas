/*===============================================================*/
/***** rilascio 2020-01-XX - versione v1.0 *****/
/***** autori: Binda, Oliva, Rizzolo *****/
/***** BIP S.p.a. *****/
/*===============================================================*/

/*============ General Options and Creation of CAS Session ========*/
options mprint mlogic symbolgen linesize=max;
ods exclude all;
/* options nosymbolgen nomprint nomlogic; */
/* %global current_hour; */
/* %global hour_train start start_fix model_path; */

/* %global dataset_presente lib_physical lib_memory g_output_final_name; */
/* %global logic_path lib_path; */

%let start = %sysfunc(datetime());
%let start_fix = %sysfunc(datetime());
%let diff_end_month=-1;
%let pfx=;
%let lib_physical=for_svil;
%let lib_memory=public;

/* PER IL TEST */
/* %let g_output_final_name=&pfx.g_trainingset_viya_test; */
%let g_output_final_name=&pfx.g_trainingset_viya;

/* %let logic_path = /Public/DAFNE_BKP/; */
%let logic_path = /Public/DAFNE/;

%let lib_path = /opt/sas/viya/config/data/cas/default/dafne;
/* %let lib_path = /opt/sas/viya/config/data/cas/default/dafne_bkp; */
%let model_path = &lib_path;
/* libname for_svil "/opt/sas/viya/config/data/cas/default/dafne_bkp"; */

filename glib FILESRVC folderpath="&logic_path" filename='librerie_viya.sas';
filename gtr  FILESRVC folderpath="&logic_path" filename='g_training.sas';

options casdatalimit=ALL;
cas mySession sessopts=(caslib=casuser timeout=31536000 locale="en_US");
caslib _all_ assign; 

data check;
	hour_train=hour(&start);
	dt_train=intnx("hour",&start,1);	
	data_gas_train = ifn(hour_train<6, intnx('day', datepart(dt_train), -1), datepart(dt_train));
	date_pred_g = data_gas_train;
	mese_pred_g = month(date_pred_g);
	anno_pred_g = year(date_pred_g);
	call symputx("mese_pred_g", put(mese_pred_g, z2.0));
	call symputx("anno_pred_g", anno_pred_g);
	call symputx("date_pred_g", date_pred_g);
	format da: date9.;
	format dt: datetime19.;
run;

%macro clean_current_models();

	filename myDir "&model_path./";

	data get;
		did=dopen("myDir");
		filecount=dnum(did);
		do i = 1 to filecount;
			file_ = dread(did, i);
			output;
		end;
		rc=dclose(did);
	run;

	data to_delete;
		set get;
		where file_ contains "NN" and file_ contains "&anno_pred_g._&mese_pred_g.";
		fname="tempfile";
		rc=filename(fname, cat("&model_path./", compress(file_)));
		rc=fdelete(fname);
	run;

%mend;

%macro clean_all();

	proc datasets lib=work kill nolist memtype=data;
	quit;

	Data MVars ( Keep = Name );
		Set SasHelp.VMacro;
		Where Scope = 'GLOBAL';

		if find(Name, "SAS") = 0;

		if find(Name, "SYS") = 0;

		if find(Name, "CLIENT") = 0;

		if find(Name, "SQL") = 0;

		if find(Name, "EG") = 0;

		IF NAME NOT IN ("CURRENT_HOUR", "HOUR_TRAIN", "START", "START_FIX", "HOUR_END", 
		"GRAPHTERM", "G_OUTPUT_FINAL_NAME", "LOGIC_PATH", "START", "PFX", "LIB_PHYSICAL", "LIB_MEMORY",
		"LIB_PATH", "DATASET_PRESENTE");
	Run;

	Data _Null_;
		Set MVars;
		Call Symdel( Name );
	Run;

%mend clean_all;

%macro increase_start(dt_start=);
	
	data start;
		start = intnx("hour", &dt_start, 1);
		call symputx("start", start);
		format start datetime19.;
		put start;
	run;
	
%mend;

%macro Emergenza_Training();
	
/* 	%clean_current_models(); */
	
	%include glib;

	%do hour_TRAIN = %sysfunc(hour(&start)) %to 23;
/* 	%do hour_TRAIN = %sysfunc(hour(&start)) %to 13; */

		%include gtr;
		%clean_all();
		%increase_start(dt_start=&start);
	%end;
	
	data end;
		hour_end = hour(&start_fix) - 1;
		call symputx("hour_end", hour_end);
	run;

	%do hour_TRAIN = 0 %to &hour_end;
/* 	%do hour_TRAIN = 0 %to 2; */

		%include gtr;
		%clean_all();
		%increase_start(dt_start=dhms(datepart(&start_fix), &hour_TRAIN, 0, 0));
	%end;
	
%mend Emergenza_Training;

/* in produzione ho l'ora solare quindi non devo fare aggiustamenti */

%Emergenza_Training();

data time_execution;
	dt_start = &start_fix;
	dt_end = datetime();
	time_execution = dt_end - dt_start;
	format dt: datetime19.;
	call symputx('time_execution', time_execution);
run;

%put "========================================================================";
%put "IL CODICE SAS E' GIUNTO AL TERMINE IN: &time_execution. SECONDI";

cas mySession terminate;
