/*============ General Options and Creation of CAS Session ========*/
options casdatalimit=ALL;
cas mySession sessopts=(caslib=casuser timeout=31536000 locale="en_US");
caslib _all_ assign;

options mprint mlogic symbolgen linesize=max;
ods exclude all;

/*============ Global Vars ========*/
%global dataset_presente lib_physical lib_memory output_final_name trainingset;

 %let start = %sysfunc(datetime());  

%let pfx=;
%let lib_physical=for_svil;
%let lib_memory=public;
%let keyprocess=tr;
%let model_path=&lib_path;

%let output_final_name=&pfx.g1_trainingset_viya; 
%let trainingset=&lib_physical..&output_final_name.;


filename utl FILESRVC folderpath="&logic_path" filename='g1_utils.sas'; 
filename fcn FILESRVC folderpath="&logic_path" filename='g1_functions.sas';
filename dat FILESRVC folderpath="&logic_path" filename='g1_dataset.sas';
/*filename trnG FILESRVC folderpath="&logic_path" filename='g_training.sas'; */
filename trnG1 FILESRVC folderpath="&logic_path" filename='g1_training.sas'; 

%let hour_limit=5;

data dates;
	length modello_totrain $2.;
/* 	dt_now=&start; */
	dt_now=dhms(datepart(&start),hour(&start),minute(&start),second(&start));
	g_flg_training=0;
	g1_flg_training=0;
	g2_flg_training=0;
	g3_flg_training=0;
	g4_flg_training=0;
	date_now=datepart(dt_now);
	hour_now=hour(dt_now);
	hour_limit=&hour_limit;
	/* g limits */
	g_limit_inf=dhms(intnx("month",date_now,0,"B"),hour_limit,0,0);
	g_limit_sup=intnx("dtday",g_limit_inf,1,"S");
	/* g1 limits */
	g1_limit_inf1=dhms(intnx("month",date_now,-1,"E"),hour_limit,0,0);
	g1_limit_sup1=g_limit_inf;
	g1_limit_inf2=dhms(intnx("month",date_now,0,"E"),hour_limit,0,0);
	g1_limit_sup2=dhms(intnx("month",date_now,1,"B"),hour_limit,0,0);
	/* -- per gli intervalli inferiori parto da g_limit_inf (1gg del mese corrente) e 
			g1_limit_sup2(1 gg del mese prossimo) andando indietro di x giorni.
	   -- per gli intervalli superiori parto dagli intervalli inferiori del modello prima */
	/* g2 limits */
	g2_limit_inf1=dhms(intnx("day",datepart(g_limit_inf),-2),hour_limit,0,0); /*penultimo gg del mese M-1*/
	g2_limit_sup1=g1_limit_inf1; /*ultimo gg del mese M-1*/
	g2_limit_inf2=dhms(intnx("day",datepart(g1_limit_sup2),-2),hour_limit,0,0); /*penultimo gg del mese M*/
	g2_limit_sup2=g1_limit_inf2; /*ultimo gg del mese M*/
	/* g3 limits */
	g3_limit_inf1=dhms(intnx("day",datepart(g_limit_inf),-3),hour_limit,0,0); /*3ultimo gg del mese M-1*/
	g3_limit_sup1=g2_limit_inf1; /*penultimo gg del mese M-1*/
	g3_limit_inf2=dhms(intnx("day",datepart(g1_limit_sup2),-3),hour_limit,0,0); /*3ultimo gg del mese M*/
	g3_limit_sup2=g2_limit_inf2; /*penultimo gg del mese M*/
	/* g4 limits */
	g4_limit_inf1=dhms(intnx("day",datepart(g_limit_inf),-4),hour_limit,0,0); /*4ultimo gg del mese M-1*/
	g4_limit_sup1=g3_limit_inf1; /*3ultimo gg del mese M-1*/
	g4_limit_inf2=dhms(intnx("day",datepart(g1_limit_sup2),-4),hour_limit,0,0); /*4ultimo gg del mese M*/
	g4_limit_sup2=g3_limit_inf2; /*3ultimo gg del mese M*/

	/* ATTENZIONE! DIANA PARTE ALLE ORE 5 e FINISCE ALLE ORE 4 DEL GIORNO DOPO
		--> NOI QUI ESEGUIAMO IL TRAININGSET DEL G ALLE ORE 4 */
	if (dt_now ge g_limit_inf and dt_now lt g_limit_sup) then do;
		g_flg_training=1;
		modello_totrain="G";
	end;
	if (dt_now ge g1_limit_inf1 and dt_now lt g1_limit_sup1) or 
		(dt_now ge g1_limit_inf2 and dt_now lt g1_limit_sup2) then do;
		g1_flg_training=1;
		modello_totrain="G1";
	end;
	if (dt_now ge g2_limit_inf1 and dt_now lt g2_limit_sup1) or 
		(dt_now ge g2_limit_inf2 and dt_now lt g2_limit_sup2) then do;
		g2_flg_training=1;
		modello_totrain="G2";
	end;
	if (dt_now ge g3_limit_inf1 and dt_now lt g3_limit_sup1) or 
		(dt_now ge g3_limit_inf2 and dt_now lt g3_limit_sup2) then do;
		g3_flg_training=1;
		modello_totrain="G3";
	end;
	if (dt_now ge g4_limit_inf1 and dt_now lt g4_limit_sup1) or 
		(dt_now ge g4_limit_inf2 and dt_now lt g4_limit_sup2) then do;
		g4_flg_training=1;
		modello_totrain="G4";
	end;
	call symputx("hour_now",hour_now);
	call symputx("modello_totrain",modello_totrain);
	call symputx("g_flg_training",g_flg_training);
	call symputx("g1_flg_training",g1_flg_training);
	call symputx("g2_flg_training",g2_flg_training);
	call symputx("g3_flg_training",g3_flg_training);
	call symputx("g4_flg_training",g4_flg_training);
	format dt_now datetime19. date_now date9. g_limit: g1_limit: g1_limit: g2_limit: g3_limit: g4_limit: datetime19.;
run;


%macro dataset_training();

	%put --- &=g_flg_training &=g1_flg_training &=g2_flg_training &=g3_flg_training &=g4_flg_training;
	%if "&modello_totrain" ne "" %then %do;
		
		%put --- SONO NELLA FINESTRA DI TRAINING DEL MODELLO AD ORIZZONTE DI PREVISIONE &modello_totrain ---;
		%include utl;
		%include fcn;

		%if (&hour_now eq &hour_limit) %then %do;
			%put --- SONO LE ORE &hour_now: CREO IL DATASET DI TRAINING;
			%include dat; 
			%if &g_flg_training = 1 %then %do; 
/*				%include trnG;*/
			%end;
			%else %do; 
 				%include trnG1; 
			%end;

		%end;

		%else %do;
			%put --- SONO LE ORE &hour_now: NON CREO IL DATASET DI TRAINING;
			%put --- CREO MODELLI PER IL &modello_totrain;
			%if &g_flg_training = 1 %then %do; 
/*				%include trnG;*/
			%end;
			%else %do; 
 				%include trnG1; 
			%end;
		%end;

	%end;
	%else %do;
		%put --- NON SONO NELLA FINESTRA DI TRAINING ---;
		%put --- NON ESEGUO IL PROCESSO DI TRAINING ---;
	%end;


%mend dataset_training; 

%dataset_training();    
  
data time_execution; 
	dt_start = &start; 
	dt_end = datetime(); 
	time_execution = dt_end - dt_start; 
	format dt: datetime19.; 
	call symputx('time_execution', time_execution); 
run; 
  
%put "========================================================================"; 
%put "IL CODICE SAS E' GIUNTO AL TERMINE IN: &time_execution. SECONDI"; 
  
 cas mySession terminate;  
