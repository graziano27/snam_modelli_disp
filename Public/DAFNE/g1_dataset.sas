/*****************************Create dataset components, merge them and drop duplicates***************************/

ods exclude all;
options cpucount=2 mprint mlogic symbolgen;
options compress=yes;
%let pfx =;

/*Set current date*/
/* %let start = %sysfunc(datetime()); */
%let dt_solare = &start;

data _null_;
	data_inizio=put(&dt_solare,datetime19.);
	call symput("data_ora",data_ora);
run;

%put ------- START &data_ora;


/*Libraries*/
%let libreria_output=&lib_physical;  
%let libreria_memory=&lib_memory;

/* Global macro variables */
%global dataset_presente;
%global input_bilanciosnam;
%global input_bilanciosnam1;
%global input_bilancioSCADA;
%global input_dlp_orario;
%global input_pcs_misura;
%global input_meteo_celle_cons;
%global input_meteo_celle_prev;
%global input_riconsegnato1;
%global input_riconsegnato2;
%global input_meteo_citta_prev;
%global input_remi_anagrafica;
%global end_date;
%global start_date;
%global cut_date;
%global input_calendario;
%global RicTot_base;
%global RicTot_removed;
%global RicTot_dupp;
%global RicTot_cut;
%global flg_anomalie;
%global output_final;


/* CALENDARIO */
%let input_calendario=work.calendario_orario_final;
%let input_calendario_G=&libreria_output..calendario_orario_G;
%let input_calendario_G1=&libreria_output..calendario_orario_G1;
%let input_calendario_G2=&libreria_output..calendario_orario_G2;
%let input_calendario_G3=&libreria_output..calendario_orario_G3;
%let input_calendario_G4=&libreria_output..calendario_orario_G4;
%let input_calendario_tot=&libreria_output..calendario_orario_tot;


/* BIL, RTOT, LAG */
%let input_bilanciosnam1=webdips.prev_bilancio;
%let input_bilanciosnam=work.bilanciosnam_final;
%let input_dlp_orario=oralpbp2.analisi_dlp_orario;
%let input_pcs_misura=webdips.pcs_misura_aop9100;
%let input_riconsegnato1=oralpbp2.riconsegnato;
%let input_riconsegnato2=oralpbp2.consuntivo_view;
%let input_termo=oralpbp2.d_e_up_new;
%let input_sime2=oralpbp2.d_e_misure_sime_2;
%let input_sime=oralpbp2.d_e_misure_sime;
%let input_vol_val=oralpbp2.d_e_volumi_validati;
%let input_up=ORALPBP2.D_E_UP;
%let input_remi_termo=ORALPBP2.D_E_UP_NEW;


%let output_rtot_lag_mvg=&libreria_output..output_rtot_lag_mvg_&keyprocess.;
%let output_bil_lag_mvg=&libreria_output..output_bil_lag_mvg_&keyprocess.;
%let output_lagsim=&libreria_output..output_lagsim_&keyprocess.;
%let output_lagsim_G1=&libreria_output..output_lagsim_G1_&keyprocess.;
%let output_rall=&libreria_output..output_rall_&keyprocess.;
%let output_rdif=&libreria_output..output_rdif_&keyprocess.;

/* SCADA */
%let input_bilancioscada= oralpbp2.analisi_bilancio;
%let output_scada=&libreria_output..output_scada_base_&keyprocess.;
%let output_scada_g =&libreria_output..output_scada_g_&keyprocess.;
%let output_scada_g1 = &libreria_output..output_scada_g1_&keyprocess.;
%let output_scada_g2 = &libreria_output..output_scada_g2_&keyprocess.;
%let output_scada_g3 = &libreria_output..output_scada_g3_&keyprocess.;
%let output_scada_g4 = &libreria_output..output_scada_g4_&keyprocess.;
%let output_scada_tot=&libreria_output..output_scada_tot_&keyprocess.;
/*%let scada_vars=scada_bil_ric scada_bil_exp scada_bil_imp scada_bil_stc scada_bil_pnl scada_bil_dlp;*/

/* SCADACONS */
%let anagrafica= WEBDIPS.ANAGRAFICA_PORTATE_SCADA;
%let valori=ORALPBP2.MISURATORI_SCADA;
%let output_scadacons_G=&libreria_output..output_scadacons_G_&keyprocess.;
%let output_scadacons=&libreria_output..output_scadacons_&keyprocess.;
%let output_scadacons_tot=&libreria_output..output_scadacons_tot_&keyprocess.;

/* FLUSSIFISICI */
%let output_scada_ff=&libreria_output..input_ff_scada_orario_G1_&keyprocess.;

/* METEO CITTA */
%let meteo_citta=oralpbp2.d_e_meteo_calc;
%let groups_meteo_citta=&libreria_output..grouping_meteo_citta;
%let output_citta=&libreria_output..output_meteo_urb_&keyprocess.; 
%let output_meteo_citta_prev=&libreria_output..input_meteoc_psol_G1_&keyprocess.;
%let output_meteo_citta_prev_new=&libreria_output..input_meteoc_pgas_G1_&keyprocess.;
%let output_meteo_citta_prevnew2=&libreria_output..meteo_citta_prevg2_Trend_&keyprocess.;
%let output_meteo_citta_prevnew3=&libreria_output..meteo_citta_prevg3_Trend_&keyprocess.;
%let output_meteo_citta_prevnew4=&libreria_output..meteo_citta_prevg4_Trend_&keyprocess.;
/*%let output_meteo_citta_prev_ut=&libreria_output..meteo_citta_prev_UT;*/
/*%let output_meteo_citta_prev_new_ut=&libreria_output..meteo_citta_prevgas_UT;*/
%let output_citta_tot = &libreria_output..output_meteo_citta_tot_&keyprocess.;


/* METEO CELLE */
%let meteo_celle_cons=oralpbp2.D_E_METEO_CONSUNTIVO;
%let meteo_celle_prev=oralpbp2.D_E_DATI_METEO_AREE;
%let grouping_all=&libreria_output..grouping_celle_full;
%let grouping_t_med=&libreria_output..grouping_celle_t_med;
%let grouping_t_gg=&libreria_output..grouping_celle_t_gg;
%let grouping_umidita=&libreria_output..grouping_celle_umid;
%let grouping_altezza_min=&libreria_output..grouping_celle_alt;
%let grouping_t_max_perc=&libreria_output..grouping_celle_tmaxp;
%let output_celle=&libreria_output..output_meteo_celle_&keyprocess.;
%let input_meteo_celle_cons=ORALPBP2.D_E_METEO_CONSUNTIVO;
%let input_meteo_celle_prev=oralpbp2.D_E_DATI_METEO_AREE;
%let output_meteo_celle_prev1=&libreria_output..input_meteo_prev_G1_&keyprocess.; /*same output for UT */
%let output_meteo_celle_prev2=&libreria_output..input_meteo_prev2_Trend_&keyprocess.;
%let output_meteo_celle_cons1=&libreria_output..input_meteo_consuntivo_G1_&keyprocess.; /*same output for UT */
%let output_meteo_celle_cons2=&libreria_output..input_meteo_consold_Trend_&keyprocess.;
%let output_meteo_celle_cons_new2=&libreria_output..input_meteo_consnew_Trend_&keyprocess.;
%let output_celle_tot = &libreria_output..output_meteo_celle_tot_&keyprocess.;


/* TERNA */
%let input_terna=ORALPBP2.D_E_TERNA;
%let input_termo=ORALPBP2.D_E_UP_NEW;
%let output_consuntivo_terna=&libreria_output..output_terna_cons_&keyprocess.;
%let output_previsione_terna=&libreria_output..output_terna_prev_&keyprocess.;
%let output_consuntivo_terna_G1 = &libreria_output..input_terna_cons_G1_&keyprocess.;
%let output_previsione_terna_G1 = &libreria_output..input_terna_prev_G1_&keyprocess.;
%let output_consuntivo_terna_Trend = &libreria_output..consuntivo_all_Trend_&keyprocess.;
%let output_consuntivo_terna_Ut = &libreria_output..terna_consuntivo_UT_&keyprocess.;
%let output_previsione_terna_Ut = &libreria_output..terna_previsione_UT_&keyprocess.;



/* ANOMALIE */
%let output_anomalie_cons = &libreria_output..anomalie_scada_cons;
%let output_anomalie_terna=&libreria_output..anomalie_terna;
%let output_anomalie_bilancio=&libreria_output..anomalie_bilancio;
%let output_anomalie_scada=&libreria_output..anomalie_scada_bil;
%let output_anomalie_all=&libreria_output..anomalie_orarie;


/* DS TOT */
%let output_final=&libreria_output..&output_final_name;
%let RicTot_base=&output_final;
%let RicTot_removed=&libreria_output..&output_final_name._prov;
%let RicTot_dupp=&libreria_output..&output_final_name._dup;
%let RicTot_cut=&libreria_output..&output_final_name._cut;
%let datetime_var_dup=datetime_solare;

/*Set macro variables for DATASET creation*/
%let flg_anomalie = 0;
/* %let dataset_presente = 1;  */


/*Define useful dates*/
data init_dates;
	g_solare=Datepart(&dt_solare);
	g_gas=ifn(hour(&dt_solare)<6, intnx("day", g_solare, -1), g_solare);
	g1_solare=intnx("day",g_solare,1);
	g1_gas=intnx("day", g_gas,1);
	call symput("g_solare",g_solare);
	call symput("g_gas",g_gas);
	call symput("g1_solare",g1_solare);
	call symput("g1_gas",g1_gas);
	format g: date9.;
run;

/* Create dataset bilancio */
	data bilancio_lim;
		if day(&g_gas)>10 then
			filter=intnx("month", &g_gas,-2, "end");
		else filter=intnx("month", &g_gas,-3, "end");
		call symput("bilancio_limit", filter);
		format filter date9.;
	run;
	
	data &input_bilanciosnam;
		set &input_bilanciosnam1(where=(fl_deleted eq "0" ));
		drop k_storico data_elaborazione fl_deleted k_prev_bilancio fl_azione utente_azione dt_azione data;
		date=datepart(data);
		format date date9.;
	
		if date<=&bilancio_limit then
			validato=1;
		else validato=0;
	
		if validato eq . then
			validato = 0;
	run;
	
	proc sort data=&input_bilanciosnam out=&input_bilanciosnam;
		by date;
	run;

%macro create_dataset_components(start_date);
	
	/* Create G-calendario orario*/
	data &input_calendario(where=(data_gas_rif<=&g_gas and data_gas_rif>=&start_date));
		date=&start_date;

		do while (date<=&g1_solare);
			do hour=0 to 23;
				datetime_solare=dhms(date,hour,0,0);

				if hour<6 then
					data_gas_rif=intnx("day", date, -1);
				else data_gas_rif=date;
				output;
			end;

			date=intnx("day", date, 1, 's');
		end;

		format date date9.;
		format data_gas_rif date9.;
		format datetime_solare datetime19.;
	run;

	%put_run_date_init(create_scada);
	%create_scada(&input_bilancioscada, &start_date, &output_scada_tot);
	%put_run_time(create_scada);

	%put_run_date_init(create_scadacons);
	%create_scadacons(&anagrafica, &valori, &start_date, &output_scadacons_tot, &dt_solare);
	%put_run_time(create_scadacons);

	%put_run_date_init(create_target_rtot);
	%create_target_rtot(&input_bilanciosnam, &input_dlp_orario, 
					&input_pcs_misura, &input_riconsegnato1, &input_riconsegnato2, 
					&output_scada, output_rtot, &g_gas, &start_date);
	%put_run_time(create_target_rtot);
	
	%put_run_date_init(create_tgt_rimanente);
	%create_tgt_rimanente(output_rtot, &output_scada, &output_rdif);
	%put_run_time(create_tgt_rimanente);

	%put_run_date_init(create_lagsimile);
	%create_lagsimile(output_rtot, &input_calendario, &output_lagsim);
	%put_run_time(create_lagsimile);

	%put_run_date_init(create_lagsimile_G1);
	%create_lagsimile_G1(output_rtot, &input_calendario, &output_lagsim_G1);
	%put_run_time(create_lagsimile_G1);
	
	%put_run_date_init(create_rtot_lag_mvg);
	%create_rtot_lag_mvg(output_rtot, &output_lagsim, &start_date, &output_rtot_lag_mvg);
	%put_run_time(create_rtot_lag_mvg);
	
	%put_run_date_init(create_target_termo);
	%create_target_termo(&start_date, &input_termo, &input_sime2, &input_sime, &input_vol_val, rcomp_termo_parziale);
	%put_run_time(create_target_termo);

	%put_run_date_init(create_bil_lag_mvg);
	%create_bil_lag_mvg(&input_bilanciosnam, output_rtot, rcomp_termo_parziale, &output_rall, 
					&input_calendario, &output_bil_lag_mvg, &start_date);
	%put_run_time(create_bil_lag_mvg);

	%put_run_date_init(create_flussifisici);
	%create_flussifisici(&start_date, &output_scada_ff);
	%put_run_time(create_flussifisici);

	%put_run_date_init(create_meteocitta);
	%create_meteocitta(&meteo_citta, &start_date, &output_citta_tot);
	%put_run_time(create_meteocitta);

	%put_run_date_init(create_meteocelle);
	%create_meteocelle(&meteo_celle_cons, &meteo_celle_prev, &start_date, &output_celle_tot);
	%put_run_time(create_meteocelle);
	
	%put_run_date_init(create_terna);
	%create_terna(&input_terna,&input_up, &input_remi_termo, &start_date, &output_consuntivo_terna, &output_previsione_terna);
	%put_run_time(create_terna);
/* 	proc sql noprint; */
/* 		select distinct memname into :table_list separated by ' ' */
/* 			from dictionary.columns */
/* 				where libname="WORK" and index(lowcase(memname), "output") =0; */
/* 	quit; */
/*  */
/* 	proc datasets lib=work nolist; */
/* 		delete &table_list; */
/* 	run; */

%mend create_dataset_components;


%macro merge_dataset_components(cut_date);


	data _null_;
		if hour(&dt_solare)<14 and hour(&dt_solare)>=6 then
			call symputx("end_date_merge", intnx("day", &G_gas, -1));
		else call symputx("end_date_merge", &G_gas);
	run;

/* 	data &input_calendario(where=(data_gas_rif<&end_date_merge)); */
/* 		data_solare_rif=&start_date; */
/*  */
/* 		do while (data_solare_rif<=&G1_solare); */
/* 			do hour=0 to 23; */
/* 				datetime_solare=dhms(data_solare_rif,hour,0,0); */
/*  */
/* 				if hour<6 then */
/* 					data_gas_rif=intnx("day", data_solare_rif, -1); */
/* 				else data_gas_rif=data_solare_rif; */
/* 				output; */
/* 			end; */
/*  */
/* 			data_solare_rif=intnx("day", data_solare_rif, 1, 's'); */
/* 		end; */
/*  */
/* 		format data_solare_rif date9.; */
/* 		format data_gas_rif date9.; */
/* 		format datetime_solare datetime16.; */
/* 	run; */
	
	data &input_calendario(where=(data_gas_rif<&end_date_merge));
	set &input_calendario;
	run;
	/*create calendar vars for all models*/
	%create_all_calendar_vars(&input_calendario,&input_calendario_tot);

	/*merge scada*/
	data scada_merge;
		set &output_scada_tot;
		drop data_gas_rif giorno_gas_scada;
		rename datetime_prev=datetime_solare;
	run;

	/*merge scadacons*/
	data scadacons_merge;
		set &output_scadacons_tot;
		rename datetime_calcolo=datetime_solare;
	run;
	
	/*merge flussifisici*/
	data ff_merge;
		set &output_scada_ff(drop=data_gas_g hour);
		rename datetime_solare_g = datetime_solare;
	run;

	/*merge meteocitta*/
	data meteocitta_merge(drop= hour data_gas_rif data_solare_rif);
		set &output_citta_tot;
	run;

	/*merge meteocelle*/
	data meteocelle_merge;
		set &output_celle_tot;
		drop data: hour;
	run;

	/*merge LAG e MVG BIL*/
	data billagmvg_merge1;
		set &output_bil_lag_mvg(drop=data_gas G_data_gas_rif date hour);
	run;

	/*merge LOG E MVG TARGET*/
	data billagmvg_merge2;
		set &output_rtot_lag_mvg(drop=data_gas /*data_gas_rif date*/ hour);
	run;

	/*merge LAG SIM*/
	data LAGSIM_merge(drop=data_gas data_gas_rif date hour);
		set &output_lagsim;
	run;

	/*merge LAG SIM G1*/
	data LAGSIM_merge_G1(drop=data_gas date hour);
		set &output_lagsim_G1;
	run;

	/*terna CONS*/
	data terna_cons_merge(drop= terna_consuntivo hour data_gas data_gas_rif date hour);
		set &output_consuntivo_terna;
		rename dt_calcolo=datetime_solare;
	run;

	/*terna prev*/
	data terna_prev_merge(drop=hour data_gas data_gas_rif date hour);
		set &output_previsione_terna;
		rename dt_calcolo=datetime_solare;
	run;

	/*rall */
	data rall_merge;
		set &output_rall(drop= hour data_gas G_data_gas_rif G1_data_gas_rif G2_data_gas_rif G3_data_gas_rif G4_data_gas_rif);
	run;
	
	/*rdiff*/
	data rdiff_merge;
		set &output_rdif (rename=(datetime_solare_g=datetime_solare)
			drop=data_gas_rif /*data_gas Riconsegnato_Consuntivo_KWH*/);
	run;

	data merge_final;
		merge &input_calendario_tot(in=p)
		billagmvg_merge1
		billagmvg_merge2
		LAGSIM_merge
		LAGSIM_merge_G1
		terna_cons_merge
		terna_prev_merge
		scada_merge
		scadacons_merge
		ff_merge
		meteocitta_merge
		meteocelle_merge
		rall_merge
		rdiff_merge;
		by datetime_solare;
		if p;
	run;


/* 	%round_input_vars(merge_final); */

	%if &dataset_presente=1 %then
		%do;

			data &RicTot_cut(where=(data_gas_rif>=&cut_date));
				set merge_final;
			run;

			data scarto_add;
				length datetime_calcolo_train 8;
				datetime_calcolo_train=&dt_solare;
				set &RicTot_base(where=(data_gas_rif>=&cut_date));
				format datetime_calcolo_train datetime19.;
			run;

			data &libreria_output..scarto_add;
			set scarto_add;
			run;

			proc append base=&RicTot_removed data=scarto_add force;
			run;

			data &output_final;
				set &RicTot_base(where=(data_gas_rif<&cut_date)) &RicTot_cut;
			run;

		%end;
	%else %if &dataset_presente=0 %then
		%do;

			data &output_final;
				set merge_final;
			run;

		%end;
%mend merge_dataset_components;


%macro create_dataset();
	%checkds(&RicTot_base);

	/* se esiste giï¿½ devo accodare gli ultimi 3 mesi*/
	%if &dataset_presente=1 %then
		%do;
			data _null_;
				set &input_bilanciosnam;
				call symputx("start_date",intnx("month",max(date),-17,"B"));
			run;


			data _null_;
				set &input_bilanciosnam;
				call symputx("cut_date",intnx("month",max(date),-2,"B"));
			run;

		%end;

	/* se non esiste devo crearlo da zero */
	%else %if &dataset_presente=0 %then
		%do;
			%let start_date="03JAN2012"d;
			%let cut_date=&G_gas;
		%end;

	%create_dataset_components(&start_date);
	%merge_dataset_components(&cut_date);
%mend create_dataset;

%create_dataset();

%remove_dupp_tset(&output_final, &output_final, &RicTot_dupp, &datetime_var_dup);

%dset_phy2mem(&output_final_name, &libreria_output, &libreria_memory, &g_gas);

data _null_;
	data_ora=put(datetime(),datetime19.);
	call symput("data_ora",data_ora);
run;

%put ------- END &data_ora;

