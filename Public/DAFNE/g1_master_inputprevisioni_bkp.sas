/*============ General Options and Creation of CAS Session ========*/
options casdatalimit=ALL;
cas mySession sessopts=(caslib=casuser timeout=31536000 locale="en_US");
caslib _all_ assign;

options mprint mlogic symbolgen linesize=max;
ods exclude all;


/*============ Global Vars ========*/
/*============ Global Vars ========*/
%global start dataset_presente lib_physical lib_memory output_final_name trainingset ensemble_id;

%let start = %sysfunc(datetime());
%let pfx=;
%let lib_physical=for_svil;
%let lib_memory=public;
%let model_path=&lib_path;
%let keyprocess=in;
%let output_final_name=&pfx.g1_inputmodelli_viya;
%let ensemble_id=35 36 37 38 39 40 41 42 43 44 45 46 50 ; /* id da tenere nella tabella finale da disporre a SNAM */
%let trainingset=&lib_physical..&pfx.g1_trainingset_viya;

/*Naming codici fisici richiamati nel parallelismo (vedi funzione "parallel_function" in g1_functions)*/
data _null_;
	librerie_viya ="librerie_viya";
	g1_utils ="g1_utils";
	g1_functions ="g1_functions";
	g1_initpar ="g1_initpar";
	call symputx("librerie_viya",librerie_viya);
	call symputx("g1_utils",g1_utils);
	call symputx("g1_functions",g1_functions);
	call symputx("g1_initpar",g1_initpar);
run;

filename utl FILESRVC folderpath="&logic_path" filename='g1_utils.sas'; 
filename fcn FILESRVC folderpath="&logic_path" filename='g1_functions.sas';
filename inp FILESRVC folderpath="&logic_path" filename='g1_input.sas';
/* filename pr FILESRVC folderpath="&logic_path" filename='g_predict.sas'; */
/* filename ens FILESRVC folderpath="&logic_path" filename='g_ensemble.sas'; */
filename pr1 FILESRVC folderpath="&logic_path" filename='g1_previsioni.sas';
filename ens1 FILESRVC folderpath="&logic_path" filename='g1_ensemble.sas';
filename mrg FILESRVC folderpath="&logic_path" filename='g1_merge.sas';

%include utl;
%include fcn;
%include inp;
/* %include pr; */
/* %include ens; */
%include pr1;
%include ens1;
%include mrg;

/* %macro clean_all(); */
/*  */
/* 	proc datasets lib=work kill nolist memtype=data; */
/* 	quit; */
/*  */
/* 	Data MVars ( Keep = Name ); */
/* 		Set SasHelp.VMacro; */
/* 		Where Scope = 'GLOBAL'; */
/*  */
/* 		if find(Name, "SAS") = 0; */
/*  */
/* 		if find(Name, "SYS") = 0; */
/*  */
/* 		if find(Name, "CLIENT") = 0; */
/*  */
/* 		if find(Name, "SQL") = 0; */
/*  */
/* 		if find(Name, "EG") = 0; */
/*  */
/* 		if find(Name, "ORA") = 0; */
/*  */
/* 		IF NAME NOT IN ("TRAININGSET", "ENSEMBLE_ID", "LIB_PHYSICAL", "LIB_MEMORY", "LIB_PATH", */
/* 						"OUTPUT_FINAL_NAME","START", "INP", "FCN", "ENS", "PRD", "MRG", "PFX"); */
/* 	Run; */
/*  */
/* 	Data _Null_; */
/* 		Set MVars; */
/* 		Call Symdel( Name ); */
/* 	Run; */
/*  */
/* %mend clean_all; */
/*  */
/* %macro input_prediction(); */
/* 	%put '=================== INIZIO PREVISIONE ===================='; */
/*  */
/* 	%if %sysfunc(exist(&trainingset)) %then */
/* 		%do; */
/* 			%include utl; */
/* 			%include fcn; */
/* 			%include inp; */
/* 				%clean_all(); */
/* 			%include prd; */
/* 				%clean_all(); */
/* 			%include ens; */
/* 				%clean_all(); */
/* 			%include mrg; */
/* 				%clean_all(); */
/* 		%end; */
/*  */
/* %mend input_prediction; */
/*  */
/* %input_prediction(); */
/*  */
/* data time_execution; */
/* 	dt_start = &start; */
/* 	dt_end = datetime(); */
/* 	time_execution = dt_end - dt_start; */
/* 	format dt: datetime19.; */
/* 	call symputx('time_execution', time_execution); */
/* run; */
/*  */
/* %put "========================================================================"; */
/* %put "IL CODICE SAS E' GIUNTO AL TERMINE IN: &time_execution. SECONDI"; */
/*  */
cas mySession terminate; 
