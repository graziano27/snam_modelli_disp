/*===============================================================*/
/***** rilascio 2020-09-XX - versione v1.0 *****/
/***** autori: Binda, Rizzolo, Gregori *****/
/***** BIP S.p.a. *****/
/*===============================================================*/

/*============ General Options and Creation of CAS Session ========*/
options mprint mlogic symbolgen linesize=max;
ods exclude all;
/* options nosymbolgen nomprint nomlogic; */

%global output_final_name;

%let start = %sysfunc(datetime());
%let start_fix = %sysfunc(datetime());
%let diff_end_month=-1;
%let pfx=;
%let lib_physical=for_svil;
%let lib_memory=public;

/* PER IL TEST */
/*%let output_final_name=&pfx.g1_trainingset_viya_test;*/
%let output_final_name=&pfx.g1_trainingset_viya; 

%let logic_path=/Public/DAFNE/; 
/*%let logic_path = /Public/Porting_deploy/prod/; */

%let lib_path=/opt/sas/viya/config/data/cas/default/dafne; 
/* %let lib_path = /sasdata/porting_viya; */

/* %let model_path = /sasdata/porting_viya/models; */
%let model_path=&lib_path;
/* libname for_svil "/opt/sas/viya/config/data/cas/default/dafne_bkp"; */

/*filename glib FILESRVC folderpath="&logic_path" filename='librerie_viya.sas';*/
/*filename utl FILESRVC folderpath="&logic_path" filename='g1_utils.sas'; */
/*filename fcn FILESRVC folderpath="&logic_path" filename='g1_functions.sas';*/
/*filename gtrG1  FILESRVC folderpath="&logic_path" filename='g1_training.sas';*/

%let librerie_viya=librerie_viya;
%let g1_utils=g1_utils;
%let g1_functions=g1_functions;
%let g1_training=g1_training;

options casdatalimit=ALL;
cas mySession sessopts=(caslib=casuser timeout=31536000 locale="en_US");
caslib _all_ assign; 



data check;
	hour_train=hour(&start);
	dt_train=intnx("hour",&start,1);	
	data_gas_train = ifn(hour_train<6, intnx('day', datepart(dt_train), -1), datepart(dt_train));
	date_pred = data_gas_train;
	mese_pred = month(date_pred);
	anno_pred = year(date_pred);
	call symputx("hour_train", hour_train);
	call symputx("mese_pred_r", compress(put(mese_pred, z2.0)));
	call symputx("anno_pred_r", compress(anno_pred));
	format da: date9.;
	format dt: datetime19.;
run;


%macro parallel_function(server_name, data_family);
	signon &server_name.;

	%syslput lib_path = &lib_path.;
	%syslput logic_path = &logic_path.;
	%syslput model_path = &model_path.;
	%syslput OUTPUT_FINAL_NAME = &OUTPUT_FINAL_NAME.;
	%syslput PFX = &PFX.;
	
/*	%syslput DATASET_PRESENTE = &DATASET_PRESENTE.;*/

	%syslput lib_physical = &lib_physical.;
	%syslput lib_memory = &lib_memory.;
	%syslput start = &start.;
	%syslput start_fix = &start_fix.;
	%syslput hour_train = &hour_train.;
	%syslput mese_pred_r = &mese_pred_r.;
	%syslput anno_pred_r = &anno_pred_r.;
	%syslput server_name = &server_name.;
	%syslput data_family = &data_family.;
	%syslput librerie_viya = &librerie_viya.;
	%syslput g1_utils = &g1_utils.;
	%syslput g1_functions = &g1_functions.;
	%syslput g1_training = &g1_training.;
rsubmit &server_name. wait=no;

	options casdatalimit=ALL;
	cas myS_&server_name. sessopts=(caslib=casuser timeout=31536000 locale="en_US");
	caslib _all_ assign; 

	%include "&lib_path./&librerie_viya..sas";
	%include "&lib_path./&g1_utils..sas";
	%include "&lib_path./&g1_functions..sas";

	%macro clean_current_models();
		filename myDir "&model_path./";

		data get;
			did=dopen("myDir");
			filecount=dnum(did);

			do i = 1 to filecount;
				file_ = dread(did, i);
				output;
			end;

			rc=dclose(did);
		run;

		data to_delete;
			set get;
			where file_ contains "&anno_pred_r._&mese_pred_r." 
				and file_ contains "&modello_totrain";
			fname="tempfile";
			rc=filename(fname, cat("&model_path./", compress(file_)));
			rc=fdelete(fname);
		run;

	%mend;

	%macro clean_all();

		proc datasets lib=work kill nolist memtype=data;
		quit;
	%mend clean_all;

	%macro increase_start(dt_start=);

		data start;
			start = intnx("hour", &dt_start, 1);
			call symputx("start", start);
			format start datetime19.;
			put start;
		run;

	%mend;

	%macro Emergenza_Training(modello_totrain);
		%clean_current_models();
		%put start doing modello &modello_totrain;
		%let start = &start_fix;

		/*		%include glib;*/
		/*		%include utl;*/
		/*		%include fcn;*/
		%do HOUR_TRAIN_CYCLE = %sysfunc(hour(&start.)) %to 23;
		%if &HOUR_TRAIN_CYCLE = 12 %then
			%do;
			%include "&lib_path./&g1_training..sas";
			%clean_all();
			%increase_start(dt_start=&start.);
			%end;
			%else %do;
			%increase_start(dt_start=&start.);

		%end;
		%end;

		data end;
			hour_end = hour(&start_fix) - 1;
			call symputx("hour_end", hour_end);
		run;

		%do HOUR_TRAIN_CYCLE = 0 %to &hour_end;
			%if &HOUR_TRAIN_CYCLE = 12 %then
			%do;
			%include "&lib_path./&g1_training..sas";
			%clean_all();
			%increase_start(dt_start=dhms(datepart(&start_fix), &HOUR_TRAIN_CYCLE, 0, 0));
			%end;
			
			%else %do;
					%increase_start(dt_start=&start.);

			%end;
		%end;

		%put end modello &modello_totrain;
	%mend Emergenza_Training;

	%macro run_parallel_session_emergenza(server_name, data_family);
		%put --- PROCESSING &data_family ---;

		%if "&server_name."="server1" %then
			%do;
				%Emergenza_Training(G1);
			%end;
		%else %if "&server_name."="server2" %then
			%do;
				%Emergenza_Training(G2);
				%Emergenza_Training(G3);
				%Emergenza_Training(G4);
			%end;
	%mend run_parallel_session_emergenza;

	%run_parallel_session_emergenza(&server_name., &data_family.);
	endrsubmit;
%mend parallel_function;


%parallel_function(server1, G1);
%parallel_function(server2, G2-G3-G4);

/* FINE PARALLELISMO */
/* Wait results from all server */
waitfor server1 server2 ;

/* signoff from all server */
signoff server1;
signoff server2;

data time_execution;
	dt_start = &start_fix;
	dt_end = datetime();
	time_execution = dt_end - dt_start;
	format dt: datetime19.;
	call symputx('time_execution', time_execution);
run;

%put "========================================================================";
%put "IL CODICE SAS E' GIUNTO AL TERMINE IN: &time_execution. SECONDI";

cas mySession terminate;

