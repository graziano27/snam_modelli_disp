/*===============================================================*/
/***** rilascio 2020-01-XX - versione v1.0 *****/
/***** autori: Binda, Oliva, Rizzolo *****/
/***** BIP S.p.a. *****/
/*===============================================================*/

/*============ Allocazione Libreria FOR_SVIL ===================*/
/* libname for_svil "/opt/sas/viya/config/data/cas/default/new_dir"; */
/* libname for_svil "/opt/sas/viya/config/data/cas/default/dafne_bkp"; */
%let model_path=&lib_path;
%let ext = .txt;
/* %let start = %sysfunc(datetime()); */

/*============ Opzioni Generali ================================*/
/* options nomlogic nomprint nosymbolgen; */
/* options mlogic mprint symbolgen msglevel=i; */
/* ods exclude all; */
/*ods exclude none;*/

/*============ Macro variabili =================================*/
/* %global lib_input lib_output time_id target_id tgt; */
%global last_date mese_val mese_pred anno_pred;
%global input_dset sim_dset input_grid1;
%global algo_nn num_epochs;
%global drop_vars drop_lagged_var tipo_modello max_date;
%global output_varsel neural_model neural_model_old;

%let lib_input=&lib_physical;
%let lib_input_inm=&lib_memory;
%let lib_output=&lib_physical;
%let time_id=data_gas_rif;
%let output_varsel=varsel;
%let input_dset=&g_output_final_name;
%let sim_dset=casuser.simulation_dataset;
%let grid=configurazioni;
%let algo_nn=HF;
%let zip_path = &model_path;
%let ds_path = &model_path;

data time;
	dt_train=intnx("hour",&start,1);
	hour_train=hour(dt_train);
	data_gas_train = ifn(hour_train<6, intnx('day', datepart(dt_train), -1), datepart(dt_train));
	date_pred_g = data_gas_train;
	mese_pred_g = month(date_pred_g);
	anno_pred_g = year(date_pred_g);
	mese_prec_g = month(intnx('month',date_pred_g, -1));
	anno_prec_g = year(intnx('month',date_pred_g, -1));
	anno_prec2_g = year(intnx('month',date_pred_g, -2));
	mese_prec2_g = month(intnx('month',date_pred_g, -2));
	call symputx("hour_pred_g", put(hour_train, z2.0));
	call symputx("mese_pred_g", put(mese_pred_g, z2.0));
	call symputx("anno_pred_g", anno_pred_g);
	call symputx("date_pred_g", date_pred_g);
	call symputx("mese_prec_g", put(mese_prec_g, z2.0)); 
	call symputx("anno_prec_g", anno_prec_g);
	call symputx("mese_prec2_g", put(mese_prec2_g, z2.0));
	call symputx("anno_prec2_g", anno_prec2_g);
	format da: date9.;
	format dt: datetime19.;
run;

/* ============ Pulizia Libreria casuser e work ================= */
proc datasets lib=casuser kill nolist;
quit;

proc datasets lib=work kill nolist;
quit;

/*============ Compilazione funzioni =================*/
%macro do_nnet_1m(training_set=,
				  int_var=, class_var=, 
				  algo=, regl1_init=, regl2_init=, layer_string_init=,
				  model_out=);
	
	proc sort data=&training_set out=work.temp;
		by &time_id hour;
	run;
		
	data &training_set;
		set work.temp;
	run;

	proc sql noprint;
		select max(&time_id) into :date_intern
		from &training_set;
	quit;
	
	proc sql noprint;
		create table valid_intern as 
		select * 
		from &training_set
		where &time_id>= intnx("month",&date_intern,0,'B')
		order by &time_id, hour;
	quit;
	
	data public.valid_intern;
		set valid_intern;
	run;
	
	proc nnet data=&training_set standardize=midrange missing=mean nthreads=16;
		&class_var;
		input &int_var / level=int;
		architecture mlp;
		target &target_id / level=int;
		%if %sysfunc(lowcase("&target_id"))="tgt_riconsegnato_totale" %then %do;
			train validation=public.valid_intern outmodel=casuser.&model_out. seed=12345;
		%end;
		%else %do;
			train outmodel=casuser.&model_out. seed=12345;
		%end;
		optimization algorithm=&algo maxiter=&num_epochs REGL1=&regl1_init REGL2=&regl2_init;
		&layer_string_init;
		code file="&model_path./&model_out.&ext";
	run;

%mend do_nnet_1m;

%macro clean_models(model=);

	filename myDir "&model_path./";

	data get;
		did=dopen("myDir");
		filecount=dnum(did);
		do i = 1 to filecount;
			file_ = dread(did, i);
			output;
		end;
		rc=dclose(did);
	run;

	data to_delete;
		set get;
		where file_ contains "&model";
		fname="tempfile";
		rc=filename(fname, cat("&model_path./", compress(file_)));
		rc=fdelete(fname);
	run;

%mend;

%macro archiviazione(old_rete);
	
	filename glob "&ds_path./&old_rete";
	filename addfile zip "&zip_path./ds.zip" 
		member="models_g/&old_rete";

	data _null_;
		infile glob recfm=n;
		file addfile recfm=n;
		input byte $char1. @;
		put  byte $char1. @;
	run;

	filename addfile clear;

%mend;

%macro archivia_modelli(model_string=);

	%do previous = 1 %to 2;
	
	data name_model;
		if &previous = 1 then 
			do;
				neural_model_old = compress(catx("_","&model_string","&anno_prec_g", 
				"&mese_prec_g", "&key_cluster_hour.&ext"));
				call symputx("neural_model_old", neural_model_old);
			end;
		else if &previous = 2 then 
			do;
				neural_model_old = compress(catx("_","&model_string","&anno_prec2_g", 
				"&mese_prec2_g", "&key_cluster_hour.&ext"));
				call symputx("neural_model_old", neural_model_old);
			end;
	run;

	%if %sysfunc(fileexist("&model_path./&neural_model_old.")) %then
		%do;
			%archiviazione(&neural_model_old);
			%clean_models(model=&neural_model_old)
		%end;

	%end;
	
%mend;

%macro drop_lag_vars(dsin=, dsout=);

	proc contents data=&dsin (keep=rtot_lag: bil_lag: terna:) 
			out=var_list (keep=name) noprint;
	run;

	/* Lag da tenere (A) */
	proc sql noprint;
		create table recent_varlagged_list as select name from var_list where 
			prxmatch("/lag[a-z_A-Z]*(?:1$|2$|6$|7$|14$|21$|28$|364$|365$|366$|simile)/", 
			trim(name));
	quit;

	proc sql noprint;
		create table all_varlagged_list as select name from var_list where 
			prxmatch("/lag[a-z_A-Z]/", trim(name));
	quit;

	/* Lag da tenere (B)
	Tengo i lag derivanti dall'analisi spettrale sulle componenti
	del bilancio commerciale */
	data lag_spectra;
		length var $40 lag 8;
		var="altro_c_p_gnc";lag=7;output;
		var="altro_c_p_gnc_termo";lag=122;output;
		var="altro_c_p_gnc_termo";lag=183;output;
		var="altro_c_p_gnc_termo";lag=365;output;
		var="termoelettrico";lag=4;output;
		var="termoelettrico";lag=7;output;
		var="termoelettrico";lag=365;output;
		var="dlp";lag=4;output;
		var="dlp";lag=5;output;
		var="dlp";lag=7;output;
		var="dlp";lag=8;output;
		var="entrate";lag=7;output;
		var="entrate";lag=183;output;
		var="entrate";lag=365;output;
		var="gnl";lag=69;output;
		var="gnl";lag=219;output;
		var="gnl";lag=274;output;
		var="gnl";lag=365;output;
		var="importazioni";lag=7;output;
		var="importazioni";lag=183;output;
		var="industriale";lag=7;output;
		var="industriale";lag=73;output;
		var="industriale";lag=122;output;
		var="industriale";lag=365;output;
		var="produzione_naz";lag=183;output;
		var="produzione_naz";lag=274;output;
		var="produzione_naz";lag=365;output;
		var="reti_distrib";lag=91;output;
		var="reti_distrib";lag=100;output;
		var="reti_distrib";lag=137;output;
		var="reti_distrib";lag=183;output;
		var="reti_distrib";lag=365;output;
		var="ric_distrib_terzi";lag=91;output;
		var="ric_distrib_terzi";lag=100;output;
		var="ric_distrib_terzi";lag=137;output;
		var="ric_distrib_terzi";lag=183;output;
		var="ric_distrib_terzi";lag=365;output;
		var="ric_reti_terzi";lag=4;output;
		var="ric_reti_terzi";lag=7;output;
		var="ric_reti_terzi";lag=183;output;
		var="ric_reti_terzi";lag=365;output;
		var="stoccaggio";lag=7;output;
		var="stoccaggio";lag=122;output;
		var="stoccaggio";lag=137;output;
		var="stoccaggio";lag=183;output;
		var="stoccaggio";lag=365;output;
		var="totale_immesso";lag=4;output;
		var="totale_immesso";lag=7;output;
		var="totale_immesso";lag=183;output;
		var="totale_immesso";lag=365;output;
		var="uscite";lag=78;output;
		var="uscite";lag=84;output;
		var="uscite";lag=100;output;
		var="uscite";lag=183;output;
		var="uscite";lag=365;output;
	run;

	data lag_spectra (keep=name);
		set lag_spectra;
		length name $32.;
		name=compress(cat("bil_lag_", var, "_", lag));
	run;

	/* Seleziono tutti i lag tranne i lag da tenere (A) e (B) + rtot_lag_simile */
	proc sql noprint;
		create table drop_varlagged_list as select * from all_varlagged_list where 
			name not in (select name from recent_varlagged_list) and name not in (select 
			name from lag_spectra) and lowcase(name) ne "rtot_lag_simile";
		run;

	data _null_;
		set drop_varlagged_list end=eof;
		length all_vars $20000;
		retain all_vars;

		if _N_=1 then
			all_vars=name;
		else
			do;
				all_vars=catx(" ", all_vars, name);
			end;

		if eof then
			do;
				call symputx("drop_lagged_var", all_vars);
			end;
	run;

	data &dsout;
		set &dsin;
		drop &drop_lagged_var;
	run;

%mend drop_lag_vars;



%macro get_months_train(mese, train_width, dset_string);

	data &dset_string(keep=string);
		ref_month=&mese;
		width=min(&train_width,6);
		lim_inf=IFN(ref_month-width<1,ref_month-width+12,ref_month-width);
		lim_sup=IFN(ref_month+width>12,ref_month+width-12,ref_month+width);

		if lim_inf<lim_sup then
			string=cat("if month(&time_id) >=", lim_inf, " and month(&time_id) <=", lim_sup);
		else string=cat("if month(&time_id) >=", lim_inf, " or month(&time_id) <=", lim_sup);
	run;

%mend;

%macro get_train_back(date_to=, horizon=, I=, input_dataset=, output_dataset=);

	data &output_dataset;
		set &input_dataset;
		if hour in &hour_cluster;
		lim_inf_train=intnx('month', &date_to, &I-&horizon+1);
		lim_sup_train=mdy(month(intnx('month', &date_to, &I, 'E')), 
			day(intnx('month', &date_to, &I, 'E'))-1, year(intnx('month', &date_to, &I, 'E')));
		format lim_inf_train lim_sup_train date9.;
		if lim_inf_train <=&time_id<=lim_sup_train;
	run;

%mend get_train_back;

%macro get_train_varsel_back(date_to=, horizon=, I=, input_dataset=, output_dataset=);

	data &output_dataset;
		set &input_dataset;
		if hour in &hour_cluster;
		lim_inf_train=intnx('month', &date_to, &I-&horizon+1);
		lim_sup_train=mdy(month(intnx('month', &date_to, &I, 'E')), 
			day(intnx('month', &date_to, &I, 'E'))-1, year(intnx('month', &date_to, &I, 
			'E')));
		format lim_inf_train lim_sup_train date9.;
		if lim_inf_train <=&time_id <=lim_sup_train;
	run;


%mend get_train_varsel_back;

%macro do_var_reduction(id=, training_varsel=, std=, tipo_modello=,tipo_approccio=, vars_reduce=, 
						out_varsel_table=, save_file=, flagme=, varexp=, maxeff=, horizon=, 
						force_vars_input=, force_time_input=,
						flg_forced_var=, hour=);

	/* NOTA: varexp=1 essenziale altrimenti non fa il maxef */

	%put "DOING VARREDUCE ALGORITHM";

	/* Decido se standardizzare o no il ds di partenza */
	%if (&std=1) %then %do;
		
		data training_varsel;
			set &training_varsel;
		run;
		
		proc stdize data=training_varsel out=casuser.training_varsel_final 
				method=range;
			var &stdize_vars;
		run;
	%end;

	%else %do;
		data casuser.training_varsel_final;
			set &training_varsel;
		run;
	%end;

	%if &flagme=1 %then %do;
			
			proc varreduce data=casuser.training_varsel_final technique=var;
				class time:;
				reduce supervised &target_id=&vars_reduce / maxeffects=&maxeff varexp=1;
				displayout SelectionSummary=var_exp_selection_eff;
			run;

	%end;
	%else %do;
			
		proc varreduce data=casuser.training_varsel_final technique=var;
				class time:;
				reduce supervised &target_id=&vars_reduce / varexp=&varexp;
				displayout SelectionSummary=var_exp_selection_eff;
		run;

	%end;

	/* Decido se aggiungere variabili forzatamente a quelle selezionate tramite varreduce */
	%if &flg_forced_var=1 %then %do;

	/* Aggiungo le variabili forzate (sia le time che non)*/
		%let count_fv=%sysfunc(countw(&force_vars_input));
		%let count_ft=%sysfunc(countw(&force_time_input));

		data var_exp_selection_eff_add(keep=variable level);
			length variable $32.;

			%do i_force=1 %to &count_fv;
				%let value=%scan(&force_vars_input, &i_force);
				variable="&value";
				level="-";
				output;
			%end;

			%do i_force=1 %to &count_ft;
				%let value=%scan(&force_time_input, &i_force);
				variable="&value";
				level="";
				output;
			%end;
		run;

		data casuser.var_exp_selection_eff;
			length variable $ 32;
			set casuser.var_exp_selection_eff var_exp_selection_eff_add;
		run;

	%end;

	/* Inserisco livello, modello, anno e mese di previsione, etc.. */
	data casuser.var_exp_selection_eff;
		length level $ 32;
		set casuser.var_exp_selection_eff;
		if find(variable, 'time', 'i') ge 1 then level='';
		else level="-";
		mese_pred="&mese_pred_g";
		anno_pred="&anno_pred_g";
		modello="&tipo_modello";
		hour="&hour";
		approccio="&tipo_approccio";
		target="&target_id";
		id=&id;
	run;

	data casuser.var_exp_selection_eff;
		set casuser.var_exp_selection_eff;
		variable=lowcase(variable);
	run;

	proc sort data=casuser.var_exp_selection_eff out=var_exp_selection_eff nodupkey;
		by variable;
	run;
	
	/* Salvo i risultati mettendoli in append in &lib_output */
	%if %sysfunc(exist(&save_file)) %then
		%do;

			proc append base=&save_file
						data=var_exp_selection_eff force;
			run;

		%end;
	%else
	%do;

		data &save_file;
			stop;
			length id 8. approccio $31 modello $10 target $24 variable $32 mese_pred $2 anno_pred $4 hour $100;
		run;

		proc append base=&save_file 
					data=var_exp_selection_eff force;
		run;

	%end;

	/* Dataset di output con variabili selezionate per anno, mese, modello in esame */
	data &out_varsel_table;
		set var_exp_selection_eff (keep=variable level);
	run;

%mend do_var_reduction;

%macro training_g(id, target_id, approccio, modello, hour_cluster, key_cluster_hour, drop_vars, vars_hpred, flag_std,
							stdize_vars, flg_forced_var,
							force_vars, force_time, flag_maxeff, this_try_maxeff, this_try_var_sel,
							horizon, season_width, regl1, regl2, layer_string);
	
	/*============ mese_pred anno_pred e mese_val ============*/
	proc sql noprint;
		select max(&time_id) into: max_date
		from &lib_input_inm..&input_dset (where = (&target_id ne .))
		;
	quit;

	data _null_;
		max_date=&max_date;
		call symput("last_date",cat("'",compress(put(max_date,date9.)),"'d"));
	run;

	data _null;	
		mese_val=month(intnx('month', &max_date, 1));
		call symput("mese_val",mese_val);
	run;

	/*============  Definisco parametro maxiter della nnet in base al target ============*/
	data _null_;
		%if %sysfunc(lowcase("&target_id"))="tgt_riconsegnato_totale" %then %do;
			call symput("num_epochs",compress(200));
		%end;
		%else %do;
			call symput("num_epochs",compress(50));
		%end;
	run;

	/*============ CHIAVI PER OUTPUT Tables ===================*/
	data _null_;
		length target_new $8;
		target="&target_id";
		if target = 'tgt_riconsegnato_totale' then target_new = 'TOT'; /* Totale */
		if target = 'tgt_riconsegnato_diff' then target_new = 'DIFF'; /* Per differenza */
		if target = 'tgt_civile' then target_new = 'CIV'; /* Per componenti */
		if target = 'tgt_termoelettrico_terna' then target_new = 'TERM'; /* Per componenti */		
		if target = 'tgt_industriale' then target_new = 'IND'; /* Per componenti */
		call symputx("tgt", target_new);
	run;

	data _null_;
		neural_model_prefix=upcase(compress(catx("_","NN","&approccio","&tgt","&modello","&anno_pred_g","&mese_pred_g","&key_cluster_hour")));
		model_base_string=upcase(compress(catx("_","NN","&approccio","&tgt","&modello")));
		call symputx("neural_model", neural_model_prefix);
		call symputx("model_base_string", model_base_string);		
	run;

	/*============ DESCRIZIONE ESTESA PER VALORI NELLA TAB DI OUTPUT VARSEL ===================*/
	data _null_;
		modello="&modello";
		approccio="&approccio";
		length tipo_modello $10 tipo_approccio $31;
		/* Creo tipo_modello */
		if lowcase(modello)="st" then tipo_modello="stagionale";
		else if lowcase(modello)="gl" then tipo_modello="globale";
		else if lowcase(modello)="si" then tipo_modello="singolo";
		/* Creo tipo_approccio */
		if lowcase(approccio)="g_01" then tipo_approccio="Approccio Target Totale";
		else if lowcase(approccio)="g_02" then tipo_approccio="Approccio Target per Differenza";
		else if lowcase(approccio)="g_03" then tipo_approccio="Approccio per Componenti";
		/* Definisco le macro variabili */
		call symput("tipo_modello",tipo_modello);
		call symput("tipo_approccio",tipo_approccio);
	run;

	/*===========================================================================================*/
	/* Controllo se esiste già il modello per il cluster di appartenenza dell'orario da prevedere */
	
	%if %sysfunc(fileexist("&model_path./&neural_model.&ext")) %then %do;

		%put ========= MODELLO GIA CREATO PER IL CLUSTER DI ORE &hour_cluster del target &tgt =========;
		%put ========= NON ESEGUO IL CODICE DI TRAINING PER IL TARGET &tgt delle ore &hour_pred_g =========;
		%archivia_modelli(model_string=&model_base_string);	
	%end;
	%else %do;

		%put ========= MODELLO NON ANCORA CREATO PER IL CLUSTER DI ORE &hour_cluster del target &tgt =========;
		%put ========= ESEGUO IL CODICE DI TRAINING PER IL TARGET &tgt delle ore &hour_pred_g =========;
		%put ========= RUNNING CODE ===========;
	

		/* Elimino alcuni lag del bil commerciale, target e terna */
		%drop_lag_vars(dsin=&lib_input_inm..&input_dset, dsout=&sim_dset);
		
		/* Elimino set di variabili basandosi sulla grid */
		data casuser.dataset1;
			set &sim_dset;
			drop &drop_vars;
		run;

		%get_train_back(date_to=&last_date, horizon=&horizon, I=0, 
			input_dataset=casuser.dataset1, output_dataset=casuser.training_eff);
		%get_train_varsel_back(date_to=&last_date, horizon=&horizon, I=0, 
			input_dataset=casuser.dataset1, output_dataset=casuser.training_var_sel);

		%put ========= DOING RETE &tipo_modello - &tipo_approccio =========;

		%get_months_train(&mese_val, &season_width, dset_string_temp);

		data dset_string_temp;
			set dset_string_temp;
			call symputx("string_tr", string);
		run;

		data casuser.training_eff;
			set casuser.training_eff;
			&string_tr;
		run;

		data casuser.training_var_sel;
			set casuser.training_eff;
			&string_tr;
		run;

		%do_var_reduction(id=&id, training_varsel=casuser.training_var_sel, std=&flag_std, 
			tipo_modello=&tipo_modello,tipo_approccio=&tipo_approccio, vars_reduce=&vars_hpred, 
			out_varsel_table=var_exp_selection_final, save_file=&lib_output..&output_varsel, 
			flagme=&flag_maxeff, varexp=&this_try_var_sel, maxeff=&this_try_maxeff, 
			horizon=&horizon, force_vars_input=&force_vars, 
			force_time_input=&force_time,
			flg_forced_var=&flg_forced_var, hour=&hour_cluster);


		/* Creo selected_variables_interval e selected_variables_class */
		data selected_var_interval(keep=variable) selected_var_class(keep=variable);
			set var_exp_selection_final;
			if level='-' then output selected_var_interval; 
			if level='' then output selected_var_class;
		run;

		proc sql noprint;
			select distinct variable into: selected_variables_interval
			separated by " "
			from selected_var_interval
			;
		quit;

		proc sort data=selected_var_class out=selected_var_class nodupkey;
			by variable;
		run;

		proc sql noprint;
			select count(*) into :obs_count 
			from selected_var_class;
		quit;

		%if (&obs_count ne 0) %then %do;

			data _null_;
				length var_list $30000;
				retain var_list '';
				set work.selected_var_class end=eof;

				if _N_=1 then
					do;
						var_list=variable;
					end;
				else
					do;
						new_var=compress(variable);
						var_list=catx (' ', var_list, new_var);
					end;

				if eof then
					var_class="input " || strip(var_list) || " /level=NOM";
				call symputx('selected_variables_class', var_class);
			run;

		%end;
		%else %do;

			data _null_;
				length var_class $ 32;
				var_class="*input /level=NOM";
				call symputx('selected_variables_class', var_class);
			run;

		%end;

		%do_nnet_1m(training_set=casuser.training_eff, 
				int_var=&selected_variables_interval, class_var=&selected_variables_class, 
				algo=&algo_nn, 
				regl1_init=&regl1, regl2_init=&regl2, layer_string_init=&layer_string, 
				model_out=&neural_model);
				
		%archivia_modelli(model_string=&model_base_string);

		%put ========= FINE ORA &hour_pred_g PER IL TARGET &target_id =========;

	%end;

%mend training_g;

%macro execute_configs();
	
	/* Filtro la Griglia delle configurazioni per l'ora che devo prevedere */
	/* Filtro la Griglia delle configurazioni per l'ora che devo prevedere */
	data input_grid_conf;
		set &lib_input..&grid;
		hour_pred_try="&hour_pred_g";
		if index(hour, "&hour_pred_g");
		/* Creo la stringa da inserire nella NNET */
		array layers[*] nunits:;
		array actfun[*] act:;
		do i=1 to nhidden;
			if i=1 then
				layer_string=cat("hidden ", compress(put(layers[i], $32.)), "/ act=", 
					compress(actfun[i]), ";");
			else
				layer_string=catx(" ", layer_string, cat("hidden ", compress(put(layers[i], 
					$32.)), "/ act=", compress(actfun[i]) , ";"));
		end;
	run;

	%if %sysfunc(exist(&lib_input_inm..&input_dset))=0 %then
		%do;

				proc casutil outcaslib="public";
					load data=&lib_input..&input_dset promote;
				quit;
		%end;

	data _null_;
		set input_grid_conf;
		call execute ('%NRSTR (%training_g('||id||', '||target||', '||approccio||','||modello||','||hour||','||key_cluster_hour||',
				'||drop_vars||','||vars_hpred||','||flag_std||',
				'||stdize_vars||','||flg_forced_var||','||force_vars||','||force_time||','||flag_maxeff||',
				'||max_eff||','||var_sel||','||horizon||','||season_width||',
				'||regl1||','||regl2||', '||layer_string||'))');
	run;

%mend;

/*============ Esecuzione funzione master =================*/
%execute_configs();

%put "==============================================================";
%put "===================   END   ============================";
%put "==============================================================";

/* cas mySession terminate; */

