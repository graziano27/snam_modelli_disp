/*****************************Create input components and merge them ************************************************/

ods exclude all;
options cpucount=2 mprint mlogic symbolgen spool;
options compress=yes;
%let pfx =;

/*Set current date*/
/* %let start = %sysfunc(datetime()); */
%let dt_solare = &start;

data _null_;
	data_inizio=put(&dt_solare,datetime19.);
	call symput("data_ora",data_inizio);
run;

%put ------- START &data_ora;


/*Libraries*/
%let libreria_output=&lib_physical;  
%let libreria_memory=&lib_memory; 

/* Global macro variables */
%global dataset_presente;
%global input_bilanciosnam;
%global input_bilanciosnam1;
%global input_bilancioSCADA;
%global input_dlp_orario;
%global input_pcs_misura;
%global input_meteo_celle_cons;
%global input_meteo_celle_prev;
%global input_riconsegnato1;
%global input_riconsegnato2;
%global input_meteo_citta_prev;
%global input_remi_anagrafica;
%global end_date;
%global start_date;
%global cut_date;
%global input_calendario;
%global RicTot_base;
%global RicTot_removed;
%global RicTot_dupp;
%global RicTot_cut;
%global flg_anomalie;
%global output_final;


/* CALENDARIO */
%let input_calendario=work.calendario_orario_final;
%let input_calendario_G=&libreria_output..calendario_orario_G;
%let input_calendario_G1=&libreria_output..calendario_orario_G1;
%let input_calendario_G2=&libreria_output..calendario_orario_G2;
%let input_calendario_G3=&libreria_output..calendario_orario_G3;
%let input_calendario_G4=&libreria_output..calendario_orario_G4;
%let input_calendario_tot=&libreria_output..calendario_orario_tot;


/* BIL, RTOT, LAG */
%let input_bilanciosnam1=webdips.prev_bilancio;
%let input_bilanciosnam=work.bilanciosnam_final;
%let input_dlp_orario=oralpbp2.analisi_dlp_orario;
%let input_pcs_misura=webdips.pcs_misura_aop9100;
%let input_riconsegnato1=oralpbp2.riconsegnato;
%let input_riconsegnato2=oralpbp2.consuntivo_view;
%let input_termo=oralpbp2.d_e_up_new;
%let input_sime2=oralpbp2.d_e_misure_sime_2;
%let input_sime=oralpbp2.d_e_misure_sime;
%let input_vol_val=oralpbp2.d_e_volumi_validati;
%let input_up=ORALPBP2.D_E_UP;
%let input_remi_termo=ORALPBP2.D_E_UP_NEW;


%let output_rtot_lag_mvg=&libreria_output..output_rtot_lag_mvg_&keyprocess.;
%let output_bil_lag_mvg=&libreria_output..output_bil_lag_mvg_&keyprocess.;
%let output_lagsim=&libreria_output..output_lagsim_&keyprocess.;
%let output_lagsim_G1=&libreria_output..output_lagsim_G1_&keyprocess.;
%let output_rall=&libreria_output..output_rall_&keyprocess.;
%let output_rdif=&libreria_output..output_rdif_&keyprocess.;

/* SCADA */
%let input_bilancioscada= oralpbp2.analisi_bilancio;
%let output_scada=&libreria_output..output_scada_base_&keyprocess.;
%let output_scada_g =&libreria_output..output_scada_g_&keyprocess.;
%let output_scada_g1 = &libreria_output..output_scada_g1_&keyprocess.;
%let output_scada_g2 = &libreria_output..output_scada_g2_&keyprocess.;
%let output_scada_g3 = &libreria_output..output_scada_g3_&keyprocess.;
%let output_scada_g4 = &libreria_output..output_scada_g4_&keyprocess.;
%let output_scada_tot=&libreria_output..output_scada_tot_&keyprocess.;
/*%let scada_vars=scada_bil_ric scada_bil_exp scada_bil_imp scada_bil_stc scada_bil_pnl scada_bil_dlp;*/

/* SCADACONS */
%let anagrafica= WEBDIPS.ANAGRAFICA_PORTATE_SCADA;
%let valori=ORALPBP2.MISURATORI_SCADA;
%let output_scadacons_G=&libreria_output..output_scadacons_G_&keyprocess.;
%let output_scadacons=&libreria_output..output_scadacons_&keyprocess.;
%let output_scadacons_tot=&libreria_output..output_scadacons_tot_&keyprocess.;

/* FLUSSIFISICI */
%let output_scada_ff=&libreria_output..input_ff_scada_orario_G1_&keyprocess.;

/* METEO CITTA */
%let meteo_citta=oralpbp2.d_e_meteo_calc;
%let groups_meteo_citta=&libreria_output..grouping_meteo_citta;
%let output_citta=&libreria_output..output_meteo_urb_&keyprocess.; 
%let output_meteo_citta_prev=&libreria_output..input_meteoc_psol_G1_&keyprocess.;
%let output_meteo_citta_prev_new=&libreria_output..input_meteoc_pgas_G1_&keyprocess.;
%let output_meteo_citta_prevnew2=&libreria_output..meteo_citta_prevg2_Trend_&keyprocess.;
%let output_meteo_citta_prevnew3=&libreria_output..meteo_citta_prevg3_Trend_&keyprocess.;
%let output_meteo_citta_prevnew4=&libreria_output..meteo_citta_prevg4_Trend_&keyprocess.;
/*%let output_meteo_citta_prev_ut=&libreria_output..meteo_citta_prev_UT;*/
/*%let output_meteo_citta_prev_new_ut=&libreria_output..meteo_citta_prevgas_UT;*/
%let output_citta_tot = &libreria_output..output_meteo_citta_tot_&keyprocess.;


/* METEO CELLE */
%let meteo_celle_cons=oralpbp2.D_E_METEO_CONSUNTIVO;
%let meteo_celle_prev=oralpbp2.D_E_DATI_METEO_AREE;
%let grouping_all=&libreria_output..grouping_celle_full;
%let grouping_t_med=&libreria_output..grouping_celle_t_med;
%let grouping_t_gg=&libreria_output..grouping_celle_t_gg;
%let grouping_umidita=&libreria_output..grouping_celle_umid;
%let grouping_altezza_min=&libreria_output..grouping_celle_alt;
%let grouping_t_max_perc=&libreria_output..grouping_celle_tmaxp;
%let output_celle=&libreria_output..output_meteo_celle_&keyprocess.;
%let input_meteo_celle_cons=ORALPBP2.D_E_METEO_CONSUNTIVO;
%let input_meteo_celle_prev=oralpbp2.D_E_DATI_METEO_AREE;
%let output_meteo_celle_prev1=&libreria_output..input_meteo_prev_G1_&keyprocess.; /*same output for UT */
%let output_meteo_celle_prev2=&libreria_output..input_meteo_prev2_Trend_&keyprocess.;
%let output_meteo_celle_cons1=&libreria_output..input_meteo_consuntivo_G1_&keyprocess.; /*same output for UT */
%let output_meteo_celle_cons2=&libreria_output..input_meteo_consold_Trend_&keyprocess.;
%let output_meteo_celle_cons_new2=&libreria_output..input_meteo_consnew_Trend_&keyprocess.;
%let output_celle_tot = &libreria_output..output_meteo_celle_tot_&keyprocess.;


/* TERNA */
%let input_terna=ORALPBP2.D_E_TERNA;
%let input_termo=ORALPBP2.D_E_UP_NEW;
%let output_consuntivo_terna=&libreria_output..output_terna_cons_&keyprocess.;
%let output_previsione_terna=&libreria_output..output_terna_prev_&keyprocess.;
%let output_consuntivo_terna_G1 = &libreria_output..input_terna_cons_G1_&keyprocess.;
%let output_previsione_terna_G1 = &libreria_output..input_terna_prev_G1_&keyprocess.;
%let output_consuntivo_terna_Trend = &libreria_output..consuntivo_all_Trend_&keyprocess.;
%let output_consuntivo_terna_Ut = &libreria_output..terna_consuntivo_UT_&keyprocess.;
%let output_previsione_terna_Ut = &libreria_output..terna_previsione_UT_&keyprocess.;



/* ANOMALIE */
/*%let output_anomalie_cons = &libreria_output..anomalie_scada_cons;*/
/*%let output_anomalie_terna=&libreria_output..anomalie_terna;*/
/*%let output_anomalie_bilancio=&libreria_output..anomalie_bilancio;*/
/*%let output_anomalie_scada=&libreria_output..anomalie_scada_bil;*/
/*%let output_anomalie_all=&libreria_output..anomalie_orarie;*/


/* DS TOT */
%let output_final=&libreria_output..&output_final_name;
%let RicTot_base=&output_final;
%let RicTot_removed=&libreria_output..&output_final_name._prov;
%let RicTot_dupp=&libreria_output..&output_final_name._dup;
%let RicTot_cut=&libreria_output..&output_final_name._cut;
%let datetime_var_dup=datetime_solare;

/*Set macro variables for DATASET creation*/
%let flg_anomalie = 1;
/* %let dataset_presente = 1;  */


/*Define useful dates*/
data init_dates;
	g_solare=Datepart(&dt_solare);
	g_gas=ifn(hour(&dt_solare)<6, intnx("day", g_solare, -1), g_solare);
	g1_solare=intnx("day",g_solare,1);
	g1_gas=intnx("day", g_gas,1);
	call symput("g_solare",g_solare);
	call symput("g_gas",g_gas);
	call symput("g1_solare",g1_solare);
	call symput("g1_gas",g1_gas);
	format g: date9.;
run;

/* Create dataset bilancio */
data bilancio_lim;
	if day(&g_gas)>10 then
		filter=intnx("month", &g_gas,-2, "end");
	else filter=intnx("month", &g_gas,-3, "end");
	call symput("bilancio_limit", filter);
	format filter date9.;
run;
	
data &input_bilanciosnam;
	set &input_bilanciosnam1(where=(fl_deleted eq "0" ));
	drop k_storico data_elaborazione fl_deleted k_prev_bilancio fl_azione utente_azione dt_azione data;
	date=datepart(data);
	format date date9.;

	if date<=&bilancio_limit then
		validato=1;
	else validato=0;

	if validato eq . then
		validato = 0;
run;
	
proc sort data=&input_bilanciosnam out=&input_bilanciosnam;
	by date;
run;

%macro create_input_components(start_date_short, start_date_long);
	
	data &input_calendario(where=(data_gas_rif<=&g_gas and data_gas_rif>=&start_date_long));
		date=&start_date_long;

		do while (date<=&g1_solare);
			do hour=0 to 23;
				datetime_solare=dhms(date,hour,0,0);

				if hour<6 then
					data_gas_rif=intnx("day", date, -1);
				else data_gas_rif=date;
				output;
			end;

			date=intnx("day", date, 1, 's');
		end;

		format date date9.;
		format data_gas_rif date9.;
		format datetime_solare datetime19.;
	run;


	%parallel_function(server1, target bil_lag);
	%parallel_function(server2, scada scadacons terna);
	%parallel_function(server3, flussifisici);
	%parallel_function(server4, meteocitta);
	%parallel_function(server5, meteocelle);

	/* FINE PARALLELISMO */
	/* Wait results from all server */
	waitfor server1 server2 server3 server4 server5;

	/* signoff from all server */
	signoff server1;
	signoff server2;
	signoff server3;
	signoff server4;
	signoff server5;
	
/*	%put_run_date_init(merge_anomalie);*/
/*	%merge_anomalie(lib_in=&libreria_output, ds_out=&output_anomalie_all);*/
/*	%put_run_date_init(merge_anomalie);*/
/* 	proc sql noprint; */
/* 		select distinct memname into :table_list separated by ' ' */
/* 			from dictionary.columns */
/* 				where libname="WORK" and index(lowcase(memname), "output") =0  */
/* 		; */
/* 	quit; */
/*  */
/* 	proc datasets lib=work nolist; */
/* 		delete &table_list; */
/* 	run; */

%mend create_input_components;

%macro merge_input_components();
	/* LA CHIAVE --> datetime_solare */
	data &input_calendario(where=(data_gas_rif<=&G_gas));
		date= intnx("month",&G_solare,0, 'B');

		do while (date<=&G1_solare);
			do hour=0 to 23;
				datetime_solare=dhms(date,hour,0,0);

				if hour<6 then
					data_gas_rif=intnx("day", date, -1);
				else data_gas_rif=date;
				output;
			end;

			date=intnx("day", date, 1, 's');
		end;

		format date date9.;
		format data_gas_rif date9.;
		format datetime_solare datetime16.;
	run;

	/*create calendar vars for all models*/
	%create_all_calendar_vars(&input_calendario,&input_calendario_tot);

	/*merge scada*/
	data scada_merge;
		set &output_scada_tot;
		drop data_gas_rif giorno_gas_scada;
		rename datetime_prev=datetime_solare;
	run;
	/* FFILL preventivo con il calendario */
	data scada_merge_c;
		merge &input_calendario(in=p)
				scada_merge;
		by datetime_solare;
		if p;
	run;

	proc sql;
		%let savelist=;
		select compress(cat("s", name))
			into :savelist separated by ' '
				from dictionary.columns
					where libname="WORK" and lowcase(memname)="scada_merge_c"
					and name like "G%";
	quit;

	proc sql;
		%let droplist=;
		select name
			into :droplist separated by ' '
				from dictionary.columns
					where libname="WORK" and lowcase(memname)="scada_merge_c"
					and name like "G%";
	quit;

	data scada_merge_fill(drop=a b i j &droplist);
		set	scada_merge_c;
		array new &savelist;
		array old &droplist;
		retain new;
		a=dim(new);
		b=dim(old);

		if _N_=1 then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;
	run;

	proc sql;
		select compress(cat(name,"=",substr(name,2,length(name))))
			into :rename_list separated by ' '
				from dictionary.columns
					where libname="WORK" and lowcase(memname)="scada_merge_fill"
					and name like "s%";
	quit;

	data scada_merge_fill;
		set scada_merge_fill;
		rename &rename_list;
	run;

	/*merge scadacons*/
	data scadacons_merge;
		set &output_scadacons_tot;
		rename datetime_calcolo=datetime_solare;
	run;
	
	/*merge flussifisici*/
	data ff_merge;
		set &output_scada_ff(drop=data_gas_g hour);
		rename datetime_solare_g = datetime_solare;
	run;

		/* FFILL preventivo con il calendario */
	data ff_merge_c;
		merge &input_calendario(in=p)
				ff_merge;
		by datetime_solare;
		if p;
	run;

	proc sql;
		%let savelist=;
		select compress(cat("s", name))
			into :savelist separated by ' '
				from dictionary.columns
					where libname="WORK" and lowcase(memname)="ff_merge_c"
					and name like "G%";
	quit;

	proc sql;
		%let droplist=;
		select name
			into :droplist separated by ' '
				from dictionary.columns
					where libname="WORK" and lowcase(memname)="ff_merge_c"
					and name like "G%";
	quit;

	data ff_merge_fill(drop=a b i j &droplist);
		set	ff_merge_c;
		array new &savelist;
		array old &droplist;
		retain new;
		a=dim(new);
		b=dim(old);

		if _N_=1 then
			do i=1 to dim(new);
				new(i)=old(i);
			end;
		else
			do;
				do j=1 to dim(new);
					if old(j) ne . then
						new(j)=old(j);
				end;
			end;
	run;

	proc sql;
		select compress(cat(name,"=",substr(name,2,length(name))))
			into :rename_list separated by ' '
				from dictionary.columns
					where libname="WORK" and lowcase(memname)="ff_merge_fill"
					and name like "s%";
	quit;

	data ff_merge_fill;
		set ff_merge_fill;
		rename &rename_list;
	run;

	/*merge meteocitta*/
	data meteocitta_merge(drop= hour data_gas_rif data_solare_rif);
		set &output_citta_tot;
	run;

	/*merge meteocelle*/
	data meteocelle_merge;
		set &output_celle_tot;
		drop data: hour;
	run;

	/*merge LAG e MVG BIL*/
	data billagmvg_merge1;
		set &output_bil_lag_mvg(drop=data_gas G_data_gas_rif date hour);
	run;

	/*merge LOG E MVG TARGET*/
	data billagmvg_merge2;
		set &output_rtot_lag_mvg(drop=data_gas /*data_gas_rif date*/ hour);
	run;

	/*merge LAG SIM*/
	data LAGSIM_merge(drop=data_gas data_gas_rif date hour);
		set &output_lagsim;
	run;

	/*merge LAG SIM G1*/
	data LAGSIM_merge_G1(drop=data_gas date hour);
		set &output_lagsim_G1;
	run;

	/*terna CONS*/
	data terna_cons_merge(drop= terna_consuntivo hour data_gas data_gas_rif date hour);
		set &output_consuntivo_terna;
		rename dt_calcolo=datetime_solare;
	run;

	/*terna prev*/
	data terna_prev_merge(drop=hour data_gas data_gas_rif date hour);
		set &output_previsione_terna;
		rename dt_calcolo=datetime_solare;
	run;

	/*rall */
	data rall_merge;
		set &output_rall(drop= hour data_gas G_data_gas_rif G1_data_gas_rif G2_data_gas_rif G3_data_gas_rif G4_data_gas_rif);
	run;
	
	/*rdiff*/
	data rdiff_merge;
		set &output_rdif (rename=(datetime_solare_g=datetime_solare)
			drop=data_gas_rif /*data_gas Riconsegnato_Consuntivo_KWH*/);
	run;


	data input_final;
		merge &input_calendario_tot(in=p)
		billagmvg_merge1
		billagmvg_merge2
		LAGSIM_merge
		LAGSIM_merge_G1
		terna_cons_merge
		terna_prev_merge
		scada_merge_fill
		scadacons_merge
		ff_merge_fill
		meteocitta_merge
		meteocelle_merge
		rall_merge
		rdiff_merge;
		by datetime_solare;
		if p;
		if data_gas_rif=&G_gas and hour=hour(&dt_solare);
	run;

	

/* 	%round_input_vars(input_final); */
	%checkds(&output_final);

	%if &dataset_presente=1 %then
		%do;

			data &output_final;
				set &output_final input_final;
			run;

		%end;
	%else %if &dataset_presente=0 %then
		%do;

			data &output_final;
				set input_final;
			run;

		%end;
%mend merge_input_components;

%macro create_input();

	data dates;
		today_gas=&G_gas;
		start_date_long=intnx("month",today_gas,-37,"B");
		start_date_short=intnx("month",today_gas,-2,"B");
		start_date_long_char=put(start_date_long, date9.);
		start_date_short_char=put(start_date_short, date9.);
		call symputx("start_date_long",start_date_long);
		call symputx("start_date_short",start_date_short);
		call symputx("start_date_long_char",start_date_long_char);
		call symputx("start_date_short_char",start_date_short_char);
	run;

	%put "======= creating input row at &data_ora =====";
	%put "======= downloading data at max from &start_date_long_char =====";

	%create_input_components(&start_date_short, &start_date_long);
	%merge_input_components();
%mend create_input;

%macro fix_festivi();
	
	data input_modelli;
		set &output_final;
	run;

	data bilancio;
		set &input_bilanciosnam1; 
		keep data data_G1 Importazioni Entrata_Tarvisio	Entrata_Gela Entrata_Gorizia Entrata_Mazara	Entrata_P_Gries	
			GNL_Cavarzere GNL_Livorno GNL_Panigaglia Produzione_Nazionale Sistemi_di_stoccaggio Stogit Edison_Stoccaggio
			Totale_Immesso Riconsegne_rete_Snam_Rete_Gas Industriale Termoelettrico Reti_di_distribuzione Uscita_Bizzarone
			Uscita_Gorizia Uscita_P_Gries Uscita_S_Marino Uscita_Tarvisio Riconsegnato_ad_altre_Reti_tr uscite;
		rename Riconsegnato_ad_altre_Reti_tr=Ric_Reti_terzi Riconsegne_rete_Snam_Rete_Gas=Riconsegne_SRG Sistemi_di_stoccaggio=stoccaggio
			Reti_di_distribuzione=reti_distrib;
		uscite = sum(of uscita:);

		if fl_deleted=0 & catx("_",day(datepart(data)), month(datepart(data))) ne "29_2";
		data_G1 = intnx("year", datepart(data), 1, "S");
		format data_G1 date9.;
	run;

	proc sort data=bilancio;
		by data_G1;
	run;

	data dataset_joined;
		merge input_modelli(in=a) bilancio(in=b rename=(data_G1=G1_data_gas_rif));
		by G1_data_gas_rif;

		if a;

		if (G1_time_festivo = 1 or G1_data_gas_rif = intnx('day',holiday('EASTER',year(G1_data_gas_rif)),2)) 
				& year(G1_data_gas_rif) >= 2013 then
			do;
				G1_bil_lag_IMPORTAZIONI_364=IMPORTAZIONI*1000000;
				G1_bil_lag_IMPORTAZIONI_365=IMPORTAZIONI*1000000;
				G1_bil_lag_IMPORTAZIONI_366=IMPORTAZIONI*1000000;
				G1_bil_lag_ENTRATA_TARVISIO_364=ENTRATA_TARVISIO*1000000;
				G1_bil_lag_ENTRATA_TARVISIO_365=ENTRATA_TARVISIO*1000000;
				G1_bil_lag_ENTRATA_TARVISIO_366=ENTRATA_TARVISIO*1000000;
				G1_bil_lag_ENTRATA_GELA_364=ENTRATA_GELA*1000000;
				G1_bil_lag_ENTRATA_GELA_365=ENTRATA_GELA*1000000;
				G1_bil_lag_ENTRATA_GELA_366=ENTRATA_GELA*1000000;
				G1_bil_lag_ENTRATA_GORIZIA_364=ENTRATA_GORIZIA*1000000;
				G1_bil_lag_ENTRATA_GORIZIA_365=ENTRATA_GORIZIA*1000000;
				G1_bil_lag_ENTRATA_GORIZIA_366=ENTRATA_GORIZIA*1000000;
				G1_bil_lag_ENTRATA_MAZARA_364=ENTRATA_MAZARA*1000000;
				G1_bil_lag_ENTRATA_MAZARA_365=ENTRATA_MAZARA*1000000;
				G1_bil_lag_ENTRATA_MAZARA_366=ENTRATA_MAZARA*1000000;
				G1_bil_lag_ENTRATA_P_GRIES_364=ENTRATA_P_GRIES*1000000;
				G1_bil_lag_ENTRATA_P_GRIES_365=ENTRATA_P_GRIES*1000000;
				G1_bil_lag_ENTRATA_P_GRIES_366=ENTRATA_P_GRIES*1000000;
				G1_bil_lag_GNL_CAVARZERE_364=GNL_CAVARZERE*1000000;
				G1_bil_lag_GNL_CAVARZERE_365=GNL_CAVARZERE*1000000;
				G1_bil_lag_GNL_CAVARZERE_366=GNL_CAVARZERE*1000000;
				G1_bil_lag_GNL_LIVORNO_364=GNL_LIVORNO*1000000;
				G1_bil_lag_GNL_LIVORNO_365=GNL_LIVORNO*1000000;
				G1_bil_lag_GNL_LIVORNO_366=GNL_LIVORNO*1000000;
				G1_bil_lag_GNL_PANIGAGLIA_364=GNL_PANIGAGLIA*1000000;
				G1_bil_lag_GNL_PANIGAGLIA_365=GNL_PANIGAGLIA*1000000;
				G1_bil_lag_GNL_PANIGAGLIA_366=GNL_PANIGAGLIA*1000000;
				G1_bil_lag_PRODUZIONE_NAZ_364=PRODUZIONE_NAZIONALE*1000000;
				G1_bil_lag_PRODUZIONE_NAZ_365=PRODUZIONE_NAZIONALE*1000000;
				G1_bil_lag_PRODUZIONE_NAZ_366=PRODUZIONE_NAZIONALE*1000000;
				G1_bil_lag_stoccaggio_364=stoccaggio*1000000;
				G1_bil_lag_stoccaggio_365=stoccaggio*1000000;
				G1_bil_lag_stoccaggio_366=stoccaggio*1000000;
				G1_bil_lag_STOGIT_364=STOGIT*1000000;
				G1_bil_lag_STOGIT_365=STOGIT*1000000;
				G1_bil_lag_STOGIT_366=STOGIT*1000000;
				G1_bil_lag_EDISON_STOCCAGGIO_364=EDISON_STOCCAGGIO*1000000;
				G1_bil_lag_EDISON_STOCCAGGIO_365=EDISON_STOCCAGGIO*1000000;
				G1_bil_lag_EDISON_STOCCAGGIO_366=EDISON_STOCCAGGIO*1000000;
				G1_bil_lag_TOTALE_IMMESSO_364=TOTALE_IMMESSO*1000000;
				G1_bil_lag_TOTALE_IMMESSO_365=TOTALE_IMMESSO*1000000;
				G1_bil_lag_TOTALE_IMMESSO_366=TOTALE_IMMESSO*1000000;
				G1_bil_lag_Riconsegne_SRG_364=Riconsegne_SRG*1000000;
				G1_bil_lag_Riconsegne_SRG_365=Riconsegne_SRG*1000000;
				G1_bil_lag_Riconsegne_SRG_366=Riconsegne_SRG*1000000;
				G1_bil_lag_INDUSTRIALE_364=INDUSTRIALE*1000000;
				G1_bil_lag_INDUSTRIALE_365=INDUSTRIALE*1000000;
				G1_bil_lag_INDUSTRIALE_366=INDUSTRIALE*1000000;
				G1_bil_lag_TERMOELETTRICO_364=TERMOELETTRICO*1000000;
				G1_bil_lag_TERMOELETTRICO_365=TERMOELETTRICO*1000000;
				G1_bil_lag_TERMOELETTRICO_366=TERMOELETTRICO*1000000;
				G1_bil_lag_reti_distrib_364=reti_distrib*1000000;
				G1_bil_lag_reti_distrib_365=reti_distrib*1000000;
				G1_bil_lag_reti_distrib_366=reti_distrib*1000000;
				G1_bil_lag_USCITA_BIZZARONE_364=USCITA_BIZZARONE*1000000;
				G1_bil_lag_USCITA_BIZZARONE_365=USCITA_BIZZARONE*1000000;
				G1_bil_lag_USCITA_BIZZARONE_366=USCITA_BIZZARONE*1000000;
				G1_bil_lag_USCITA_GORIZIA_364=USCITA_GORIZIA*1000000;
				G1_bil_lag_USCITA_GORIZIA_365=USCITA_GORIZIA*1000000;
				G1_bil_lag_USCITA_GORIZIA_366=USCITA_GORIZIA*1000000;
				G1_bil_lag_USCITA_P_GRIES_364=USCITA_P_GRIES*1000000;
				G1_bil_lag_USCITA_P_GRIES_365=USCITA_P_GRIES*1000000;
				G1_bil_lag_USCITA_P_GRIES_366=USCITA_P_GRIES*1000000;
				G1_bil_lag_USCITA_S_MARINO_364=USCITA_S_MARINO*1000000;
				G1_bil_lag_USCITA_S_MARINO_365=USCITA_S_MARINO*1000000;
				G1_bil_lag_USCITA_S_MARINO_366=USCITA_S_MARINO*1000000;
				G1_bil_lag_USCITA_TARVISIO_364=USCITA_TARVISIO*1000000;
				G1_bil_lag_USCITA_TARVISIO_365=USCITA_TARVISIO*1000000;
				G1_bil_lag_USCITA_TARVISIO_366=USCITA_TARVISIO*1000000;
				G1_bil_lag_Ric_Reti_Terzi_364=Ric_Reti_Terzi*1000000;
				G1_bil_lag_Ric_Reti_Terzi_365=Ric_Reti_Terzi*1000000;
				G1_bil_lag_Ric_Reti_Terzi_366=Ric_Reti_Terzi*1000000;
				G1_bil_lag_uscite_364=uscite*1000000;
				G1_bil_lag_uscite_365=uscite*1000000;
				G1_bil_lag_uscite_366=uscite*1000000;
			end;
	run;

	data target_dset(rename=(G1_tgt_riconsegnato_totale=target));
		set &output_rall; 
		keep G1_tgt_riconsegnato_totale G1_data_gas_rif datetime_solare;
		G1_data_gas_rif = intnx("DAY",365,data_gas,"S");
		datetime_solare = ifn(hour(datetime_solare)>=6, dhms(intnx('day',-1,G1_data_gas_rif), hour(datetime_solare),0,0)
								, dhms( G1_data_gas_rif, hour(datetime_solare),0,0));
		format G1_data_gas_rif date9.;
	run;
	
	proc sort data=dataset_joined;
	by G1_data_gas_rif datetime_solare;
	run;
	proc sort data=target_dset;
	by G1_data_gas_rif datetime_solare;
	run;

	data dataset_joined;
		merge dataset_joined(in=uno) target_dset(in=due);
		by G1_data_gas_rif datetime_solare;

		if uno;
		
		if G1_time_festivo = 1 & year(G1_data_gas_rif) >= 2013 then
			do;
				G1_rtot_lag_364=target;
				G1_rtot_lag_365=target;
				G1_rtot_lag_366=target;
			end;
	run;

	proc sort data=input_modelli;
	by G1_data_gas_rif datetime_solare;
	run;
	
	data &output_final.(drop=IMPORTAZIONI--target);
		merge dataset_joined(in=uno) input_modelli(in=due keep=G1_data_gas_rif datetime_solare);
		by G1_data_gas_rif datetime_solare;
		if due;
	run;

%mend fix_festivi;


%create_input();
%fix_festivi();

data _null_;
	data_ora=put(datetime(),datetime19.);
	call symput("data_ora",data_ora);
run;

%put ------- END &data_ora;