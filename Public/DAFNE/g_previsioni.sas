/*===============================================================*/
/***** rilascio 2020-01-XX - versione v1.0 *****/
/***** autori: Binda, Oliva, Rizzolo *****/
/***** BIP S.p.a. *****/
/*===============================================================*/

/* options nomlogic nomprint nosymbolgen; */
/* options mlogic mprint symbolgen msglevel=i; */
/* ods exclude all; */
/*ods exclude none;*/

/*============ Macro variabili =================================*/
/* Definizione macro variabili Globali */
%let dt_solare=&start;
%let libreria_input=&lib_physical;
%let libreria_inmemory_mod=&lib_memory;
%let libreria_output=&lib_physical;
%let libreria_inmemory=casuser;

%let name_grid = configurazioni;
%let name_grid_inm = configurazioni_tmp;

%let name_input = &g_output_final_name;
%let physic_input = &libreria_input..&name_input;
%let physic_grid = &libreria_input..&name_grid;
%let inmemory_name = riga_di_input;
%let inmemory_input = &libreria_inmemory..&inmemory_name;
%let inmemory_grid = &libreria_inmemory..&name_grid_inm;
%let time_id = data_gas_rif;
%let model_path=&lib_path; * dichiarato nel prevedi_viya;
%let ext = .txt;

%let output_name = previsioni_riconsegnato;
/* %let output_name = test_prev; */
%let output_prev = &libreria_output..&output_name;

/* Prendo l'ultima riga del dataset di input */

proc sort data=&physic_input out=work.g_input_sort;
	by datetime_solare;
run;

data &inmemory_input;
	set g_input_sort end=eof;
		where data_gas_rif > intnx("day", datepart(&dt_solare), -10);
	if eof then output;
run;

data check;
	set &inmemory_input;
	check = nmiss(of _numeric_);
	keep da: check;
	call symputx("check",check);
run;

%put "TROVATI &check VALORI MANCANTI";

	
%macro fill_anomalie(dset);	
	data check;
		set &dset;
		check = nmiss(of _numeric_);
		keep da: check;
		call symputx("check",check);
	run;
	
	%if &check ne 0 %then %do;
			proc stdize data=&dset out=&dset replace missing=0;
				var scada: ;
			run;
	%end;

%mend;

/* %fill_anomalie(&inmemory_input); */

/* data check_after_imputing; */
/* 	set &inmemory_input; */
/* 	check = nmiss(of _numeric_); */
/* 	keep da: check; */
/* run; */

/*============ Date per i modelli =================================*/
/* Seleziono l'ora con cui selezionare il modello da utilizzare */

data date_to_pred;
	dt_now =&dt_solare;
	date_now = datepart(dt_now);
	hour_now = hour(dt_now);
	data_gas_now = ifn(hour_now<6, intnx('day', date_now, -1), date_now);
	hour_pred = hour_now;
	date_pred_g = data_gas_now;
	mese_pred_g = month(date_pred_g);
	anno_pred_g = year(date_pred_g);
	mese_prec_g = month(intnx('month',date_pred_g, -1));
	anno_prec_g = year(intnx('month',date_pred_g, -1));
	anno_prec2_g = year(intnx('month',date_pred_g, -2));
	mese_prec2_g = month(intnx('month',date_pred_g, -2));
	call symputx("hour_pred_g", put(hour_pred, z2.0));
	call symputx("mese_pred_g", put(mese_pred_g, z2.0));
	call symputx("anno_pred_g", anno_pred_g);
	call symputx("date_pred_g", date_pred_g);
	call symputx("mese_prec_g", put(mese_prec_g, z2.0)); 
	call symputx("anno_prec_g", anno_prec_g);
	call symputx("mese_prec2_g", put(mese_prec2_g, z2.0));
	call symputx("anno_prec2_g", anno_prec2_g);
	format da: date9.;
	format dt: datetime19.;
run;

%macro previsione(hour_pred);

	/* Filtro la Griglia delle configurazioni per il target e ora che devo prevedere */
	data &inmemory_grid;
		length base_model_string $20 target_new $4;
		set &physic_grid;
		hour_pred_try="&hour_pred_g";
		if index(hour,"&hour_pred_g");
		if target = 'tgt_riconsegnato_totale' then target_new = 'TOT';
		if target = 'tgt_riconsegnato_diff' then target_new = 'DIFF';
		if target = 'tgt_civile' then target_new = 'CIV';
		if target = 'tgt_termoelettrico_terna' then target_new = 'TERM';		
		if target = 'tgt_industriale' then target_new = 'IND';
		base_model_string = compress(catx('_', "NN", approccio, target_new, modello));
		drop nhidden -- force_vars;
	run;

	data _null_;
		set &inmemory_grid;
		call execute ('%NRSTR (%doing_previsione('||key_cluster_hour||','||base_model_string||', 
		'||modello||', '||target||', '||target_new||', '||id||', '||approccio||'))');
	run;

%mend previsione;

%macro doing_previsione(key_cluster_hour, base_model_string, modello, 
	target, target_new, id, approccio);

	/*============ ESEGUO LA PREVISIONE ===================*/
	/*============ MODELLO DI INPUT PER LA RETE NEURALE ===================*/
	
	data name_model;
		neural_model_prefix0=compress(catx("_","&base_model_string","&anno_pred_g", "&mese_pred_g", "&key_cluster_hour.&ext"));
		neural_model_prefix1=compress(catx("_","&base_model_string","&anno_prec_g", "&mese_prec_g", "&key_cluster_hour.&ext"));
		neural_model_prefix2=compress(catx("_","&base_model_string","&anno_prec2_g", "&mese_prec2_g", "&key_cluster_hour.&ext"));
		%put "&anno_pred_g";
		call symputx("neural_model0", neural_model_prefix0);
		call symputx("neural_model1", neural_model_prefix1);
		call symputx("neural_model2", neural_model_prefix2);
	run;

	%if %sysfunc(fileexist("&model_path./&neural_model0.")) %then %do;
		data _null_;
			call symputx("neural_model_file","&model_path./&neural_model0.");
		run;
	%end;

	%else %do;
			%if %sysfunc(fileexist("&model_path./&neural_model1.")) %then %do;
				data _null_;
					call symputx("neural_model_file","&model_path./&neural_model1.");
				run;
			%end;

			%else %do;
				data _null_;
					call symputx("neural_model_file","&model_path./&neural_model2.");
				run;
			%end;
	%end;

	%put -- Utilizzo modello "&neural_model_file" --;

	data casuser.score_out(keep=P_&target &time_id datetime_solare hour scada_bil_ric_kwh_cumday);
		set &inmemory_input;
		%include "&neural_model_file.";
	run;
	
	/* APPEND DEI RISULTATI IN &libreria_output..&output_prev */ ;
	
	data score_out_complete;
		set casuser.score_out;
		if "&target" = "tgt_riconsegnato_diff" then 
			do;
				P_&target = P_&target + scada_bil_ric_kwh_cumday;
			end;
		rename P_&target.=previsione;
		target="&target_new";
		modello="&modello";
		approccio="&approccio";
		ID = &ID;
		drop scada_bil_ric_kwh_cumday;
	run;

	%if %sysfunc(exist(&output_prev)) %then %do;

		proc append base=&output_prev data=score_out_complete force;
		run;

	%end;

	%else %do;

		data &output_prev;
			length datetime_solare data_gas_rif hour previsione 8 approccio $10 target $24 
			modello $10 ID 8;
			format datetime_solare datetime19. data_gas_rif date9.;
			stop;
		run;

		proc append base=&output_prev data=score_out_complete force;
		run;

	%end;
	

%mend doing_previsione;

%previsione(&hour_pred_g);



