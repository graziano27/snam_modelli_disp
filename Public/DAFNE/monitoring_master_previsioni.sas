/*===============================================================*/
/***** rilascio 2020-04-XX - versione v1.0 *****/
/***** autori: Binda, Gregori, Oliva, Rizzolo *****/
/***** BIP S.p.a. *****/
/*===============================================================*/

/*============ General Options and Creation of CAS Session ========*/
options casdatalimit=ALL;
cas mySession sessopts=(caslib=casuser timeout=31536000 locale="en_US");
caslib _all_ assign;
ods exclude all;
options nomlogic nomprint nosymbolgen compress=yes emailackwait=240 dflang='italian' validvarname=any; 

/*============ Global variables ========*/
%global at_corrente am_corso start_cut_date_prev n_giorni_full check_prev_all outlier_g_diana_flag outlier_g1_flag;


%macro include_all_pgm();

	filename M2A FILESRVC folderpath="&logic_path" filename='monitoring_inputparameters.sas';
	filename M2B FILESRVC folderpath="&logic_path" filename='monitoring_functions.sas';
	filename M2C FILESRVC folderpath="&logic_path" filename='monitoring_run_all.sas';
	%include M2A;
	%include M2B;
	%include M2C;

%mend include_all_pgm;


%macro check_update_prev(prev1);
	/* check su prev_giornaliere */
	/* 	valorizzo datetime_now */
	/* 	check tra datetime_now max_date */
	/* 	se check ok --> valorizzo check_prev_all=1 */
	/* 	altrimenti 0 */

	proc sql noprint;
		select max(dt_calcolo) format datetime19. into: max_date
			from &prev1
		;
	quit;

	data temp;
		dt_now=dhms(today(),hour(datetime()),0,0);
		dt_1=dhms(datepart("&max_date"dt),hour("&max_date"dt),0,0);
		format dt: datetime19.;
		val = 0;

		if (dt_now = dt_1) then
			do;
				put 'Tabella pronte';
				val = 1;
			end;

		if dt_now ne dt_1 then
			do;
				put "La tabella &prev1 non è aggiornata";
			end;

	run;

	proc sql noprint;
		select val into: check_prev_all
			from temp
		;
	quit;

%mend check_update_prev;

%macro eseguimonitoring(minuti_sleeping=, max_loops=, max_minute=);

	/* CHECK CONDITIONS */
	%check_update_prev(webdips.prev_giornaliere);

	%if &check_prev_all=1 %then
		%do;
			%put PARTE MONITORING;

			%include_all_pgm();
		%end;
	%else
		%do;
			%let todo = 1;
			%let n_loops = 0;

			%do %until(&todo=0);

				/* SLEEP */
				%put Previsioni aggiornate non disponibili. Ricontrollo tra &minuti_sleeping minuto/i;
				%let stop = %sysfunc(sleep(&minuti_sleeping*60,1));
				%put &minuti_sleeping  mins passed - finished sleeping;

				/* CHECK CONDITIONS */
				%check_update_prev(webdips.prev_giornaliere);

				%if &check_prev_all=1 %then
					%do;
						%put Previsioni si sono aggiornate.;
						%let todo = 0;
					%end;
				%else
					%do;
						/*	CHECK N LOOPS e MAX TIME */
						%let n_loops_temp=&n_loops;
						%let n_loops = %eval(&n_loops_temp+1);
						%put N LOOPS &n_loops;
						%let minute_now = %sysfunc(minute(%sysfunc(datetime())));

						%if (&n_loops ge &max_loops or &minute_now > &max_minute) %then
							%do;
								%put QUI FINE LOOP;
								%let todo = 0;
							%end;
					%end;
			%end;

			%put PARTE MONITORING;
			%include_all_pgm();
			
		%end;

	%put FINE;

%mend eseguimonitoring;

%eseguimonitoring(minuti_sleeping=1, max_loops=2, max_minute=59);



