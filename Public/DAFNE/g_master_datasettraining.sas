/*===============================================================*/
/***** rilascio 2020-01-XX - versione v1.0 *****/
/***** autori: Binda, Oliva, Rizzolo *****/
/***** BIP S.p.a. *****/
/*===============================================================*/

/*============ General Options and Creation of CAS Session ========*/
options casdatalimit=ALL;
cas mySession sessopts=(caslib=casuser timeout=31536000 locale="en_US");
caslib _all_ assign;

options mprint mlogic symbolgen linesize=max;
ods exclude all;

/*============ Global Vars ========*/
%global dataset_presente lib_physical lib_memory g_output_final_name;

%let start = %sysfunc(datetime());
%let pfx=;
%let lib_physical=for_svil;
%let lib_memory=public;
%let g_output_final_name=&pfx.g_trainingset_viya;

/* filename glib FILESRVC folderpath="&logic_path" filename='librerie_viya.sas'; */
filename gfcn FILESRVC folderpath="&logic_path" filename='g_functions.sas';
filename gdat FILESRVC folderpath="&logic_path" filename='g_dataset.sas';
filename gtrn FILESRVC folderpath="&logic_path" filename='g_training.sas';

/******************************/
/* FORZO PER I TEST */
/* %let start="01FEB2020:20:00:00"dt; */
/* %let g_output_final_name=&pfx.g_trainingset_viya_test1; */
/******************************/

/*ora in cui viene generato il dataset (fra hour_dset:00 e hour_dset:59)*/
%let hour_dset=5;


data dates_train;
	dt_now=intnx("dthour",&start,0,'B');
	dt_prev=intnx("dthour",&start,1,'B');
	hour_now=hour(dt_now);
	hour_prev=hour(dt_prev);
	data_gas_prev=ifn(hour_prev>=6,datepart(dt_prev),datepart(dt_prev)+1);
	hour_dset=&hour_dset;
	hour_end=ifn(hour_dset-1<0,hour_dset+23,hour_dset-1);
/* devo iniziare a prevedere da questo giorno (ore 6) */
	data_g_rif=intnx("month",data_gas_prev,0,'B');
/*train to predict previous month*/
	dt_g_previous_start=dhms(intnx("month",data_gas_prev,-1,'B'),hour_dset,0,0);
	dt_g_previous_end=dhms(intnx("month",data_gas_prev,-1,'B')+1,hour_end,59,59);
/*train to predict current month*/
	dt_g_current_start=dhms(intnx("month",data_gas_prev,0,'B'),hour_dset,0,0);
	dt_g_current_end=dhms(intnx("month",data_gas_prev,0,'B')+1,hour_end,59,59);
	if DT_G_CURRENT_START <= dt_now and dt_now < DT_G_CURRENT_END or 
	dt_g_previous_start <= dt_now and dt_now < dt_g_previous_end then 
	period_train=1;
	else period_train=0;
	format dt_: datetime19. data: date9.;
	call symputx("hour_now",hour_now);
	call symputx("period_train",period_train);
run;


%macro clean_all();

	proc datasets lib=work kill nolist memtype=data;
	quit;

	Data MVars ( Keep = Name );
		Set SasHelp.VMacro;
		Where Scope = 'GLOBAL';

		if find(Name, "SAS") = 0;

		if find(Name, "SYS") = 0;

		if find(Name, "CLIENT") = 0;

		if find(Name, "SQL") = 0;

		if find(Name, "EG") = 0;

		if find(Name, "ORA") = 0;

		IF NAME NOT IN ("G_OUTPUT_FINAL_NAME", "LIB_PHYSICAL", "LIB_MEMORY",
						"START", "PERIOD_TRAIN", "LOGIC_PATH", "HOUR_DSET",  
						"GDAT", "GFCN", "GTRN", "PFX", "LIB_PATH","DATASET_PRESENTE");
	Run;

	data _null_;
		set MVars;
		Call Symdel( Name );
	Run;

%mend clean_all;


%macro dataset_training();
	%put "============================= INIZIO CODICE TRAINING =====================================";
	%put "==========================================================================================";

	%if (&period_train eq 1) %then
		%do;
			%include gfcn;
			%if (&HOUR_NOW eq &hour_dset) %then
				%do;
					%put "==================== INIZIO G DATASET e TRAINING ore &HOUR_NOW ====================";
					%put STARTING EXECUTION OF G DATASET: FILE &gdat;
					%include gdat;
					%put "G DATASET TERMINATO";

					%clean_all();
					%put STARTING EXECUTION OF G TRAINING: FILE &gtrn;
					%include gtrn;
					%put "G TRAINING TERMINATO";
				%end;
			%else
				%do;
					%put "==================== INIZIO G TRAINING ore &HOUR_NOW ====================";
					%put STARTING EXECUTION OF G TRAINING: FILE &gtrn;
					%include gtrn;
					%put "G TRAINING TERMINATO";
				%end;
		%end;
	%else %put "==================== NIENTE TRAINING ====================";
	%put "============================= FINE CODICE TRAINING =====================================";
%mend dataset_training;


%dataset_training();

data time_execution;
	dt_start = &start;
	dt_end = datetime();
	time_execution = dt_end - dt_start;
	format dt: datetime19.;
	call symputx('time_execution', time_execution);
run;

%put "========================================================================";
%put "IL CODICE SAS E' GIUNTO AL TERMINE IN: &time_execution. SECONDI";

cas mySession terminate; 
