options casdatalimit=ALL;
cas mySession sessopts=(caslib=casuser timeout=31536000 locale="en_US");
caslib _all_ assign;

options mprint mlogic symbolgen linesize=max;
ods exclude all;

%global start dataset_presente lib_physical lib_memory output_final_name trainingset ensemble_id;

%let start = %sysfunc(datetime());
%let pfx=;
%let lib_physical=for_svil;
%let lib_memory=public;
%let keyprocess=in;
%let output_final_name=&pfx.g1_inputmodelli_viya;
%let ensemble_id=9 10 11 12 41 42 43 44 45 46; /* id da tenere nella tabella finale da disporre a SNAM */
%let trainingset=&lib_physical..&pfx.g1_trainingset_viya;

/************************/
ods exclude all;
options cpucount=2 mprint mlogic symbolgen spool;
options compress=yes;
%let pfx =;

/*Set current date*/
/* %let start = %sysfunc(datetime()); */
%let dt_solare = &start;

data _null_;
	data_inizio=put(&dt_solare,datetime19.);
	call symput("data_ora",data_inizio);
run;

%put ------- START &data_ora;


/*Libraries*/
%let libreria_output=&lib_physical;  
%let libreria_memory=&lib_memory;/* 

/* Global macro variables */
%global dataset_presente;
%global input_bilanciosnam;
%global input_bilanciosnam1;
%global input_bilancioSCADA;
%global input_dlp_orario;
%global input_pcs_misura;
%global input_meteo_celle_cons;
%global input_meteo_celle_prev;
%global input_riconsegnato1;
%global input_riconsegnato2;
%global input_meteo_citta_prev;
%global input_remi_anagrafica;
%global end_date;
%global start_date;
%global cut_date;
%global input_calendario;
%global RicTot_base;
%global RicTot_removed;
%global RicTot_dupp;
%global RicTot_cut;
%global flg_anomalie;
%global output_final;


/* CALENDARIO */
%let input_calendario=work.calendario_orario_final;
%let input_calendario_G=&libreria_output..calendario_orario_G;
%let input_calendario_G1=&libreria_output..calendario_orario_G1;
%let input_calendario_G2=&libreria_output..calendario_orario_G2;
%let input_calendario_G3=&libreria_output..calendario_orario_G3;
%let input_calendario_G4=&libreria_output..calendario_orario_G4;
%let input_calendario_tot=&libreria_output..calendario_orario_tot;


/* BIL, RTOT, LAG */
%let input_bilanciosnam1=webdips.prev_bilancio;
%let input_bilanciosnam=work.bilanciosnam_final;
%let input_dlp_orario=oralpbp2.analisi_dlp_orario;
%let input_pcs_misura=webdips.pcs_misura_aop9100;
%let input_riconsegnato1=oralpbp2.riconsegnato;
%let input_riconsegnato2=oralpbp2.consuntivo_view;
%let input_termo=oralpbp2.d_e_up_new;
%let input_sime2=oralpbp2.d_e_misure_sime_2;
%let input_sime=oralpbp2.d_e_misure_sime;
%let input_vol_val=oralpbp2.d_e_volumi_validati;
%let input_up=ORALPBP2.D_E_UP;
%let input_remi_termo=ORALPBP2.D_E_UP_NEW;


%let output_rtot_lag_mvg=&libreria_output..output_rtot_lag_mvg_&keyprocess.;
%let output_bil_lag_mvg=&libreria_output..output_bil_lag_mvg_&keyprocess.;
%let output_lagsim=&libreria_output..output_lagsim_&keyprocess.;
%let output_lagsim_G1=&libreria_output..output_lagsim_G1_&keyprocess.;
%let output_rall=&libreria_output..output_rall_&keyprocess.;
%let output_rdif=&libreria_output..output_rdif_&keyprocess.;

/* SCADA */
%let input_bilancioscada= oralpbp2.analisi_bilancio;
%let output_scada=&libreria_output..output_scada_base_&keyprocess.;
%let output_scada_g =&libreria_output..output_scada_g_&keyprocess.;
%let output_scada_g1 = &libreria_output..output_scada_g1_&keyprocess.;
%let output_scada_g2 = &libreria_output..output_scada_g2_&keyprocess.;
%let output_scada_g3 = &libreria_output..output_scada_g3_&keyprocess.;
%let output_scada_g4 = &libreria_output..output_scada_g4_&keyprocess.;
%let output_scada_tot=&libreria_output..output_scada_tot_&keyprocess.;
/*%let scada_vars=scada_bil_ric scada_bil_exp scada_bil_imp scada_bil_stc scada_bil_pnl scada_bil_dlp;*/

/* SCADACONS */
%let anagrafica= WEBDIPS.ANAGRAFICA_PORTATE_SCADA;
%let valori=ORALPBP2.MISURATORI_SCADA;
%let output_scadacons_G=&libreria_output..output_scadacons_G_&keyprocess.;
%let output_scadacons=&libreria_output..output_scadacons_&keyprocess.;
%let output_scadacons_tot=&libreria_output..output_scadacons_tot_&keyprocess.;

/* FLUSSIFISICI */
%let output_scada_ff=&libreria_output..input_ff_scada_orario_G1_&keyprocess.;

/* METEO CITTA */
%let meteo_citta=oralpbp2.d_e_meteo_calc;
%let groups_meteo_citta=&libreria_output..grouping_meteo_citta;
%let output_citta=&libreria_output..output_meteo_urb_&keyprocess.; 
%let output_meteo_citta_prev=&libreria_output..input_meteoc_psol_G1_&keyprocess.;
%let output_meteo_citta_prev_new=&libreria_output..input_meteoc_pgas_G1_&keyprocess.;
%let output_meteo_citta_prevnew2=&libreria_output..meteo_citta_prevg2_Trend_&keyprocess.;
%let output_meteo_citta_prevnew3=&libreria_output..meteo_citta_prevg3_Trend_&keyprocess.;
%let output_meteo_citta_prevnew4=&libreria_output..meteo_citta_prevg4_Trend_&keyprocess.;
/*%let output_meteo_citta_prev_ut=&libreria_output..meteo_citta_prev_UT;*/
/*%let output_meteo_citta_prev_new_ut=&libreria_output..meteo_citta_prevgas_UT;*/
%let output_citta_tot = &libreria_output..output_meteo_citta_tot_&keyprocess.;


/* METEO CELLE */
%let meteo_celle_cons=oralpbp2.D_E_METEO_CONSUNTIVO;
%let meteo_celle_prev=oralpbp2.D_E_DATI_METEO_AREE;
%let grouping_all=&libreria_output..grouping_celle_full;
%let grouping_t_med=&libreria_output..grouping_celle_t_med;
%let grouping_t_gg=&libreria_output..grouping_celle_t_gg;
%let grouping_umidita=&libreria_output..grouping_celle_umid;
%let grouping_altezza_min=&libreria_output..grouping_celle_alt;
%let grouping_t_max_perc=&libreria_output..grouping_celle_tmaxp;
%let output_celle=&libreria_output..output_meteo_celle_&keyprocess.;
%let input_meteo_celle_cons=ORALPBP2.D_E_METEO_CONSUNTIVO;
%let input_meteo_celle_prev=oralpbp2.D_E_DATI_METEO_AREE;
%let output_meteo_celle_prev1=&libreria_output..input_meteo_prev_G1_&keyprocess.; /*same output for UT */
%let output_meteo_celle_prev2=&libreria_output..input_meteo_prev2_Trend_&keyprocess.;
%let output_meteo_celle_cons1=&libreria_output..input_meteo_consuntivo_G1_&keyprocess.; /*same output for UT */
%let output_meteo_celle_cons2=&libreria_output..input_meteo_consold_Trend_&keyprocess.;
%let output_meteo_celle_cons_new2=&libreria_output..input_meteo_consnew_Trend_&keyprocess.;
%let output_celle_tot = &libreria_output..output_meteo_celle_tot_&keyprocess.;


/* TERNA */
%let input_terna=ORALPBP2.D_E_TERNA;
%let input_termo=ORALPBP2.D_E_UP_NEW;
%let output_consuntivo_terna=&libreria_output..output_terna_cons_&keyprocess.;
%let output_previsione_terna=&libreria_output..output_terna_prev_&keyprocess.;
%let output_consuntivo_terna_G1 = &libreria_output..input_terna_cons_G1_&keyprocess.;
%let output_previsione_terna_G1 = &libreria_output..input_terna_prev_G1_&keyprocess.;
%let output_consuntivo_terna_Trend = &libreria_output..consuntivo_all_Trend_&keyprocess.;
%let output_consuntivo_terna_Ut = &libreria_output..terna_consuntivo_UT_&keyprocess.;
%let output_previsione_terna_Ut = &libreria_output..terna_previsione_UT_&keyprocess.;



/* ANOMALIE */
%let output_anomalie_cons = &libreria_output..anomalie_scada_cons;
%let output_anomalie_terna=&libreria_output..anomalie_terna;
%let output_anomalie_bilancio=&libreria_output..anomalie_bilancio;
%let output_anomalie_scada=&libreria_output..anomalie_scada_bil;
%let output_anomalie_all=&libreria_output..anomalie_orarie;


/* DS TOT */
%let output_final=&libreria_output..&output_final_name;
%let RicTot_base=&output_final;
%let RicTot_removed=&libreria_output..&output_final_name._prov;
%let RicTot_dupp=&libreria_output..&output_final_name._dup;
%let RicTot_cut=&libreria_output..&output_final_name._cut;
%let datetime_var_dup=datetime_solare;

/*Set macro variables for DATASET creation*/
%let flg_anomalie = 1;
/* %let dataset_presente = 1;  */


/*Define useful dates*/
data init_dates;
	g_solare=Datepart(&dt_solare);
	g_gas=ifn(hour(&dt_solare)<6, intnx("day", g_solare, -1), g_solare);
	g1_solare=intnx("day",g_solare,1);
	g1_gas=intnx("day", g_gas,1);
	call symput("g_solare",g_solare);
	call symput("g_gas",g_gas);
	call symput("g1_solare",g1_solare);
	call symput("g1_gas",g1_gas);
	format g: date9.;
run;

/* Create dataset bilancio */
data bilancio_lim;
	if day(&g_gas)>10 then
		filter=intnx("month", &g_gas,-2, "end");
	else filter=intnx("month", &g_gas,-3, "end");
	call symput("bilancio_limit", filter);
	format filter date9.;
run;
	
data &input_bilanciosnam;
	set &input_bilanciosnam1(where=(fl_deleted eq "0" ));
	drop k_storico data_elaborazione fl_deleted k_prev_bilancio fl_azione utente_azione dt_azione data;
	date=datepart(data);
	format date date9.;

	if date<=&bilancio_limit then
		validato=1;
	else validato=0;

	if validato eq . then
		validato = 0;
run;
	
proc sort data=&input_bilanciosnam out=&input_bilanciosnam;
	by date;
run;

data dates;
	today_gas=&G_gas;
	start_date_long=intnx("month",today_gas,-37,"B");
	start_date_short=intnx("month",today_gas,-2,"B");
	start_date_long_char=put(start_date_long, date9.);
	start_date_short_char=put(start_date_short, date9.);
	call symputx("start_date_long",start_date_long);
	call symputx("start_date_short",start_date_short);
	call symputx("start_date_long_char",start_date_long_char);
	call symputx("start_date_short_char",start_date_short_char);
run;

%put "======= creating input row at &data_ora =====";
%put "======= downloading data at max from &start_date_long_char =====";

data &input_calendario(where=(data_gas_rif<=&g_gas and data_gas_rif>=&start_date_long));
	date=&start_date_long;
	do while (date<=&g1_solare);
		do hour=0 to 23;
			datetime_solare=dhms(date,hour,0,0);
			if hour<6 then
				data_gas_rif=intnx("day", date, -1);
			else data_gas_rif=date;
			output;
		end;

		date=intnx("day", date, 1, 's');
	end;

	format date date9.;
	format data_gas_rif date9.;
	format datetime_solare datetime19.;
run;




