/*===============================================================*/
/***** rilascio 2020-01-XX - versione v1.0 *****/
/***** autori: Binda, Oliva, Rizzolo *****/
/***** BIP S.p.a. *****/
/*===============================================================*/

/* options nomlogic nomprint nosymbolgen; */
/* options mlogic mprint symbolgen msglevel=i; */
/* ods exclude all; */
/*ods exclude none;*/

/*============ Macro variabili =================================*/
%let dt_solare=&start;
%let libreria_output=&lib_physical;
%let libreria_memory=&lib_memory;

data _null_;
	data_ora=put(&dt_solare,datetime19.);
	call symput("data_ora",data_ora);
run;

%put ------- START &data_ora;

/*********************************/
/* Definizione Macro Variabili */
data init_dates;
	g_solare=Datepart(&dt_solare);
	g_gas=ifn(hour(&dt_solare)<6, intnx("day", g_solare, -1), g_solare);
	g1_solare=intnx("day",g_solare,1);
	g1_gas=intnx("day", g_gas,1);
	call symput("g_solare",g_solare);
	call symput("g_gas",g_gas);
	call symput("g1_solare",g1_solare);
	call symput("g1_gas",g1_gas);
	format g: date9.;
run;

%let input_bilanciosnam1=webdips.prev_bilancio;
%let input_bilanciosnam=work.bilanciosnam_final;
%let input_bilancioscada= oralpbp2.analisi_bilancio;
%let input_calendario=work.calendario_orario_final;
%let input_dlp_orario=oralpbp2.analisi_dlp_orario;
%let input_pcs_misura=webdips.pcs_misura_aop9100;
%let input_riconsegnato1=oralpbp2.riconsegnato;
%let input_riconsegnato2=oralpbp2.consuntivo_view;
%let input_termo=oralpbp2.d_e_up_new;
%let input_sime2=oralpbp2.d_e_misure_sime_2;
%let input_sime=oralpbp2.d_e_misure_sime;
%let input_vol_val=oralpbp2.d_e_volumi_validati;
%let input_terna=ORALPBP2.D_E_TERNA;
%let input_up=ORALPBP2.D_E_UP;
%let input_remi_termo=ORALPBP2.D_E_UP_NEW;
%let anagrafica= WEBDIPS.ANAGRAFICA_PORTATE_SCADA;
%let valori=ORALPBP2.MISURATORI_SCADA;
%let scada_vars=scada_bil_ric scada_bil_exp scada_bil_imp scada_bil_stc scada_bil_pnl scada_bil_dlp;

%let output_celle=&libreria_output..output_meteo_celle;
%let output_citta=&libreria_output..output_meteo_urb;
%let output_scada=&libreria_output..output_scada_base;
%let output_rtot_lag_mvg=&libreria_output..output_rtot_lag_mvg;
%let output_bil_lag_mvg=&libreria_output..output_bil_lag_mvg;
%let output_lagsim=&libreria_output..output_lagsim;
%let output_consuntivo_terna=&libreria_output..output_terna_cons;
%let output_previsione_terna=&libreria_output..output_terna_prev;
%let output_scada_cons=&libreria_output..output_scada_cons;
%let output_rall=&libreria_output..output_rall;
%let output_rdif=&libreria_output..output_rdif;
%let flg_anomalie=0;
/**/

%let GxG_output_final=&libreria_output..&g_output_final_name;
%let GxG_RicTot_base=&GxG_output_final;
%let GxG_RicTot_removed=&libreria_output..&g_output_final_name._prov;
%let GxG_dupp=&libreria_output..&g_output_final_name._dup;
%let GxG_RicTot_cut=work.input_cutds;
%let datetime_var_dup=datetime_solare;
/**/
%let meteo_citta=oralpbp2.d_e_meteo_calc;
%let groups_meteo_citta=&libreria_output..grouping_meteo_citta;
%let meteo_celle_cons=oralpbp2.D_E_METEO_CONSUNTIVO;
%let meteo_celle_prev=oralpbp2.D_E_DATI_METEO_AREE;
%let grouping_all=&libreria_output..grouping_celle_full;
%let grouping_t_med=&libreria_output..grouping_celle_t_med;
%let grouping_t_gg=&libreria_output..grouping_celle_t_gg;
%let grouping_umidita=&libreria_output..grouping_celle_umid;
%let grouping_altezza_min=&libreria_output..grouping_celle_alt;
%let grouping_t_max_percepita=&libreria_output..grouping_celle_tmaxp;

/****************************/
/* Creo ds bilancio */
data bilancio_lim;
	if day(&g_gas)>10 then
		filter=intnx("month", &g_gas,-2, "end");
	else filter=intnx("month", &g_gas,-3, "end");
	call symput("bilancio_limit", filter);
	format filter date9.;
run;

data &input_bilanciosnam;
	set &input_bilanciosnam1(where=(fl_deleted eq "0" ));
	drop k_storico data_elaborazione fl_deleted k_prev_bilancio fl_azione utente_azione dt_azione data;
	date=datepart(data);
	format date date9.;

	if date<=&bilancio_limit then
		validato=1;
	else validato=0;

	if validato eq . then
		validato = 0;
run;

proc sort data=&input_bilanciosnam out=&input_bilanciosnam;
	by date;
run;

/****************************/
%macro GxG_create_dataset_components(start_date);
	/* Creo maschera calendario */
	data &input_calendario(where=(data_gas_rif<=&g_gas and data_gas_rif>=&start_date));
		date=&start_date;

		do while (date<=&g1_solare);
			do hour=0 to 23;
				datetime_solare=dhms(date,hour,0,0);

				if hour<6 then
					data_gas_rif=intnx("day", date, -1);
				else data_gas_rif=date;
				output;
			end;

			date=intnx("day", date, 1, 's');
		end;

		format date date9.;
		format data_gas_rif date9.;
		format datetime_solare datetime19.;
	run;

	%GxG_create_scada(&input_bilancioscada, &start_date, &output_scada);
	%gxg_create_target_rtot(&input_bilanciosnam, &input_dlp_orario, &input_pcs_misura,
		&input_riconsegnato1, &input_riconsegnato2, &output_scada, rtot_parziale, 
		&g_gas, &start_date);
	%gxg_create_tgt_rimanente(rtot_parziale, &output_scada,&output_rdif);
	%gxg_create_lagsimile(rtot_parziale, &input_calendario, &output_lagsim);
	%gxg_create_rtot_lag_mvg(rtot_parziale, &output_lagsim, &start_date, &output_rtot_lag_mvg); /*&output_rtot_lag_mvg*/
	%gxg_create_target_termo(&start_date, &input_termo, &input_sime2,
		&input_sime, &input_vol_val, rcomp_termo_parziale);
	%gxg_create_bil_lag_mvg(&input_bilanciosnam, rtot_parziale, rcomp_termo_parziale, &output_rall, 
		&input_calendario, &output_bil_lag_mvg, &start_date);
	%gxg_create_scadacons(&anagrafica, &valori, &start_date, &output_scada_cons, &dt_solare);
	%GxG_create_dataset_meteo_citta(&meteo_citta, &groups_meteo_citta, &input_calendario, 
		&start_date, &output_citta);
	%GxG_create_dataset_meteo_celle(&meteo_celle_cons, &meteo_celle_prev,&start_date,
		&grouping_all, &output_celle);
	%GXG_create_terna(&input_terna,&input_up,
		&input_remi_termo, &start_date, &output_consuntivo_terna, &output_previsione_terna);

	proc sql noprint;
		select distinct memname into :table_list separated by ' '
			from dictionary.columns
				where libname="WORK" and index(lowcase(memname), "output") =0 
		;
	quit;

	proc datasets lib=work nolist;
		delete &table_list;
	run;

%mend GxG_create_dataset_components;

%macro Create_G_Trainingset();
	%checkds(&GxG_RicTot_base);

	/* se esiste gi� devo accodare gli ultimi 3 mesi*/
	%if &dataset_presente=1 %then
		%do;
			data _null_;
				set &input_bilanciosnam;
				call symputx("start_date",intnx("month",max(date),-17,"B"));
			run;

			data _null_;
				set &input_bilanciosnam;
				call symputx("cut_date",intnx("month",max(date),-2,"B"));
			run;

		%end;

	/* se non esiste devo crearlo da zero */
	%else %if &dataset_presente=0 %then
		%do;
			%let start_date="03JAN2012"d;
			%let cut_date=&G_gas;
		%end;

	%GxG_create_dataset_components(&start_date);
	%GxG_create_dataset_merge(&cut_date);
%mend Create_G_Trainingset;

%Create_G_Trainingset();

%remove_dupp_tset(&GxG_output_final, &GxG_output_final, &GxG_dupp, &datetime_var_dup);

%dset_phy2mem(&g_output_final_name, &libreria_output, &libreria_memory, &g_gas);

data _null_;
	data_ora=put(datetime(),datetime19.);
	call symput("data_ora",data_ora);
run;

%put ------- END &data_ora;

