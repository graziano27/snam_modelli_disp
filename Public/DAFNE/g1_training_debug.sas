/*===============================================================*/
/***** rilascio 2020-09-XX - versione v1.0 *****/
/***** autori:  *****/
/***** BIP S.p.a. *****/
/*===============================================================*/

/*============ Opzioni Generali ================================*/
options mlogic mprint;
ods exclude all;

/*============ Definizione Path ================================*/
%let fs=&lib_physical;
/* %let model_path=&lib_path; */
%let ext = .txt;

/*============ Pulizia Libreria casuser e work =================*/
proc datasets lib=casuser kill nolist;
quit;

proc datasets lib=work kill nolist;
quit;

/*============ Macro variabili =================================*/
%global selected_variables_interval selected_variables_class;
%global selected_inverno_interval selected_inverno_class;
%global selected_estate_interval selected_estate_class;
%global horizon_retain;

/* configurazione */
%let input_grid = &fs..g1_configurazioni;

%let save_var_file=&fs..g1_varsel_test; /* ds dove storicizza le variabili scelte dai modelli */
/*%let hor_train=%sysfunc(dequote(&modello_totrain));*/

%let input_dspre = &fs..&output_final_name.;

/*%let input_dataset = &fs..&hor_train._training_ds_col;*/

/*============ Assegnazione Date ===============================*/

%global start_hour end_hour;

data time;
	datetime_solare=&start;
	dt_train=intnx("hour",datetime_solare,1);
	hour_train=hour(dt_train);
	data_gas_train = ifn(hour_train<6, intnx('day', datepart(dt_train), -1), datepart(dt_train));
	/* devo portarmi all'inizio del mese dopo: vado in avanti di 5 giorni (va bene sia per il g4,g3,g2 e g1) */
	data_base_model=mdy(month(intnx("day",data_gas_train,5)),1,year(intnx("day",data_gas_train,5)));
	mese_pr=put(month(data_base_model),z2.);
	year_pr=put(year(data_base_model),z4.);
	mese_pr_old=put(month(intnx("month",data_base_model,-1)),z2.);
	year_pr_old=put(year(intnx("month",data_base_model,-1)),z4.);
	mese_pr_old2=put(month(intnx("month",data_base_model,-2)),z2.);
	year_pr_old2=put(year(intnx("month",data_base_model,-2)),z4.);
	call symputx('data_gas_train', put(data_gas_train, date9.));
	call symputx("hour_train", put(hour_train, z2.));
	call symputx("mese_pr",mese_pr);
	call symputx("anno_pr",year_pr);
	call symputx("mese_old",mese_pr_old);
	call symputx("anno_old",year_pr_old);
	call symputx("mese_old2",mese_pr_old2);
	call symputx("anno_old2",year_pr_old2);
	format datetime_solare dt_train datetime19. data_gas_train data_base_model date9.;
run;


/* preprocessing sul ds per ottenere variabili specifiche dell'orizzonte + round */
%macro preprocessing_ds(input_ds, modello_totrain, output_ds);

/* %let input_ds=&input_dataset_pre;  */
/* %let output_ds=work.input_ds; */

	data _null_;
		modello_totrain="&modello_totrain";
		keep_vars=compress(cat(compress(modello_totrain),":"));
		tgt=compress(cat(compress(modello_totrain),"_tgt_riconsegnato_totale"));
		call symputx("keep_vars",keep_vars);
		call symputx("tgt",tgt);
	run;
/* %put &=keep_vars; */
	data input_ds;
		set &input_ds. (where = (&tgt ne .) 
						keep=datetime_solare hour &keep_vars);
	run;

	%if "&modello_totrain" eq "G1" or "&modello_totrain" eq "G2" %then %do;
	
		%rename_cols(work.input_ds,
		cat(name, ' = ', substr(name,4)),
		upcase(name) like "&modello_totrain._MET_C%" and name contains "sup");
	
		%rename_cols(work.input_ds,
		cat(name, ' = ', substr(name,4)),
		upcase(name) like "&modello_totrain._MET_P%" and name contains "sup");
	
	%end;
	
	%rename_cols(work.input_ds,
	cat(name, ' = ', substr(name,4)),
	upcase(name) like "&modello_totrain._SCADA%");
	
	%rename_cols(work.input_ds,
	cat(name, ' = ', substr(name,4)),
	upcase(name) like "&modello_totrain._BIL%");
	
	%rename_cols(work.input_ds,
	cat(name, ' = ', substr(name,4)),
	upcase(name) like "&modello_totrain._%");
	
	%round_input_vars(work.input_ds);

	data &output_ds;
		set work.input_ds;
	run;

%mend preprocessing_ds;

%preprocessing_ds(&input_dspre, &modello_totrain, work.input_ds);

proc sql noprint;
	select max(horizon) into :max_step
	from &input_grid (where =(pred_horizon eq "&modello_totrain") )
	;
quit;


data _null_;
	start_date = intnx('month', "&data_gas_train"d, -&max_step, 'B');
	call symputx('start_date', put(start_date, date9.));
run;
%let start_date = "&start_date"d;

/*%put &=start_date;*/

proc sql noprint;
	select max(data_gas_rif) format date9. into: end_date
	from input_ds /*(where = (&target_id ne .))*/;
quit;

%let end_date = "&end_date"d;

%put -----------------------;
%put &=start_date &=end_date;
%put -----------------------;

/*============ Compilazione funzioni =================*/
%macro split_data(dsin, start, end, dataset, target_table);

	data temp_prov;
		set &dsin;
		if &start <=data_gas_rif < &end;
	run;

	data temp;
		set temp_prov;
		time_month=month(data_gas_rif);
		time_day=weekday(data_gas_rif);
		time_week=week(data_gas_rif);
	run;

	data temp;
		set temp;
		time_day_7=0;
		time_day_6=0;
		time_day_1=0;
		time_day_2=0;
		time_month_7=0;
		time_month_4=0;
		time_month_12=0;
		time_month_5=0;
		time_festivo=0;
		time_prefestivo=0;

		if day(data_gas_rif)=1 and month(data_gas_rif)=1 then
			time_festivo=1;

		if day(data_gas_rif)=6 and month(data_gas_rif)=1 then
			time_festivo=1;

		if day(data_gas_rif)=25 and month(data_gas_rif)=4 then
			time_festivo=1;

		if day(data_gas_rif)=1 and month(data_gas_rif)=5 then
			time_festivo=1;

		if day(data_gas_rif)=2 and month(data_gas_rif)=6 then
			time_festivo=1;

		if day(data_gas_rif)=15 and month(data_gas_rif)=8 then
			time_festivo=1;

		if day(data_gas_rif)=1 and month(data_gas_rif)=11 then
			time_festivo=1;

		if day(data_gas_rif)=8 and month(data_gas_rif)=12 then
			time_festivo=1;

		if data_gas_rif=holiday('EASTER', year(data_gas_rif)) then
			time_festivo=1;

		if data_gas_rif=intnx('day', holiday('EASTER', year(data_gas_rif)), 1) then
			time_festivo=1;

		if data_gas_rif=intnx('day', holiday('EASTER', year(data_gas_rif)), 2) then
			time_festivo=1;

		if day(data_gas_rif)=25 and month(data_gas_rif)=12 then
			time_festivo=1;

		if day(data_gas_rif)=26 and month(data_gas_rif)=12 then
			time_festivo=1;

		if day(data_gas_rif)=31 and month(data_gas_rif)=12 then
			time_festivo=1;

		if day(data_gas_rif)=5 and month(data_gas_rif)=1 then
			time_prefestivo=1;

		if day(data_gas_rif)=24 and month(data_gas_rif)=4 then
			time_prefestivo=1;
		*if data_gas_rif = intnx('day',holiday('EASTER',year(data_gas_rif)),-1) then time_prefestivo = 1;

		if day(data_gas_rif)=30 and month(data_gas_rif)=4 then
			time_prefestivo=1;

		if day(data_gas_rif)=1 and month(data_gas_rif)=6 then
			time_prefestivo=1;

		if day(data_gas_rif)=14 and month(data_gas_rif)=8 then
			time_prefestivo=1;

		if day(data_gas_rif)=31 and month(data_gas_rif)=10 then
			time_prefestivo=1;

		if day(data_gas_rif)=7 and month(data_gas_rif)=12 then
			time_prefestivo=1;

		if day(data_gas_rif)=24 and month(data_gas_rif)=12 then
			time_prefestivo=1;

		if day(data_gas_rif)=30 and month(data_gas_rif)=12 then
			time_prefestivo=1;
		time_week_52=0;
		time_week_32=0;
		time_week_51=0;
		time_week_33=0;

		if time_day=7 then
			time_day_7=1;

		if time_day=6 then
			time_day_6=1;

		if time_day=1 then
			time_day_1=1;

		if time_day=2 then
			time_day_2=1;

		if time_month=7 then
			time_month_7=1;

		if time_month=4 then
			time_month_4=1;

		if time_month=12 then
			time_month_12=1;

		if time_month=5 then
			time_month_5=1;

		if time_week=52 then
			time_week_52=1;

		if time_week=32 then
			time_week_32=1;

		if time_week=51 then
			time_week_51=1;

		if time_week=33 then
			time_week_33=1;
		drop time_week time_month;
	run;

	data temp_ftd;
		set temp;
		fake_time_day=time_day;
		time_natale=0;
		time_ferragosto=0;

		if time_festivo=1 and time_day=2 then
			fake_time_day=5;

		if time_festivo=1 and time_day=3 then
			fake_time_day=5;

		if time_festivo=1 and time_day=4 then
			fake_time_day=7;

		if time_festivo=1 and time_day=5 then
			fake_time_day=7;

		if time_festivo=1 and time_day=6 then
			fake_time_day=7;

		if data_gas_rif=intnx('day', holiday('EASTER', year(data_gas_rif)), 0) then
			fake_time_day=1;

		if data_gas_rif=intnx('day', holiday('EASTER', year(data_gas_rif)), 1) then
			fake_time_day=5;

		/*natale*/
		/*da sabato prima di natale a domenica dopo befana*/
		if month(data_gas_rif)=12 and data_gas_rif >=intnx("week", mdy(12, 25, 
			year(data_gas_rif)), -1, 'E') and weekday(mdy(12, 25, year(data_gas_rif))) 
			< 7 then
				do;
				time_natale=1;
			end;
		else if month(data_gas_rif)=12 and data_gas_rif >=intnx("day", mdy(12, 25, 
			year(data_gas_rif)), -1) and weekday(mdy(12, 25, year(data_gas_rif)))=7 then
				do;
				time_natale=1;
			end;

		if month(data_gas_rif)=1 and data_gas_rif<=intnx("week", mdy(1, 6, 
			year(data_gas_rif)), 0, 'E') and weekday(mdy(1, 6, year(data_gas_rif))) > 1 
			then
				do;
				time_natale=1;
			end;
		else if month(data_gas_rif)=1 and data_gas_rif <=mdy(1, 6, 
			year(data_gas_rif)) and weekday(mdy(1, 6, year(data_gas_rif)))=1 then
				do;
				time_natale=1;
			end;

		/*ferragosto*/
		/*tutta la settimana di ferragosto da sabato prima a domenica*/
		if data_gas_rif >=intnx("day", mdy(8, 15, year(data_gas_rif)), -2) and 
			data_gas_rif <=intnx("day", mdy(8, 15, year(data_gas_rif)), 2) then
				do;
				time_ferragosto=1;
			end;
	run;

	data temp_new_vars;
		set temp_ftd;
		time_weekend=0;
		time_weekend_ext=0;
		time_day_7_ext=0;
		time_day_7_ext_holy=0;
		time_festivo_ext=0;
		fake_time_day_group1=0;
		fake_time_day_group2=0;
		time_risc_zona_A=0;

		/* 3 comuni --> approssimo a 0.5 province */
		time_risc_zona_B=0;

		/* 8 province */
		time_risc_zona_C=0;

		/* 16 province */
		time_risc_zona_D=0;

		/* 30 province */
		time_risc_zona_E=0;

		/* 45 province */
		time_risc_zona_F=1;

		/* 3 province - No limitazioni */
		time_day_group1=0;
		time_day_group2=0;

		if time_day_7=1 or time_festivo=1 then
			do;
				time_day_7_ext=1;

				/* Sabato esteso ai festivi */
				time_day_7_ext_holy=1;

				/* Sabato esteso ai festivi e ai periodi di vacanza */
			end;

		if time_festivo=1 then
			time_festivo_ext=1;

		if time_ferragosto=1 or time_natale=1 then
			do;
				time_day_7_ext_holy=1;
				time_festivo_ext=1;
			end;

		if time_day in (1, 7) then
			time_weekend=1;

		if time_day in (1, 7) or time_festivo=1 then
			time_weekend_ext=1;

		if (month(data_gas_rif) in (12, 1, 2)) or (month(data_gas_rif)=3 and 
			day(data_gas_rif) <=15) then
				time_risc_zona_A=1;

		if (month(data_gas_rif) in (12, 1, 2, 3)) then
			time_risc_zona_B=1;

		if (month(data_gas_rif) in (12, 1, 2, 3)) or (month(data_gas_rif)=11 and 
			day(data_gas_rif) >=15) then
				time_risc_zona_C=1;

		if (month(data_gas_rif) in (11, 12, 1, 2, 3)) or (month(data_gas_rif)=4 and 
			day(data_gas_rif) <=15) then
				time_risc_zona_D=1;

		if (month(data_gas_rif) in (11, 12, 1, 2, 3)) or (month(data_gas_rif)=4 and 
			day(data_gas_rif) <=15) or (month(data_gas_rif)=10 and day(data_gas_rif) 
			>=15) then
				time_risc_zona_E=1;

		/* Riscaldamento complessivo */
		time_risc_totale=time_risc_zona_A*(0.5/102.5) + time_risc_zona_B*(8/102.5) 
			+ time_risc_zona_C*(16/102.5) + 
					   time_risc_zona_D*(30/102.5) + time_risc_zona_E*(45/102.5) 
			+ time_risc_zona_F*(3/102.5);

		if fake_time_day in (2, 3, 4, 5, 6) then
			fake_time_day_group1=1;
		else if fake_time_day in (1, 7) then
			fake_time_day_group2=1;

		if time_day in (2, 3, 4, 5, 6) and time_festivo=0 then
			time_day_group1=1;

		if time_day in (1, 7) or time_festivo=1 then
			time_day_group2=1;
		drop time_day fake_time_day;
	run;

	data &dataset;
		set temp_new_vars;
		data_gas_rif_364=intnx("day", data_gas_rif, -364);
		data_gas_rif_366=intnx("day", data_gas_rif, -366);
		time_festivo_lag=0;

		if day(data_gas_rif_364)=1 and month(data_gas_rif_364)=1 then
			time_festivo_lag=1;
		else if day(data_gas_rif_366)=1 and month(data_gas_rif_366)=1 then
			time_festivo_lag=1;

		if day(data_gas_rif_364)=6 and month(data_gas_rif_364)=1 then
			time_festivo_lag=1;
		else if day(data_gas_rif_366)=6 and month(data_gas_rif_366)=1 then
			time_festivo_lag=1;

		if day(data_gas_rif_364)=25 and month(data_gas_rif_364)=4 then
			time_festivo_lag=1;
		else if day(data_gas_rif_366)=25 and month(data_gas_rif_366)=4 then
			time_festivo_lag=1;

		if day(data_gas_rif_364)=1 and month(data_gas_rif_364)=5 then
			time_festivo_lag=1;
		else if day(data_gas_rif_366)=1 and month(data_gas_rif_366)=5 then
			time_festivo_lag=1;

		if day(data_gas_rif_364)=2 and month(data_gas_rif_364)=6 then
			time_festivo_lag=1;
		else if day(data_gas_rif_366)=2 and month(data_gas_rif_366)=6 then
			time_festivo_lag=1;

		if day(data_gas_rif_364)=15 and month(data_gas_rif_364)=8 then
			time_festivo_lag=1;
		else if day(data_gas_rif_366)=15 and month(data_gas_rif_366)=8 then
			time_festivo_lag=1;

		if day(data_gas_rif_364)=1 and month(data_gas_rif_364)=11 then
			time_festivo_lag=1;
		else if day(data_gas_rif_366)=1 and month(data_gas_rif_366)=11 then
			time_festivo_lag=1;

		if day(data_gas_rif_364)=8 and month(data_gas_rif_364)=12 then
			time_festivo_lag=1;
		else if day(data_gas_rif_366)=8 and month(data_gas_rif_366)=12 then
			time_festivo_lag=1;

		if data_gas_rif_364=holiday('EASTER', year(data_gas_rif_364)) then
			time_festivo_lag=1;
		else if data_gas_rif_366=holiday('EASTER', year(data_gas_rif_366)) then
			time_festivo_lag=1;

		if data_gas_rif_364=intnx('day', holiday('EASTER', year(data_gas_rif_364)), 
			1) then
				time_festivo_lag=1;
		else if data_gas_rif_366=intnx('day', holiday('EASTER', 
			year(data_gas_rif_366)), 1) then
				time_festivo_lag=1;

		if data_gas_rif_364=intnx('day', holiday('EASTER', year(data_gas_rif_364)), 
			2) then
				time_festivo_lag=1;
		else if data_gas_rif_366=intnx('day', holiday('EASTER', 
			year(data_gas_rif_366)), 2) then
				time_festivo_lag=1;

		if day(data_gas_rif_364)=25 and month(data_gas_rif_364)=12 then
			time_festivo_lag=1;
		else if day(data_gas_rif_366)=25 and month(data_gas_rif_366)=12 then
			time_festivo_lag=1;

		if day(data_gas_rif_364)=26 and month(data_gas_rif_364)=12 then
			time_festivo_lag=1;
		else if day(data_gas_rif_366)=26 and month(data_gas_rif_366)=12 then
			time_festivo_lag=1;

		if day(data_gas_rif_364)=31 and month(data_gas_rif_364)=12 then
			time_festivo_lag=1;
		else if day(data_gas_rif_366)=31 and month(data_gas_rif_366)=12 then
			time_festivo_lag=1;
		format data: date9.;
		time_day_group_fake1=1;
	run;

	PROC SQL noprint;
		create table &target_table as select distinct intnx("year", data_gas_rif, 1, 
			'S') as data_gas_rif format=date9., tgt_riconsegnato_totale as target 
			from &dataset where catx("_", day(data_gas_rif), month(data_gas_rif)) ne 
			"29_2" order by data_gas_rif;
	run;

%mend split_data;

%macro get_train_back(date_from=, date_to=, step=, I=, input_dataset=, 
		training_eff=, td_gr=, gr_col=);

	data &training_eff;
		set &input_dataset;

		if hour=&hour_train;
		lim_inf_train=intnx('month', &date_to, &I-&step);
		lim_sup_train=mdy(month(intnx('month', &date_to, &I, 'E')), 
			day(intnx('month', &date_to, &I, 'E'))-2, year(intnx('month', &date_to, &I, 
			'E')));
		format lim_inf_train date9. lim_sup_train date9.;
		where &gr_col.&td_gr=1;

		if lim_inf_train <=data_gas_rif <= lim_sup_train;
	run;

%mend;

%macro do_varreduce(pred_horizon, tipo_modello, var_sel, td_gr, vars_hpred, 
					stdize, force_drop, training_in, season, stack_level,save_var_file, id);

	
	
	%if &season eq 'all' %then %do;
		%if &stdize ne no %then %do;
			proc stdize data=&training_in out=training_in method=range;
				var &stdize;
			run;
		%end;
		%else %do;
			data training_in;
				set &training_in;
			run;
		%end;

		%let key_out = variables;
		%let mod_name = globale;
	%end;
	%else %do;

		data training_in;
			set &training_in;
		run;

		%if &season eq 'inverno' %then %do;
				%let mod_name = winter;
				%let key_out = inverno;
		%end;
		%else %do;
				%let mod_name = summer;
				%let key_out = estate;
		%end;
	%end;

	proc sql noprint;
		select name into :scada_bil_drop separated by ' ' 
		from dictionary.columns 
			where libname='WORK' and lowcase(memname)=lowcase('training_in') and name 
			like "scada_%" and (name contains "bil_lag" or name contains "bil_mvg" or 
			name contains "kwh" or name contains "smooth" or name contains "lag");
	quit;

	%let force_drop=%sysfunc(tranwrd(&force_drop, scada_bil_new, &scada_bil_drop));

	/* bringing table to casuser, in order to perform varreduce */
	data casuser.training_in(drop=&force_drop);
		set work.training_in;
	run;

	proc varreduce data=casuser.training_in technique=var;
		class time:;
		reduce supervised &target_id=&vars_hpred / varexp=&var_sel;
		displayout SelectionSummary=var_exp_selection_eff;
	run;

	/* Inserisco livello, modello, anno e mese di previsione, etc.. */
	data casuser.var_exp_selection_eff;
		length level $ 32;
		set casuser.var_exp_selection_eff;

		if variable =: 'time' then
			level='';
		else
			level="-";
		key_modello=put("&tipo_modello.", 32.);
		var_sel=&var_sel;
		stack_level=&stack_level;
	run;

	/* Decido se aggiungere variabili forzatamente a quelle selezionate tramite varreduce */
	%if &stack_level=2 %then %do;

		proc sql noprint;
			select name into :force_vars separated by ' '
			from dictionary.columns
			where libname="CASUSER"
			and memname="TRAINING_IN"
			and (name like "mape_%" or name like "p_%");
		quit;

	/* Aggiungo le variabili forzate (sia le time che non)*/
		%let count_fv=%sysfunc(countw(&force_vars));

		data var_exp_selection_eff_add(keep=variable level);
			length variable $32.;

			%do i_force=1 %to &count_fv;
				%let value=%scan(&force_vars, &i_force);
				variable="&value";
				level="-";
				output;
			%end;

		run;

		data casuser.var_exp_selection_eff;
			length variable $ 32;
			set casuser.var_exp_selection_eff var_exp_selection_eff_add;
		run;

	%end;

	data work.var_exp_selection_eff;
		set casuser.var_exp_selection_eff;
		time_day_group=&td_gr;
	run;

	proc sort data=var_exp_selection_eff nodupkey;
		by variable time_day_group;
	run;

	 data var_exp_selection_eff;
        set var_exp_selection_eff;
        length modello $10;
        season="&season";
        if season="'all'" then modello="globale";
        else if season="'inverno'" then modello="inverno";
        else if season="'estate'" then modello='estate';
        sottomodello="&tipo_modello";
        pred_horizon="&pred_horizon";
        id=&id;
        mese_pred="&mese_pr";
        anno_pred="&anno_pr";
        hour_train="&hour_train";
        hour=cat("(",hour_train,")");
        target="&target_id";
    run;

	/* Salvo il ds finale in append */
	%if %sysfunc(exist(&save_var_file)) %then
		%do;

			proc append base=&save_var_file
						data=var_exp_selection_eff force;
			run;

		%end;
	%else
	%do;

		data &save_var_file;
			stop;
			length id 8. pred_horizon $2 approccio $31 time_day_group $5 modello $10 sottomodello $5 target $24 variable $32 mese_pred $2 anno_pred $4 hour $100;
		run;

		proc append base=&save_var_file 
					data=var_exp_selection_eff force;
		run;

	%end;


	proc sql noprint;
		select month(intnx("month", max(data_gas_rif), 1, 'B')), year(intnx("month", 
			max(data_gas_rif), 1, 'B')) into :mese_pred, :anno_pred from training_eff;
	quit;

	data var_exp_add;
		set var_exp_selection_eff;
		modello="&mod_name";
		mese_pred=&mese_pred;
		anno_pred=&anno_pred;
	run;

	proc sql;
		create table work.selected_var_interval as select variable from 
			var_exp_selection_eff where level like "%-%" order by mse desc;
	quit;

	data _null_(drop=new_var);
		length var_list $30000;
		retain var_list '';
		set work.selected_var_interval end=eof;

		if _N_=1 then
			do;
				var_list=variable;
				output;
			end;
		else
			do;
				new_var=compress(variable);
				var_list=catx (' ', var_list, new_var);
				output;
			end;

		if eof then
			call symput ("SELECTED_&key_out._INTERVAL", var_list);
	run;

	proc sql;
		create table work.selected_var_class as select variable from 
			var_exp_selection_eff where level not like "%-%" order by mse desc;
	quit;

	proc sort data=work.selected_var_class out=work.selected_var_class nodupkey;
		by variable;
	run;

	proc sql noprint;
		select count(*) into :obs_count from selected_var_class;
	quit;

	%if (&obs_count ne 0) %then
		%do;

			data _null_;
				length var_list $30000;
				retain var_list '';
				set work.selected_var_class end=eof;

				if _N_=1 then
					do;
						var_list=variable;
					end;
				else
					do;
						new_var=compress(variable);
						var_list=catx (' ', var_list, new_var);
					end;

				if eof then
					var_class="input " || strip(var_list) || " /level=NOM";
				call symputx("SELECTED_&key_out._CLASS", var_class);
			run;

		%end;
	%else
		%do;

			data _null_;
				length var_class $ 32;
				var_class="*input /level=NOM";
				call symputx("SELECTED_&key_out._CLASS", var_class);
			run;

		%end;
%mend do_varreduce;


%macro clean_models(model=);

	filename myDir "&model_path./";

	data get;
		did=dopen("myDir");
		filecount=dnum(did);
		do i = 1 to filecount;
			file_ = dread(did, i);
			output;
		end;
		rc=dclose(did);
	run;

	data to_delete;
		set get;
		where file_ contains "&model";
		fname="tempfile";
		rc=filename(fname, cat("&model_path./", compress(file_)));
		rc=fdelete(fname);
	run;

%mend;

%macro archiviazione(old_rete);
	
	filename glob "&model_path./&old_rete.&ext";
	filename addfile zip "&model_path./old_models.zip" 
		member="models/&old_rete";

	data _null_;
		infile glob recfm=n;
		file addfile recfm=n;
		input byte $char1. @;
		put  byte $char1. @;
	run;

	filename addfile clear;

%mend;

%macro archivia_modelli(horizon, key_modello, anno_old1, mese_old1, anno_old2, mese_old2, hour_train);

	%let mod_list = GL WI SU;

	%if &key_modello=S %then
		%let td_gr=2;
	%else
		%let td_gr=1;

	%do td_c = 1 %to &td_gr;
		
		%if &key_modello=S %then
			%let td_ext=&td_c;
		%else
			%let td_ext=;
		
		%do prev = 1 %to 2;
			%do i=1 %to %sysfunc(countw(&mod_list));
				%let mod=%scan(&mod_list, &i);
		
				data name_model;
					old_name = compress(catx("_","&horizon","&key_modello.&td_ext","&mod","&&anno_old&prev","&&mese_old&prev","&hour_train"));
					call symputx("old_name", old_name);
				run;

				%if %sysfunc(fileexist("&model_path./&old_name.&ext")) %then %do;
					%archiviazione(&old_name);
					%clean_models(model=&old_name)
				%end;
			%end;
		%end;
	%end;
	
%mend;

%macro do_nnet(horizon, hour_train, J, dataset, nome_modello, var_sel, 
				target_table, string_layer1, string_layer2, string_layer3, n_td, epochs, 
				solver, vars_hpred, vars_hpred_summer, vars_hpred_winter, stdize, 
				force_drop, id_gl, id_st);

/* %let dataset=dataset; */
/* %let nome_modello=&key_modello; */
/* %let target_table=target_table; */

	data _null_;
		length gr_col $19.;
		n_td=&n_td;
		force_drop="&force_drop";
		if n_td=2 then gr_col="time_day_group";
		else gr_col="time_day_group_fake";
		if force_drop="" then force_drop="FAKE";
		call symputx("gr_col",gr_col);
		call symputx("force_drop",force_drop);
	run;
	
	%do td_gr=1 %to &n_td;

		%if &key_modello=S %then
			%let td_ext=&td_gr;
		%else
			%let td_ext=;

		data _null_;
			model_name=upcase(compress(catx("_","&horizon","&key_modello.&td_ext","GL","&anno_pr","&mese_pr","&hour_train")));
			model_name_wi=upcase(compress(catx("_","&horizon","&key_modello.&td_ext","WI","&anno_pr","&mese_pr","&hour_train")));
			model_name_su=upcase(compress(catx("_","&horizon","&key_modello.&td_ext","SU","&anno_pr","&mese_pr","&hour_train")));

			call symputx("model_name", model_name);
			call symputx("model_name_wi", model_name_wi);
			call symputx("model_name_su", model_name_su);
		run;

		%get_train_back(date_from=&start_date, date_to=intnx('day', &end_date, -1),
						step=&J, I=0, input_dataset=&dataset, training_eff=training_eff, 
						td_gr=&td_gr, gr_col=&gr_col);

		proc sort data=training_eff;
			by data_gas_rif hour;
		run;

		data casuser.training_eff;
			set work.training_eff;
		run;

		/* CICLO GLOBALE -- PRENDO TUTTI I MESI */
		%if %sysfunc(fileexist("&model_path./&model_name.&ext")) %then %do;
		%end;
		%else %do;
			%do_varreduce(&horizon, &nome_modello, &var_sel, &td_gr, &vars_hpred, 
						&stdize, &force_drop, training_eff, 'all', 1,&save_var_file, &id_gl);

			proc nnet data=casuser.training_eff;
				input &SELECTED_VARIABLES_interval /level=INT;
				&SELECTED_VARIABLES_CLASS;
				target &target_id / level=int;
				&string_layer1;

				%if %length(&string_layer2) %then
					%do;
						&string_layer2;
					%end;

				%if %length(&string_layer3) %then
					%do;
						&string_layer3;
					%end;
				optimization algorithm=&solver maxiter=&epochs;
				train outmodel=casuser.model_neural seed=12345;
				code file="&model_path./&model_name.&ext";
			run;
		%end;

		/* CICLO STAGIONALE -- PRENDO SOLO ALCUNI MESI -- A SECONDA DEL MESE DA PREDIRE TRAINO SU ESTATE/INVERNO */
		/* MESE DA PREDIRE */
/*		data _null_;*/
/*			mese_pr=input(&mese_pr,best32.);*/
/*			if mese_pr in (4, 5, 6, 7, 8, 9) then flg_winter=0;*/
/*			else flg_winter=1;*/
/*			call symputx("flg_winter",flg_winter);*/
/*		run;*/
/**/
/*		%if &flg_winter=1 %then %do;*/

			%if %sysfunc(fileexist("&model_path./&model_name_wi.&ext")) %then %do;
			%end;
			%else %do;
				/* ciclo invernale */
				data training_inverno;
					set casuser.training_eff;

					if month(data_gas_rif) in (1, 2, 3, 9, 10, 11, 12);
				run;

				%do_varreduce(&horizon,&nome_modello, &var_sel, &td_gr, &vars_hpred_winter, 
								&stdize, &force_drop, training_inverno, 'inverno', 1,&save_var_file, &id_st);

				proc sort data=work.training_inverno;
					by data_gas_rif hour;
				run;

				data casuser.training_inverno;
					set work.training_inverno;
				run;

				proc nnet data=casuser.training_inverno;
					input &SELECTED_inverno_INTERVAL /level=INT;
					&SELECTED_inverno_CLASS;
					target &target_id / level=int;
					&string_layer1;

					%if %length(&string_layer2) %then
						%do;
							&string_layer2;
						%end;

					%if %length(&string_layer3) %then
						%do;
							&string_layer3;
						%end;
					optimization algorithm=&solver maxiter=&epochs;
					train outmodel=casuser.model_neural_winter seed=12345;
					code file="&model_path./&model_name_wi.&ext";
				run;
			%end;
/*		%end;*/

		/*CICLO ESTIVO (SOLO SE FLG_WINTER=0) */

/*		%else %do;*/

			%if %sysfunc(fileexist("&model_path./&model_name_su.&ext")) %then %do;
			%end;
			%else %do;
				/* ciclo estivo */			
				data training_estate;
					set casuser.training_eff;
					if month(data_gas_rif) in (3, 4, 5, 6, 7, 8, 9);
				run;

				%do_varreduce(&horizon, &nome_modello, &var_sel, &td_gr, &vars_hpred_summer, 
								&stdize, &force_drop, training_estate, 'estate', 1,&save_var_file, &id_st);

				proc sort data=work.training_estate;
					by data_gas_rif hour;
				run;

				data casuser.training_estate;
					set work.training_estate;
				run;

				proc nnet data=casuser.training_estate;
					input &SELECTED_ESTATE_INTERVAL /level=INT;
					&SELECTED_ESTATE_CLASS;
					target &target_id / level=int;
					&string_layer1;

					%if %length(&string_layer2) %then
						%do;
							&string_layer2;
						%end;

					%if %length(&string_layer3) %then
						%do;
							&string_layer3;
						%end;
					optimization algorithm=&solver maxiter=&epochs;
					train outmodel=casuser.model_neural_summer seed=12345;
					code file="&model_path./&model_name_su.&ext";
				run;
			%end;
/*		%end;*/

	%end;
%mend do_nnet;

%macro do_training(horizon, hour_train, thisTry_layer1, thisTry_strut_layer1, 
						thisTry_layer2, thisTry_strut_layer2, thisTry_layer3, 
						thisTry_strut_layer3, var_sel, J, epochs, solver, 
						vars_hpred, vars_hpred_summer, vars_hpred_winter,
						stdize, force_drop, key_modello, target_id, id);

	/* Definisco id per valorizzarli correttamente nella tabella di storico delle variabili selezionate */
	data id;
		id_gl=&id; /* ho filtrato la griglia per GL */
		id_st=id_gl-1; /* id dello stagionale è sempre GL -1 */
		call symputx("id_gl",id_gl);
		call symputx("id_st",id_st);
	run;

	data _null_;
		key_modello="&key_modello";
		if key_modello="S" then n_td=2;
		else n_td=1;
		call symputx("n_td",n_td);
	run;

	data _null_;
		length var_class $ 32;
		var_class_1=cat("hidden " , &thisTry_layer1, " / act = ", 
			"&thisTry_strut_layer1");

		%if &thisTry_layer2 ne 0 and &thisTry_layer2 ne . %then
			%do;
				var_class_2=cat("hidden " , &thisTry_layer2, " / act = ", 
					"&thisTry_strut_layer2");
			%end;
		%else
			%do;
				var_class_2="";
			%end;

		%if &thisTry_layer3 ne 0 and &thisTry_layer3 ne . %then
			%do;
				var_class_3=cat("hidden " , &thisTry_layer3, " / act = ", 
					"&thisTry_strut_layer3");
			%end;
		%else
			%do;
				var_class_3="";
			%end;
		call symput('string_layer1', var_class_1);
		call symput('string_layer2', var_class_2);
		call symput('string_layer3', var_class_3);
	run;

	data predictions_&key_modello;
		stop;
		length data_gas_rif 8 key_modello $32 hour 8 tgt_riconsegnato_totale 8 
			P_tgt_riconsegnato_totale 8 P_tgt_riconsegnato_glob 8 P_tgt_riconsegnato_stag 8
			P_tgt_riconsegnato_winter 8 P_tgt_riconsegnato_summer 8 stack_level 8 
		 	time_day_group 8;
	run;

	%do_nnet(&horizon, &hour_train, &j, dataset, &key_modello, &var_sel, 
				target_table, &string_layer1, &string_layer2, &string_layer3, &n_td,
				&epochs, &solver, &vars_hpred, &vars_hpred_summer, &vars_hpred_winter, 
				&stdize, &force_drop, &id_gl, &id_st);

	%archivia_modelli(&horizon, &key_modello, &anno_old, &mese_old, &anno_old2, &mese_old2, &hour_train);

%mend do_training;

%macro execute_configs();

	%split_data(input_ds, &start_date, &end_date, dataset, target_table);

	data _null_;
		set &input_grid(where=(pred_horizon eq "&modello_totrain" and modello eq "GL") );
		hour_train = "&hour_train";

		call execute ('%NRSTR (%do_training('||pred_horizon||','||hour_train||','||nunits1||','||act1||',
		'||nunits2||','||act2||','||nunits3||','||act3||','||var_sel||','||horizon||','||epochs||',
		'||solver||','||vars_hpred||','||vars_hpred_summer||','||vars_hpred_winter||','||stdize_vars||',
		'||drop_vars||', '||sottomodello||','||target||','||id||'))');
	run;

%mend execute_configs;

%execute_configs();
/* cas mySession terminate; */