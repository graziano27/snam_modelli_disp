
/*===============================================================*/
/***** rilascio 2020-01-XX - versione v1.0 *****/
/***** autori: Binda, Oliva, Rizzolo *****/
/***** BIP S.p.a. *****/
/*===============================================================*/

/* options nomlogic nomprint nosymbolgen; */
/* options mlogic mprint symbolgen msglevel=i; */
/* ods exclude all; */
/*ods exclude none;*/

/*============ Macro variabili =================================*/
/* Definizione macro variabili Globali */
%let dt_solare=&start;
%let libreria_input = &lib_physical;
%let dset_input_name = previsioni_riconsegnato; 
%let dataset_input = &libreria_input..&dset_input_name;
%let libreria_output = &lib_physical;
%let dset_output_name = output_models_viya; 
%let dataset_output = &libreria_output..&dset_output_name;
/* %let ensemble_id = 9 10 11 12; */
%let g_finalpred_id = &ensemble_id;


%macro get_predictions_g(dataset_input=, time_id=);

	%if %sysfunc(exist(&dataset_input)) %then
		%do;

		proc sort data=&dataset_input;
			by &time_id;
		run;

		data time;
			set &dataset_input end=eof;
			if eof then 
				do;
					call symputx("last_hour",compress(put(hour,z2.)));
					call symputx("last_date",data_gas_rif);
					call symputx("hour_pred",(put(hour(&dt_solare),z2.)));
					call symputx("date_pred",compress(put(datepart(&dt_solare),date9.)));
				end;
		run;

		data preds_g_topub(keep= PREV DT_RIFERIMENTO_DATI unita_misura Tipologia_previsione
			modello dt_calcolo);
			length DT_CALCOLO 8 DT_RIFERIMENTO_DATI 8 PREV 8 UNITA_MISURA $ 30 
			Tipologia_Previsione $ 30 MODELLO $ 30;
			set &dataset_input(Rename=(previsione=PREV));
			where id in (&g_finalpred_id) and data_gas_rif = &last_date
			and hour = &last_hour;
			dt_calcolo = datetime();
			DT_RIFERIMENTO_DATI = dhms(&last_date, 0, 0, 0);
			UNITA_MISURA = 'KWH';
			if target = "IND" then Tipologia_Previsione = 'Industriale';
			if target = "CIV" then Tipologia_Previsione = 'Civile';
			if target = "TERM" then Tipologia_Previsione = 'Termoelettrico';
			if target = "TOT" then Tipologia_Previsione = 'RiconsTot';
			MODELLO = 'Predizione G per G';
/* 			format datetime: datetime19.; */
			format dt: datetime19.;
		run;

	%end;

%mend;

%macro prev();
	/* controllo l'esistenza della tabella finale, in caso contrario la definisco */
	%if not(%sysfunc(exist(&dataset_output))) %then
		%do;

			data &dataset_output;
				stop;
				length DT_CALCOLO 8 DT_RIFERIMENTO_DATI 8 PREV 8 UNITA_MISURA $ 30 Tipologia_Previsione $ 30 MODELLO $ 30;
				format DT_CALCOLO datetime19. DT_RIFERIMENTO_DATI datetime19.;
			run;

		%end;

	/* raccolta previsioni g */
	%get_predictions_g(dataset_input=&dataset_input, time_id=datetime_solare);

	data &dataset_output;
		set &dataset_output preds_g_topub;
	run;
	
	proc sort data= &dataset_output nodupkey;
		by DT_CALCOLO DT_RIFERIMENTO_DATI MODELLO Tipologia_Previsione;
	run;

%mend;

%prev();
