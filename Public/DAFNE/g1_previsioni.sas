/*===============================================================*/
/***** rilascio 2020-09-XX - versione v1.0 *****/
/***** autori:  *****/
/***** BIP S.p.a. *****/
/*===============================================================*/

/*============ Opzioni Generali ================================*/
/* options mlogic mprint; */
/* ods exclude all; */

/*============ Definizione Path ================================*/
%let lib_input=&lib_physical;
%let lib_output=&lib_physical;

/*============ Macro variabili =================================*/
%let time_id = data_gas_rif;

/*============ Dataset di input / output =================================*/
%let input_dataset = &lib_input..&output_final_name.;
%let input_grid = &lib_input..g1_configurazioni;
%let output_prev = &lib_output..g1_previsioni_riconsegnato;


/* Prendo l'ultima riga del dataset di input */
data input_row;
	set &input_dataset end=eof;
	if eof;
run;


/*data check;*/
/*	set input_row;*/
/*	check = nmiss(of _numeric_);*/
/*	keep da: check;*/
/*	call symputx("check",check);*/
/*run;*/
/**/
/*%put "TROVATI &check VALORI MANCANTI";*/

data time;
	datetime=&start;
	hour_pred=hour(datetime);
	call symputx("hour_pred",hour_pred);
	format datetime datetime19.;
run;


%macro preprocess_row(pred_horizon, row_in, row_out);

	data &row_out(keep=datetime_solare hour &pred_horizon.:);
		set &row_in;
	run;
	
	%if &pred_horizon. eq G1 or &pred_horizon. eq G2 %then %do;

		%rename_cols(work.&row_out,
		cat(name, ' = ', substr(name,4)),
		upcase(name) like "&pred_horizon._MET_C%" and name contains "sup");

		%rename_cols(work.&row_out,
		cat(name, ' = ', substr(name,4)),
		upcase(name) like "&pred_horizon._MET_P%" and name contains "sup");
	
	%end;

	%rename_cols(work.&row_out,
	cat(name, ' = ', substr(name,4)),
	upcase(name) like "&pred_horizon._SCADA%");

	%rename_cols(work.&row_out,
	cat(name, ' = ', substr(name,4)),
	upcase(name) like "&pred_horizon._BIL%");

	%rename_cols(work.&row_out,
	cat(name, ' = ', substr(name,4)),
	upcase(name) like "&pred_horizon._%");

	%round_input_vars(&row_out);

%mend preprocess_row;




%macro previsione_grid(id, approccio, target, id_target, pred_horizon, sottomodello,
					id_modello, id_rete, flg_mesi_omologhi, mesi_omologhi, mesi_val,
					code_file, code_file_old, code_file_old2, mese_pr, model_string);


	/* CONTROLLO SU MODELLO S: SE HO IL MODELLO S ALLORA CONTROLLO SIA QUELLO CORRETTO PER IL GIORNO IN ESAME
		ALTRIMENTI SKIP AL MODELLO DELLA RIGA SUCCESSIVA */

	%if "&sottomodello"="S1" or "&sottomodello"="S2" %then %do;

		data _null_;
			time_day_similarita=catx("_","&pred_horizon","time_day_group1");
			call symputx("time_day_similarita",time_day_similarita);
		run;

		data time_day_similarita_input;
			set input_row(keep=&time_day_similarita);
			if &time_day_similarita=1 then modello_S_to_use="S1";
			else modello_S_to_use="S2";
			if modello_S_to_use="&sottomodello" then flg_do_previsione_S=1;
			else flg_do_previsione_S=0;
			call symputx("flg_do_previsione_S",flg_do_previsione_S);
		run;

		%if &flg_do_previsione_S=1 %then %do;

			%do_prediction();

		%end;

	%end;

	/* CONTROLLO SU MODELLO S: SE NON HO IL MODELLO S ALLORA ESEGUO LA PREVISIONE */
	%else %do;

		%do_prediction();

	%end;


%mend previsione_grid;


%macro do_prediction();

		/* Filtro colonne modello corretto + rename ultima riga del inputmodelli */
		%preprocess_row(&pred_horizon., input_row, &pred_horizon._input_row);

		/* Eseguo la previsione con l'ultimo modello disponibile */
		%if %sysfunc(fileexist("&code_file")) %then %do;

			data score_out(keep=previsione &time_id datetime_solare hour);
				set &pred_horizon._input_row;
				%include "&code_file";
				rename P_&target = previsione;
			run;

		%end;

		%else %do;

			%if %sysfunc(fileexist("&code_file_old")) %then %do;

				data score_out(keep=previsione &time_id datetime_solare hour);
					set &pred_horizon._input_row;
					%include "&code_file_old";
					rename P_&target = previsione;
				run;

			%end;

			%else %do;

				data score_out(keep=previsione &time_id datetime_solare hour);
					set &pred_horizon._input_row;
					%include "&code_file_old2";
					rename P_&target = previsione;
				run;

			%end;

		%end;

		data score_out;
			set score_out;
			length approccio $5 id_target $5 pred_horizon $2 sottomodello $5 id_modello $2 
					id_rete 8 mesi_previsione $28 previsione 8;
			if &flg_mesi_omologhi=1 then do;
				mesi_previsione="&mesi_omologhi";
			end;
			else do;
				mesi_previsione="&mesi_val";
			end;
			id_rete=&id_rete;
			id_modello="&id_modello";
			id=&id;
			approccio="&approccio";
			id_target="&id_target";
			pred_horizon="&pred_horizon";
			sottomodello="&sottomodello";
		run;

		/* Appendo in una tab temporanea */
		proc append base=preds_all data=score_out;
		run;

%mend do_prediction;

%macro get_ensemble(preds=, input_grid=);

	proc sort data=&preds out=preds_sottomodelli;
		by datetime_solare &time_id hour approccio id_target pred_horizon sottomodello;
	run;

	proc means data=preds_sottomodelli noprint mean;
	   var previsione;
	   by datetime_solare &time_id hour approccio id_target pred_horizon sottomodello;
	   output out=preds_en (drop=_:) mean(previsione)=previsione;
	run;

	data preds_en;
		set preds_en;
		id_modello="EN";
		id_rete=.;
		length mesi_previsione $28;
		drop approccio;
	run;

	proc sort data=&input_grid (keep=id_ensemble approccio_ensemble sottomodello pred_horizon) 
		out=codifica_ensemble(rename=(approccio_ensemble=approccio id_ensemble=id)) nodupkey;
		by _all_;
	run;

	proc sql;
		create table preds_en_final as 
		select a.*
			  ,b.approccio
			  ,b.id
		from preds_en as a 
		left join codifica_ensemble as b 
		on a.pred_horizon=b.pred_horizon and a.sottomodello=b.sottomodello
		order by b.approccio
		;
	quit;

	proc append base=&preds data=preds_en_final force;
	run;

%mend get_ensemble;

%macro save_preds(ds_in=, ds_out=);

	%if %sysfunc(exist(&ds_out)) %then %do;

		proc append base=&ds_out data=&ds_in force;
		run;

	%end;

	%else %do;

		data &ds_out;
			length datetime_solare &time_id. hour id 8 approccio $5 id_target $5
					 pred_horizon $2 sottomodello $5 id_modello $2 id_rete 8 mesi_previsione $28 
					previsione 8;
			format datetime_solare datetime19. &time_id. date9.;
			stop;
		run;

		proc append base=&ds_out data=&ds_in force;
		run;

	%end;


%mend save_preds;


%macro previsione();

	data input_grid0(where=(flg_filter=1));
		set &input_grid;
		datetime_solare=&start;
		hour_pred=hour(datetime_solare);
		hour_pr=put(hour_pred,z2.);
		data_solare=datepart(datetime_solare);
		data_gas=ifn(hour_pred<6, intnx('day', data_solare, -1), data_solare);
		/* Filtro Griglia per hour 13 o 14 per avere duplicati */
		if pred_horizon="G1" and id_target="TOT" then do;
			if hour_pred=14 then do;
				if hour=14 or mesi_omologhi="(5 6 8)" or mesi_val="(4 5 6 7 8 9)" or sottomodello="AR" then flg_filter=1;
				else flg_filter=0;
			end;
			else do;
				if hour=13 or mesi_omologhi="(1 2 12)" then flg_filter=1;
				else flg_filter=0;
			end;
		end;
		else do;
			if hour=13 then flg_filter=1;
			else flg_filter=0;
		end;
		/* Creo le stringhe di tutti i modelli */
		horizon_day_pred=input(substr(pred_horizon,2,1),best32.);
		data_pr=intnx("day",data_gas,horizon_day_pred);
		mese_pr_num=month(data_pr);
		mese_pr=put(month(data_pr),z2.);
		anno_pr=put(year(data_pr),z4.);
		mese_old=put(month(intnx("month",data_pr,-1)),z2.);
		anno_old = put(year(intnx('month',data_pr, -1)),z4.);
		anno_old2 = put(year(intnx('month',data_pr, -2)),z4.);
		mese_old2 = put(month(intnx('month',data_pr, -2)),z2.);
		R_id_rete=cat("R",id_rete);
		/* Nuovo modello */
		model_path="&model_path";
		model_string=catx("_",pred_horizon,sottomodello,id_modello,R_id_rete,anno_pr,mese_pr,hour_pr);
		code_file=compress(catx("",model_path,"/",model_string,".txt"));
		/* Modello mese precedente */
		model_string_old=catx("_",pred_horizon,sottomodello,id_modello,R_id_rete,anno_old,mese_old,hour_pr);
		code_file_old=compress(catx("",model_path,"/",model_string_old,".txt"));
		/* Modello due mesi fa */
		model_string_old2=catx("_",pred_horizon,sottomodello,id_modello,R_id_rete,anno_old2,mese_old2,hour_pr);
		code_file_old2=compress(catx("",model_path,"/",model_string_old2,".txt"));
		format data: date9. datetime: datetime19.;
	run;

	/* Eseguo le previsioni dei modelli della stagione corretta */
	data input_grid(where=(flg_keep=1));
		set input_grid0;
		mese_pr_to_search=put(mese_pr_num,best2.);
		mese_pr_to_search_new = mese_pr_to_search || " ";
		mesi_omologhi_new=tranwrd(mesi_omologhi,"(","( ");
		mesi_omologhi_new=tranwrd(mesi_omologhi_new,")"," )");
		mesi_val_new=tranwrd(mesi_val,"(","( ");
		mesi_val_new=tranwrd(mesi_val_new,")"," )");
		if flg_mesi_omologhi=1 then do;
			if index(mesi_omologhi_new,mese_pr_to_search_new) then flg_keep=1;
		end;
		else do;
			if index(mesi_val_new,mese_pr_to_search_new) then flg_keep=1;
		end;
	run;

	/* To debug */
/* 	data null_; */
/* 		set input_grid(obs=1); */
/* 		call symputx("id",id); */
/* 		call symputx("approccio",approccio); */
/* 		call symputx("target",target); */
/* 		call symputx("id_target",id_target); */
/* 		call symputx("pred_horizon",pred_horizon); */
/* 		call symputx("sottomodello",sottomodello); */
/* 		call symputx("id_modello",id_modello); */
/* 		call symputx("id_rete",id_rete); */
/* 		call symputx("flg_mesi_omologhi",flg_mesi_omologhi); */
/* 		call symputx("mesi_omologhi",mesi_omologhi); */
/* 		call symputx("mesi_val",mesi_val); */
/* 		call symputx("code_file",code_file); */
/* 		call symputx("code_file_old",code_file_old); */
/* 		call symputx("code_file_old2",code_file_old2); */
/* 		call symputx("mese_pr",mese_pr); */
/* 		call symputx("model_string",model_string); */
/* 	run; */

	data _null_;
		set input_grid;
		call execute ('%NRSTR (%previsione_grid('||id||', '||approccio||', '||target||',
				 	'||id_target||','||pred_horizon||', '||sottomodello||','||id_modello||', 
					'||id_rete||','||flg_mesi_omologhi||', 
					'||mesi_omologhi||', '||mesi_val||','||code_file||', '||code_file_old||',
					'||code_file_old2||','||mese_pr||','||model_string||'))');
	run;


	/* Eseguo Ensemble */
	%get_ensemble(preds=preds_all, input_grid=input_grid);

	/* Salvo le previsioni nella tabella permanente &output_prev */
	%save_preds(ds_in=preds_all, ds_out=&output_prev);

%mend previsione;


%previsione();